using System;
using System.Linq;
using UnityEngine;

namespace Level.Generation
{
    [CreateAssetMenu(fileName = nameof(WeightedWorkerFactoryConfig), 
        menuName = "Level/" + nameof(WeightedWorkerFactoryConfig))]
    public class WeightedWorkerFactoryConfig : ScriptableObject, IFactoryProvider
    {
        public WeightedWorkerFactory[] WeightedWorkerFactories;

        [SerializeField] WorkerProperties m_workerProperties;
        public WorkerProperties WorkerProperties => m_workerProperties;

        public ILevelGeneratorWorkerFactory GetFactory(IGeneratorData generatorData)
        {
            var randomValue = (float) generatorData.Random.NextDouble();
            var sum = 0.0f;
            for (var i = 0; i < WeightedWorkerFactories.Length; ++i)
            {
                sum += WeightedWorkerFactories[i].Weight;
                if (randomValue > sum)
                    continue;

                return WeightedWorkerFactories[i].Factory;
            }

            return WeightedWorkerFactories.LastOrDefault().Factory;
        }

        public Texture2D Editor_FactoryIcon => null;
    }
    
    [Serializable]
    public struct WeightedWorkerFactory
    {
        public float Weight;
        public LevelGeneratorWorkerFactory Factory;
    }
}
