using Level.Room;
using UnityEngine;

namespace Level.Generation
{
    public abstract class LevelGeneratorWorkerFactory : ScriptableObject, ILevelGeneratorWorkerFactory, IFactoryProvider
    {
        [HideInInspector]
        public string CustomName;

        public RefIFactoryProvider ContinueWithFactories;

        public abstract WorkerProperties WorkerProperties { get; }
        public ILevelGeneratorWorkerFactory GetContinuingFactories(IGeneratorData generatorData)
            => ContinueWithFactories.Result?.GetFactory(generatorData);
        
        public abstract string EditorTitle { get; }

        public ILevelGeneratorWorkerFactory GetFactory(IGeneratorData data) => this;
#if UNITY_EDITOR
        public abstract Texture2D Editor_FactoryIcon { get; }
#endif
        
        public abstract ILevelGeneratorWorker SpawnWorker(IGeneratorData data, IRoomInstance spawnerRoom, ExitInfo atExit);
    }

}
