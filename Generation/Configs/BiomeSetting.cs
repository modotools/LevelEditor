﻿using System.Collections.Generic;
using UnityEngine;

namespace Level.Generation
{
    /// <summary>
    /// A Biome defines the level generation process through the factories that are setup and their settings.
    /// </summary>
    [CreateAssetMenu(fileName = nameof(BiomeSetting), menuName = "Level/"+nameof(BiomeSetting))]
    public class BiomeSetting : ScriptableObject
    {
        /// <summary>
        /// Responsible of creating LevelGeneratorWorker. Contains settings to adjust their behaviour.
        /// </summary>
        public List<LevelGeneratorWorkerFactory> Factories = new List<LevelGeneratorWorkerFactory>();

        public RefIFactoryProvider FallbackFactory;
        public ILevelGeneratorWorkerFactory GetInitialSpawner(IGeneratorData generatorData) 
            => FallbackFactory.Result?.GetFactory(generatorData);
    }
}
