using System;
using System.Collections.Generic;
using System.Linq;
using Core.Unity.Types;
using Level.Room;
using UnityEngine;
using UnityEngine.Serialization;

namespace Level.Generation
{
    [CreateAssetMenu(fileName = nameof(RandomRoomSelectionConfig), menuName = "Level/Generation/"+nameof(RandomRoomSelectionConfig))]
    public class RandomRoomSelectionConfig : ScriptableObject, IRoomConfigProvider
    {
        [FormerlySerializedAs("Templates")] [SerializeField] 
        List<RoomConfig> m_templates = new List<RoomConfig>();
        [FormerlySerializedAs("SpawnChances")] [SerializeField] 
        List<float> m_spawnChances = new List<float>();
        
        public IEnumerable<RoomConfig> Templates => m_templates;

        public RoomConfig GetRoom(IGeneratorData data)
        {
            var spawnVal = (float) data.Random.NextDouble();

            var sum = 0.0f;
            for (var i = 0; i < m_spawnChances.Count; ++i)
            {
                sum += m_spawnChances[i];
                if (spawnVal > sum)
                    continue;

                return m_templates[i];
            }

            Debug.LogError($"RandomRoomSelectionConfig {name} skipped through all Templates, SpawnChances might not be correctly setup!");
            return m_templates.Last();
        }
        
#if UNITY_EDITOR
        public List<RoomConfig> Editor_Templates => m_templates;
        public List<float> Editor_SpawnChances => m_spawnChances;
#endif
    }
}