using System;
using Core.Unity.Types.ID;
using UnityEngine;

namespace Level.Procedural
{
    [Serializable]
    public class LevelKeyID : IDAsset
    {
        [SerializeField] KeyLockCounting m_counting;

        public KeyLockCounting Counting => m_counting;
    }
}