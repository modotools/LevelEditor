using System;
using _Editor.Level.Procedural;
using Level.Generation;
using UnityEngine;

namespace Level.Procedural
{
    /// <summary> </summary>
    [Serializable, CreateAssetMenu(fileName = nameof(LevelFactoryNode), menuName = "Level/"+nameof(LevelFactoryNode))]
    public class LevelFactoryNode : ScriptableObject
    {
        //Tag-List?
        public string Name;
        // todo GRAPH: maybe key/lock asset could also be wildcard (collection of keys/locks)
        public NodeIDData NodeID;

        public RefIRoomConfigProvider RoomProvider;
        public RefIFactoryProvider FactoryProvider;
        public Texture2D IconOverride;
    }
}