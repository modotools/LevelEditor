using UnityEngine;

namespace Level.Procedural
{
    [System.Serializable]
    public struct KeyStyleData
    {
        public LevelKeyID Key;
        // can be null
        public Texture2D Icon;
        // negative = don't care, 0 = default (fe. gray), 
        public int ColorIdx;
    }
}