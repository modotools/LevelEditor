using UnityEngine;

namespace Level.Procedural
{
    [System.Serializable]
    public class NodeBG
    {
        public Texture2D BackgroundStyleDefault;
        public Texture2D BackgroundStyleClicked;
        public Texture2D BackgroundStyleActive;
    }
}