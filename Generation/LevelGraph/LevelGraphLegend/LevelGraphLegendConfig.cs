using System.Collections.Generic;
using System.Linq;
using Core.Types;
using Core.Unity.Interface;
using UnityEngine;

namespace Level.Procedural
{
    /// <summary>
    /// A Legend for the LevelGraph, defines the Node types and key-name etc.
    /// </summary>
    [CreateAssetMenu(menuName = "Level/Procedural/LevelGraphLegendConfig")]
    public class LevelGraphLegendConfig : ScriptableObject, IVerify
    {
        // todo GRAPH: refreshing key-consumable? eg. Zelda-like growing bombs.
        // todo GRAPH: key that needs multiple locks
        // todo GRAPH: rooms needed for survival? track distance (also to home)? eg. food/ water, warm resting places w. air, 

        public KeyStyleData[] KeyData;
        public NodeTypeData[] NodeTypeData;
        public Color[] Colors;

        // todo GRAPH: Can we have kind of prefab??? then we could also link worker + components for placing keys and locks, dialogue, etc here
        // todo GRAPH: how to handle key/lock asset-wildcards (collection of keys/locks)

#if UNITY_EDITOR
        public void Editor_Verify(ref VerificationResult result)
        {
            Editor_UpdateVerifyKeys(ref result);
            Editor_UpdateTypesVerifyTypeData(ref result);
        }

        void Editor_UpdateVerifyKeys(ref VerificationResult result)
        {
            using var keyLockNamesScoped = SimplePool<Dictionary<string, KeyStyleData>>.I.GetScoped();
            var keyLockNames = keyLockNamesScoped.Obj;
            keyLockNames.Clear();
            foreach (var keyData in KeyData)
            {
                if (keyData.Key == null)
                    continue;
                
                // We cannot check ColorIdx because we don't know for which type its used. (keyData.ColorIdx > 0)
                if (keyLockNames.ContainsKey(keyData.Key.name))
                {
                    result.Warning($"Key {keyData.Key} already defined!", this);
                    continue;
                }
                keyLockNames.Add(keyData.Key.name, keyData);
            }
        }

        void Editor_UpdateTypesVerifyTypeData(ref VerificationResult result)
        {
            using var typeNamesScoped = SimplePool<Dictionary<string, NodeTypeData>>.I.GetScoped();
            var typeNames = typeNamesScoped.Obj;

            typeNames.Clear();
            foreach (var data in NodeTypeData)
            {
                if (data.Type == null)
                {
                    result.Warning("Type is not set!", this);
                    continue;
                }

                var type = data.Type.ToString();
                if (data.NodeSize.x <= 0 || data.NodeSize.y <= 0)
                    result.Warning($"type {type} with invalid NodeSize!", this);

                if (typeNames.Keys.Contains(type))
                    result.Warning($"Duplicated type {type} in NodeTypeData!", this);
                else
                    typeNames.Add(type, data);

                // foreach (var bg in data.BGData)
                // {
                if (data.NodeTextures == null)
                {
                    result.Warning($"type {type}: Empty entry in BGData!", this);
                    continue;
                }

                if (data.NodeTextures.BackgroundStyleDefault == null)
                    result.Warning($"type {type}: No Default BG for an entry in BGData!", this);
                // }
            }
        }
#endif
    }
}