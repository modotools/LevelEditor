namespace Level.Procedural
{
    // todo GRAPH: refreshing key-consumable? eg. Zelda-like growing bombs.
    // todo GRAPH: key that needs multiple locks
    // todo GRAPH: rooms needed for survival? track distance (also to home)? eg. food/ water, warm resting places w. air, 
    public enum NodeType
    {
        Default,
        KeyConsumable,
        ConsumingLock,
        PermanentKey,
        PermanentLock,
        Other // loot, monster, boss, dialogue, etc.
    }

    
    // /// <summary> </summary>
    // public enum NodeType
    // {
    //     /// <summary> Empty nodes where default room generation takes place with standard gameplay-stuff </summary>
    //     Default,
    //     /// <summary> One-way streets, could be collapsing blocks </summary>
    //     OneWayPath,
    //     /// <summary> A connection point for other parts of the graph. </summary>
    //     LoopPoint,
    //     /// <summary> fe. zelda-like default-keys or bombs - keys that are only usable for one lock (ConsumingLock) of same type </summary>
    //     Consumable,
    //     /// <summary> f.e. Metroid-Ability - key that is usable for all locks (PermaLock) of same type/ Or some kind of trigger-event </summary>
    //     Permanent,
    //     /// <summary> f.e. La-Mulana type hints for a puzzle somewhere else. </summary>
    //     Hint,
    //     /// <summary> A switch that might be needed to be touched for a puzzle, (can require Hint). </summary>
    //     Switch,
    //     /// <summary>  eg. metroid-missiles act like PermaKey the first time, then only increase amount (optional)
    //     /// or Zelda heart containers, these things could get tracked, so we can add difficulty-requirements 
    //     /// (eg. normal difficulty = 10 hearts and 5 missile upgrades) </summary>
    //     PowerUp,
    //     /// <summary> Can be setup to require keys (consumable, permanent, switch, hint)  </summary>
    //     Lock_Consumable, Lock_Permanent, Lock_Secret, Lock_Switch,
    //     /// <summary> Not a real lock, but more like a kind of difficulty meassurement </summary>
    //     Lock_Difficulty,
    //     /// <summary> loot, monster, boss, dialogue, etc. </summary>
    //     Other
    // }
}