using System.Collections.Generic;
using UnityEngine;

namespace Level.Procedural
{
    [System.Serializable]
    public struct NodeTypeData
    {
        public NodeTypeID Type;
        // public List<NodeBG> BGData;

        public NodeBG NodeTextures;
        public Vector2 NodeSize;
        public Vector2 IconSize;
        public Vector2 TextOffset;
        public Vector4 ConnectorOffsets;
    }
}