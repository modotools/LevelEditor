// using Core.Interface;
// using Core.Unity.Utility.Debug;
// using UnityEngine;
//
//
// namespace LevelGraph
// {
//     public class PathWay : MonoBehaviour
//     {
//         [SerializeField] Vector3Int m_from;
//         [SerializeField] Vector3Int m_to;
//
//         [SerializeField] Color m_color;
//
//         //public void Get(out Bounds provided)
//         //{
//         //    provided = new Bounds(transform.position, m_size);
//         //}
//
//         void OnDrawGizmos()
//         {
//             var p0 = transform.position + m_from;
//             var p1 = transform.position + m_to;
//
//             Debug.DrawLine(p0, p1, m_color);
//         }
//     }
// }
