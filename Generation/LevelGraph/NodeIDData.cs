using System;
using Level.Procedural;

namespace _Editor.Level.Procedural
{
    // used as key for getting the style of a node in dictionary
    [Serializable]
    public struct NodeIDData
    {
        public NodeTypeID NodeTypeID;
        public LevelKeyID LevelKeyID;
    }
}