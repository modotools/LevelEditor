using _Editor.Level.Procedural;
using Core;
using Core.Types;
using Core.Unity.Attributes;
using Core.Unity.Extensions;
using Core.Unity.Utility.Debug;
using Level;
using Level.Generation;
using Level.Generation.Operations;
using Level.Procedural;
using Level.Room;
using UnityEngine;

namespace LevelEditor
{
    [EditorIcon("workerarea")]
    public class LayoutWorkerArea : MonoBehaviour, IWorkerArea, IHierarchyIcon
    {
        public string HierarchyIcon => "Icons/workerarea";

        [SerializeField] LayoutConfig m_layoutConfig;
        [SerializeField] GridSettings m_gridSettings;
        [SerializeField] Color m_color;
        Vector3 m_lastPos;

        public void Get(out GridSettings provided) => provided = m_gridSettings;
        
        public void Get(out Bounds provided) => this.GetBounds(m_layoutConfig.Grid.Size.Vector3Int(1), out provided);

        public bool SpawnAllWorker(IGeneratorData data, ref DebugString errorString)
        {
            for (var x = 0; x < m_layoutConfig.Grid.Size.x; x++)
            for (var z = 0; z < m_layoutConfig.Grid.Size.y; z++)
            {
                var gridPos = new Vector2Int(x, z);
                
                if (!GetWorkerFactory(data, gridPos, out var factory, ref errorString))
                    continue;

                this.GetBoundsForGridPos(gridPos, out var bounds);
                
                var worker = factory.SpawnWorker(data, bounds, new LayoutWorkerData()
                {
                    GridPos = gridPos,
                    LayoutConfig = m_layoutConfig
                });
            
                data.AddWorker(worker, bounds.center);
            }
            return true;
        }
        
        public bool TrySpawnWorker(IGeneratorData data, IRoomInstance fromRoom, Bounds minOpBounds, ExitInfo exitInfo,
            IFactoryProvider factoryConfig, out ILevelGeneratorWorker worker, ref DebugString errorString)
        {
            worker = null;
            GetWorkerFactory(data, minOpBounds, out var gridPos, out var spawner, ref errorString);
            if (spawner == null)
            {
                errorString.AddError("No Spawner");
                return false;
            }
            
            worker = spawner.SpawnWorker(data, fromRoom, exitInfo, new LayoutWorkerData()
            {
                GridPos = gridPos,
                LayoutConfig = m_layoutConfig
            });
            
            data.AddWorker(worker, fromRoom.Bounds.center);
            return true;
        }
        
        public bool GetWorkerFactory(IGeneratorData data, Bounds opBounds, 
            out Vector2Int gridPos, 
            out IWorkerAreaFactory factory, 
            ref DebugString errorString)
        {
            this.GetGridPosForBounds(opBounds, out gridPos);
            return GetWorkerFactory(data, gridPos, out factory, ref errorString);
        }

        bool GetWorkerFactory(IGeneratorData data, Vector2Int gridPos, out IWorkerAreaFactory factory,
            ref DebugString errorString)
        {
            factory = null;

            var gridData = m_layoutConfig.GetGridData(gridPos);
            var noEntry = gridData.LayoutData.Block || gridData.LayoutData.IsEmpty;
            if (noEntry)
            {
                errorString.AddError($"No Entry at Layout grid pos {gridPos}!");
                return false;
            }

            if (gridData.RoomLevelGraphNode != null && gridData.RoomLevelGraphNode.FactoryProvider?.Result != null)
            {
                factory = gridData.RoomLevelGraphNode.FactoryProvider.Result.GetFactory(data) as IWorkerAreaFactory;
                if (factory == null)
                    errorString.AddError("FactoryProvider did not return Factory of type IWorkerAreaFactory!");

                return factory != null;
            }

            var isConnector = gridData.LayoutData.Connector;
            var defaultNode = isConnector ? m_layoutConfig.DefaultNodeConnector : m_layoutConfig.DefaultNodeRoom;
            if (defaultNode.FactoryProvider?.Result == null)
            {
                errorString.AddError("No DefaultFactory set!");
                return false;
            }

            factory = defaultNode.FactoryProvider?.Result.GetFactory(data) as IWorkerAreaFactory;
            return true;
        }

#if UNITY_EDITOR
        void OnDrawGizmos()
        {
            this.OnDrawGizmos(ref m_lastPos, m_color);

            var grid = m_layoutConfig.Grid;
            var off= m_gridSettings.HalfRoomChunkSize * new Vector3(1, 0.35f, 1);
            
            DrawAccessGizmo(grid.Entry, off, Vector3.down, Color.green);
            foreach (var ex in grid.Exits)
                DrawAccessGizmo(ex, off, Vector3.down, Color.red);

            // DrawArrow.ForDebug(entryPos, Vector3.forward);
            
            UnityEditor.Handles.BeginGUI();
            var size = grid.Size;
            for (var y = 0; y < size.y; y++)
            for (var x = 0; x < size.x; x++)
            {
                var chunkPos = m_gridSettings.RoomChunkSize * new Vector3(x,0, y);
                var pos = transform.position + chunkPos + off;
                
                var screenPosition = CalculateScreenPosition(pos, out _);
                var tile = grid.Grid[y * size.x + x];
                
                if (tile.RoomLevelGraphNode == null || tile.RoomLevelGraphNode.IconOverride == null)
                    continue;
                var icon = tile.RoomLevelGraphNode.IconOverride;
                
                // Debug.Log($"Drawing {icon} at {screenPosition}");

                // I dont understand why y-50 is correct instead of 25
                GUI.DrawTexture(new Rect(screenPosition.x-25, Screen.height-screenPosition.y-50, 50, 50), icon, ScaleMode.ScaleToFit, true);
            }
            UnityEditor.Handles.EndGUI();
        }

        void DrawAccessGizmo(LevelLayoutGridAccess access, Vector3 off, Vector3 defaultDir, Color exitColor)
        {
            var chunkPos = m_gridSettings.RoomChunkSize * access.GridPos.Vector3();
            var pos = transform.position + chunkPos + off;

            var dir = defaultDir;
            switch (access.Direction)
            {
                case Cardinals.North: dir = Vector3.back; pos += 2*Vector3.forward; break;
                case Cardinals.East: dir = Vector3.left; pos += 2*Vector3.right;  break;
                case Cardinals.South: dir = Vector3.forward; pos += 2*Vector3.back;  break;
                case Cardinals.West: dir = Vector3.right; pos += 2*Vector3.left; break;
            }

            DrawArrow.GizmoDrawLineArrow(pos, dir, exitColor, 4.5f, arrowPosition: 1f);
        }

        static Vector2 CalculateScreenPosition(Vector3 position, out float z)
        {
            var sv = UnityEditor.SceneView.currentDrawingSceneView;
            z = 0f;
            if (sv == null)
                return Vector2.zero;
            var cam = sv.camera;
            var v3 = cam == null ? default : cam.WorldToScreenPoint(position);
            z = v3.z;
            return new Vector2(v3.x, v3.y);
        }
        #endif
    }
}
