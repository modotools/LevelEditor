using System;
using System.Collections.Generic;
using Core.Types;
using Level.Generation;
using Level.Procedural;
using UnityEngine;

namespace _Editor.Level.Procedural
{
    [Serializable]
    public struct LevelLayoutGrid
    {
        public LevelFactoryNode LevelFactoryNode;
        public WorkerProperties WorkerProperties
        {
            get
            {
                if (LevelFactoryNode == null)
                    return default;
                return LevelFactoryNode.FactoryProvider?.Result?.WorkerProperties ?? default;
            }
        }

        public LevelNodeGridData[] Grid;
        public Vector2Int Size;

        public LevelLayoutGridAccess Entry;
        public LevelLayoutGridAccess[] Exits;
    }
    
    [Serializable]
    public struct LevelLayoutGridAccess
    {
        public Vector2Int GridPos;
        public Cardinals Direction;
    }
    
    public struct LevelGraphNodeGrids
    {
        public Dictionary<LevelGraphNode, LevelLayoutGrid> Grids;
    }
}