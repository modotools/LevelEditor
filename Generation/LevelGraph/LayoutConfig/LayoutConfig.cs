using _Editor.Level.Procedural;
using Core.Extensions;
using Core.Types;
using Core.Unity.Interface;
using Level.Generation;
using UnityEngine;
using static Level.Procedural.LevelLayoutGridOperations;

namespace Level.Procedural
{
    /// <summary>
    /// The LayoutConfig defines a layout of rooms
    /// </summary>
    [CreateAssetMenu(fileName = nameof(LayoutConfig), menuName = "Level/Procedural/"+nameof(LayoutConfig))]
    public class LayoutConfig : ScriptableObject, IVerify
    {
        [SerializeField] LevelGraphLegendConfig m_legend;
        [SerializeField] LevelLayoutGrid m_grid;

        [SerializeField] LevelFactoryNode m_defaultNodeRoom;
        [SerializeField] LevelFactoryNode m_defaultNodeConnector;
        
        [SerializeField] RefIFactoryProvider m_defaultFactoryProvider;
        [SerializeField] RefIRoomConfigProvider m_defaultRoomConfigProvider;
        
        public LevelLayoutGrid Grid => m_grid;
        public LevelNodeGridData GetGridData(Vector2Int pos)
        {
            var idx = pos.y * Grid.Size.x + pos.x;
            if (!idx.IsInRange(Grid.Grid))
            {
                Debug.LogError($"{pos} not in Range of grid {this}");
                return default;
            }
            return Grid.Grid[pos.y * Grid.Size.x + pos.x];
        }

        public LevelFactoryNode DefaultNodeRoom => m_defaultNodeRoom;
        public LevelFactoryNode DefaultNodeConnector => m_defaultNodeConnector;
        
        // public IFactoryProvider DefaultFactoryRoom => m_defaultNodeRoom.FactoryProvider.Result;
        // public IRoomConfigProvider DefaultRoomProviderRoom => m_defaultNodeRoom.RoomProvider.Result;
        // public IFactoryProvider DefaultFactoryConnector => m_defaultNodeConnector.FactoryProvider.Result;
        // public IRoomConfigProvider DefaultRoomProviderConnector => m_defaultNodeConnector.RoomProvider.Result;

        #region Editor
#if UNITY_EDITOR
        public LevelGraphLegendConfig Editor_LegendConfig
        {
            get => m_legend;
            set => m_legend = value;
        }

        public static string Editor_DefaultNodeRoomPropName => nameof(m_defaultNodeRoom);
        public static string Editor_DefaultNodeConnectorPropName => nameof(m_defaultNodeConnector);

        public ref LevelLayoutGrid Editor_Grid => ref m_grid;
        public static string Editor_GridPropName => nameof(m_grid);

        public void Editor_Verify(ref VerificationResult result)
        {
            Editor_VerifyAccess(ref result, m_grid.Entry, "Entry");
            for (var i = 0; i < m_grid.Exits.Length; i++) 
                Editor_VerifyAccess(ref result, m_grid.Exits[i], $"Exit {i}");
        }

        void Editor_VerifyAccess(ref VerificationResult result, LevelLayoutGridAccess access, string accessName)
        {
            if (!OnGrid(m_grid, access.GridPos))
            {
                result.Error($"Access {accessName} is not on the grid!", this);
                return;
            }

            var data = m_grid.Get(access.GridPos);
            if (data.LayoutData.Block)
            {
                result.Error($"Access {accessName} is blocked!", this);
                return;
            }

            var needConnection = access.Direction == Cardinals.Invalid ? 1 : 2;
            var connections = CountConnectionsAndExtension(data.LayoutData);

            if (connections < needConnection)
            {
                result.Error($"Access {accessName} at {access.GridPos} has less connections, then needed for this exit-type!" +
                             $" Connections {connections} needed {needConnection}", this);
                return;
            }

            if (!ConnectionOK(ref m_grid, access))
            {
                result.Error($"Access {accessName} does not connect to the access-direction!", this);
                return;
            }
            
            if (access.Direction != Cardinals.Invalid && OnGrid(m_grid, GetEntryGridPos(access)))
                result.Error($"Access {accessName} connects to position inside the grid!", this);
        }
#endif
        #endregion
    }
}