using System;
using Level.Procedural;

namespace _Editor.Level.Procedural
{
    [Serializable]
    public struct LevelNodeGridData
    {
        public LevelNodeGridLayoutData LayoutData;

        public LevelFactoryNode RoomLevelGraphNode;

        public LevelFactoryNode R;
        public LevelFactoryNode L;
        public LevelFactoryNode T;
        public LevelFactoryNode B;

        public static LevelNodeGridData Default => new LevelNodeGridData();
    }

    public enum ConnectionType
    {
        None,
        Path,
        RoomExtension
    }

    [Serializable]
    public struct LevelNodeGridLayoutData
    {
        public bool Block;

        public bool Connector;
        
        // 0 no connection, 1 = path, 2 = extend room 
        public ConnectionType R;
        public ConnectionType L;
        public ConnectionType T;
        public ConnectionType B;
        
        public bool IsEmpty => R == 0 && L == 0 && T == 0 && B == 0;
    }
}