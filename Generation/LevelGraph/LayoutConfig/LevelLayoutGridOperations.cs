using System;
using _Editor.Level.Procedural;
using Core.Extensions;
using Core.Types;
using UnityEngine;
using static Core.Extensions.CollectionExtensions;

namespace Level.Procedural
{
    public static class LevelLayoutGridOperations
    {
        static LevelNodeGridData m_default = LevelNodeGridData.Default;

        public static bool OnGrid(LevelLayoutGrid grid, Vector2Int tilePos) => tilePos.x >= 0 && tilePos.y
            >= 0 && tilePos.x < grid.Size.x && tilePos.y < grid.Size.y;

        public static ref LevelNodeGridData Get(this ref LevelLayoutGrid grid, Vector2Int tilePos)
            => ref grid.Grid[tilePos.y * grid.Size.x + tilePos.x];

        public static ref LevelNodeGridData GetNext(this ref LevelLayoutGrid grid, Vector2Int tilePos)
        {
            if (!OnGrid(grid, tilePos))
                return ref m_default;

            return ref Get(ref grid, tilePos);
        }
        public static void CopyTopPassageToNextTileBottom(ref LevelLayoutGrid grid, Vector2Int selectedTile)
        {
            var layout = Get(ref grid, selectedTile).LayoutData;
            ref var next = ref GetNext(ref grid, selectedTile + Vector2Int.up);
            next.LayoutData.B = layout.T;
        }
        public static void CopyBottomPassageToNextTileTop(ref LevelLayoutGrid grid, Vector2Int selectedTile)
        {
            var layout = Get(ref grid, selectedTile).LayoutData;
            ref var next = ref GetNext(ref grid, selectedTile + Vector2Int.down);
            next.LayoutData.T = layout.B;
        }
        public static void CopyRightPassageToNextTileLeft(ref LevelLayoutGrid grid, Vector2Int selectedTile)
        {
            var layout = Get(ref grid, selectedTile).LayoutData;
            ref var next = ref GetNext(ref grid, selectedTile + Vector2Int.right);
            next.LayoutData.L = layout.R;
        }
        public static void CopyLeftPassageToNextTileRight(ref LevelLayoutGrid grid, Vector2Int selectedTile)
        {
            var layout = Get(ref grid, selectedTile).LayoutData;
            ref var next = ref GetNext(ref grid, selectedTile + Vector2Int.left);
            next.LayoutData.R = layout.L;
        }
        
        public static int CountConnectionsAndExtension(LevelNodeGridLayoutData data)
        {
            var connections = 0;
            if (data.B > 0) ++connections;
            if (data.L > 0) ++connections;
            if (data.R > 0) ++connections;
            if (data.T > 0) ++connections;
            return connections;
        }
        
        public static Vector2Int GetEntryGridPos(LevelLayoutGridAccess access)
        {
            var entryFrom = access.GridPos;
            switch (access.Direction)
            {
                case Cardinals.North: entryFrom += Vector2Int.up; break;
                case Cardinals.East: entryFrom += Vector2Int.right; break;
                case Cardinals.South: entryFrom += Vector2Int.down; break;
                case Cardinals.West: entryFrom += Vector2Int.left; break;
            }

            return entryFrom;
        }
        
        public static bool ConnectionOK(ref LevelLayoutGrid grid, LevelLayoutGridAccess access)
        {
            var tile = Get(ref grid, access.GridPos).LayoutData;
            switch (access.Direction)
            {
                case Cardinals.South when tile.B != ConnectionType.Path:
                case Cardinals.North when tile.T != ConnectionType.Path:
                case Cardinals.East when tile.R != ConnectionType.Path:
                case Cardinals.West when tile.L != ConnectionType.Path:
                    return false;
                default:
                    return true;
            }
        }

        public static void Rotate(ref LevelLayoutGrid grid, int count)
        {
            var dir = (int) Mathf.Sign(count);
            var left = dir < 0;
            for (var i = 0; i < Mathf.Abs(count); i++)
            {
                var newGrid = new LevelNodeGridData[grid.Grid.Length];
                CollectionExtensions.Rotate(grid.Grid, newGrid, 
                    grid.Size.x, grid.Size.y, dir);
                grid.Grid = newGrid;
                
                var size= grid.Size;
                grid.Size = new Vector2Int(size.y, size.x);

                if (left)
                    RotateTileConnectionsLeft(ref grid);
                else
                    RotateTileConnectionsRight(ref grid);
                
                RotateAccess(ref grid.Entry, dir, size);
                for (var exitIdx = 0; exitIdx < grid.Exits.Length; exitIdx++)
                    RotateAccess(ref grid.Exits[exitIdx], dir, size);
            }
        }

        static void RotateTileConnectionsRight(ref LevelLayoutGrid grid)
        {
            for (var y = 0; y < grid.Size.y; ++y)
            for (var x = 0; x < grid.Size.x; ++x)
            {
                ref var gridTile = ref grid.Grid[y * grid.Size.x + x];
                var prev = grid.Grid[y * grid.Size.x + x];
                RotateTileConnectionsRight(ref gridTile, prev);
            }
        }
        static void RotateTileConnectionsLeft(ref LevelLayoutGrid grid)
        {
            for (var y = 0; y < grid.Size.y; ++y)
            for (var x = 0; x < grid.Size.x; ++x)
            {
                ref var gridTile = ref grid.Grid[y * grid.Size.x + x];
                var prev = grid.Grid[y * grid.Size.x + x];
                RotateTileConnectionsLeft(ref gridTile, prev);
            }
        }

        static void RotateTileConnectionsRight(ref LevelNodeGridData gridTile, LevelNodeGridData prev)
        {
            gridTile.T = prev.L;
            gridTile.L = prev.B;
            gridTile.B = prev.R;
            gridTile.R = prev.T;

            gridTile.LayoutData.T = prev.LayoutData.L;
            gridTile.LayoutData.L = prev.LayoutData.B;
            gridTile.LayoutData.B = prev.LayoutData.R;
            gridTile.LayoutData.R = prev.LayoutData.T;
        }
        static void RotateTileConnectionsLeft(ref LevelNodeGridData gridTile, LevelNodeGridData prev)
        {
            gridTile.T = prev.R;
            gridTile.L = prev.T;
            gridTile.B = prev.L;
            gridTile.R = prev.B;

            gridTile.LayoutData.L = prev.LayoutData.T;
            gridTile.LayoutData.B = prev.LayoutData.L;
            gridTile.LayoutData.R = prev.LayoutData.B;
            gridTile.LayoutData.T = prev.LayoutData.R;
        }

        static void RotateAccess(ref LevelLayoutGridAccess access, int dir, Vector2Int origSize)
        {
            var left = dir < 0;

            var entryPos = access.GridPos;
            var newX = dir > 0 ? entryPos.y : origSize.x - (entryPos.y + 1);
            var newY = dir > 0 ? origSize.y - (entryPos.x + 1) : entryPos.x;
            access.GridPos = new Vector2Int(newX, newY);
            if (access.Direction == Cardinals.Invalid) 
                return;
            
            var entryDirection = (int) access.Direction;
            entryDirection += left ? 3: 1;
            access.Direction = (Cardinals) (entryDirection % 4);
        }
        
        public static void ApplySizePosition(ref LevelLayoutGrid grid, Vector2Int newSize,
            Vector2Int tileModifier)
        {
            var newData = new LevelNodeGridData[newSize.x * newSize.y];
            ApplySizePosition(ref grid, newSize, tileModifier, newData);
            ShiftEntryAndExits(ref grid, tileModifier);

            grid.Grid = newData;
            grid.Size = newSize;
        }

        static void ShiftEntryAndExits(ref LevelLayoutGrid grid, Vector2Int tileModifier)
        {
            grid.Entry.GridPos += tileModifier;
            for (var i = 0; i < grid.Exits.Length; i++)
                grid.Exits[i].GridPos += tileModifier;
        }

        public static void ApplySizePosition(ref LevelLayoutGrid grid, Vector2Int newSize,
            Vector2Int tileModifier, LevelNodeGridData[] newGridData)
        {
            for (var z = 0; z < newSize.y; ++z)
            for (var x = 0; x < newSize.x; ++x)
            {
                var oldX = x - tileModifier.x;
                var oldZ = z - tileModifier.y;
                var idx = z * newSize.x + x;

                var prevPos = new Vector2Int(oldX, oldZ);
                if (!OnGrid(grid, prevPos))
                    continue;

                newGridData[idx] = Get(ref grid, prevPos);
            }
        }
        public static void CopyGridToModifiedGrid(
            ref LevelLayoutGrid fromGrid, 
            ref LevelLayoutGrid toGrid, 
            Vector2Int newSize,
            Vector2Int tileModifier) =>
            ApplySizePosition(ref fromGrid, newSize, tileModifier, toGrid.Grid);

        public static void CopyExits(LevelLayoutGrid fromGrid, ref LevelLayoutGrid toGrid, Vector2Int tileModifier)
        {
            toGrid.Exits = new LevelLayoutGridAccess[fromGrid.Exits.Length];
            for (var i = 0; i < toGrid.Exits.Length; i++)
            {
                toGrid.Exits[i].Direction = fromGrid.Exits[i].Direction;
                toGrid.Exits[i].GridPos = fromGrid.Exits[i].GridPos + tileModifier;
            }
        }
        public static void AppendExits(LevelLayoutGrid fromGrid, ref LevelLayoutGrid toGrid, Vector2Int tileModifier)
        {
            var prevExits = toGrid.Exits?.Length ?? 0;
            toGrid.Exits ??= new LevelLayoutGridAccess[fromGrid.Exits.Length];
            Array.Resize(ref toGrid.Exits, prevExits + fromGrid.Exits.Length);
            for (var i = prevExits; i < prevExits + toGrid.Exits.Length; i++)
            {
                toGrid.Exits[i].Direction = fromGrid.Exits[i-prevExits].Direction;
                toGrid.Exits[i].GridPos = fromGrid.Exits[i-prevExits].GridPos + tileModifier;
            }
        }
    }
}