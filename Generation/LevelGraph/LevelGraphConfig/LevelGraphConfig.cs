using System;
using System.Collections.Generic;
using UnityEngine;

namespace Level.Procedural
{

    /// <summary>
    /// A LevelGraph defines the linearity/ branching, how the player is able to progress 
    /// or how much/ which key-items/ abilities to gain and use.
    /// These LevelGraphs can then be used to create a 3d-layout and worker will then follow this layout,
    /// which helps with decisions like spawning and turning direction and more.
    /// </summary>
    [CreateAssetMenu(menuName = "Level/Procedural/LevelGraphConfig")]
    public class LevelGraphConfig : ScriptableObject, ISerializationCallbackReceiver
    {
        [NonSerialized] public LevelGraphNode EntryLevelGraphNode;
        [SerializeField, HideInInspector] public List<LevelGraphNode> Nodes;

        [SerializeField] public LevelGraphLegendConfig LegendConfig;
        [SerializeField] public GridSettings GridSettings;
        [SerializeField] public RunData[] RunData;
        
        void OnEnable() => this.CreateEntryNode();

        /// <summary> </summary>
        public void OnBeforeSerialize()
        {
            Nodes ??= new List<LevelGraphNode>();
            Nodes.Clear();
            
            if (EntryLevelGraphNode == null)
                return;

            this.AddAndMapNodes(EntryLevelGraphNode);
        }
        
        /// <summary> </summary>
        public void OnAfterDeserialize() => this.MapNodesFromIndices();
    }
}