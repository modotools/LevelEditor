using System;
using Core.Extensions;
using static Core.Extensions.CollectionExtensions;

namespace Level.Procedural
{
    public static class LevelGraphConfigOperations
    {
        public static void CreateEntryNode(this LevelGraphConfig config) => config.EntryLevelGraphNode ??= new LevelGraphNode();

        public static void AddNode(this LevelGraphConfig config, LevelGraphNode levelGraphNode)
        {
            var newNode = new LevelGraphNode();
            if (levelGraphNode != null)
            {
                levelGraphNode.ConnectedTo ??= new LevelGraphNode[0];
                var idx = levelGraphNode.ConnectedTo.Length;
                Array.Resize(ref levelGraphNode.ConnectedTo, idx + 1);
                levelGraphNode.ConnectedTo[idx] = newNode;
            }   
            else config.EntryLevelGraphNode = newNode;
        }
        
        public static void RemoveNodeFrom(this LevelGraphConfig config, LevelGraphNode levelGraphNode, LevelGraphNode prevLevelGraphNode)
        {
            if (levelGraphNode.ConnectedTo.IsNullOrEmpty())
            {
                config.WipeNodeFrom(levelGraphNode, prevLevelGraphNode);
                return;
            }

            var l = levelGraphNode.ConnectedTo.Length;
            
            if (prevLevelGraphNode == null)
            {
                config.EntryLevelGraphNode = l == 1 
                    ? levelGraphNode.ConnectedTo[0] 
                    : new LevelGraphNode {ConnectedTo = levelGraphNode.ConnectedTo};
                return;
            }

            Remove(ref prevLevelGraphNode.ConnectedTo, levelGraphNode);
            if (prevLevelGraphNode.ConnectedTo.IsNullOrEmpty())
            {
                prevLevelGraphNode.ConnectedTo = levelGraphNode.ConnectedTo;
                return;
            }

            var prevL = prevLevelGraphNode.ConnectedTo.Length;
            Array.Resize(ref prevLevelGraphNode.ConnectedTo, prevL + l);
            for (var i = prevL; i < prevL+l; i++) 
                prevLevelGraphNode.ConnectedTo[i] = levelGraphNode.ConnectedTo[i - prevL];
        }

        public static void WipeNodeFrom(this LevelGraphConfig config, LevelGraphNode levelGraphNode, LevelGraphNode prevLevelGraphNode)
        {
            if (prevLevelGraphNode == null)
            {
                config.EntryLevelGraphNode = new LevelGraphNode();
                return;
            }
            Remove(ref prevLevelGraphNode.ConnectedTo, levelGraphNode);
        }
        
        public static void MoveLeft(this LevelGraphConfig config, LevelGraphNode levelGraphNode, LevelGraphNode prevLevelGraphNode)
        {
            if (prevLevelGraphNode == null)
                return;
            var idxOfNode = prevLevelGraphNode.ConnectedTo.FirstIndexWhere(a => a == levelGraphNode);
            var targetIdx = idxOfNode - 1;
            config.Swap(prevLevelGraphNode, idxOfNode, targetIdx);
        }
        
        public static void MoveRight(this LevelGraphConfig config, LevelGraphNode levelGraphNode, LevelGraphNode prevLevelGraphNode)
        {
            if (prevLevelGraphNode == null)
                return;
            var idxOfNode = prevLevelGraphNode.ConnectedTo.FirstIndexWhere(a => a == levelGraphNode);
            var targetIdx = idxOfNode + 1;
            config.Swap(prevLevelGraphNode, idxOfNode, targetIdx);
        }

        public static void Swap(this LevelGraphConfig config, LevelGraphNode levelGraphNode, int idxA, int idxB)
        {
            var a = levelGraphNode.ConnectedTo[idxA];
            var b = levelGraphNode.ConnectedTo[idxB];

            levelGraphNode.ConnectedTo[idxB] = a;
            levelGraphNode.ConnectedTo[idxA] = b;
        }

        public static void AddAndMapNodes(this LevelGraphConfig config, LevelGraphNode fromLevelGraphNode)
        {
            config.Nodes.Add(fromLevelGraphNode);

            var i = 0;
            while (i < config.Nodes.Count)
            {
                var node = config.Nodes[i];
                if (node.ConnectedTo != null)
                {
                    node.ConnectedToIdx ??= new int[node.ConnectedTo.Length];
                    Array.Resize(ref node.ConnectedToIdx, node.ConnectedTo.Length);
                    for (var ci = 0; ci < node.ConnectedTo.Length; ci++)
                    {
                        var connected = node.ConnectedTo[ci];
                        config.Nodes.Add(connected);
                        node.ConnectedToIdx[ci] = config.Nodes.Count - 1;
                    }
                }
                else node.ConnectedToIdx = null;

                ++i;
            }
        }

        /// <summary> </summary>
        public static void MapNodesFromIndices(this LevelGraphConfig config)
        {
            if (config.Nodes == null || config.Nodes.Count <= 0)
                return;
            config.EntryLevelGraphNode = config.Nodes[0];

            var i = 0;
            while (i < config.Nodes.Count)
            {
                var node = config.Nodes[i];
                if (node.ConnectedToIdx != null)
                {
                    node.ConnectedTo ??= new LevelGraphNode[node.ConnectedToIdx.Length];
                    Array.Resize(ref node.ConnectedTo, node.ConnectedToIdx.Length);
                    for (var ci = 0; ci < node.ConnectedToIdx.Length; ci++)
                        node.ConnectedTo[ci] = config.Nodes[node.ConnectedToIdx[ci]];
                }
                else node.ConnectedTo = null;

                ++i;
            }
        }
    }
}