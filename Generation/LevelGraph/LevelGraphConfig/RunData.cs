using System;

namespace Level.Procedural
{
    /// <summary> </summary>
    [Serializable]
    public struct RunData
    {
        public KeyAmountData[] Keys;
        public KeyAmountData[] Results;
    };

    [Serializable]
    public struct KeyAmountData
    {
        public LevelKeyID Key;
        public int Count;
    }
}