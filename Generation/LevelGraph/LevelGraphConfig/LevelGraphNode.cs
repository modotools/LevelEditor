using System;
using _Editor.Level.Procedural;
using Level.Generation;
using UnityEngine;

namespace Level.Procedural
{
    // todo GRAPH: verification,

    /// <summary> </summary>
    [Serializable]
    public class LevelGraphNode
    {
        public static NodeTypeID DefaultID;

        public LevelFactoryNode FactoryNode;

        public int[] ConnectedToIdx = null;
        public bool HasEnd;
        public bool LoopBack;

        public Vector3Int MinSize;

        [NonSerialized] public LevelGraphNode[] ConnectedTo;
        [NonSerialized] public Vector2 NodePos;
        
        public string Name => FactoryNode == null ? "" : FactoryNode.Name;

        public NodeIDData IDData => new NodeIDData() {LevelKeyID = KeyLockID, NodeTypeID = Type};
        public NodeTypeID Type => FactoryNode == null ? DefaultID : FactoryNode.NodeID.NodeTypeID;
        public LevelKeyID KeyLockID => FactoryNode == null ? null : FactoryNode.NodeID.LevelKeyID;
        public IFactoryProvider FactoryProvider =>  FactoryNode == null ? null : FactoryNode.FactoryProvider?.Result;
        public Texture2D IconOverride => FactoryNode == null ? null : FactoryNode.IconOverride;
    };
}