using System;
using Core.Unity.Types.ID;
using UnityEngine;

namespace Level.Procedural
{
    [Serializable]
    public class NodeTypeID : IDAsset
    {
        [SerializeField] KeyLockType m_isKeyLock;
        [SerializeField] KeyLockCounting m_counting;

        public bool IsKeyLock => m_isKeyLock != KeyLockType.None;
        public bool IsKey => m_isKeyLock == KeyLockType.Key;
        public bool IsLock => m_isKeyLock == KeyLockType.Lock;
        public KeyLockType KeyLockType => m_isKeyLock;
        public KeyLockCounting Counting => m_counting;
    }
    
    public enum KeyLockType
    {
        None,
        Key,
        Lock,
    }
    public enum KeyLockCounting
    {
        None,
        Consumable,
        Permanent,
    }
}