﻿using System.Collections.Generic;
using Core.Types;
using Core.Unity.Extensions;
using Level.Enums;
using Level.PlatformLayer;
using Level.PlatformLayer.Interface;
using Level.Room;
using Level.Room.Operations;
using Level.Tiles;
using UnityEngine;
using static Level.PlatformLayer.Operations;

namespace Level.Generation.Operations
{
    public static partial class WorkerOperations
    {
        #region Public
        public static OperationResult FillLevelExceptEdge(IRoomInstance room, int lvlIdx, TilesSetFilter fillingTile)
        {
            var lvl = room.GetPlatformLayer(lvlIdx);
            var allOk = true;
            for (var z = 0; z < lvl.TileDim.y; ++z)
            {
                for (var x = 0; x < lvl.TileDim.x; ++x)
                {
                    var rx = x + lvl.Position.x;
                    var rz = z + lvl.Position.y;

                    if (lvl.IsInsideWithoutEdge(new Vector2Int(x, z)))
                        allOk &= SetTile(lvl, lvl, new Vector2Int(rx, rz), fillingTile) == OperationResult.OK;
                }
            }

            return allOk ? OperationResult.OK : OperationResult.Error;
        }

        public static OperationResult AddTilesAtBounds(IRoomInstance room, IEnumerable<Bounds> bounds,
            GeometryType type = GeometryType.Floor)
        {
            var geoFilter = room.RoomSettings.GetFilter(type);
            var allOk = true;
            foreach (var ex in bounds)
            {
                var min = ex.min - room.Bounds.min;
                var max = ex.max - room.Bounds.min;
                var bSize = max - min;
                var relativeBounds = new Bounds(room.Position.Vector3() + min + bSize / 2, bSize);
                var levelIdx = (int)(min.y);

                if (ExpandLevelIfRequired(room, levelIdx) == OperationResult.Error)
                {
                    if (DebugInfo.Action == OutOfLevelBoundsAction.IgnoreTile)
                        continue;
                    allOk = ReportErrorHelper("AddTilesAtBounds failed, because bounds are above or below Room! " +
                                              $"Bounds:{relativeBounds} Room-Bounds{room.Bounds}",
                        relativeBounds, room.Bounds);
                    continue;
                }

                //Debug.Log("levelIdx: " + levelIdx + " LevelCount: "+ room.Levels.Count);
                var lvlBounds = GetBounds(room.GetPlatformLayer(levelIdx));

                //TODO!!
                //if (errorRep.action == OutOfLevelBoundsAction.IgnoreTile)
                //{
                //    min = new Vector3(Mathf.Max(min.x, lvlBounds.min.x), 0.0f, Mathf.Max(min.z, lvlBounds.min.z));
                //    max = new Vector3(Mathf.Min(max.x, lvlBounds.max.x), 0.0f, Mathf.Min(max.z, lvlBounds.max.z));
                //}
                //else 
                if (lvlBounds.GetIntersectionType(relativeBounds) != IntersectionType.Contains)
                {
                    if (DebugInfo.Action == OutOfLevelBoundsAction.Error)
                    {
                        allOk = ReportErrorHelper(
                            $"AddTilesAtBounds failed because bounds are not in level Bounds:{relativeBounds} Level-pos{lvlBounds} extends",
                            relativeBounds, lvlBounds);
                        continue;
                    }

                    if (DebugInfo.Action == OutOfLevelBoundsAction.ExpandBounds)
                    {
                        allOk = ReportErrorHelper(
                            $"ExpandBounds not implemented! level Bounds:{relativeBounds} Level-pos{lvlBounds} extends",
                            relativeBounds, lvlBounds);
                        continue;

                        //todo: this is untested, probably does not work, check method in RoomEditor to expand level
                        //level.Expand(new Vector2Int((int)Mathf.Min(min.x, lvlBounds.min.x), (int)Mathf.Min(min.z, lvlBounds.min.z)),
                        //        new Vector2Int((int)Mathf.Max(max.x, lvlBounds.max.x), (int)Mathf.Max(max.z, lvlBounds.max.z)));
                    }
                }

                for (var x = (int)Mathf.Ceil(min.x); x <= Mathf.Floor(max.x); ++x)
                {
                    for (var z = (int)Mathf.Ceil(min.z); z <= Mathf.Floor(max.z); ++z)
                    {
                        var lvl = room.GetPlatformLayer(levelIdx);
                        SetTile(lvl, lvl, new Vector2Int(x, z), geoFilter);
                    }
                }
            }

            if (DebugInfo.Action != OutOfLevelBoundsAction.IgnoreTile && !allOk)
                DebugInfo.Result = OperationResult.Error;

            return allOk ? OperationResult.OK : OperationResult.Error;
        }

        public static void CreatePassage(IRoomInstance room, int lvlIdx, Vector2Int tileA, Vector2Int tileB, TilesSetFilter drawFilter)
        {
            var line = GetLine(tileA, tileB);
            foreach (var c in line)
                _DrawCircle(room, lvlIdx, c, 1, drawFilter);
        }

        public static void DrawCircle(IRoomInstance room, int lvlIdx, Vector2Int c, int r, TilesSetFilter drawFilter) 
            => _DrawCircle(room, lvlIdx, c, r, drawFilter);
                
        public static void RandomFillRoomLevel(this IRoomInstance room, int levelIdx, int randomFillPercent, 
            TilesSetFilter filterA, TilesSetFilter filterB, string seed = null)
        {
            var useRandomSeed = seed == null;
            if (useRandomSeed)
                seed = $"{System.DateTime.Now}";

            var rand = new System.Random(seed.GetHashCode());

            var level = room.GetPlatformLayer(levelIdx);
            for (var x = 0; x < level.TileDim.x; ++x)
            {
                for (var z = 0; z < level.TileDim.y; ++z)
                {
                    var rx = level.Position.x + x;
                    var rz = level.Position.y + z;

                    if (level.IsOnEdge(x, z))
                        SetTile(level, level, new Vector2Int(rx, rz), filterB); //level.Tiles[z * room.Size.x + x] = (int)TileType.StandardWall;
                    else
                        SetTile(level, level, new Vector2Int(rx, rz), rand.Next(0, 100) < randomFillPercent
                            ? filterB : filterA);
                }
            }
        }

        public static void SmoothMap(this IRoomInstance room, int levelIdx, 
            TilesSetFilter filterA, TilesSetFilter filterB, 
            int thresholdFilterA = 4, int thresholdFilterB = 4)
        {
            var lvl = room.GetPlatformLayer(levelIdx);

            IPlatformLayer newLevel = new PlatformLayerInstance(room.RoomSettings.Grid, lvl.Size, lvl.Position, Vector3Int.zero);
            lvl.Tiles.CopyTo(newLevel.Tiles, 0);
            for (var x = 0; x < lvl.TileDim.x; ++x)
            {
                for (var z = 0; z < lvl.TileDim.y; ++z)
                {
                    var tile = GetTile(newLevel, newLevel, new Vector2Int(x, z));
                    var neighborWallTiles = GetSurroundingWallCount(room, lvl, x, z);
                    if (neighborWallTiles > thresholdFilterB && filterA.IsTileActive(tile))
                        SetTile(newLevel, newLevel, new Vector2Int(x, z), filterB);
                    if (neighborWallTiles < thresholdFilterA && !filterA.IsTileActive(tile))
                        SetTile(newLevel, newLevel, new Vector2Int(x, z), filterA);
                }
            }

            newLevel.Tiles.CopyTo(lvl.Tiles, 0);
        }

        public static void DestroyRegionsThatAreTooSmall(this IRoomInstance room, int levelIdx, TilesSetFilter tileFilter, TilesSetFilter replaceFilter,
            int threshold)
        {
            var lvl = room.GetPlatformLayer(levelIdx);
            var regions = GetRegions(lvl, tileFilter);

            foreach (var region in regions)
            {
                if (region.Count >= threshold)
                    continue;

                // destroy:
                foreach (var tile in region)
                {
                    var rx = lvl.Position.x + tile.x;
                    var rz = lvl.Position.y + tile.y;

                    SetTile(lvl, lvl, new Vector2Int(rx, rz), replaceFilter);
                }
            }
        }
        
        public static void ConnectClosestAreas(this IRoomInstance room, int lvlIdx, TilesSetFilter connectFilter)
        {
            var lvl = room.GetPlatformLayer(lvlIdx);
            var regions = GetRegions(lvl, connectFilter);
            var areas = new List<LevelArea>();
            foreach (var r in regions) 
                areas.Add(new LevelArea(r, room, lvlIdx));

            areas.Sort();
            if (areas.Count > 0)
            {
                areas[0].IsMainArea = true;
                areas[0].IsAccessibleFromMainLevelArea = true;
            }

            _ConnectClosestAreas(room, lvlIdx, areas, connectFilter);
        }
        #endregion

        #region Private
        static void _CreatePassage(IRoomInstance room, int lvlIdx, LevelArea areaA, LevelArea areaB, Vector2Int tileA,
            Vector2Int tileB, TilesSetFilter drawFilter)
        {
            LevelArea.ConnectLevelAreas(areaA, areaB);

            var line = GetLine(tileA, tileB);
            foreach (var c in line)
                _DrawCircle(room, lvlIdx, c, 1, drawFilter);
        }
        
        static void _DrawCircle(IRoomInstance room, 
            int lvlIdx, Vector2Int c, int r, 
            TilesSetFilter drawFilter)
        {
            var lvl = room.GetPlatformLayer(lvlIdx);

            for (var x = -r; x <= r; ++x)
            {
                for (var z = -r; z <= r; ++z)
                {
                    if (x * x + z * z > r * r)
                        continue;

                    var tileX = c.x + x;
                    var tileZ = c.y + z;
                    if (!lvl.IsInside(new Vector2Int(tileX, tileZ))) 
                        continue;
                    var rx = lvl.Position.x + tileX;
                    var rz = lvl.Position.y + tileZ;
                    SetTile(lvl, lvl, new Vector2Int(rx, rz), drawFilter);
                    //lvl.SetTile(tileX, tileZ, drawTile);
                }
            }
        }

        static int GetSurroundingWallCount(this IRoomInstance room, IPlatformLayer lvl, int gridX, int gridZ)
        {
            var floorFilter = room.RoomSettings.GetFilter(GeometryType.Floor);

            //var lvl = room.Levels[levelIdx];
            var wallCount = 0;
            for (var neighborX = gridX - 1; neighborX <= gridX + 1; ++neighborX)
            for (var neighborZ = gridZ - 1; neighborZ <= gridZ + 1; ++neighborZ)
            {
                if (gridX == neighborX && gridZ == neighborZ)
                    continue;
                var nPos = new Vector2Int(neighborX, neighborZ);
                if (lvl.IsInside(nPos))
                {
                    var tile = GetTile(lvl, lvl, nPos);
                    if (!floorFilter.IsTileActive(tile))
                        wallCount++;
                }
                else
                    wallCount++;
            }

            return wallCount;
        }

        static IEnumerable<List<Vector2Int>> GetRegions(IPlatformLayer lvl, TilesSetFilter tileType)
        {
            var regions = new List<List<Vector2Int>>();
            // mark tiles that have already been processed
            var mapFlags = new int[lvl.TileDim.x, lvl.TileDim.y];

            for (var x = 0; x < lvl.TileDim.x; ++x)
                for (var z = 0; z < lvl.TileDim.y; ++z)
                {
                    if (mapFlags[x, z] != 0)
                        continue;

                    var tile = GetTile(lvl, lvl, new Vector2Int(x, z));
                    if (!tileType.IsTileActive(tile))
                        continue;

                    var newRegion = GetRegionTiles(lvl, x, z, tileType.Data.Mask);
                    regions.Add(newRegion);

                    foreach (var regTile in newRegion) 
                        mapFlags[regTile.x, regTile.y] = 1;
                }

            return regions;
        }

        static List<Vector2Int> GetRegionTiles(IPlatformLayer lvl, int startX, int startZ, int compareMask)
        {
            var tiles = new List<Vector2Int>();
            // mark tiles that have already been processed
            var mapFlags = new int[lvl.TileDim.x, lvl.TileDim.y];

            var tileType = GetTile(lvl, lvl, new Vector2Int(startX, startZ)) & compareMask;
            //lvl.Tiles[startZ * room.Size.x + startX];

            var queue = new Queue<Vector2Int>();
            queue.Enqueue(new Vector2Int(startX, startZ));
            mapFlags[startX, startZ] = 1;

            // spread-algorithm, select all adjacent tiles with same tileType
            while (queue.Count > 0)
            {
                var tile = queue.Dequeue();
                tiles.Add(tile);

                for (var x = tile.x - 1; x <= tile.x + 1; ++x)
                    for (var z = tile.y - 1; z <= tile.y + 1; ++z)
                    {
                        var adjacent = (z == tile.y || x == tile.x);
                        if (!adjacent)
                            continue;
                        if (z == tile.y && x == tile.x)
                            continue;
                        var pos = new Vector2Int(x, z);
                        if (!lvl.IsInside(pos))
                            continue;
                        if (mapFlags[x, z] != 0
                            || (GetTile(lvl, lvl, pos) & compareMask) != tileType) 
                            continue;

                        mapFlags[x, z] = 1;
                        queue.Enqueue(new Vector2Int(x, z));
                    }
            }

            return tiles;
        }

        static void _ConnectClosestAreas(IRoomInstance room, int lvlIdx, List<LevelArea> allAreas, TilesSetFilter drawFilter,
            bool forceAccessibilityFromMainArea = false)
        {
            var areaListA = forceAccessibilityFromMainArea ? new List<LevelArea>() : allAreas;
            var areaListB = forceAccessibilityFromMainArea ? new List<LevelArea>() : allAreas;
            if (forceAccessibilityFromMainArea)
            {
                foreach (var area in allAreas)
                    if (area.IsAccessibleFromMainLevelArea)
                        areaListB.Add(area);
                    else 
                        areaListA.Add(area);
            }

            var bestDistance = int.MaxValue;
            var bestTileA = new Vector2Int();
            var bestTileB = new Vector2Int();
            var bestAreaA = new LevelArea();
            var bestAreaB = new LevelArea();
            var possibleConnectionFound = false;

            foreach (var areaA in areaListA)
            {
                if (!forceAccessibilityFromMainArea)
                {
                    bestDistance = int.MaxValue;
                    possibleConnectionFound = false;
                }

                foreach (var areaB in areaListB)
                {
                    if (areaA == areaB)
                        continue;
                    foreach (var aTile in areaA.EdgeTiles)
                    foreach (var bTile in areaB.EdgeTiles)
                    {
                        var tileA = aTile;
                        var tileB = bTile;
                        var distanceBetweenAreas =
                            (int)(Mathf.Pow(tileA.x - tileB.x, 2) + Mathf.Pow(tileA.y - tileB.y, 2));

                        if (distanceBetweenAreas >= bestDistance)
                            continue;
                        bestDistance = distanceBetweenAreas;
                        possibleConnectionFound = true;
                        bestTileA = tileA;
                        bestTileB = tileB;
                        bestAreaA = areaA;
                        bestAreaB = areaB;
                    }
                }

                if (!possibleConnectionFound || forceAccessibilityFromMainArea) 
                    continue;
                if (bestAreaA.IsConnected(bestAreaB))
                    continue;

                _CreatePassage(room, lvlIdx, bestAreaA, bestAreaB, bestTileA, bestTileB, drawFilter);
            }

            if (possibleConnectionFound && forceAccessibilityFromMainArea)
            {
                _CreatePassage(room, lvlIdx, bestAreaA, bestAreaB, bestTileA, bestTileB, drawFilter);
                _ConnectClosestAreas(room, lvlIdx, allAreas, drawFilter, true);
            }

            if (!forceAccessibilityFromMainArea)
                _ConnectClosestAreas(room, lvlIdx, allAreas, drawFilter, true);
        }

        static IEnumerable<Vector2Int> GetLine(Vector2Int from, Vector2Int to)
        {
            var line = new List<Vector2Int>();

            var x = from.x;
            var z = from.y;

            var dx = to.x - from.x;
            var dy = to.y - from.y;

            var step = System.Math.Sign(dx);
            var gradientStep = System.Math.Sign(dy);

            var longest = Mathf.Abs(dx);
            var shortest = Mathf.Abs(dy);

            var inverted = (longest < shortest);
            if (inverted)
            {
                step = System.Math.Sign(dy);
                gradientStep = System.Math.Sign(dx);

                longest = Mathf.Abs(dy);
                shortest = Mathf.Abs(dx);
            }

            var gradientAccumulation = longest / 2;
            for (var i = 0; i < longest; i++)
            {
                line.Add(new Vector2Int(x, z));
                if (inverted) z += step;
                else x += step;

                gradientAccumulation += shortest;
                if (gradientAccumulation < longest) 
                    continue;
                if (inverted) x += gradientStep;
                else z += gradientStep;
                gradientAccumulation -= longest;
            }

            return line;
        }
        #endregion
    }
}
