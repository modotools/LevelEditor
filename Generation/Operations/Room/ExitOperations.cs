﻿using System.Collections.Generic;
using System.Linq;
using Core.Types;
using Core.Unity.Extensions;
using Core.Extensions;
using Generation.Data;
using Level.Enums;
using Level.PlatformLayer.Interface;
using Level.Room;
using UnityEngine;
using static Level.PlatformLayer.Operations;
using static Level.BoundsOperations;

namespace Level.Generation.Operations
{
    public static partial class WorkerOperations
    {
        #region Public
        public static void UpdateExits(this IRoomInstance roomInstance)
        {
            AddOpenExitsFromTiles(roomInstance);

            UpdateExitConfiguration(roomInstance);
        }

        static readonly bool[] k_mandatoryExits = new bool[4];
        static readonly bool[] k_optionalExits = new bool[4];
        static readonly bool[] k_possibleEntries = new bool[4];
        static readonly bool[] k_cardinalMapping = new bool[4];

        public static void UpdateExitConfiguration(this IRoomInstance roomInstance)
        {
            var conf = ExitConfiguration.Invalid;

            _UpdateExitInfo_InitArrays(roomInstance);

            roomInstance.ValidExitConfiguration?.Clear();
            roomInstance.ValidCardinalMap?.Clear();
            // offset to rotate room so entry is always considered from south
            var offset = 2;
            for (var i = 0; i < 4; i++, offset--)
            {
                if (!k_possibleEntries[i])
                    continue;
                // entry is always south
                // var entry = (Cardinals) ((i + 4 + offset) % 4);
                _UpdateExitInfo_MandatoryExitsToBools(offset, out var north, out var east, out var west);
                // Debug.Log($"i {i} {(Cardinals) i} mapped to entry {entry} {north} {east} {west}");
                using (var exitConfScoped = SimplePool<List<ExitConfiguration>>.I.GetScoped())
                using (var optionalExitsScoped = SimplePool<List<Cardinals>>.I.GetScoped())
                {
                    _UpdateExitInfo_OptionalExitsToList(offset, optionalExitsScoped);
                    _UpdateExitInfo_AddExitConfiguration(exitConfScoped.Obj, west, north, east, optionalExitsScoped.Obj);
                    _UpdateExitInfo_AddValidConfiguration(roomInstance, exitConfScoped);
                    _UpdateExitInfo_AddCardinalMaps(roomInstance, exitConfScoped, offset);
                }
            }

            roomInstance.ValidExitConfiguration?.Sort();
        }

        static void _UpdateExitInfo_AddCardinalMaps(IRoomInstance roomInstance, SimplePool<List<ExitConfiguration>>.PooledObject exitConfScoped, int offset)
        {
            foreach (var exitConf in exitConfScoped.Obj)
            {
                var exitConfInt = (int) exitConf;
                var east = (exitConfInt & 1) == 1;
                var north = (exitConfInt & 2) == 2;
                var west = (exitConfInt & 4) == 4;
                _UpdateExitInfo_AddMapping(roomInstance, -offset, false, false, north, east, west);
                var rotate = roomInstance.AllowRotate;
                var mirror = roomInstance.AllowMirror;

                if (mirror)
                    _UpdateExitInfo_AddMirrorCases(roomInstance, -offset, north, east, west);
                if (rotate)
                    _UpdateExitInfo_AddRotateCases(roomInstance, mirror, -offset, north, east, west);
            }
        }

        static void _UpdateExitInfo_AddValidConfiguration(IRoomInstance roomInstance, SimplePool<List<ExitConfiguration>>.PooledObject exitConfScoped)
        {
            foreach (var exitConf in exitConfScoped.Obj)
            {
                if (roomInstance.ValidExitConfiguration.Contains(exitConf))
                    continue;
                roomInstance.ValidExitConfiguration.Add(exitConf);
            }
        }

        static void _UpdateExitInfo_OptionalExitsToList(int offset, SimplePool<List<Cardinals>>.PooledObject optionalExitsScoped)
        {
            for (var j = 0; j < 4; j++)
            {
                if (!k_optionalExits[j])
                    continue;
                var optionalExit = (Cardinals) ((j + 4 + offset) % 4);
                if (optionalExit == Cardinals.South)
                    continue;
                // Debug.Log($"optionalExit j {j} {(Cardinals) j} mapped to exit {optionalExit}");

                optionalExitsScoped.Obj.Add(optionalExit);
            }
        }

        static void _UpdateExitInfo_MandatoryExitsToBools(int offset, out bool north, out bool east, out bool west)
        {
            north = false;
            east = false;
            west = false;
            
            for (var j = 0; j < 4; j++)
            {
                if (!k_mandatoryExits[j])
                    continue;
                var mandatoryExit = (Cardinals) ((j + 4 + offset) % 4);

                switch (mandatoryExit)
                {
                    case Cardinals.North:
                        north = true;
                        break;
                    case Cardinals.East:
                        east = true;
                        break;
                    case Cardinals.South: continue;
                    case Cardinals.West:
                        west = true;
                        break;
                }
            }
        }

        static void _UpdateExitInfo_InitArrays(IRoomInstance roomInstance)
        {
            var initEntry = roomInstance.RestrictionModeEntry == RestrictionMode.AllowFirst;
            var initExits = roomInstance.RestrictionModeExit == RestrictionMode.AllowFirst;

            // Debug.Log($"init entry {initEntry} exits {initExits}");
            for (var i = 0; i < 4; i++)
            {
                k_optionalExits[i] = initExits;
                k_possibleEntries[i] = initEntry;
            }

            foreach (var ex in roomInstance.OpenExits)
            {
                var blocked = ex.Restriction != null && ex.Restriction.Type == RestrictionType.AlwaysBlocked;

                var dirIdx = (int) ex.ExitDirection;
                k_mandatoryExits[dirIdx] = !blocked;

                if (ex.PossibleEntry)
                    k_possibleEntries[dirIdx] = !blocked;

                if (!blocked)
                    continue;
                k_optionalExits[dirIdx] = false;
                k_possibleEntries[dirIdx] = false;
            }

            foreach (var exRes in roomInstance.ExitRestrictions)
            {
                if (exRes.Restriction == null)
                    continue;
                var restrictionType = exRes.Restriction.Type;
                var blocked = restrictionType == RestrictionType.AlwaysBlocked;
                var unblock = !blocked;
                var dirIdx = (int) exRes.ExitDirection;
                // Debug.Log($"exRes {exRes.ExitDirection} {dirIdx} unblock {unblock}");

                if (unblock)
                {
                    k_optionalExits[dirIdx] = true;
                    if (exRes.PossibleEntry)
                        k_possibleEntries[dirIdx] = true;
                }

                if (!blocked)
                    continue;
                k_mandatoryExits[dirIdx] = false;
                k_optionalExits[dirIdx] = false;
                k_possibleEntries[dirIdx] = false;
            }
        }

        static void _UpdateExitInfo_AddRotateCases(IRoomInstance roomInstance, bool mirror, int offset, bool north, bool east, bool west)
        {
            for (var i = 1; i < 4; i++)
            {
                _UpdateExitInfo_AddMapping(roomInstance, offset + i, false, false, north, east, west);
                if (mirror) 
                    _UpdateExitInfo_AddMirrorCases(roomInstance, offset + i, north, east, west);
            }
        }

        static void _UpdateExitInfo_AddMirrorCases(IRoomInstance roomInstance, int offset, bool north, bool east, bool west)
        {
            _UpdateExitInfo_AddMapping(roomInstance, offset, true, false, north, east, west);
            _UpdateExitInfo_AddMapping(roomInstance, offset, false, true, north, east, west);
            _UpdateExitInfo_AddMapping(roomInstance, offset, true, true, north, east, west);
        }

        static void _UpdateExitInfo_AddMapping(IRoomInstance roomInstance, int offset, 
            bool mirrorH, bool mirrorV,
            bool north, bool east, bool west)
        {
            const bool south = true;
            var h = mirrorH ? 2 : 0;
            var v = mirrorV ? 2 : 0;
            k_cardinalMapping[((int)Cardinals.East + 4 + offset + h) % 4] = east;
            k_cardinalMapping[((int)Cardinals.North + 4 + offset + v) % 4] = north;
            k_cardinalMapping[((int)Cardinals.West + 4 + offset + h) % 4] = west;
            k_cardinalMapping[((int)Cardinals.South + 4 + offset + v) % 4] = south;
            var cardinalMappingInt = 0;
            if (k_cardinalMapping[0])
                cardinalMappingInt |= 1;
            if (k_cardinalMapping[1])
                cardinalMappingInt |= 2;
            if (k_cardinalMapping[2])
                cardinalMappingInt |= 4;
            if (k_cardinalMapping[3])
                cardinalMappingInt |= 8;

            if (!roomInstance.ValidCardinalMap.Contains(cardinalMappingInt))
                roomInstance.ValidCardinalMap.Add(cardinalMappingInt);
        }

        static void _UpdateExitInfo_AddExitConfiguration(ICollection<ExitConfiguration> exitConfigurations, bool west, bool north, bool east, IEnumerable<Cardinals> optional)
        {
            var conf = 0;
            if (west) 
                conf |= 4;
            if (north)  
                conf |= 2;
            if (east) 
                conf |= 1;
            var exitConfiguration = (ExitConfiguration) conf;

            if (exitConfigurations.Contains(exitConfiguration))
                return;
            exitConfigurations.Add(exitConfiguration);
            // Debug.Log($"Add exitConfiguration {exitConfiguration}");

            foreach (var o in optional)
            {
                // Debug.Log($"optional {o}");
                switch (o)
                {
                    case Cardinals.North:
                        if (north) 
                            continue;
                        _UpdateExitInfo_AddExitConfiguration(exitConfigurations, west, true, east, optional);
                        break;
                    case Cardinals.East: 
                        if (east) 
                            continue;
                        _UpdateExitInfo_AddExitConfiguration(exitConfigurations, west, north, true, optional);
                        break;
                    case Cardinals.South: continue;
                    case Cardinals.West: 
                        if (west) 
                            continue;
                        _UpdateExitInfo_AddExitConfiguration(exitConfigurations, true, north, east, optional);
                        break;
                }
            }
        }

        public static void AddOpenExitsFromTiles(this IRoomInstance room)
        {
            using (var prevExits = SimplePool<List<ExitInfo>>.I.GetScoped())
            {
                prevExits.Obj.AddRange(room.OpenExits);
                room.OpenExits.Clear();

                for (var levelI = 0; levelI < room.PlatformCount; ++levelI)
                {
                    var level = room.GetPlatformLayer(levelI);
                    // Add open exits Where level is on the edge of the room
                    if (level.Position.x == 0)
                        _AddOpenExitsFromTiles(room, levelI, level, Cardinals.West);

                    if (level.Position.y == 0)
                        _AddOpenExitsFromTiles(room, levelI, level, Cardinals.South);

                    if (level.Extends.x == room.Size.x)
                        _AddOpenExitsFromTiles(room, levelI, level, Cardinals.East);

                    if (level.Extends.y == room.Size.z)
                        _AddOpenExitsFromTiles(room, levelI, level, Cardinals.North);
                }

                CopyPreviousData(room, prevExits);
            }

        }

        static void CopyPreviousData(IRoomInstance room, SimplePool<List<ExitInfo>>.PooledObject prevExits)
        {
            foreach (var openExit in room.OpenExits)
            {
                var prev = prevExits.Obj.FirstOrDefault(p => p.ExitDirection == openExit.ExitDirection &&
                                                             p.Level == openExit.Level
                                                             && (p.FromIdx <= openExit.FromIdx && p.ToIdx >= openExit.ToIdx
                                                                 || p.FromIdx >= openExit.FromIdx &&
                                                                 p.ToIdx <= openExit.ToIdx));
                if (prev == null)
                    continue;

                // Debug.Log("prev.restr" +prev.Restriction);
                openExit.Restriction = prev.Restriction;
                openExit.PossibleEntry = prev.PossibleEntry;
            }
        }

        public static void OpenExitsFromTiles(this IRoomInstance room, List<ExitInfo> exitInfos)
        {
            room.OpenExits.Clear();
            for (var levelI = 0; levelI < room.PlatformCount; ++levelI)
            {
                var level = room.GetPlatformLayer(levelI);
                // Add open exits Where level is on the edge of the room
                if (level.Position.x == 0)
                    _OpenExitsFromTiles(room, levelI, level, Cardinals.West, exitInfos);

                if (level.Position.y == 0)
                    _OpenExitsFromTiles(room, levelI, level, Cardinals.South, exitInfos);

                if (level.Extends.x == room.Size.x)
                    _OpenExitsFromTiles(room, levelI, level, Cardinals.East, exitInfos);

                if (level.Extends.y == room.Size.z)
                    _OpenExitsFromTiles(room, levelI, level, Cardinals.North, exitInfos);
            }
        }

        public static void FilterExitsAndCloseNeighborExits(IRoomInstance myRoom, List<ExitInfo> exitInfos, NeighborData[] neighborData, 
            IOpenEndTracker openEndTracker = null)
        {
            for (var i = exitInfos.Count-1; i >= 0; --i)
            {
                var exit = exitInfos[i];
                var exitBounds = GetBoundsFollowingExit(myRoom.Bounds, exit);
                if (!TryGetNeighborForExit(neighborData, exitBounds, out var neighbor))
                    continue;

                exit.ConnectedTo = neighbor;
                myRoom.ClosedExits.Add(exit);
                exitInfos.RemoveAt(i);

                CloseExitsConnecting(neighbor, myRoom, openEndTracker);
            }
        }

        static bool TryGetNeighborForExit(IEnumerable<NeighborData> neighborData, Bounds exitBounds, out IRoomInstance neighbor)
        {
            neighbor = null;
            var nd = neighborData.FirstOrDefault(n => n.BlockedSpace
                                                        && n.Bounds.GetIntersectionType(exitBounds) ==
                                                        IntersectionType.Contains);
            if (nd.Rooms.IsNullOrEmpty())
                return false;
            neighbor = nd.Rooms.FirstOrDefault(r =>
                r.Bounds.GetIntersectionType(exitBounds) == IntersectionType.Contains);
            return neighbor != null;
        }

        static void CloseExitsConnecting(IRoomInstance roomClosingExits, IRoomInstance connectingToRoom, 
            IOpenEndTracker openEndTracker)
        {
            for (var j = roomClosingExits.OpenExits.Count - 1; j >= 0; --j)
            {
                var closingExit = roomClosingExits.OpenExits[j];
                CloseExit(roomClosingExits, connectingToRoom, openEndTracker, closingExit, j);
            }
        }

        public static void CloseExit(IRoomInstance roomClosingExits, IRoomInstance connectingToRoom, IOpenEndTracker openEndTracker,
            ExitInfo closingExit, int closeExitIdx)
        {
            var closingExitBounds = GetBoundsFollowingExit(roomClosingExits.Bounds, closingExit);
            var contains = connectingToRoom.Bounds.GetIntersectionType(closingExitBounds) == IntersectionType.Contains;
            if (!contains)
                return;

            closingExit.ConnectedTo = connectingToRoom;

            roomClosingExits.ClosedExits.Add(closingExit);
            roomClosingExits.OpenExits.RemoveAt(closeExitIdx);

            openEndTracker?.CloseExit(closingExit);
        }

        /// <summary>
        /// Closes exits that apply to given arguments
        /// </summary>
        /// <param name="connector">the room that will connect to this exit</param>
        /// <param name="room">the room where exits should be closed</param>
        /// <param name="exitDirection">the direction of exits to close</param>
        /// <param name="bounds">the bounds the exit should be in to close</param>
        /// <param name="exits">list of bounds of exits that will be closed</param>
        /// <param name="openEndTracker">tracks open exits</param>
        /// <returns>how many exits have been closed</returns>
        public static void ConnectOpenEndsInsideBounds(IRoomInstance connector, IRoomInstance room, Cardinals exitDirection, Bounds bounds,
            ref List<Bounds> exits, 
            IOpenEndTracker openEndTracker)
        {
            for (var i = room.OpenExits.Count - 1; i >= 0; i--)
            {
                var exit = room.OpenExits[i];
                if (exit.ExitDirection != exitDirection)
                    continue;

                var exitBounds = GetBoundsFollowingExit(room.Bounds, exit);
                if (bounds.GetIntersectionType(exitBounds) != IntersectionType.Contains)
                    continue;

                exits.Add(exitBounds);
                room.OpenExits.Remove(exit);
                exit.ConnectedTo = connector;
                room.ClosedExits.Add(exit);
                openEndTracker.CloseExit(exit);
            }
        }


        // todo: maybe easier to convert from ExitInfo to ExitInfo or have ExitInfo as Bounds already, then ExitInfo => Tiles, Tiles=> ExitInfo
        public static void ExitsFromBounds(this IRoomInstance room,
            Bounds exitBounds, List<ExitInfo> exits)
        {
            var minLvl = Mathf.Max(0, Mathf.FloorToInt(exitBounds.min.y - room.Position.y));
            var maxLvl = Mathf.Min(Mathf.CeilToInt(exitBounds.max.y - room.Position.y), room.PlatformCount);
            for (var levelI = minLvl; levelI < maxLvl; ++levelI)
            {
                var level = room.GetPlatformLayer(levelI);
                // Add open exits Where level is on the edge of the room
                if (level.Position.x == 0)
                    _ExitsFromBounds(room, levelI, level, exitBounds, Cardinals.West, exits);
                if (level.Position.y == 0)
                    _ExitsFromBounds(room, levelI, level, exitBounds, Cardinals.South, exits);
                if (level.Extends.x == room.Size.x)
                    _ExitsFromBounds(room, levelI, level, exitBounds, Cardinals.East, exits);
                if (level.Extends.y == room.Size.z)
                    _ExitsFromBounds(room, levelI, level, exitBounds, Cardinals.North, exits);
            }
        }
        #endregion

        #region Private
        /// <summary>
        /// loops through the edge of the room-level only to add Floor-Tiles as exits
        /// </summary>
        static void _AddOpenExitsFromTiles(IRoomInstance room,
            int levelIdx, IPlatformLayer level,
            Cardinals cardinals) 
            => _OpenExitsFromTiles(room, levelIdx, level, cardinals, room.OpenExits);

        static void _OpenExitsFromTiles(IRoomInstance room,
            int levelIdx, IPlatformLayer level,
            Cardinals cardinals, List<ExitInfo> exits)
        {
            _AddOpenExitsFromTiles_GetParameter(room, level, cardinals, exits,
                out var o);

            for (var idx = 0; idx < o.LoopSize; ++idx)
            {
                var roomIdx = o.AddToIdx + idx;

                var cornerTile = roomIdx % o.Grid.RoomChunkSize == 0;
                var endOfRoom = roomIdx == (o.RoomSize - 1);

                if (_GetExitTileActive(level, idx, o))
                {
                    var justCreated = o.Info == null;
                    o.Info ??= new ExitInfo
                    {
                        FromIdx = roomIdx,
                        ExitDirection = cardinals, Level = levelIdx
                    };

                    o.Info.ToIdx = roomIdx;
                    // push exit when reaching corner, except when we just started from new corner
                    if (!cornerTile || justCreated)
                        continue;

                    _PushExit(ref o);

                    // if next tile is also floor, we create another ExitInfo from same corner (intersection)
                    if (!endOfRoom && _GetExitTileActive(level, idx+1, o))
                        o.Info = new ExitInfo
                        {
                            FromIdx = roomIdx,
                            ExitDirection = cardinals, Level = levelIdx
                        };
                }
                // if the wall is interrupted the exit is pushed so a new exit can be found on that side
                else _PushExit(ref o);
            }

            // not to leave any exits forgotten
            _PushExit(ref o);
        }

        static void _AddOpenExitsFromTiles_GetParameter(IRoomInstance room, IPlatformLayer level, 
            Cardinals cardinals,
            List<ExitInfo> exits,
            out ExitsOperationData exitOpData)
        {
            exitOpData.TargetList = exits;
            exitOpData.Grid = room.RoomSettings.Grid;
            exitOpData.FloorFilter = room.RoomSettings.GetFilter(GeometryType.Floor);
            exitOpData.Info = null;

            exitOpData.LoopData = default;
            exitOpData.RoomSize = 0;
            exitOpData.AddToIdx = 0;

            exitOpData.LoopData.LoopSize = -1;
            exitOpData.LoopData.MulXIdx = 0;
            exitOpData.LoopData.MulZIdx = 0;
            exitOpData.LoopData.AddXIdx = 0;
            exitOpData.LoopData.AddZIdx = 0;

            if (cardinals == Cardinals.North || cardinals == Cardinals.South)
            {
                exitOpData.LoopData.LoopSize = level.TileDim.x;
                exitOpData.RoomSize = room.TileDim.x;
                exitOpData.AddToIdx = level.Position.x;

                exitOpData.LoopData.MulXIdx = 1;
                if (cardinals == Cardinals.North)
                    exitOpData.LoopData.AddZIdx = level.TileDim.y - 1;
            }
            else
            {
                exitOpData.LoopData.LoopSize = level.TileDim.y;
                exitOpData.RoomSize = room.TileDim.y;
                exitOpData.AddToIdx = level.Position.y;

                exitOpData.LoopData.MulZIdx = 1;
                if (cardinals == Cardinals.East)
                    exitOpData.LoopData.AddXIdx = level.TileDim.x - 1;
            }
        }

        static void _PushExit(ref ExitsOperationData o)
        {
            if (o.Info == null)
                return;
            o.TargetList.Add(o.Info);
            o.Info = null;
        }

        static bool _GetExitTileActive(IPlatformLayer level, int idx, ExitsOperationData o)
        {
            _GetExitPosition(idx, o.MulXIdx, o.AddXIdx, o.MulZIdx, o.AddZIdx, out var pos);
            // todo: resulting?
            var tile = GetTile(level, level, pos);
            return o.FloorFilter.IsTileActive(tile);
        }
        static bool _GetExitTileActive(Bounds exitBounds, int idx, ExitsOperationData o)
        {
            _GetExitPosition(idx, o.MulXIdx, o.AddXIdx, o.MulZIdx, o.AddZIdx, out var pos);
            return exitBounds.Contains(new Vector3(pos.x, exitBounds.center.y, pos.y));
        }

        static void _GetExitPosition(int idx, int mulXIdx, int addXIdx, int mulZIdx, int addZIdx, out Vector2Int pos)
        {
            // the x and z positions in the array, this loop will only iterate through one of them (mul_Idx is 0 or 1) and set to the corresponding side via add_Idx
            var xComp = (mulXIdx * idx) + addXIdx;
            var zComp = (mulZIdx * idx) + addZIdx;
            pos = new Vector2Int(xComp, zComp);
        }


        static void _ExitsFromBounds(IRoomInstance room,
            int levelIdx, IPlatformLayer level, Bounds exitBounds,
            Cardinals cardinals, List<ExitInfo> exits)
        {
            _AddOpenExitsFromTiles_GetParameter(room, level, cardinals, exits,
                out var o);
            for (var idx = 0; idx < o.LoopSize; ++idx)
            {
                var roomIdx = o.AddToIdx + idx;
                var cornerTile = roomIdx % o.Grid.RoomChunkSize == 0;
                var endOfRoom = roomIdx == (o.RoomSize - 1);
                if (_GetExitTileActive(exitBounds, idx, o))
                {
                    var justCreated = o.Info == null;
                    o.Info ??= new ExitInfo
                    {
                        FromIdx = roomIdx,
                        ExitDirection = cardinals,
                        Level = levelIdx
                    };
                    o.Info.ToIdx = roomIdx;
                    // push exit when reaching corner, except when we just started from new corner
                    if (!cornerTile || justCreated)
                        continue;
                    _PushExit(ref o);
                    // if next tile is also floor, we create another ExitInfo from same corner (intersection)
                    if (!endOfRoom && _GetExitTileActive(exitBounds, idx + 1, o))
                        o.Info = new ExitInfo
                        {
                            FromIdx = roomIdx,
                            ExitDirection = cardinals,
                            Level = levelIdx
                        };
                }
                // if the wall is interrupted the exit is pushed so a new exit can be found on that side
                else _PushExit(ref o);
            }
            // not to leave any exits forgotten
            _PushExit(ref o);
        }
        #endregion
    }
}
