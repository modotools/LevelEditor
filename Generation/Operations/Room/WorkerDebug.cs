
using Core.Types;
using Level.Room.Operations;
using UnityEngine;

namespace Level.Generation.Operations
{
    public static partial class WorkerOperations
    {
        public static void ResetOperationDebugInfo() => DebugInfo.Result = OperationResult.OK;
        public static BoundOperationDebugInfo DebugInfo = BoundOperationDebugInfo.Default;

        static bool ReportErrorHelper(string errorString, Bounds err, Bounds op)
        {
            Debug.LogError(errorString);
            DebugInfo.ErrorBounds.Add(err);
            DebugInfo.OperationBounds.Add(op);
            return false;
        }
    }
}