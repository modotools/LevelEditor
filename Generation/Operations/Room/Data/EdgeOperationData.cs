﻿using System.Collections.Generic;
using Level.Room;
using Level.Tiles;

namespace Level.Generation.Operations
{
    public static partial class WorkerOperations
    {
        struct ExitsOperationData
        {
            public GridSettings Grid;
            public TilesSetFilter FloorFilter;

            public List<ExitInfo> TargetList;
            public ExitInfo Info;

            public int RoomSize;
            public int AddToIdx;

            public EdgeTilesLoopData LoopData;

            public int LoopSize => LoopData.LoopSize;
            public int MulXIdx => LoopData.MulXIdx;
            public int MulZIdx => LoopData.MulZIdx;
            public int AddXIdx => LoopData.AddXIdx;
            public int AddZIdx => LoopData.AddZIdx;
        }

        public struct EdgeTilesLoopData
        {
            public int LoopSize;
            public int MulXIdx;
            public int MulZIdx;
            public int AddXIdx;
            public int AddZIdx;
        }
    }
}
