using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Types;
using Core.Unity.Extensions;
using Generation.Data;
using Level.PlatformLayer.Interface;
using Level.Room;
using Level.Tiles;
using Level.Enums;
using UnityEngine;

using static Level.PlatformLayer.Operations;

namespace Level.Generation.Operations
{
    public static partial class WorkerOperations
    {
        public struct CornerMunchingData
        {
            public IOrderedEnumerable<int> Corners;
            public float MunchChance;
            public AnimationCurve SuccessiveMunchChanceMultiplier;
            public System.Random Random;

            public static CornerMunchingData Default => new CornerMunchingData()
            {
                Corners = new List<int>() {0, 1, 2, 3}.OrderBy(a=>a),
                MunchChance = 0.5f,
                SuccessiveMunchChanceMultiplier = AnimationCurve.Constant(0, 1, 1),
                Random = new System.Random()
            };
        }
    }
}