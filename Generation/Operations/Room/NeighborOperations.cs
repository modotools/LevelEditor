using System.Collections.Generic;
using System.Linq;
using Core;
using Core.Extensions;
using Core.Types;
using Core.Unity.Types;
using Generation.Data;
using Level.Room;
using Level.Generation.Data;
using Level.PlatformLayer.Interface;
using UnityEngine;

using static Level.PlatformLayer.Operations;
using static Level.BoundsOperations;
using LogType = Core.LogType;

namespace Level.Generation.Operations
{
    public static partial class WorkerOperations
    {
        public static void GatherNeighbors(IRoomInstance room, Octree<IRoomInstance> octree,
            NeighborData[] neighborData)
        {
            for (var i = 0; i < 4; ++i)
                GatherNeighbors(room, octree, (Cardinals) i, neighborData);
        }
        
        public static void GatherNeighbors(IRoomInstance room, Octree<IRoomInstance> octree, 
            Cardinals direction, NeighborData[] neighborData)
        {
            BoundsOperations.GetBoundsNextTo(room.Bounds, direction, 0, room.RoomSettings.Grid.RoomChunkSize3D,
                out var neighborBounds);

            // ReSharper disable once ConvertToUsingDeclaration
            using (var roomListScope = SimplePool<List<IRoomInstance>>.I.GetScoped())
            {
                var tempRooms = roomListScope.Obj;
                octree.IsFreePlaceOrGatherIntersecting(neighborBounds, ref tempRooms);
                neighborData[(int)direction] = new NeighborData(neighborBounds, tempRooms);
            }

            var boundTagID = neighborData[(int) direction].BlockedSpace
                ? BoundTags.NeighborCheckBoundsBlocked
                : BoundTags.NeighborCheckBoundsFree;
            LevelManager.DEBUG_AddDrawBounds(boundTagID, neighborBounds);
        }

        /// <summary>
        /// Gathers all connecting rooms that are max given steps (in rooms) away.
        /// </summary>
        /// <param name="room">The room which should be connected</param>
        /// <param name="yPos">the y-pos of the hero</param>
        /// <param name="steps">steps until it is considered too far or too indirectly connected</param>
        /// <param name="connecting">all the connecting rooms</param>
        public static void GatherConnecting(this IRoomInstance room, int yPos, int steps, Dictionary<IRoomInstance, int> connecting)
        {
            //Debug.Log($"{room} steps {steps}");

            connecting.Add(room, steps);
            
            using (var currentRoomsScoped = SimplePool<List<IRoomInstance>>.I.GetScoped())
            using (var nextRoomsScoped = SimplePool<List<IRoomInstance>>.I.GetScoped())
            {
                var currentRooms = currentRoomsScoped.Obj;
                var nextRooms = nextRoomsScoped.Obj;

                _GatherConnecting_AddNextRooms(room, yPos, steps-1, connecting, nextRooms);

                while (steps > 0 && !nextRooms.IsNullOrEmpty())
                {
                    steps--;
                    currentRooms.Clear();
                    currentRooms.AddRange(nextRooms);
                    nextRooms.Clear();

                    foreach (var r in currentRooms) 
                        _GatherConnecting_AddNextRooms(r, yPos, steps-1, connecting, nextRooms);
                }
            }

        }

        static void _GatherConnecting_AddNextRooms(IRoomInstance room, int yPos, int steps, 
            IDictionary<IRoomInstance, int> connecting, 
            ICollection<IRoomInstance> nextRooms)
        {
            foreach (var e in room.ClosedExits)
            {
                e.ConnectedTo.Get(out var connectedToBounds);
                if (connectedToBounds.max.y <= (yPos + 1) || connectedToBounds.min.y > yPos)
                {
                    //Debug.Log($"ignoring {e} because y stuff");
                    continue;
                }

                if (connecting.ContainsKey(e.ConnectedTo))
                {
                    //Debug.Log($"ignoring {e} because ConnectedTo {e.ConnectedTo} is contained");
                    continue;
                }

                connecting.Add(e.ConnectedTo, steps);
                nextRooms.Add(e.ConnectedTo);
            }
        }
        
        public static void CopyAllEdgeTiles(IRoomInstance myRoom, NeighborData[] neighborData)
        {
            for (var i = 0; i < 4; ++i)
            {
                var neighborDir = (Cardinals) i;
                // var exitDir = (Cardinals) ((i + 2) % 4);
        
                var neighbor = neighborData[i];
                if (neighbor.Rooms == null)
                    continue;
                foreach (var r in neighbor.Rooms)
                    CopyEdgeTiles(myRoom, r, neighborDir);
            }
        }
        
        public static void CopyEdgeTiles(IRoomInstance myRoom, IRoomInstance otherRoom, Cardinals neighborDir)
        {
            var yOff = myRoom.Position.y - otherRoom.Position.y;
            //Debug.Log($"neighborDir {neighborDir} exitDir {exitDir} yOff {yOff}");

            DebugString debugString = default;
            debugString.AddLog($"CopyEdgeTiles {myRoom.name} from {otherRoom.name} Cardinals {neighborDir}");
            for (var i = 0; i < myRoom.PlatformCount; i++)
            {
                var otherLvl = i + yOff;
                if (!otherLvl.IsInRange(0, otherRoom.PlatformCount))
                    continue;
                //Debug.Log($"i {i} otherLvl {otherLvl}");

                var myPlatform = myRoom.GetPlatformLayer(i);
                var otherPlatform = otherRoom.GetPlatformLayer(otherLvl);

                _GetCopyEdgeTilesLoopData(neighborDir, myPlatform, otherPlatform, out var myLoop, out var otherLoop,
                    out var xOff, out var zOff);

                for (var idx = 0; idx < myLoop.LoopSize; ++idx)
                {
                    if (_CopyEdgeTilesGetPos(myLoop, idx, otherLoop, xOff, zOff, otherPlatform, myPlatform, 
                        out var myTilePos, out var otherTilePos, 
                        ref debugString) == FilterResult.Filtered) 
                        continue;

                    //Debug.Assert(myPos.Equals(otherPos));
                    var otherTile = GetTile(otherPlatform, otherPlatform, otherTilePos);
                    // Debug.Log($" CopyEdgeTiles {myTilePos} {otherTile}");
                    SetTile(myPlatform, myPlatform, myTilePos, otherTile, ushort.MaxValue);
                }
            }
            
            if (debugString.Type == LogType.Error)
                debugString.Print();
        }

        static FilterResult _CopyEdgeTilesGetPos(EdgeTilesLoopData myLoop, int idx, EdgeTilesLoopData otherLoop, int xOff, int zOff,
            IPlatformLayer otherPlatform, IPlatformLayer myPlatform, 
            out Vector2Int myTilePos, out Vector2Int otherTilePos, 
            ref DebugString debugString)
        {
            myTilePos = default;
            myTilePos.x = (myLoop.MulXIdx * idx) + myLoop.AddXIdx;
            myTilePos.y = (myLoop.MulZIdx * idx) + myLoop.AddZIdx;

            otherTilePos = default;
            otherTilePos.x = (myLoop.MulXIdx * idx) + otherLoop.AddXIdx + xOff;
            otherTilePos.y = (myLoop.MulZIdx * idx) + otherLoop.AddZIdx + zOff;
            //Debug.Log($"myXComp {myXComp} myZComp {myZComp} otherXComp {otherXComp} otherZComp {otherZComp}");

            if (!otherTilePos.x.IsInRange(0, otherPlatform.TileDim.x))
                return FilterResult.Filtered;
            if (!otherTilePos.y.IsInRange(0, otherPlatform.TileDim.y))
                return FilterResult.Filtered;

            var myPos = new Vector3(myPlatform.PositionOffset.x + myTilePos.x, myPlatform.PositionOffset.y,
                myPlatform.PositionOffset.z + myTilePos.y);
            var otherPos = new Vector3(otherPlatform.PositionOffset.x + otherTilePos.x, otherPlatform.PositionOffset.y,
                otherPlatform.PositionOffset.z + otherTilePos.y);

            if (!myPos.Equals(otherPos))
                debugString.AddError($"myPos {myPos} otherPos {otherPos}");

            return FilterResult.Match;
        }

        static void _GetCopyEdgeTilesLoopData(Cardinals neighborDir, IPlatformLayer myPlatform, IPlatformLayer otherPlatform,
            out EdgeTilesLoopData myLoop, out EdgeTilesLoopData otherLoop, out int xOff, out int zOff)
        {
            myLoop = default;
            otherLoop = default;

            if (neighborDir == Cardinals.North || neighborDir == Cardinals.South)
            {
                myLoop.LoopSize = myPlatform.TileDim.x;
                myLoop.MulXIdx = 1;
                if (neighborDir == Cardinals.North)
                    myLoop.AddZIdx = myPlatform.TileDim.y - 1;
                else
                    otherLoop.AddZIdx = otherPlatform.TileDim.y - 1;
            }
            else
            {
                myLoop.LoopSize = myPlatform.TileDim.y;
                myLoop.MulZIdx = 1;
                if (neighborDir == Cardinals.East)
                    myLoop.AddXIdx = myPlatform.TileDim.x - 1;
                else
                    otherLoop.AddXIdx = otherPlatform.TileDim.x - 1;
            }

            xOff = myLoop.MulXIdx * Mathf.RoundToInt(myPlatform.PositionOffset.x - otherPlatform.PositionOffset.x);
            zOff = myLoop.MulZIdx * Mathf.RoundToInt(myPlatform.PositionOffset.z - otherPlatform.PositionOffset.z);
        }
        
        
        public static void GetNextOperationBounds(IRoomInstance spawnerRoom, ExitInfo atExit, 
            GridSettings gridSettings, out Bounds nextMinOperationBounds)
        {
            spawnerRoom.Get(out var bounds);
            var followingBounds = GetBoundsFollowingExit(bounds, atExit);
            // grow exit bounds, so that we get the bounds for the new corridor
            nextMinOperationBounds = GrowBoundsGridAligned(followingBounds, gridSettings);
        }

        public static bool TryGetWorkerArea(Bounds minOpBounds, out IWorkerArea area)
        {
            area = null;
            using var workerAreaScoped = SimplePool<List<IWorkerArea>>.I.GetScoped();
            var workerAreas = workerAreaScoped.Obj;
            OctreeInstance.WorkerArea.IsFreePlaceOrGatherIntersecting(minOpBounds, ref workerAreas);
            Debug.Assert(workerAreas.Count < 2);
            if (workerAreas.IsNullOrEmpty())
                return false;

            area = workerAreas.FirstOrDefault();
            return true;
        }
    }
}
