﻿using Core;
using Level.Room;
using UnityEngine;

namespace Level.Generation.Operations
{
    public static partial class WorkerOperations
    {
        #region Public
        public static bool IsReserved(Bounds minOpBounds)
        {
            var hasArea = TryGetWorkerArea(minOpBounds, out _);
            return hasArea;
        }

        public static bool TrySpawnWorker(IGeneratorData data, IRoomInstance fromRoom, Bounds minOpBounds, 
            ExitInfo exitInfo, IFactoryProvider factoryConfig, out ILevelGeneratorWorker worker, out DebugString errorString)
        {
            worker = null;
            errorString = default;
            
            var hasArea = TryGetWorkerArea(minOpBounds, out var area);
            errorString.AddLog($"Is Worker-Area: {hasArea}");
            if (hasArea)
                return area.TrySpawnWorker(data, fromRoom, minOpBounds, exitInfo, factoryConfig, out worker, ref errorString);
            
            errorString.AddLog($"factoryConfig is set: {factoryConfig == null}");
            var spawner = factoryConfig?.GetFactory(data);
            if (spawner == null)
                return false;
            
            worker = spawner.SpawnWorker(data, fromRoom, exitInfo);
            data.AddWorker(worker, fromRoom.Bounds.center);
            return true;
        }

        #endregion
    }
}
