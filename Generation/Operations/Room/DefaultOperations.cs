﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Core.Types;
using Generation.Data;
using Level.Enums;
using Level.Generation.Data;
using Level.Room;
using Level.Tiles;
using UnityEngine;

using static Level.Room.Operations.RoomOperations;
using LogType = Core.LogType;

namespace Level.Generation.Operations
{
    public static partial class WorkerOperations
    {
        public static void CreateInitRoom(IGeneratorData data, RoomConfig room, string name, Vector3 pos,
            Bounds currentExitBounds,
            out IRoomInstance roomInstance,
            out RoomObjectsDataComponent roomObjects)
        {
            CreateRoom(room, out var roomInstanceComponent, out roomObjects, name);

            roomInstanceComponent.gameObject.SetActive(false);
            roomInstanceComponent.Data.Transform.position = pos;
            roomInstanceComponent.Data.UpdateRoomPosition(pos);
            roomInstanceComponent.Data.UpdateVisuals();

            roomInstance = roomInstanceComponent;

            data.DEBUG_AddDebugText($"\r\n AddRoom {roomInstance.Bounds} \r\n");
            OctreeInstance.Rooms.AddObject(roomInstance);

            LevelManager.DEBUG_AddDrawBounds(BoundTags.CurrentExitBounds, currentExitBounds);
            LevelManager.DEBUG_AddDrawBounds(BoundTags.OperationBounds, roomInstance.Bounds);
        }
        
        public static void ProcessOpenExits(this ILevelGeneratorWorker this_worker, IGeneratorData data, 
            IRoomInstance currentRoom, LevelGeneratorWorkerFactory parentFactory,
            ICollection<ExitInfo> exits, ref bool dead, ref ExitInfo next)
        {
            //var grid = data.GridSettings;
            var thisWorkerDone = dead;

            foreach (var e in exits)
            {
                GetNextOperationBounds(currentRoom, e, data.GridSettings, out var opBounds);

                var thisWorker = !thisWorkerDone;
                // var exitBounds = BoundsOperations.GetBoundsFollowingExit(m_currentRoom.Bounds, e);
                if (thisWorker && !IsReserved(opBounds))
                {
                    thisWorkerDone = true;
                    next = e;
                }
                else
                {
                    // data.DEBUG_AddDebugText($" #{WorkerNr}spawnVal{spawnVal}");
                    if (!TrySpawnWorker(data, currentRoom, opBounds, e, parentFactory.ContinueWithFactories.Result,
                        out var newWorker, out var errorString))
                    {
                        errorString.Prepend($"#{this_worker.WorkerNr} Could not spawn worker!");
                        errorString.Type = LogType.Error;
                        errorString.Print();
                        return;
                    }
                    data.DEBUG_AddDebugText($" #{this_worker.WorkerNr}workerAdd{newWorker.WorkerNr}_{newWorker.IconName}");
                }
            }

            dead |= next == null;
        }
        public static void PrepareForNext(this ILevelGeneratorWorker worker, 
            IRoomInstance currentRoom,
            IGeneratorData data, ExitInfo next,
            ref Vector3Int position)
        {
            if (next == null) 
                return;
            
            data.DEBUG_AddDebugText(";");
            worker.UpdateOperationBounds(data, currentRoom, next);
            position = new Vector3Int((int) worker.CurrentExitBounds.center.x, (int) worker.CurrentExitBounds.center.y,
                (int) worker.CurrentExitBounds.center.z);
        }
        
        public static bool DefaultDecideDeath(this ILevelGeneratorWorker worker, 
            float deathChance,
            IRoomInstance currentRoom, 
            IEnumerable<NeighborData> neighbors, 
            IGeneratorData data)
        {
            if (currentRoom == null)
                return true;

            var freeSpaceExists = neighbors.Any((n) => !n.BlockedSpace);
            var deathP = data.Random.NextDouble();
            data.DEBUG_AddDebugText($" #{worker.WorkerNr}deathP{deathP}");
            if (freeSpaceExists && deathP > deathChance)
                return false;

            // cannot die with no offspring
            return !freeSpaceExists || (data.BabyWorker.Count != 0 || data.Worker.Count - data.DyingCount >= 3);
        }
        
        public static void DefaultDeathAction(this ILevelGeneratorWorker worker, IRoomInstance currentRoom,
            GeometryIdentifierConfig geometryIdentifier,
            TilesSetListConfig tilesSetListConfig)
        {
            if (!worker.Dead)
                return;

            var floorId = geometryIdentifier.Get(GeometryType.Floor);
            var groundTileFilter = TileTypeOperations.GetFilter(tilesSetListConfig, floorId);

            // Death by creating room (todo treasure)
            FillLevelExceptEdge(currentRoom, 0, groundTileFilter);
        }
        
        public static void TurningDecision(this ILevelGeneratorWorker worker,
            float turnChance, bool dead,
            NeighborData[] neighbors,
            IGeneratorData data,
            ref Cardinals currentDirection)
        {
            if (dead)
                return;
            
            var needTurn = (neighbors[(int)currentDirection].BlockedSpace);
            //Debug.Log("needTurn " + needTurn);

            var turnP = data.Random.NextDouble();
            data.DEBUG_AddDebugText($" #{worker.WorkerNr}turnP{turnP}");

            if (!needTurn && turnP > turnChance)
                return;

            var dir = data.Random.Next(0, 2);
            data.DEBUG_AddDebugText($" #{worker.WorkerNr}turnDir{dir}");

            Cardinals proposedDir;
            if (dir == 0)
                proposedDir = (Cardinals)(((int)currentDirection + 3) % 4);
            else
                proposedDir = (Cardinals)(((int)currentDirection + 1) % 4);

            if (neighbors[(int)proposedDir].BlockedSpace)
            {
                proposedDir = (Cardinals)(((int)proposedDir + 2) % 4);
                // in this case turning was actually impossible:
                if (neighbors[(int)proposedDir].BlockedSpace)
                    proposedDir = currentDirection;
            }

            currentDirection = proposedDir;
            //Debug.Log("I turn from" + prevDir + " to: " + m_currentDirection);
        }

    }
}
