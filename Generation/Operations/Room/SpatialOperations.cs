﻿using System.Collections.Generic;
using Core.Types;
using Level.Enums;
using Level.PlatformLayer;
using Level.PlatformLayer.Interface;
using Level.Room;
using UnityEngine;
using static Level.PlatformLayer.Operations;

namespace Level.Generation.Operations
{
    public static partial class WorkerOperations
    {
        public static void ChangeLevelCount(IRoomInstance room, int newLevelCount)
        {
            var prevCount = room.PlatformCount;
            var roomSettings = room.RoomSettings;

            var autoFilter = roomSettings.GetFilter(GeometryType.Auto);
            var floorFilter = roomSettings.GetFilter(GeometryType.Floor);
            var emptyFilter = roomSettings.GetFilter(GeometryType.Empty);
            //var wallFilter = roomSettings.GetFilter(GeometryType.Wall);

            // how many levels does the room have
            newLevelCount = Mathf.Clamp(newLevelCount, 1, room.MaxPlatformCount);
            if (newLevelCount == prevCount)
                return;

            // ReSharper disable once ConvertToUsingDeclaration
            using (var platformLayersObj = SimplePool<List<IPlatformLayer>>.I.GetScoped())
            {
                var newLevels = platformLayersObj.Obj;
                var lastLevel = room.GetPlatformLayer(room.PlatformCount - 1);
                for (var i = 0; i < newLevelCount; ++i)
                {
                    if (i < room.PlatformCount)
                        newLevels.Add(room.GetPlatformLayer(i));
                    else
                    {
                        var lvl = new PlatformLayerInstance(room.RoomSettings.Grid,
                            new Vector2Int(room.Size.x, room.Size.z),
                            Vector2Int.zero,
                            Vector3Int.zero);

                        // set lvl-tiles according to level below
                        for (var x = 0; x < room.TileDim.x; ++x)
                        {
                            for (var z = 0; z < room.TileDim.y; ++z)
                            {
                                var pos = new Vector2Int(x, z);
                                SetTile(lvl, lvl, pos, autoFilter);
                                var belowTile = GetTile(lastLevel, lastLevel,
                                    new Vector2Int(x, z), OutOfLevelBoundsAction.IgnoreTile);
                                var belowGeo = autoFilter.Data.GetTileIdx(belowTile);
                                //if (belowTile == ushort.MaxValue)
                                //    belowTile = (ushort)GeometryType.Wall << 2;
                                //var belowResult = ResultingType(belowTile);
                                if (floorFilter.IsTileActive(belowTile))
                                    belowGeo = emptyFilter.FilterIdx;
                                SetTile(lvl, lvl, pos, belowGeo, 
                                    autoFilter.Data.Mask, 
                                    autoFilter.Data.Shift);
                            }
                        }

                        newLevels.Add(lvl);
                    }
                }

                room.SetPlatformLevels(newLevels);
            }
        }

        public static OperationResult ExpandLevelIfRequired(IRoomInstance room, int levelIdx)
        {
            var allOk = true;
            if (levelIdx + 1 > room.PlatformCount)
            {
                if (DebugInfo.Action == OutOfLevelBoundsAction.ExpandBounds && (levelIdx + 1) <= room.MaxPlatformCount)
                {
                    ChangeLevelCount(room, levelIdx + 1);
                    return OperationResult.OK;
                }

                allOk = false;
            }
            else if (levelIdx < 0)
                allOk = false;

            if (DebugInfo.Action != OutOfLevelBoundsAction.IgnoreTile && !allOk)
                DebugInfo.Result = OperationResult.Error;
            return allOk ? OperationResult.OK : OperationResult.Error;
        }
    }
}
