using Core.Types;
using Generation.Data;
using Level.PlatformLayer.Interface;
using Level.Room;
using Level.Tiles;
using Level.Enums;
using UnityEngine;

using static Level.PlatformLayer.Operations;

namespace Level.Generation.Operations
{
    public static partial class WorkerOperations
    {
        public static void CornerMunching(IRoomInstance myRoom, int lvl, CornerMunchingData munchingData, NeighborData[] neighborData,
            bool[] munchAtCorners)
        {
            Debug.Assert(munchAtCorners!= null && munchAtCorners.Length == 4);
            Debug.Assert(neighborData!= null && neighborData.Length == 4);

            var platform = myRoom.GetPlatformLayer(lvl);
            var tileDim = platform.TileDim;
            var autoFilter = myRoom.RoomSettings.GetFilter(GeometryType.Auto);
            var wallFilter = myRoom.RoomSettings.GetFilter(GeometryType.Wall);

            var munched = 0;
            foreach (var i in munchingData.Corners)
            {
                GetCardinalCornerOpData(i, neighborData, out var corner, out var sideA, out var sideB, out var sideABlocked, out var sideBBlocked);
                GetCornerPositionAndSideDirections(corner, tileDim, out var tilePos, out _, out _);
                
                var freeSpaces = 0;
                if (!sideABlocked) ++freeSpaces;
                if (!sideBBlocked) ++freeSpaces;

                var tile = GetTile(platform, platform, tilePos);
                var isWall = wallFilter.IsTileActive(tile) || autoFilter.IsTileActive(tile);
                var ok = isWall || freeSpaces == 2;

                if (!ok)
                {
                    munchAtCorners[i] = false;
                    //Debug.Log($"!OK corner: {corner} freeSpaces {freeSpaces} isWall false");
                    continue;
                }

                //Debug.Log($"corner: {corner} sideA {sideA} blocked {sideABlocked} sideB {sideB} blocked {sideBBlocked} isWall {isWall}");
                var mustCreateCorner = isWall;

                var munchChance = munchingData.MunchChance *
                                  munchingData.SuccessiveMunchChanceMultiplier.Evaluate(munched / 3f);
                var munch = mustCreateCorner 
                            || munchingData.Random.NextDouble() < munchChance;
                //Debug.Log($"munchChance {munchChance} munch {munch}");

                if (munch) munched++;
                munchAtCorners[i] = munch;
            }
            for (var i = 0; i < 4; i++)
            {
                if (!munchAtCorners[i])
                {
                    //Debug.Log($"corner {i} ignored!");
                    continue;
                }

                GetCardinalCornerOpData(i, neighborData, out var corner, out var sideA, out var sideB, out var sideABlocked, out var sideBBlocked);

                var munchPrevCorner = munchAtCorners[(i + 3) % 4];
                var munchNextCorner = munchAtCorners[(i + 1) % 4];
                var munchSideA = !sideABlocked && munchPrevCorner;
                var munchSideB = !sideBBlocked && munchNextCorner;

                //Debug.Log($"corner: {corner} sideA {sideA} blocked {sideABlocked} sideB {sideB} blocked {sideBBlocked}\n" +
                //          $"munchPrevCorner {munchPrevCorner} munchNextCorner {munchNextCorner} munchSideA {munchSideA} munchSideB {munchSideB}");

                GetCornerPositionAndSideDirections(corner, tileDim, out var tilePos, out var sideADir, out var sideBDir);

                GetSideWallRange(platform, sideADir, sideABlocked, tilePos, wallFilter, autoFilter, munchSideA, out var sideAWallRange);
                GetSideWallRange(platform, sideBDir, sideBBlocked, tilePos, wallFilter, autoFilter, munchSideB, out var sideBWallRange);

                var sideAPos = tilePos + sideADir * Mathf.Abs(sideAWallRange);
                var sideBPos = tilePos + sideBDir * Mathf.Abs(sideBWallRange);

                //Debug.Log($"sideAWallRange {sideAWallRange} {sideAPos}");
                //Debug.Log($"sideBWallRange {sideBWallRange} {sideBPos}");

                var minX = Mathf.Min(sideAPos.x, sideBPos.x);
                var minZ = Mathf.Min(sideAPos.y, sideBPos.y);
                var maxX = Mathf.Max(sideAPos.x, sideBPos.x);
                var maxZ = Mathf.Max(sideAPos.y, sideBPos.y);

                for (var x = minX; x <= maxX; x++)
                for (var z = minZ; z <= maxZ; z++)
                    SetTile(platform, platform, new Vector2Int(x, z), autoFilter);
            }
        }

        static void GetCardinalCornerOpData(int i,
            NeighborData[] neighborData,
            out CardinalCorners corner, out Cardinals sideA, out Cardinals sideB,
            out bool sideABlocked, out bool sideBBlocked)
        {
            corner = (CardinalCorners) i;

            sideA = (Cardinals) i;
            sideB = (Cardinals) ((i + 1) % 4);

            sideABlocked = neighborData[(int) sideA].BlockedSpace;
            sideBBlocked = neighborData[(int) sideB].BlockedSpace;
        }

        static void GetCornerPositionAndSideDirections(CardinalCorners corner, Vector2Int tileDim,
            out Vector2Int tilePos, out Vector2Int sideADir, out Vector2Int sideBDir)
        {
            tilePos = default;
            sideADir = default;
            sideBDir = default;
            switch (corner)
            {
                case CardinalCorners.NorthEast:
                    tilePos = new Vector2Int(tileDim.x - 1, tileDim.y - 1);
                    sideADir = new Vector2Int(-1, 0);
                    sideBDir = new Vector2Int(0, -1);
                    break;
                case CardinalCorners.EastSouth:
                    tilePos = new Vector2Int(tileDim.x - 1, 0);
                    sideADir = new Vector2Int(0, 1);
                    sideBDir = new Vector2Int(-1, 0);
                    break;
                case CardinalCorners.SouthWest:
                    tilePos = new Vector2Int(0, 0);
                    sideADir = new Vector2Int(1, 0);
                    sideBDir = new Vector2Int(0, 1);
                    break;
                case CardinalCorners.WestNorth:
                    tilePos = new Vector2Int(0, tileDim.y - 1);
                    sideADir = new Vector2Int(0, -1);
                    sideBDir = new Vector2Int(1, 0);
                    break;
            }
        }

        static void GetSideWallRange(IPlatformLayer lvl, Vector2Int sideDir, bool sideBlocked, Vector2Int tilePos,
            TilesSetFilter wallFilter, TilesSetFilter autoFilter, bool fullSide, out int sideAWallRange)
        {
            var absDir = new Vector2Int(Mathf.Abs(sideDir.x), Mathf.Abs(sideDir.y));
            var halfSideLoop = Mathf.RoundToInt(0.5f * lvl.TileDim.x * absDir.x + 0.5f * lvl.TileDim.y * absDir.y);
            //Debug.Log(halfSideLoop);
            if (!sideBlocked)
            {
                sideAWallRange = fullSide ? halfSideLoop : Random.Range(1, halfSideLoop);
                return;
            }

            int i;
            for (i = 1; i < halfSideLoop; i++)
            {
                var sideTilePos = tilePos + i * sideDir;
                var sideTile = GetTile(lvl, lvl, sideTilePos);
                if (!wallFilter.IsTileActive(sideTile) && !autoFilter.IsTileActive(sideTile))
                    break;
            }

            sideAWallRange = i - 1;
        }
    }
}