using Core.Types;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Utility.Debug;
using UnityEngine;
using static Level.BoundsOperations;

namespace Level.Generation.Operations
{
    public static class WorkerAreaComponent
    {
        public static void GetBounds<T>(this T workerAreaComponent, Vector3Int gridSize, out Bounds provided) where T : IComponent, 
            IWorkerArea
        {
            var tr = workerAreaComponent.transform;
            workerAreaComponent.Get(out GridSettings gridSettings);
            if (gridSettings == null)
            {
                provided = new Bounds(tr.position, gridSize);
                return;
            }

            var size = new Vector3(gridSize.x * gridSettings.RoomChunkSize, 
                gridSize.y * gridSettings.RoomYChunkSize, 
                gridSize.z * gridSettings.RoomChunkSize);
            var trPos = tr.position;
        
            var x = Mathf.FloorToInt(trPos.x / gridSettings.RoomChunkSize) * gridSettings.RoomChunkSize;
            var y = Mathf.FloorToInt(trPos.y / gridSettings.RoomYChunkSize) * gridSettings.RoomYChunkSize;
            var z = Mathf.FloorToInt(trPos.z / gridSettings.RoomChunkSize) * gridSettings.RoomChunkSize;
        
            var pos = new Vector3(x, y, z);
        
            var bounds = GrowBoundsGridAligned(new Bounds(pos + 0.5f * size, size), gridSettings);
            provided = bounds;
        }
    
        public static OperationResult GetGridPosForBounds<T>(this T workerAreaComponent, Bounds bounds, out Vector2Int gridPos) where T : IComponent, 
            IWorkerArea
        {
            gridPos = default;
            var tr = workerAreaComponent.transform;
            workerAreaComponent.Get(out GridSettings gridSettings);
            if (gridSettings == null)
            {
                Debug.LogError("Could not provide Grid Pos!");
                return OperationResult.Error;
            }

            var trPos = tr.position;
            var x = Mathf.RoundToInt((bounds.min.x - trPos.x) / gridSettings.RoomChunkSize);
            var y = Mathf.RoundToInt((bounds.min.z - trPos.z) / gridSettings.RoomChunkSize);
            gridPos = new Vector2Int(x, y);
            return OperationResult.OK;
        }
        
        public static OperationResult GetBoundsForGridPos<T>(this T workerAreaComponent, Vector2Int gridPos, out Bounds bounds) where T : IComponent, 
            IWorkerArea
        {
            bounds = default;
            var tr = workerAreaComponent.transform;
            workerAreaComponent.Get(out GridSettings gridSettings);
            if (gridSettings == null)
            {
                Debug.LogError("Could not provide Grid Pos!");
                return OperationResult.Error;
            }

            var trPos = tr.position;

            var min = trPos + new Vector3(gridSettings.RoomChunkSize * gridPos.x, 0, gridSettings.RoomChunkSize * gridPos.y);
            var size = gridSettings.RoomChunkSize3D.Vector3();
            var center = min + 0.5f * size;
            bounds = new Bounds(center, size);
            
            return OperationResult.OK;
        }
        public static void OnDrawGizmos<T>(this T workerAreaComponent, ref Vector3 lastPos, Color color) where T : IComponent, IWorkerArea
        {
            workerAreaComponent.Get(out Bounds bounds);
            CustomDebugDraw.DrawBounds(bounds, color);

            workerAreaComponent.Snap(ref lastPos, bounds.min);
        }

        public static void Snap<T>(this T workerAreaComponent, ref Vector3 lastPos, Vector3 min) where T : IComponent, IWorkerArea
        {
            var tr = workerAreaComponent.transform;
            var trPos = tr.position;
            if ((trPos - min).sqrMagnitude <= float.Epsilon)
                return;
            if ((lastPos - trPos).sqrMagnitude <= float.Epsilon)
                return;

            lastPos = trPos;
            if (Event.current.control)
                tr.position = min;
        }
    }
}