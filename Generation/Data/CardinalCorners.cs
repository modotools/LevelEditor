
namespace Level.Generation
{
    public enum CardinalCorners
    {
        NorthEast,
        EastSouth,
        SouthWest,
        WestNorth,
    }
}