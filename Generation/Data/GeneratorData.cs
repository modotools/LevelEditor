using System.Collections.Generic;
using Core;
using UnityEngine;
using Core.Interface;
using Core.Unity.Types;
using Core.Unity.Types.ID;
using Core.Unity.Utility;
using Core.Unity.Utility.Debug;
using Level.Room;
using Random = System.Random;

namespace Level.Generation
{
    public class GeneratorData : IGeneratorData, IDebuggable, IOpenEndTracker
    {
        public GeneratorData(IDebuggable debuggable, 
            SeedData seed, 
            GeneratorSpatialData spatialData,
            StopwatchFrameSplitter frameSplitter,

            Dictionary<IRoomInstance, int> visibleRooms,
            CeilingGrid ceiling,

            bool showAllRooms = false)
        {
            DebugContext = debuggable.DebugContext;
            DebugID = debuggable.DebugID;

            if (!seed.UseSeed)
                seed.Seed = new Random().Next();
            // Debug.Log("Seed " + seed.Seed);
            m_random = new Random(seed.Seed);

            GridSettings = spatialData.Grid;

            HeroRoomGridPos = spatialData.HeroPos;
            CurrentGridBounds = spatialData.GridPosBound;

            FrameSplitter = frameSplitter;

            Worker = new List<ILevelGeneratorWorker>();
            BabyWorker = new List<ILevelGeneratorWorker>();

            // if (startRoom!= null)
            //     AddOpenExits(startRoom, startRoom.OpenExits);

            ShowAllRooms = showAllRooms;

            m_visibleRooms = visibleRooms;
            m_ceiling = ceiling;
        }

        #region Properties
        public Random Random
        {
            get
            {
                WorkHasBeenDone = true;
                return m_random;
            }
        }

        public List<ILevelGeneratorWorker> Worker { get; }
        public List<ILevelGeneratorWorker> BabyWorker { get; }
        public GridSettings GridSettings { get; }

        public int OpenEnds { get; private set; }

        public Bounds CurrentGridBounds { get; private set; }
        public Vector3Int HeroRoomGridPos { get; private set; }
        public StopwatchFrameSplitter FrameSplitter { get; }
        #endregion

        readonly List<int> m_roomDirections = new List<int> { 0, 1, 2, 3 };

        public IEnumerable<int> RoomDirections => m_roomDirections;
        public readonly bool ShowAllRooms;
        public bool WorkHasBeenDone;

        public IRoomInstance HeroRoom
        {
            get => RoomGlobals.HeroRoom;
            set => RoomGlobals.HeroRoom = value;
        }

        public int DyingCount { get; set; }

        public int NewWorkerNr
        {
            get
            {
                var nr = SpawnNr++;
                return nr;
            }
        }
        //public OperationData MeshOperationData = new OperationData();
        public int SpawnNr = 0;

        readonly Random m_random;

        readonly Dictionary<IRoomInstance, int> m_visibleRooms;
        readonly CeilingGrid m_ceiling;

        #region public methods
        public void AddWorker(ILevelGeneratorWorker worker, Vector3 parentPos)
        {
            if (worker == null)
                return;
            worker.SetDebugPos(parentPos);
            BabyWorker.Add(worker);
        }

        public void RaiseBabies()
        {
            Worker.AddRange(BabyWorker);
            BabyWorker.Clear();
        }

        internal void SetCurrentGridBounds(Vector3Int heroGridPos, Bounds gridPosBound)
        {
            HeroRoomGridPos = heroGridPos;
            CurrentGridBounds = gridPosBound;
        }

        internal void SetHeroRoom(RoomInstanceComponent r)
            => HeroRoom = r;

        public void AddNewRoom_GetIsVisible(IRoomInstance newRoom, out bool isVisible)
        {
            isVisible = false;
            foreach (var c in newRoom.ClosedExits)
            {
                if (!m_visibleRooms.ContainsKey(c.ConnectedTo))
                    continue;
                var steps = m_visibleRooms[c.ConnectedTo];
                if (steps <= 0)
                    continue;

                isVisible = true;
                m_visibleRooms.Add(newRoom, steps);
                m_ceiling.SetSectionVisible(newRoom, isVisible);
                break;
            }
        }

        #region IOpenEndTracker
        public void AddOpenExits(IRoomInstance r, IEnumerable<ExitInfo> openExits)
        {
#if UNITY_EDITOR
            foreach (var e in openExits) 
                AddOpenExit(r, e);
#else
            OpenEnds += openExits.Count();
#endif
        }

        public void AddOpenExit(IRoomInstance r, ExitInfo exit)
        {
            exit.ConnectFrom = r;

#if UNITY_EDITOR
            Editor_OpenExits.Add(exit);
#endif
            OpenEnds++;
        }

        public void CloseExit(ExitInfo exit)
        {
#if UNITY_EDITOR
            Editor_OpenExits.Remove(exit);
#endif
            OpenEnds++;
        }
        #endregion

#endregion

        internal void CremateDead()
        {
            // cremation of the dead
            //foreach (var w in m_data.Worker.Where(w=>w.Dead)) Debug.Log($"worker died: {w.WorkerNr}");
            Worker.RemoveAll((w) => w.Dead && w.Return());
            DyingCount = 0;
        }
        #region Debug

        public DebugString DebugText;

        public void ClearDebugText()
        {
            if (this.DEBUGGING())
                DebugText.Reset();
        }

        public void DEBUG_AddDebugText(string text)
        {
            if (this.DEBUGGING())
                DebugText.AddLog(text);
        }

        public IDebugContext DebugContext { get; }
        public DebugID DebugID { get; }
#endregion

#if UNITY_EDITOR
        public List<ExitInfo> Editor_OpenExits { get; } = new List<ExitInfo>();
#endif
    }

}