using System;

namespace Level.Generation
{
    [Serializable]
    public struct SeedData
    {
        public bool UseSeed;
        public int Seed;
    }
}