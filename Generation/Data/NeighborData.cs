using System.Collections.Generic;
using Core.Extensions;
using Level.Room;
using UnityEngine;

namespace Generation.Data
{
    public struct NeighborData
    {
        public NeighborData(Bounds neighborBounds, List<IRoomInstance> rooms)
        {
            Bounds = neighborBounds;
            Rooms = rooms.ToArray();
            BlockedSpace = !rooms.IsNullOrEmpty();
        }
        public NeighborData(Bounds neighborBounds, int lvlSeg)
        {
            Bounds = neighborBounds;
            Rooms = new IRoomInstance[lvlSeg];
            BlockedSpace = false;
        }
        public void SetRoom(int lvl, IRoomInstance r)
        {
            Rooms[lvl] = r;
            BlockedSpace = true;
        }

        public Bounds Bounds;
        public IRoomInstance[] Rooms;

        public bool BlockedSpace;

        public static NeighborData Default => new NeighborData()
        {
            Rooms = null,
            Bounds = default,
            BlockedSpace = false
        };
    }
}
