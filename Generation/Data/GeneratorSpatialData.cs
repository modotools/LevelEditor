using System;
using UnityEngine;

namespace Level.Generation
{
    [Serializable]
    public struct GeneratorSpatialData
    {
        public GridSettings Grid;
        public Vector3Int HeroPos;
        public Bounds GridPosBound;
    }
}