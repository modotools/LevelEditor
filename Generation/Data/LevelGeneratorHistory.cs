﻿using System.Collections.Generic;
using Core.Extensions;
using UnityEngine;

namespace Level.Generation
{
    [System.Serializable]
    public class LevelGeneratorHistory
    {
        public List<Vector3> Positions = new List<Vector3>();
        public List<int> Iterations = new List<int>();

        public bool HasData => !Positions.IsNullOrEmpty() && !Iterations.IsNullOrEmpty();
    }
}