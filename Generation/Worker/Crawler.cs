using Core.Types;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Core.Unity.Extensions;
using Cysharp.Threading.Tasks;
using Level.Generation.Data;
using Generation.Data;
using Level.Room;
using Level.Room.Operations;
using Level.Tiles;
using Level.Enums;
using UnityEngine;

using static Level.Room.Operations.RoomOperations;
using static Level.Generation.Operations.WorkerOperations;
using static UnityEngine.Mathf;

namespace Level.Generation
{
    public class Crawler : LevelGeneratorWorker<CrawlerParameter>
    {
        CrawlerParameter m_parameter;
        const string k_workerTag = "[CRWL]";
        const string k_icon = "crawler.png";

        protected override void SetParameter(CrawlerParameter parameter)
            => m_parameter = parameter;

        public override string IconName => k_icon;
        
        #if UNITY_EDITOR
        public static Texture2D Editor_Icon => (Texture2D) UnityEditor.EditorGUIUtility.Load(k_icon);
        #endif
        
        protected override Bounds CurrentOperationBounds => m_currentMinOperationBounds;

        readonly NeighborData[] m_tempNeighbors = new NeighborData[4];

        ExitInfo m_next;
        // System.Action m_doLater;

        public override bool Return()
        {
            SimplePool<Crawler>.I.Return(this);
            return true;
        }

        public override async UniTask Operate(IGeneratorData data, CancellationToken cancellationToken)
        {
            // create the next corridor so this space is blocked for other workers
            //m_currentRoom = CreateFlatRoom(data.GridSettings.RoomChunkSize);
            var pos = CurrentOperationBounds.min;

            var name = $"{k_workerTag}#{WorkerNr} {pos} ";
            CreateInitRoom(data, m_parameter.Template, name, pos, m_currentExitBounds, out m_currentRoom, out var roomObjects);

            //var originDir = m_currentDirection;
            GatherNeighbors(m_currentRoom, OctreeInstance.Rooms, m_tempNeighbors);

            // maybe die later, controlled by LevelManager
            // todo remove from worker list
            m_dead = Dying(data);

            TurningDecision(data);

            using (var boundListScoped = SimplePool<List<Bounds>>.I.GetScoped())
            using (var exitInfoScoped = SimplePool<List<ExitInfo>>.I.GetScoped())
            {
                var exits = boundListScoped.Obj;
                var newOpenExits = exitInfoScoped.Obj;

                exits.Add(m_currentExitBounds);

                ProcessConnectionsAndNewExits(data, exits, newOpenExits);
                m_dead |= m_next == null;
                DeathAction();
                
                m_currentRoom.OpenExits.AddRange(newOpenExits);
                data.AddOpenExits(m_currentRoom, m_currentRoom.OpenExits);

                BuildTheRoom(data, exits);
            }

            PrepareForNext(data);
        }
        
        void PrepareForNext(IGeneratorData data) => this.PrepareForNext(m_currentRoom, data, m_next, ref m_position);
        bool Dying(IGeneratorData data) =>
            this.DefaultDecideDeath(m_parameter.DeathChance,
                m_currentRoom, m_tempNeighbors, data);
        void DeathAction() => this.DefaultDeathAction(m_currentRoom, m_parameter.GeometryIdentifierConfig, m_parameter.Template.TileSetListConfig);

        void TurningDecision(IGeneratorData data) 
            => this.TurningDecision(m_parameter.TurnChance, Dead, m_tempNeighbors, data, ref m_currentDirection);

        void ProcessConnectionsAndNewExits(IGeneratorData data,
            List<Bounds> exits, List<ExitInfo> newOpenExits)
        {
            // we randomize the direction order, so we don't accidentally create tendencies 
            // f.e. new exits more likely for North or so, 
            // this is probably not needed anymore, but it doesn't hurt either
            var roomDirectionsRandom = data.RoomDirections.OrderBy(item => data.Random.Next());
            foreach (var direction in roomDirectionsRandom)
            {
                var counterDir = (Cardinals)((direction + 2) % 4);
                ProcessExitsForDirection(data, (Cardinals)direction, counterDir, exits, newOpenExits);
            }
        }

        void ProcessExitsForDirection(IGeneratorData data, Cardinals direction, Cardinals counterDirection,
            List<Bounds> exits, List<ExitInfo> newOpenExits)
        {
            if (m_tempNeighbors[(int)direction].BlockedSpace)
            {
                //Debug.Log("There is neighbor at " + direction);
                ProcessConnectionsToNeighborRooms(data, m_tempNeighbors[(int)direction].Rooms, direction, exits,
                    newOpenExits);
            }
            else
            {
                ProcessProceedingsToFreeSpace(data, m_tempNeighbors[(int)direction].Bounds, direction, counterDirection,
                    exits, newOpenExits);
            }
        }

        void ProcessConnectionsToNeighborRooms(IGeneratorData data, IEnumerable<IRoomInstance> neighborRooms,
            Cardinals direction,
            List<Bounds> exits, List<ExitInfo> newOpenExits)
        {
            var counterDir = (Cardinals)(((int)direction + 2) % 4);

            foreach (var neighborRoom in neighborRooms)
            {
                if (neighborRoom == null)
                    continue;

                using (var scopedExitBounds = SimplePool<List<Bounds>>.I.GetScoped())
                {
                    var newClosedExitBounds = scopedExitBounds.Obj;

                    ConnectOpenEndsInsideBounds(m_currentRoom, neighborRoom, counterDir,
                        m_currentRoom.Bounds, ref newClosedExitBounds, data);

                    var forceP = data.Random.NextDouble();
                    data.DEBUG_AddDebugText($" #{WorkerNr}forceP{forceP}");

                    foreach (var exit in newClosedExitBounds)
                    {
                        // TODO: important!! extract to helper class
                        // make ExitInfo and Bounds easily convertible
                        var inf = new ExitInfo { ConnectedTo = neighborRoom, ExitDirection = direction };
                        m_currentRoom.ClosedExits.Add(inf);
                    }

                    exits.AddRange(newClosedExitBounds);
                }
            }
        }

        void ProcessProceedingsToFreeSpace(IGeneratorData data, Bounds freeSpace, Cardinals direction,
            Cardinals counterDirection,
            ICollection<Bounds> exits, ICollection<ExitInfo> newOpenExits)
        {
            var grid = data.GridSettings;

            var thisCrawler = !Dead && m_currentDirection == direction;
            var spawnP = data.Random.NextDouble();
            data.DEBUG_AddDebugText($" #{WorkerNr}spawnP{spawnP}");

            // todo: check if room has already an exit there, then we must spawn a worker there
            var spawnWorker = !thisCrawler && (spawnP <= m_parameter.SpawnChance);

            if (!thisCrawler && !spawnWorker)
                return;

            var newExit = new ExitInfo();
            // random exists at min. two tiles away from corner (so we can create passages with radius 1 without accidentally destroying walls)
            var exitAt = data.Random.Next(3, grid.RoomChunkSize - 1);
            data.DEBUG_AddDebugText($" #{WorkerNr}exitAt{exitAt}");

            newExit.ExitDirection = counterDirection;
            newExit.Level = 0;
            newExit.FromIdx = exitAt - 1;
            newExit.ToIdx = exitAt;
            var newExitB = BoundsOperations.GetBoundsFollowingExit(freeSpace, newExit);
            exits.Add(newExitB);
            newExit.ExitDirection = direction;
            newOpenExits.Add(newExit);

            LevelManager.DEBUG_AddDrawBounds(BoundTags.NewExitBounds, newExitB);

            if (spawnWorker)
            {
                var spawnVal = (float)data.Random.NextDouble();
                data.DEBUG_AddDebugText($" #{WorkerNr}spawnVal{spawnVal}");

                var spawner = m_parentFactory.GetContinuingFactories(data);
                var worker = spawner.SpawnWorker(data, m_currentRoom, newExit);
                data.DEBUG_AddDebugText($" #{WorkerNr}workerAdd{worker.WorkerNr}_{worker.IconName}");
                data.AddWorker(worker, DebugPos);

                // todo do this via method, so they will only start next Generation
            }
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            else if (thisCrawler)
            {
                //Debug.Log("I continue hopefully in " + direction);
                m_next = newExit;
            }
        }


        void BuildTheRoom(IGeneratorData data, List<Bounds> exits)
        {
            var floor = m_currentRoom.RoomSettings.GetFilter(GeometryType.Floor);
            ResetOperationDebugInfo();

            AddTilesAtBounds(m_currentRoom, exits);

            var chunkSize = data.GridSettings.RoomChunkSize3D;
            var roomCenterIdx =
                new Vector2Int(RoundToInt(chunkSize.x / 2f), RoundToInt(chunkSize.z / 2f));
            foreach (var e in exits)
            {
                var exitCenterIdx = e.center - m_currentRoom.Bounds.min;

                // todo: this is quick and dirty code to reduce artifacts:
                // would be nice to not only have the bounds here
                exitCenterIdx = Vector3.Lerp(exitCenterIdx, roomCenterIdx.Vector3(), 0.1f);

                CreatePassage(m_currentRoom, 0, new Vector2Int(RoundToInt(exitCenterIdx.x), RoundToInt(exitCenterIdx.z)),
                    roomCenterIdx, floor);
            }

            DrawCircle(m_currentRoom, 0, roomCenterIdx, 1, floor);

            if (DebugInfo.Result == OperationResult.Error)
                Debug.LogError("Error for " + WorkerNr);

            //m_currentRoom.UpdateResulting(0);

            //if (IsVisible(data))
                m_currentRoom.Show();
        }

        public override OperationResult ReduceOperationBounds(IGeneratorData data)
        {
            Debug.LogError("Cannot reduce! This should not happen!");
            return OperationResult.Error;
        }
    }
}