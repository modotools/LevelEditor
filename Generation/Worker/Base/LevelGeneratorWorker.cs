using Core.Types;
using Core.Unity.Extensions;
using Level.Room;
using Level.Generation.Data;
using UnityEngine;

using static Level.BoundsOperations;

#if UNITASK
using System.Threading;
using Cysharp.Threading.Tasks;
#endif

namespace Level.Generation
{
    public abstract class LevelGeneratorWorker<T> : ILevelGeneratorWorker
    {
        protected abstract void SetParameter(T parameter);

        public ILevelGeneratorWorker Init(T parameter, IGeneratorData data,
            LevelGeneratorWorkerFactory parent,
            IRoomInstance spawnerRoom, ExitInfo atExit)
        {
            m_currentRoom = null;
            m_dead = false;

            SetParameter(parameter);

            m_workerNr = data.NewWorkerNr;

            m_parentFactory = parent;
            m_currentDirection = atExit.ExitDirection;
            UpdateOperationBounds(data, spawnerRoom, atExit);
            m_position = new Vector3Int((int)m_currentExitBounds.center.x, (int)m_currentExitBounds.center.y, (int)m_currentExitBounds.center.z);
            return this;
        }
        
        public ILevelGeneratorWorker Init(T parameter, IGeneratorData data,
            LevelGeneratorWorkerFactory parent, Bounds operationBounds)
        {
            m_currentRoom = null;
            m_dead = false;

            SetParameter(parameter);

            m_workerNr = data.NewWorkerNr;

            m_parentFactory = parent;
            m_currentDirection = Cardinals.Invalid;
            m_currentExitBounds = k_invalidBounds;
            m_currentMinOperationBounds = operationBounds;
            m_position = new Vector3Int((int)m_currentMinOperationBounds.center.x, (int)m_currentMinOperationBounds.center.y, (int)m_currentMinOperationBounds.center.z);
            return this;
        }
        
        protected LevelGeneratorWorkerFactory m_parentFactory;

        protected IRoomInstance m_currentRoom;

        static readonly Bounds k_invalidBounds = new Bounds(new Vector3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity), 
            new Vector3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity));
        
        protected Bounds m_currentExitBounds;
        protected Bounds m_currentMinOperationBounds;
        protected Cardinals m_currentDirection;

        protected bool m_dead;
        protected Vector3Int m_position;
        int m_workerNr;

        #region Properties

        public string FactoryName => m_parentFactory.CustomName;
        public string TypeName => GetType().Name;

        public int WorkerNr => m_workerNr;
        public Vector3 Position => m_position;
        public bool Dead => m_dead;

        public bool HasDebugPos => m_hasDebugPos;
        public Vector3 DebugPos => m_debugPos;

        protected abstract Bounds CurrentOperationBounds { get; }
        public abstract string IconName { get; }
        public string DeadIconName => "dead" + IconName;

        public Bounds CurrentExitBounds => m_currentExitBounds;
        #endregion

        public abstract bool Return();

#if UNITASK
        public abstract UniTask Operate(IGeneratorData data, CancellationToken cancellationToken);
#else
        public abstract void Operate(IGeneratorData data);
#endif

        public virtual void UpdateOperationBounds(IGeneratorData data, IRoomInstance spawnerRoom, ExitInfo atExit)
        {
            spawnerRoom.Get(out var bounds);
            m_currentExitBounds = GetBoundsFollowingExit(bounds, atExit);
            // grow exit bounds, so that we get the bounds for the new corridor
            m_currentMinOperationBounds = GrowBoundsGridAligned(m_currentExitBounds, data.GridSettings);
        }

        public bool CanOperate(IGeneratorData data, out bool inFocus)
        {
            inFocus = IsInFocusArea(data);

            // die if CurrentOperationBounds is blocked
            if (!OctreeInstance.Rooms.IsFreePlace(m_currentMinOperationBounds))
            {
                data.DEBUG_AddDebugText($" \r\nDIED#{m_workerNr}_{IconName}@Min-Bounds{m_currentMinOperationBounds} \r\n");
                m_currentRoom = null;

                LevelManager.DEBUG_AddDrawBounds(BoundTags.DebugInfoErrorBounds, m_currentExitBounds);
                //Debug.Log($"Muncher cannot operate #{WorkerNr} !IsFreePlace {m_currentMinOperationBounds}");
                m_dead = true;
                return false;
            }

            while (!OctreeInstance.Rooms.IsFreePlace(CurrentOperationBounds))
            {
                if (ReduceOperationBounds(data) != OperationResult.Error) 
                    continue;
                
                LevelManager.DEBUG_AddDrawBounds(BoundTags.DebugInfoErrorBounds, m_currentExitBounds);
                //Debug.Log($"Muncher cannot operate #{WorkerNr} !IsFreePlace {CurrentOperationBounds}, not reducable");

                data.DEBUG_AddDebugText($" \r\nDIED#{m_workerNr}_{IconName}@Bounds{CurrentOperationBounds} \r\n");
                m_currentRoom = null;
                m_dead = true;
                return false;
            }

            return inFocus;
        }

        public abstract OperationResult ReduceOperationBounds(IGeneratorData data);

        public bool IsInFocusArea(IGeneratorData data)
        {
            var currentGridBoundsIntersection = data.CurrentGridBounds.GetIntersectionType(m_currentMinOperationBounds);
            return currentGridBoundsIntersection != IntersectionType.None 
                   && currentGridBoundsIntersection != IntersectionType.Touch;
        }

        bool m_hasDebugPos;
        protected Vector3 m_debugPos;

        public void SetDebugPos(Vector3 debugPos)
        {
            m_hasDebugPos = true;
            m_debugPos = debugPos;
        }

        public void DebugDrawGizmos()
        {
            if (!m_hasDebugPos)
                SetDebugPos(m_position.Vector3());

            var visPos = m_debugPos;
            var pos = m_position.Vector3();
            m_debugPos = Vector3.Lerp(visPos, pos, 0.05f + (0.5f / (visPos - pos).sqrMagnitude));

            const string folder = "LevelGenerator";
            var iconNam = m_dead ? DeadIconName : IconName;
            Gizmos.DrawIcon(m_debugPos, $"{folder}/{iconNam}", true);
        }
    }
}