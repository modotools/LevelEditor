using System.Collections.Generic;
using Core.Types;
using System.Linq;
using System.Threading;
using Cysharp.Threading.Tasks;
using Level.Generation.Data;
using Generation.Data;

using Level.Room;
using Level.Room.Operations;
using Level.Generation.Operations;
using UnityEngine;

using static Level.Room.Operations.RoomOperations;
using static Level.Generation.Operations.WorkerOperations;

namespace Level.Generation
{
    public class Muncher : LevelGeneratorWorker<MuncherParameter>
    {
        MuncherParameter m_parameter;
        const string k_workerTag = "[MNCH]";
        const string k_icon = "muncher.png";

        protected override void SetParameter(MuncherParameter parameter)
            => m_parameter = parameter;

        public override string IconName => k_icon;
        
#if UNITY_EDITOR
        public static Texture2D Editor_Icon => (Texture2D) UnityEditor.EditorGUIUtility.Load(k_icon);
#endif

        protected override Bounds CurrentOperationBounds => m_currentMinOperationBounds;

        readonly NeighborData[] m_neighborData = new NeighborData[4];
        readonly bool[] m_munchAtCorner = new bool[4];

        ExitInfo m_next;
        
        System.Action m_doLater;

        public override bool Return()
        {
            SimplePool<Muncher>.I.Return(this);
            return true;
        }

        public override async UniTask Operate(IGeneratorData data, CancellationToken cancellationToken)
        {
            // create the next corridor so this space is blocked for other workers
            //m_currentRoom = CreateFlatRoom(data.GridSettings.RoomChunkSize);
            var pos = CurrentOperationBounds.min;

            var name = $"{k_workerTag}#{WorkerNr} {pos} ";

            var room = m_parameter.RoomProvider.Result.GetRoom(data);
            CreateInitRoom(data, room, name, pos, m_currentExitBounds, out m_currentRoom, out var roomObjects);

            //var originDir = m_currentDirection;
            GatherNeighbors(m_currentRoom, OctreeInstance.Rooms, m_neighborData);
            CopyAllEdgeTiles(m_currentRoom, m_neighborData);
            
            MunchCorners(data);

            m_currentRoom.OpenExitsFromTiles(m_currentRoom.OpenExits);
            FilterExitsAndCloseNeighborExits(m_currentRoom, m_currentRoom.OpenExits, m_neighborData, data);
            data.AddOpenExits(m_currentRoom, m_currentRoom.OpenExits);
            
            // maybe die later, controlled by LevelManager
            // todo remove from worker list
            m_dead = IsDying(data);
            ProcessOpenExits(data);
            DeathAction();      
            
            if (data.FrameSplitter != null)
                await data.FrameSplitter.SplitFrame(cancellationToken);

            await m_currentRoom.AsyncBuild(cancellationToken);

            await roomObjects.InitObjects(data.FrameSplitter, cancellationToken);
            data.AddNewRoom_GetIsVisible(m_currentRoom, out var isVisible);
            m_currentRoom.gameObject.SetActive(isVisible);

            PrepareForNext(data);
        }
        
        void MunchCorners(IGeneratorData data)
        {
            var randomCorners = data.RoomDirections.OrderBy(item => data.Random.Next());
            var munchData = new CornerMunchingData()
            {
                Corners = randomCorners,
                MunchChance = m_parameter.MunchChance,
                Random = data.Random,
                SuccessiveMunchChanceMultiplier = m_parameter.SuccessiveMunchChanceMultiplier
            };

            CornerMunching(m_currentRoom, 0, munchData, m_neighborData, m_munchAtCorner);
        }
        
        void PrepareForNext(IGeneratorData data) => this.PrepareForNext(m_currentRoom, data, m_next, ref m_position);
        void ProcessOpenExits(IGeneratorData data) => this.ProcessOpenExits(data, m_currentRoom, m_parentFactory, m_currentRoom.OpenExits, ref m_dead, ref m_next);
        void DeathAction() => this.DefaultDeathAction(m_currentRoom, m_parameter.GeometryIdentifierConfig, m_currentRoom.RoomSettings.TileSet);
        bool IsDying(IGeneratorData data) => this.DefaultDecideDeath(m_parameter.DeathChance,
               m_currentRoom, m_neighborData, data);

        public override OperationResult ReduceOperationBounds(IGeneratorData data)
        {
            Debug.LogError("Cannot reduce! This should not happen!");
            return OperationResult.Error;
        }
    }
}