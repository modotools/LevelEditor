using Core.Types;
using Level.Room;
using LevelEditor;
using UnityEngine;

using static Level.Generation.Operations.WorkerAreaComponent;

namespace Level.Generation
{
    [CreateAssetMenu(fileName = nameof(LayoutWorkerFactory), menuName = "Level/Worker/"+nameof(LayoutWorkerFactory))]
    public class LayoutWorkerFactory : LevelGeneratorWorkerFactory, IWorkerAreaFactory
    {
        public override WorkerProperties WorkerProperties => new WorkerProperties()
        {
            ExitModifier = false,
            MinSpace = Vector3Int.one,
            MaxSpace = Vector3Int.zero
        };
        
        public override string EditorTitle => nameof(LayoutWorkerFactory);

#if UNITY_EDITOR
        public override Texture2D Editor_FactoryIcon => LayoutWorkerRoomSpawner.Editor_Icon;
#endif

        public override ILevelGeneratorWorker SpawnWorker(IGeneratorData data, IRoomInstance spawnerRoom,
            ExitInfo atExit)
        {
            Debug.LogError("Use SpawnWorker with LayoutWorkerData");
            return null;
        }
        
        public ILevelGeneratorWorker SpawnWorker(IGeneratorData data, IRoomInstance spawnerRoom, ExitInfo atExit,
            LayoutWorkerData layoutWorkerData) =>
            SimplePool<LayoutWorkerRoomSpawner>.I.Get().Init(layoutWorkerData, data, this, spawnerRoom, atExit);

        public ILevelGeneratorWorker SpawnWorker(IGeneratorData data, Bounds bounds,
            LayoutWorkerData gridData) =>
            SimplePool<LayoutWorkerRoomSpawner>.I.Get().Init(gridData, data, this, bounds);
    }
}