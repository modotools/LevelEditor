using Core.Types;
using Level.Room;
using Level.Tiles;
using UnityEngine;
using UnityEngine.Serialization;

namespace Level.Generation
{
    [System.Serializable]
    public struct CrawlerParameter
    {
        public GeometryIdentifierConfig GeometryIdentifierConfig;
        public RoomConfig Template;

        [FormerlySerializedAs("deathChance")]
        public float DeathChance;
        [FormerlySerializedAs("turnChance")] 
        public float TurnChance;
        [FormerlySerializedAs("spawnChance")] 
        public float SpawnChance;
        [FormerlySerializedAs("forceConnectChance")]
        public float ForceConnectChance;

        public static CrawlerParameter Default => new CrawlerParameter()
        {
            DeathChance = 0.25f,
            TurnChance = 0.25f,
            SpawnChance = 0.25f,
            ForceConnectChance = 0.25f,
        };
    }

    public class CrawlerFactory : LevelGeneratorWorkerFactory
    {
        public CrawlerParameter Params;

        public override WorkerProperties WorkerProperties => new WorkerProperties()
        {
            ExitModifier = false,
            MinSpace = Vector3Int.one,
            MaxSpace = Vector3Int.zero
        };
        
        public override string EditorTitle => nameof(CrawlerFactory);

#if UNITY_EDITOR
        public override Texture2D Editor_FactoryIcon => Crawler.Editor_Icon;
#endif

        public override ILevelGeneratorWorker SpawnWorker(IGeneratorData data, IRoomInstance spawnerRoom,
            ExitInfo atExit)
            => SimplePool<Crawler>.I.Get().Init(Params, data, this, spawnerRoom, atExit);
    }
}