using Core.Types;
using Level.Room;
using Level.Tiles;
using UnityEngine;

namespace Level.Generation
{
    [System.Serializable]
    public struct MuncherParameter
    {
        public GeometryIdentifierConfig GeometryIdentifierConfig;
        public RefIRoomConfigProvider RoomProvider;

        public float DeathChance;
        public float MunchChance;
        public AnimationCurve SuccessiveMunchChanceMultiplier;

        public static MuncherParameter Default => new MuncherParameter()
        {
            MunchChance = 0.25f,
            DeathChance = 0.25f
        };
    }

    public class MuncherFactory : LevelGeneratorWorkerFactory
    {
        public MuncherParameter Params;
        public override WorkerProperties WorkerProperties => new WorkerProperties()
        {
            ExitModifier = false,
            MinSpace = Vector3Int.one,
            MaxSpace = Vector3Int.zero
        };
        
        public override string EditorTitle => nameof(MuncherFactory);

#if UNITY_EDITOR
        public override Texture2D Editor_FactoryIcon => Muncher.Editor_Icon;
#endif
        public override ILevelGeneratorWorker SpawnWorker(IGeneratorData data, IRoomInstance spawnerRoom,
            ExitInfo atExit)
            => SimplePool<Muncher>.I.Get().Init(Params, data, this, spawnerRoom, atExit);
    }
}