using System.Collections.Generic;
using System.Linq;
using Core.Types;
using System.Threading;
using _Editor.Level.Procedural;
using Core.Unity.Extensions;
using Cysharp.Threading.Tasks;
using Generation.Data;
using Level.Enums;
using Level.PlatformLayer.Interface;
using Level.Room;
using Level.Tiles;
using UnityEngine;

using static Level.Generation.Operations.WorkerOperations;
using static Level.PlatformLayer.Operations;
using static Level.Tiles.TileTypeOperations;

namespace Level.Generation
{
    public class LayoutWorkerRoomSpawner : LevelGeneratorWorker<LayoutWorkerData>
    {
        LayoutWorkerData m_data;
        const string k_workerTag = "[LAYOUT]";
        const string k_icon = "muncher.png";

        protected override void SetParameter(LayoutWorkerData parameter)
            => m_data = parameter;

        public override string IconName => k_icon;
        
#if UNITY_EDITOR
        public static Texture2D Editor_Icon => (Texture2D) UnityEditor.EditorGUIUtility.Load(k_icon);
#endif

        protected override Bounds CurrentOperationBounds => m_currentMinOperationBounds;

        readonly NeighborData[] m_neighborData = new NeighborData[4];
        ExitInfo m_next;
        public override bool Return()
        {
            SimplePool<LayoutWorkerRoomSpawner>.I.Return(this);
            return true;
        }

        public override async UniTask Operate(IGeneratorData data, CancellationToken cancellationToken)
        {
            // create the next corridor so this space is blocked for other workers
            //m_currentRoom = CreateFlatRoom(data.GridSettings.RoomChunkSize);
            var pos = CurrentOperationBounds.min;
            
            var name = $"{k_workerTag}#{WorkerNr} {pos} ";

            var gridData = m_data.LayoutConfig.GetGridData(m_data.GridPos);
            GetRoom(data, gridData, out var room);
            CreateInitRoom(data, room, name, pos, m_currentExitBounds, out m_currentRoom, out var roomObjects);
            
            //var originDir = m_currentDirection;
            GatherNeighbors(m_currentRoom, OctreeInstance.Rooms, m_neighborData);
            CopyAllEdgeTiles(m_currentRoom, m_neighborData);

            AddExitsFromLayout();
            
            m_currentRoom.OpenExitsFromTiles(m_currentRoom.OpenExits);
            FilterExitsAndCloseNeighborExits(m_currentRoom, m_currentRoom.OpenExits, m_neighborData, data);
            data.AddOpenExits(m_currentRoom, m_currentRoom.OpenExits);
            
            // maybe die later, controlled by LevelManager
            // todo remove from worker list
            m_dead = IsDying(data);
            ProcessOpenExits(data);
            // DeathAction();

            if (gridData.LayoutData.Connector)
            {
                using (var boundsListScoped = SimplePool<List<Bounds>>.I.GetScoped())
                {
                    ExitToBounds(m_currentRoom.ClosedExits, boundsListScoped.Obj);
                    ExitToBounds(m_currentRoom.OpenExits, boundsListScoped.Obj);
                    CreateConnectionsToCenter(data, boundsListScoped.Obj);
                }
            }

            if (data.FrameSplitter != null)
                await data.FrameSplitter.SplitFrame(cancellationToken);
            
            await m_currentRoom.AsyncBuild(cancellationToken);
            
            await roomObjects.InitObjects(data.FrameSplitter, cancellationToken);
            data.AddNewRoom_GetIsVisible(m_currentRoom, out var isVisible);
            m_currentRoom.gameObject.SetActive(isVisible);
            
        }

        void ExitToBounds(IEnumerable<ExitInfo> exits, List<Bounds> boundsList) 
            => boundsList.AddRange(exits.Select(exit => BoundsOperations.GetBoundsFollowingExit(m_currentRoom.Bounds, exit)));

        void GetRoom(IGeneratorData data, LevelNodeGridData gridData, out RoomConfig roomConfig)
        {
            if (gridData.RoomLevelGraphNode != null && gridData.RoomLevelGraphNode.RoomProvider?.Result != null)
            {
                roomConfig = gridData.RoomLevelGraphNode.RoomProvider.Result.GetRoom(data);
                return;
            }

            var isConnector = gridData.LayoutData.Connector;

            var node = isConnector ? m_data.LayoutConfig.DefaultNodeConnector : m_data.LayoutConfig.DefaultNodeRoom;
            roomConfig = node.RoomProvider?.Result?.GetRoom(data);
        }
        
        void AddExitsFromLayout()
        {
            var floorId = m_currentRoom.RoomSettings.GeometryIdentifierConfig.Get(GeometryType.Floor);
            var floorFilter = GetFilter(m_currentRoom.RoomSettings.TileSet, floorId);

            var gridData = m_data.LayoutConfig.GetGridData(m_data.GridPos);
            CreatePath(gridData.LayoutData.T, Cardinals.North, floorFilter);
            CreatePath(gridData.LayoutData.L, Cardinals.West, floorFilter);
            CreatePath(gridData.LayoutData.R, Cardinals.East, floorFilter);
            CreatePath(gridData.LayoutData.B, Cardinals.South, floorFilter);
        }

        void CreateConnectionsToCenter(IGeneratorData data, IReadOnlyCollection<Bounds> exits)
        {
            var floor = m_currentRoom.RoomSettings.GetFilter(GeometryType.Floor);
            ResetOperationDebugInfo();

            AddTilesAtBounds(m_currentRoom, exits);

            var chunkSize = data.GridSettings.RoomChunkSize3D;
            var roomCenterIdx =
                new Vector2Int(Mathf.RoundToInt(chunkSize.x / 2f), Mathf.RoundToInt(chunkSize.z / 2f));
            foreach (var e in exits)
            {
                var exitCenterIdx = e.center - m_currentMinOperationBounds.min;
                exitCenterIdx = Vector3.Lerp(exitCenterIdx, roomCenterIdx.Vector3(), 0.1f);

                CreatePassage(m_currentRoom, 0,
                    new Vector2Int(Mathf.RoundToInt(exitCenterIdx.x), Mathf.RoundToInt(exitCenterIdx.z)),
                    roomCenterIdx, floor);
            }

            DrawCircle(m_currentRoom, 0, roomCenterIdx, 1, floor);
        }

        void CreatePath(ConnectionType pathType, Cardinals direction, TilesSetFilter floorFilter)
        {
            if (pathType == ConnectionType.Path) 
                CreateExitTiles(m_currentRoom, direction, floorFilter);
        }

        public static void CreateExitTiles(IRoomInstance myRoom, Cardinals dir, TilesSetFilter floorFilter)
        {
            var myPlatform = myRoom.GetPlatformLayer(0);
            _GetEdgeTilesLoopData(dir, myPlatform, out var myLoop);

            for (var idx = 0; idx < myLoop.LoopSize; ++idx)
            {
                _EdgeTilesGetPos(myLoop, idx, myPlatform, out var myTilePos);
                // Debug.Log($" CreateExitTiles {myTilePos}");

                SetTile(myPlatform, myPlatform, myTilePos, floorFilter);
            }
        }
        
        static void _EdgeTilesGetPos(EdgeTilesLoopData myLoop, int idx,
            IPlatformLayer myPlatform, out Vector2Int myTilePos)
        {
            myTilePos = default;
            myTilePos.x = (myLoop.MulXIdx * idx) + myLoop.AddXIdx;
            myTilePos.y = (myLoop.MulZIdx * idx) + myLoop.AddZIdx;
            
            var myPos = new Vector3(myPlatform.PositionOffset.x + myTilePos.x, myPlatform.PositionOffset.y,
                myPlatform.PositionOffset.z + myTilePos.y);
        }
        
        static void _GetEdgeTilesLoopData(Cardinals neighborDir, IPlatformLayer myPlatform,
            out EdgeTilesLoopData myLoop)
        {
            myLoop = default;

            const int doorSize = 3;
            if (neighborDir == Cardinals.North || neighborDir == Cardinals.South)
            {
                myLoop.AddXIdx = Mathf.RoundToInt(0.5f * myPlatform.TileDim.x - 0.5f * doorSize);
                myLoop.LoopSize = doorSize;
                myLoop.MulXIdx = 1;
                if (neighborDir == Cardinals.North)
                    myLoop.AddZIdx = myPlatform.TileDim.y - 1;
            }
            else
            {
                myLoop.AddZIdx = Mathf.RoundToInt(0.5f * myPlatform.TileDim.y - 0.5f * doorSize);
                myLoop.LoopSize = doorSize;
                myLoop.MulZIdx = 1;
                if (neighborDir == Cardinals.East)
                    myLoop.AddXIdx = myPlatform.TileDim.x - 1;
            }

            // xOff = myLoop.MulXIdx * Mathf.RoundToInt(myPlatform.PositionOffset.x - otherPlatform.PositionOffset.x);
            // zOff = myLoop.MulZIdx * Mathf.RoundToInt(myPlatform.PositionOffset.z - otherPlatform.PositionOffset.z);
        }
        
        void ProcessOpenExits(IGeneratorData data)
        {
            ExitInfo next = null;
            this.ProcessOpenExits(data, m_currentRoom, m_parentFactory, m_currentRoom.OpenExits, ref m_dead,
                ref next);
            Debug.Assert(next == null);
        }

        void DeathAction()
        {
            // this.DefaultDeathAction(m_currentRoom, m_currentRoom.RoomSettings.GeometryIdentifierConfig,
            //     m_currentRoom.RoomSettings.TileSet);
        }

        bool IsDying(IGeneratorData data) => true;

        public override OperationResult ReduceOperationBounds(IGeneratorData data)
        {
            Debug.LogError("Cannot reduce! This should not happen!");
            return OperationResult.Error;
        }
    }
}