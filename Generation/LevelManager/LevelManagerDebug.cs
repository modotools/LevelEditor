using UnityEngine;
using System.IO;
using Core.Unity.Utility.Debug;
using Level.Generation.Data;
using Debug = UnityEngine.Debug;

namespace Level.Generation
{
    /// <summary>
    /// Manages all Operations for generating the Level
    /// </summary>
    public partial class LevelManager
    {
        /// <summary> how many cycles been iterated with current grid-position without actual work been done </summary>
        int m_emptyIterations;
        /// <summary> how many cycles of work has been done with current grid-position </summary>
        int m_posIterations;

        const string k_lastLevelGenFileDir = "/StreamingAssets/";
        const string k_lastLevelGenFilePath = k_lastLevelGenFileDir + "LastLevelGen.json";

        const string k_levelManagerDebugTextPathSave = k_lastLevelGenFileDir + "LastLevelGenDebugSave.json";
        const string k_levelManagerDebugTextPathLoad = k_lastLevelGenFileDir + "LastLevelGenDebugLoad.json";

        void Debug_LevelGeneratorStarted()
        {
            if (m_loadHistory)
                LoadHistory();
            m_history = new LevelGeneratorHistory();

            if (this.DEBUGGING())
                m_emptyIterations = 0;

            m_posIterations = 0;
            DEBUG_iteration = 0;

            m_history.Positions.Add(m_lastHeroGridPos);
            m_history.Iterations.Add(m_posIterations);
        }
        
        void DEBUG_NewHeroGridPos(Vector3Int newPos, Bounds gridPosBounds)
        {
            if (!this.DEBUGGING())
                return;
            if (m_posIterations != 0)
                m_debugText += m_data.DebugText;

            m_data.ClearDebugText();
            m_data.DEBUG_AddDebugText("\r\nNewHeroGridPos" + newPos);
            m_data.DEBUG_AddDebugText("GridPos" + gridPosBounds);
        }

        void Debug_LevelGenCycleEnd()
        {
            if (m_data.WorkHasBeenDone)
            {
                m_posIterations++;
                DEBUG_iteration++;
                m_data.DEBUG_AddDebugText("WorkDone@" + m_posIterations + " debug@" + DEBUG_iteration);
            }
            else if (this.DEBUGGING())
                m_emptyIterations++;

            if (DEBUG_SaveHistoryEveryIteration)
                SaveHistory();
        }

        #region History
        void _GetHistoryData()
        {
            if (m_loadedHistory == null)
                return;
            if (!m_loadedHistory.HasData)
            {
                // --------- trigger that load ended here
                m_loadedHistory = null;
                return;
            }

            if (m_posIterations >= m_loadedHistory.Iterations[0])
            {
                if (this.DEBUGGING())
                    m_debugText += m_data.DebugText;

                // prepare for next save
                var idx = m_history.Positions.Count - 1;
                m_history.Positions[idx] = m_loadedHistory.Positions[0];
                m_history.Iterations[idx] = m_loadedHistory.Iterations[0];
                m_history.Positions.Add(m_lastHeroGridPos);
                m_history.Iterations.Add(m_posIterations);

                m_loadedHistory.Iterations.RemoveAt(0);
                m_loadedHistory.Positions.RemoveAt(0);
                Debug.Log("Finished pos: " + m_lastHeroGridPos + " @it: " + m_posIterations);

                m_posIterations = 0;
                if (this.DEBUGGING())
                    m_emptyIterations = 0;

                _GetHistoryData();
                return;
            }

            if (this.DEBUGGING() && m_emptyIterations == 5)
            {
                Debug.Log("ERROR! " + m_data.CurrentGridBounds);

                var filePath = Application.dataPath + k_levelManagerDebugTextPathLoad;
                File.WriteAllText(filePath, m_debugText);
            }
            // pos is not allowed to change
            m_hero.position = m_loadedHistory.Positions[0];
        }
        
        public void SaveHistory()
        {
            if (m_history == null)
                return;

            var idx = m_history.Positions.Count - 1;
            m_history.Positions[idx] = m_lastHeroGridPos;
            m_history.Iterations[idx] = m_posIterations;
            if (m_posIterations <= 0)
            {
                m_history.Positions.RemoveAt(m_history.Positions.Count - 1);
                m_history.Iterations.RemoveAt(m_history.Iterations.Count - 1);
            }
            else Debug.Log("SaveHistory with pos: " + m_hero.position + " @it: " + m_posIterations);

            Directory.CreateDirectory(Application.dataPath + k_lastLevelGenFileDir);
            var json = JsonUtility.ToJson(m_history, true);
            var filePath = Application.dataPath + k_lastLevelGenFilePath;
            File.WriteAllText(filePath, json);

            if (!this.DEBUGGING())
                return;
            filePath = Application.dataPath + k_levelManagerDebugTextPathSave;
            File.WriteAllText(filePath, m_debugText);
        }

        void LoadHistory()
        {
            var filePath = Application.dataPath + k_lastLevelGenFilePath;
            var json = File.ReadAllText(filePath);
            m_loadedHistory = JsonUtility.FromJson<LevelGeneratorHistory>(json);
        }

        public void LoadHistoryOnStartPlay(bool load) 
            => m_loadHistory = load;

        void History_UpdateLastData()
        {
            //Debug.Log("Add lastPos: " + _lastPos + " it: "+ _posIterations);
            var idx = m_history.Positions.Count - 1;
            m_history.Positions[idx] = m_lastHeroGridPos;
            m_history.Iterations[idx] = m_posIterations;
        }

        void History_NewData(Bounds gridPosBound, Vector3Int newPos)
        {
            if (m_posIterations == 0) 
                return;
            var idx = m_history.Positions.Count - 1;
            //Debug.Log("put to history pos: " + m_history.Positions[idx] + " @it: " + m_posIterations + " grid: " +
            //          gridPosBound);

            if (this.DEBUGGING())
                m_emptyIterations = 0;

            m_posIterations = 0;
            m_history.Positions.Add(newPos);
            m_history.Iterations.Add(m_posIterations);
        }
        #endregion

        #region DEBUG
        // ReSharper disable InconsistentNaming
        [HideInInspector] public bool DEBUG_SaveHistoryEveryIteration;
        [HideInInspector] public bool DEBUG_Pause;
        [HideInInspector] public bool DEBUG_StepNext;
        [HideInInspector] public bool DEBUG_StepAllNext;
        [HideInInspector] public int DEBUG_PauseOnWorkerNr = -1;
        [HideInInspector] public int DEBUG_PauseOnIteration = -1;
        // current iteration cycle
        [HideInInspector] public int DEBUG_iteration;
        bool DEBUG_StepNeedWait => DEBUG_Pause && !(DEBUG_StepNext || DEBUG_StepAllNext);

        // ReSharper restore InconsistentNaming

        public GeneratorData DEBUG_GET_GeneratorData() 
            => m_data;
        
        // set next to false again
        void DEBUG_AfterOneWorkerStep() => DEBUG_StepNext = false;

        //Debug.Log("Worker Nr is Equal: " + (w.workerNr == DEBUG_PauseOnWorkerNr) + " w.workerNr : "+ w.workerNr);
        bool DEBUG_NextWorkerNeedWait(ILevelGeneratorWorker w) =>
            w.WorkerNr == DEBUG_PauseOnWorkerNr
            || DEBUG_PauseOnIteration == DEBUG_iteration;
        
        public delegate void AddDrawBoundsDelegate(BoundTags boundTag, Bounds currentExitBounds);

        public static event AddDrawBoundsDelegate AddDrawBoundsEvent;

        public static void DEBUG_AddDrawBounds(BoundTags boundTag, Bounds currentExitBounds) 
            => AddDrawBoundsEvent?.Invoke(boundTag, currentExitBounds);
        #endregion
    }
}