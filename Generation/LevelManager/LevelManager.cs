#if UNITASK

using System.Collections.Generic;
using System.Linq;
using Core;
using Core.Extensions;
using UnityEngine;
using Core.Interface;
using Core.Loading;
using Core.Types;
using Core.Unity.Attributes;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Types;
using Level.Room;
using Level.Generation.Operations;
using static UnityEngine.Mathf;
using static Level.BoundsOperations;
using Debug = UnityEngine.Debug;

namespace Level.Generation
{
    /// <summary>
    /// Manages all Operations for generating the Level
    /// </summary>
    [EditorIcon("workflow")]
    public partial class LevelManager : DebuggableBehaviour, ILevelManager,
        IHierarchyIcon, IVerify
    {
        public string HierarchyIcon => "Icons/workflow";
        
        [SerializeField] RoomSettings m_roomSettings;

        [SerializeField] BiomeSetting m_startingBiome;
        [SerializeField] CeilingGrid m_ceiling;
        [SerializeField] Transform m_hero;

        [SerializeField] bool m_initWithAllRoomsInScene;
        [SerializeField] RoomInstanceComponent[] m_initialLoadedRooms;
        
        [SerializeField] SeedData m_seedData;

        [SerializeField] int m_generateBoundsXZ = 2;
        [SerializeField] int m_generateBoundsY = 2;

        [SerializeField] bool m_showAllRooms;
        [SerializeField] [HideInInspector] bool m_loadHistory;
        [SerializeField] bool m_drawExits;

        [SerializeField, SelectAssetByName("LevelGenerator")] LoadOperationID m_loadingId;

        GridSettings GridSettings => m_roomSettings.Grid;

        //public List<ILevelGeneratorWorkerFactory> Factories;
        public BiomeSetting StartingBiome => m_startingBiome;
        public bool IsLoading;


        string m_debugText = "";

        GeneratorData m_data;

        // for debugging purposes, we can record the movement-path (because it influences generation)
        LevelGeneratorHistory m_history;
        LevelGeneratorHistory m_loadedHistory;

        Vector3 m_generatingBoundsSize;

        float m_heroGroundOffset;

        Vector3 m_lastHeroGridPos;
        Bounds m_lastHeroGridPosBounds;

        const int k_visibilitySteps = 3;
        readonly Dictionary<IRoomInstance, int> m_visibleRooms = new Dictionary<IRoomInstance, int>();

        #region Unity Calls
        void Start() => Init();

        void OnDrawGizmos()
        {
            if (m_data?.Worker != null)
                foreach (var w in m_data.Worker)
                    w.DebugDrawGizmos();
            if (m_data?.BabyWorker != null)
                foreach (var w in m_data.BabyWorker)
                    w.DebugDrawGizmos();

            if (m_data?.Editor_OpenExits != null && m_drawExits)
                foreach (var exit in m_data?.Editor_OpenExits) 
                    ExitOperations.Editor_DrawExit(exit.ConnectFrom, exit);
        }

        #endregion

        #region Init
        void Init()
        {
            IsLoading = true;
            LoadOperations.StartLoading(m_loadingId);

            if (!OctreeInstance.TryInitOctreesFromGameObject(gameObject))
                return;
            
            using var initialRoomsScoped = SimplePool<List<IRoomInstance>>.I.GetScoped();
            initialRoomsScoped.Obj.AddRange(m_initWithAllRoomsInScene
                ? OctreeInstance.Rooms.GetAllObjects()
                : m_initialLoadedRooms);

            foreach (var room in initialRoomsScoped.Obj)
                InitRoom(room);
        
            InitSpatialData(out var gridPos, out var generatorSpatialData);
            InitGeneratorData(generatorSpatialData);
            InitCeiling(initialRoomsScoped.Obj, gridPos);
            _UpdateVisibility(gridPos);

            foreach (var room in initialRoomsScoped.Obj)
                InitializeWorkers(room);
            
            DebugString errorString = default;
            foreach (var workerArea in OctreeInstance.WorkerArea.GetAllObjects())
                workerArea.SpawnAllWorker(m_data, ref errorString);
            m_data.RaiseBabies();
            
            StartAsync();
        }

        void InitCeiling(IEnumerable<IRoomInstance> initialRooms, Vector3Int gridPos)
        {
            if (m_ceiling == null)
                return;
            m_ceiling.SetPosition(gridPos);
            foreach (var initialRoom in initialRooms) 
                m_ceiling.SetSectionVisible(initialRoom, true);
        }

        void InitRoom(IRoomInstance initialRoom)
        {
            if (initialRoom is RoomInstanceComponent ric)
                ric.UpdatePosition();
            initialRoom.AddOpenExitsFromTiles();
            initialRoom.Show(); // startRoom.Room.ShowRoom(Data.MeshOperationData);
        }

        void InitGeneratorData(GeneratorSpatialData generatorSpatialData) 
            => m_data = new GeneratorData(this, m_seedData, generatorSpatialData, m_stopwatchFrameSplitter, 
                m_visibleRooms, m_ceiling, m_showAllRooms);

        void InitSpatialData(out Vector3Int gridPos, out GeneratorSpatialData generatorSpatialData)
        {
            m_generatingBoundsSize = new Vector3(GridSettings.RoomChunkSize * m_generateBoundsXZ,
                GridSettings.RoomYChunkSize * m_generateBoundsY, GridSettings.RoomChunkSize * m_generateBoundsXZ);

            m_heroGroundOffset = m_hero.position.y;
            gridPos = _GridifyHeroPos();
            m_lastHeroGridPos = gridPos;

            var gridPosBound = GrowBoundsGridAligned(new Bounds(gridPos, m_generatingBoundsSize), GridSettings);
            m_lastHeroGridPosBounds = gridPosBound;

            generatorSpatialData = new GeneratorSpatialData()
            {
                GridPosBound = gridPosBound,
                HeroPos = gridPos,
                Grid = m_roomSettings.Grid
            };
        }

        void InitializeWorkers(IRoomInstance startRoom)
        {
            for (var i = startRoom.OpenExits.Count-1; i >= 0; --i)
            {
                var exitInfo = startRoom.OpenExits[i];
                WorkerOperations.GetNextOperationBounds(startRoom, exitInfo, GridSettings, out var opBounds);

                using var neighbor = SimplePool<List<IRoomInstance>>.I.GetScoped();
                var neighborList = neighbor.Obj;
                OctreeInstance.Rooms.IsFreePlaceOrGatherIntersecting(opBounds, ref neighborList);
                Debug.Assert(neighborList.Count < 2);
                if (!neighborList.IsNullOrEmpty())
                {
                    WorkerOperations.CloseExit(startRoom, neighborList[0], m_data, exitInfo, i);
                    continue;
                }

                if (StartingBiome != null)
                {
                    var factoryConfig = StartingBiome.FallbackFactory;
                    var ok = WorkerOperations.TrySpawnWorker(m_data, startRoom, opBounds, exitInfo, factoryConfig.Result, out _, 
                        out var errorString);
                    if (!ok)
                        errorString.Print();
                }
            }
        }
        
        #endregion

        #region Level Gen Flow
        void LevelGeneratorStarted() => Debug_LevelGeneratorStarted();
        void OnNewLevelGenCycle()
        {
            _GetHistoryData();
            _UpdateFocusArea();

            m_stopwatchFrameSplitter.ResetWatch();

            m_data.WorkHasBeenDone = false;
        }
        
        void UpdateWorkersLifeAndDeath()
        {
            CremateDeadWorkers();
            // babies join the party
            m_data.RaiseBabies();
        }

        void CremateDeadWorkers() => m_data.CremateDead();
        void OnLevelGenCycleEnd() => Debug_LevelGenCycleEnd();
        #endregion

        void _UpdateVisibility(Vector3Int newPos)
        {
            var pool = SimplePool<List<IRoomInstance>>.I;
            // ReSharper disable once ConvertToUsingDeclaration
            using (var visibleScope = pool.GetScoped())
            using (var prevVisibleScope = pool.GetScoped())
            {
                var visibleList = visibleScope.Obj;
                var prevVisibleList = prevVisibleScope.Obj;

                prevVisibleList.AddRange(m_visibleRooms.Keys);

                var bounds = new Bounds(newPos + Vector3.up, Vector3.one);
                if (OctreeInstance.Rooms.IsFreePlaceOrGatherIntersecting(bounds, ref visibleList))
                    return;

                var heroRoom = visibleList[0];
                //if (heroRoom == Data.HeroRoom)
                //    return;
                m_visibleRooms.Clear();

                m_data.HeroRoom = heroRoom;
                //m_ceiling.SetSectionVisible(heroRoom.Bounds, true);
                m_data.HeroRoom.GatherConnecting(newPos.y, k_visibilitySteps, m_visibleRooms);

                foreach (var visRoom in m_visibleRooms.Keys)
                {
                    visRoom.Show();
                    visRoom.SetLevel(newPos.y - visRoom.Position.y);
                    //Debug.Log($"room visible: {visRoom}");
                    if (m_ceiling != null)
                        m_ceiling.SetSectionVisible(visRoom, true);
                }

                prevVisibleList.RemoveAll((r) => m_visibleRooms.ContainsKey(r));
                if (m_data.ShowAllRooms)
                    return;
                foreach (var prevRoom in prevVisibleList)
                {
                    prevRoom.Hide();
                    if (m_ceiling != null)
                        m_ceiling.SetSectionVisible(prevRoom.Bounds, false);
                }
            }
        }

        void _UpdateLevelCurrentLvl(int yPos)
        {
            //var pool = SimplePool<List<IRoomInstance>>.I;
            // ReSharper disable once ConvertToUsingDeclaration
            //using (var visibleListScope = pool.GetScoped())
            //{
            //var visibleList = visibleListScope.Obj;
            m_visibleRooms.Clear();
            m_data.HeroRoom.GatherConnecting(yPos, k_visibilitySteps, m_visibleRooms);

            foreach (var visRoom in m_visibleRooms.Keys)
            {
                visRoom.Get(out var bounds);
                //// TODO: BUG! gridPosBound did not change but the Hero-Level changed to next section, make sure gridPosBound works similar to HeroRoomGridPos?
                //if (visRoom.RoomComponent == null)
                //    continue;
                visRoom.SetLevel(yPos - visRoom.Position.y);
                //m_ceiling.SetSectionVisible(bounds, true);
            }
            //}
        }

        void _UpdateFocusArea()
        {
            var newHeroGridPos = _GridifyHeroPos();

            var gridPosChanged = !m_data.HeroRoomGridPos.Equals(newHeroGridPos);
            if (!gridPosChanged)
                return;

            if (m_ceiling != null)
                m_ceiling.SetPosition(newHeroGridPos);
            var newHeroGridPosBounds = GrowBoundsGridAligned(new Bounds(newHeroGridPos, m_generatingBoundsSize), GridSettings);

            DEBUG_NewHeroGridPos(newHeroGridPos, newHeroGridPosBounds);

            var gridPosBoundsChanged = !newHeroGridPosBounds.Equals(m_lastHeroGridPosBounds);
            // f.e. if hero only jumped
            if (!gridPosBoundsChanged)
            {
                Debug.Log("gridPosBoundsChanged didn't change");
                if (newHeroGridPos.y != m_data.HeroRoomGridPos.y)
                    _UpdateLevelCurrentLvl(newHeroGridPos.y);

                return;
            }

            _UpdateVisibility(newHeroGridPos);

            History_UpdateLastData();
            UpdateGridPos(newHeroGridPos, newHeroGridPosBounds);
            History_NewData(newHeroGridPosBounds, newHeroGridPos);

            m_data.DEBUG_AddDebugText("GridPos and Bounds Is Set");
        }

        void UpdateGridPos(Vector3Int newHeroGridPos, Bounds newHeroGridPosBounds)
        {
            m_lastHeroGridPos = newHeroGridPos;
            m_lastHeroGridPosBounds = newHeroGridPosBounds;

            m_data.SetCurrentGridBounds(newHeroGridPos, newHeroGridPosBounds);
        }

        Vector3Int _GridifyHeroPos()
        {
            var pos = m_hero.position;
            var posX = FloorToInt(pos.x / GridSettings.RoomChunkSize)
                * GridSettings.RoomChunkSize + (int)GridSettings.HalfRoomChunkSize;
            var posY = RoundToInt(pos.y - m_heroGroundOffset);
            var posZ = FloorToInt(pos.z / GridSettings.RoomChunkSize)
                * GridSettings.RoomChunkSize + (int)GridSettings.HalfRoomChunkSize;
            return new Vector3Int(posX, posY, posZ);
        }

        #if UNITY_EDITOR
        public void Editor_Verify(ref VerificationResult result)
        {
            if (m_hero == null) 
                result.Error("Hero Transform not set!", this);
            if (m_ceiling == null) 
                result.Warning("Ceiling not set!", this);
            if (m_roomSettings == null) 
                result.Warning("Room-Settings not set!", this);
            if (m_startingBiome == null) 
                result.Warning("Starting Biome not set!", this);
            if (m_loadingId == null) 
                result.Warning("Loading ID not set!", this);
            
            #if UNITASK
            if (m_stopwatchFrameSplitter == null) 
                result.Warning("Frame Splitter not set!", this);
            #endif
        }
                
        public bool Editor_IsInitVisible(IRoomInstance component) => m_initWithAllRoomsInScene;
        public bool Editor_InitWithAllRoomsInScene => m_initWithAllRoomsInScene;
        public RoomInstanceComponent[] Editor_InitialRooms => m_initialLoadedRooms;
        #endif
    }
}
#endif