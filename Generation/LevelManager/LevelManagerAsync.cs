#if UNITASK

using System;
using System.Threading;
using Core.Loading;
using Core.Unity.Utility;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Serialization;

namespace Level.Generation
{
    public partial class LevelManager
    {
        [SerializeField, FormerlySerializedAs("stopwatchFrameSplitter")] 
        StopwatchFrameSplitter m_stopwatchFrameSplitter;

        void StartAsync() => StartCoroutine(UniTask.ToCoroutine(UpdateLevel));

        async UniTask UpdateLevel() => await UpdateLevel(default);
        async UniTask UpdateLevel(CancellationToken cancellationToken)
        {
            LevelGeneratorStarted();

            // fix bug where inspector takes longer to init
            await UniTask.Delay(TimeSpan.FromSeconds(1), cancellationToken: cancellationToken);

            while (true)
            {
                OnNewLevelGenCycle();
                await UpdateWorkers(cancellationToken);
                if (DEBUG_StepNeedWait)
                    await DEBUG_Step();
                DEBUG_StepAllNext = false;

                UpdateWorkersLifeAndDeath();
                OnLevelGenCycleEnd();

                if (!m_data.WorkHasBeenDone && IsLoading)
                {            
                    var newHeroGridPos = _GridifyHeroPos();
                    _UpdateVisibility(newHeroGridPos);
                    LoadOperations.LoadingDone(m_loadingId);
                    IsLoading = false;
                }
                await UniTask.WaitForEndOfFrame();
            }

            // ReSharper disable once IteratorNeverReturns
        }

        async UniTask UpdateWorkers(CancellationToken cancellationToken)
        {
            foreach (var w in m_data.Worker)
            {
                if (w.CanOperate(m_data, out var inFocusArea))
                {
                    DEBUG_Pause |= DEBUG_NextWorkerNeedWait(w);
                    //Debug.Log("pause "+DEBUG_Pause+ " DEBUG_StepNeedWait: "+ DEBUG_StepNeedWait);
                    if (DEBUG_StepNeedWait)
                        await DEBUG_Step();
                    m_data.DEBUG_AddDebugText(" Op ");

                    await w.Operate(m_data, cancellationToken);
                    await m_stopwatchFrameSplitter.SplitFrame(cancellationToken);
                }

                if (w.Dead)
                    m_data.DyingCount++;

                if (!inFocusArea)
                    continue;

                DEBUG_AfterOneWorkerStep();
            }
        }

        async UniTask DEBUG_Step()
        {
            while (DEBUG_StepNeedWait)
                await UniTask.WaitForEndOfFrame();
        }
    }
}
#endif