#if !UNITASK

using System.Collections;
using UnityEngine;

namespace Level.Generation
{
    public partial class LevelManager
    {
        void StartAsync() => StartCoroutine(UpdateLevel());

        IEnumerator UpdateLevel()
        {
            LevelGeneratorStarted();

            // fix bug where inspector takes longer to init
            yield return new WaitForSeconds(1f);

            while (true)
            {
                OnNewLevelGenCycle();
                yield return StartCoroutine(UpdateWorkers());
                if (DEBUG_StepNeedWait)
                    yield return StartCoroutine(DEBUG_Step());
                DEBUG_StepAllNext = false;

                UpdateWorkersLifeAndDeath();
                OnLevelGenCycleEnd();

                yield return new WaitForEndOfFrame();
            }

            // ReSharper disable once IteratorNeverReturns
        }

        IEnumerator UpdateWorkers()
        {
            foreach (var w in m_data.Worker)
            {
                if (w.CanOperate(m_data, out var inFocusArea))
                {
                    DEBUG_Pause |= DEBUG_NextWorkerNeedWait(w);
                    //Debug.Log("pause "+DEBUG_Pause+ " DEBUG_StepNeedWait: "+ DEBUG_StepNeedWait);
                    if (DEBUG_StepNeedWait)
                        yield return StartCoroutine(DEBUG_Step());
                    
                    m_data.DEBUG_AddDebugText(" Op ");
                    w.Operate(m_data);
                    //stopwatchFrameSplitter.SplitFrame();
                }

                if (w.Dead)
                    m_data.DyingCount++;

                if (!inFocusArea)
                    continue;

                DEBUG_AfterOneWorkerStep();
            }
        }

        IEnumerator DEBUG_Step()
        {
            while (DEBUG_StepNeedWait)
                yield return new WaitForEndOfFrame();
        }
    }
}
#endif