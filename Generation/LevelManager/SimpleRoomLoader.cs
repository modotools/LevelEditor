using System.Collections.Generic;
using UnityEngine;
using Core.Interface;
using Core.Loading;
using Core.Types;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using Level.Room;
using Level.Generation.Operations;

namespace Level.Generation
{
    /// <summary>
    /// Manages initially loaded Rooms
    /// </summary>
    [EditorIcon("workflow")]
    public class SimpleRoomLoader : DebuggableBehaviour, ILevelManager,
        IHierarchyIcon, IVerify
    {
        public string HierarchyIcon => "Icons/workflow";
        
        [SerializeField] RoomSettings m_roomSettings;

        [SerializeField] Transform m_hero;
        
        [SerializeField] bool m_initWithAllRoomsInScene;
        [SerializeField] RoomInstanceComponent[] m_initialLoadedRooms;

        [SerializeField, SelectAssetByName("LevelGenerator")]
        LoadOperationID m_loadingId;

        #region Unity Calls
        void Start() => Init();
        #endregion

        #region Init
        void Init()
        {
            // todo: async
            // IsLoading = true;
            // LoadOperations.StartLoading(m_loadingId);

            if (!OctreeInstance.TryInitOctreesFromGameObject(gameObject))
                return;
            
            using var initialRoomsScoped = SimplePool<List<IRoomInstance>>.I.GetScoped();
            initialRoomsScoped.Obj.AddRange(m_initWithAllRoomsInScene
                ? OctreeInstance.Rooms.GetAllObjects()
                : m_initialLoadedRooms);

            foreach (var room in initialRoomsScoped.Obj)
                InitRoom(room);
        }

        static void InitRoom(IRoomInstance initialRoom)
        {
            if (initialRoom is RoomInstanceComponent ric)
                ric.UpdatePosition();
            initialRoom.AddOpenExitsFromTiles();
            initialRoom.Show(); // startRoom.Room.ShowRoom(Data.MeshOperationData);
        }

        #endregion

        #if UNITY_EDITOR
        public bool Editor_IsInitVisible(IRoomInstance component) => m_initWithAllRoomsInScene;
        public bool Editor_InitWithAllRoomsInScene => m_initWithAllRoomsInScene;
        public RoomInstanceComponent[] Editor_InitialRooms => m_initialLoadedRooms;
        
        public void Editor_Verify(ref VerificationResult result)
        {
            if (m_hero == null) 
                result.Error("Hero Transform not set!", this);
            if (m_roomSettings == null) 
                result.Warning("Room-Settings not set!", this);
            if (m_loadingId == null) 
                result.Warning("Loading ID not set!", this);
        }
        #endif
    }
}