using Level.Procedural;
using Level.Room;
using LevelEditor;
using UnityEngine;

namespace Level.Generation
{
    public interface IWorkerAreaFactory : ILevelGeneratorWorkerFactory
    {
        public abstract ILevelGeneratorWorker SpawnWorker(IGeneratorData data, IRoomInstance spawnerRoom, 
            ExitInfo atExit, LayoutWorkerData gridData);
        
        public abstract ILevelGeneratorWorker SpawnWorker(IGeneratorData data, Bounds bounds, LayoutWorkerData gridData);
    }
    
    public struct LayoutWorkerData
    {
        public LayoutConfig LayoutConfig;
        public Vector2Int GridPos;
    }
}