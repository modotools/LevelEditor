using UnityEngine;

using System.Collections.Generic;
using Core.Interface;
using Core.Types;
using Core.Unity.Attributes;
using Core.Unity.Extensions;

namespace Level.Generation
{
    /// <summary> Responsible for creating Ceiling Mesh for invisible sections. </summary>
    [EditorIcon("ceiling")]
    public class CeilingGrid : MonoBehaviour
    {
        [SerializeField] GridSettings m_gridSettings;

        int RoomChunkSize => m_gridSettings.RoomChunkSize;

        /// <summary> Size of Grid </summary>
        public Vector2Int Size;

        Transform m_transform;
        MeshFilter m_filter;

        bool[] m_grid;
        bool m_gridDirty;

        Vector3Int m_worldHalfSize;
        Vector3 m_offset;
        Vector3 m_pos;

        void Awake()
        {
            m_transform = transform;
            m_filter = GetComponent<MeshFilter>();

            m_grid = new bool[Size.x * Size.y];
            m_filter.mesh = new Mesh();

            m_worldHalfSize = (Size.Vector3() / 2.0f).Vector3Int() * m_gridSettings.RoomChunkSize;
            m_offset = new Vector3(-m_gridSettings.HalfRoomChunkSize, 0, -m_gridSettings.HalfRoomChunkSize);
        }

        void OnDestroy()
        {
            Destroy(m_filter.mesh);
            m_filter.mesh = null;
        }

        void Update()
        {
            if (!m_gridDirty)
                return;

            var mesh = m_filter.sharedMesh;
            mesh.Clear();

            using (var v3Scope = SimplePool<List<Vector3>>.I.GetScoped())
            using (var v2Scope = SimplePool<List<Vector2>>.I.GetScoped())
            using (var iScope = SimplePool<List<int>>.I.GetScoped())
            {
                var vertices = v3Scope.Obj;
                var uvs = v2Scope.Obj;
                var newTriangles = iScope.Obj;

                var i = 0;
                for (var x = 0; x < Size.x; ++x)
                {
                    for (var z = 0; z < Size.y; ++z)
                    {
                        if (m_grid[(z * Size.x) + x])
                            continue;

                        vertices.Add(new Vector3(x * RoomChunkSize, 2, z * RoomChunkSize));
                        vertices.Add(new Vector3((x + 1) * RoomChunkSize, 2, z * RoomChunkSize));
                        vertices.Add(new Vector3(x * RoomChunkSize, 2, (z + 1) * RoomChunkSize));
                        vertices.Add(new Vector3((x + 1) * RoomChunkSize, 2, (z + 1) * RoomChunkSize));

                        uvs.Add(Vector2.zero);
                        uvs.Add(Vector2.right);
                        uvs.Add(Vector2.up);
                        uvs.Add(Vector2.up + Vector2.right);

                        newTriangles.Add((i * 4) + 0);
                        newTriangles.Add((i * 4) + 2);
                        newTriangles.Add((i * 4) + 1);
                        newTriangles.Add((i * 4) + 1);
                        newTriangles.Add((i * 4) + 2);
                        newTriangles.Add((i * 4) + 3);

                        ++i;
                    }
                }

                mesh.vertices = vertices.ToArray();
                mesh.uv = uvs.ToArray();
                mesh.triangles = newTriangles.ToArray();
                mesh.RecalculateNormals();
                m_transform.position = m_pos;

                m_gridDirty = false;
            }
        }


        /// <summary> SetPosition of the grid </summary>
        public void SetPosition(Vector3 pos)
        {
            var minPos = pos - m_worldHalfSize + m_offset;
            var movement = minPos - m_pos;

            //var shift = new Vector3(movement.x / Constants.RoomGridSize, movement.y, movement.z/Constants.RoomGridSize).Vector3Int();
            //if (shift.Equals(Vector3Int.zero))
            //    return;

            m_pos = minPos;
            //Debug.Log($"CeilingGrid SetPosition {pos} m_worldHalfSize {m_worldHalfSize} m_offset {m_offset} m_pos: {minPos} {movement}");

            //System.Array.Copy(Grid, GridCopy, Grid.Length);
            for (var x = 0; x < Size.x; ++x)
            {
                for (var z = 0; z < Size.y; ++z)
                {
                    m_grid[(z * Size.x) + x] = false;
                    //        var shiftX = x + shift.x;
                    //        var shiftZ = z + shift.z;
                    //        if (shiftZ < 0 || shiftZ >= Size.y || shiftX < 0 || shiftX >= Size.x)
                    //            continue;
                    //        Grid[(z * Size.x) + x] = GridCopy[(shiftZ * Size.x) + shiftX];
                }
            }
            m_gridDirty = true;
        }

        public void SetSectionVisible(IProvider<Bounds> bProv, bool visible)
        {
            bProv.Get(out var b);
            SetSectionVisible(b, visible);
        }

        /// <summary> SetSectionVisible </summary>
        public void SetSectionVisible(Bounds b, bool visible)
        {
            if (b.min.y > m_pos.y || b.max.y <= m_pos.y)
                return;

            var bMin = b.min / RoomChunkSize;
            var bMax = b.size / RoomChunkSize;
            var pos = m_pos / RoomChunkSize;
            var min = bMin - pos;
            var minX = (int) Mathf.Clamp(min.x, 0, Size.x);
            var minZ = (int) Mathf.Clamp(min.z, 0, Size.y);
            var maxX = (int) Mathf.Clamp(min.x + bMax.x, 0, Size.x);
            var maxZ = (int) Mathf.Clamp(min.z + bMax.z, 0, Size.y);
            //Debug.Log($"SetSectionVisible b {b} RoomChunkSize {RoomChunkSize} " +
            //          $"bMin {bMin}, bMax {bMax}, pos {pos} min {min} " +
            //          $"minX {minX} minZ {minZ} maxX {maxX} maxZ {maxZ}");

            for (var x = minX; x < maxX; ++x)
            for (var z = minZ; z < maxZ; ++z)
            {
                //Debug.Log($"SetSectionVisible x {x}, z {z}, visible {visible}");
                m_grid[z * Size.x + x] = visible;
            }
            m_gridDirty = true;
        }
    }
}
