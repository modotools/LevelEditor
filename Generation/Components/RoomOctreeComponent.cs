using Core.Extensions;
using Core.Interface;
using Core.Unity.Attributes;
using UnityEngine;
using Core.Unity.Types;
using Level.Room;

namespace Level.Generation
{
    [EditorIcon("workflow")]
    public class RoomOctreeComponent : DebuggableBehaviour, IProvider<Octree<IRoomInstance>>, IHierarchyIcon
    {
        public string HierarchyIcon => "Icons/workflow";
        
        [SerializeField] float m_octTreeSize = 2048;
        [SerializeField] Color m_debugDrawColor;

        Octree<IRoomInstance> m_rooms;
        public void Get(out Octree<IRoomInstance> provided) => provided = m_rooms;

        void Awake() => InitOctree();

        public void InitOctree()
        {
            m_rooms = new Octree<IRoomInstance>(Vector3.zero, m_octTreeSize);
            OctreeInstance.Rooms = m_rooms;
            InitWithAllRoomsFromScene();
        }

        void InitWithAllRoomsFromScene()
        {
            var rooms = FindObjectsOfType<RoomInstanceComponent>();
            foreach (var r in rooms) 
                m_rooms.AddObject(r);
        }

        void OnDrawGizmos()
        {
            if (m_rooms == null)
                InitOctree();

            m_rooms.DrawBoxLines(m_debugDrawColor);
        }

    }
}