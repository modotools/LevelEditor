using System.ComponentModel;
using System.Linq;
using Core.Extensions;
using Core.Interface;
using Core.Unity.Attributes;
using UnityEngine;
using Core.Unity.Types;
using Level.Room;

namespace Level.Generation
{
    [EditorIcon("workflow")]
    public class WorkerAreaOctreeComponent : DebuggableBehaviour, IProvider<Octree<IWorkerArea>>, IHierarchyIcon
    {
        public string HierarchyIcon => "Icons/workflow";
        
        [SerializeField] float m_octTreeSize = 2048;
        [SerializeField] Color m_debugDrawColor;

        Octree<IWorkerArea> m_workerArea;
        public void Get(out Octree<IWorkerArea> provided) => provided = m_workerArea;

        void Awake() => InitOctree();

        public void InitOctree()
        {
            m_workerArea = new Octree<IWorkerArea>(Vector3.zero, m_octTreeSize);
            OctreeInstance.WorkerArea = m_workerArea;
            InitWithAllRoomsFromScene();
        }

        void InitWithAllRoomsFromScene()
        {
            var workerAreas = FindObjectsOfType<MonoBehaviour>().OfType<IWorkerArea>();
            foreach (var wa in workerAreas) 
                m_workerArea.AddObject(wa);
        }

        void OnDrawGizmos()
        {
            if (m_workerArea == null)
                InitOctree();

            m_workerArea.DrawBoxLines(m_debugDrawColor);
        }

    }
}