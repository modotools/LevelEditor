using Core;
using Core.Unity.Attributes;
using Level.Generation.Operations;
using Level.Room;
using UnityEngine;

namespace Level.Generation
{
    [EditorIcon("workerarea")]
    public class WorkerArea : MonoBehaviour, IWorkerArea, IHierarchyIcon
    {
        public string HierarchyIcon => "Icons/workerarea";
        [SerializeField] RefIFactoryProvider m_factoryProvider;
        [SerializeField] GridSettings m_gridSettings;
        [SerializeField] Vector3Int m_size;
        [SerializeField] Color m_color;
        Vector3 m_lastPos;

        public void Get(out GridSettings provided) => provided = m_gridSettings;
        
        public void Get(out Bounds provided) => this.GetBounds(m_size, out provided);

        public bool SpawnAllWorker(IGeneratorData data, ref DebugString errorString)
        {
            Debug.Log("WorkerArea ignores SpawnAllWorker");
            // spawns workers via other rooms
            return false;
        }

        public bool TrySpawnWorker(IGeneratorData data, IRoomInstance fromRoom, Bounds minOpBounds, ExitInfo exitInfo,
            IFactoryProvider factoryConfig, out ILevelGeneratorWorker worker, ref DebugString errorString)
        {
            worker = null;
            var factory = m_factoryProvider.Result.GetFactory(data);
            if (factory == null)
            {
                errorString.AddError("No Factory!");
                return false;
            }
            
            worker = factory.SpawnWorker(data, fromRoom, exitInfo);
            data.AddWorker(worker, fromRoom.Bounds.center);
            return true;
        }

        public ILevelGeneratorWorkerFactory GetWorkerFactory(IGeneratorData data, Bounds opBounds) 
            => m_factoryProvider.Result?.GetFactory(data);

        void OnDrawGizmos() => this.OnDrawGizmos(ref m_lastPos, m_color);
    }
}
