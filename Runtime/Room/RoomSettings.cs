﻿using Core.Unity.Interface;
using Level.Tiles;
using Level.Tiles.Interface;
using UnityEngine;

using Core.Unity.Attributes;
using Core.Unity.Utility.PoolAttendant;
using Level.Enums;
using ScriptableUtility;

namespace Level.Room
{
    [CreateAssetMenu(fileName = nameof(RoomSettings), menuName = "Level/Room/"+nameof(RoomSettings))]
    public class RoomSettings : ScriptableObject, IScriptableObject, IVerify
    {
        public GameObject RoomPrefab;
        public GameObject PlatformPrefab;
        public GameObject PlatformVisualPrefab;

        public GridSettings Grid;

        public GeometryIdentifierConfig GeometryIdentifierConfig;

        public TilesSetListConfig TileSet;
        public TilesSetListConfig IntermediateTileSet;

        public RefITileConfig LevelMaterialTypeConfig;

        public RoomBuilder Builder;

        [Layer]
        public int GroundLayer;

        // todo: remove this here, should be applied from builder
        public Material CeilingMaterial;

        public GameObject GetRoomGameObject()
        {
            if (RoomPrefab == null || !Application.isPlaying)
            {
                var go = new GameObject(name, 
                    typeof(ContextComponent),
                    typeof(RoomInstanceComponent), 
                    typeof(RoomObjectsDataComponent));
                return go;
            }

            var pooled = RoomPrefab.GetPooledInstance();
            pooled.TryGetComponent(out PoolEntity pe);
            pe.DespawnOnDisable = false;
            return pooled;
        }

        public TilesSetFilter GetFilter(GeometryType geoType)
        {
            var groundTile = GeometryIdentifierConfig.Get(geoType);
            return TileTypeOperations.GetFilter(TileSet, groundTile);
        }

        #if UNITY_EDITOR
        public void Editor_Verify(ref VerificationResult result)
        {
            if (Grid == null)
                result.Error($"{nameof(Grid)} is not set", this);
            if (TileSet == null)
                result.Error($"{nameof(TileSet)} is not set", this);
            if (IntermediateTileSet == null)
                result.Error($"{nameof(IntermediateTileSet)} is not set", this);
            if (LevelMaterialTypeConfig == null)
                result.Error($"{nameof(LevelMaterialTypeConfig)} is not set", this);
            if (Builder == null)
                result.Error($"{nameof(Builder)} is not set", this);
            if (CeilingMaterial == null)
                result.Error($"{nameof(CeilingMaterial)} is not set", this);
        }
        #endif
    }
}
