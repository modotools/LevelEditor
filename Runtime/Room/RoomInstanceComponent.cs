﻿using System.Collections.Generic;
using Core.Interface;
using Core.Unity.Attributes;
using Level.Enums;
using Level.PlatformLayer.Interface;
using Level.Room.Operations;
using ScriptableUtility;
using UnityEngine;

#if UNITASK
using System.Threading;
using Cysharp.Threading.Tasks;
#endif

namespace Level.Room
{
    [EditorIcon("room")]
    public class RoomInstanceComponent : MonoBehaviour, IPoolable,
        IRoomInstance, IProvider<IContext>, IHierarchyIcon
    {
        
        #region Serialized Fields
        [SerializeField] RoomInstanceData m_roomInstanceData;

        // todo: set via editor
        [SerializeField] bool m_initVisibleOnStart;
        [SerializeField] bool m_updatePositionOnUpdate;
        #endregion

        public string HierarchyIcon => "Icons/room";
        #region RoomInstanceData Properties
        public ref RoomInstanceData Data => ref m_roomInstanceData;
        bool IsValid => Data.RoomConfig != null;

        public IEnumerable<IPlatformLayer> PlatformLayer => Data.PlatformLayer;
        public int MaxPlatformCount => Data.MaxPlatformCount;
        public RoomSettings RoomSettings => Data.RoomSettings;
        public Vector3Int Size => Data.Size;
        public Vector2Int TileDim => Data.TileDim;
        public int PlatformCount => Data.PlatformCount;
        public IPlatformLayer GetPlatformLayer(int idx) => Data.GetPlatformLayer(idx);
        public List<ExitInfo> OpenExits => Data.OpenExits;
        public List<ExitInfo> ClosedExits => Data.ClosedExits;
        public Transform PrefabParent => Data.PrefabParent;
        public RoomConfig Config => Data.RoomConfig;

        public List<ExitInfo> ExitRestrictions => Data.ExitRestrictions;
        public List<ExitConfiguration> ValidExitConfiguration => Data.ValidExitConfiguration;
        public List<int> ValidCardinalMap => Data.ValidCardinalMap;
        public RestrictionMode RestrictionModeEntry => Data.RestrictionModeEntry;
        public RestrictionMode RestrictionModeExit => Data.RestrictionModeExit;
        public bool AllowRotate  => Data.AllowRotate;
        public bool AllowMirror  => Data.AllowMirror;

        public Vector3Int Position => Data.Position;
        public Bounds Bounds => Data.Bounds;
        public bool IsBuilt { get; private set; }

        #endregion

        IContext m_context;
        Vector3 m_lastPos;

        #region Unity Calls
        void Awake()
        {
            if (IsValid)
                m_roomInstanceData.InitLayer();
        }

        void Start()
        {
            if (!m_initVisibleOnStart) 
                return;

            InitVisible();
        }

        void Update()
        {
            if (m_updatePositionOnUpdate)
                UpdatePosition();
        }

//         void OnDrawGizmos()
//         {
// #if UNITY_EDITOR
//             foreach (var openExit in OpenExits)
//                 ExitOperations.Editor_DrawExit(this, openExit);
// #endif
//         }

        #endregion

        #region Data Init
        public void SetData(RoomInstanceData data)
        {
            m_roomInstanceData = data;
            m_roomInstanceData.UpdateRoomPosition(transform.position);
            m_roomInstanceData.ShowRoom();
        }
        #endregion

        #region IProvider<IContext>
        public IContext Context
        {
            get
            {
                if (m_context == null)
                    TryGetComponent(out m_context);
                return m_context;
            }
        }

        public void Get(out IContext provided) => provided = Context;
        #endregion

        #region Room Build
        public IDefaultAction BuildAction { get; private set; }

        public RoomBuilder Builder =>
            m_roomInstanceData.RoomConfig == null 
                ? null 
                : m_roomInstanceData.RoomConfig.Builder;

        public void SetBuildAction(IDefaultAction action) => BuildAction = action;

        void InitRoomInstance()
        {
            m_roomInstanceData.InitLayer();
            m_roomInstanceData.UpdateVisuals();
        }

        public void InitVisible()
        {
            InitRoomInstance();
            IsBuilt = true;
            this.Build();
            // todo:
            SetLevel(0);

            // todo: think about how when where to spawn objects
            if (TryGetComponent(out RoomObjectsDataComponent roomObjectsDataComponent))
                roomObjectsDataComponent.InitObjects();
        }
        #endregion

        #region public methods

#if UNITASK
        public async UniTask AsyncBuild(CancellationToken cancellationToken)
        {
            if (IsBuilt)
                return;

            IsBuilt = true;
            gameObject.SetActive(true);
            InitRoomInstance();

            await RoomOperations.AsyncBuild(this, cancellationToken);
        }
#endif

        // todo: pool?
        public void Show()
        {
            gameObject.SetActive(true);
            if (!IsBuilt)
                InitVisible();
        }

        public void Hide() => gameObject.SetActive(false);

        public void SetPlatformLevels(List<IPlatformLayer> newLevels) => Data.SetLayer(newLevels);

        public void UpdatePosition()
        {
            var currentPos = transform.position;
            if (m_lastPos == currentPos) 
                return;
            m_lastPos = currentPos;
            
            m_roomInstanceData.UpdateRoomPosition(currentPos);
        }
        
        public void Get(out Bounds provided) => provided = Bounds;

        // todo:
        const int k_visibleLevels = 10;
        public void SetLevel(int positionY) => m_roomInstanceData.SetLevel(0, positionY+k_visibleLevels);
        #endregion

        public void OnSpawn()
        {
        }

        public void OnDespawn()
        {
            // todo: test
            Debug.Log("Despawn!");
            transform.DetachChildren();
        }

#if UNITY_EDITOR
        public bool Editor_InitVisibleOnStart
        {
            get => m_initVisibleOnStart;
            set => m_initVisibleOnStart = value;
        }
#endif

    }
}
