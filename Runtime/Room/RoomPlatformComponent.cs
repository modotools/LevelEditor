﻿using System;
using Core.Unity.Attributes;
using UnityEngine;

namespace Level.Room
{
    [EditorIcon("platform")]
    public class RoomPlatformComponent : MonoBehaviour, IHierarchyIcon
    {
        public string HierarchyIcon => "Icons/platform";

        void Update()
        {
        }
    }
}
