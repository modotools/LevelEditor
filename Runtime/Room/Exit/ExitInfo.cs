
using Core.Types;

namespace Level.Room
{
    [System.Serializable]
    public class ExitInfo
    {
        public IRoomInstance ConnectFrom;
        public IRoomInstance ConnectedTo;

        public Cardinals ExitDirection;
        public ExitRestrictionTypeID Restriction;
        public int FromIdx;
        public int ToIdx;
        public int Level;
        
        public bool PossibleEntry;
    }
}