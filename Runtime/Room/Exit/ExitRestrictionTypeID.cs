using System;
using Core.Unity.Types.ID;

namespace Level.Room
{
    [Serializable]
    public class ExitRestrictionTypeID : IDAsset
    {
        public RestrictionType Type;
    }

    public enum RestrictionType
    {
        None = 0,
        AlwaysBlocked,
        Condition
    }
}