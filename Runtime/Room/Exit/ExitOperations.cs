#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
#endif
using Core.Types;
using Core.Unity.Extensions;

namespace Level.Room
{
    public static class ExitOperations
    {
#if UNITY_EDITOR
        public static void Editor_DrawExit(IRoomInstance roomInstance, ExitInfo exit)
        {
            float arrPosX = 0.0f, arrPosZ = 0.0f;
            var arrDir = exit.ExitDirection.ToVector3();
            DrawExits_CornerFix(roomInstance, exit, out var from, out var to);
            var exitRange = (to - @from);
            var halfArrowSize = 0.5f * exitRange;
            var centerPos = @from + halfArrowSize;
            float xInf = 0.0f, zInf = 0.0f; // influence

            switch (exit.ExitDirection)
            {
                case Cardinals.North:
                    arrPosX = centerPos;
                    arrPosZ = roomInstance.Size.z;
                    xInf = 1.0f;
                    break;
                case Cardinals.East:
                    arrPosX = roomInstance.Size.x;
                    arrPosZ = centerPos;
                    zInf = 1.0f;
                    break;
                case Cardinals.South:
                    arrPosX = centerPos;
                    arrPosZ = 0;
                    xInf = 1.0f;
                    break;
                case Cardinals.West:
                    arrPosX = 0;
                    arrPosZ = centerPos;
                    zInf = 1.0f;
                    break;
            }

            var arrPos = roomInstance.Position + new Vector3(arrPosX, exit.Level, arrPosZ);
            var halfVec = new Vector3(xInf * halfArrowSize, 0.0f, zInf * halfArrowSize);

            var restrictionType = exit.Restriction == null ? RestrictionType.None : exit.Restriction.Type;
            var lerp = restrictionType switch
            {
                RestrictionType.None => 0f,
                RestrictionType.AlwaysBlocked => 1f,
                RestrictionType.Condition => 0.5f,
                _ => 0f
            };

            var shapeOffset = Vector3.Lerp(Vector3.zero, arrDir, lerp);
            Handles.DrawLine(arrPos, arrPos + arrDir);
            Handles.DrawLine(arrPos - halfVec + shapeOffset, arrPos + arrDir);
            Handles.DrawLine(arrPos + halfVec + shapeOffset, arrPos + arrDir);
        }
        
        static void DrawExits_CornerFix(IRoomInstance roomInstance, ExitInfo exit, out float from, out float to)
        {
            var lvl = roomInstance.GetPlatformLayer(exit.Level);
            var useXAxis = exit.ExitDirection == Cardinals.North || exit.ExitDirection == Cardinals.South;
            var addToIdx = useXAxis ? lvl.Position.x : lvl.Position.y;
            var platformSize = useXAxis ? lvl.TileDim.x : lvl.TileDim.y;
            var fromIdxRoom = (exit.FromIdx + addToIdx);
            var toIdxRoom = (exit.ToIdx + addToIdx);

            var fromIsStartOfRoom = exit.FromIdx == 0;
            var fromIsCorner = fromIdxRoom % roomInstance.RoomSettings.Grid.RoomChunkSize == 0;
            var toIsCorner = toIdxRoom % roomInstance.RoomSettings.Grid.RoomChunkSize == 0;
            var toIsEndOfPlatform = exit.ToIdx == platformSize - 1;

            var exitRange = (exit.ToIdx - exit.FromIdx);
            var smallExit = exitRange < 1f;

            from = fromIsStartOfRoom || (fromIsCorner && !smallExit) ? exit.FromIdx: exit.FromIdx - 0.5f;
            to = toIsEndOfPlatform || (toIsCorner && !smallExit) ? exit.ToIdx : exit.ToIdx + 0.5f;
        }
#endif
    }
}