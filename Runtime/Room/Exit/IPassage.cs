
using Core.Types;

namespace Level.Room
{
    public interface IPassage
    {
        ExitInfo Exit { get; }
        float Open { get; }
        
        // todo: On spawning (and readjusting pos) for passage-object grab room and Exit,
        // check which rooms are visited by player/ which rooms are concealed
        // LevelManager is iterating visible rooms, should be connected to that logic,
        // probably best put to this to ExitInfo to stop iterating visible rooms
        // when revealing the room objects can react to it float 0-1, eg. LightIntensity
    }
}