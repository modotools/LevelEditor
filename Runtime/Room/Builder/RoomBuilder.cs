﻿using System.Collections.Generic;
using System.Linq;
using Core.Interface;
using Core.Types;
using Core.Unity.Interface;
using Core.Unity.Types.ID;
using Level.Data;
using Level.PlatformLayer.Interface;
using Level.Tiles;
using Level.Tiles.Interface;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.Graph;
using UnityEngine;

namespace Level.Room
{
    [CreateAssetMenu(fileName = nameof(RoomBuilder), menuName = "Level/Room/"+nameof(RoomBuilder)), 
     RequireNode(typeof(ActionChannelEntryNode))]
    public class RoomBuilder : NodeGraph, IProvider<IContext>, IVariableContainer, IParentMarker
        , IEntryNodeGraph<ActionChannelEntryNode>
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] ScriptableContainer m_container;

        [SerializeField] IDAsset m_buildRoom;
        [SerializeField] IDAsset m_buildPlatform;
#pragma warning restore 0649 // wrong warnings for SerializeField

        #region Input Variables
        public VarReference<IRoom> Room;
        public VarReference<IPlatformLayer> Platform;
        public VarReference<HardEdges> Edges;

        public VarReference<TilesSetListConfig> TilesSetListConfig;
        public VarReference<TilesSetListConfig> IntermediateTilesSetListConfig;

        public VarReference<ITileConfig> LevelMaterialTypeConfig;
        #endregion
        
        public Material Material;
        
        IContext m_context;

        #region Properties
        IEnumerable<IActionConfig> Actions =>
            NodeObjs.OfType<INode>().Select(n => n.NodeData as IActionConfig).Where(a => a != null);
        
        public IEnumerable<IScriptableVariable> Variables => m_container == null ? null : m_container.Variables;
        public IScriptableObject Parent => this;

        public INode EntryNode => Entry;
        public INodeData EntryNodeData => Entry;
        public ActionChannelEntryNode Entry 
            => NodeObjs.FirstOrDefault(n => n is ActionChannelEntryNode) as ActionChannelEntryNode;
        public string Name => name;

        #endregion
        
        public IDefaultAction CreateRoomBuilderAction()
        {
            using var dict = SimplePool<Dictionary<IActionConfig, IBaseAction>>.I.GetScoped();
            CreateActions(dict.Obj);
            LinkActions(dict.Obj);

            return dict.Obj[Entry.GetAction(m_buildRoom)] as IDefaultAction;
        }

        public IDefaultAction CreatePlatformBuilderAction()
        {
            using var dict = SimplePool<Dictionary<IActionConfig, IBaseAction>>.I.GetScoped();
            CreateActions(dict.Obj);
            LinkActions(dict.Obj);

            return dict.Obj[Entry.GetAction(m_buildPlatform)] as IDefaultAction;
        }

        void LinkActions(IDictionary<IActionConfig, IBaseAction> dict)
        {
            foreach (var action in Actions)
            {
                if (!(action is ILinkActionConfigs link))
                    continue;

                //Debug.Log($"Linking {action.Name} {action.GetInstanceID()}");
                link.LinkActions(dict);
            }
        }

        void CreateActions(IDictionary<IActionConfig, IBaseAction> dict)
        {
            foreach (var action in Actions)
            {
                Debug.Assert(action != null);
                var baseAction = action.CreateAction();
                //Debug.Log($"Creating {action.Name} {action.GetInstanceID()} {baseAction.GetHashCode()}");
                dict.Add(action, baseAction);
            }
        }

        public void Get(out IContext provided) => provided = m_context;
        public void SetContext(IContext context) => m_context = context;

        public override IEnumerable<ScriptableObject> Dependencies
        {
            get
            {
                foreach (var d in base.Dependencies)
                    yield return d;
                yield return m_container;
            }
        }

    }
}
