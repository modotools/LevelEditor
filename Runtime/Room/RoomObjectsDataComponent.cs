﻿using System.Threading;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Operations;
using Core.Unity.Types;
using Core.Unity.Utility;
using Cysharp.Threading.Tasks;
using Level.Objects;
using UnityEngine;

#if UNITY_EDITOR
using Level.Room.Operations;
using UnityEditor;
#endif

using static Level.Objects.ObjectContextMarker;

namespace Level.Room
{
    /// <summary>
    /// Helps instantiate the level-objects in editor, because they are not saved in scene
    /// and in game when they are needed (depends on Conditions)
    /// </summary>
    public class RoomObjectsDataComponent : MonoBehaviour, IComponent
    {
        public LevelObjectsConfig LvlObjectsConfigScene;
        public LevelObjectsConfig LvlObjectsConfigRoom;

        public RoomObjectMapper MapperScene;
        public RoomObjectMapper MapperRoom;
        
        IRoomInstance m_roomInstance;
        InitState m_initState;

        enum InitState
        {
            NotInitialized,
            Initializing,
            Initialized
        }

        void Start()
        {
            if (!Application.isPlaying)
                return;

            // todo do later?:
            // InitObjects();
        }

        public async UniTask InitObjects(StopwatchFrameSplitter splitter = null,
            CancellationToken cancellationToken = default)
        {
            if (m_initState != InitState.NotInitialized)
                return;
            m_initState = InitState.Initializing;

            var sceneGo = SceneRootOperations.GetSceneRoot(gameObject.scene, out _);
            var roomGo = gameObject;

            IExposedPropertyTable sceneResolver = null;
            if (sceneGo != null) 
                sceneGo.TryGet(out sceneResolver);

            var roomResolver = LvlObjectsConfigRoom.GetOrInstantiateMapper(gameObject, ref MapperRoom);
            if (roomResolver != null)
            {
                foreach (var child in roomResolver.transform.Children()) 
                    child.gameObject.SetActive(false);
            }
            else Debug.LogError("No Room Resolver!");

            gameObject.TryGet(out m_roomInstance);
            Debug.Assert(m_roomInstance != null);

            if (ConfigExists(LvlObjectsConfigScene, "Scene") && sceneResolver != null)
                await InitObjects(LvlObjectsConfigScene, sceneResolver, sceneGo, roomGo, sceneGo, splitter, cancellationToken);
            if (ConfigExists(LvlObjectsConfigRoom, "Room"))
                await InitObjects(LvlObjectsConfigRoom, roomResolver, sceneGo, roomGo, roomGo, splitter, cancellationToken);
 
            m_initState = InitState.Initialized;
        }

        async UniTask InitObjects(LevelObjectsConfig config, IExposedPropertyTable resolver,
            Object sceneGo, Object roomGo, GameObject parentGo, 
            StopwatchFrameSplitter splitter = null,
            CancellationToken cancellationToken = default)
        {
            // set scene context
            if (config.SceneContext != null)
                config.SceneContext.SetContextLink(sceneGo);
            // set room context
            if (config.ContextAsset != null)
                config.ContextAsset.SetContextLink(roomGo);

            var inactiveObjects = parentGo.transform.Find("Inactive");
            if (inactiveObjects == null)
            {
                var inactiveGo = new GameObject("Inactive");
                inactiveObjects = inactiveGo.transform;
                inactiveObjects.SetParent(parentGo.transform);
                inactiveGo.SetActive(false);
            }

            var lvlObjectRuntimeData = new LevelObjectRuntimeData();
            await lvlObjectRuntimeData.Init(parentGo, inactiveObjects, config, resolver, m_roomInstance, splitter, cancellationToken);
        }

        bool ConfigExists(LevelObjectsConfig config, string context)
        {
            if (config == null)
            {
                //Debug.LogError($"LevelObjectsConfig {context} for {gameObject} is null!");
                return false;
            }

            return true;
        }

        #region Editor
#if UNITY_EDITOR
        public void Editor_SpawnObjects()
        {
            //m_editorSpawnedObjects = true;
            // Debug.Log("Editor_SpawnObjects");
            var sceneGo = SceneRootOperations.GetSceneRoot(gameObject.scene, 
                out var sceneRoot);

            TryGetComponent(out RoomInstanceComponent ric);
            ric.Data.UpdateVisuals();

            sceneRoot.gameObject.TryGet(out IExposedPropertyTable sceneResolver);
            Editor_SpawnObjects(LvlObjectsConfigScene, sceneResolver, ObjectPlacingContext.Scene, sceneGo);
            Editor_SpawnObjects(LvlObjectsConfigRoom, 
                LvlObjectsConfigRoom.GetOrInstantiateMapper(gameObject, ref MapperRoom), ObjectPlacingContext.Room, gameObject);
        }

        public void Editor_SpawnObjects(LevelObjectsConfig lvlObjectsConfig, IExposedPropertyTable resolver, ObjectPlacingContext placingContext, GameObject parent)
        {
            // Debug.Log($"Editor_SpawnObjects! {placingContext}");
            if (lvlObjectsConfig == null || Application.isPlaying)
                return;
            Debug.Assert(TryGetComponent(out RoomInstanceComponent ric));

            ref var objData = ref lvlObjectsConfig.Editor_ObjectData;
            for (var i = 0; i < objData.Length; i++)
            {
                var ok = Editor_TryResolve(resolver, objData[i].Link, lvlObjectsConfig, out var go, out var objCtx, out var tr);
                if (!ok)
                    ok = Editor_SpawnFromPrefab(resolver, ref objData[i], out go, out objCtx, out tr);
                
                if (!ok)
                {
                    Debug.LogError($"Could not Spawn prefab: {objData[i].Prefab}, exposedName: {objData[i].Link.exposedName}");
                    continue;
                }
                
                Editor_InitPlacingContext(objCtx, lvlObjectsConfig, placingContext, objData[i], ric);
                Editor_InitTransform(tr, placingContext, parent, ric, objData[i], MapperRoom);
                // Debug.Log($"Spawned {go}, should be placed under {ric.Data.PrefabParent}");
                Editor_LinkInstance(ref objData[i], go);
            }
        }

        static void Editor_LinkInstance(ref LevelObjectsConfig.ObjectData objData, GameObject go)
        {
            objData.Editor_Instance = go;
            objData.Editor_InstanceID = go.GetInstanceID();
        }

        static void Editor_InitTransform(Transform tr, ObjectPlacingContext placingContext, GameObject positionParent,
            IRoomInstance room, LevelObjectsConfig.ObjectData objData, RoomObjectMapper roomObjectMapper)
        {
            var setParent = placingContext == ObjectPlacingContext.Scene
                ? room.Data.PrefabParent
                : roomObjectMapper.transform;
            // Debug.Log($"{placingContext} set: {setParent} current: {tr.parent}");
            tr.SetParent(setParent);

            tr.position = positionParent.transform.TransformPoint(objData.LocalPosition);
            tr.rotation = positionParent.transform.rotation * Quaternion.Euler(objData.LocalRotation);
            tr.localScale = objData.Scale;
        }

        static void Editor_InitPlacingContext(ObjectContextMarker objCtx, LevelObjectsConfig lvlObjectsConfig, ObjectPlacingContext placingContext,
             LevelObjectsConfig.ObjectData objData, RoomInstanceComponent ric)
        {
            objCtx.hideFlags = HideFlags.HideAndDontSave;
            objCtx.PlacingContext = placingContext;
            objCtx.Actions = objData.ObjectActions;
            objCtx.LevelObjectsConfig = lvlObjectsConfig;
            objCtx.CurrentRoom = ric;
            ric.TryGet(out objCtx.RoomObjects);
            Debug.Assert(objCtx.RoomObjects != null);
        }

        static bool Editor_SpawnFromPrefab(IExposedPropertyTable resolver, ref LevelObjectsConfig.ObjectData objData, out GameObject go,
            out ObjectContextMarker objCtx, out Transform tr)
        {
            go = null;
            objCtx = null;
            tr = null;
            
            if (objData.Prefab == null) 
                return false;
            
            go = (GameObject) PrefabUtility.InstantiatePrefab(objData.Prefab);
            // Debug.Log($"Instantiated: {objData[i].Prefab} {go.GetInstanceID()} {lvlObjectsConfig}");
            var exposedRefPropName = new PropertyName($"{go.name}_{go.GetInstanceID()}");
            objData.Link = new ExposedReference<GameObject> {exposedName = exposedRefPropName, defaultValue = null};
            resolver?.SetReferenceValue(exposedRefPropName, go);
            objCtx = go.AddComponent<ObjectContextMarker>();
            tr = go.transform;
            
            return true;
        }

        static bool Editor_TryResolve(IExposedPropertyTable resolver, ExposedReference<GameObject> link, LevelObjectsConfig config,
            out GameObject go, 
            out ObjectContextMarker objCtx, 
            out Transform tr)
        {
            go = null;
            objCtx = null;
            tr = null;
            if (resolver == null) 
            { 
                Debug.Log("Resolver null!");
                return false;
            }

            go = link.Resolve(resolver);
            if (go == null)
            {
                Debug.LogError($"resolver {resolver as MonoBehaviour} Could not resolve {link.exposedName}, " +
                               $"for config {config}");
                return false;
            }

            // Debug.Log($"Resolved {go}");
            if (!go.TryGet(out objCtx))
                objCtx = go.AddComponent<ObjectContextMarker>();

            tr = go.transform;
            return true;
        }
#endif
        #endregion

    }
}
