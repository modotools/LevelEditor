﻿using System.Collections.Generic;
using Core.Types;
using Core.Unity.Extensions;
using UnityEngine;

namespace Level.Room
{
    public struct PlatformVisualData
    {
        public GameObject Go;
        public MeshRenderer Renderer;
        public MeshFilter Filter;
        public Mesh Mesh;
        public void Release()
        {
            if (Mesh != null) Mesh.DestroyEx();
        }
    }

    public struct PlatformInstanceData
    {
        public RoomSettings RoomSettings;

        public Transform PlatformTr;
        public GameObject PlatformGo;
        public int PlatformLayer;

        public MeshCollider Collider;
        public MeshFilter ShadowCaster;

        public Dictionary<Material, PlatformVisualData> Data;

        public static PlatformInstanceData Default(RoomSettings roomSettings) => new PlatformInstanceData()
        {
            Data = SimplePool<Dictionary<Material, PlatformVisualData>>.I.Get(),
            RoomSettings = roomSettings
        };

        public void Release()
        {
            foreach (var v in Data.Values) 
                v.Release();

            if (Collider != null && Collider.sharedMesh != null)
                Collider.sharedMesh.DestroyEx();
            
            SimplePool<Dictionary<Material, PlatformVisualData>>.I.Return(Data);
        }
    }
}
