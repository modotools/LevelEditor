﻿using System.Collections.Generic;
using Core.Types;
using UnityEngine;

namespace Level.Room
{
    public class RoomObjectMapper : MonoBehaviour, IExposedPropertyTable
    {
        public List<PropertyName> PropertyNames = new List<PropertyName>();
        public List<GameObject> References = new List<GameObject>();

        public bool TryGetPropertyName(GameObject go, out PropertyName pn)
        {
            var idx = References.IndexOf(go);
            if (idx != -1)
            {
                pn = PropertyNames[idx];
                return true;
            }

            pn = default;
            return false;
        }

        public void ClearMissingProperties()
        {
            for (var i = References.Count-1; i >= 0; i--)
            {
                if (References[i] != null)
                    continue;
                References.RemoveAt(i);
                PropertyNames.RemoveAt(i);
            }
        }
        
        public void SetReferenceValue(PropertyName id, Object value)
        {
            var goValue = value as GameObject;
            if (value != null && goValue == null)
            {
                Debug.LogError("Only GameObjects allowed!");
                return;
            }
            
            var idx = PropertyNames.IndexOf(id);
            if (idx != -1)
            {
                References[idx] = goValue;
                return;
            }
            
            PropertyNames.Add(id);
            References.Add(goValue);
        }

        public Object GetReferenceValue(PropertyName id, out bool idValid)
        {
            idValid = false;
            var idx = PropertyNames.IndexOf(id);
            if (idx <= -1) 
                return null;
            
            idValid = true;
            return References[idx];
        }

        public void ClearReferenceValue(PropertyName id)
        {
            var idx = PropertyNames.IndexOf(id);
            if (idx <= -1) 
                return;
            PropertyNames.RemoveAt(idx);
            References.RemoveAt(idx);
        }
    }
}
