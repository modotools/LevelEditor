using System.Collections.Generic;
using Core.Extensions;
using Core.Unity.Interface;
using Level.Enums;
using Level.PlatformLayer;
using Level.PlatformLayer.Interface;
using Level.Tiles;
using Level.Objects;
using UnityEngine;
using UnityEngine.Serialization;

namespace Level.Room
{
    public class RoomConfig : ScriptableObject, IRoom, IParentMarker, IVerify
    {
        [FormerlySerializedAs("RoomBasicProperties")] 
        public RoomConfigData RoomConfigData;

        public List<PlatformLayerConfig> PlatformLayerConfigs = new List<PlatformLayerConfig>();
        public List<ExitInfo> ExitRestrictions = new List<ExitInfo>();
        public List<ExitConfiguration> ValidExitConfiguration = new List<ExitConfiguration>();
        public List<int> ValidCardinalMap = new List<int>();
        public RestrictionMode RestrictionModeEntry;
        public RestrictionMode RestrictionModeExit;
        
        public bool AllowRotate = true;
        public bool AllowMirror;
        
        public LevelObjectsConfig RoomObjects;

        public RoomSettings RoomSettings => RoomConfigData.Settings;
        public GridSettings Grid => RoomSettings == null ? null : RoomSettings.Grid;
        public TilesSetListConfig TileSetListConfig => RoomSettings == null ? null :RoomSettings.TileSet;
        public TilesSetListConfig IntermediateSetListConfig => RoomSettings == null ? null : RoomSettings.IntermediateTileSet;

        public RoomBuilder Builder => RoomSettings == null ? null : RoomSettings.Builder;

        // todo: remove this here, should be applied from builder OR set to builder Input?
        public Material CeilingMaterial => RoomSettings == null ? null : RoomSettings.CeilingMaterial;
        public Vector3Int Size => RoomConfigData.Size;
        public Vector2Int TileDim => RoomConfigData.TileDim;
        public int PlatformCount => PlatformLayerConfigs.Count;
        public IPlatformLayer GetPlatformLayer(int idx)
        {
            Debug.Assert(idx.IsInRange(PlatformLayerConfigs));
            return PlatformLayerConfigs[idx];
        }

        public int MaxPlatformCount => RoomConfigData.MaxPlatformCount;
        public IEnumerable<IPlatformLayer> PlatformLayer => PlatformLayerConfigs;
        public IScriptableObject Parent => this;

        public void Editor_Verify(ref VerificationResult result)
        {
            if (PlatformLayerConfigs.Contains(null))
                result.Error("Null entry in Platforms!", this);
        }
    }
}

