using Core;
using Core.Extensions;
using Core.Types;
using Core.Unity;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Utility.PoolAttendant;
using Level.Enums;
using Level.Objects;
using Level.PlatformLayer;
using Level.PlatformLayer.Interface;
using ScriptableUtility;
using UnityEngine;
using static Core.Utils;

#if UNITASK
using System.Threading;
using Cysharp.Threading.Tasks;
#endif

namespace Level.Room.Operations
{
    public static class RoomOperations
    {
        const string k_floorVisualContainerName = "FloorVisuals";
        const string k_ceilingVisualContainerName = "CeilingVisuals";
        const string k_prefabParentContainerName = "PrefabParent";

        public static bool CreateRoom(RoomConfig r, 
            out RoomInstanceComponent roomInstanceComponent,
            out RoomObjectsDataComponent roomObjectsComponent,
            string name = null, Transform parent = null)
        {
            name ??= r.RoomConfigData.Name;

            var go = InitRoomObject(r.RoomSettings.GetRoomGameObject(), name, 
                out var cc, out roomInstanceComponent,
                out roomObjectsComponent);

            if (parent != null)
                go.transform.SetParent(parent);

            roomObjectsComponent.LvlObjectsConfigRoom = r.RoomObjects;
            roomInstanceComponent.SetData(new RoomInstanceData(r, go));
            Debug.Assert(cc.Editor_Containers != null, "Editor_Containers null");
            Debug.Assert(roomInstanceComponent.Data.RoomConfig.Builder != null, "Builder null");

            cc.AddVariables(roomInstanceComponent.Data.RoomConfig.Builder);
            cc.Init();
            return true;
        }

        static GameObject InitRoomObject(GameObject roomInstance, string name, out ContextComponent cc, 
            out RoomInstanceComponent instance, 
            out RoomObjectsDataComponent objComp)
        {
            //Debug.Log(name);
            roomInstance.name = name;
            roomInstance.SetActive(true);

            if (!roomInstance.TryGetComponent(out cc))
                cc = roomInstance.AddComponent<ContextComponent>();
            if (!roomInstance.TryGetComponent(out instance))
                instance = roomInstance.AddComponent<RoomInstanceComponent>();
            if (!roomInstance.TryGetComponent(out objComp))
                objComp = roomInstance.AddComponent<RoomObjectsDataComponent>();

            return roomInstance;
        }

        #region RoomInstanceData

        public static Bounds PlatformLayerBounds(this ref RoomInstanceData data, int idx)
        {
            var config = data.RoomConfig;
            if (config.PlatformLayerConfigs.Count <= idx || idx < 0)
                return new Bounds();
            var lvl = config.PlatformLayerConfigs[idx];
            return new Bounds(data.Position.Vector3() + lvl.Position.Vector3(idx) + lvl.Size.Vector3(1.0f) / 2, lvl.Size.Vector3(1.0f));
        }

        public static void UpdateRoomPosition(this ref RoomInstanceData data, Vector3 position)
        {
            data.Position = position.Vector3Int();
            data.Bounds = new Bounds(data.Position.Vector3() + (data.Size.Vector3() / 2.0f), data.Size.Vector3());
            data.UpdatePositionOffset();
        }

        public static void ShowRoom(this ref RoomInstanceData data)
        {
            var config = data.RoomConfig;
            var builder = config.Builder;
            data.UpdateVisuals();

            //for (var lvl = 0; lvl < config.PlatformLayerConfigs.Count; lvl++)
            //{
            //var platformLayer = config.PlatformLayer[lvl];
            //var m = data.VisualData.GetPlatformInstance(lvl).Mesh;

            //builder.BuildPlatform(m);
            //}

            //OperationData dat
            //if (RoomComponent!= null)
            //{
            //    RoomComponent.gameObject.SetActive(true);
            //    return;
            //}
            //RoomComponent = CreateObjectForRoom(this, Name);
            //dat.Room = this;
            //GenerateMesh(dat);
        }

        internal static void HideRoom(this ref RoomInstanceData data)
        {
            //if (RoomComponent == null)
            //    return;
            //RoomComponent.Room = null;
            //RoomGameObjectPool.I.Return(RoomComponent.gameObject);
            //// TODO: remove this but reset it properly, or there is this bug that collision object is missing
            //RoomComponent.DestroyEx();
            //RoomComponent = null;
        }


        internal static void InitLayer(this ref RoomInstanceData data)
        {
            if (!Application.isPlaying)
                return;
            var initialized = data.Layer != null;
            if (initialized)
                return;
            // if we want to edit layer in game via player mining etc, we need to instance platforms
            // also needed for setting position offset in the layer
            Debug.Assert(data.RoomConfig != null);
            Debug.Assert(data.RoomConfig.PlatformLayerConfigs != null);
            data.Layer = new IPlatformLayer[data.RoomConfig.PlatformLayerConfigs.Count];
            for (var i = 0; i < data.Layer.Length; i++)
                data.Layer[i] = new PlatformLayerInstance(data.RoomConfig.PlatformLayerConfigs[i]);
        }

        /// <summary>
        /// Sets the Level the player is currently at, eg. what should be visible
        /// </summary>
        /// <param name="data">the room instance</param>
        /// <param name="minLvl">first level visible</param>
        /// <param name="maxLvl">last level visible</param>
        public static void SetLevel(this ref RoomInstanceData data, int minLvl, int maxLvl)
            => data.VisualData.SetVisibleHeight(minLvl, maxLvl);
        public static void UpdateVisuals(this ref RoomInstanceData data)
        {
            var config = data.RoomConfig;
            if (config == null)
                return;

            data.UpdatePositionOffset();

            if (!data.VisualData.IsValid)
                CreateVisualData(ref data);

            data.VisualData.UpdateVisuals(config);
        }

        static void CreateVisualData(this ref RoomInstanceData data)
        {
            data.FindOrCreate(k_floorVisualContainerName, out var floorVisual);
            data.FindOrCreate(k_ceilingVisualContainerName, out var ceilingVisual);
            data.FindOrCreate(k_prefabParentContainerName, out var prefabParent);

            ceilingVisual.localPosition = Vector3.zero;
            data.PrefabParent = prefabParent;
            data.VisualData = new RoomVisualData(data.RoomSettings, floorVisual, ceilingVisual);
        }

        static void FindOrCreate(this ref RoomInstanceData data, string name, out Transform visualContainer)
        {
            visualContainer = data.Transform.Find(name);
            if (visualContainer == null)
                data.Transform.CreateChildObject(name, out visualContainer);
        }

        static void UpdatePositionOffset(this ref RoomInstanceData data)
        {
            var roomTr = data.Transform;
            var pos = roomTr.position.Vector3Int();
            roomTr.position = pos;

            if (data.RoomConfig == null || data.RoomConfig.Grid == null)
            {
                Debug.LogError($"{nameof(UpdatePositionOffset)} failed for {roomTr.name}!" +
                               $" RoomConfig: {data.RoomConfig}" +
                               $" RoomSettings: {data.RoomConfig?.RoomSettings}" +
                               $" Grid: {data.RoomConfig?.Grid}");
                return;
            }

            var height = data.RoomConfig.Grid.TileHeight;
            var i = 0;
            foreach (var pl in data.PlatformLayer)
            {
                if (pl == null)
                {
                    Debug.LogError("Platform is null!");
                    continue;
                }
                pl.PositionOffset = new Vector3(pos.x, pos.y + (height * i), pos.z);
                ++i;
            }
        }

        public static void UpdateData(this ref RoomInstanceData iData, RoomConfigData configData)
        {
            iData.GameObject.name = configData.Name;
            iData.RoomConfig.RoomConfigData = configData;
            iData.UpdateRoomPosition(iData.Transform.position);
        }

        public static void Destroy(this ref RoomInstanceData data, bool undo = false)
        {
            data.VisualData.Destroy(undo);
            if (data.GameObject != null)
                data.GameObject.DestroyEx(undo);
            data = default;
        }

        public static ref RoomConfigData GetRoomData(this ref RoomInstanceData data) => ref data.RoomConfig.RoomConfigData;
        #endregion
        
        #region RoomInstanceComponent Build
#if UNITASK
        public static async UniTask AsyncBuild(RoomInstanceComponent ric, CancellationToken cancellationToken)
        {
            if (ric.Builder == null)
            {
                Debug.LogError($"{nameof(AsyncBuild)} failed! Builder is null!");
                return;
            }

            ric.EnsureContextAndVariables();
            ric.EnsureAction();
            // todo: init this in graph
            ric.Builder.Edges.Value.Data.Clear();

            if (ric.BuildAction is IAsyncAction asyncAction)
                await asyncAction.InvokeAsync(cancellationToken);
            else
                ric.BuildAction?.Invoke();
        }
#endif

        public static void Build(this RoomInstanceComponent ric)
        {
            if (ric.Builder == null)
            {
                Debug.LogError($"{nameof(Build)} failed! Builder is null!");
                return;
            }

            ric.EnsureContextAndVariables();
            ric.EnsureAction();
            // todo: init this in graph
            ric.Builder.Edges.Value.Data.Clear();
            ric.BuildAction?.Invoke();
        }

        static void EnsureContextAndVariables(this RoomInstanceComponent ric)
        {
            var ctx = ric.Context;
            var builder = ric.Builder;

            // make sure context is correct
            EnsureContextAndVariables(ric, ric.RoomSettings, ctx, builder);
        }

        static void EnsureContextAndVariables(IRoom room,
            RoomSettings settings,
            IContext ctx, RoomBuilder builder)
        {
            if (NULL.IsAny(room, settings, ctx, builder))
            {
                Debug.LogError($"{nameof(EnsureContextAndVariables)} failed!");
                return;
            }

            ctx.Init();
            builder.SetContext(ctx);

            // make sure variable point to correct objects
            builder.Room.Value = room;
            Debug.Assert(settings != null);

            builder.TilesSetListConfig.Value = settings.TileSet;
            builder.IntermediateTilesSetListConfig.Value = settings.IntermediateTileSet;
            builder.LevelMaterialTypeConfig.Value = settings.LevelMaterialTypeConfig.Result;

            builder.Platform.Value = null;
        }

        static void EnsureAction(this RoomInstanceComponent ric)
        {
            if (ric.BuildAction != null || ric.Builder == null)
                return;
            ric.SetBuildAction(ric.Builder.CreateRoomBuilderAction());
        }
        #endregion

        #region Placement
        public static bool IsOpenPlaceForSpawn(this IRoomInstance room, TestPlacing testPlacing, GameObject prefab, SpatialData spatialData)
        {
            if (testPlacing == TestPlacing.None)
                return true;
            if (prefab == null || !prefab.TryGetComponent(out IBoundsComponent bc))
            {
                //Debug.LogWarning($"{room.gameObject} Cannot test for free spaces for prefab {prefab} because it does not have BoundsComponent");
                return true; // can't test, just spawn
            }
            var scale = spatialData.Scale;
            var size = bc.ObjectBounds.size;
            var bounds = new Bounds(spatialData.Position + bc.ObjectBounds.center, 
                new Vector3(scale.x * size.x, scale.y * size.y, scale.z * size.z));

            var floor = room.RoomSettings.GetFilter(GeometryType.Floor);
            var wall = room.RoomSettings.GetFilter(GeometryType.Wall);

            var testFloor = testPlacing == TestPlacing.OnGround;
            var testNonWall = testPlacing == TestPlacing.NoWall;

            var spaceOk = true;
            //var platformCount = room.PlatformCount;

            //DebugString debugString = default;
            //debugString.AddLog($"PlatformCount {room.PlatformCount}");
            foreach (var pl in room.PlatformLayer)
            {
                var platformBounds = PlatformLayer.Operations.GetBounds(pl, pl.PositionOffset, room.RoomSettings.Grid);
                var intersectionType = platformBounds.GetIntersectionType(bounds);
                if (intersectionType == IntersectionType.Touch || intersectionType == IntersectionType.None)
                {
                    //debugString.AddLog($"intersectionType {intersectionType} bounds {bounds} platform {platformBounds}");
                    continue;
                }
                GetLoopVariablesForBoundsAndPlatform(bounds, pl, out var xMin, out var zMin, out var xMax, out var zMax);
                //debugString.AddLog($"x {xMin} - {xMax} z {zMin} - {zMax}");

                for (var x = xMin; x < xMax; ++x)
                for (var z = zMin; z < zMax; ++z)
                {
                    var tile = PlatformLayer.Operations.GetTile(pl, pl, new Vector2Int(x, z));
                    if (testFloor && floor.IsTileActive(tile)) 
                        continue;
                    if (testNonWall && !wall.IsTileActive(tile)) 
                        continue;
                    // CustomDebugDraw.DrawBounds(bounds, Color.red, 60f);
                    // Debug.Log($"Cannot spawn {prefab} at pos {spatialData.Position}");
                    spaceOk = false;
                    break;
                }
                break;
            }
            //debugString.Prepend($"{room.gameObject} {prefab} at pos {spatialData.Position} Can Spawn {spaceOk}");
            //debugString.Print();
            return spaceOk;
        }

        public static void GetLoopVariablesForBoundsAndPlatform(Bounds bounds, IPlatformLayer pl, out int xMin, out int zMin,
            out int xMax, out int zMax)
        {
            bounds.SetMinMax(
                new Vector3(Mathf.FloorToInt(bounds.min.x), Mathf.FloorToInt(bounds.min.y), Mathf.FloorToInt(bounds.min.z)),
                new Vector3(Mathf.CeilToInt(bounds.max.x+1), Mathf.CeilToInt(bounds.max.y+1), Mathf.CeilToInt(bounds.max.z+1)));
            var objTileMin = bounds.min.Vector2Int();
            var objTileMax = bounds.max.Vector2Int();

            xMin = (int) Mathf.Max(0, objTileMin.x - pl.PositionOffset.x);
            zMin = (int) Mathf.Max(0, objTileMin.y - pl.PositionOffset.z);
            xMax = (int) Mathf.Min(pl.TileDim.x, objTileMax.x - pl.PositionOffset.x);
            zMax = (int) Mathf.Min(pl.TileDim.y, objTileMax.y - pl.PositionOffset.z);
        }

        #endregion
    }
}
