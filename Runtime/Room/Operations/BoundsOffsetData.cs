﻿namespace Level
{
    public struct BoundsOffsetData
    {
        public float OffsetFromMin;
        public float HalfTileSize;
        public float HalfTileSizeY;

        public static BoundsOffsetData Default => new BoundsOffsetData()
        {
            OffsetFromMin = -1,
            HalfTileSize = 0f,
            HalfTileSizeY = 0f,
        };
    }
}