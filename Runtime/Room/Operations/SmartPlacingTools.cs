﻿using Level.Enums;
using Level.PlatformLayer.Interface;
using UnityEngine;
using static Level.PlatformLayer.Operations;

namespace Level.Room
{
    public static class SmartPlacingTools
    {
        public static void ApplySmartPlacingToolsOnLevel(this IRoomInstance roomInstance, int lvl, 
            bool fixVisibility, bool avoidOneSpaceTiles)
        {
            for (var x = 0; x < roomInstance.TileDim.x; ++x)
            for (var z = 0; z < roomInstance.TileDim.y; ++z)
                roomInstance.ModifySurroundingLevel(new Vector2Int(x, z), lvl, fixVisibility, avoidOneSpaceTiles);
        }

        public static IPlatformLayer GetLayer_OffsetFromLevel(this IRoomInstance roomInstance, int offset, int lvl)
        {
            var targetLvl = lvl + offset;
            if (targetLvl < 0)
                return null;
            return targetLvl >= roomInstance.PlatformCount 
                ? null 
                : roomInstance.GetPlatformLayer(lvl + offset);
        }

        public static void FixVisibility(this IRoomInstance roomInstance, Vector2Int pos, int lvl)
        {
            if (lvl + 1 >= roomInstance.PlatformCount)
                return;
            var aboveLvl = roomInstance.GetLayer_OffsetFromLevel(1, lvl);

            var floorFilter = roomInstance.RoomSettings.GetFilter(GeometryType.Floor);
            var autoFilter = roomInstance.RoomSettings.GetFilter(GeometryType.Auto);
            var wallFilter = roomInstance.RoomSettings.GetFilter(GeometryType.Wall);
            var emptyFilter = roomInstance.RoomSettings.GetFilter(GeometryType.Empty);

            var pl = roomInstance.GetPlatformLayer(lvl);

            var tile = GetTile(pl, pl, pos, OutOfLevelBoundsAction.IgnoreTile);
            if (floorFilter.IsTileActive(tile)
                || emptyFilter.IsTileActive(tile))
                SetTile(aboveLvl, aboveLvl, pos, autoFilter, OutOfLevelBoundsAction.IgnoreTile);
        }

        public static void FillOneSpaceTiles(this IRoomInstance roomInstance, Vector2Int pos, int lvl)
        {
            var above1Lvl = roomInstance.GetLayer_OffsetFromLevel(1, lvl);
            var above2Lvl = roomInstance.GetLayer_OffsetFromLevel(2, lvl);
            var below1Lvl = roomInstance.GetLayer_OffsetFromLevel(-1, lvl);
            var below2Lvl = roomInstance.GetLayer_OffsetFromLevel(-2, lvl);

            var floorFilter = roomInstance.RoomSettings.GetFilter(GeometryType.Floor);
            var autoFilter = roomInstance.RoomSettings.GetFilter(GeometryType.Auto);
            var wallFilter = roomInstance.RoomSettings.GetFilter(GeometryType.Wall);
            var emptyFilter = roomInstance.RoomSettings.GetFilter(GeometryType.Empty);

            var pl = roomInstance.GetPlatformLayer(lvl);
            var tile = GetTile(pl, pl, pos, OutOfLevelBoundsAction.IgnoreTile);

            if (floorFilter.IsTileActive(tile))
            {
                if (above1Lvl != null)
                    SetTile(above1Lvl, above1Lvl, pos, autoFilter, OutOfLevelBoundsAction.IgnoreTile);

                if (above2Lvl != null)
                {
                    tile = GetTile(above1Lvl, above1Lvl, pos, OutOfLevelBoundsAction.IgnoreTile);
                    if (!emptyFilter.IsTileActive(tile))
                        SetTile(above2Lvl, above2Lvl, pos, autoFilter, OutOfLevelBoundsAction.IgnoreTile);
                }

                if (below2Lvl != null)
                {
                    tile = GetTile(below2Lvl, below2Lvl, pos, OutOfLevelBoundsAction.IgnoreTile);
                    //if (belowType == TileType.Empty)
                    //{
                    //    tile = _Room.GetTile(lvl - 3, x, z, OutOfLevelBoundsAction.IgnoreTile);
                    //    belowType = ResultingType(tile);
                    //    if (belowType != TileType.Empty && belowType != TileType.Floor)
                    //        _Room.SetTile(lvl - 2, x, z, (ushort)TileType.Wall, action: OutOfLevelBoundsAction.IgnoreTile);
                    //}
                    //else 
                    if (floorFilter.IsTileActive(tile))
                        SetTile(below2Lvl, below2Lvl, pos, wallFilter, OutOfLevelBoundsAction.IgnoreTile);
                }

                return;
            }

            var isWall = wallFilter.IsTileActive(tile);
            //var isAuto = autoFilter.IsTileActive(tile);

            // todo: Auto
            if (isWall) //|| isAuto)
            {
                // todo: Resulting Type
                //if (isAuto)
                //{
                //    tile = _Room.GetTile(lvl, x, z, OutOfLevelBoundsAction.IgnoreTile);
                //    var resultType = ResultingType(tile);
                //    if (resultType != Type.Wall)
                //        return;
                //}

                if (above1Lvl != null)
                {
                    tile = GetTile(above1Lvl, above1Lvl, pos, OutOfLevelBoundsAction.IgnoreTile);
                    if (tile == ushort.MaxValue)
                        return;

                    if (floorFilter.IsTileActive(tile))
                    {
                        if (above2Lvl != null)
                        {
                            tile = GetTile(above2Lvl, above2Lvl, pos, OutOfLevelBoundsAction.IgnoreTile);
                            //aboveType = ResultingType(tile);
                            if (emptyFilter.IsTileActive(tile))
                                SetTile(above1Lvl, above1Lvl, pos, autoFilter, OutOfLevelBoundsAction.IgnoreTile);
                        }
                    }
                }

                if (below1Lvl != null)
                {
                    tile = GetTile(below1Lvl, below1Lvl, pos, OutOfLevelBoundsAction.IgnoreTile);
                    //var belowType = ResultingType(tile);
                    if (floorFilter.IsTileActive(tile))
                        SetTile(below1Lvl, below1Lvl, pos, wallFilter, action: OutOfLevelBoundsAction.IgnoreTile);
                }
            }
        }

        public static void FixVisualProblems(this IRoomInstance roomInstance, Vector2Int pos, int lvl)
        {
            var floorFilter = roomInstance.RoomSettings.GetFilter(GeometryType.Floor);
            var autoFilter = roomInstance.RoomSettings.GetFilter(GeometryType.Auto);
            var wallFilter = roomInstance.RoomSettings.GetFilter(GeometryType.Wall);
            var emptyFilter = roomInstance.RoomSettings.GetFilter(GeometryType.Empty);

            var pl = roomInstance.GetPlatformLayer(lvl);
            var tile = GetTile(pl, pl, pos, action: OutOfLevelBoundsAction.IgnoreTile);
            //Type tileType = ResultingType(tile);

            var above1Lvl = roomInstance.GetLayer_OffsetFromLevel(1, lvl);
            var below1Lvl = roomInstance.GetLayer_OffsetFromLevel(-1, lvl);
            var below2Lvl = roomInstance.GetLayer_OffsetFromLevel(-2, lvl);

            // todo: Auto
            var isWall = wallFilter.IsTileActive(tile);
            var isAuto = autoFilter.IsTileActive(tile);
            var isEmpty = emptyFilter.IsTileActive(tile);

            if (isWall) // || autoFilter.IsTileActive(tile))
            {
                // todo: Resulting Type
                //if (autoFilter.IsTileActive(tile))
                //{
                //    tile = GetTile(lvl, lvl, pos, OutOfLevelBoundsAction.IgnoreTile);
                //    //var resultType = ResultingType(tile);
                //    if (resultType != Type.Wall)
                //        return;
                //}

                if (above1Lvl != null)
                {
                    tile = GetTile(above1Lvl, above1Lvl, pos, OutOfLevelBoundsAction.IgnoreTile);
                    if (tile == ushort.MaxValue)
                        return;
                    //var aboveType = OriginType(tile);

                    if (emptyFilter.IsTileActive(tile))
                    {
                        //aboveType = Type.Floor;
                        SetTile(above1Lvl, above1Lvl, pos, floorFilter, OutOfLevelBoundsAction.IgnoreTile);
                    }
                }

                return;
            }

            if (isEmpty)
            {
                if (below1Lvl == null)
                    return;
                tile = GetTile(below1Lvl, below1Lvl, pos, OutOfLevelBoundsAction.IgnoreTile);
                if (tile == ushort.MaxValue)
                    return;
                // var belowType = OriginType(tile);
                // var belowResultType = ResultingType(tile);

                var belowWall = wallFilter.IsTileActive(tile);
                var belowAuto = autoFilter.IsTileActive(tile);

                if (!(belowWall || belowAuto))
                    return;

                var toAuto = false;
                if (below2Lvl != null)
                {
                    tile = GetTile(below2Lvl, below2Lvl, pos, OutOfLevelBoundsAction.IgnoreTile);
                    //if (tile != ushort.MaxValue)
                    //{
                    //    belowType = OriginType(tile);
                    //    belowResultType = ResultingType(tile);
                    //}
                    var below2Wall = wallFilter.IsTileActive(tile);
                    var below2Auto = autoFilter.IsTileActive(tile);

                    if (!(below2Wall || below2Auto))
                        toAuto = true;
                }

                SetTile(below1Lvl, below1Lvl, pos, toAuto ? autoFilter : floorFilter,
                    OutOfLevelBoundsAction.IgnoreTile);
            }
        }

        public static void ModifySurroundingLevel(this IRoomInstance roomInstance, Vector2Int pos, int lvl, 
            bool fixVisibility, bool avoidOneSpaceTiles)
        {
            if (fixVisibility)
                roomInstance.FixVisibility(pos, lvl);
            
            roomInstance.FixVisualProblems(pos, lvl);

            if (avoidOneSpaceTiles)
                roomInstance.FillOneSpaceTiles(pos, lvl);
        }
    }
}