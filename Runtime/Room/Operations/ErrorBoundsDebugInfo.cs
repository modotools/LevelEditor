﻿using Core.Types;
using Level.Enums;
using System.Collections.Generic;
using UnityEngine;

namespace Level.Room.Operations
{
    public struct BoundOperationDebugInfo
    {
        public OutOfLevelBoundsAction Action;
        public OperationResult Result;

        public List<Bounds> ErrorBounds;
        // room or level where error happened
        public List<Bounds> OperationBounds;

        public static BoundOperationDebugInfo Default => new BoundOperationDebugInfo()
        {
            ErrorBounds = new List<Bounds>(),
            OperationBounds = new List<Bounds>()
        };
    }
}
