﻿using System;
using UnityEngine;
using Core.Types;
using Core.Unity.Extensions;
using Level.Room;

namespace Level
{
    public static class BoundsOperations
    {
        public static Bounds GrowBounds(Bounds b, Vector3 dir, Vector3 size)
        {
            var pos = b.center
                      - new Vector3(dir.x * b.extents.x, dir.y * b.extents.y, dir.z * b.extents.z)
                      + new Vector3(dir.x * size.x / 2, dir.y * size.y / 2, dir.z * size.z / 2);
            return new Bounds(pos, size);
        }

        public static Bounds GrowBoundsGridAligned(Bounds b, GridSettings gridSettings)
        {
            // todo: do not align y-axis?
            return GrowBoundsGridAligned(b, new Vector3Int(gridSettings.RoomChunkSize,
                gridSettings.RoomYChunkSize,
                gridSettings.RoomChunkSize));
        }

        public static void GetBoundsNextTo(Bounds originalBounds, Cardinals cardinal, int level,
            Vector3Int size, out Bounds bounds) 
            => GetBoundsNextTo(originalBounds, cardinal, level, size, BoundsOffsetData.Default, out bounds);

        public static void GetBoundsNextTo(Bounds originalBounds, Cardinals cardinal, int level,
            Vector3Int size, BoundsOffsetData boundsOffsetData, out Bounds bounds)
        {
            var minPosition = Vector3.zero;
            var offsetNotSet = Math.Abs(boundsOffsetData.OffsetFromMin + 1) < float.Epsilon;

            var halfSize = 0.5f * size.Vector3();

            var minY = originalBounds.min.y - boundsOffsetData.HalfTileSizeY;
            switch (cardinal)
            {
                case Cardinals.North:
                    {
                        var x = offsetNotSet
                            ? originalBounds.center.x - halfSize.x
                            : originalBounds.min.x + boundsOffsetData.OffsetFromMin - halfSize.x;

                        minPosition = new Vector3(x, minY + level, originalBounds.max.z - boundsOffsetData.HalfTileSize);
                    }
                    break;
                case Cardinals.East:
                    {
                        var z = offsetNotSet
                            ? originalBounds.center.z - halfSize.z
                            : originalBounds.min.z + boundsOffsetData.OffsetFromMin - halfSize.z;

                        minPosition = new Vector3(originalBounds.max.x - boundsOffsetData.HalfTileSize, minY + level, z);
                    }
                    break;
                case Cardinals.South:
                    {
                        var x = offsetNotSet
                            ? originalBounds.center.x - halfSize.x
                            : originalBounds.min.x + boundsOffsetData.OffsetFromMin - halfSize.x;

                        minPosition = new Vector3(x, minY + level, originalBounds.min.z - size.z + boundsOffsetData.HalfTileSize);
                    }
                    break;
                case Cardinals.West:
                    {
                        var z = offsetNotSet
                            ? originalBounds.center.z - halfSize.z
                            : originalBounds.min.z + boundsOffsetData.OffsetFromMin - halfSize.z;

                        minPosition = new Vector3(originalBounds.min.x - size.x + boundsOffsetData.HalfTileSize, minY + level, z);
                    }
                    break;
            }

            bounds = new Bounds(minPosition + halfSize, size);
        }

        #region Exit to Bounds
        // Bounds exactly at exit-tiles, so bounds contains the exit tiles for this room and the neighbor-room
        // this is useful when trying to select the corresponding tiles for the exit.
        public static Bounds GetBoundsContainingExitTiles(Bounds roomBounds, ExitInfo exit)
        {
            return GetExitBounds(roomBounds, exit, new BoundsOffsetData()
            {
                HalfTileSizeY = 0.5f,
                OffsetFromMin = 0.5f
            });
        }

        // Bounds are following the exit, so they would contain the player once he steps out of the room at this exit.
        // this is useful for room-workers to check whether the neighbor-exit is contained in the current generating room.
        public static Bounds GetBoundsFollowingExit(Bounds roomBounds, ExitInfo exit)
        {
            var exitBounds = GetExitBounds(roomBounds, exit, BoundsOffsetData.Default);
            Clamp(roomBounds, exit.ExitDirection, ref exitBounds);
            return exitBounds;
        }

        static Bounds GetExitBounds(Bounds roomBounds, ExitInfo exit, BoundsOffsetData boundsOffsetData)
        {
            var exitTilesSize = 1 + (exit.ToIdx - exit.FromIdx);
            float centerAdd = exit.FromIdx;
            var exitRange = exit.ToIdx - exit.FromIdx;
            if (exit.ToIdx > exit.FromIdx)
                centerAdd = exit.FromIdx + 0.5f * exitRange;

            const int outwardSize = 1;

            Vector3Int size;
            if (exit.ExitDirection == Cardinals.North 
                || exit.ExitDirection == Cardinals.South)
                size = new Vector3Int(exitTilesSize, 1, outwardSize);
            else
                size = new Vector3Int(outwardSize, 1, exitTilesSize);

            boundsOffsetData.OffsetFromMin = centerAdd;
            GetBoundsNextTo(roomBounds, exit.ExitDirection, 
                 exit.Level, size, boundsOffsetData,out var exitBounds);

            return exitBounds;
        }
        #endregion

        static void Clamp(Bounds clampArea, Cardinals exitDir, ref Bounds clampedBounds)
        {
            var min = clampedBounds.min;
            var max = clampedBounds.max;
            if (exitDir == Cardinals.North
                || exitDir == Cardinals.South)
            {
                min.x = Mathf.Max(min.x, clampArea.min.x);
                max.x = Mathf.Min(max.x, clampArea.max.x);
            }
            else
            {
                min.z = Mathf.Max(min.z, clampArea.min.z);
                max.z = Mathf.Min(max.z, clampArea.max.z);
            }

            var size = max - min;
            clampedBounds = new Bounds(min + (0.5f * size), size);
        }

        static Bounds GrowBoundsGridAligned(Bounds b, Vector3Int gridSize)
        {
            var minX = Mathf.FloorToInt(b.min.x / gridSize.x) * gridSize.x;
            var minY = Mathf.FloorToInt(b.min.y / gridSize.y) * gridSize.y;
            var minZ = Mathf.FloorToInt(b.min.z / gridSize.z) * gridSize.z;

            var maxX = Mathf.CeilToInt(b.max.x / gridSize.x) * gridSize.x;
            var maxY = Mathf.CeilToInt(b.max.y / gridSize.y) * gridSize.y;
            var maxZ = Mathf.CeilToInt(b.max.z / gridSize.z) * gridSize.z;

            var minVec = new Vector3(minX, minY, minZ);
            var maxVec = new Vector3(maxX, maxY, maxZ);
            var sizeVec = maxVec - minVec;
            var center = minVec + sizeVec / 2;

            return new Bounds(center, sizeVec);
        }
    }
}