﻿using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Unity.Extensions;
using Core.Unity.Utility.PoolAttendant;
using Level.Data;
using UnityEngine;
using static Core.Unity.Extensions.TransformExtensions;
using static Core.Unity.Utility.PoolAttendant.PoolAttendantExtension;

namespace Level.Room
{
    public static class RoomVisualDataOperations
    {
        /// <summary>
        /// Sets the Level the player is currently at, eg. what should be visible
        /// </summary>
        /// <param name="lvl">the level</param>
        public static void SetVisibleHeight(this ref RoomVisualData rvd, int lvl)
            => rvd.SetVisibleHeight(0, lvl);
        public static void SetVisibleHeight(this ref RoomVisualData rvd, int minLvl, int maxLvl)
        {
            //int ceilLvl = lvl;
            //if (ceilLvl >= _CeilingVisuals.Count)
            //    ceilLvl = _CeilingVisuals.Count -1;
            if (!rvd.IsValid)
                return;

            if (rvd.FloorCount != rvd.CeilingCount)
            {
                Debug.LogError("FloorCount != CeilingCount");
                return;
            }

            //var minCeilLvl = Mathf.Clamp(minLvl + 2, 0, maxLvl);
            //Debug.Log($"SetVisibleHeight {lvl}");
            var i = 0;
            for (; i < rvd.CeilingCount; ++i)
            {
                if (!TryGetCeiling(ref rvd, i, out var ceiling)) 
                    continue;
                ceiling.Go.SetActive(i >= minLvl && i <= maxLvl);
                
                if (!TryGetPlatformInstance(ref rvd, i, out var platformInst))
                    continue;
                platformInst.PlatformGo.SetActive(i>= minLvl && i <= maxLvl);
            }

            // cannot remember why i >= lvl - 1 (which means for last level we cannot go higher then one above or it will not show)
            //_FloorVisuals[i].Go.SetActive(i >= lvl - 1 && i <= lvl + 1);
        }

        static bool TryGetCeiling(ref RoomVisualData rvd, int i, out PlatformVisualData ceiling)
        {
            ceiling = rvd.GetCeiling(i);
            if (ceiling.Go != null) 
                return true;

            Debug.LogError($"Ceiling GameObject {i} does not exist");
            return false;
        }
        static bool TryGetPlatformInstance(ref RoomVisualData rvd, int i, out PlatformInstanceData platform)
        {
            platform = rvd.GetPlatformInstance(i);
            if (platform.PlatformGo != null) 
                return true;

            Debug.LogError($"Platform GameObject {i} does not exist");
            return false;
        }

        public static void UpdateVisuals(this ref RoomVisualData rvd, IRoom room)
        {
            var maxLevel = room.MaxPlatformCount;

            if (room.RoomSettings == null)
            {
                Debug.LogError($"{nameof(UpdateVisuals)} failed! RoomSettings null!");
                return;
            }

            var ceilingMaterial = room.RoomSettings.CeilingMaterial;
            var groundLayer = room.RoomSettings.GroundLayer;

            var i = 0;
            for (; i < maxLevel; ++i)
            {
                EnsureCeilingVisuals(rvd.m_roomSettings, rvd.m_ceilingVisualsParent, rvd.m_ceilingVisuals, i, ceilingMaterial);
                EnsurePlatforms(rvd.m_roomSettings, rvd.m_floorVisualsParent, rvd.m_floorVisuals, i, groundLayer);
            }
            // we always need one extra floor visual, because we render 2 for each level
            // EnsurePlatforms(rvd.m_floorVisualsParent, rvd.m_floorVisuals, i, groundLayer);

            var killLoopCeilings = Mathf.Max(rvd.m_ceilingVisualsParent.childCount, rvd.m_ceilingVisuals.Count);
            var killLoopFloors = Mathf.Max(rvd.m_floorVisualsParent.childCount, rvd.m_floorVisuals.Count);
            var loop = Mathf.Max(killLoopCeilings, killLoopFloors);

            for (var j = loop-1; j > i; --j)
            {
                RemoveCeilingVisuals(rvd.m_ceilingVisualsParent, rvd.m_ceilingVisuals, j);
                //if (j > i)
                RemovePlatformInstance(rvd.m_floorVisualsParent, rvd.m_floorVisuals, j);
            }
        }

        public static void Destroy(this ref RoomVisualData rvd, bool undo = false)
        {
            DestroyPlatform(rvd.m_floorVisuals, undo);
            DestroyCeiling(rvd.m_ceilingVisuals, undo);
        }

        #region Platform

        public static void ApplyMaterialToMeshData(this ref PlatformInstanceData platformInstanceData,
            MaterialToMeshData materialToMeshData)
        {
            var targetMap = materialToMeshData.MaterialMeshData;
            var instanceMap = platformInstanceData.Data;
            var deactivate = instanceMap.Keys.Where(k => !targetMap.Keys.Contains(k));
            foreach (var material in targetMap.Keys)
            {
                if (!instanceMap.ContainsKey(material))
                    instanceMap.Add(material, default);
                var visualData = instanceMap[material];
                if (visualData.Go == null)
                {
                    var parent = platformInstanceData.PlatformTr;
                    var prefab = platformInstanceData.RoomSettings.PlatformVisualPrefab;
                    parent.CreatePooledChildObject(prefab, material.name, out visualData.Go, out var platformTr, 
                        HideFlags.DontSave);
                    visualData.Go.layer = platformInstanceData.PlatformLayer;

                    visualData.Go.TryGetComponent(out visualData.Filter);
                    visualData.Go.TryGetComponent(out visualData.Renderer);
                    SetMeshAndMaterial(material, visualData.Renderer, visualData.Filter, out visualData.Mesh);
                }
                else visualData.Go.SetActive(true);
                var md = targetMap[material];

                ApplyMesh(visualData, md);

                instanceMap[material] = visualData;
            }

            foreach (var m in deactivate)
            {
                if (instanceMap[m].Go == null)
                    continue;
                instanceMap[m].Go.SetActive(false);
            }
        }

        public static void ClearCeilingMesh(PlatformVisualData visualData) => visualData.Mesh.Clear();

        public static void ApplyCeilingMesh(PlatformVisualData visualData, MeshData md)
        {
            visualData.Mesh.Clear();
            var vertices = new Vector3[md.Vertices.Count * 2];
            var uvs = new Vector2[md.UVs.Count * 2];
            var triangles = new int[md.Triangles.Count * 2];
            for (var i = 0; i < md.Vertices.Count; i++)
            {
                vertices[i] = md.Vertices[i] + 0.01f * Vector3.up;
                vertices[i + md.Vertices.Count] = md.Vertices[i] + 0.99f * Vector3.up;
            }
            for (var i = 0; i < md.UVs.Count; i++)
            {
                uvs[i] = md.UVs[i];
                uvs[i + md.UVs.Count] = md.UVs[i];
            }
            for (var i = 0; i < md.Triangles.Count; i++)
            {
                triangles[i] = md.Triangles[i];
                triangles[i + md.Triangles.Count] = md.Triangles[i] + md.Vertices.Count;
            }
            visualData.Mesh.vertices = vertices;
            visualData.Mesh.uv = uvs;
            visualData.Mesh.triangles = triangles;
            visualData.Mesh.RecalculateNormals();
            visualData.Mesh.RecalculateTangents();
            visualData.Mesh.RecalculateBounds();
        }

        public static void ApplyMesh(PlatformVisualData visualData, MeshData md)
        {
            visualData.Mesh.Clear();

            visualData.Mesh.vertices = md.Vertices.ToArray();
            visualData.Mesh.uv = md.UVs.ToArray();
            if (!md.UVs2.IsNullOrEmpty())
                visualData.Mesh.uv2 = md.UVs2.ToArray();
            if (!md.UVs3.IsNullOrEmpty())
                visualData.Mesh.uv3 = md.UVs3.ToArray();
            visualData.Mesh.triangles = md.Triangles.ToArray();
            visualData.Mesh.RecalculateNormals();
            visualData.Mesh.RecalculateTangents();
            visualData.Mesh.RecalculateBounds();
        }

        public static void ApplyCollider(this ref PlatformInstanceData instanceData, MeshData md)
        {
            var colliderMesh = instanceData.Collider.sharedMesh;
            colliderMesh.Clear();

            colliderMesh.vertices = md.Vertices.ToArray();
            colliderMesh.uv = md.UVs.ToArray();
            colliderMesh.triangles = md.Triangles.ToArray();
            colliderMesh.RecalculateNormals();
            colliderMesh.RecalculateTangents();
            colliderMesh.RecalculateBounds();
            
            instanceData.Collider.sharedMesh = colliderMesh;
            instanceData.ShadowCaster.sharedMesh = colliderMesh;
        } 

        public static PlatformInstanceData GetPlatformInstance(this ref RoomVisualData rvd, int levelIdx) => rvd.m_floorVisuals[levelIdx];

        static void DestroyPlatform(IEnumerable<PlatformInstanceData> map, bool undo = false)
        {
            if (map == null)
                return;
            DestroyCeiling(map.SelectMany(m=>m.Data.Values), undo);
        }

        static void RemovePlatformInstance(Transform parent, List<PlatformInstanceData> visuals, int idx)
        {
            if (parent.childCount > idx)
                parent.GetChild(idx).gameObject.DestroyEx();
            
            if (!idx.IsInRange(visuals))
                return;

            visuals[idx].Release();
            visuals.RemoveAt(idx);
        }

        static void EnsurePlatforms(RoomSettings roomSettings, Transform parent, IList<PlatformInstanceData> visuals, int idx, int groundLayer)
        {
            GetOrCreatePlatform(roomSettings.PlatformPrefab, parent, idx, out var platformGo, 
                out var platformTr, out var collider, out var shadowCaster);
            platformGo.layer = groundLayer;
            
            while (visuals.Count <= idx)
                visuals.Add(PlatformInstanceData.Default(roomSettings));
            var visual = visuals[idx];

            InitPlatformInstance(platformTr, ref visual);

            visual.PlatformGo = platformGo;
            visual.PlatformTr = platformTr;
            visual.Collider = collider;
            visual.ShadowCaster = shadowCaster;
            visual.PlatformLayer = groundLayer;

            visuals[idx] = visual;
        }

        static void InitPlatformInstance(Transform platformTr, ref PlatformInstanceData visual)
        {
            for (var i = 0; i < platformTr.childCount; i++)
            {
                var materialLayerTr = platformTr.GetChild(i);
                var materialLayerGo = materialLayerTr.gameObject;
                // todo: always set inactive before building
                // materialLayerGo.SetActive(false);

                materialLayerTr.TryGetComponent(out MeshRenderer mr);
                materialLayerTr.TryGetComponent(out MeshFilter mf);
                Debug.Assert(mr != null && mf != null);

                var data = new PlatformVisualData()
                {
                    Go = materialLayerGo,
                    Renderer = mr,
                    Filter = mf,
                    Mesh = mf.sharedMesh
                };

                Debug.Assert(mr.sharedMaterial != null);
                visual.Data[mr.sharedMaterial] = data;
            }
        }
        
        static void GetOrCreatePlatform(GameObject prefab, Transform parent, int idx, out GameObject platformGo, out Transform platformTr,
            out MeshCollider coll, out MeshFilter shadowCaster)
        {
            if (parent.childCount <= idx)
            {
                parent.CreatePooledChildObject(prefab, idx.ToString(), out platformGo, out platformTr, 
                    HideFlags.DontSave);
                platformGo.TryGetComponent(out coll);
                platformGo.TryGetComponent(out shadowCaster);
                platformTr.localPosition = new Vector3(0, 0, 0);
            }
            else
            {
                platformTr = parent.GetChild(idx);
                platformGo = platformTr.gameObject;
                platformGo.TryGetComponent(out coll);
                platformGo.TryGetComponent(out shadowCaster);
            }

            if (coll == null)
                coll = platformGo.AddComponent<MeshCollider>();
            if (shadowCaster == null)
                shadowCaster = platformGo.AddComponent<MeshFilter>();
            if (coll.sharedMesh == null)
                coll.sharedMesh = new Mesh();
        }
        #endregion

        #region Ceiling
        public static PlatformVisualData GetCeiling(this ref RoomVisualData rvd, int levelIdx) => rvd.m_ceilingVisuals[levelIdx];

        static void DestroyCeiling(IEnumerable<PlatformVisualData> visuals, bool undo = false)
        {
            if (visuals == null)
                return;
            foreach (var v in visuals)
            {
                if (v.Mesh != null) 
                    v.Mesh.DestroyEx(undo);
            }
        }

        static void GetOrCreateCeiling(GameObject prefab, Transform parent, int idx, out GameObject visualGo, 
            out MeshRenderer rend, out MeshFilter filter)
        {
            if (parent.childCount <= idx)
            {
                parent.CreatePooledChildObject(prefab,idx.ToString(), out visualGo, out var visualTr, HideFlags.DontSave);
                visualTr.localPosition = new Vector3(0, 0, 0);
            }
            else
            {
                var tr = parent.GetChild(idx);
                visualGo = tr.gameObject;
            }

            visualGo.TryGetComponent(out rend);
            visualGo.TryGetComponent(out filter);
        }

        static void RemoveCeilingVisuals(Transform parent, List<PlatformVisualData> visuals, int idx)
        {
            if (parent.childCount > idx)
                parent.GetChild(idx).gameObject.DestroyEx();

            if (!idx.IsInRange(visuals))
                return;

            visuals[idx].Release();
            visuals.RemoveAt(idx);
        }

        static void EnsureCeilingVisuals(RoomSettings settings, Transform parent, IList<PlatformVisualData> visuals, int idx, Material m)
        {
            GetOrCreateCeiling(settings.PlatformVisualPrefab, parent, idx, out var visualGo, out var rend, out var filter);
            SetMeshAndMaterial(m, rend, filter, out var mesh);
            SetVisualData(visuals, idx, filter, visualGo, rend, mesh);
        }

        static void SetVisualData(IList<PlatformVisualData> visuals, 
            int idx, MeshFilter filter, GameObject visualGo,
            MeshRenderer rend, Mesh mesh)
        {
            var data = new PlatformVisualData()
            {
                Filter = filter,
                Go = visualGo,
                Renderer = rend,
                Mesh = mesh,
            };

            while (visuals.Count <= idx)
                visuals.Add(default);

            visuals[idx] = data;
        }

        static void SetMeshAndMaterial(Material m, Renderer rend, MeshFilter filter, out Mesh mesh)
        {
            rend.sharedMaterial = m;
            mesh = filter.sharedMesh;
            if (mesh != null) 
                return;
            mesh = new Mesh { hideFlags = HideFlags.DontSave };
            filter.mesh = mesh;
        }
        #endregion
    }
}
