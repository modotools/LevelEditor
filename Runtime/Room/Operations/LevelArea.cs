using System.Collections.Generic;
using Level.PlatformLayer;
using Level.Enums;
using UnityEngine;
using static Level.PlatformLayer.Operations;

namespace Level.Room.Operations
{
    public class LevelArea : System.IComparable<LevelArea>
    {
        public readonly List<Vector2Int> TilePositions;
        public readonly List<Vector2Int> EdgeTiles;
        public readonly List<LevelArea> ConnectedAreas;
        public readonly int LevelAreaSize;
        public bool IsAccessibleFromMainLevelArea;
        public bool IsMainArea;

        public LevelArea() { }

        public LevelArea(List<Vector2Int> areaTilePositions, IRoomInstance r, int levelIdx)
        {
            TilePositions = areaTilePositions;
            LevelAreaSize = TilePositions.Count;
            ConnectedAreas = new List<LevelArea>();
            EdgeTiles = new List<Vector2Int>();

            var lvl = r.GetPlatformLayer(levelIdx);
            var floorFilter = r.RoomSettings.GetFilter(GeometryType.Floor);

            foreach (var tilePos in TilePositions)
            {
                for (var x = tilePos.x - 1; x <= tilePos.x + 1; ++x)
                    for (var z = tilePos.y - 1; z <= tilePos.y + 1; ++z)
                    {
                        var adjacent = x == tilePos.x || z == tilePos.y;
                        if (!adjacent)
                            continue;

                        if (!lvl.IsInside(new Vector2Int(x - lvl.Position.x, z - lvl.Position.y)))
                            continue;
                        var tile = GetTile(lvl, lvl, new Vector2Int(x, z));
                        if (!floorFilter.IsTileActive(tile))
                            continue;

                        EdgeTiles.Add(tilePos);
                    }
            }
        }

        public void SetAccessibleFromMainLevelArea()
        {
            if (IsAccessibleFromMainLevelArea)
                return;
            IsAccessibleFromMainLevelArea = true;
            foreach (var area in ConnectedAreas)
                area.SetAccessibleFromMainLevelArea();
        }

        public static void ConnectLevelAreas(LevelArea areaA, LevelArea areaB)
        {
            if (areaA.IsAccessibleFromMainLevelArea)
                areaB.SetAccessibleFromMainLevelArea();
            else if (areaB.IsAccessibleFromMainLevelArea)
                areaA.SetAccessibleFromMainLevelArea();

            areaA.ConnectedAreas.Add(areaB);
            areaB.ConnectedAreas.Add(areaA);
        }

        public bool IsConnected(LevelArea otherArea) => ConnectedAreas.Contains(otherArea);
        public int CompareTo(LevelArea other) => other.LevelAreaSize.CompareTo(LevelAreaSize);
    }
}