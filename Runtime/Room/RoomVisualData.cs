﻿using System.Collections.Generic;
using UnityEngine;

namespace Level.Room
{
    public readonly struct RoomVisualData
    {
        public RoomVisualData(RoomSettings roomSettings, Transform floor, Transform ceiling)
        {
            m_roomSettings = roomSettings;
            m_floorVisualsParent = floor;
            m_ceilingVisualsParent = ceiling;
            m_floorVisuals = new List<PlatformInstanceData>();
            m_ceilingVisuals = new List<PlatformVisualData>();
        }

        internal readonly RoomSettings m_roomSettings;
        internal readonly Transform m_floorVisualsParent;
        internal readonly Transform m_ceilingVisualsParent;
        internal readonly List<PlatformInstanceData> m_floorVisuals;
        internal readonly List<PlatformVisualData> m_ceilingVisuals;

        public bool IsValid =>
            m_floorVisualsParent != null && m_ceilingVisualsParent != null && 
            m_floorVisuals != null && m_ceilingVisuals != null;

        public int FloorCount => m_floorVisuals?.Count ?? -1;
        public int CeilingCount => m_ceilingVisuals?.Count ?? -1;
    }
}
