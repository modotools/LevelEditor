using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.Interface;
using Level.Enums;
using Level.PlatformLayer.Interface;
using ScriptableUtility;
using UnityEngine;

using static Level.Room.Operations.RoomOperations;

namespace Level.Room
{
    [Serializable]
    public struct RoomInstanceData : IRoom
    {
        public RoomInstanceData(RoomConfig roomConfig, GameObject go)
        {
            RoomConfig = roomConfig;
            GameObject = go;
            go.TryGetComponent(out IProvider<IContext> ctx);
            ContextProvider = new RefIContextProvider() {Result = ctx};

            OpenExits = new List<ExitInfo>();
            ClosedExits = new List<ExitInfo>();

            Position = default;
            Bounds = default;
            VisualData = default;
            PrefabParent = null;
            Layer = null;

            this.InitLayer();
        }
        
        public GameObject GameObject;
        public RefIContextProvider ContextProvider;

        public RoomConfig RoomConfig;

        public List<ExitInfo> OpenExits;
        public List<ExitInfo> ClosedExits;
        public List<ExitInfo> ExitRestrictions => RoomConfig != null ? RoomConfig.ExitRestrictions : null;
        public List<ExitConfiguration> ValidExitConfiguration => RoomConfig != null ? RoomConfig.ValidExitConfiguration : null;
        public List<int> ValidCardinalMap => RoomConfig != null ? RoomConfig.ValidCardinalMap : null;
        public RestrictionMode RestrictionModeEntry => RoomConfig != null ? RoomConfig.RestrictionModeEntry : RestrictionMode.AllowFirst;
        public RestrictionMode RestrictionModeExit => RoomConfig != null ? RoomConfig.RestrictionModeExit : RestrictionMode.AllowFirst;
        public bool AllowRotate => RoomConfig != null && RoomConfig.AllowRotate;
        public bool AllowMirror => RoomConfig != null && RoomConfig.AllowMirror;

        public IPlatformLayer[] Layer;

        public Vector3Int Position;
        public Bounds Bounds;
        public RoomVisualData VisualData;
        public Transform PrefabParent;

        public Vector3Int Size => RoomConfig != null ? RoomConfig.Size : Vector3Int.zero; 
        public Vector2Int TileDim => RoomConfig != null ? RoomConfig.TileDim : Vector2Int.zero;

        public int MaxPlatformCount => RoomConfig != null ? RoomConfig.MaxPlatformCount : 0;
        public RoomSettings RoomSettings => RoomConfig.RoomSettings;

        public Transform Transform => GameObject != null ? GameObject.transform : null;
        public IContext RoomContext
        {
            get
            {
                if (ContextProvider.Result == null)
                    return null;
                ContextProvider.Result.Get(out var ctx);
                return ctx;
            }
        }
         
        public IEnumerable<IPlatformLayer> PlatformLayer
        {
            get
            {
                #if UNITY_EDITOR
                if (!Application.isPlaying)
                    return RoomConfig.PlatformLayer;
                #endif
                return Layer;
            }
        }

        public int PlatformCount
        {
            get
            {
#if UNITY_EDITOR
                if (!Application.isPlaying)
                    return RoomConfig.PlatformCount;
#endif
                return Layer.Length;
            }
        }

        public IPlatformLayer GetPlatformLayer(int idx)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                return RoomConfig.GetPlatformLayer(idx);
#endif
            Debug.Assert(idx.IsInRange(Layer));
            return Layer[idx];
        }

        public void SetLayer(List<IPlatformLayer> layer)
        {
            // todo: pool data, release old data
            Layer = layer.ToArray();
        }
    }
}