using System.Collections.Generic;
using Core.Interface;
using Core.Unity.Interface;
using Level.Enums;
using Level.PlatformLayer.Interface;
using UnityEngine;

#if UNITASK
using System.Threading;
using Cysharp.Threading.Tasks;
#endif

namespace Level.Room
{
    public interface IRoomInstance : IComponent, IRoom, IProvider<Bounds>
    {
        public ref RoomInstanceData Data { get; }

        public List<ExitInfo> ExitRestrictions { get; }
        public List<ExitConfiguration> ValidExitConfiguration { get; }
        public List<int> ValidCardinalMap { get; }
        
        public RestrictionMode RestrictionModeEntry { get; }
        public RestrictionMode RestrictionModeExit { get; }

        public bool AllowRotate { get; }
        public bool AllowMirror { get; }
        
        public List<ExitInfo> OpenExits { get; }
        public List<ExitInfo> ClosedExits { get; }

        public Transform PrefabParent { get; }
        // can be null:
        public RoomConfig Config { get; }

        Vector3Int Position { get; }
        Bounds Bounds { get; }
        bool IsBuilt { get; }

        void SetLevel(int positionY);

#if UNITASK
        UniTask AsyncBuild(CancellationToken ct);
#endif

        void Show();
        void Hide();

        void SetPlatformLevels(List<IPlatformLayer> newLevels);
    }
}