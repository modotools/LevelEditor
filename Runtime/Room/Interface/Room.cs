using System.Collections.Generic;
using Level.PlatformLayer.Interface;
using UnityEngine;

namespace Level.Room
{
    public interface IRoom
    {
        IEnumerable<IPlatformLayer> PlatformLayer { get; }
        int MaxPlatformCount { get; }

        RoomSettings RoomSettings { get; }

        public Vector3Int Size { get; }
        public Vector2Int TileDim { get; }

        int PlatformCount { get; }
        IPlatformLayer GetPlatformLayer(int idx);
    }
}