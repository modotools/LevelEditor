﻿using System;
using Core.Unity.Extensions;
using UnityEngine;

namespace Level.Room
{
    [Serializable]
    public struct RoomConfigData
    {
        public string Name;
        public Vector3Int Size;
        public RoomSettings Settings;

        public Vector2Int TileDim => Size.Vector2Int() + Vector2Int.one;
        public int MaxPlatformCount => Size.y - 1;
    }
}
