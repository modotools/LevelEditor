using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Types;
using Level.Generation;
using UnityEngine;

namespace Level.Room
{
    public static class OctreeInstance
    {
        public static Octree<IRoomInstance> Rooms;
        public static Octree<IWorkerArea> WorkerArea;

        public static bool HasOctreeRooms => Rooms != null;
        public static bool HasOctreeWorker => WorkerArea != null;
        
        public static bool TryInitOctreesFromGameObject(GameObject gameObject)
        {
            var hasRoomOctree = gameObject.TryGet(out Octree<IRoomInstance> roomTree);
            var hasWorkerOctree = gameObject.TryGet(out Octree<IWorkerArea> workerTree);
            
            if (hasRoomOctree) 
                Rooms = roomTree;
            if (hasWorkerOctree) 
                WorkerArea = workerTree;

            if (Rooms == null)
            {
                Debug.LogError($"No {nameof(Octree<IRoomInstance>)} found!");
                return false;
            }
            if (WorkerArea == null)
            {
                Debug.LogError($"No {nameof(Octree<IRoomInstance>)} found!");
                return false;
            }

            return true;
        }
    }
}
