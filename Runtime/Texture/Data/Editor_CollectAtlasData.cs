using System.Collections.Generic;
using Level.Texture;

namespace Level.Data
{
#if UNITY_EDITOR
    public struct Editor_CollectAtlasData
    {
        public Dictionary<AtlasDataConfig, Editor_AtlasDataValue> Data;

        public static Editor_CollectAtlasData Default => new Editor_CollectAtlasData()
        {
            Data = new Dictionary<AtlasDataConfig, Editor_AtlasDataValue>()
        };
    }

    public struct Editor_AtlasDataValue
    {
        public AtlasOutputData Output;
        public List<IAtlasOutputReceiver> Receivers;
        public List<TextureData> TextureData;

        public static Editor_AtlasDataValue Default => new Editor_AtlasDataValue()
        {
            Receivers = new List<IAtlasOutputReceiver>(),
            TextureData = new List<TextureData>()
        };
    }
#endif

    public interface IAtlasOutputReceiver
    {
#if UNITY_EDITOR
        void Editor_SetOutput(AtlasOutputData m);
#endif
    }
}
