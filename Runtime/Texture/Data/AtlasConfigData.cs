using UnityEngine;
using System;
using System.Collections.Generic;

namespace Level.Texture
{
    [Serializable]
    public struct AtlasConfigData
    {
        public string Name;

        public Material TemplateMaterial;
        public Shader Shader;// => Material!=null?Material.shader:null;
        public string[] TextureChannelNames;

        public int AtlasDimension; // = 1024;
        public int HalfTileDimension; // = 16;

        public int RepeatedPixels;

        public float UVTileSize;
        public float UVInset;

        public static AtlasConfigData Default => new AtlasConfigData()
        {
            AtlasDimension = 1024,
            HalfTileDimension = 16,
        };
    }

    [Serializable]
    public struct AtlasOutputData
    {
        public List<string> UsedTexturePixelHashes;
        public Texture2D[] Atlas; // = new Texture2D[1];
        public Material OutputMaterial;

        public static AtlasOutputData Default => new AtlasOutputData()
        {
            UsedTexturePixelHashes = new List<string>(8),
            Atlas = new Texture2D[1],
            OutputMaterial = null
        };
    }
}