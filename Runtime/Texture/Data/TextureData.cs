using UnityEngine;
using System;
using System.Collections.Generic;
using Level.Tiles;

namespace Level.Texture
{
    public static class Utility
    {
        public static int SortBiggestDimensionFirst(UVLayoutConfig typeConfig, TextureData dat1, TextureData dat2)
        {
            var dim1 = dat1.TileDimension(typeConfig);
            var dim2 = dat2.TileDimension(typeConfig);
            if (dim1.y > dim2.y) return -1;
            if (dim1.y < dim2.y) return 1;
            if (dim1.x > dim2.x) return -1;
            if (dim1.x < dim2.x) return 1;
            return 0;
        }
    }

    [Serializable]
    public class TextureData
    {
        public TextureData(int texChannels, TileBuilderTypeID id)
        {
            m_textures = new Texture2D[texChannels];
            TypeID = id;
        }

        [SerializeField] Texture2D[] m_textures;

        public Vector2 UVOffset;
        public TileBuilderTypeID TypeID;

        public Texture2D[] Textures => m_textures;
        public Texture2D DiffuseTex => m_textures[0];

        //public TextureTypes Type
        //{
        //    get
        //    {
        //        if (TypeID == null)
        //            return TextureTypes.Invalid;
        //        return Enum.TryParse(TypeID.name, out TextureTypes result) 
        //            ? result 
        //            : TextureTypes.Invalid;
        //    }
        //}

        public void ChangeTextureTypeCount(int texChannels)
            => Array.Resize(ref m_textures, texChannels);


        /// <summary>
        /// is default texture set?
        /// </summary>
        public bool IsSet()
        {
            return m_textures != null
                   && m_textures.Length > 0
                   && m_textures[0] != null;
        }

        public Vector2Int TileDimension(UVLayoutConfig typeConfig) 
            => typeConfig != null 
                ? typeConfig.GetDimension(TypeID) 
                : Vector2Int.one;

        public Texture2D GetTexture(int channel = 0) => m_textures[channel];

        public void SetTexture(int channel, Texture2D tex)
        {
            m_textures[channel] = tex;
            //if (channel == 0 && tex == null) 
            //    TypeID = null; // invalid
        }

        /// <summary>
        /// checks whether data has the texture and if so, puts the typeIdx in the set
        /// </summary>
        public void Has(Texture2D tex, ref HashSet<int> typeIdx)
        {
            for (var i = 0; i < m_textures.Length; i++)
            {
                if (m_textures[i] == tex && !typeIdx.Contains(i))
                    typeIdx.Add(i);
            }
        }
    }

    [Serializable]
    public class TransitionTextureData : TextureData
    {
        public TransitionTextureData(int texChannels, TileBuilderTypeID id) : base(texChannels, id) { }

        public string TexAName;
        public string TexBName;

        public int TexAIdx;
        public int TexBIdx;
    }
}