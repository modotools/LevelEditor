using Core.Extensions;
using Core.Types;
using Level.Data;
using Level.Tiles;
using UnityEngine;

namespace Level.Texture
{
    [CreateAssetMenu(fileName = nameof(TransitionTextureCollectionConfig),
        menuName = "Level/Texture/" + nameof(TransitionTextureCollectionConfig))]
    public class TransitionTextureCollectionConfig : TextureCollectionConfig
    {
        [SerializeField] TileBuilderTypeID m_floor;

        public TextureCollectionConfig A;
        public TextureCollectionConfig B;

        public override void Get(ref MeshMaterialUVSet set, TileBuilderTypeID typeID, 
            Vector2Int tilePos,
            int ms_caseConfigMesh, 
            int ms_caseConfigUV, bool isHard, int variation = -1)
        {
            var meshConfig = isHard ? MeshConfigHard : MeshConfig;
            var meshData = meshConfig.Get(ms_caseConfigMesh);
            if (meshData.MeshVariations.IsNullOrEmpty())
                return;

            //Debug.Log($"Transition tilepos {tilePos} variation {variation}");
            var meshVariationIdx = variation;

            if (!meshVariationIdx.IsInRange(meshData.MeshVariations))
                meshVariationIdx %= meshData.MeshVariations.Length;

            var meshVar= meshData.MeshVariations[meshVariationIdx];

            if (GetUVOffset(typeID, variation, ms_caseConfigUV, tilePos,
                out var uvTileStart, out var uvTileEnd) == OperationResult.Error)
                return;
            
            A.GetUVOffset(m_floor, ms_caseConfigUV,15,  tilePos, out var uvTileStartA, out var uvTileEndA);
            B.GetUVOffset(m_floor, ms_caseConfigUV, 15, tilePos, out var uvTileStartB, out var uvTileEndB);

            //Debug.Log($"uvTileStart {uvTileStart} uvTileEnd {uvTileEnd}" +
            //          $"uvTileStartA {uvTileStartA} uvTileEndA {uvTileEndA}" +
            //          $"uvTileStartB {uvTileStartB} uvTileEndB {uvTileEndB}");
            foreach (var m in meshVar.Meshes)
            {
                var meshMatUVData = new MeshMaterialUVData()
                {
                    Mesh = m,
                    Material = m_targetMaterial,
                    UVStart = uvTileStartA,
                    UVEnd = uvTileEndA,
                    UV2Start = uvTileStartB,
                    UV2End = uvTileEndB,
                    UV3Start = uvTileStart,
                    UV3End = uvTileEnd
                };
                set.MeshMaterialUVData.Add(meshMatUVData);
            }
            
            var colliderMesh = GetColliderData(isHard, ms_caseConfigMesh);
            if (colliderMesh != null)
                set.ColliderMeshes.Add(colliderMesh);
        }
        Mesh GetColliderData(bool isHard, int @case)
        {
            var conf = isHard ? 
                (m_colliderMeshHard != null ? m_colliderMeshHard.Get(@case) : default)
                : (m_colliderMesh != null ? m_colliderMesh.Get(@case) : default);

            if (conf.MeshVariations.IsNullOrEmpty())
                return null;
            return conf.MeshVariations[0].Meshes.IsNullOrEmpty() 
                ? null 
                : conf.MeshVariations[0].Meshes[0];
        }
    }
}