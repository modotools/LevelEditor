﻿using System.Linq;
using Core.Unity.Interface;
using Level.Tiles;
using UnityEngine;
using UnityEngine.Serialization;

namespace Level.Texture
{
    [CreateAssetMenu(fileName = nameof(TextureCollectionLayout), menuName = "Level/Texture/"+nameof(TextureCollectionLayout))]
    public class TextureCollectionLayout : ScriptableObject, IScriptableObject, IVerify
    {
        [FormerlySerializedAs("m_textureTypeConfig")] [SerializeField] 
        UVLayoutConfig m_uvLayoutConfig;
        [FormerlySerializedAs("m_tileBorderTypes")] [FormerlySerializedAs("m_textureTypes")] 
        [SerializeField] 
        TileBuilderTypeID[] m_tileBuilderTypes = new TileBuilderTypeID[0];

        public UVLayoutConfig UVLayoutConfig => m_uvLayoutConfig;
        public TileBuilderTypeID[] TileBuilderTypeIDs => m_tileBuilderTypes;

        public void Editor_Verify(ref VerificationResult result)
        {
            foreach (var type in m_tileBuilderTypes)
            {
                var count = m_tileBuilderTypes.Count(t => type == t);
                if (count > 1)
                    result.Error($"{type} is contained more then once in layout", this);
                if (!m_uvLayoutConfig.GetAllIds().Contains(type))
                    result.Error($"{type} is not contained in given TextureTypeConfig set in layout", this);
            }
        }
    }
}
