using UnityEngine;
using Core.Extensions;
using Core.Unity.Interface;
using Level.Data;

namespace Level.Texture
{
    [CreateAssetMenu(fileName = nameof(TextureCollectionConfigSettings), 
        menuName = "Level/Texture/" + nameof(TextureCollectionConfigSettings))]
    public class TextureCollectionConfigSettings : ScriptableObject, IScriptableObject, IVerify
    {
        [SerializeField] TextureCollectionLayout m_layout;
        [SerializeField] AtlasDataConfig m_atlasData;
        [SerializeField] TileMeshConfig m_meshConfig;
        [SerializeField] TileMeshConfig m_meshConfigHard;

        public TextureCollectionLayout Layout => m_layout;
        public AtlasDataConfig AtlasData => m_atlasData;
        public TileMeshConfig MeshConfig => m_meshConfig;
        public TileMeshConfig MeshConfigHard => m_meshConfigHard;

        public string[] TextureChannelNames => AtlasData == null 
            ? null 
            : AtlasData.ConfigData.TextureChannelNames;
        public int TextureChannels => TextureChannelNames?.Length ?? 1;

#if UNITY_EDITOR
        #region Verify
        public void Editor_Verify(ref VerificationResult result)
        {
            if (Layout == null) 
                result.Error($"No Layout set in {nameof(TextureCollectionConfigSettings)}", this);
            else if (Layout.TileBuilderTypeIDs.IsNullOrEmpty())
                result.Error($"TextureTypeIDs in Layout not set {nameof(TextureCollectionConfigSettings)}", this);
            if (AtlasData == null) 
                result.Error($"No AtlasData set in {nameof(TextureCollectionConfigSettings)}", this);
            if (MeshConfig == null)
                result.Error($"No MeshConfig set in {nameof(TextureCollectionConfigSettings)}", this);
            if (MeshConfigHard == null)
                result.Error($"No MeshConfigHard set in {nameof(TextureCollectionConfigSettings)}", this);
        }
        #endregion
#endif

    }
}