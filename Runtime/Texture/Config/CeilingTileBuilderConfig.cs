using UnityEngine;
using System.Collections.Generic;
using Core.Extensions;
using Core.Interface;
using Core.Unity.Interface;
using Level.Data;
using Level.Tiles;

namespace Level.Texture
{
    [CreateAssetMenu(fileName = nameof(CeilingTileBuilderConfig), 
        menuName = "Level/Texture/" + nameof(CeilingTileBuilderConfig))]
    public class CeilingTileBuilderConfig : ScriptableObject, IScriptableObject, INamed, IVerify,
        ITileMeshMaterialUVProvider
    {
        [SerializeField] TileBuilderTypeID m_builderTypeID;
        [SerializeField] TileMeshConfig m_meshConfig;
        [SerializeField] TileMeshConfig m_meshConfigHard;
        [SerializeField] Material m_targetMaterial;

        public string Name => name;

        public Texture2D PreviewTex { get; }

        public void Get(ref MeshMaterialUVSet set, TileBuilderTypeID typeID, 
            Vector2Int tilePos,
            int ms_caseConfigMesh, 
            int ms_caseConfigUV, bool isHard, int variation = -1)
        {
            if (typeID != m_builderTypeID)
                return;

            var meshConfig = isHard ? m_meshConfigHard : m_meshConfig;
            var meshData = meshConfig.Get(ms_caseConfigMesh);
            if (meshData.MeshVariations.IsNullOrEmpty())
                return;
            if (!variation.IsInRange(meshData.MeshVariations))
                variation = variation % meshData.MeshVariations.Length; // Random.Range(0, meshData.MeshVariations.Length);
            var meshVar = meshData.MeshVariations[variation];
            foreach (var m in meshVar.Meshes)
            {
                var meshMatUVData = new MeshMaterialUVData()
                {
                    Mesh = m,
                    Material = m_targetMaterial,
                };
                set.MeshMaterialUVData.Add(meshMatUVData);
            }
        }

        public void GetAvailableBuilderTypeIDs(int ms_caseConfiguration, bool isHard, List<TileBuilderTypeID> borderTypeIds) 
            => borderTypeIds.Add(m_builderTypeID);


#if UNITY_EDITOR
        public void Editor_CollectAllAtlasData(Editor_CollectAtlasData atlasToTexture) { return; }
        public void Editor_Verify(ref VerificationResult result)
        {
        }
#endif
    }
}