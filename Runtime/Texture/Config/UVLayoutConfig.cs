﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Level.Tiles;
using UnityEngine;
using UnityEngine.Serialization;

namespace Level.Texture
{
    [CreateAssetMenu(fileName = nameof(UVLayoutConfig), menuName = "Level/Texture/"+nameof(UVLayoutConfig))]
    public class UVLayoutConfig : ScriptableObject
    {
        [Serializable]
        public struct UVLayoutData
        {
            public TileBuilderTypeID ID;
            public string Name;
            public TileBuilderTypeID[] IDs;
			// when using multiple IDs you can use one dimension for all or you have to specify for each one (IDs.Length == Dimensions.Length)
            public Vector2Int[] Dimensions;
            // can be null or needs 16 elements
            public Vector2[] Configuration_TextureOffsets;
            public int SplitTiles;
        }

        [FormerlySerializedAs("m_textureTypeData")] [SerializeField] UVLayoutData[] m_uvLayoutData = new UVLayoutData[0];

        #if UNITY_EDITOR
        public ref UVLayoutData[] Editor_UVLayoutData => ref m_uvLayoutData;
        public static string Editor_UVLayoutPropName => nameof(m_uvLayoutData);
        #endif

        UVLayoutData m_invalidData;
        // ReSharper disable once SuggestBaseTypeForParameter
        ref UVLayoutData GetData(TileBuilderTypeID id)
        {
            var idx = Array.FindIndex(m_uvLayoutData, a => a.IDs.Contains(id));
            if (idx == -1)
                return ref m_invalidData;
            return ref m_uvLayoutData[idx];
        }

        public Vector2Int GetDimension(TileBuilderTypeID id)
        {
            var data = GetData(id);
            if (data.Dimensions.IsNullOrEmpty())
                return Vector2Int.one;
            if (data.Dimensions.Length == 1)
                return data.Dimensions[0];

            var idx = Array.IndexOf(data.IDs, id);
            return data.Dimensions[idx];
        }

        public Vector2 GetOffset(TileBuilderTypeID id, Vector2Int tilePos, int configuration)
        {
            var data = GetData(id);

            var posOffset = GetPositionOffset(tilePos, data);
            
            if (data.Configuration_TextureOffsets.IsNullOrEmpty())
                return posOffset;

            var idx = configuration.IsInRange(data.Configuration_TextureOffsets) 
                ? configuration 
                : 0;

            return data.Configuration_TextureOffsets[idx] + posOffset;
        }

        static Vector2 GetPositionOffset(Vector2Int tilePos, UVLayoutData data)
        {
            var posOffset = Vector2.zero;
            if (data.SplitTiles <= 0)
                return posOffset;

            var x = tilePos.x % data.SplitTiles;
            var y = tilePos.y % data.SplitTiles;
            posOffset.x += x;
            posOffset.y += y;

            //Debug.Log($"tilePos {tilePos} pos offset {posOffset}");
            return posOffset;
        }

        public IEnumerable<TileBuilderTypeID> GetAllIds() => m_uvLayoutData.SelectMany(d => d.IDs);
    }
}
