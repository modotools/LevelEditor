using Core.Unity.Interface;
using UnityEngine;
using UnityEngine.Serialization;

namespace Level.Texture
{
    [CreateAssetMenu(fileName = nameof(AtlasDataConfig), menuName = "Level/Texture/" + nameof(AtlasDataConfig))]
    public class AtlasDataConfig : ScriptableObject, IScriptableObject, IVerify
    {
        [FormerlySerializedAs("m_data")] [SerializeField] AtlasConfigData m_configData;

        public AtlasConfigData ConfigData => m_configData;
        public string Name => ConfigData.Name;


#if UNITY_EDITOR
        public ref AtlasConfigData Editor_ConfigData => ref m_configData;
        public static string Editor_DataPropName => nameof(m_configData);

        public void Editor_Verify(ref VerificationResult result)
        {
            var tileDimension = 2 * m_configData.HalfTileDimension;

            if (!Mathf.IsPowerOfTwo(tileDimension))
                result.Error("Atlas TileDimension is not power of two", this);
        }
#endif
    }
}