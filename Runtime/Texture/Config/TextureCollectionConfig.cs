using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using Core.Extensions;
using Core.Interface;
using Core.Types;
using Core.Unity.Interface;
using Level.Data;
using Level.Tiles;
using UnityEngine.Serialization;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Level.Texture
{
    [CreateAssetMenu(fileName = nameof(TextureCollectionConfig), 
        menuName = "Level/Texture/" + nameof(TextureCollectionConfig))]
    public class TextureCollectionConfig : ScriptableObject, IScriptableObject, INamed, IVerify,
        ITileMeshMaterialUVProvider, IAtlasOutputReceiver,
        IEnumerable<TextureData>, IEnumerator<TextureData>
    {
        [SerializeField] TextureCollectionConfigSettings m_settings;
        [SerializeField] protected TextureCollectionConfigData[] m_data;
        [SerializeField] protected Material m_targetMaterial;
        [SerializeField] protected TileMeshConfig m_colliderMesh;
        [SerializeField] protected TileMeshConfig m_colliderMeshHard;
        public string Name => name;
        public TextureCollectionConfigData[] Data => m_data;

        #region Settings Properties
        public TextureCollectionLayout Layout => m_settings == null ? null : m_settings.Layout;
        public TileMeshConfig MeshConfig =>  m_settings == null ? null: m_settings.MeshConfig;
        public TileMeshConfig MeshConfigHard =>  m_settings == null ? null: m_settings.MeshConfigHard;

        public AtlasDataConfig AtlasData =>  m_settings == null ? null: m_settings.AtlasData;
        public string[] TextureChannelNames => AtlasData == null 
            ? null 
            : AtlasData.ConfigData.TextureChannelNames;
        public int TextureChannels => TextureChannelNames?.Length ?? 1;

        #endregion


        #region Numerate
        int m_position = -1;
        public TextureData Current
        {
            get
            {
                if (m_position < 0)
                    return null;
                if (m_data.IsNullOrEmpty())
                    return null;

                var length = 0;
                foreach (var dat in m_data)
                {
                    if (dat.Variations.IsNullOrEmpty())
                        continue;
                    var nextLength = length + dat.Variations.Length;
                    if (m_position < nextLength)
                        return dat.Variations[m_position - length];
                    length = nextLength;
                }

                throw new InvalidOperationException();
            }
        }
        object IEnumerator.Current => Current;

        public int Count() => m_data?.Where(d => !d.Variations.IsNullOrEmpty())
            .Sum(d => d.Variations.Length) ?? 0;
        public bool MoveNext()
        {
            m_position++;
            return (m_position < Count());
        }
        public void Dispose() { Reset(); }
        public void Reset() { m_position = -1; }
        IEnumerator IEnumerable.GetEnumerator() { m_position = -1; return this; }
        public IEnumerator<TextureData> GetEnumerator() { m_position = -1; return this; }
        #endregion

        public Texture2D PreviewTex
        {
            get
            {
                using var enumerator = GetEnumerator();
                enumerator.MoveNext();
                return enumerator.Current?.DiffuseTex;
            }
        }

        public virtual void Get(ref MeshMaterialUVSet set, TileBuilderTypeID typeID, 
            Vector2Int tilePos,
            int ms_caseConfigMesh, 
            int ms_caseConfigUV, bool isHard, int variation = -1)
        {
            var meshConfig = isHard ? MeshConfigHard : MeshConfig;
            var meshData = meshConfig.Get(ms_caseConfigMesh);
            if (meshData.MeshVariations.IsNullOrEmpty())
                return;

            var meshVariationIdx = variation;

            if (!meshVariationIdx.IsInRange(meshData.MeshVariations))
                meshVariationIdx %= meshData.MeshVariations.Length;

            var meshVar= meshData.MeshVariations[meshVariationIdx];

            if (GetUVOffset(typeID, variation, ms_caseConfigUV, tilePos,
                out var uvTileStart, out var uvTileEnd) == OperationResult.Error)
                return;

            foreach (var m in meshVar.Meshes)
            {
                var meshMatUVData = new MeshMaterialUVData()
                {
                    Mesh = m,
                    Material = m_targetMaterial,
                    UVStart = uvTileStart,
                    UVEnd = uvTileEnd
                };
                set.MeshMaterialUVData.Add(meshMatUVData);
                // set.ColliderMeshes.Add(m);
            }
            var colliderMesh = GetColliderData(isHard, ms_caseConfigMesh);
            if (colliderMesh != null)
                set.ColliderMeshes.Add(colliderMesh);
        }
        
        Mesh GetColliderData(bool isHard, int @case)
        {
            var conf = isHard ? 
                (m_colliderMeshHard != null ? m_colliderMeshHard.Get(@case) : default)
                : (m_colliderMesh != null ? m_colliderMesh.Get(@case) : default);

            if (conf.MeshVariations.IsNullOrEmpty())
                return null;
            return conf.MeshVariations[0].Meshes.IsNullOrEmpty() 
                ? null 
                : conf.MeshVariations[0].Meshes[0];
        }

        public void GetAvailableBuilderTypeIDs(int ms_caseConfiguration, bool isHard, List<TileBuilderTypeID> borderTypeIds) 
            => borderTypeIds.AddRange(m_data.Select(d => d.TileBuilderTypeID));

        internal OperationResult GetUVOffset(TileBuilderTypeID typeID, int variation, int ms_caseConfig, Vector2Int tilePos,
            out Vector2 uvTileStart, out Vector2 uvTileEnd)
        {
            uvTileStart = Vector2.zero;
            uvTileEnd = Vector2.one;
            
            var data = m_data.FirstOrDefault(d => d.TileBuilderTypeID == typeID);
            var uvVariationIdx = variation;

            if (data.Variations.IsNullOrEmpty())
                return OperationResult.Error;

            if (!uvVariationIdx.IsInRange(data.Variations))
                uvVariationIdx %= data.Variations.Length;
            var varData = data.Variations[uvVariationIdx];


            GetUVOffset(varData, ms_caseConfig, tilePos, out uvTileStart, out uvTileEnd);
            return OperationResult.OK;
        }

        internal void GetUVOffset(TextureData textureData, int ms_caseConfig, Vector2Int tilePos,
            out Vector2 uvTileStart, out Vector2 uvTileEnd)
        {
            var offset = textureData.UVOffset;
            var atlas = AtlasData.ConfigData;

            uvTileStart = offset + (atlas.UVInset * Vector2.one);
            uvTileEnd = uvTileStart + (atlas.UVTileSize * Vector2.one);

            var layoutOffset = atlas.UVTileSize * Layout.UVLayoutConfig.GetOffset(textureData.TypeID, tilePos, ms_caseConfig);
            uvTileStart += layoutOffset;
            uvTileEnd += layoutOffset;
        }
        
#if UNITY_EDITOR
        public void Editor_CollectAllAtlasData(Editor_CollectAtlasData atlasToTexture)
        {
            var atlasDataExists = atlasToTexture.Data.ContainsKey(AtlasData);
            var data = atlasDataExists
                ? atlasToTexture.Data[AtlasData]
                : Editor_AtlasDataValue.Default;
            
            if (!atlasDataExists)
                atlasToTexture.Data.Add(AtlasData, data);

            data.Receivers.Add(this);
            data.TextureData.AddRange(this);
            atlasToTexture.Data[AtlasData] = data;
        }

        public void Editor_SetOutput(AtlasOutputData output)
        {
            m_targetMaterial = output.OutputMaterial;
            EditorUtility.SetDirty(this);
        }

        public void Editor_SetData(TextureCollectionConfigData[] data) => m_data = data;
        public ref TextureCollectionConfigData[] Editor_Data => ref m_data;

        public static string Editor_SettingsPropertyName => nameof(m_settings);
        public static string Editor_DataPropertyName => nameof(m_data);

        #region Verify
        public void Editor_Verify(ref VerificationResult result)
        {
            Editor_VerifyAtlasLayout(out var atlasLayoutResult);
            result.Merge(atlasLayoutResult);
            if (atlasLayoutResult.Errors > 0) 
                return;

            for (var i = 0; i < Data.Length; i++)
            {
                var d = Data[i];
                if (Layout.TileBuilderTypeIDs[i] != d.TileBuilderTypeID)
                    result.Error($"TextureTypeID in position {i} is not same as in Layout {nameof(TextureCollectionConfig)}",
                        this);

                Editor_VerifyVariations(d, ref result);
            }
        }

        void Editor_VerifyVariations(TextureCollectionConfigData d, ref VerificationResult result)
        {
            foreach (var v in d.Variations)
            {
                if (v.Textures.IsNullOrEmpty())
                    result.Error($"{d.TileBuilderTypeID} has Variation without Textures!", this);

                if (v.Textures.Length != TextureChannels)
                    result.Error($"{d.TileBuilderTypeID} has Variation with {v.Textures.Length} channels, " +
                                 $"but should have {TextureChannels} !", this);

                Editor_VerifyTextures(d, v, ref result);
            }
        }

        void Editor_VerifyTextures(TextureCollectionConfigData d, TextureData v, ref VerificationResult result)
        {
            if (v.TypeID != d.TileBuilderTypeID)
                result.Error($"TextureTypeID in textureData is {v.TypeID}, expected  {d.TileBuilderTypeID}", this);

            foreach (var t in v.Textures)
            {
                if (t == null)
                    result.Error($"There is a NULL-entry in {d.TileBuilderTypeID} variation", this);
                else Editor_VerifyTexture(t, ref result);
            }
        }

        void Editor_VerifyTexture(UnityEngine.Texture texture, ref VerificationResult result)
        {
            var tileDimension = 2 * AtlasData.ConfigData.HalfTileDimension;

            Debug.Assert(texture != null);
            if (texture.width < tileDimension || texture.height < tileDimension)
                result.Error($"{texture} size is smaller then TileDimension in Atlas", this);
            if (!Mathf.IsPowerOfTwo(texture.width) || !Mathf.IsPowerOfTwo(texture.height))
                result.Error($"{texture} size is not power of two", this);
            if (texture.width % tileDimension != 0 || texture.width % tileDimension != 0) 
                result.Error($"{texture} size is not multiple of TileDimension in Atlas", this);
        }

        void Editor_VerifyAtlasLayout(out VerificationResult result)
        {
            result = VerificationResult.Default;
            if (Layout == null) 
                result.Error($"No Layout set in {nameof(TextureCollectionConfig)}", this);
            else
            {
                if (Layout.TileBuilderTypeIDs.IsNullOrEmpty())
                    result.Error($"TextureTypeIDs in Layout not set {nameof(TextureCollectionConfig)}", this);
                if (Layout.TileBuilderTypeIDs.Length != Data.Length)
                    result.Error($"TextureTypeIDs in Layout and Data not of same Length! {nameof(TextureCollectionConfig)}",
                        this);
            }
            if (AtlasData == null) 
                result.Error($"No AtlasData set in {nameof(TextureCollectionConfig)}", this);
        }
        #endregion
#endif
    }

    [Serializable]
    public struct TextureCollectionConfigData
    {
        [FormerlySerializedAs("TileBorderTypeID")] [FormerlySerializedAs("TextureTypeID")] 
        public TileBuilderTypeID TileBuilderTypeID;
        public TextureData[] Variations;
    }
}