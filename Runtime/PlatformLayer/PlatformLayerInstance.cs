using System;
using UnityEngine;

using Level.PlatformLayer.Interface;

namespace Level.PlatformLayer
{
    [Serializable]
    public class PlatformLayerInstance : IPlatformLayer
    {
        public PlatformLayerInstance(PlatformLayerConfig config, Vector3Int posOffset) : this(config) 
            => m_positionOffset = posOffset;

        public PlatformLayerInstance(PlatformLayerConfig config)
        {
            m_config = config;
            m_size = config.Size;
            m_position = config.Position;
            m_positionOffset = Vector3Int.zero;
            m_tiles = config.Tiles.Clone() as ushort[];
            IsDirty = true;
        }

        public PlatformLayerInstance(GridSettings grid, Vector2Int size, Vector2Int pos, Vector3Int off)
        {
            m_grid = grid;

            InitSize(size);
            m_position = pos;
            m_positionOffset = off;

            IsDirty = true;
        }

        // ReSharper disable ConvertToAutoProperty
        // public TilesSetListConfig TilesSet => m_config.TilesSet;

        public Vector2Int Position
        {
            get
            {
#if UNITY_EDITOR
                if (!Application.isPlaying && m_config != null)
                    return m_config.Position;
#endif
                return m_position;
            }
            set
            {
#if UNITY_EDITOR
                if (!Application.isPlaying && m_config != null)
                    m_config.Position = value;
#endif
                m_position = value;
            }
        }
        public Vector2Int Size
        {
            get
            {
#if UNITY_EDITOR
                if (!Application.isPlaying && m_config != null)
                    return m_config.Size;
#endif
                return m_size;
            }
            set
            {
#if UNITY_EDITOR
                if (!Application.isPlaying && m_config != null)
                    m_config.Size = value;
#endif
                m_size = value;
            }
        }
        // ReSharper restore ConvertToAutoProperty

        public Vector3 PositionOffset
        {
            get => m_positionOffset;
            set => m_positionOffset = value;
        }

        public bool IsDirty { get; private set; }

        public ushort[] Tiles
        {
            get
            {
#if UNITY_EDITOR
                if (!Application.isPlaying && m_config != null)
                    return m_config.Tiles;
#endif
                return m_tiles;
            }
        }
        public int Count => Tiles.Length;

        public ushort this[int index]
        {
            get => Tiles[index];
            set
            {
                Tiles[index] = value;
                IsDirty = true;
            }
        }

        public Vector2Int Extends => Position + Size;
        public Vector2Int TileDim => Size + Grid.ExtraDim * Vector2Int.one;
        public GridSettings Grid => m_config != null ? m_config.GridSettings : m_grid;
#pragma warning disable 0649 // wrong warnings for SerializeField
        // for undo we need list of ushort
        [SerializeField, HideInInspector]
        ushort[] m_tiles;
        
        [SerializeField] Vector3 m_positionOffset;

        [SerializeField, HideInInspector] Vector2Int m_position;
        [SerializeField, HideInInspector] Vector2Int m_size;

        [SerializeField] GridSettings m_grid;
        [SerializeField] PlatformLayerConfig m_config;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void InitSize(Vector2Int size)
        {
            Size = size;
            m_tiles = new ushort[TileDim.x * TileDim.y];
        }
        public void SetTiles(ushort[] tiles) => m_tiles = tiles;

        //public void SetTilesSet(TilesSetListConfig config) => m_config.SetTilesSet(config);
        //public void SetGrid(GridSettings config)
        //{
        //    m_grid = config;
        //    if (m_config != null)
        //        m_config.SetGrid(config);
        //}

        public void Get(out IGridData provided) => provided = Grid;
    }
}