using System;
using System.Collections.Generic;
using Core.Types;
using Core.Unity.Extensions;
using Level.Enums;
using Level.PlatformLayer.Interface;
using Level.Tiles;
using UnityEngine;

namespace Level.PlatformLayer
{
    public static class Operations
    {
        public static ushort GetTile(IPlatformLayerSpatialData platform,
            IPlatformLayerTiles tiles,
            Vector2Int pos,
            OutOfLevelBoundsAction action = OutOfLevelBoundsAction.Error)
        {
            //Debug.Log($"GetTile {z} * {platform.TileDim.x} + {x} = {z * platform.TileDim.x + x}");
            if (platform.IsInside(pos))
                return tiles[pos.y * platform.TileDim.x + pos.x];
            switch (action)
            {
                case OutOfLevelBoundsAction.Error:
                    Debug.LogError($"tile {pos} is out of level bounds");
                    break;
                case OutOfLevelBoundsAction.ExpandBounds:
                    Debug.LogError("ExpandBounds not supported in this method");
                    break;
                case OutOfLevelBoundsAction.IgnoreTile:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(action), action, null);
            }
            return ushort.MaxValue;
        }

        public static OperationResult SetTile(IPlatformLayerSpatialData lvlDat, IPlatformLayerTiles tiles,
            Vector2Int lvlPos, TilesSetFilter data, OutOfLevelBoundsAction action = OutOfLevelBoundsAction.Error)
            => SetTile(lvlDat, tiles, lvlPos, data.FilterIdx, data.Data.Mask, data.Data.Shift, action);

        public static OperationResult SetTile(IPlatformLayerSpatialData lvlDat, IPlatformLayerTiles tiles, 
            Vector2Int lvlPos, ushort tile,
            ushort mask, ushort shift = 0, 
            OutOfLevelBoundsAction action = OutOfLevelBoundsAction.Error)
        {
            if (!lvlDat.IsInside(lvlPos))
            {
                switch (action)
                {
                    case OutOfLevelBoundsAction.Error:
                        Debug.LogError("tile is out of level bounds");
                        break;
                    case OutOfLevelBoundsAction.ExpandBounds:
                        Debug.LogError("ExpandBounds not supported in this method");
                        break;
                    case OutOfLevelBoundsAction.IgnoreTile:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(action), action, null);
                }

                return OperationResult.Error;
            }
            var invertedMask = (ushort)~mask;
            var origTile = tiles[lvlPos.y * lvlDat.TileDim.x + lvlPos.x];
            var setTile = (ushort) ((origTile & invertedMask) | (mask & (tile << shift)));
            //Debug.Log(setTile);
            tiles[lvlPos.y * lvlDat.TileDim.x + lvlPos.x] = setTile;
            return OperationResult.OK;
        }

        public static bool IsInside(this IPlatformLayerSpatialData lvl, Vector2Int v) =>
            v.x >= 0 && v.x < lvl.TileDim.x && v.y >= 0 && v.y < lvl.TileDim.y;

        public static bool IsInsideWithoutEdge(this IPlatformLayerSpatialData lvl, Vector2Int v)
            => lvl.IsInsideWithInset(v, 1);
        public static bool IsInsideWithInset(this IPlatformLayerSpatialData lvl, Vector2Int v, int inset)
            => v.x >=inset && v.x < lvl.TileDim.x - inset && v.y >= inset && v.y < lvl.TileDim.y - inset;

        public static IEnumerable<Vector2Int> GetRegionTiles(IPlatformLayerSpatialData lvlDat, IPlatformLayerTiles tiles, 
            Vector2Int startPos,
            ushort compareMask)
        {
            var v2Tiles = new List<Vector2Int>();
            // mark tiles that have already been processed
            var mapFlags = new int[lvlDat.TileDim.x, lvlDat.TileDim.y];

            var tileType = (GetTile(lvlDat, tiles, startPos) & compareMask);

            var queue = new Queue<Vector2Int>();
            queue.Enqueue(startPos);
            mapFlags[startPos.x, startPos.y] = 1;

            // spread-algorithm, select all adjacent tiles with same tileType
            while (queue.Count > 0)
            {
                var tile = queue.Dequeue();
                v2Tiles.Add(tile);

                for (var x = tile.x - 1; x <= tile.x + 1; ++x)
                for (var z = tile.y - 1; z <= tile.y + 1; ++z)
                {
                    var adjacent = (z == tile.y || x == tile.x);
                    if (!adjacent)
                        continue;
                    if (z == tile.y && x == tile.x)
                        continue;

                    var pos = new Vector2Int(x, z);
                    if (!lvlDat.IsInside(pos))
                        continue;
                    if (mapFlags[x, z] != 0 || (GetTile(lvlDat, tiles, pos) & compareMask) != tileType)
                        continue;

                    mapFlags[x, z] = 1;
                    queue.Enqueue(new Vector2Int(x, z));
                }
            }

            return v2Tiles;
        }

        public static bool IsOnEdge(this IPlatformLayer lvl, int x, int z) => x == 0 || x == lvl.TileDim.x - 1 || z == 0 || z == lvl.TileDim.y - 1;

        public static Bounds GetBounds(IPlatformLayer layer)
        {
            layer.Get(out IGridData gridData);
            return GetBounds(layer, layer.PositionOffset, gridData);
        }

        public static Bounds GetBounds(IPlatformLayerSpatialData platform, 
            Vector3 posOffset, IGridData gridData)
        {
            var size = gridData.TileSize * platform.Size.Vector3() + new Vector3(0, gridData.TileHeight, 0);
            var bounds = new Bounds(posOffset + platform.Position.Vector3() + (size / 2), size);
            return bounds;
        }
    }
}