﻿using UnityEngine;

namespace Level.PlatformLayer.Interface
{
    public interface IGridData
    {
        float TileSize { get; }
        float TileHeight { get; }

        int ExtraDim { get; }
        Vector2 Offset { get; }
    }
}
