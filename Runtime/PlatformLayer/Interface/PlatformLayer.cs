﻿using Core.Interface;
using UnityEngine;

namespace Level.PlatformLayer.Interface
{
    public interface IPlatformLayer : IPlatformLayerTiles, IPlatformLayerSpatialData, 
        IProvider<IGridData>
    {
        Vector3 PositionOffset { get; set; }
    }
}
