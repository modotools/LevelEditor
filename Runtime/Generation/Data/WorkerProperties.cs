using System;
using UnityEngine;

namespace Level.Generation
{
    /// <summary>
    /// Some properties to predict some behaviour of the Workers, especially for Space-Management 
    /// </summary>
    [Serializable]
    public struct WorkerProperties
    {
        // door modifier will just modify exits instead of creating rooms
        public bool ExitModifier;
        
        // when creating rooms, how much space is required
        public Vector3Int MinSpace;
        public Vector3Int MaxSpace;
    }
}