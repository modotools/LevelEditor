using System.Collections.Generic;
using Core.Unity.Types;
using Core.Unity.Utility;
using Level.Room;
using UnityEngine;

namespace Level.Generation
{
    public interface IGeneratorData : IOpenEndTracker
    {
        System.Random Random { get; }
        GridSettings GridSettings { get; }
        Bounds CurrentGridBounds { get; }
        IEnumerable<int> RoomDirections  { get; }
        List<ILevelGeneratorWorker> Worker { get; }
        List<ILevelGeneratorWorker> BabyWorker { get; }
        StopwatchFrameSplitter FrameSplitter { get; }

        int DyingCount { get; set; }

        int NewWorkerNr { get; }
        void AddWorker(ILevelGeneratorWorker worker, Vector3 parentPos);
        void AddNewRoom_GetIsVisible(IRoomInstance newRoom, out bool isVisible);
        void DEBUG_AddDebugText(string text);
    }
}
