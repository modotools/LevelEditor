using Level.Room;

namespace Level.Generation
{
    /// <summary>
    /// takes care of managing/ loading/ saving the rooms
    /// </summary>
    public interface ILevelManager
    {
#if UNITY_EDITOR
        bool Editor_IsInitVisible(IRoomInstance component);
#endif
    }  
}