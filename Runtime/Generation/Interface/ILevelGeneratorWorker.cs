using Level.Room;
using UnityEngine;

#if UNITASK
using System.Threading;
using Cysharp.Threading.Tasks;
#endif

namespace Level.Generation
{
    public interface ILevelGeneratorWorker
    {
        string FactoryName { get; }
        string TypeName { get; }

        int WorkerNr { get; }
        Vector3 Position { get; }
        bool Dead { get; }

        bool HasDebugPos { get; }
        Vector3 DebugPos { get; }

        string DeadIconName { get; }
        string IconName { get; }

        public Bounds CurrentExitBounds { get; }
        
        void SetDebugPos(Vector3 debugPos);

        void DebugDrawGizmos();
        bool CanOperate(IGeneratorData data, out bool inFocusArea);

#if UNITASK
        UniTask Operate(IGeneratorData data, CancellationToken cancellationToken);
#else
        void Operate(IGeneratorData data);
#endif

        bool Return();

        void UpdateOperationBounds(IGeneratorData data, IRoomInstance spawnerRoom, ExitInfo atExit);
    }
}
