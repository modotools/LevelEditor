using System;
using Core.Unity.Types;
using UnityEngine;

namespace Level.Generation
{
    public interface IFactoryProvider
    {
        public WorkerProperties WorkerProperties { get; }
        public ILevelGeneratorWorkerFactory GetFactory(IGeneratorData data);
        
        public Texture2D Editor_FactoryIcon { get; }
    }

    [Serializable]
    public class RefIFactoryProvider : InterfaceContainer<IFactoryProvider>
    {
        
    }
}
