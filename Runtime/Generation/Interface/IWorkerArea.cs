using Core;
using Core.Interface;
using Level.Room;
using UnityEngine;

namespace Level.Generation
{
    public interface IWorkerArea : 
        IProvider<Bounds>,
        IProvider<GridSettings>
    {
        bool SpawnAllWorker(IGeneratorData data, ref DebugString errorString);
        bool TrySpawnWorker(IGeneratorData data, IRoomInstance fromRoom, Bounds minOpBounds, 
            ExitInfo exitInfo, IFactoryProvider factoryConfig, out ILevelGeneratorWorker worker, 
            ref DebugString errorString);
    }
}