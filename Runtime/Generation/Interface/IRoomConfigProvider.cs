using System;
using Core.Unity.Types;
using Level.Room;

namespace Level.Generation
{
    public interface IRoomConfigProvider
    {
        RoomConfig GetRoom(IGeneratorData data);
    }

    [Serializable]
    public class RefIRoomConfigProvider : InterfaceContainer<IRoomConfigProvider> { }
}