using System.Collections.Generic;
using Level.Room;

namespace Level.Generation
{
    public interface IOpenEndTracker
    {
        void AddOpenExits(IRoomInstance r, IEnumerable<ExitInfo> exits);
        void AddOpenExit(IRoomInstance r, ExitInfo exit);
        void CloseExit(ExitInfo exit);

        public int OpenEnds { get; }
    }
}
