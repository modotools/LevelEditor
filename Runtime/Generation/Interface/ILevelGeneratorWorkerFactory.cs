using Level.Room;


namespace Level.Generation
{
    public interface ILevelGeneratorWorkerFactory
    {
        public abstract ILevelGeneratorWorker SpawnWorker(IGeneratorData data, IRoomInstance spawnerRoom, ExitInfo atExit);
    }
}
