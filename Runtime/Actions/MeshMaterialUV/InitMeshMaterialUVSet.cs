﻿using System;
using Core.Interface;
using Level.Data;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions
{
    public class InitMeshMaterialUVSet : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<MeshMaterialUVSet> m_meshMaterialUVSet;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(InitMeshMaterialUVSet);
        public static Type StaticFactoryType => typeof(InitMeshMaterialUVSetAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            return new InitMeshMaterialUVSetAction(m_meshMaterialUVSet.Init(nameof(m_meshMaterialUVSet), Node));
        }
    }

    public class InitMeshMaterialUVSetAction : IDefaultAction
    {
        readonly IVar<MeshMaterialUVSet> m_meshMaterialUVSet;

        public InitMeshMaterialUVSetAction(IVar<MeshMaterialUVSet> mts) => m_meshMaterialUVSet = mts;
        public void Invoke() => m_meshMaterialUVSet.Value = MeshMaterialUVSet.Default;
    }
}
