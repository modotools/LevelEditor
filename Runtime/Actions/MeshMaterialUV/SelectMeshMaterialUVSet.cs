﻿using System;
using Core.Interface;
using Core.Unity.Extensions;
using Level.Data;
using Level.Tiles;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;
using UnityEngine.Serialization;

namespace Level.Actions
{
    public class SelectMeshMaterialUVSet : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<MeshMaterialUVSet> m_meshMaterialUVSet;
        [SerializeField, Input] internal VarReference<ITileMeshMaterialUVProvider> m_provider;
        [FormerlySerializedAs("m_borderTypeID")] 
        [SerializeField, Input] internal VarReference<TileBuilderTypeID> m_builderTypeID;

        [SerializeField, Input] internal VarReference<Vector3> m_currentPos;

        [SerializeField, Input] internal VarReference<int> m_caseConfigMesh;
        [SerializeField, Input] internal VarReference<int> m_uvConfig;
        [SerializeField, Input] internal VarReference<bool> m_hard;

        [SerializeField, Input] internal VarReference<ushort> m_variation;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(SelectMeshMaterialUVSet);
        public static Type StaticFactoryType => typeof(SelectMeshMaterialUVSetAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            return new SelectMeshMaterialUVSetAction(m_meshMaterialUVSet.Init(nameof(m_meshMaterialUVSet), Node),
                m_provider.Init(nameof(m_provider), Node),
                m_builderTypeID.Init(nameof(m_builderTypeID), Node),
                m_currentPos.Init(nameof(m_currentPos), Node),
                m_caseConfigMesh.Init(nameof(m_caseConfigMesh), Node),
                m_uvConfig.Init(nameof(m_uvConfig), Node),
                m_hard.Init(nameof(m_hard), Node),
                m_variation.Init(nameof(m_variation), Node));
        }
    }

    public class SelectMeshMaterialUVSetAction : IDefaultAction
    {
        readonly IVar<MeshMaterialUVSet> m_meshMaterialUVSet;
        readonly IVar<ITileMeshMaterialUVProvider> m_provider;
        readonly IVar<TileBuilderTypeID> m_builderTypeID;

        readonly IVar<Vector3> m_pos;

        readonly IVar<int> m_caseConfigMesh;
        readonly IVar<int> m_caseConfigUV;
        readonly IVar<bool> m_hardEdge;
        readonly IVar<ushort> m_variation;

        public SelectMeshMaterialUVSetAction(IVar<MeshMaterialUVSet> mts,
            IVar<ITileMeshMaterialUVProvider> provider,
            IVar<TileBuilderTypeID> builderTypeID,
            IVar<Vector3> pos,
            IVar<int> caseConfigMesh,
            IVar<int> caseConfigUV,
            IVar<bool> hardEdge,
            IVar<ushort> variation)
        {
            m_meshMaterialUVSet = mts;
            m_provider = provider;
            m_builderTypeID = builderTypeID;

            m_pos = pos;

            m_caseConfigMesh = caseConfigMesh;
            m_caseConfigUV = caseConfigUV;

            m_hardEdge = hardEdge;
            m_variation = variation;
        }

        public void Invoke()
        {
            // todo: Init Set, release
            var set = m_meshMaterialUVSet.Value;
            var variation = m_variation.Value;
            if (variation == 0)
                variation = (ushort) UnityEngine.Random.Range(1, ushort.MaxValue);
            m_variation.Value = variation;

            // because we use 0 for uninitialized and getting random value from 1 instead of 0,
            // so we subtract 1 again here
            m_provider.Value.Get(ref set, m_builderTypeID.Value, 
                m_pos.Value.Vector2Int(), 
                m_caseConfigMesh.Value,
                m_caseConfigUV.Value,
                m_hardEdge.Value, variation - 1);
        }
    }
}
