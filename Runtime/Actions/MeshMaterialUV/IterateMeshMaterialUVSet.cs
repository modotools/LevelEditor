﻿using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.Interface;
using Level.Data;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions
{
    public class IterateMeshMaterialUVSet : ScriptableBaseAction, ILinkActionConfigs
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] 
        internal VarReference<MeshMaterialUVSet> m_meshMaterialUVSet;
        [SerializeField, Input] 
        internal VarReference<MeshMaterialUVData> m_meshMaterialUVData;
        [SerializeField, Input]
        internal VarReference<Material> m_material;
        [SerializeField, Input]
        internal VarReference<UnityEngine.Mesh> m_meshVar;
        [SerializeField, Input]
        internal VarReference<UnityEngine.Mesh> m_colliderVar;
        [SerializeField, Input] 
        internal VarReference<Vector3> m_meshOffset;
        [SerializeField, Input] 
        internal VarReference<Vector3> m_orientation;

        [SerializeField, NodeConnector(IO.Output, typeof(ScriptableActionConnector))] 
        ScriptableBaseAction m_continue;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(IterateMeshMaterialUVSet);
        public static Type StaticFactoryType => typeof(IterateMeshMaterialUVSetAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            return new IterateMeshMaterialUVSetAction(
                m_meshMaterialUVSet.Init(nameof(m_meshMaterialUVSet), Node),
                m_meshMaterialUVData.Init(nameof(m_meshMaterialUVData), Node),
                m_material.Init(nameof(m_material), Node), 
                m_meshVar.Init(nameof(m_meshVar), Node),
                m_colliderVar.Init(nameof(m_colliderVar), Node),
                m_meshOffset.Init(nameof(m_meshOffset), Node),
                m_orientation.Init(nameof(m_orientation), Node));
        }

        public IEnumerable<ScriptableObject> Dependencies => new[] { m_continue };
        public IEnumerable<IActionConfig> GetActionConfigsLinked() => new[] { m_continue };

        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (!(graphActions[this] is IterateMeshMaterialUVSetAction thisAction)) 
                return;
            graphActions.TryGetValue(m_continue, out var continueAction);
            thisAction.LinkActions(continueAction as IDefaultAction);
        }
    }

    public class IterateMeshMaterialUVSetAction : IDefaultAction
    {
        readonly IVar<MeshMaterialUVSet> m_meshMaterialUVSetData;
        readonly IVar<MeshMaterialUVData> m_meshMaterialUVData;

        readonly IVar<Material> m_material;
        readonly IVar<UnityEngine.Mesh> m_mesh;
        readonly IVar<UnityEngine.Mesh> m_collider;
        readonly IVar<Vector3> m_offset;
        readonly IVar<Vector3> m_rotation;

        IDefaultAction m_action;

        public IterateMeshMaterialUVSetAction(IVar<MeshMaterialUVSet> mts, 
            IVar<MeshMaterialUVData> uvDataVar,
            IVar<Material> material,
            IVar<UnityEngine.Mesh> mesh,
            IVar<UnityEngine.Mesh> collider,
            IVar<Vector3> offset,
            IVar<Vector3> rotation)
        {
            m_meshMaterialUVSetData = mts;
            m_meshMaterialUVData = uvDataVar;

            m_material = material;
            m_mesh = mesh;
            m_collider = collider;

            m_offset = offset;
            m_rotation = rotation;
        }

        public void Invoke()
        {
            var meshMatUVData = m_meshMaterialUVSetData.Value;
            var data = meshMatUVData.MeshMaterialUVData;

            if (data.IsNullOrEmpty())
                return;

            var l = data.Count;
            for (var i = 0; i < l; i++)
            {
                var meshMaterialUV = data[i];
                m_meshMaterialUVData.Value = meshMaterialUV;
                m_material.Value = meshMaterialUV.Material;

                Debug.Assert(meshMaterialUV.Mesh);
                m_mesh.Value = meshMaterialUV.Mesh;
                m_offset.Value = meshMaterialUV.Offset;
                m_rotation.Value = meshMaterialUV.Rotation;

                // todo:
                m_collider.Value = i.IsInRange(meshMatUVData.ColliderMeshes)
                    ? meshMatUVData.ColliderMeshes[i] : null;

                m_action.Invoke();
            }
        }

        public void LinkActions(IDefaultAction continueAction) => m_action = continueAction;
    }
}
