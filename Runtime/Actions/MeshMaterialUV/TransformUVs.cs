﻿using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.Interface;
using Level.Data;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions
{
    public class TransformUVs : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<MeshMaterialUVData> m_meshMaterialUVData;
        [SerializeField, Input] internal VarReference<MeshData> m_meshData;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(TransformUVs);
        public static Type StaticFactoryType => typeof(TransformUVsAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            return new TransformUVsAction(m_meshData.Init(nameof(m_meshData), Node),
                m_meshMaterialUVData.Init(nameof(m_meshMaterialUVData), Node));
        }
    }

    public class TransformUVsAction : IDefaultAction
    {
        readonly IVar<MeshData> m_meshData;
        readonly IVar<MeshMaterialUVData> m_meshMaterialUVData;

        public TransformUVsAction(IVar<MeshData> mdr,
            IVar<MeshMaterialUVData> meshMatUVData)
        {
            m_meshData = mdr;
            m_meshMaterialUVData = meshMatUVData;
        }

        public void Invoke()
        {
            var data = m_meshMaterialUVData.Value;
            var md = m_meshData.Value;
            
            var uvs = md.UVs;
            // todo: rules to use uvs2, 3
            md.UVs2.AddRange(uvs);
            md.UVs3.AddRange(uvs);
            UpdateUVs(data.UVStart, data.UVEnd, uvs);
            
            var uvs2 = md.UVs2;
            if (!uvs2.IsNullOrEmpty()) 
                UpdateUVs(data.UV2Start, data.UV2End, uvs2);
            var uvs3 = md.UVs3;
            if (!uvs3.IsNullOrEmpty()) 
                UpdateUVs(data.UV3Start, data.UV3End, uvs3);
        }

        static void UpdateUVs(Vector2 start, Vector2 end, List<Vector2> uvs)
        {
            var range = end - start;
            for (var i = 0; i < uvs.Count; i++)
            {
                var origUV = uvs[i];
                uvs[i] = new Vector2(start.x + origUV.x * range.x, start.y + origUV.y * range.y);
            }
        }
    }
}
