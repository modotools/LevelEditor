﻿using System;
using Level.Data;
using Level.Texture;
using Level.Tiles;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions
{
    public class SelectCeilingMeshMaterialUV : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<MeshMaterialUVSet> m_meshMaterialUVSet;
        [SerializeField, Input] internal CeilingTileBuilderConfig m_ceilingTileBuilder;
        [SerializeField, Input] internal TileBuilderTypeID m_ceilingBuilderTypeID;

        [SerializeField, Input] internal VarReference<Vector3> m_pos;

        [SerializeField, Input] internal VarReference<int> m_caseConfigMesh;
        [SerializeField, Input] internal VarReference<int> m_uvConfig;
        [SerializeField, Input] internal VarReference<bool> m_hard;

        [SerializeField, Input] internal VarReference<ushort> m_variation;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(SelectCeilingMeshMaterialUV);
        public static Type StaticFactoryType => typeof(SelectMeshMaterialUVSetAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            return new SelectMeshMaterialUVSetAction(m_meshMaterialUVSet.Init(nameof(m_meshMaterialUVSet), Node),
                new VarReference<ITileMeshMaterialUVProvider>((ITileMeshMaterialUVProvider) m_ceilingTileBuilder ), 
                new VarReference<TileBuilderTypeID>(m_ceilingBuilderTypeID),
                m_pos.Init(nameof(m_pos), Node),
                m_caseConfigMesh.Init(nameof(m_caseConfigMesh), Node),
                m_uvConfig.Init(nameof(m_uvConfig), Node),
                m_hard.Init(nameof(m_hard), Node), 
                m_variation.Init(nameof(m_variation), Node));
        }
    }
}
