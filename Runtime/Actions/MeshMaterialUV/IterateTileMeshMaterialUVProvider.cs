﻿using System;
using System.Collections.Generic;
using Core.Interface;
using Level.Data;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions
{
    public class IterateTileMeshMaterialUVProvider : ScriptableBaseAction, ILinkActionConfigs
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<TileMeshMaterialUVProviderList> m_tileMeshMatUVProviderList;
        [SerializeField, Input] internal VarReference<ITileMeshMaterialUVProvider> m_tileMeshMatUVProvider;

        [SerializeField, NodeConnector(IO.Output, typeof(ScriptableActionConnector))] 
        ScriptableBaseAction m_continue;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(IterateTileMeshMaterialUVProvider);
        public static Type StaticFactoryType => typeof(IterateTileMeshMaterialProviderAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            return new IterateTileMeshMaterialProviderAction(
                m_tileMeshMatUVProviderList.Init(nameof(m_tileMeshMatUVProviderList), Node),
                m_tileMeshMatUVProvider.Init(nameof(m_tileMeshMatUVProvider), Node));
        }

        public IEnumerable<ScriptableObject> Dependencies => new[] { m_continue };
        public IEnumerable<IActionConfig> GetActionConfigsLinked() => new[] { m_continue};

        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (!(graphActions[this] is IterateTileMeshMaterialProviderAction iterateAction)) 
                return;
            graphActions.TryGetValue(m_continue, out var continueAction);
            iterateAction.LinkActions(continueAction as IDefaultAction);
        }
    }

    public class IterateTileMeshMaterialProviderAction : IDefaultAction
    {
        readonly IVar<TileMeshMaterialUVProviderList> m_providerList;
        readonly IVar<ITileMeshMaterialUVProvider> m_provider;

        IDefaultAction m_continueAction;

        public IterateTileMeshMaterialProviderAction(
            IVar<TileMeshMaterialUVProviderList> providerList,
            IVar<ITileMeshMaterialUVProvider> provider)
        {
            m_providerList = providerList;
            m_provider = provider;
        }

        public void Invoke()
        {
            foreach (var prov in m_providerList.Value.Providers)
            {
                m_provider.Value = prov;
                m_continueAction.Invoke();
            }
        }

        public void LinkActions(IDefaultAction continueAction) 
            => m_continueAction = continueAction;
    }
}
