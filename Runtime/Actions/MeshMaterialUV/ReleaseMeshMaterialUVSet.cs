﻿using System;
using Core.Interface;
using Level.Data;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions
{
    public class ReleaseMeshMaterialUVSet : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<MeshMaterialUVSet> m_meshMaterialUVSet;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(ReleaseMeshMaterialUVSet);
        public static Type StaticFactoryType => typeof(ReleaseMeshMaterialUVSetAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            return new ReleaseMeshMaterialUVSetAction(m_meshMaterialUVSet.Init(nameof(m_meshMaterialUVSet), Node));
        }
    }

    public class ReleaseMeshMaterialUVSetAction : IDefaultAction
    {
        readonly IVar<MeshMaterialUVSet> m_meshMaterialUVSet;

        public ReleaseMeshMaterialUVSetAction(IVar<MeshMaterialUVSet> mts) => m_meshMaterialUVSet = mts;
        public void Invoke()
        {
            var set = m_meshMaterialUVSet.Value;
            MeshMaterialUVSet.Release(ref set);
            m_meshMaterialUVSet.Value = set;
        }
    }
}
