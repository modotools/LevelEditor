﻿using System;
using System.Collections.Generic;
using Core.Interface;
using Level.Data;
using Level.Tiles;
using Level.Tiles.Interface;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions 
{
    public class GetTileMeshMaterialUVProvidersForTransition : ScriptableBaseAction, ILinkActionConfigs
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField] internal VarReference<ITileConfig> m_lvlMaterialConfig;
        [SerializeField] TileTypeIdentifier m_geometryIdentifier;
        [SerializeField, Input] internal VarReference<TileMeshMaterialUVProviderList> m_tileMeshMatUVProviders;
        [SerializeField, Input] internal VarReference<TilesSetFilter> m_tileSetFilterA;
        [SerializeField, Input] internal VarReference<TilesSetFilter> m_tileSetFilterB;
        [SerializeField, Input] internal VarReference<bool> m_currentIsHard;

        [SerializeField, NodeConnector(IO.Output, typeof(ScriptableActionConnector))] 
        ScriptableBaseAction m_continue;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(GetTileMeshMaterialUVProvidersForTransition);
        public static Type StaticFactoryType => typeof(GetTileMeshMaterialUVProvidersForTransitionAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            return new GetTileMeshMaterialUVProvidersForTransitionAction(m_geometryIdentifier.TilesData(), 
                m_tileMeshMatUVProviders.Init(nameof(m_tileMeshMatUVProviders), Node),
                m_tileSetFilterA.Init(nameof(m_tileSetFilterA), Node),
                m_tileSetFilterB.Init(nameof(m_tileSetFilterB), Node),
                m_currentIsHard.Init(nameof(m_currentIsHard), Node),
                m_lvlMaterialConfig.Init(nameof(m_lvlMaterialConfig), Node).Value as LevelMaterialTypeConfig);
        }

        public IEnumerable<ScriptableObject> Dependencies => new[] { m_continue };
        public IEnumerable<IActionConfig> GetActionConfigsLinked() => new[] { m_continue};

        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (!(graphActions[this] is GetTileMeshMaterialUVProvidersForTransitionAction iterateAction)) 
                return;
            graphActions.TryGetValue(m_continue, out var continueAction);
            iterateAction.LinkActions(continueAction as IDefaultAction);
        }
    }

    public class GetTileMeshMaterialUVProvidersForTransitionAction : IDefaultAction
    {
        readonly ITileType m_geoTile;
        readonly IVar<TilesSetFilter> m_tilesSetFilterA;
        readonly IVar<TilesSetFilter> m_tilesSetFilterB;

        readonly IVar<TileMeshMaterialUVProviderList> m_provider;

        readonly IVar<bool> m_hard;

        readonly LevelMaterialTypeConfig m_lvlMaterialConfig;
        IDefaultAction m_continueAction;

        public GetTileMeshMaterialUVProvidersForTransitionAction(ITileType geoTile, 
            IVar<TileMeshMaterialUVProviderList> providers,
            IVar<TilesSetFilter> tsfA, IVar<TilesSetFilter> tsfB,
            IVar<bool> isHard,
            LevelMaterialTypeConfig lvlMaterialTypeConfig)
        {
            m_geoTile = geoTile;
            m_provider = providers;
            m_tilesSetFilterA = tsfA;
            m_tilesSetFilterB = tsfB;
            m_hard = isHard;

            m_lvlMaterialConfig = lvlMaterialTypeConfig;
        }

        public void Invoke()
        {
            var material = m_lvlMaterialConfig.GetTransition(m_tilesSetFilterA.Value.FilterIdx, 
                m_tilesSetFilterB.Value.FilterIdx);

            var provider = TileMeshMaterialUVProviderList.Default;
            material.GetProvider(m_geoTile, provider.Providers);
            m_provider.Value = provider;

            m_hard.Value = true;
            m_continueAction.Invoke();
        
            TileMeshMaterialUVProviderList.Release(ref provider);
            m_provider.Value = provider;
        }

        public void LinkActions(IDefaultAction continueAction) 
            => m_continueAction = continueAction;
    }
}
