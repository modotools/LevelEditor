﻿using System;
using System.Collections.Generic;
using Core.Interface;
using Level.Data;
using Level.Tiles;
using Level.Tiles.Interface;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions
{
    public class GetTileMeshMaterialUVProviders : ScriptableBaseAction, ILinkActionConfigs
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField] internal VarReference<ITileConfig> m_lvlMaterialConfig;
        [SerializeField] TileTypeIdentifier m_geometryIdentifier;
        [SerializeField, Input] internal VarReference<TileMeshMaterialUVProviderList> m_tileMeshMatUVProviders;
        [SerializeField, Input] internal VarReference<TilesSetFilter> m_tileSetFilter;

        [SerializeField, NodeConnector(IO.Output, typeof(ScriptableActionConnector))] 
        ScriptableBaseAction m_continue;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(GetTileMeshMaterialUVProviders);
        public static Type StaticFactoryType => typeof(GetTileMeshMaterialUVProvidersAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            return new GetTileMeshMaterialUVProvidersAction(m_geometryIdentifier.TilesData(), 
                m_tileMeshMatUVProviders.Init(nameof(m_tileMeshMatUVProviders), Node),
                m_tileSetFilter.Init(nameof(m_tileSetFilter), Node),
                m_lvlMaterialConfig.Init(nameof(m_lvlMaterialConfig), Node).Value as LevelMaterialTypeConfig);
        }

        public IEnumerable<ScriptableObject> Dependencies => new[] { m_continue };
        public IEnumerable<IActionConfig> GetActionConfigsLinked() => new[] { m_continue};

        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (!(graphActions[this] is GetTileMeshMaterialUVProvidersAction iterateAction)) 
                return;
            graphActions.TryGetValue(m_continue, out var continueAction);
            iterateAction.LinkActions(continueAction as IDefaultAction);
        }
    }

    public class GetTileMeshMaterialUVProvidersAction : IDefaultAction
    {
        readonly ITileType m_geoTile;
        readonly IVar<TilesSetFilter> m_tilesSetFilter;

        readonly IVar<TileMeshMaterialUVProviderList> m_provider;
        readonly LevelMaterialTypeConfig m_lvlMaterialConfig;
        IDefaultAction m_continueAction;

        public GetTileMeshMaterialUVProvidersAction(ITileType geoTile, 
            IVar<TileMeshMaterialUVProviderList> providers,
            IVar<TilesSetFilter> tsf,
            LevelMaterialTypeConfig lvlMaterialTypeConfig)
        {
            m_geoTile = geoTile;
            m_provider = providers;
            m_tilesSetFilter = tsf;

            m_lvlMaterialConfig = lvlMaterialTypeConfig;
        }

        public void Invoke()
        {
            var material = m_lvlMaterialConfig.MaterialTypes[m_tilesSetFilter.Value.FilterIdx];
            var provider = TileMeshMaterialUVProviderList.Default;
            material.GetProvider(m_geoTile, provider.Providers);
            m_provider.Value = provider;

            m_continueAction.Invoke();
        
            TileMeshMaterialUVProviderList.Release(ref provider);
            m_provider.Value = provider;
        }

        public void LinkActions(IDefaultAction continueAction) 
            => m_continueAction = continueAction;
    }
}
