﻿using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.Interface;
using Level.Data;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions.MarchingSquares
{
    /// <summary>
    /// Deals with special cases for MarchingSquares <see cref="MSSpecialCasesConfig"/>
    /// </summary>
    public class MarchingSquaresSpecialCases : ScriptableBaseAction, ILinkActionConfigs
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<int> m_configuration;
        [SerializeField, Input] internal VarReference<int> m_uvConfiguration;

        [SerializeField] internal MSSpecialCasesConfig m_specialCasesConfig;

        [SerializeField, Input]
        internal VarReference<bool> m_isHard;

        [SerializeField, NodeConnector(IO.Output, typeof(ScriptableActionConnector))] 
        internal  ScriptableBaseAction m_continueWith;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(MarchingSquaresSpecialCases);
        public static Type StaticFactoryType => typeof(MarchingSquaresSpecialCasesAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction()
        {
            return new MarchingSquaresSpecialCasesAction(
                m_configuration.Init(nameof(m_configuration), Node),
                m_uvConfiguration.Init(nameof(m_uvConfiguration), Node),
                m_isHard.Init(nameof(m_isHard), Node),
                m_specialCasesConfig);
        }
        
        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (graphActions[this] is MarchingSquaresSpecialCasesAction action)
                action.LinkActions(graphActions[m_continueWith]);

        }

        public IEnumerable<ScriptableObject> Dependencies => new[] {m_continueWith};
        public IEnumerable<IActionConfig> GetActionConfigsLinked() => new[] {m_continueWith};
    }

    public class MarchingSquaresSpecialCasesAction : IDefaultAction
    {
        readonly IVar<int> m_configuration;
        readonly IVar<int> m_uvConfiguration;

        readonly IVar<bool> m_isHard;

        readonly MSSpecialCasesConfig m_specialCasesConfig;
        IDefaultAction m_continue;

        public MarchingSquaresSpecialCasesAction(
            IVar<int> config, IVar<int> uvConfig,
            IVar<bool> hard, MSSpecialCasesConfig specialCases)
        {
            m_configuration = config;
            m_uvConfiguration = uvConfig;
            m_isHard = hard;
            m_specialCasesConfig = specialCases;
        }

        public void Invoke()
        {
            var conf = m_configuration.Value;
            var specialCase = m_specialCasesConfig.Get(conf);

            if (specialCase.IsHard)
                m_isHard.Value = true;
            if (specialCase.ConfigurationSplit.IsNullOrEmpty())
            {
                m_uvConfiguration.Value = conf;
                m_continue.Invoke();
            }
            else foreach (var c in specialCase.ConfigurationSplit)
            {
                m_uvConfiguration.Value = c;
                m_continue.Invoke();
            }
        }

        public void LinkActions(IBaseAction graphAction) => m_continue = graphAction as IDefaultAction;
    }
}
