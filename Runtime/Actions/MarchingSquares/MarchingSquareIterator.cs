﻿using System;
using System.Collections.Generic;
using Core.Interface;
using Core.Unity.Extensions;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;
using static Level.Tiles.TileProcessing;

namespace Level.Actions.MarchingSquares
{
    /// <summary>
    /// Iterates over all corners of a tile
    /// </summary>
    public class MarchingSquareIterator : ScriptableBaseAction, ILinkActionConfigs
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input]
        internal VarReference<Vector3> m_pos;
        [SerializeField, Input]
        internal VarReference<Vector3> m_cornerPos;
        [SerializeField, Input]
        internal VarReference<int> m_idxOffset;
        [SerializeField, Input]
        internal VarReference<int> m_iteratorIdx;
        [SerializeField, NodeConnector(IO.Output, typeof(ScriptableActionConnector))]
        internal ScriptableBaseAction m_continueWith;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(MarchingSquareIterator);
        public static Type StaticFactoryType => typeof(MarchingSquareIteratorAction);
        public override Type FactoryType => StaticFactoryType;

        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (graphActions[this] is MarchingSquareIteratorAction action)
                action.LinkActions(graphActions[m_continueWith]);
        }
        public override IBaseAction CreateAction() => 
            new MarchingSquareIteratorAction(m_pos, m_idxOffset, m_iteratorIdx, m_cornerPos);

        public IEnumerable<ScriptableObject> Dependencies 
            => new[] { m_continueWith };
        public IEnumerable<IActionConfig> GetActionConfigsLinked()
            => new[] { m_continueWith };
    }

    public class MarchingSquareIteratorAction : IDefaultAction
    {
        readonly IVar<Vector3> m_pos;
        readonly IVar<int> m_idxOffset;

        readonly IVar<int> m_iteratorIdx;
        readonly IVar<Vector3> m_cornerPos;

        IDefaultAction m_continue;

        public MarchingSquareIteratorAction(IVar<Vector3> pos, 
            IVar<int> idxOffset, 
            IVar<int> iteratorIdx, 
            IVar<Vector3> cornerPos)
        {
            m_pos = pos;
            m_idxOffset = idxOffset;
            m_iteratorIdx = iteratorIdx;
            m_cornerPos = cornerPos;
        }
        public void LinkActions(IBaseAction graphAction) => m_continue = (IDefaultAction) graphAction;

        public void Invoke()
        {
            var pos = m_pos.Value.Vector3Int();
            for (var idx = m_idxOffset.Value; idx < IterationOffset.Length; idx++)
            {
                var offset = IterationOffset[idx];
                m_iteratorIdx.Value = idx;
                m_cornerPos.Value = new Vector3(pos.x + offset.x, pos.y + offset.y, pos.z + offset.z);

                m_continue.Invoke();
            }
        }
    }
}
