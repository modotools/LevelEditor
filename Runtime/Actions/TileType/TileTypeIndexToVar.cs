﻿using System;
using Core.Interface;
using Core.Unity.Extensions;
using Level.Data;
using Level.Tiles;
using Level.Tiles.Interface;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions.TileType
{
    public class TileTypeIndexToVar : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input]
        internal VarReference<TilesSetListConfig> m_sourceConfig;
        [SerializeField, Input]
        internal RefITileConfig m_tileConfig;

        [SerializeField, Input]
        internal VarReference<GridData> m_grid;
        [SerializeField, Input]
        internal VarReference<Vector3> m_currentPosition;
        [SerializeField, Input]
        internal VarReference<ushort> m_var;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(TileTypeIndexToVar);
        public static Type StaticFactoryType => typeof(TileTypeIndexToVarAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            var sourceConfig = m_sourceConfig.Init(nameof(m_sourceConfig), Node).Value;
            return new TileTypeIndexToVarAction(sourceConfig.GetSet(m_tileConfig.Result),
                m_grid.Init(nameof(m_grid), Node),
                m_currentPosition.Init(nameof(m_currentPosition), Node),
                m_var.Init(nameof(m_var), Node));
        }
    }

    public class TileTypeIndexToVarAction : IDefaultAction
    {
        readonly TilesSetData m_setData;

        readonly IVar<GridData> m_grid;
        readonly IVar<Vector3> m_currentPos;

        readonly IVar<ushort> m_var;

        public TileTypeIndexToVarAction(TilesSetData setData, 
            IVar<GridData> grid, 
            IVar<Vector3> currentPos,
            IVar<ushort> var)
        {
            m_setData = setData;
            
            m_grid = grid;
            m_currentPos = currentPos;
            m_var = var;
        }
        
        public void Invoke()
        {
            var grid = m_grid.Value;
            var pos = m_currentPos.Value.Vector3Int();
            var tile = grid[pos];
            var index= m_setData.GetTileIdx(tile);
            m_var.Value = index;
        }
    }
}
