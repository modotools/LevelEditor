﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Interface;
using Core.Unity.Extensions;
using Level.Data;
using Level.Tiles;
using Level.Tiles.Interface;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;
using static Level.Tiles.TileProcessing;
using static Level.Tiles.TileTypeOperations;

namespace Level.Actions.TileType
{
    public class TileTypeIterator : ScriptableBaseAction, ILinkActionConfigs
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<TilesSetListConfig> m_sourceConfig;
        [SerializeField, Input] internal VarReference<ITileConfig> m_iterateOnTileConfig;
        [SerializeField] internal TileTypeIdentifier[] m_filter;

        [SerializeField, Input]
        internal VarReference<GridData> m_grid;
        [SerializeField, Input]
        internal VarReference<Vector3> m_currentPosition;
        [SerializeField, Input]
        internal VarReference<TilesSetFilter> m_currentTilesSetFilter;

        [SerializeField, HideInInspector, NodeConnector(IO.Output, typeof(ScriptableActionConnector))] 
        internal ScriptableBaseAction m_continueWith;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(TileTypeIterator);
        public static Type StaticFactoryType => typeof(TileTypeIteratorAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() =>
            new TileTypeIteratorAction(m_sourceConfig.Init(nameof(m_sourceConfig), Node).Value, 
                m_iterateOnTileConfig.Init(nameof(m_iterateOnTileConfig), Node).Value, 
                m_filter, m_grid, m_currentPosition, m_currentTilesSetFilter);

        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (graphActions[this] is TileTypeIteratorAction action)
                action.LinkActions(graphActions[m_continueWith]);
        }

#if UNITY_EDITOR
        public const string Editor_ContinueWithPropName = nameof(m_continueWith);
#endif
        public IEnumerable<ScriptableObject> Dependencies 
            => new[] { m_continueWith };
        public IEnumerable<IActionConfig> GetActionConfigsLinked()
            => new[] { m_continueWith };
    }

    public class TileTypeIteratorAction : IDefaultAction
    {
        readonly TilesSetListConfig m_sourceConfig;
        readonly ITileConfig m_iterateOnTileConfig;
        readonly TileTypeIdentifier[] m_filterIdentifier;

        readonly IVar<GridData> m_grid;
        readonly IVar<Vector3> m_currentPos;
        readonly IVar<TilesSetFilter> m_tilesSetFilter;
        readonly TilesSetFilter[] m_filter;
        IDefaultAction m_continueAction;

        public TileTypeIteratorAction(TilesSetListConfig sourceConfig, 
            ITileConfig iterateOnTileConfig, 
            TileTypeIdentifier[] filter, 
            IVar<GridData> grid, 
            IVar<Vector3> currentPos,
            IVar<TilesSetFilter> tilesSetFilter)
        {
            m_sourceConfig = sourceConfig;
            m_iterateOnTileConfig = iterateOnTileConfig; 
            m_filterIdentifier = filter;
            m_filter = new TilesSetFilter[filter.Length];

            m_grid = grid;
            m_currentPos = currentPos;
            m_tilesSetFilter = tilesSetFilter;
        }
        public void LinkActions(IBaseAction graphAction) 
            => m_continueAction = (IDefaultAction) graphAction;

        public void Invoke()
        {
            var iterationSet = m_sourceConfig.GetSet(m_iterateOnTileConfig);

            for (var i = 0; i < m_filterIdentifier.Length; i++)
                m_filter[i] = GetFilter(m_sourceConfig, m_filterIdentifier[i]);

            var startNode = 0;
            ushort[] texTypesDone = { ushort.MaxValue, ushort.MaxValue, ushort.MaxValue, ushort.MaxValue };
            var texTypesDoneIdx = 0;

            ushort next;
            while ((next = NextType(iterationSet, m_filter, ref startNode,
                       ref texTypesDone, ref texTypesDoneIdx)) != ushort.MaxValue)
            {
                //Debug.Log($"m_currentPos {m_currentPos.Value} next {next}");
                m_tilesSetFilter.Value = new TilesSetFilter(iterationSet, next);
                m_continueAction.Invoke();
            }
        }

        public ushort NextType(TilesSetData iterationSet, TilesSetFilter[] filters, 
            ref int startNode, ref ushort[] texTypeDone, ref int texTypesDoneIdx)
        {
            var grid = m_grid.Value;
            var pos = m_currentPos.Value.Vector3Int();
            for (; startNode < IterationOffset.Length; ++startNode)
            {
                var off = IterationOffset[startNode];
                var offsetPos = pos + off;
                var tile = grid[offsetPos];

                var ignoreTile = filters.Any(filter => !filter.IsTileActive(tile));
                if (ignoreTile)
                    continue;
                //Debug.Log($"{iterationSet.Mask} {iterationSet.Shift} {tile}");
                var type = iterationSet.GetTileIdx(tile);

                if (texTypeDone.Any(t => t == type))
                    continue;

                if (!texTypesDoneIdx.IsInRange(texTypeDone))
                {
                    Debug.LogError($"Not in Range texTypeDone.Length {texTypeDone.Length} texTypesDoneIdx {texTypesDoneIdx} for tile {type}\n" +
                                   $"DONE: {texTypeDone[0]} DONE: {texTypeDone[1]} DONE: {texTypeDone[2]}");
                    continue;
                }
                texTypeDone[texTypesDoneIdx] = type;
                texTypesDoneIdx++;

                return type;
            }
            return ushort.MaxValue;
        }
    }
}
