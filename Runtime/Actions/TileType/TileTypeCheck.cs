﻿using System;
using System.Collections.Generic;
using Core.Interface;
using Core.Unity.Extensions;
using Level.Data;
using Level.Tiles;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions.TileType
{
    public class TileTypeCheck : ScriptableBaseAction, ILinkActionConfigs
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input]
        internal TilesSetListConfig m_sourceConfig;
        [SerializeField, Input]
        internal TileTypeIdentifier m_tileIdentifier;

        [SerializeField, Input]
        internal VarReference<GridData> m_grid;
        [SerializeField, Input]
        internal VarReference<Vector3> m_currentPosition;

        [SerializeField, NodeConnector(IO.Output, typeof(ScriptableActionConnector))] 
        internal ScriptableBaseAction m_trueAction;
        [SerializeField, NodeConnector(IO.Output, typeof(ScriptableActionConnector))]
        internal ScriptableBaseAction m_falseAction;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(TileTypeCheck);
        public static Type StaticFactoryType => typeof(TileTypeCheckAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() => 
            new TileTypeCheckAction(m_sourceConfig, m_tileIdentifier, m_grid, m_currentPosition);

        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (graphActions[this] is TileTypeCheckAction action)
                action.LinkActions(graphActions[m_trueAction], graphActions[m_falseAction]);
        }

        public IEnumerable<ScriptableObject> Dependencies 
            => new[] {m_trueAction, m_falseAction};
        public IEnumerable<IActionConfig> GetActionConfigsLinked()
            => new[] {m_trueAction, m_falseAction};
    }

    public class TileTypeCheckAction : IDefaultAction
    {
        readonly TilesSetFilter m_filter;

        readonly IVar<GridData> m_grid;
        readonly IVar<Vector3> m_currentPos;

        IDefaultAction m_trueAction;
        IDefaultAction m_falseAction;
        
        public TileTypeCheckAction(TilesSetListConfig sourceConfig, 
            TileTypeIdentifier identifier, 
            IVar<GridData> grid, 
            IVar<Vector3> currentPos)
        {
            var iterationSet = sourceConfig.GetSet(identifier.TileConfig.Result);

            m_filter = new TilesSetFilter(iterationSet, identifier.TileIdx);

            m_grid = grid;
            m_currentPos = currentPos;
        }

        public void LinkActions(IBaseAction trueAction, IBaseAction falseAction)
        {
            m_trueAction = (IDefaultAction) trueAction;
            m_falseAction = (IDefaultAction) falseAction;
        }

        public void Invoke()
        {
            var grid = m_grid.Value;
            var pos = m_currentPos.Value.Vector3Int();
            var tile = grid[pos];

            if (m_filter.IsTileActive(tile))
                m_trueAction?.Invoke();
            else 
                m_falseAction?.Invoke();
        }
    }
}
