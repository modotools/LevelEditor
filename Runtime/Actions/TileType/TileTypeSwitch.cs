﻿using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.Interface;
using Core.Unity.Extensions;
using Level.Data;
using Level.Tiles;
using Level.Tiles.Interface;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions.TileType
{
    public class TileTypeSwitch : ScriptableBaseAction, ILinkActionConfigs
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, HideInInspector] internal string m_name;
        [SerializeField, Input] internal VarReference<TilesSetListConfig> m_sourceConfig;
        [SerializeField, Input] internal RefITileConfig m_tileConfig;

        [SerializeField, Input]
        internal VarReference<GridData> m_grid;
        [SerializeField, Input]
        internal VarReference<Vector3> m_currentPosition;

        [SerializeField, HideInInspector, NodeConnectorList(IO.Output, typeof(ScriptableActionConnector))] 
        internal ScriptableBaseAction[] m_tileActions;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => m_name.IsNullOrEmpty()? nameof(TileTypeCheck) : m_name;
        public static Type StaticFactoryType => typeof(TileTypeSwitchAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() 
            => new TileTypeSwitchAction(m_sourceConfig.Init(nameof(m_sourceConfig), Node).Value,
                m_tileConfig.Result, m_grid, m_currentPosition);

#if UNITY_EDITOR
        public static string Editor_NameProperty => nameof(m_name);
        public static string Editor_ActionProperty => nameof(m_tileActions);

        public ITileConfig Editor_TileConfig => m_tileConfig?.Result;
#endif
        public IEnumerable<ScriptableObject> Dependencies => m_tileActions;
        public IEnumerable<IActionConfig> GetActionConfigsLinked() => m_tileActions;

        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (!(graphActions[this] is TileTypeSwitchAction action)) 
                return;

            var actions = new IDefaultAction[m_tileActions.Length];
            for (var i = 0; i < m_tileActions.Length; i++)
            {
                if (m_tileActions[i] != null && graphActions.TryGetValue(m_tileActions[i], out var ac))
                    actions[i] = ac as IDefaultAction;
            }   
            action.LinkActions(actions);
        }
    }

    public class TileTypeSwitchAction : IDefaultAction
    {
        readonly TilesSetData m_setData;
        readonly IVar<GridData> m_grid;
        readonly IVar<Vector3> m_currentPos;

        IDefaultAction[] m_actions;
        
        public TileTypeSwitchAction(TilesSetListConfig sourceConfig, 
            ITileConfig config, 
            IVar<GridData> grid, 
            IVar<Vector3> currentPos)
        {
            m_setData = sourceConfig.GetSet(config);

            m_grid = grid;
            m_currentPos = currentPos;
        }

        public void LinkActions(IDefaultAction[] actions) => m_actions = actions;

        public void Invoke()
        {
            var grid = m_grid.Value;
            var pos = m_currentPos.Value.Vector3Int();
            var tile = grid[pos];

            var idx = m_setData.GetTileIdx(tile);
            //if (pos.x == 0 && pos.z == 0)
            //    Debug.Log($"TileType Switch {pos} combined: {tile} idx: {idx}");
            if (m_actions.IsIndexInRange(idx))
                m_actions[idx]?.Invoke();
        }
    }
}
