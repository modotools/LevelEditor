﻿using System;
using System.Collections.Generic;
using Core.Interface;
using Core.Unity.Extensions;
using Level.Data;
using Level.Tiles;
using Level.Tiles.Interface;
using Level.Tiles.MarchingSquares;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

using static Level.Tiles.TileProcessing;
using static Level.Tiles.TileTypeOperations;

namespace Level.Actions.TileType
{
    public class BranchForTransition : ScriptableBaseAction, ILinkActionConfigs
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<TilesSetListConfig> m_sourceConfig;
        [SerializeField, Input] internal VarReference<ITileConfig> m_lvlMaterialConfig;
        [SerializeField] internal TileTypeIdentifier[] m_filter;

        [SerializeField, Input] internal VarReference<TilesSetListConfig> m_tilesEditConfig;
        [SerializeField, Input] internal VarReference<GridData> m_editGrid;
        [SerializeField] internal CountNodesConfig m_countFloorTexTypes;

        [SerializeField, Input]
        internal VarReference<GridData> m_grid;
        [SerializeField, Input]
        internal VarReference<Vector3> m_currentPosition;

        [SerializeField, Input] internal VarReference<LevelMaterialTransition> m_transition;
        [SerializeField, Input] internal VarReference<TilesSetFilter> m_tileSetFilterA;
        [SerializeField, Input] internal VarReference<TilesSetFilter> m_tileSetFilterB;

        [SerializeField, HideInInspector, NodeConnector(IO.Output, typeof(ScriptableActionConnector))] 
        internal ScriptableBaseAction m_transitionAction;
        [SerializeField, HideInInspector, NodeConnector(IO.Output, typeof(ScriptableActionConnector))] 
        internal ScriptableBaseAction m_noTransitionAction;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(BranchForTransition);
        public static Type StaticFactoryType => typeof(BranchForTransitionAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            var countTexNodesSet = m_tilesEditConfig.Init(nameof(m_tilesEditConfig), Node).Value.GetSet(m_countFloorTexTypes);
            return new BranchForTransitionAction(m_sourceConfig.Init(nameof(m_sourceConfig), Node).Value,
                m_lvlMaterialConfig.Init(nameof(m_lvlMaterialConfig), Node).Value as LevelMaterialTypeConfig,
                m_filter,
                m_grid.Init(nameof(m_grid), Node),
                m_currentPosition.Init(nameof(m_currentPosition), Node),
                m_transition.Init(nameof(m_transition), Node),
                m_tileSetFilterA.Init(nameof(m_tileSetFilterA), Node),
                m_tileSetFilterB.Init(nameof(m_tileSetFilterB), Node),
                countTexNodesSet,
                m_editGrid.Init(nameof(m_editGrid), Node));
        }

        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (graphActions[this] is BranchForTransitionAction action)
                action.LinkActions(graphActions[m_transitionAction] as IDefaultAction, 
                    graphActions[m_noTransitionAction] as IDefaultAction);
        }

        public IEnumerable<ScriptableObject> Dependencies 
            => new[] { m_transitionAction, m_noTransitionAction };
        public IEnumerable<IActionConfig> GetActionConfigsLinked()
            => new[] { m_transitionAction, m_noTransitionAction };
    }

    public class BranchForTransitionAction : IDefaultAction
    {
        readonly TilesSetListConfig m_sourceConfig;
        readonly LevelMaterialTypeConfig m_levelMaterialTypeConfig;
        readonly TileTypeIdentifier[] m_filterIdentifier;

        readonly IVar<GridData> m_grid;
        readonly IVar<Vector3> m_currentPos;
        readonly TilesSetFilter[] m_filter;

        readonly IVar<LevelMaterialTransition> m_transition;
        readonly IVar<TilesSetFilter> m_filterA;
        readonly IVar<TilesSetFilter> m_filterB;

        IDefaultAction m_transitionAction;
        IDefaultAction m_noTransitionAction;

        TilesSetData m_countTexFloorNodes;
        readonly IVar<GridData> m_editGrid;

        public BranchForTransitionAction(TilesSetListConfig sourceConfig, 
            LevelMaterialTypeConfig lvlMaterialTypeConfig, 
            TileTypeIdentifier[] filter, 
            IVar<GridData> grid, 
            IVar<Vector3> currentPos,
            IVar<LevelMaterialTransition> transition,
            IVar<TilesSetFilter> tilesSetFilterA,
            IVar<TilesSetFilter> tilesSetFilterB,
            TilesSetData countFloorTexNodes,
            IVar<GridData> editGrid)
        {
            m_sourceConfig = sourceConfig;
            m_levelMaterialTypeConfig = lvlMaterialTypeConfig; 
            m_filterIdentifier = filter;
            m_filter = new TilesSetFilter[filter.Length];

            m_grid = grid;
            m_currentPos = currentPos;

            m_transition = transition;
            m_filterA = tilesSetFilterA;
            m_filterB = tilesSetFilterB;

            m_countTexFloorNodes = countFloorTexNodes;
            m_editGrid = editGrid;
        }

        public void LinkActions(IDefaultAction transitionAction, IDefaultAction noTransitionAction)
        {
            m_transitionAction = transitionAction;
            m_noTransitionAction = noTransitionAction;
        }

        public void Invoke()
        {
            var grid = m_grid.Value;

            var iterationSet = m_sourceConfig.GetSet(m_levelMaterialTypeConfig);
            for (var i = 0; i < m_filterIdentifier.Length; i++)
                m_filter[i] = GetFilter(m_sourceConfig, m_filterIdentifier[i]);

            var pos = m_currentPos.Value.Vector3Int();

            var count = m_countTexFloorNodes.GetTileIdx(m_editGrid.Value[pos]);
            //Debug.Log($"count {count} TransitionsPossible {m_levelMaterialTypeConfig.TransitionsPossible}");

            if (m_levelMaterialTypeConfig.TransitionsPossible 
                && count == 2 
                && TryTransition(iterationSet, grid, pos))
                return;

            m_noTransitionAction.Invoke();
        }

        bool TryTransition(TilesSetData iterationSet, GridData grid, Vector3Int pos)
        {
            var startNode = 0;
            ushort[] texTypesDone = {ushort.MaxValue, ushort.MaxValue, ushort.MaxValue, ushort.MaxValue};
            var texTypesDoneIdx = 0;

            var lvlMatTypeA = NextType(iterationSet, m_filter, ref grid, pos,
                ref startNode, ref texTypesDone, ref texTypesDoneIdx);
            var lvlMatTypeB = NextType(iterationSet, m_filter, ref grid, pos,
                ref startNode, ref texTypesDone, ref texTypesDoneIdx);

            var tr = m_levelMaterialTypeConfig.GetTransition(lvlMatTypeA, lvlMatTypeB);
            //Debug.Log($"lvlMatTypeA {lvlMatTypeA} lvlMatTypeB {lvlMatTypeB} => {tr}");

            if (tr == null) 
                return false;

            //if (!m_transition.IsNone())
                m_transition.Value = tr;
            m_filterA.Value = new TilesSetFilter(iterationSet, tr.IndexA);
            m_filterB.Value = new TilesSetFilter(iterationSet, tr.IndexB);

            m_transitionAction.Invoke();
            return true;
        }
    }
}
