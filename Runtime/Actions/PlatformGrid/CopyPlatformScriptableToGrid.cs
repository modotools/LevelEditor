﻿using System;
using Core.Interface;
using Level.Data;
using UnityEngine;

using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using Level.Room;
using nodegraph.Attributes;

namespace Level.Actions.PlatformGrid
{
    public class CopyPlatformScriptableToGrid : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        //[SerializeField, Input]
        //internal ScriptableUnityObject m_platform;
        [SerializeField, Input]
        internal VarReference<GridData> m_grid;

        [SerializeField] VarReference<RoomBuilder> m_builder;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(CopyPlatformScriptableToGrid);
        public static Type StaticFactoryType => typeof(CopyPlatformScriptableToGridAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() 
            => new CopyPlatformScriptableToGridAction(m_builder.Value, m_grid);
    }

    public class CopyPlatformScriptableToGridAction : IDefaultAction
    {
        readonly RoomBuilder m_builder;
        IVar<GridData> m_grid;

        public CopyPlatformScriptableToGridAction(RoomBuilder builder, IVar<GridData> grid)
        {
            m_builder = builder;
            m_grid = grid;
        }

        public void Invoke()
        {
            //var platforms = m_builder.Layer;
            //var platformA = platforms[0];

            //var defaultData = new ushort[platformA.TileDim.x, platforms.Length, platformA.TileDim.y];

            //for (var y = 0; y < platforms.Length; ++y)
            //for (var x = 0; x < platformA.TileDim.x; ++x)
            //for (var z = 0; z < platformA.TileDim.y; ++z)
            //    defaultData[x, y, z] = GetTile(platforms[y], platforms[y], x, z);

            //m_grid.SetValue(defaultData);
        }
    }
}
