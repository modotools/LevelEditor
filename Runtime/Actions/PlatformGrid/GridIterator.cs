﻿using System;
using System.Collections.Generic;
using Core.Interface;
using Level.Data;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;
using UnityEngine.Serialization;

#if UNITASK
using System.Threading;
using Cysharp.Threading.Tasks;
#endif

namespace Level.Actions.PlatformGrid
{
    public class GridIterator : ScriptableBaseAction, ILinkActionConfigs
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input]
        internal VarReference<GridData> m_grid;
        [SerializeField, Input]
        internal VarReference<Vector3> m_currentPosition;
        // f.e. if grid pos x is compared with x+1 we reduce iteration by 1
        [SerializeField, Input]
        internal Vector3Int m_reducedIteration;

        [SerializeField] bool m_iterateX = true;
        [SerializeField] bool m_iterateY;
        [SerializeField] bool m_iterateZ = true;

        [FormerlySerializedAs("m_continueWith"), NodeConnector(IO.Output, typeof(ScriptableActionConnector))]
        public ScriptableBaseAction ContinueWith;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(GridIterator);
        public static Type StaticFactoryType => typeof(GridIteratorAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction()
        {
            var reduce = new Vector3Int(Mathf.Abs(m_reducedIteration.x),
                Mathf.Abs(m_reducedIteration.y),
                Mathf.Abs(m_reducedIteration.z));

            return new GridIteratorAction(m_grid,
                m_currentPosition,
                reduce, 
                m_iterateX, m_iterateY, m_iterateZ);
        }

        public IEnumerable<ScriptableObject> Dependencies => new[] { ContinueWith };
        public IEnumerable<IActionConfig> GetActionConfigsLinked() => new[] { ContinueWith };
        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (graphActions[this] is GridIteratorAction gridAction)
                gridAction.LinkActions(graphActions[ContinueWith]);
        }
    }

    public class GridIteratorAction : IDefaultAction, IAsyncAction
    {
        readonly IVar<GridData> m_grid;
        readonly IVar<Vector3> m_currentPosition;
        readonly Vector3Int m_reducedIteration;
        IDefaultAction m_continue;

        readonly bool m_iterateX;
        readonly bool m_iterateY;
        readonly bool m_iterateZ;

        public GridIteratorAction(IVar<GridData> gridReference, IVar<Vector3> vector3Reference,
            Vector3Int reducedIteration, 
            bool iterateX, bool iterateY, bool iterateZ)
        {
            m_grid = gridReference;
            m_currentPosition = vector3Reference;
            m_reducedIteration = reducedIteration;

            m_iterateX = iterateX;
            m_iterateY = iterateY;
            m_iterateZ = iterateZ;
        }
        
        void GetIterationData(out float yStart, out float yEnd, out float xStart, out float xEnd, out float zStart,
            out float zEnd)
        {
            var data = m_grid.Value;

            yStart = m_iterateY ? 0 : m_currentPosition.Value.y;
            yEnd = m_iterateY ? data.Height - m_reducedIteration.y : yStart + 1;

            xStart = m_iterateX ? 0 : m_currentPosition.Value.x;
            xEnd = m_iterateX ? data.Width - m_reducedIteration.x : xStart + 1;

            zStart = m_iterateZ ? 0 : m_currentPosition.Value.z;
            zEnd = m_iterateZ ? data.Depth - m_reducedIteration.z : zStart + 1;
        }

        IEnumerable<Vector3> Iterator()
        {
            GetIterationData(out var yStart, out var yEnd, 
                out var xStart, out var xEnd, 
                out var zStart, out var zEnd);

            for (var y = yStart; y < yEnd; y++)
            {
                //Debug.Log($"y {y}");
                for (var x = xStart; x < xEnd; x++)
                {
                    //Debug.Log($"x {x}");
                    for (var z = zStart; z < zEnd; z++)
                    {
                        //Debug.Log($"z {z}");
                        yield return new Vector3(x, y, z);
                    }
                }
            }
        }

        public void Invoke()
        {
            foreach (var i in Iterator())
            {
                m_currentPosition.Value = new Vector3(i.x, i.y, i.z);
                m_continue.Invoke();
            }
        }

#if UNITASK
        public async UniTask InvokeAsync(CancellationToken cancellationToken)
        {
            if (!(m_continue is IAsyncAction asyncAction))
            {
                Invoke();
                return;
            }
            foreach (var i in Iterator())
            {
                if (cancellationToken.IsCancellationRequested)
                    throw new OperationCanceledException();

                m_currentPosition.Value = new Vector3(i.x, i.y, i.z);
                await asyncAction.InvokeAsync(cancellationToken);
            }
        }
#endif

        public void LinkActions(IBaseAction continueAction)
            => m_continue = continueAction as IDefaultAction;
    }
}
