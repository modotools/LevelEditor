﻿using System;
using System.Linq;
using Core.Interface;
using Level.Data;
using Level.PlatformLayer.Interface;
using Level.Room;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

using static Level.PlatformLayer.Operations;

namespace Level.Actions.PlatformGrid
{
    public class InitGrid : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input]
        internal VarReference<IRoom> m_room;

        [SerializeField, Input]
        internal VarReference<IPlatformLayer> m_platform;

        [SerializeField, Input] internal VarReference<GridData> m_grid;
        [SerializeField, Input] internal VarReference<GridData> m_intermediateGrid;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(InitGrid);
        public static Type StaticFactoryType => typeof(InitGridAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            var r = m_room.Init(nameof(m_room), Node);
            var pl = m_platform.Init(nameof(m_platform), Node);
            var grid = m_grid.Init(nameof(m_grid), Node);
            var intermediateGrid = m_intermediateGrid.Init(nameof(m_intermediateGrid), Node);
            return new InitGridAction(r, pl, grid, intermediateGrid);
        }
    }

    public class InitGridAction : IDefaultAction
    {
        readonly IVar<IRoom> m_room;
        readonly IVar<IPlatformLayer> m_platform;
        readonly IVar<GridData> m_gridData;
        readonly IVar<GridData> m_intermediateGrid;

        public InitGridAction(IVar<IRoom> room, IVar<IPlatformLayer> platform,
            IVar<GridData> gridData,
            IVar<GridData> intermediateGrid)
        {
            m_room = room;
            m_platform = platform;
            m_gridData = gridData;
            m_intermediateGrid = intermediateGrid;
        }

        public void Invoke()
        {
            var r = m_room.Value;
            var p = m_platform.Value;

            if (r != null)
                InitGridWithRoom(r);
            else if (p != null)
                InitGridWithPlatform(p);
        }

        void InitGridWithPlatform(IPlatformLayer platformLayer)
        {
            if (!m_intermediateGrid.IsNone())
                m_intermediateGrid.Value = new ushort[platformLayer.TileDim.x, 1, platformLayer.TileDim.y];

            var gridData = new ushort[platformLayer.TileDim.x, 1, platformLayer.TileDim.y];
            for (var x = 0; x < platformLayer.TileDim.x; ++x)
            for (var z = 0; z < platformLayer.TileDim.y; ++z)
                gridData[x, 0, z] = GetTile(platformLayer, platformLayer, new Vector2Int(x, z));

            m_gridData.Value = gridData;
        }

        void InitGridWithRoom(IRoom r)
        {
            var platforms = r.PlatformLayer;
            //var platformA = r.PlatformLayer.FirstOrDefault();
            //Debug.Assert(platformA!= null);

            if (!m_intermediateGrid.IsNone())
                m_intermediateGrid.Value = new ushort[r.TileDim.x, r.MaxPlatformCount, r.TileDim.y];
            var gridData = new ushort[r.TileDim.x, r.MaxPlatformCount, r.TileDim.y];

            var y = 0;
            foreach (var p in platforms)
            {
                for (var x = 0; x < r.TileDim.x; ++x)
                for (var z = 0; z < r.TileDim.y; ++z)
                {
                    var px = x - p.Position.x;
                    var pz = z - p.Position.y;

                    var tile = GetTile(p, p, new Vector2Int(px, pz));
                    gridData[x, y, z] = tile;
                    //Debug.Log($"{x} {z} {tile}");
                }
                ++y;
            }

            m_gridData.Value = gridData;
        }
    }
}