﻿using System;
using System.Collections.Generic;
using Core.Interface;
using Level.PlatformLayer.Interface;
using Level.Room;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions.PlatformGrid
{
    public class PlatformIterator : ScriptableBaseAction, ILinkActionConfigs
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        // actually must be RoomInstanceData
        [SerializeField, Input]
        internal VarReference<IRoom> m_room;
        [SerializeField, Input]
        internal VarReference<IPlatformLayer> m_currentPlatform;

        [SerializeField, Input]
        internal VarReference<Vector3> m_currentPosition;

        [SerializeField, Input] internal VarReference<PlatformInstanceData> m_platformInstance;
#pragma warning restore 0649 // wrong warning for SerializeField
         
        [NodeConnector(IO.Output, typeof(ScriptableActionConnector))]
        public ScriptableBaseAction ContinueWith;

        public override string Name => nameof(PlatformIterator);
        public static Type StaticFactoryType => typeof(PlatformIteratorAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction()
        {
            return new PlatformIteratorAction(m_room.Init(nameof(m_room), Node),
                m_currentPlatform.Init(nameof(m_currentPlatform), Node),
                m_platformInstance.Init(nameof(m_platformInstance), Node),
                m_currentPosition.Init(nameof(m_currentPosition), Node));
        }

        public IEnumerable<ScriptableObject> Dependencies => new[] { ContinueWith };
        public IEnumerable<IActionConfig> GetActionConfigsLinked() => new[] { ContinueWith };
        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (graphActions[this] is PlatformIteratorAction gridAction)
                gridAction.LinkActions(graphActions[ContinueWith]);
        }
    }

    public class PlatformIteratorAction : IDefaultAction
    {
        readonly IVar<IRoom> m_room;
        readonly IVar<IPlatformLayer> m_platform;

        readonly IVar<PlatformInstanceData> m_platformInstance;
        readonly IVar<Vector3> m_currentPos;

        IDefaultAction m_continue;

        public PlatformIteratorAction(IVar<IRoom> room, IVar<IPlatformLayer> platform, 
            IVar<PlatformInstanceData> platformInstance, IVar<Vector3> currentPos)
        {
            m_room = room;
            m_platform = platform;
            m_platformInstance = platformInstance;

            m_currentPos = currentPos;
        }

        public void Invoke()
        {
            var room = m_room.Value;

            if (!(room is RoomInstanceComponent ric))
                return;

            var lvl = 0;
            foreach (var pl in room.PlatformLayer)
            {
                m_platformInstance.Value = ric.Data.VisualData.GetPlatformInstance(lvl);
                //Debug.Log($"{floorVisuals.Go.name} {floorVisuals.Mesh}");
                m_platform.Value = pl;
                // todo: @see hotfix in GridIterator
                m_currentPos.Value = new Vector3Int(0, lvl, 0);

                m_continue.Invoke();
                ++lvl;
            }
        }

        public void LinkActions(IBaseAction continueAction)
            => m_continue = continueAction as IDefaultAction;
    }
}
