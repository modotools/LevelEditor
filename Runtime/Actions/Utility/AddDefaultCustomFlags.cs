﻿using System;
using System.Collections.Generic;
using Core;
using Core.Interface;
using Core.Unity.Extensions;
using Level.Data;
using Level.Tiles;
using Level.Tiles.Interface;
using Level.Tiles.MarchingSquares;
using Level.Enums;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;
using UnityEngine.Serialization;
using static Level.Tiles.TileProcessing;

namespace Level.Actions.Utility
{
    public class AddDefaultCustomFlags : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<TilesSetListConfig> m_tilesSetListConfig;
        [SerializeField] internal GeometryIdentifierConfig m_geoTiles;
        [FormerlySerializedAs("m_texTypeConfig")] 
        [SerializeField, Input] internal VarReference<ITileConfig> m_materialTypeConfig;
        [SerializeField, Input] internal VarReference<TilesSetListConfig> m_tilesEditConfig;

        [SerializeField] internal HardEdgeConfig m_hardEdgeConfig;
        [FormerlySerializedAs("m_floorTexTypesConfig")] [SerializeField] 
        internal CountNodesConfig m_floorMatTypesConfig;
        [SerializeField] internal CountNodesConfig m_activeFloorNodesConfig;

        [SerializeField, Input] internal VarReference<HardEdges> m_hardEdges;
        [SerializeField, Input] internal VarReference<Vector3> m_currentPosition;
        [SerializeField] internal Vector3 m_offsetPositionIndex;

        [SerializeField, Input] internal VarReference<GridData> m_sourceGrid;
        [SerializeField, Input] internal VarReference<GridData> m_targetGrid;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(AddDefaultCustomFlags);
        public static Type StaticFactoryType => typeof(AddDefaultCustomFlagsAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction()
        {
            var tilesConfigVar = m_tilesSetListConfig.Init(nameof(m_tilesSetListConfig), Node);
            var tilesEditConfigVar = m_tilesEditConfig.Init(nameof(m_tilesEditConfig), Node);

            var floorMaterialTypesSet = m_tilesEditConfig.Value.GetSet(m_floorMatTypesConfig);

            var sourceHardEdgeSet = tilesConfigVar.Value.GetSet(m_hardEdgeConfig);
            var hardEdgeSet = tilesEditConfigVar.Value.GetSet(m_hardEdgeConfig);
            var activeFloorNodesSet = tilesEditConfigVar.Value.GetSet(m_activeFloorNodesConfig);

            m_hardEdges.Value = new List<Vector3Int>();
            return new AddDefaultCustomFlagsAction(
                m_tilesSetListConfig.Init(nameof(m_tilesSetListConfig), Node).Value, 
                m_geoTiles, 
                m_materialTypeConfig.Init(nameof(m_materialTypeConfig), Node).Value as LevelMaterialTypeConfig, 
                m_currentPosition, m_offsetPositionIndex, 
                m_sourceGrid, m_targetGrid,
                m_hardEdges,
                sourceHardEdgeSet, hardEdgeSet, floorMaterialTypesSet, activeFloorNodesSet);
        }
    }
     
    public class AddDefaultCustomFlagsAction : IDefaultAction
    {
        readonly TileTypeIdentifier m_floorNode;
        readonly TileTypeIdentifier m_wallNode;

        readonly LevelMaterialTypeConfig m_lvlMaterialTypeConfig;

        readonly IVar<Vector3> m_pos;
        readonly Vector3 m_offsetPositionIndex;

        readonly IVar<GridData> m_sourceGrid;
        readonly IVar<GridData> m_targetGrid;
        readonly IVar<HardEdges> m_hardEdges;

        readonly TilesSetData m_geoDataSet;
        readonly TilesSetData m_textureSet;

        readonly TilesSetData m_sourceHardEdgeSet;
        readonly TilesSetData m_hardEdgeSet;
        readonly TilesSetData m_floorMatTypesSet;
        readonly TilesSetData m_activeFloorNodesSet;

        //DebugString m_debug;

        public AddDefaultCustomFlagsAction(TilesSetListConfig tilesSetListConfig, 
            GeometryIdentifierConfig geoTiles, 
            LevelMaterialTypeConfig lvlMaterialTypeConfig,
            IVar<Vector3> pos, Vector3 offsetPositionIndex,
            IVar<GridData> sourceGrid, 
            IVar<GridData> targetGrid,
            IVar<HardEdges> hardEdges, 
            TilesSetData sourceHardEdgeSet,
            TilesSetData hardEdgeSet,
            TilesSetData floorMatTypesSet, 
            TilesSetData activeFloorNodesSet)
        {
            m_floorNode = geoTiles.Get(GeometryType.Floor);
            m_wallNode = geoTiles.Get(GeometryType.Wall);
            m_geoDataSet = tilesSetListConfig.GetSet(geoTiles.TileConfig);

            m_lvlMaterialTypeConfig = lvlMaterialTypeConfig;
            m_textureSet = tilesSetListConfig.GetSet(m_lvlMaterialTypeConfig);

            m_pos = pos;
            m_offsetPositionIndex = offsetPositionIndex;
            m_sourceGrid = sourceGrid;
            m_targetGrid = targetGrid;

            m_hardEdges = hardEdges;

            m_sourceHardEdgeSet = sourceHardEdgeSet;
            m_hardEdgeSet = hardEdgeSet;
            m_floorMatTypesSet = floorMatTypesSet;
            m_activeFloorNodesSet = activeFloorNodesSet;
        }

        public void Invoke()
        {
            //m_debug.Reset();

            var pos = m_pos.Value + m_offsetPositionIndex;
            // compares each of the 4 nodes geo-type (6 comparisons), 4 types = 0, 3 types = 1, 2 types = 2, 1 type = 6
            var equalNodesGeoType = 0;
            // how many 0-4 nodes are of type floor
            var activeFloorNodes = 0;
            // how many 0-4 different texture-types do we have for floor nodes:
            var floorMatTypes = 4;
            // how many 0-4 different texture-types do we have for wall nodes:
            var wallMatTypes = 4;

            var forceHardEdge = false;

            if (pos.x + 1 >= m_sourceGrid.Value.Width || pos.x < 0)
                return;
            if (pos.z + 1 >= m_sourceGrid.Value.Width || pos.z < 0)
                return;
            var posV3 = pos.Vector3Int();
            CollectNodeInfo(posV3, ref activeFloorNodes, ref forceHardEdge, ref floorMatTypes, ref wallMatTypes, ref equalNodesGeoType);
            if (m_sourceHardEdgeSet.GetTileIdx(m_sourceGrid.Value[posV3]) > 0)
                forceHardEdge = true;

            //m_debug.Prepend($"pos {pos}, equalNodesGeoType {equalNodesGeoType} floorMatTypes {floorMatTypes} " +
            //                          $"wallMatTypes {wallMatTypes} activeFloorNodes {activeFloorNodes} " +
            //                          $"forceHardEdge {forceHardEdge} \n");
            //m_debug.Print();

            var flag = GetHardEdgeState(equalNodesGeoType, floorMatTypes, wallMatTypes, activeFloorNodes, forceHardEdge);
            UpdateGrid(posV3, flag, floorMatTypes, activeFloorNodes);
        }

        void CollectNodeInfo(Vector3Int pos, ref int activeFloorNodes, ref bool forceHardEdge, ref int floorMatTypes, ref int wallMatTypes, ref int equalNodesGeoType)
        {
            var sourceGrid = m_sourceGrid.Value;

            for (var nodeAOffIdx = 0; nodeAOffIdx < IterationOffset.Length; nodeAOffIdx++)
            {
                var offA = IterationOffset[nodeAOffIdx];
                var offsetPosA = pos + offA;
                var nodeA = sourceGrid[offsetPosA];

                var equalsInMatType = false;

                var nodeAGeoType = m_geoDataSet.GetTileIdx(nodeA);
                var nodeAMatType = m_textureSet.GetTileIdx(nodeA);
                //m_debug.AddLog($"nodeA {offsetPosA} " +
                //          $"GeoType {m_geoDataSet.TileConfig.Result[nodeAGeoType].TileName} nodeAMatType {nodeAMatType} \n ");

                for (var nodeBOffIdx = nodeAOffIdx + 1; nodeBOffIdx < IterationOffset.Length; nodeBOffIdx++)
                {
                    var offB = IterationOffset[nodeBOffIdx];
                    var offsetPosB = pos + offB; 
                    var nodeB = sourceGrid[offsetPosB];

                    var nodeBGeoType = m_geoDataSet.GetTileIdx(nodeB);
                    var nodeBMatType = m_textureSet.GetTileIdx(nodeB);
                    //m_debug.AddLog($"nodeB {offsetPosB} " +
                    //          $"GeoType {m_geoDataSet.TileConfig.Result[nodeBGeoType].TileName} nodeBMatType {nodeBMatType}\n");

                    if (nodeAGeoType != nodeBGeoType)
                        continue;

                    ++equalNodesGeoType;
                    equalsInMatType |= (nodeAMatType == nodeBMatType);
                }

                var isFloor = nodeAGeoType == m_floorNode.TileIdx;
                var isWall = nodeAGeoType == m_wallNode.TileIdx;

                if (isFloor)
                {
                    ++activeFloorNodes;
                    if (m_lvlMaterialTypeConfig != null)
                        forceHardEdge |= m_lvlMaterialTypeConfig.IsTypeForcingAngularCorners(nodeAMatType);
                }

                if (!isFloor || equalsInMatType) 
                    --floorMatTypes;
                if (!isWall || equalsInMatType) 
                    --wallMatTypes;
            }
        }

        void UpdateGrid(Vector3Int pos, HardEdgeState flag, int floorMatTypes, int activeFloorNodes)
        {
            var gridData = m_targetGrid.Value;

            var tileData = gridData[pos];
            tileData = m_hardEdgeSet.GetCombinedTile(tileData, (uint) flag);
            //if (pos.x == 0 && pos.z == 0)
            //    Debug.Log($" After m_hardEdgeSet {tileData} | {flag} {m_hardEdgeSet.Mask} {m_hardEdgeSet.Shift}");
            tileData = m_floorMatTypesSet.GetCombinedTile(tileData, (uint) floorMatTypes);
            //if (pos.x == 0 && pos.z == 0)
            //    Debug.Log($" After m_floorMatTypesSet {tileData}");
            tileData = m_activeFloorNodesSet.GetCombinedTile(tileData, (uint) activeFloorNodes);
            //if (pos.x == 0 && pos.z == 0)
            //    Debug.Log($" After m_activeFloorNodesSet {tileData}");

            if (flag == HardEdgeState.Hard || flag == HardEdgeState.BreakAndHard)
            {
                //Debug.Log($"{flag} at {pos}");
                m_hardEdges.Value.Data.Add(pos);
            }

            gridData[pos] = tileData;

        }

        HardEdgeState GetHardEdgeState(int equalNodesGeoType, 
            int floorMatTypes, int wallMatTypes,
            int activeFloorNodes,
            bool forceHardEdge)
        {
            var allNodesEqualGeoType = equalNodesGeoType == 6;
            var multipleTexAndGeometryTypes = floorMatTypes > 1 && !allNodesEqualGeoType;
            var tooManyMatTypes = floorMatTypes > 2 || (wallMatTypes > 1 && !allNodesEqualGeoType);
            var tooManyGeoTypes = equalNodesGeoType <= 1;

            var hard = tooManyMatTypes || multipleTexAndGeometryTypes || tooManyGeoTypes;
            // when we have 2 activeFloorNodes or all node types are equal we don't have diagonal edges anyway, so it breaks the force-hard-edge propagation
            var breakHard = !hard && (activeFloorNodes == 2 || allNodesEqualGeoType);
            var flag = GetHardEdgeState(hard, forceHardEdge, breakHard);

            //if (flag == HardEdgeState.Hard || flag == HardEdgeState.BreakAndHard)
            //{
            //    m_debug.Prepend(
            //        $"tooManyMatTypes {tooManyMatTypes} multipleTexAndGeometryTypes {multipleTexAndGeometryTypes} " +
            //        $"tooManyGeoTypes {tooManyGeoTypes} activeFloorNodes {activeFloorNodes} " +
            //        $"allNodesEqualGeoType {allNodesEqualGeoType} \n");
            //    m_debug.Print();
            //}

            return flag;
        }

        static HardEdgeState GetHardEdgeState(bool hard, bool forceHardEdge, bool breakHard)
        {
            var flag = HardEdgeState.Default;
            if (hard || forceHardEdge)
                flag |= HardEdgeState.Hard;
            if (breakHard)
                flag |= HardEdgeState.Break;
            return flag;
        }
    }
}
