﻿using System;
using Core.Interface;
using Core.Unity.Extensions;
using Level.Data;
using Level.Tiles;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;
using UnityEngine.Tilemaps;
using static Level.Tiles.TileTypeOperations;

namespace Level.Actions.Utility
{
    public class AddDestinationFlag : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input]
        internal TileTypeIdentifier m_resultTag;
        [SerializeField, Input]
        internal TilesSetListConfig m_destinationConfig;

        [SerializeField, Input]
        internal VarReference<GridData> m_resultGrid;
        [SerializeField, Input]
        internal VarReference<Vector3> m_currentPosition;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(AddDestinationFlag);
        public static Type StaticFactoryType => typeof(AddDestinationFlagAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction()
        {
            var tileFilter = GetFilter(m_destinationConfig, m_resultTag);
            return new AddDestinationFlagAction(tileFilter, m_resultGrid, m_currentPosition);
        }
    }

    public class AddDestinationFlagAction : IDefaultAction
    {
        readonly TilesSetFilter m_tileTypeToAdd;
        readonly IVar<GridData> m_resultGrid;
        readonly IVar<Vector3> m_currentPosition;

        public AddDestinationFlagAction(TilesSetFilter tileTypeToAdd, 
            IVar<GridData> resultGrid, IVar<Vector3> currentPosition)
        {
            m_tileTypeToAdd = tileTypeToAdd;
            m_resultGrid = resultGrid;
            m_currentPosition = currentPosition;
        }

        public void Invoke()
        {
            var resultData = m_resultGrid.Value;
            var pos = m_currentPosition.Value.Vector3Int();
            Combine(ref resultData, pos, m_tileTypeToAdd);
        }
    }
}
