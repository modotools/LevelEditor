﻿using System;
using System.Linq;
using Core.Interface;
using Level.PlatformLayer.Interface;
using Level.Room;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions.Utility
{
    public class SelectPlatformInstance : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        // todo: must be RoomInstanceComponent
        [SerializeField, Input] internal VarReference<IRoom> m_room;
        [SerializeField, Input] internal VarReference<IPlatformLayer> m_currentPlatform;
        [SerializeField, Input] internal VarReference<Vector3> m_currentPosition;
        [SerializeField, Input] internal VarReference<PlatformInstanceData> m_instance;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(SelectPlatformInstance);
        public static Type StaticFactoryType => typeof(SelectPlatformInstanceAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction()
        {
            return new SelectPlatformInstanceAction(m_room.Init(nameof(m_room), Node),
                m_currentPlatform.Init(nameof(m_currentPlatform), Node),
                m_instance.Init(nameof(m_instance), Node),
                m_currentPosition.Init(nameof(m_currentPosition), Node));
        }
    }

    public class SelectPlatformInstanceAction : IDefaultAction
    {
        readonly IVar<IRoom> m_room;
        readonly IVar<IPlatformLayer> m_platform;
        readonly IVar<PlatformInstanceData> m_instance;
        readonly IVar<Vector3> m_currentPos;

        public SelectPlatformInstanceAction(IVar<IRoom> room,
            IVar<IPlatformLayer> platformLayer, IVar<PlatformInstanceData> platformInstance,
            IVar<Vector3> currentPos)
        {
            m_room = room;
            m_platform = platformLayer;
            m_instance = platformInstance;
            m_currentPos = currentPos;
        }

        public void Invoke()
        {
            var room = m_room.Value;

            if (!(room is RoomInstanceComponent ric))
                return;

            var lvl =  m_currentPos.Value.y;
            var lvlIdx = (int) lvl;
            m_instance.Value = ric.Data.VisualData.GetPlatformInstance(lvlIdx);
            m_platform.Value = ric.Data.PlatformLayer.Count() > lvlIdx 
                ? ric.Data.PlatformLayer.ElementAt(lvlIdx)
                : null;
        }
    }
}
