﻿using System;
using Core.Interface;
using Core.Unity.Extensions;
using Level.Data;
using Level.Tiles;
using Level.Tiles.Interface;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;
using static Level.Tiles.TileProcessing;

namespace Level.Actions.Utility
{
    public class ConfigForFilter : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<TilesSetListConfig> m_primaryFilterSetListConfig;
        [SerializeField] internal TileTypeIdentifier m_primaryFilterIdentifier;

        [SerializeField, Input] internal VarReference<TilesSetFilter> m_filter;
        [SerializeField, Input] internal VarReference<GridData> m_grid;
        [SerializeField, Input] internal VarReference<Vector3> m_pos;

        [SerializeField, NodeConnectorList(IO.Input, typeof(VarReference<int>))]
        internal VarReference<int>[] m_configuration;

        [SerializeField] bool m_invertConfiguration;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(ConfigForFilter);
        public static Type StaticFactoryType => typeof(ConfigForFilterAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction()
        {
            if (!GetFilter(out var primaryFilter)) 
                return null;

            return new ConfigForFilterAction(primaryFilter,
                m_filter, m_grid, m_pos, m_invertConfiguration,
                m_configuration.Init(nameof(m_configuration), Node));
        }

        bool GetFilter(out TilesSetFilter primaryFilter)
        {
            primaryFilter = default;
            var primFilterVar = m_primaryFilterSetListConfig.Init(nameof(m_primaryFilterSetListConfig), Node);
            var primaryFilterSetData = primFilterVar.Value.GetSet(m_primaryFilterIdentifier.TileConfig.Result);
            if (primaryFilterSetData.TileConfig == null)
            {
                Debug.LogError(
                    $"{nameof(ITileConfig)} {m_primaryFilterIdentifier.TileConfig} not found in given {nameof(TilesSetListConfig)}");
                return false;
            }

            primaryFilter = new TilesSetFilter(primaryFilterSetData, m_primaryFilterIdentifier.TileIdx);
            return true;
        }
    }

    public class ConfigForFilterAction : IDefaultAction
    {
        readonly TilesSetFilter m_primaryFilter;
        readonly IVar<TilesSetFilter> m_tilesSetFilter;
        readonly IVar<GridData> m_grid;
        readonly IVar<Vector3> m_pos;

        readonly bool m_invertConfiguration;

        readonly IVar<int>[] m_configuration;

        public ConfigForFilterAction(TilesSetFilter primaryFilter, 
            IVar<TilesSetFilter> tilesSetFilter, 
            IVar<GridData> grid, 
            IVar<Vector3> pos,
            bool invertConfiguration,
            IVar<int>[] configuration)
        {
            m_primaryFilter = primaryFilter;
            m_tilesSetFilter = tilesSetFilter;
            m_grid = grid;
            m_pos = pos;
            m_invertConfiguration = invertConfiguration;

            m_configuration = configuration;
        }

        public void Invoke()
        {
            var tiles = m_grid.Value;
            var pos = m_pos.Value.Vector3Int();
            // tileTypes:
            var result = GetConfiguration(m_primaryFilter, m_tilesSetFilter.Value, tiles, pos);
            var config = result.Configuration;
            if (m_invertConfiguration)
                config = ~config & 15;
            //Debug.Log($"pos {m_pos.Value} config {config}");

            foreach (var c in m_configuration)
                c.Value = config;
        }
    }
}
