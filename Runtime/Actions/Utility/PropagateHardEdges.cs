﻿using System;
using Core.Interface;
using Core.Types;
using Level.Data;
using Level.Tiles;
using Level.Tiles.MarchingSquares;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions.Utility
{
    public class PropagateHardEdges : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input]
        internal VarReference<GridData> m_grid;
        [SerializeField, Input]
        internal VarReference<HardEdges> m_hardEdges;

        [SerializeField, Input]
        internal VarReference<TilesSetListConfig> m_tilesEditConfig;
        [SerializeField] internal HardEdgeConfig m_hardEdgeConfig;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(PropagateHardEdges);
        public static Type StaticFactoryType => typeof(PropagateHardEdgesAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction()
        {
            var config = m_tilesEditConfig.Init(nameof(m_tilesEditConfig), Node);
            var hardEdgeSet = config.Value.GetSet(m_hardEdgeConfig);

            return new PropagateHardEdgesAction(m_grid, m_hardEdges, hardEdgeSet);
        }
    }

    public class PropagateHardEdgesAction : IDefaultAction
    {
        readonly IVar<GridData> m_grid;
        readonly IVar<HardEdges> m_hardEdges;

        readonly TilesSetData m_hardEdgeSet;
        public PropagateHardEdgesAction(IVar<GridData> gridReference, 
            IVar<HardEdges> hardEdges, TilesSetData hardEdgesSet)
        {
            m_grid = gridReference;
            m_hardEdges = hardEdges;
            m_hardEdgeSet = hardEdgesSet;
        }

        public void Invoke()
        {
            var grid = m_grid.Value;
            var hardEdges = m_hardEdges.Value;
            for (var i = 0; i < hardEdges.Count; i++)
            {
                var edge = hardEdges[i];
                for (var y = edge.y - 1; y >= 0; --y)
                {
                    if (Apply(new Vector3Int(edge.x, y, edge.z), ref grid) == FlowResult.Stop) 
                        break;
                }
                for (var y = edge.y + 1; y < grid.Height; ++y)
                {
                    if (Apply(new Vector3Int(edge.x, y, edge.z), ref grid) == FlowResult.Stop) 
                        break;
                }
            }
        }

        FlowResult Apply(Vector3Int pos, ref GridData grid)
        {
            uint tile = grid[pos];
            var hardState = (HardEdgeState) m_hardEdgeSet.GetTileIdx(tile);
            if (hardState == HardEdgeState.Break)
                return FlowResult.Stop;

            grid[pos] = m_hardEdgeSet.GetCombinedTile(tile, (uint) HardEdgeState.Hard);
            //if (pos.x == 0 && pos.z == 0)
            //    Debug.Log($" After Propagate Edges { grid[pos.x, pos.y, pos.z]}");
            return FlowResult.Continue;
        }
    }
}
