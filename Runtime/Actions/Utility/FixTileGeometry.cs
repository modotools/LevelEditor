﻿using System;
using Core.Interface;
using Core.Unity.Extensions;
using Level.Data;
using Level.Tiles;
using Level.Enums;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions.Utility
{
    public class FixTileGeometry : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input]
        internal VarReference<GridData> m_grid;
        [SerializeField, Input]
        internal VarReference<Vector3> m_currentPosition;

        [SerializeField, Input] internal VarReference<TilesSetListConfig> m_setListConfig;
        [SerializeField] internal GeometryIdentifierConfig m_geoTiles;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(FixTileGeometry);
        public static Type StaticFactoryType => typeof(FixTileGeometryAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction()
        {
            var setListConfigVar = m_setListConfig.Init(nameof(m_setListConfig), Node);
            var setData = setListConfigVar.Value.GetSet(m_geoTiles.TileConfig);

            return new FixTileGeometryAction(m_grid, m_currentPosition, 
                setData, m_geoTiles);
        }
    }

    public class FixTileGeometryAction : IDefaultAction
    {
        public FixTileGeometryAction(IVar<GridData> gr, IVar<Vector3> gridPos, 
            TilesSetData setData,
            GeometryIdentifierConfig geoTiles)
        {
            m_gridReference = gr;
            m_gridPos = gridPos;
            m_setData = setData;
            m_floor = geoTiles.Get(GeometryType.Floor);
            m_wall = geoTiles.Get(GeometryType.Wall);
            m_empty = geoTiles.Get(GeometryType.Empty);
        }

        readonly IVar<GridData> m_gridReference;
        readonly IVar<Vector3> m_gridPos;
        readonly TilesSetData m_setData;

        readonly TileTypeIdentifier m_floor;
        readonly TileTypeIdentifier m_wall;
        readonly TileTypeIdentifier m_empty;

        public void Invoke()
        {
            var grid = m_gridReference.Value;
            var pos = m_gridPos.Value.Vector3Int();

            var hasAbove = grid.Height > (pos.y+1);
            if (!hasAbove)
                return;
            var abovePos = pos + Vector3Int.up;
            var aboveTile= m_setData.GetTileIdx(m_gridReference.Value[abovePos]);
            var gridTile = m_setData.GetTileIdx(grid[pos]);
            if (aboveTile == m_floor.TileIdx && gridTile != m_wall.TileIdx)
                grid[pos.x, pos.y, pos.z] = m_setData.GetCombinedTile(gridTile, m_wall.TileIdx);
            if (aboveTile == m_empty.TileIdx && gridTile == m_wall.TileIdx)
                grid[pos.x, pos.y+1, pos.z] = m_setData.GetCombinedTile(aboveTile, m_floor.TileIdx);

            //if (pos.x == 0 && pos.z == 0)
            //    Debug.Log($" After FixTileGeometryAction {grid[pos.x, pos.y, pos.z]}");
        }
    }
}
