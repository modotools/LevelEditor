﻿using System;
using Core.Interface;
using Core.Unity.Extensions;
using Level.Data;
using Level.Tiles;
using Level.Enums;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;
using static Level.Tiles.TileTypeOperations;

namespace Level.Actions.Utility
{
    public class UpdateAutoTile : ScriptableBaseAction
    {

#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<GridData> m_grid;
        [SerializeField, Input] internal VarReference<Vector3> m_currentPosition;

        [SerializeField, Input] internal VarReference<TilesSetListConfig> m_setListConfig;
        [SerializeField] GeometryIdentifierConfig m_geoTiles;

        // todo: maybe we want to set a height for floor level, then we can use procedural height map for this
        // outside areas would fix with floor, dungeons would fix with wall
        [SerializeField] internal FixBelowWallSolution m_fixBelowWallWith = FixBelowWallSolution.Floor;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(UpdateAutoTile);
        public static Type StaticFactoryType => typeof(UpdateAutoTileAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction()
        {
            var setListVar = m_setListConfig.Init(nameof(m_setListConfig), Node);
            var setData = setListVar.Value.GetSet(m_geoTiles.TileConfig);

            return new UpdateAutoTileAction(m_grid, m_currentPosition, 
                setData, m_geoTiles, m_fixBelowWallWith);
        }
    }

    public class UpdateAutoTileAction : IDefaultAction
    {
        public UpdateAutoTileAction(IVar<GridData> gr, IVar<Vector3> gridPos, 
            TilesSetData setData,
            GeometryIdentifierConfig identifier,
            FixBelowWallSolution solution)
        {
            m_gridReference = gr;
            m_gridPos = gridPos;
            m_setData = setData;
            m_auto = identifier.Get(GeometryType.Auto);
            m_floor = identifier.Get(GeometryType.Floor);
            m_wall = identifier.Get(GeometryType.Wall);
            m_empty = identifier.Get(GeometryType.Empty);

            m_fixBelowWallSolution = solution;
        }

        readonly IVar<GridData> m_gridReference;
        readonly IVar<Vector3> m_gridPos;
        readonly TilesSetData m_setData;

        readonly TileTypeIdentifier m_auto;
        readonly TileTypeIdentifier m_floor;
        readonly TileTypeIdentifier m_wall;
        readonly TileTypeIdentifier m_empty;

        readonly FixBelowWallSolution m_fixBelowWallSolution;
        public void Invoke()
        {
            var grid = m_gridReference.Value;
            var pos = m_gridPos.Value.Vector3Int();
            var belowPos = pos - Vector3Int.up;
            // todo: idea for outside rooms only raise walls on unconnected sides of room (exit)
            // var isAtEdgeOfRoom = IsEdge(ref grid, ref pos);
            // var fix = isAtEdgeOfRoom?FixBelowWallSolution.Wall : m_fixBelowWallSolution;

            var prevTile = grid[pos];
            var gridTile = m_setData.GetTileIdx(grid[pos]);
            if (gridTile != m_auto.TileIdx)
                return;
            
            var belowTile = m_wall.TileIdx;
            var hasBelow = pos.y > 0;
            if (hasBelow) 
                belowTile = m_setData.GetTileIdx(m_gridReference.Value[belowPos]);

            if (belowTile == m_floor.TileIdx || belowTile == m_empty.TileIdx)
                Combine(ref grid, pos, m_setData, m_empty);
            else if (belowTile == m_wall.TileIdx)
                Combine(ref grid, pos, m_setData, m_fixBelowWallSolution == FixBelowWallSolution.Floor
                    ? m_floor
                    : m_wall);
            else Debug.LogError($"Left Auto tile untouched {pos}");

            //if (pos.x == 0 && pos.z == 0)
            //    Debug.Log($" After UpdateAutoTile {grid[pos.x, pos.y, pos.z]}");
        }

        bool IsEdge(ref GridData gridData, ref Vector3Int pos)
        {
            if (pos.x == 0 || pos.z == 0)
                return true;
            if (pos.x + 1 == gridData.Width)
                return true;
            if (pos.z + 1 == gridData.Depth)
                return true;
            return false;
        }
    }
    public enum FixBelowWallSolution
    {
        Floor,
        Wall
    }
}
