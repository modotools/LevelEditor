﻿using System;
using System.Collections.Generic;
using Core.Interface;
using Core.Unity.Extensions;
using Level.Data;
using Level.Tiles;
using Level.Tiles.Interface;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;
using static Level.Tiles.TileProcessing;

namespace Level.Actions.Utility
{
    public class TransitionCases : ScriptableBaseAction, ILinkActionConfigs
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<TilesSetListConfig> m_primaryFilterSetListConfig;
        [SerializeField] internal TileTypeIdentifier m_primaryFilterIdentifier;

        [SerializeField, Input] internal VarReference<TilesSetFilter> m_tileSetFilterA;

        [SerializeField, Input] internal VarReference<GridData> m_grid;
        [SerializeField, Input] internal VarReference<Vector3> m_pos;

        [SerializeField, Input] internal VarReference<int> m_configuration;
        [SerializeField, Input] internal VarReference<int> m_uvConfiguration;

        [SerializeField, HideInInspector, NodeConnector(IO.Output, typeof(ScriptableActionConnector))] 
        internal ScriptableBaseAction m_continueAction;
        //[SerializeField] bool m_invertConfiguration;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(TransitionCases);
        public static Type StaticFactoryType => typeof(TransitionCasesAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction()
        {
            if (!GetFilter(out var primaryFilter)) 
                return null;

            return new TransitionCasesAction(primaryFilter,
                m_tileSetFilterA, m_grid, m_pos, 
                m_configuration.Init(nameof(m_configuration), Node),
                m_uvConfiguration.Init(nameof(m_uvConfiguration), Node));
        }

        bool GetFilter(out TilesSetFilter primaryFilter)
        {
            primaryFilter = default;
            var primFilterVar = m_primaryFilterSetListConfig.Init(nameof(m_primaryFilterSetListConfig), Node);
            var primaryFilterSetData = primFilterVar.Value.GetSet(m_primaryFilterIdentifier.TileConfig.Result);
            if (primaryFilterSetData.TileConfig == null)
            {
                Debug.LogError(
                    $"{nameof(ITileConfig)} {m_primaryFilterIdentifier.TileConfig} not found in given {nameof(TilesSetListConfig)}");
                return false;
            }

            primaryFilter = new TilesSetFilter(primaryFilterSetData, m_primaryFilterIdentifier.TileIdx);
            return true;
        }

        public IEnumerable<ScriptableObject> Dependencies 
            => new[] { m_continueAction };
        public IEnumerable<IActionConfig> GetActionConfigsLinked()
            => new[] { m_continueAction };

        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (graphActions[this] is TransitionCasesAction action)
                action.LinkActions(graphActions[m_continueAction] as IDefaultAction);
        }
    }

    public class TransitionCasesAction : IDefaultAction
    {
        readonly TilesSetFilter m_primaryFilter;
        readonly IVar<TilesSetFilter> m_tilesSetFilterA;

        readonly IVar<GridData> m_grid;
        readonly IVar<Vector3> m_pos;

        readonly IVar<int> m_configuration;
        readonly IVar<int> m_uvConfiguration;

        IDefaultAction m_continueAction;

        public TransitionCasesAction(TilesSetFilter primaryFilter, 
            IVar<TilesSetFilter> tilesSetFilterA, 
            IVar<GridData> grid, 
            IVar<Vector3> pos,
            IVar<int> configuration,
            IVar<int> uvConfiguration)
        {
            m_primaryFilter = primaryFilter;
            m_tilesSetFilterA = tilesSetFilterA;
            m_grid = grid;
            m_pos = pos;
            m_configuration = configuration;
            m_uvConfiguration = uvConfiguration;
        }

        public void Invoke()
        {
            var tiles = m_grid.Value;

            var tileFilterA = m_tilesSetFilterA.Value;
            var pos = m_pos.Value.Vector3Int();

            //tileTypes:
            var uvCase = GetConfiguration(tileFilterA, default, tiles, pos);
            m_uvConfiguration.Value = uvCase.Configuration;
            var floorCases = GetConfiguration(m_primaryFilter, default, tiles, pos);

            Case(floorCases, CornerIdx.BottomLeft, CaseValue.BottomLeft);
            Case(floorCases, CornerIdx.BottomRight, CaseValue.BottomRight);
            Case(floorCases, CornerIdx.TopRight, CaseValue.TopRight);
            Case(floorCases, CornerIdx.TopLeft, CaseValue.TopLeft);
        }

        void Case(ConfigurationResult floorCases, CornerIdx cornerIdx, CaseValue caseValue)
        {
            if (!floorCases.IsActive(cornerIdx))
                return;
            m_configuration.Value = (int) caseValue;
            m_continueAction.Invoke();
        }

        public void LinkActions(IDefaultAction graphAction) => m_continueAction = graphAction;
    }
}
