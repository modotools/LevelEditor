﻿using System;
using Core.Interface;
using Core.Types;
using Core.Unity.Extensions;
using Level.Data;
using Level.Tiles;
using Level.Tiles.Interface;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;
using static Level.Tiles.TileProcessing;
using static Level.Tiles.TileTypeOperations;

namespace Level.Actions.Utility
{
    public class MatchPattern : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input]
        internal TilesSetListConfig m_sourceConfig;

        [SerializeField, Input]
        internal VarReference<TilesSetFilter> m_filter;
        [SerializeField, Input]
        internal VarReference<GridData> m_grid;
        [SerializeField, Input]
        internal VarReference<Vector3> m_pos;
#pragma warning restore 0649 // wrong warning for SerializeField

        internal TileTypeIdentifier Identifier;
        internal int Configuration;

        public bool InvertSourceConfiguration;

        [Tooltip("AND = contained?/ Configuration as Minimum \n" +
                 "OR = allowed?/ Configuration as Maximum \n" +
                 "None = direct equality comparison")]
        public BinaryOperation Operation;
        //public bool OnlyTwoTileTypes;
        //public string Tag;

        public override string Name => nameof(MatchPattern);
        public static Type StaticFactoryType => typeof(MatchPatternAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction()
        {
            var set = m_sourceConfig.GetSet(Identifier.TileConfig.Result);
            if (set.TileConfig == null)
            {
                Debug.LogError($"{nameof(ITileConfig)} {Identifier.TileConfig} not found in given {nameof(TilesSetListConfig)}");
                return null;
            }

            return new MatchPatternAction(m_filter, m_grid, m_pos,
                GetFilter(set, Identifier), Configuration, InvertSourceConfiguration, Operation);
        }
    }

    public class MatchPatternAction : IBoolAction
    {
        bool m_result;

        readonly IVar<TilesSetFilter> m_tilesSetFilter;
        readonly IVar<GridData> m_grid;
        readonly IVar<Vector3> m_pos;
        readonly TilesSetFilter m_identifier;
        readonly int m_configuration;
        readonly bool m_invertSourceConfiguration;
        readonly BinaryOperation m_binaryOperation;

        public MatchPatternAction(IVar<TilesSetFilter> tilesSetFilter, 
            IVar<GridData> grid, 
            IVar<Vector3> pos, 
            TilesSetFilter identifier, 
            int configuration, 
            bool invertSourceConfiguration,
            BinaryOperation binOp)
        {
            m_tilesSetFilter = tilesSetFilter;
            m_grid = grid;
            m_pos = pos;
            m_identifier = identifier;
            m_configuration = configuration;
            m_invertSourceConfiguration = invertSourceConfiguration;
            m_binaryOperation = binOp;
        }

        public void Invoke()
        {
            m_result = false;


            var tiles = m_grid.Value;

            //tileTypes:
            var config = GetConfiguration(m_identifier, 
                m_tilesSetFilter.Value, tiles, m_pos.Value.Vector3Int()).Configuration;

            if (m_invertSourceConfiguration)
                config = ~config;

            switch (m_binaryOperation)
            {
                case BinaryOperation.AND:
                    config &= m_configuration; break;
                case BinaryOperation.OR:
                    config |= m_configuration; break;
                case BinaryOperation.None: break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            m_result = m_configuration == config;
        }

        public void Invoke(out bool result)
        {
            Invoke();
            result = m_result;
        }
    }
}
