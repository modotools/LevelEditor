using System;
using Core.Extensions;
using Core.Interface;
using Level.Data;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions.Mesh
{
    public class ApplyMeshData : ScriptableBaseAction
    {        
        public VarReference<MeshData> MeshData;
        public VarReference<UnityEngine.Mesh> MeshObject;
        public VarReference<GameObject> MeshGo;

        public override string Name => nameof(ApplyMeshData);
        public static Type StaticFactoryType => typeof(ApplyMeshDataAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() =>
            new ApplyMeshDataAction(MeshData.Init(nameof(MeshData), Node), 
                MeshObject.Init(nameof(MeshObject), Node),
                MeshGo.Init(nameof(MeshGo), Node));
    }

    public class ApplyMeshDataAction : IDefaultAction
    {
        readonly IVar<MeshData> m_meshData;
        readonly IVar<UnityEngine.Mesh> m_meshObj;
        readonly IVar<GameObject> m_meshGO;

        public ApplyMeshDataAction(IVar<MeshData> meshData, IVar<UnityEngine.Mesh> meshObj, IVar<GameObject> meshGO)
        {
            m_meshData = meshData;
            m_meshObj = meshObj;
            m_meshGO = meshGO;
        }

        public void Invoke()
        {
            ApplyMesh();
            UpdateBounds();
        }

        void ApplyMesh()
        {
            var mesh = m_meshObj.Value;
            if (mesh == null)
                return;

            mesh.Clear();

            var md = m_meshData.Value;
            mesh.vertices = md.Vertices.ToArray();
            mesh.uv = md.UVs.ToArray();
            if (!md.UVs2.IsNullOrEmpty())
                mesh.uv2 = md.UVs2.ToArray();
            if (!md.UVs3.IsNullOrEmpty())
                mesh.uv3 = md.UVs3.ToArray();
            mesh.triangles = md.Triangles.ToArray();
            mesh.RecalculateNormals();
            mesh.RecalculateTangents();
            mesh.RecalculateBounds();
        }

        void UpdateBounds()
        {
            var meshGo = m_meshGO.Value;
            if (meshGo == null)
                return;
            var meshTr = meshGo.transform;
            // todo: ugly hotfix for updating bounds of object
            var sibIdx = meshTr.GetSiblingIndex();
            var p = meshTr.transform.parent;
            meshTr.transform.SetParent(null);
            meshTr.transform.SetParent(p);
            meshTr.SetSiblingIndex(sibIdx);
        }
    }

}
