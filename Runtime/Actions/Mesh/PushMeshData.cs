﻿using System;
using Core.Extensions;
using Core.Interface;
using Level.Data;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions.Mesh
{
    public class PushMeshData : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input]
        internal VarReference<MeshData> m_pushData;
        [SerializeField, Input]
        internal VarReference<MeshData> m_targetData;

        [SerializeField] bool m_releaseDataAfterPush = true;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(PushMeshData);
        public static Type StaticFactoryType => typeof(PushMeshDataAction);

        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() => new PushMeshDataAction(m_pushData, m_targetData, m_releaseDataAfterPush);
    }

    public class PushMeshDataAction : IDefaultAction
    {
        public PushMeshDataAction(IVar<MeshData> push, IVar<MeshData> target, bool releaseAfterPush)
        {
            m_push = push;
            m_target = target;
            m_releaseDataAfterPush = releaseAfterPush;
        }

        readonly IVar<MeshData> m_push;
        readonly IVar<MeshData> m_target;
        readonly bool m_releaseDataAfterPush;

        public void Invoke()
        {
            var source = m_push.Value;
            var target = m_target.Value;

            var startCount = target.Vertices.Count;

            if (!source.Vertices.IsNullOrEmpty())
            {
                target.Vertices.AddRange(source.Vertices);
                target.UVs.AddRange(source.UVs);
                if (!source.UVs2.IsNullOrEmpty())
                    target.UVs2.AddRange(source.UVs2);
                if (!source.UVs3.IsNullOrEmpty())
                    target.UVs3.AddRange(source.UVs3);

                for (var i = 0; i < source.Triangles.Count; i++) 
                    target.Triangles.Add(startCount + source.Triangles[i]);

                source.Vertices.Clear();
                source.UVs.Clear();
                source.UVs2.Clear();
                source.UVs3.Clear();

                source.Triangles.Clear();
            }

            if (m_releaseDataAfterPush)
            {
                MeshData.Release(ref source);
                m_push.Value = source;
            }

        }
    }
}
