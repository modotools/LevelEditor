﻿using System;
using Core.Interface;
using Level.Data;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions.Mesh
{
    public class PushMaterialToMeshData : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] VarReference<MeshData> m_pushMeshData;
        [SerializeField, Input] VarReference<Material> m_targetMaterial;
        [SerializeField, Input] VarReference<MaterialToMeshData> m_targetData;

        [SerializeField] bool m_releaseDataAfterPush = true;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(PushMaterialToMeshData);
        public static Type StaticFactoryType => typeof(PushMaterialToMeshDataAction);

        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() => new PushMaterialToMeshDataAction(
            m_pushMeshData.Init(nameof(m_pushMeshData), Node),
            m_targetMaterial.Init(nameof(m_targetMaterial), Node),
            m_targetData.Init(nameof(m_targetData), Node),
            m_releaseDataAfterPush);
    }

    public class PushMaterialToMeshDataAction : IDefaultAction
    {
        public PushMaterialToMeshDataAction(IVar<MeshData> pushMeshData,
            IVar<Material> targetMaterial,
            IVar<MaterialToMeshData> target,
            bool release)
        {
            m_pushMeshData = pushMeshData;
            m_material = targetMaterial;
            m_target = target;
            m_release = release;
        }

        readonly IVar<MeshData> m_pushMeshData;
        readonly IVar<Material> m_material;
        readonly IVar<MaterialToMeshData> m_target;
        readonly bool m_release;

        public void Invoke()
        {
            Debug.Assert(m_material.Value != null);

            var targetMap = m_target.Value.MaterialMeshData;
            if (!targetMap.ContainsKey(m_material.Value))
                targetMap.Add(m_material.Value, MeshData.Default());

            var sourceMeshData = m_pushMeshData.Value;
            var targetMeshData = targetMap[m_material.Value];

            MeshData.Merge(ref targetMeshData, sourceMeshData);
            targetMap[m_material.Value] = targetMeshData;

            if (m_release) 
                Release(ref sourceMeshData);
        }

        void Release(ref MeshData sourceMeshData)
        {
            MeshData.Release(ref sourceMeshData);
            m_pushMeshData.Value = sourceMeshData;
        }
    }
}
