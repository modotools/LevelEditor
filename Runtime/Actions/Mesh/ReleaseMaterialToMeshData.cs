﻿using System;
using Core.Interface;
using Level.Data;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions.Mesh
{
    
    public class ReleaseMaterialToMeshData : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, NodeConnectorList(IO.Input, typeof(VarReference<MaterialToMeshData>))]
        internal VarReference<MaterialToMeshData> m_meshData;

        [SerializeField, NodeConnectorList(IO.Input, typeof(VarReference<MeshData>))]
        internal VarReference<MeshData> m_colliderMeshData;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(ReleaseMaterialToMeshData);
        public static Type StaticFactoryType => typeof(ReleaseMaterialToMeshDataAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() => 
            new ReleaseMaterialToMeshDataAction(m_meshData.Init(nameof(m_meshData), Node),
                m_colliderMeshData.Init(nameof(m_colliderMeshData), Node));
    }

    public class ReleaseMaterialToMeshDataAction : IDefaultAction
    {
        readonly IVar<MaterialToMeshData> m_meshData;
        readonly IVar<MeshData> m_colliderMeshData;

        public ReleaseMaterialToMeshDataAction(IVar<MaterialToMeshData> meshData, IVar<MeshData> colliderMeshData)
        {
            m_meshData = meshData;
            m_colliderMeshData = colliderMeshData;
        }

        public void Invoke()
        {
            var val = m_meshData.Value;
            MaterialToMeshData.Release(ref val);
            m_meshData.Value = val;
        }

        public void ReleaseCollider()
        {
            var val = m_colliderMeshData.Value;
            MeshData.Release(ref val);
            m_colliderMeshData.Value = val;
        }

    }

}
