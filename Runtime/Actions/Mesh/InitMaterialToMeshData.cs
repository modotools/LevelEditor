﻿using System;
using Core.Interface;
using Level.Data;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions.Mesh
{
    
    public class InitMaterialToMeshData : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, NodeConnectorList(IO.Input, typeof(VarReference<MaterialToMeshData>))]
        internal VarReference<MaterialToMeshData> m_meshData;
        
        [SerializeField, NodeConnectorList(IO.Input, typeof(VarReference<MeshData>))]
        internal VarReference<MeshData> m_colliderMeshData;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(InitMaterialToMeshData);
        public static Type StaticFactoryType => typeof(InitMaterialToMeshDataAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() => 
            new InitMaterialToMeshDataAction(m_meshData.Init(nameof(m_meshData), Node),
                m_colliderMeshData.Init(nameof(m_colliderMeshData), Node));
    }

    public class InitMaterialToMeshDataAction : IDefaultAction
    {
        readonly IVar<MaterialToMeshData> m_meshData;
        readonly IVar<MeshData> m_colliderMeshData;

        public InitMaterialToMeshDataAction(IVar<MaterialToMeshData> meshData, IVar<MeshData> colliderMeshData)
        {
            m_meshData = meshData;
            m_colliderMeshData = colliderMeshData;
        }

        public void Invoke()
        {
            m_meshData.Value = MaterialToMeshData.Default;
            m_colliderMeshData.Value = MeshData.Default();
        }
    }

}
