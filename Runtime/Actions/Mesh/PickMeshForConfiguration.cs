﻿using System;
using Core.Extensions;
using Core.Interface;
using Level.Data;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions.Mesh
{
    public class PickMeshForConfiguration : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input]
        internal VarReference<int> m_configuration;
        [SerializeField, Input]
        internal VarReference<MeshSet> m_meshSet;

        // todo: restrict to MeshConfigs
        [SerializeField, Input]
        internal VarReference<TileMeshConfig> m_meshConfig;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(PickMeshForConfiguration);
        public static Type StaticFactoryType => typeof(PickMeshForConfigurationAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction()
        {
            return new PickMeshForConfigurationAction(m_configuration.Init(nameof(m_configuration), Node), 
                m_meshSet.Init(nameof(m_meshSet), Node), 
                m_meshConfig);
        }
    }

    public class PickMeshForConfigurationAction : IDefaultAction
    {
        readonly IVar<int> m_configuration;

        readonly IVar<MeshSet> m_meshSet;
        readonly IVar<TileMeshConfig> m_meshConfig;

        public PickMeshForConfigurationAction(
            IVar<int> config,
            IVar<MeshSet> meshSet,
            IVar<TileMeshConfig> meshConfig)
        {
            m_configuration = config;
            m_meshSet = meshSet;
            m_meshConfig = meshConfig;
        }

        public void Invoke()
        {
            var meshConfig = m_meshConfig.Value;
            if (meshConfig == null)
                return;
            var meshes = meshConfig.Get(m_configuration.Value);
            m_meshSet.Value = meshes.MeshVariations.IsNullOrEmpty() 
                ? default 
                : meshes.MeshVariations.RandomItem();
        }
    }
}
