﻿using System;
using Core.Interface;
using Level.Data;
using Level.Room;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;
using static Level.Room.RoomVisualDataOperations;

namespace Level.Actions.Mesh
{
    public class ApplyCeiling : ScriptableBaseAction
    {        
        [SerializeField, Input] internal VarReference<IRoom> m_room;
        [SerializeField, Input] internal VarReference<Vector3> m_currentPosition;

        [SerializeField, Input] internal VarReference<MaterialToMeshData> m_materialToMeshData;

        public override string Name => nameof(ApplyCeiling);
        public static Type StaticFactoryType => typeof(ApplyCeilingAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() =>
            new ApplyCeilingAction(m_materialToMeshData.Init(nameof(m_materialToMeshData), Node),
                m_room.Init(nameof(m_room), Node),
                m_currentPosition.Init(nameof(m_currentPosition), Node));
    }

    public class ApplyCeilingAction : IDefaultAction
    {
        readonly IVar<MaterialToMeshData> m_materialMeshData;
        readonly IVar<IRoom> m_room;
        readonly IVar<Vector3> m_pos;

        public ApplyCeilingAction(IVar<MaterialToMeshData> matToMeshData, 
            IVar<IRoom> room,
            IVar<Vector3> position)
        {
            m_materialMeshData = matToMeshData;
            m_room = room;
            m_pos = position;
        }

        public void Invoke()
        {
            var room = m_room.Value;
            if (!(room is RoomInstanceComponent ric))
                return;
            var lvl =  m_pos.Value.y;
            var lvlIdx = (int) lvl;

            var matMeshData = m_materialMeshData.Value.MaterialMeshData;
            var ceilingData = ric.Data.VisualData.GetCeiling(lvlIdx);
            var ceilingMat = ric.RoomSettings.CeilingMaterial;

            if (!matMeshData.ContainsKey(ceilingMat))
            {
                //Debug.Log($"Clear {lvlIdx}");
                ClearCeilingMesh(ceilingData);
                return;
            }   
            var ceilingMesh = matMeshData[ceilingMat];
            ApplyCeilingMesh(ceilingData, ceilingMesh);
            matMeshData.Remove(ceilingMat);
        }
    }

}
