﻿using System;
using Core.Interface;
using Level.Data;
using Level.Room;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions.Mesh
{
    public class ApplyMaterialToMeshData : ScriptableBaseAction
    {        
        public VarReference<MaterialToMeshData> MaterialToMeshData;
        public VarReference<MeshData> ColliderMeshData;

        public VarReference<PlatformInstanceData> PlatformInstance;

        public override string Name => nameof(ApplyMaterialToMeshData);
        public static Type StaticFactoryType => typeof(ApplyMaterialToMeshDataAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() =>
            new ApplyMaterialToMeshDataAction(MaterialToMeshData.Init(nameof(MaterialToMeshData), Node), 
                ColliderMeshData.Init(nameof(ColliderMeshData), Node),
                PlatformInstance.Init(nameof(PlatformInstance), Node));
    }

    public class ApplyMaterialToMeshDataAction : IDefaultAction
    {
        readonly IVar<MaterialToMeshData> m_materialMeshData;
        readonly IVar<MeshData> m_colliderMeshData;
        readonly IVar<PlatformInstanceData> m_platformInstance;

        public ApplyMaterialToMeshDataAction(IVar<MaterialToMeshData> matToMeshData,
            IVar<MeshData> colliderMeshData,
            IVar<PlatformInstanceData> platformInstance)
        {
            m_materialMeshData = matToMeshData;
            m_colliderMeshData = colliderMeshData;
            m_platformInstance = platformInstance;
        }

        public void Invoke()
        {
            var platformInstance = m_platformInstance.Value;
            platformInstance.ApplyMaterialToMeshData(m_materialMeshData.Value);
            platformInstance.ApplyCollider(m_colliderMeshData.Value);

            //CombineMeshesForCollider(ref platformInstance);
            UpdateBounds(ref platformInstance);
            m_platformInstance.Value = platformInstance;
        }

        static void UpdateBounds(ref PlatformInstanceData platformInstance)
        {
            var platformGo = platformInstance.PlatformGo;
            if (platformGo == null)
                return;

            var platformTr = platformGo.transform;
            // todo: ugly hotfix for updating bounds of object
            var sibIdx = platformTr.GetSiblingIndex();
            var p = platformTr.transform.parent;
            platformTr.transform.SetParent(null);
            platformTr.transform.SetParent(p);
            platformTr.SetSiblingIndex(sibIdx);
            
            //foreach (var child in platformTr.Children())
            //{
            //    var sibIdx = child.GetSiblingIndex();
            //    var p = child.transform.parent;
            //    child.transform.SetParent(null);
            //    child.transform.SetParent(p);
            //    child.SetSiblingIndex(sibIdx);
            //}
        }

        // todo: put to action, probably we don't want to use complex custom mesh as collider but the simple one, maybe even reduced triangles
        static void CombineMeshesForCollider(ref PlatformInstanceData platformInstance)
        {
            Debug.Assert(platformInstance.Collider!= null 
                         && platformInstance.Collider.sharedMesh != null);

            var combine = new CombineInstance[platformInstance.Data.Count];
            var i = 0;
            foreach (var v in platformInstance.Data.Values)
            {
                Debug.Assert(v.Go != null);
                combine[i].mesh = v.Mesh;
                combine[i].transform = v.Go.transform.localToWorldMatrix * platformInstance.PlatformTr.worldToLocalMatrix;
                ++i;
            }

            platformInstance.Collider.sharedMesh.Clear();
            platformInstance.Collider.sharedMesh.CombineMeshes(combine);
        }
    }

}
