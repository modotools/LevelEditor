﻿using System;
using Core.Interface;
using Level.Data;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions.Mesh
{
    // todo make general, use set Object
    public class SelectMeshConfig : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input]
        internal TileMeshConfig m_meshConfig;
        [SerializeField, Input]
        internal VarReference<TileMeshConfig> m_meshConfigVar;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => m_meshConfig == null 
            ? $"{nameof(SelectMeshConfig)}" 
            : $"Select {m_meshConfig.name}";

        public static Type StaticFactoryType => typeof(SelectMeshConfigAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() 
            => new SelectMeshConfigAction(m_meshConfig, m_meshConfigVar);
    }

    public class SelectMeshConfigAction : IDefaultAction
    {
        readonly TileMeshConfig m_meshConfig;
        readonly IVar<TileMeshConfig> m_meshConfigVar;

        public SelectMeshConfigAction(TileMeshConfig meshConfig,
            IVar<TileMeshConfig> meshConfigVar)
        {
            m_meshConfig = meshConfig;
            m_meshConfigVar = meshConfigVar;
        }

        public void Invoke() => m_meshConfigVar.Value = m_meshConfig;
    }
}
