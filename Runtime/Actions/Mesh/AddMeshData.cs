﻿using System;
using Core.Extensions;
using Core.Interface;
using Level.Data;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions.Mesh
{
    public class AddMeshData : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        //[SerializeField, Input] internal VarReference<GridData> m_grid;
        [SerializeField, Input]
        internal VarReference<Vector3> m_currentPosition;
        [SerializeField, Input]
        internal VarReference<MeshData> m_meshData;
        [SerializeField, Input]
        internal VarReference<UnityEngine.Mesh> m_meshVar;
        [SerializeField, Input] 
        internal VarReference<Vector3> m_meshOffset;
        [SerializeField, Input] 
        internal VarReference<Vector3> m_orientation;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(AddMeshData);
        public static Type StaticFactoryType => typeof(AddMeshDataAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            return new AddMeshDataAction(
                m_currentPosition.Init(nameof(m_currentPosition), Node),
                m_meshData.Init(nameof(m_meshData), Node), 
                m_meshVar.Init(nameof(m_meshVar), Node),
                m_meshOffset.Init(nameof(m_meshOffset), Node), 
                m_orientation.Init(nameof(m_orientation), Node));
        }
    }

    public class AddMeshDataAction : IDefaultAction
    {
        readonly IVar<Vector3> m_currentPos;
        readonly IVar<MeshData> m_meshData;
        readonly IVar<UnityEngine.Mesh> m_mesh;
        readonly IVar<Vector3> m_meshOffset;
        readonly IVar<Vector3> m_orientation;

        public AddMeshDataAction(IVar<Vector3> currentPos,
            IVar<MeshData> mdr,
            IVar<UnityEngine.Mesh> mesh, 
            IVar<Vector3> meshOffset,
            IVar<Vector3> orientation)
        {
            m_currentPos = currentPos;

            m_mesh = mesh;
            m_meshData = mdr;
            m_meshOffset = meshOffset;
            m_orientation = orientation;
        }

        public void Invoke() => AddToMesh();

        void AddToMesh()
        {
            var pos = m_currentPos.Value;

            var md = m_meshData.Value;
            var verts = md.Vertices;
            var tris = md.Triangles;
            var uvs = md.UVs;
            var uvs2 = md.UVs2;
            var uvs3 = md.UVs3;

            var mesh = m_mesh.Value;
            if (verts == null)
                return;
            if (mesh == null)
            {
                Debug.LogWarning($"{nameof(AddMeshDataAction)}: Mesh is null!"); 
                return;
            }
            var startCount = verts.Count;

            var orientation = Quaternion.Euler(m_orientation.Value);
            foreach (var v in mesh.vertices)
            {
                var rotatedV = orientation * v;
                verts.Add(rotatedV + pos + m_meshOffset.Value);
            }   
            foreach (var t in mesh.triangles) 
                tris.Add(t + startCount);

            uvs.AddRange(mesh.uv);
            if (!mesh.uv2.IsNullOrEmpty())
                uvs2.AddRange(mesh.uv2);

            if (!mesh.uv3.IsNullOrEmpty())
                uvs3.AddRange(mesh.uv3);
        }
    }
}
