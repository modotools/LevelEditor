﻿using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.Interface;
using Level.Data;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions.Mesh
{
    public class MeshSetIterator : ScriptableBaseAction, ILinkActionConfigs
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input]
        internal VarReference<MeshSet> m_meshSet;
        [SerializeField, Input]
        internal VarReference<UnityEngine.Mesh> m_mesh;
        [SerializeField, NodeConnector(IO.Output, typeof(ScriptableActionConnector))] 
        internal  ScriptableBaseAction m_continueWith;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(MeshSetIterator);
        public static Type StaticFactoryType => typeof(MeshSetIteratorAction);
        public override Type FactoryType => StaticFactoryType;

#if UNITY_EDITOR
        public static string Editor_ContinueWithPropName => nameof(m_continueWith);
#endif

        public override IBaseAction CreateAction() => new MeshSetIteratorAction(
            m_meshSet.Init(nameof(m_meshSet), Node),
            m_mesh.Init(nameof(m_mesh), Node));

        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (graphActions[this] is MeshSetIteratorAction action)
                action.LinkActions(graphActions[m_continueWith]);

        }

        public IEnumerable<ScriptableObject> Dependencies => new[] {m_continueWith};
        public IEnumerable<IActionConfig> GetActionConfigsLinked() => new[] {m_continueWith};
    }

    public class MeshSetIteratorAction : IDefaultAction
    {
        readonly IVar<MeshSet> m_meshSet;
        readonly IVar<UnityEngine.Mesh> m_mesh;
        IDefaultAction m_continue;

        public MeshSetIteratorAction(IVar<MeshSet> meshSet, IVar<UnityEngine.Mesh> mesh)
        {
            m_meshSet = meshSet;
            m_mesh = mesh;
        }

        public void Invoke()
        {
            var data = m_meshSet.Value;
            if (data.Meshes.IsNullOrEmpty())
                return;
            foreach (var m in data.Meshes)
            {
                m_mesh.Value = m;
                m_continue.Invoke();
            }
        }

        public void LinkActions(IBaseAction graphAction) => m_continue = (IDefaultAction) graphAction;
    }
}
