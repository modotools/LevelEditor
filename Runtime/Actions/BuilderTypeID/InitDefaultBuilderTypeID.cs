﻿using System;
using Core.Interface;
using Level.Data;
using Level.Tiles;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;
using UnityEngine.Serialization;

namespace Level.Actions
{
    /// <summary>
    /// Initializes the TileBuilderTagLists for the current tile. (availableTags and actual active tags)
    /// These tags are later used to decide which meshes to place
    /// </summary>
    public class InitDefaultBuilderTypeID : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<TileMeshMaterialUVProviderList> m_providers;
        [SerializeField, Input] internal VarReference<int> m_uvConfig;
        [SerializeField, Input] internal VarReference<bool> m_hard;
        [SerializeField, Input] internal VarReference<TileBuilderTagList> m_availableTags;
        [SerializeField, Input] internal VarReference<TileBuilderTagList> m_tagList;

        [FormerlySerializedAs("m_defaultBorderTypes")] 
        [SerializeField] DefaultBuilderTypeIDs m_defaultBuilderTypes; 
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(InitDefaultBuilderTypeID);
        public static Type StaticFactoryType => typeof(InitDefaultBuilderTypeIDAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            return new InitDefaultBuilderTypeIDAction(m_providers.Init(nameof(m_providers), Node),
                m_uvConfig.Init(nameof(m_uvConfig), Node),
                m_hard.Init(nameof(m_hard), Node),
                m_availableTags.Init(nameof(m_availableTags), Node),
                m_tagList.Init(nameof(m_tagList), Node),
                m_defaultBuilderTypes);
        }
    }

    public class InitDefaultBuilderTypeIDAction : IDefaultAction
    {
        readonly IVar<TileMeshMaterialUVProviderList> m_providers;
        readonly IVar<int> m_msCaseConfig;
        readonly IVar<bool> m_hard;
        readonly IVar<TileBuilderTagList> m_availableTags;
        readonly IVar<TileBuilderTagList> m_tagList;

        readonly DefaultBuilderTypeIDs m_defaultBuilderTypeIDs;

        public InitDefaultBuilderTypeIDAction(IVar<TileMeshMaterialUVProviderList> providers,
            IVar<int> msCaseConfig,
            IVar<bool> hard,
            IVar<TileBuilderTagList> available,
            IVar<TileBuilderTagList> tagList,
            DefaultBuilderTypeIDs defaultBuilderTypes)
        {
            m_providers = providers;
            m_msCaseConfig = msCaseConfig;
            m_hard = hard;
            m_availableTags = available;
            m_tagList = tagList;

            m_defaultBuilderTypeIDs = defaultBuilderTypes;
        }

        public void Invoke()
        {
            var selectTags = TileBuilderTagList.Default;
            var available = TileBuilderTagList.Default;

            foreach (var p in m_providers.Value.Providers)
            {
                p.GetAvailableBuilderTypeIDs(m_msCaseConfig.Value, m_hard.Value,
                available.BuilderTypeIDs);
            }
            
            m_availableTags.Value = available;

            var @case = m_msCaseConfig.Value;
            var isEdge = @case > 0 && @case < 15;

            var id = available.BuilderTypeIDs.Contains(m_defaultBuilderTypeIDs.Default) ? m_defaultBuilderTypeIDs.Default : null;
            if (isEdge && available.BuilderTypeIDs.Contains(m_defaultBuilderTypeIDs.Edge))
            {
                if (!m_hard.Value && available.BuilderTypeIDs.Contains(m_defaultBuilderTypeIDs.FlatCornerEdge))
                    id = m_defaultBuilderTypeIDs.FlatCornerEdge;
                else 
                    id = m_defaultBuilderTypeIDs.Edge;
            }

            if (id != null)
                selectTags.BuilderTypeIDs.Add(id);
            m_tagList.Value = selectTags;
        }
    }
}
