﻿using System;
using Core.Interface;
using Core.Unity.Extensions;
using Level.Data;
using Level.Tiles;
using Level.Enums;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;
using static Level.Tiles.TileProcessing;
using static Level.Tiles.TileTypeOperations;

namespace Level.Actions
{
    /// <summary>
    /// This action is used to tag edge-tiles when neighbors are of TileType empty,
    /// This is mostly the case when there is wall below,
    /// so f.e. later we can place top-wall-stones on floor tiles where there is wall below etc.
    /// </summary>
    public class AddBuilderTypeIDAroundPit : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<TilesSetListConfig> m_tilesSetListConfig;
        [SerializeField] internal GeometryIdentifierConfig m_geometryIdentifier;
        [SerializeField] internal BuilderTypeIDMap[] m_idMap;

        [SerializeField, Input] internal VarReference<GridData> m_gridData;
        [SerializeField, Input] internal VarReference<Vector3> m_currentPosition;

        [SerializeField, Input] internal VarReference<TileBuilderTagList> m_availableTags;
        [SerializeField, Input] internal VarReference<TileBuilderTagList> m_tagList;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(AddBuilderTypeIDAroundPit);
        public static Type StaticFactoryType => typeof(AddBuilderTypeIDAroundPitAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            var emptyIdentifier = m_geometryIdentifier.Get(GeometryType.Empty);
            return new AddBuilderTypeIDAroundPitAction(emptyIdentifier, m_idMap,
                m_tilesSetListConfig.Init(nameof(m_tilesSetListConfig), Node),
                m_gridData.Init(nameof(m_gridData), Node),
                m_currentPosition.Init(nameof(m_currentPosition), Node),
                m_availableTags.Init(nameof(m_availableTags), Node),
                m_tagList.Init(nameof(m_tagList), Node));
        }
    }

    public class AddBuilderTypeIDAroundPitAction : IDefaultAction
    {
        readonly IVar<TilesSetListConfig> m_tileSetListConfig;

        readonly IVar<GridData> m_grid;
        readonly IVar<Vector3> m_pos;
        readonly IVar<TileBuilderTagList> m_availableTags;
        readonly IVar<TileBuilderTagList> m_tagList;

        readonly TileTypeIdentifier m_emptyIdentifier;
        readonly BuilderTypeIDMap[] m_map;
        public AddBuilderTypeIDAroundPitAction(
            TileTypeIdentifier emptyIdentifier, BuilderTypeIDMap[] map,
            IVar<TilesSetListConfig> tileSet,
            IVar<GridData> grid,
            IVar<Vector3> pos,
            IVar<TileBuilderTagList> available,
            IVar<TileBuilderTagList> tagList)
        {
            m_tileSetListConfig = tileSet;
            m_grid = grid;
            m_pos = pos;
            m_availableTags = available;
            m_tagList = tagList;

            m_map = map;
            m_emptyIdentifier = emptyIdentifier;
        }

        public void Invoke()
        {
            var tileSet = m_tileSetListConfig.Value;
            var emptyFilter = GetFilter(tileSet, m_emptyIdentifier);

            var pos = m_pos.Value.Vector3Int();
            var grid = m_grid.Value;

            var hasEmpty = false;
            foreach (var offset in IterationOffset)
            {
                var offsetPos = pos + offset;
                var neighbor = grid[offsetPos.x, offsetPos.y, offsetPos.z];
                if (emptyFilter.IsTileActive(neighbor))
                    hasEmpty = true;
            }
            
            //Debug.Log("isWall: "+isWall);
            if (!hasEmpty)
                return;

            var idList = m_tagList.Value.BuilderTypeIDs;
            for (var i = 0; i < m_map.Length; i++)
            {
                var containsOriginal = idList.Contains(m_map[i].Original);
                var tagAvailable = m_availableTags.Value.BuilderTypeIDs.Contains(m_map[i].Mapped);
                //Debug.Log("containsOriginal: "+containsOriginal);
                if (containsOriginal && tagAvailable)
                    idList.Add(m_map[i].Mapped);
            }
        }
    }

    [Serializable]
    public struct BuilderTypeIDMap
    {
        public TileBuilderTypeID Original;
        public TileBuilderTypeID Mapped;
    }
}
