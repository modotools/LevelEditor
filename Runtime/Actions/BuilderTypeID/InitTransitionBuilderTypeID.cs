﻿using System;
using Core.Interface;
using Level.Data;
using Level.Tiles;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions
{
    /// <summary>
    /// Initializes the TileBuilderTagLists for the current tile. (availableTags and actual active tags)
    /// These tags are later used to decide which meshes to place
    /// </summary>
    public class InitTransitionBuilderTypeID : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<TileMeshMaterialUVProviderList> m_providers;
        [SerializeField, Input] internal VarReference<int> m_uvConfig;
        [SerializeField, Input] internal VarReference<TileBuilderTagList> m_availableTags;
        [SerializeField, Input] internal VarReference<TileBuilderTagList> m_tagList;

        [SerializeField] CaseToBuilderTypeIDConfig m_transitionBuilderTypes;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(InitTransitionBuilderTypeID);
        public static Type StaticFactoryType => typeof(InitTransitionBuilderTypeIDAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            return new InitTransitionBuilderTypeIDAction(m_providers.Init(nameof(m_providers), Node),
                m_uvConfig.Init(nameof(m_uvConfig), Node),
                m_availableTags.Init(nameof(m_availableTags), Node),
                m_tagList.Init(nameof(m_tagList), Node),
                m_transitionBuilderTypes);
        }
    }

    public class InitTransitionBuilderTypeIDAction : IDefaultAction
    {
        readonly IVar<TileMeshMaterialUVProviderList> m_providers;
        readonly IVar<int> m_msCaseConfig;
        readonly IVar<TileBuilderTagList> m_availableTags;
        readonly IVar<TileBuilderTagList> m_tagList;

        readonly CaseToBuilderTypeIDConfig m_transitionBuilderTypeIDs;

        public InitTransitionBuilderTypeIDAction(IVar<TileMeshMaterialUVProviderList> providers,
            IVar<int> msCaseConfig,
            IVar<TileBuilderTagList> available,
            IVar<TileBuilderTagList> tagList,
            CaseToBuilderTypeIDConfig transitionBuilderTypes)
        {
            m_providers = providers;
            m_msCaseConfig = msCaseConfig;
            m_availableTags = available;
            m_tagList = tagList;

            m_transitionBuilderTypeIDs = transitionBuilderTypes;
        }

        public void Invoke()
        {
            var selectTags = TileBuilderTagList.Default;
            var available = TileBuilderTagList.Default;

            foreach (var p in m_providers.Value.Providers)
                p.GetAvailableBuilderTypeIDs(m_msCaseConfig.Value, true, available.BuilderTypeIDs);
            m_availableTags.Value = available;

            var @case = m_msCaseConfig.Value;
            var id = m_transitionBuilderTypeIDs.Get(@case);
            if (id != null && available.BuilderTypeIDs.Contains(id))
                selectTags.BuilderTypeIDs.Add(id);
            m_tagList.Value = selectTags;
        }
    }
}
