﻿using System;
using System.Collections.Generic;
using Core.Interface;
using Level.Tiles;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions
{
    /// <summary>
    /// Iterates through all BuilderTypeIds and performs connected action
    /// </summary>
    public class IterateBuilderTypeIDs : ScriptableBaseAction, ILinkActionConfigs
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<TileBuilderTagList> m_tagList;
        [SerializeField, Input] internal VarReference<TileBuilderTypeID> m_id;

        [SerializeField, NodeConnector(IO.Output, typeof(ScriptableActionConnector))] 
        ScriptableBaseAction m_continue;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(IterateBuilderTypeIDs);
        public static Type StaticFactoryType => typeof(IterateBuilderTypeIDsAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            return new IterateBuilderTypeIDsAction(m_tagList.Init(nameof(m_tagList), Node),
                m_id.Init(nameof(m_id), Node));
        }

        public IEnumerable<ScriptableObject> Dependencies => new[] { m_continue };
        public IEnumerable<IActionConfig> GetActionConfigsLinked() => new[] { m_continue };

        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (!(graphActions[this] is IterateBuilderTypeIDsAction iterateBorderTypeIds)) 
                return;
            graphActions.TryGetValue(m_continue, out var continueAction);
            iterateBorderTypeIds.LinkActions(continueAction as IDefaultAction);
        }
    }

    public class IterateBuilderTypeIDsAction : IDefaultAction
    {
        readonly IVar<TileBuilderTagList> m_tagList;
        readonly IVar<TileBuilderTypeID> m_builderTypeID;

        IDefaultAction m_action;

        public IterateBuilderTypeIDsAction(IVar<TileBuilderTagList> tagList, 
            IVar<TileBuilderTypeID> builderTypeID)
        {
            m_builderTypeID = builderTypeID;
            m_tagList = tagList;
        }

        public void Invoke()
        {
            foreach (var id in m_tagList.Value.BuilderTypeIDs)
            {
                m_builderTypeID.Value = id;
                m_action.Invoke();
            }
        }

        public void LinkActions(IDefaultAction continueAction) => m_action = continueAction;
    }
}
