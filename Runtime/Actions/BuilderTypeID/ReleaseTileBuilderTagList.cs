﻿using System;
using Core.Interface;
using Level.Tiles;
using ScriptableUtility;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace Level.Actions
{
    /// <summary>
    /// Releases the TileBuilderTagLists
    /// </summary>
    public class ReleaseTileBuilderTagList : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField] internal VarReference<TileBuilderTagList>[] m_tagList;
#pragma warning restore 0649 // wrong warning for SerializeField

        public override string Name => nameof(ReleaseTileBuilderTagList);
        public static Type StaticFactoryType => typeof(ReleaseTileBuilderTagListAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            return new ReleaseTileBuilderTagListAction(m_tagList.Init(nameof(m_tagList), Node));
        }
    }

    public class ReleaseTileBuilderTagListAction : IDefaultAction
    {
        readonly IVar<TileBuilderTagList>[] m_tagList;

        public ReleaseTileBuilderTagListAction(IVar<TileBuilderTagList>[] tagList) => m_tagList = tagList;
        public void Invoke()
        {
            foreach (var tlVar in m_tagList)
            {
                var tl = tlVar.Value;
                TileBuilderTagList.Release(ref tl);
                tlVar.Value = tl;
            }
        }
    }
}
