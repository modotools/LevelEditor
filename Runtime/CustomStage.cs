using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LevelEditor
{
    public class CustomStage : PreviewSceneStage
    {
        public GUIContent TitleContent;
        
        protected override GUIContent CreateHeaderContent()
            => TitleContent;
        
        public void SetupStage(GameObject stageObject)
        {
            var light = new GameObject("Light");
            light.transform.eulerAngles = new Vector3(50,-30,0);
            light.AddComponent<Light>().type = LightType.Directional;
            
            stageObject.transform.SetParent(null);
            StageUtility.PlaceGameObjectInCurrentStage(light);
            StageUtility.PlaceGameObjectInCurrentStage(stageObject);
            Selection.activeObject = stageObject;
            if (SceneView.currentDrawingSceneView != null)
                SceneView.currentDrawingSceneView.FrameSelected();
        }
    }
}
