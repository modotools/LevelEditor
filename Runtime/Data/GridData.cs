﻿using UnityEngine;

namespace Level.Data
{
    public struct GridData
    {
        public ushort[,,] Data;

        public static implicit operator ushort[,,] (GridData g) => g.Data;
        public static implicit operator GridData(ushort[,,] data) => new GridData { Data = data };

        public int Width => Data.GetLength(0);
        public int Height => Data.GetLength(1);
        public int Depth => Data.GetLength(2);
        public ushort this[int x, int y, int z]
        {
            get => Data[x,y,z];
            set => Data[x,y,z] = value;
        }
        public ushort this[Vector3Int pos]
        {
            get => Data[pos.x, pos.y, pos.z];
            set => Data[pos.x, pos.y, pos.z] = value;
        }
    }
}
