using System;

namespace Level.Tiles
{
    [Serializable]
    public struct DefaultBuilderTypeIDs
    {
        public TileBuilderTypeID Default;
        public TileBuilderTypeID Edge;
        public TileBuilderTypeID FlatCornerEdge;
    }

}