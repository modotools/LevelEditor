using System;
using Core.Unity.Types.ID;

namespace Level.Tiles
{
    /// <summary>
    /// Previously known as TextureTypeID, because it is also used for setting UVMapping-layout.
    /// But texture type is to unspecific. Currently we have separate ids for each border-cases,
    /// like border with flattened corners, angular corners, transition with another LevelMaterialType,
    /// as well as Floor-no-border or Wall-no-border. I can also imagine StartOfWall, EndOfWall.
    /// So this can be used to identify UVMapping-layout as well as which mesh to pick.
    /// </summary>
    [Serializable]
    public class TileBuilderTypeID : IDAsset
    {
        public bool IsHard;
    }
}