
using System.Collections.Generic;
using Core.Types;

namespace Level.Tiles
{
    public struct TileBuilderTagList
    {
        public List<TileBuilderTypeID> BuilderTypeIDs { get; private set; }

        public static TileBuilderTagList Default => new TileBuilderTagList()
        {
            BuilderTypeIDs = SimplePool<List<TileBuilderTypeID>>.I.Get()
        };

        public static void Release(ref TileBuilderTagList tileBuilderTagList)
        {
            if (tileBuilderTagList.BuilderTypeIDs != null)
                SimplePool<List<TileBuilderTypeID>>.I.Return(tileBuilderTagList.BuilderTypeIDs);
            tileBuilderTagList.BuilderTypeIDs = null;
        }
    }
}