﻿using JetBrains.Annotations;

namespace Level.Tiles
{
    /// <summary>
    /// Used for finding specific ITilesData from a set on a combined tile
    /// </summary>
    public readonly struct TilesSetFilter
    {
        public TilesSetFilter(TilesSetData data, ushort filterIdx)
        {
            Data = data;
            FilterIdx = filterIdx;
        }

        public readonly TilesSetData Data;
        public readonly ushort FilterIdx;
        [Pure]
        public bool IsTileActive(uint combinedTile) => Data.GetTileIdx(combinedTile) == FilterIdx;

        [Pure]
        public ushort GetCombinedTile(uint tile) => Data.GetCombinedTile(tile, FilterIdx);
    }
}
