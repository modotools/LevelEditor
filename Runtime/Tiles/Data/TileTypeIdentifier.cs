﻿using System;
using Level.Tiles.Interface;
using UnityEngine.Serialization;

namespace Level.Tiles
{
    /// <summary>
    /// To identify a specific Tile in a TileConfig within a Set (TilesSetListConfig)
    /// </summary>
    [Serializable]
    public struct TileTypeIdentifier
    {
        [FormerlySerializedAs("Config")] 
        public RefITileConfig TileConfig;
        public ushort TileIdx;
    }
}
