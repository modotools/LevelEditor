﻿using System;
using Core.Unity.Attributes;

namespace Level.Tiles
{
    [Serializable]
    public class TileTypeIdentifierAttribute : MultiPropertyAttribute
    {
        public TileTypeIdentifierAttribute(bool drawConfig) => DrawConfig = drawConfig;
        public bool DrawConfig;
    }
}
