namespace Level.Data
{
    public struct TileMeshMaterialUVData
    {
        public MeshMaterialUVSet[] Variations;
    }
}