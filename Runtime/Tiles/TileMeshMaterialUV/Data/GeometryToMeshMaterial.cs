using System;
using Level.Data;

namespace Level.Tiles
{
    [Serializable]
    public struct GeometryToMeshMaterialData
    {
        [TileTypeIdentifier(false)]
        public TileTypeIdentifier GeometryId;
        public RefITileMeshMaterialProvider MeshMaterial;
    }
}