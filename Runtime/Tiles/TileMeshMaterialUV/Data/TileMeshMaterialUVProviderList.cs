using System.Collections.Generic;
using Core.Types;

namespace Level.Data
{
    public struct TileMeshMaterialUVProviderList
    {
        public List<ITileMeshMaterialUVProvider> Providers;

        public static TileMeshMaterialUVProviderList Default => new TileMeshMaterialUVProviderList()
        {
            Providers = SimplePool<List<ITileMeshMaterialUVProvider>>.I.Get()
        };

        public static void Release(ref TileMeshMaterialUVProviderList data)
        {
            if (data.Providers != null)
                SimplePool<List<ITileMeshMaterialUVProvider>>.I.Return(data.Providers);
            data.Providers = null;
        }
    }
}