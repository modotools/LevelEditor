﻿using System;
using System.Collections.Generic;
using Core.Types;
using UnityEngine;
using UnityEngine.Serialization;

namespace Level.Data
{
    [Serializable] 
    public struct MeshMaterialUVSet
    {
        [FormerlySerializedAs("MeshTex")] 
        public List<MeshMaterialUVData> MeshMaterialUVData;
        public List<Mesh> ColliderMeshes;

        public static MeshMaterialUVSet Default => new MeshMaterialUVSet()
        {
            MeshMaterialUVData = SimplePool<List<MeshMaterialUVData>>.I.Get(),
            ColliderMeshes = SimplePool<List<Mesh>>.I.Get()
        };

        public static void Release(ref MeshMaterialUVSet set)
        {
            if (set.MeshMaterialUVData != null)
                SimplePool<List<MeshMaterialUVData>>.I.Return(set.MeshMaterialUVData);
            set.MeshMaterialUVData = null;

            if (set.ColliderMeshes != null)
                SimplePool<List<Mesh>>.I.Return(set.ColliderMeshes);
        }
    }

    [Serializable]
    public struct MeshMaterialUVData
    {
        public Mesh Mesh;
        public Material Material;

        public Vector3 Offset;
        public Vector3 Rotation;

        public Vector2 UVStart;
        public Vector2 UVEnd;

        public Vector2 UV2Start;
        public Vector2 UV2End;

        public Vector2 UV3Start;
        public Vector2 UV3End;
    }
}
