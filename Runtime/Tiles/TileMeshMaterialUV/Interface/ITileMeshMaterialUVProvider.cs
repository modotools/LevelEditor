using System;
using System.Collections.Generic;
using Core.Unity.Types;
using Level.Tiles;
using UnityEngine;

namespace Level.Data
{
    public interface ITileMeshMaterialUVProvider
    {
        Texture2D PreviewTex { get; }
        void Get(ref MeshMaterialUVSet set, TileBuilderTypeID builderTypeID,
            Vector2Int tilePos,
            int ms_caseConfigMesh, int ms_caseConfigUV,
            bool isHard, int variation = -1);
        void GetAvailableBuilderTypeIDs(int ms_caseConfiguration, bool isHard, List<TileBuilderTypeID> borderTypeIds);

#if UNITY_EDITOR
        void Editor_CollectAllAtlasData(Editor_CollectAtlasData atlasToTexture);
#endif
    }

    [Serializable]
    public class RefITileMeshMaterialProvider : InterfaceContainer<ITileMeshMaterialUVProvider> { }
}