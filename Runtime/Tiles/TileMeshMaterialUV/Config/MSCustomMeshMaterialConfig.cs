using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Unity.Interface;
using Level.Data;
using UnityEngine;
using UnityEngine.Serialization;

namespace Level.Tiles
{
    /// <summary>
    /// MS = Marching Squares, 
    /// </summary>
    [CreateAssetMenu(fileName = nameof(MSCustomMeshMaterialConfig), menuName = "Level/Tiles/"+nameof(MSCustomMeshMaterialConfig))]
    public class MSCustomMeshMaterialConfig : ScriptableObject, IScriptableObject,
        ITileMeshMaterialUVProvider
    {
        [SerializeField] bool m_autoRotateCases;

        [FormerlySerializedAs("m_configuration")] [SerializeField] 
        MS_CustomMesh_CaseData[] m_caseData = new MS_CustomMesh_CaseData[16];

        [SerializeField] TileMeshConfig m_colliderMesh;
        [SerializeField] TileMeshConfig m_colliderMeshHard;

        //[SerializeField] TileMeshMaterialUVData[] m_targetData;
        //[SerializeField] TileMeshMaterialUVData[] m_hardCornerTargetData;

        [SerializeField] Texture2D m_assetPreview;

        public Texture2D PreviewTex => m_assetPreview;

        public void Get(ref MeshMaterialUVSet set, TileBuilderTypeID builderTypeID, 
            Vector2Int tilePos,
            int ms_caseConfigMesh, int ms_caseConfigUV, 
            bool isHard, int variation = -1)
        {
            // todo
            var @case = m_caseData[ms_caseConfigUV];

            var dataVariations = @case.MeshData
                .FirstOrDefault(a=>a.BuilderTypeID == builderTypeID).VariationSets;

            if (dataVariations.IsNullOrEmpty())
                return;
            if (!variation.IsInRange(dataVariations))
                variation %= dataVariations.Length; // UnityEngine.Random.Range(0, dataVariations.Length);

            var dataSet = dataVariations[variation];
            foreach (var data in dataSet.Data)
            {
                var meshMatUVData = new MeshMaterialUVData()
                {
                    Mesh = data.Mesh,
                    Material = data.PreviewMaterial,
                    Offset = data.Offset,
                    Rotation = data.Rotation,
                    UVStart = Vector2.zero,
                    UVEnd = Vector2.one
                };
                set.MeshMaterialUVData.Add(meshMatUVData);
            }

            var colliderMesh = GetColliderData(isHard, ms_caseConfigMesh);
            if (colliderMesh != null)
                set.ColliderMeshes.Add(colliderMesh);
        }

        public void GetAvailableBuilderTypeIDs(int ms_caseConfiguration, bool isHard, List<TileBuilderTypeID> borderTypeIds)
        {
            Debug.Assert(ms_caseConfiguration.IsInRange(m_caseData));
            borderTypeIds.AddRange(m_caseData[ms_caseConfiguration]
                .MeshData.Select(a => a.BuilderTypeID));
        }
        Mesh GetColliderData(bool isHard, int @case)
        {
            var conf = isHard ? 
                (m_colliderMeshHard != null ? m_colliderMeshHard.Get(@case) : default)
                : (m_colliderMesh != null ? m_colliderMesh.Get(@case) : default);

            if (conf.MeshVariations.IsNullOrEmpty())
                return null;
            return conf.MeshVariations[0].Meshes.IsNullOrEmpty() 
                ? null 
                : conf.MeshVariations[0].Meshes[0];
        }
#if UNITY_EDITOR
        public void Editor_CollectAllAtlasData(Editor_CollectAtlasData atlasToTexture)
        {
            // todo: atlas
            return;
        }

        public const string Editor_ConfigDataPropName = nameof(m_caseData);
        //public const string Editor_TargetDataPropName = nameof(m_targetData);
        //public const string Editor_HardCornerTargetDataPropName = nameof(m_hardCornerTargetData);

        public bool Editor_AutoRotateCases => m_autoRotateCases;
        public MS_CustomMesh_CaseData[] Editor_GetCaseData() => m_caseData;
        public ref MS_CustomMesh_CaseData Editor_GetSourceData(int i) => ref m_caseData[i];
        public Mesh Editor_GetColliderData(bool isHard, int @case) => GetColliderData(isHard, @case);
#endif
    }

    [Serializable]
    public struct MS_CustomMesh_CaseData
    {
        public BuilderTypeID_To_CustomMeshMaterialVariations[] MeshData;
    }

    [Serializable]
    public struct BuilderTypeID_To_CustomMeshMaterialVariations
    {
        [FormerlySerializedAs("BorderTypeID")] 
        public TileBuilderTypeID BuilderTypeID;

        //public CustomMeshMaterialData[] Variations;
        public CustomMeshMaterialDataSet[] VariationSets;
    }

    [Serializable]
    public struct CustomMeshMaterialDataSet
    {
        public CustomMeshMaterialData[] Data;
    }

    [Serializable]
    public struct CustomMeshMaterialData
    {
        public Mesh Mesh;

        public Vector3 Offset;
        public Vector3 Rotation;

        // with this material you are supposed to be able to preview the mesh,
        // textures used for the mesh will be extracted from here and put into an atlas
        public Material PreviewMaterial;
        // this material will be used for the level, all textures from meshes using this
        // will be combined in atlas generation and set there
        public Material FinalMaterial;
    }
}
