﻿using System;
using Core.Unity.Types;
using UnityEngine;

namespace Level.Tiles.Interface
{
    public interface ITileType
    {
        string TileName { get; }
        Color TileColor { get; }
        Texture2D PreviewTex { get; }
    }

    [Serializable]
    public class RefITileType: InterfaceContainer<ITileType>{}
}
