﻿using System;
using Core.Unity.Types;
using Level.PlatformLayer.Interface;

namespace Level.Tiles.Interface
{
    public interface ITileConfig
    {
        string Name { get; }
        ushort ReserveBits { get; }

        int Count { get; }
        ITileType this[int idx] { get; }
    }

    public interface IDimensionLayoutOverride
    {
        GridSettings.DimensionLayout Layout { get; }
    }

    [Serializable]
    public class RefITileConfig: InterfaceContainer<ITileConfig>{}
}
