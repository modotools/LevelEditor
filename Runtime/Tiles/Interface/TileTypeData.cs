﻿using System;
using UnityEngine;

namespace Level.Tiles.Interface
{
    [Serializable]
    public struct TileTypeData : ITileType
    {
        public TileTypeData(string name)
        {
            TileName = name;
            TileColor = Color.white;
            PreviewTex = null;
        }

        public TileTypeData(string name, Color color, Texture2D previewTexture = null)
        {
            TileName = name;
            TileColor = color;
            PreviewTex = previewTexture;
        }

        public string TileName { get; }
        public Color TileColor { get; }
        public Texture2D PreviewTex { get; }
    }
}
