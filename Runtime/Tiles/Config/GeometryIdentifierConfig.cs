using System;
using Core.Extensions;
using Core.Unity.Interface;
using Level.Tiles.Interface;
using Level.Enums;
using UnityEngine;

namespace Level.Tiles
{
    /// <summary>
    /// Config for mapping TileType (ITileConfig->ITilesData) to GeometryType-enum
    /// We map the generic ITileConfig, ITilesData to specific Geometry-Data like Floor, Wall once in this config.
    /// This way we can use geometry tiles with generic actions that deal only with ITileConfig, ITilesData
    /// and with other actions that deal with specific Geometry-Data, that needs to be identified,
    /// without needing to identify it in each action. We just link this config there.
    /// </summary>
    [CreateAssetMenu(fileName = nameof(GeometryIdentifierConfig), menuName = "Level/Tiles/" + nameof(GeometryIdentifierConfig))]
    public class GeometryIdentifierConfig : ScriptableObject, IVerify
    {
        [SerializeField] RefITileConfig m_tileConfig;

        // todo: better inspector with GeometryType enum, auto set size and tileConfig there
        [SerializeField] TileTypeIdentifier[] m_geoIdentifiers;

        public ITileConfig TileConfig => m_tileConfig.Result;
        public TileTypeIdentifier Get(GeometryType geo)
        {
            var idx = (int) geo;
            return idx.IsInRange(m_geoIdentifiers) 
                ? m_geoIdentifiers[(int) geo] 
                : default;
        }

#if UNITY_EDITOR
        public void Editor_Verify(ref VerificationResult result)
        {
            var config = m_tileConfig?.Result;
            if (config == null)
            {
                result.Error($"TileConfig not set", this);
                return;
            }

            var expectedSize = Enum.GetNames(typeof(GeometryType)).Length;
            if (m_geoIdentifiers.Length != Enum.GetNames(typeof(GeometryType)).Length)
                result.Error($"Identifiers Array should have size of {expectedSize}", this);

            for (var i = 0; i < m_geoIdentifiers.Length; i++)
            {
                var gId = m_geoIdentifiers[i];
                var geoConf = gId.TileConfig?.Result;

                if (geoConf == null || config != geoConf)
                    result.Error($"{(GeometryType)i} ({i}) with wrong TileConfig", this);
            }
        }
#endif
    }
}
