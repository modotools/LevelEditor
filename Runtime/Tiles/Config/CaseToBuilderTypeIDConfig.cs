using Core.Extensions;
using Core.Unity.Interface;
using UnityEngine;

namespace Level.Tiles
{
    [CreateAssetMenu(fileName = nameof(CaseToBuilderTypeIDConfig),
        menuName = "Level/Tiles/" + nameof(CaseToBuilderTypeIDConfig))]
    public class CaseToBuilderTypeIDConfig : ScriptableObject, IVerify
    {
        [SerializeField] TileBuilderTypeID[] m_builderTypeID = new TileBuilderTypeID[16];

        public TileBuilderTypeID Get(int ms_caseConfiguration) => m_builderTypeID[ms_caseConfiguration];

#if UNITY_EDITOR
        public static string Editor_TileBuilderTypeIDs_PropName => nameof(m_builderTypeID);
        public void Editor_Verify(ref VerificationResult result)
        {
            if (m_builderTypeID.IsNullOrEmpty())
                result.Error("m_builderTypeID IsNullOrEmpty", this);
            else if (m_builderTypeID.Length != 16)
                result.Error("m_builderTypeID Length should be 16", this);
        }
#endif
    }
}