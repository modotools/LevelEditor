﻿using System;
using System.Collections.Generic;
using Level.Tiles.Interface;
using UnityEngine;
using UnityEngine.Serialization;

namespace Level.Tiles
{
    [CreateAssetMenu(menuName = "Level/Tiles/"+nameof(TileConfig))]
    public class TileConfig : ScriptableObject, ITileConfig
    {
        const int k_defaultPreviewTexSize = 16;

#pragma warning disable 0649 // wrong warning for SerializeField
        [Serializable]
        public class TileType : ITileType
        {
            [FormerlySerializedAs("TileName"), SerializeField]
            string m_tileName;
            [FormerlySerializedAs("TileColor"), SerializeField]
            Color m_tileColor;

            public string TileName
            {
                get => m_tileName;
                set => m_tileName = value;
            }
            public Color TileColor
            {
                get => m_tileColor;
                set => m_tileColor = value;
            }
            public Texture2D PreviewTex 
            {
                get
                {
                    if (m_previewTex != null)
                        return m_previewTex;

                    m_previewTex = new Texture2D(k_defaultPreviewTexSize, k_defaultPreviewTexSize);

                    for (var x = 0; x < k_defaultPreviewTexSize; x++)
                    for (var y = 0; y < k_defaultPreviewTexSize; y++)
                        m_previewTex.SetPixel(x, y, TileColor);
                    m_previewTex.Apply();
                    return m_previewTex;
                }
            }

            Texture2D m_previewTex;
        }

        // todo Tiles: limit maximum TileTypes
        [FormerlySerializedAs("TileTypes"), SerializeField] 
        List<TileType> m_tileTypes;

        [FormerlySerializedAs("ReserveBits"), SerializeField] 
        ushort m_reserveBits;
#pragma warning restore 0649 // wrong warning for SerializeField

        public string Name => name;
        public ushort ReserveBits => m_reserveBits;

        public int Count => m_tileTypes.Count;
        public ITileType this[int idx] => m_tileTypes[idx];
    }
}
