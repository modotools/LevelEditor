﻿using System.Linq;
using Core.Extensions;
using Level.Data;
using UnityEngine;

namespace Level.Tiles
{
    public static class TileProcessing
    {
        public enum CornerIdx
        {
            BottomLeft,
            BottomRight,
            TopRight,
            TopLeft
        }
        public enum CaseValue
        {
            None = 0,

            BottomLeft = 1,
            BottomRight = 2,
            TopRight = 4,
            TopLeft = 8,

            Bottom = BottomLeft | BottomRight, //3
            Top = TopLeft | TopRight, // 12
            Left = BottomLeft | TopLeft, // 9
            Right = TopRight | BottomRight, // 6

            NotBottomLeft = BottomRight | TopRight | TopLeft, // 14
            NotBottomRight = BottomLeft | TopRight | TopLeft, // 13
            NotTopRight = BottomLeft | BottomRight | TopLeft, // 11
            NotTopLeft = BottomLeft | BottomRight | TopRight, // 7

            All = BottomLeft | BottomRight | TopRight | TopLeft // 15
        }

        // Iterating over a grid counter-clockwise like this
        // |3|2|
        // |0|1|
        public static Vector3Int[] IterationOffset { get; } = { Vector3Int.zero, 
            new Vector3Int(1, 0, 0), new Vector3Int(1, 0, 1), new Vector3Int(0, 0, 1) };

        public struct ConfigurationResult
        {
            public int Configuration;
            public int ActiveNodes;
            public bool[] Active;

            public bool IsActive(CornerIdx c) => Active[(int) c];
        }

        public static ConfigurationResult GetConfiguration(TilesSetFilter primary,
            TilesSetFilter secondary, ushort[,,] tiles, Vector3Int pos)
        {
            ConfigurationResult result;
            // todo: try without new bool[]
            result.Active = new bool[4];

            result.Configuration = 0;
            result.ActiveNodes = 0;

            ushort posVal = 1;

            for (var i = 0; i < IterationOffset.Length; ++i, posVal *= 2)
            {
                var off = IterationOffset[i];
                var offsetPos = pos + off;
                var tile = tiles[offsetPos.x, offsetPos.y, offsetPos.z];
                result.Active[i] = primary.IsTileActive(tile);

                if (secondary.Data.TileConfig != null)
                    result.Active[i] &= secondary.IsTileActive(tile);

                if (!result.Active[i])
                    continue;

                result.Configuration += posVal;
                result.ActiveNodes++;
            }

            return result;
        }

        public static ushort NextType(TilesSetData iterationSet, TilesSetFilter[] filters, 
            ref GridData grid, Vector3Int pos,
            ref int startNode, ref ushort[] texTypeDone, ref int texTypesDoneIdx)
        {
            for (; startNode < IterationOffset.Length; ++startNode)
            {
                var off = IterationOffset[startNode];
                var offsetPos = pos + off;
                var tile = grid[offsetPos];

                var ignoreTile = filters.Any(filter => !filter.IsTileActive(tile));
                if (ignoreTile)
                    continue;
                //Debug.Log($"{iterationSet.Mask} {iterationSet.Shift} {tile}");
                var type = iterationSet.GetTileIdx(tile);

                if (texTypeDone.Any(t => t == type))
                    continue;

                if (!texTypesDoneIdx.IsInRange(texTypeDone))
                {
                    Debug.LogError($"Not in Range texTypeDone.Length {texTypeDone.Length} texTypesDoneIdx {texTypesDoneIdx} for tile {type}\n" +
                                   $"DONE: {texTypeDone[0]} DONE: {texTypeDone[1]} DONE: {texTypeDone[2]}");
                    continue;
                }
                texTypeDone[texTypesDoneIdx] = type;
                texTypesDoneIdx++;

                return type;
            }
            return ushort.MaxValue;
        }
    }
}
