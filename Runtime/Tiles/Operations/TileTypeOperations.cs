﻿using Level.Data;
using Level.Tiles.Interface;
using UnityEngine;

namespace Level.Tiles
{
    public static class TileTypeOperations
    {
        public static ITileType TilesData(this TileTypeIdentifier identifier)
        {
            if (identifier.TileConfig.Result == null)
                return null;
            return identifier.TileConfig.Result.Count > identifier.TileIdx 
                ? identifier.TileConfig.Result[identifier.TileIdx] 
                : null;
        }

        public static TilesSetFilter GetFilter(TilesSetListConfig setListConfig, TileTypeIdentifier identifier)
        {
            var set = setListConfig.GetSet(identifier.TileConfig.Result);
            return new TilesSetFilter(set, identifier.TileIdx);
        }
        public static TilesSetFilter GetFilter(TilesSetData setData, TileTypeIdentifier identifier) 
            => new TilesSetFilter(setData, identifier.TileIdx);

        public static void Combine(ref GridData grid, Vector3Int pos, TilesSetFilter identifier) 
            => grid[pos] = identifier.GetCombinedTile(grid[pos]);
        
        public static void Combine(ref GridData grid, Vector3Int pos, TilesSetData setData, TileTypeIdentifier identifier) 
            => grid[pos] = setData.GetCombinedTile(grid[pos], identifier.TileIdx);
    }
}
