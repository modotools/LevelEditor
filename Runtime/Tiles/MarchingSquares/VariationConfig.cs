using Level.PlatformLayer.Interface;
using Level.Tiles.Interface;
using UnityEngine;
using static Level.GridSettings.DimensionLayout;

namespace Level.Tiles.MarchingSquares
{
    [CreateAssetMenu(menuName = "Level/Tiles/VariationConfig")]
    public class VariationConfig : ScriptableObject, ITileConfig, IDimensionLayoutOverride
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField] string m_name;
        [SerializeField] int m_maxVariations;
#pragma warning restore 0649 // wrong warning for SerializeField

        public string Name => m_name;
        public ushort ReserveBits => 0;
        public int Count => m_maxVariations;

        public ITileType this[int idx] =>
            idx == 0 ? new TileTypeData("?")
                : new TileTypeData($"{idx}");

        
        #region IDimensionLayoutOverride
        public GridSettings.DimensionLayout Layout => GridSettings.DimensionLayout.Tiles;
        #endregion

    }
}