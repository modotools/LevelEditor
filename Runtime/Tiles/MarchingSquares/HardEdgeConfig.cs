using System;
using Level.Tiles.Interface;
using UnityEngine;

namespace Level.Tiles.MarchingSquares
{
    [CreateAssetMenu(menuName = "Level/Tiles/HardEdgeConfig")]
    public class HardEdgeConfig : ScriptableObject, ITileConfig, IDimensionLayoutOverride
    {
        public string Name => "HardEdge";
        public ushort ReserveBits => 2;
        public int Count => 3;
        public ITileType this[int idx] => new TileTypeData(((HardEdgeState)idx).ToString());
        public GridSettings.DimensionLayout Layout => GridSettings.DimensionLayout.Tiles;
    }

    public enum HardEdgeState
    {
        Default = 0,
        Hard = 1,
        Break = 2,
        BreakAndHard = 3
    }
}