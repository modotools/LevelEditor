using Level.Tiles.Interface;
using UnityEngine;

namespace Level.Tiles.MarchingSquares
{
    [CreateAssetMenu(menuName = "Level/Tiles/CountNodesConfig")]
    public class CountNodesConfig : ScriptableObject, ITileConfig
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField] string m_name;
#pragma warning restore 0649 // wrong warning for SerializeField

        public string Name => m_name;
        public ushort ReserveBits => 4;
        public int Count => 5;

        // todo: don't make me implement editor shit
        public ITileType this[int idx] => null;
    }
}