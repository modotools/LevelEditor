using System;
using System.Collections.Generic;
using System.Linq;
using Level.Data;
using Level.Tiles.Interface;
using UnityEngine;
using UnityEngine.Serialization;

namespace Level.Tiles
{
    [Serializable]
    public class LevelMaterialTransition : ILevelMaterial
    {
        public string NameA;
        public string NameB;
        public ushort IndexA;
        public ushort IndexB;

        [SerializeField, FormerlySerializedAs("MeshMaterials")] 
        GeometryToMeshMaterialData[] m_meshMaterials = new GeometryToMeshMaterialData[0];

        public void GetProvider(ITileType geoTileConfig, List<ITileMeshMaterialUVProvider> provider) => 
            provider.AddRange(m_meshMaterials.Where(mm => mm.GeometryId.TilesData() == geoTileConfig)
                .Select(mm=>mm.MeshMaterial?.Result));

        public GeometryToMeshMaterialData[] MeshMaterials => m_meshMaterials;

#if UNITY_EDITOR
        public ref GeometryToMeshMaterialData[] Editor_MeshMaterials => ref m_meshMaterials;
        public string Editor_MeshMaterialsPropName => nameof(m_meshMaterials);
#endif
    }
}