using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Level.Data;
using Level.Tiles.Interface;
using UnityEngine;
using UnityEngine.Serialization;

namespace Level.Tiles
{
    [Serializable]
    public class LevelMaterialType : ITileType, ILevelMaterial
    {
        public string Name;

        public Color DebugColor;

        public bool UseEdgeOnlyForTexTypeChanges;
        public bool ForceAngularCorners;

        //[FormerlySerializedAs("FloorMeshes")] 
        //public RefITileMeshMaterialProvider Floor;
        //[FormerlySerializedAs("WallMeshes")] 
        //public RefITileMeshMaterialProvider Wall;

        [SerializeField, FormerlySerializedAs("MeshMaterials")] 
        GeometryToMeshMaterialData[] m_meshMaterials = new GeometryToMeshMaterialData[0];

        public string TileName => Name;
        public Color TileColor => DebugColor;
        public Texture2D PreviewTex => MeshMaterials.IsNullOrEmpty() 
            ? null 
            : MeshMaterials[0].MeshMaterial.Result?.PreviewTex;

        public void GetProvider(ITileType geoTileConfig, List<ITileMeshMaterialUVProvider> provider) => 
            provider.AddRange(MeshMaterials.Where(mm => mm.GeometryId.TilesData() == geoTileConfig)
                .Select(mm=>mm.MeshMaterial?.Result));

        public GeometryToMeshMaterialData[] MeshMaterials => m_meshMaterials;

#if UNITY_EDITOR
        public ref GeometryToMeshMaterialData[] Editor_MeshMaterials => ref m_meshMaterials;
        public string Editor_MeshMaterialsPropName => nameof(m_meshMaterials);
#endif
    }
}