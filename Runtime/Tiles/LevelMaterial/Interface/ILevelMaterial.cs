namespace Level.Tiles
{
    public interface ILevelMaterial
    {
        public GeometryToMeshMaterialData[] MeshMaterials { get; }
#if UNITY_EDITOR
        public ref GeometryToMeshMaterialData[] Editor_MeshMaterials { get; }
        public string Editor_MeshMaterialsPropName { get; }
#endif
    }
}