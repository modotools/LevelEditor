using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Unity.Interface;
using Level.Tiles.Interface;
using Level.Texture;
using UnityEngine;
using UnityEngine.Serialization;

namespace Level.Tiles
{
    [CreateAssetMenu(fileName = nameof(LevelMaterialTypeConfig), 
        menuName = "Level/Tiles/" + nameof(LevelMaterialTypeConfig))]
    public class LevelMaterialTypeConfig : ScriptableObject, IParentMarker, IVerify,
        ITileConfig, IDependencies
    {
        #region SerializeField
        [SerializeField] RefITileConfig m_geometryConfig;
        [FormerlySerializedAs("m_textureTypeConfig")]
        [SerializeField] UVLayoutConfig m_uvLayoutConfig;
        [SerializeField] LevelMaterialType[] m_materialTypes;
        [SerializeField] LevelMaterialTransition[] m_materialTransitions;

        [SerializeField] ushort m_reserveBits;
        #endregion

        public LevelMaterialType[] MaterialTypes => m_materialTypes;
        public LevelMaterialTransition[] MaterialTransitions => m_materialTransitions;

        public UVLayoutConfig UVLayoutConfig => m_uvLayoutConfig;
        public ITileConfig GeometryConfig => m_geometryConfig.Result;

        #region ITileConfig implementation
        public string Name => name;
        public ushort ReserveBits => m_reserveBits;
        public int Count => m_materialTypes.Length;
        public ITileType this[int idx] => m_materialTypes[idx];
        #endregion

        public bool TransitionsPossible => !MaterialTransitions.IsNullOrEmpty();
        public LevelMaterialTransition GetTransition(int tileTypeA, int tileTypeB)
        {
            var t= MaterialTransitions.FirstOrDefault(mt =>
                mt.IndexA == tileTypeA && mt.IndexB == tileTypeB || 
                mt.IndexB == tileTypeA && mt.IndexA == tileTypeB);
            return t;
        }
        
        public bool IsTypeForcingAngularCorners(ushort matType) => MaterialTypes[matType].ForceAngularCorners;

        #region UNITY_EDITOR
#if UNITY_EDITOR
        public ref LevelMaterialType[] Editor_MaterialTypes => ref m_materialTypes;
        public ref LevelMaterialTransition[] Editor_MaterialTransitionTypes => ref m_materialTransitions;

        public static string Editor_GeometryConfigPropName => nameof(m_geometryConfig);
        public static string Editor_MaterialTypesPropName => nameof(m_materialTypes);
        public static string Editor_MaterialTransitionTypesPropName => nameof(m_materialTransitions);

        public static string Editor_TexTypeConfigPropName => nameof(m_uvLayoutConfig);

        public void Editor_Verify(ref VerificationResult result)
        {
            if (m_materialTypes.IsNullOrEmpty())
            {
                result.Error($"{nameof(m_materialTypes)} is null or empty", this);
                return;
            }

            for (var i = 0; i < m_materialTypes.Length; i++)
            {
                var t = m_materialTypes[i];
                if (t == null)
                {
                    result.Error($"NULL entry at index {i} in {nameof(LevelMaterialTypeConfig)}", this);
                    continue;
                }            
            }
        }
#endif
        #endregion

        public IScriptableObject Parent => this;

        public IEnumerable<ScriptableObject> Dependencies
        {
            get
            {
                yield return m_geometryConfig.Object as ScriptableObject;
                yield return m_uvLayoutConfig;
                foreach (var mt in MaterialTypes)
                foreach (var mm in mt.MeshMaterials)
                    yield return mm.MeshMaterial.Object as ScriptableObject;
                foreach (var mt in MaterialTransitions)
                foreach (var mm in mt.MeshMaterials)
                    yield return mm.MeshMaterial.Object as ScriptableObject;
            }
        }
    }
}