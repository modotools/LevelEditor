﻿using System;
using UnityEngine;

namespace Level.Data
{
    [Serializable] 
    public struct MeshSet
    {
        public Mesh[] Meshes;
    }
}
