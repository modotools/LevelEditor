﻿using System.Collections.Generic;
using Core.Extensions;
using Core.Types;
using UnityEngine;

namespace Level.Data
{
    /// <summary>
    /// Data for Mesh-Building, will be set to the MeshFilter-component
    /// </summary>
    public struct MeshData
    {
        public static MeshData Default(int capacity = 1000)
        {
            var md = new MeshData()
            {
                Vertices = SimplePool<List<Vector3>>.I.Get(),
                UVs = SimplePool<List<Vector2>>.I.Get(),
                UVs2 = SimplePool<List<Vector2>>.I.Get(),
                UVs3 = SimplePool<List<Vector2>>.I.Get(),
                Triangles = SimplePool<List<int>>.I.Get()
            };
            md.Vertices.Capacity = capacity;
            md.UVs.Capacity = capacity;
            md.UVs2.Capacity = capacity;
            md.UVs3.Capacity = capacity;
            md.Triangles.Capacity = capacity;
            return md;
        }

        public List<Vector3> Vertices;
        public List<Vector2> UVs;
        public List<Vector2> UVs2;
        public List<Vector2> UVs3;

        public List<int> Triangles;

        public static void Merge(ref MeshData target, MeshData from)
        {
            var startCount = target.Vertices.Count;
            target.Vertices.AddRange(from.Vertices);
            target.UVs.AddRange(from.UVs);
            target.UVs2.AddRange(from.UVs2);
            target.UVs3.AddRange(from.UVs3);
            for (var i = 0; i < from.Triangles.Count; i++) 
                target.Triangles.Add(startCount + from.Triangles[i]);
        }

        public static void Release(ref MeshData md)
        {
            SimplePool<List<Vector3>>.I.Return(md.Vertices);
            SimplePool<List<Vector2>>.I.Return(md.UVs);
            SimplePool<List<Vector2>>.I.Return(md.UVs2);
            SimplePool<List<Vector2>>.I.Return(md.UVs3);
            SimplePool<List<int>>.I.Return(md.Triangles);
            md.Vertices = null;
            md.UVs = null;
            md.UVs2 = null;
            md.UVs3 = null;
            md.Triangles = null;
        }
    }
}
