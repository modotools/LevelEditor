﻿using System.Collections.Generic;
using Core.Types;
using UnityEngine;

namespace Level.Data
{
    /// <summary>
    /// Data for Mesh-Building, will be set to the MeshFilter-component
    /// </summary>
    public struct MaterialToMeshData
    {
        public static MaterialToMeshData Default => new MaterialToMeshData
        {
            MaterialMeshData = SimplePool<Dictionary<Material, MeshData>>.I.Get()
        };

        public Dictionary<Material, MeshData> MaterialMeshData;

        public static void Release(ref MaterialToMeshData data)
        {
            if (data.MaterialMeshData == null)
                return;
            foreach (var md in data.MaterialMeshData.Values)
            {
                var value = md;
                MeshData.Release(ref value);
            }
            SimplePool<Dictionary<Material, MeshData>>.I.Return(data.MaterialMeshData);
            data.MaterialMeshData = null;
        }
    }
}
