﻿using System;
using UnityEngine;

namespace Level.Data
{
    /// <summary>
    /// Config for picking a specific Mesh for each different Marching-Squares-Case
    /// </summary>
    [CreateAssetMenu(menuName = "Level/Tiles/"+nameof(TileMeshConfig), fileName = nameof(TileMeshConfig))]
    public class TileMeshConfig : ScriptableObject
    {
        [SerializeField] TileMeshConfigData[] m_newConfigData = new TileMeshConfigData[16];
        [SerializeField] MeshSet[] m_configData = new MeshSet[16];
        public TileMeshConfigData Get(int i) => m_newConfigData[i];

#if UNITY_EDITOR
        public const string Editor_ConfigDataPropName = nameof(m_newConfigData);
#endif
    }

    [Serializable]
    public struct TileMeshConfigData
    {
        public MeshSet[] MeshVariations;
    }
}
