﻿
using Level.PlatformLayer.Interface;
using UnityEngine;
using UnityEngine.Serialization;

namespace Level
{
    [CreateAssetMenu(fileName = nameof(GridSettings), menuName = "Level/" + nameof(GridSettings))]
    public class GridSettings : ScriptableObject, IGridData
    {
        /// <summary>
        /// when using a Grid to f.e. create a room with size 2x2 (with TileSize 1) you can choose 
        /// a data layout of 2 x 2 (<see cref="DimensionLayout.Tiles"></see>) for choosing each room-tiles explicitly
        /// or data layout 3 x 3 (<see cref="DimensionLayout.Corners"></see>) for using a MarchingSquares algorithm 
        /// and create something less blocky
        /// 
        /// Tiles (2x2)   Corners (3x3)
        /// ╔═══╦═══╗     6═══7═══8
        /// ║ 2 ║ 3 ║     ║   ║   ║
        /// ╠═══╬═══╣     3═══4═══5
        /// ║ 0 ║ 1 ║     ║   ║   ║
        /// ╚═══╩═══╝     0═══1═══2
        /// </summary>
        public enum DimensionLayout
        {
            Tiles,
            Corners
        }

        [FormerlySerializedAs("TileSize")] 
        [SerializeField] 
        float m_tileSize = 1;
        [FormerlySerializedAs("TileHeight")] 
        [SerializeField]
        float m_tileHeight = 1;

        public Vector3 TileSize3D => new Vector3(m_tileSize, m_tileHeight, m_tileSize);
        public int RoomChunkSize = 8;
        public int RoomYChunkSize = 4;
        public Vector3Int RoomChunkSize3D => new Vector3Int(RoomChunkSize, RoomYChunkSize, RoomChunkSize);

        public float HalfRoomChunkSize => 0.5f * RoomChunkSize;

        public DimensionLayout DimLayout;

        public float TileSize => m_tileSize;
        public float TileHeight => m_tileHeight;
        public int ExtraDim => _ExtraDim(DimLayout);
        public Vector2 Offset => _Offset(DimLayout);

        internal static int _ExtraDim(DimensionLayout layout) => layout == DimensionLayout.Corners ? 1 : 0;
        internal Vector2 _Offset(DimensionLayout layout) => layout == DimensionLayout.Corners ? Vector2.zero 
            : 0.5f * m_tileSize * new Vector2(1,1);

        
        //public int SmallRoomSize => RoomGridSize;
        //public Vector3Int SmallFlatRoomSize => new Vector3Int(SmallRoomSize, RoomYGridSize, SmallRoomSize);
        //public int StandardRoomSize => SmallRoomSize * 2;
        //public int BigRoomSize => StandardRoomSize * 2;
        //public int HugeRoomSize => BigRoomSize * 2;
    }
}