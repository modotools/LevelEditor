using System.Collections.Generic;
using Core.Extensions;
using Core.Interface;
using Core.Unity.Interface;
using ScriptableUtility;
using UnityEngine;

namespace Level.Objects
{
    public class SceneLevelObjectsConfig : ScriptableObject, IDependencies, IAvailableContexts, IParentMarker
    {
        [SerializeField] LevelObjectsConfig[] m_lvlObjectsConfigs;
        [SerializeField] ContextAsset m_scene;

        public ContextAsset ContextAsset => m_scene;

#if UNITY_EDITOR
        public ref LevelObjectsConfig[] Editor_LevelObjectsConfigs => ref m_lvlObjectsConfigs;
        public void Editor_LinkContextAssets(ContextAsset scene)
        {
            m_scene = scene;
            if (m_lvlObjectsConfigs.IsNullOrEmpty())
                return;
            foreach (var levelObjectsConfig in m_lvlObjectsConfigs)
                levelObjectsConfig.Editor_LinkAvailableContext(this);
        }
#endif

        public IEnumerable<ScriptableObject> Dependencies
        {
            get
            {
                foreach (var loc in m_lvlObjectsConfigs)
                    yield return loc;
                yield return m_scene;
            }
        }

        public IEnumerable<IProvider<IContext>> ContextProviders => new[] {m_scene};
        public IScriptableObject Parent => this;
    }
}
