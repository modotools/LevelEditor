using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Core.Extensions;
using Core.Unity;
using Core.Unity.Extensions;
using Core.Unity.Utility;
using Core.Unity.Utility.PoolAttendant;
using Cysharp.Threading.Tasks;
using Level.Room;
using Level.Room.Operations;
using ScriptableUtility;
using ScriptableUtility.Behaviour;
using UnityEngine;

using static ScriptableUtility.ConditionOperations;

namespace Level.Objects
{
    public class LevelObjectRuntimeData : IScriptableEventListener<bool>
    {
        struct RuntimeObjectData
        {
            public RuntimeConditionData[] SpawnConditions; // <- AND
            public GameObject Instance;
            //public ObjectActionsRuntimeData ObjectActions;
        }

        readonly List<RuntimeObjectData> m_toSpawnObjects = new List<RuntimeObjectData>();
        GameObject m_parent;
        Transform m_inactiveParent;

        public async UniTask Init(GameObject parent, Transform inactiveParent,
            LevelObjectsConfig config, 
            IExposedPropertyTable resolver,
            IRoomInstance room, 
            StopwatchFrameSplitter splitter = null,
            CancellationToken cancellationToken = default)
        {
            m_parent = parent;
            m_inactiveParent = inactiveParent;
            
            foreach (var d in config.GetObjectData())
                await Init(room, resolver, d, splitter, cancellationToken);
        }

        async UniTask Init(IRoomInstance room, 
            IExposedPropertyTable resolver, LevelObjectsConfig.ObjectData od, 
            StopwatchFrameSplitter splitter = null,
            CancellationToken cancellationToken = default)
        {
            var runtimeConditionData = GetRuntimeConditionData(od.SpawnConditions, this);

            GetSpatialDataForSpawn(od, out var spatialData);
            if (od.Prefab == null)
                return;
            var canSpawn = room.IsOpenPlaceForSpawn(od.TestPlacing, od.Prefab, spatialData);
            if (!canSpawn)
                return;

            var instance = Spawn(od, spatialData, resolver);
            InitObjectActions(od, instance);

            var runtimeData = new RuntimeObjectData()
            {
                SpawnConditions = runtimeConditionData,
                Instance = instance,
                //ObjectActions = objActions,
            };

            var fulfilled = runtimeConditionData.IsNullOrEmpty() || 
                runtimeConditionData.All(c => c.Condition.Value == c.ConditionValue);

            var parent = fulfilled ? room.PrefabParent : m_inactiveParent;
            //Debug.Log($"parenting {instance} to {parent}");
            instance.transform.SetParent(parent);
            if (!fulfilled)
                m_toSpawnObjects.Add(runtimeData);

            if (splitter != null)
                await splitter.SplitFrame(cancellationToken);
        }

        static void InitObjectActions(LevelObjectsConfig.ObjectData od, GameObject instance)
        {
            if (od.ObjectActions == null)
            {
                //Debug.Log($"{nameof(LevelObjectRuntimeData)}. {nameof(Init)} od.ObjectActions == null");
                return;
            }

            if (od.ObjectActions.ObjectContext == null)
            {
                Debug.LogError($"{nameof(LevelObjectRuntimeData)}." +
                               $"{nameof(Init)} od.ObjectActions.ObjectContext == null");
                return;
            }

            od.ObjectActions.ObjectContext.SetContextLink(instance);
            var actionStates = instance.AddComponent<ObjectActionStatesComponent>();
            actionStates.Init(od.ObjectActions);
        }

        void GetSpatialDataForSpawn(LevelObjectsConfig.ObjectData data, 
            out SpatialData spatialData)
        {
            spatialData.Position = m_parent.transform.TransformPoint(data.LocalPosition);
            spatialData.Rotation = m_parent.transform.rotation * Quaternion.Euler(data.LocalRotation);
            spatialData.Scale = data.Scale;
        }

        static GameObject Spawn(LevelObjectsConfig.ObjectData data, SpatialData spatialData, IExposedPropertyTable resolver)
        {
            var instance= data.Link.Resolve(resolver);
            if (instance == null) 
                instance = data.Prefab.GetPooledInstance();
            else 
                instance.SetActive(true);
            
            var tr = instance.transform;

            tr.position = spatialData.Position;
            tr.rotation = spatialData.Rotation;
            tr.localScale = spatialData.Scale;

            //Debug.Log($"{data.Prefab} {tr.position}");
            return instance;
        }

        public void OnEvent(bool var)
        {
            for (var i = m_toSpawnObjects.Count-1; i >= 0; i--)
            {
                var objData = m_toSpawnObjects[i];
                var conditionData = objData.SpawnConditions;
                var fulfilled = conditionData.IsNullOrEmpty() || 
                                conditionData.All(c => c.Condition.Value == c.ConditionValue);
                if (!fulfilled) 
                    continue;

                objData.Instance.transform.SetParent(null);
                m_toSpawnObjects.RemoveAt(i);
            }
        }
    }
}