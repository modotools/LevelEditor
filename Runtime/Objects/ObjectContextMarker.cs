using Core.Unity.Extensions;
using Core.Unity.Interface;
using Level.Room;
using ScriptableUtility.Behaviour;
using UnityEngine;

namespace Level.Objects
{
    /// <summary>
    /// only used in editor
    /// </summary>
    public class ObjectContextMarker : MonoBehaviour, IComponent
    {
        public enum ObjectPlacingContext
        {
            Scene,
            Room,
            None
        }

        public ObjectPlacingContext PlacingContext;
        public TestPlacing TestPlacing;

        public ObjectActionsConfig Actions;

        public LevelObjectsConfig LevelObjectsConfig;
        public RoomInstanceComponent CurrentRoom;
        public RoomObjectsDataComponent RoomObjects;

        public void ClearPlacingContext()
        {
            PlacingContext = ObjectPlacingContext.None;
            LevelObjectsConfig = null;
            CurrentRoom = null;
            RoomObjects = null;
        }
        public void SetPlacingContext(ObjectPlacingContext ctx, RoomInstanceComponent roomInstance, LevelObjectsConfig levelObjectsConfig)
        {
            PlacingContext = ctx;
            LevelObjectsConfig = levelObjectsConfig;
            CurrentRoom = roomInstance;
            CurrentRoom.TryGet(out RoomObjects);
            Debug.Assert(RoomObjects != null);
        }
    }
}