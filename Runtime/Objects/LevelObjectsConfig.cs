using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Interface;
using Core.Types;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Level.Room;
using ScriptableUtility;
using ScriptableUtility.Behaviour;
using UnityEngine;
using UnityEngine.Serialization;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Level.Objects
{
    public class LevelObjectsConfig : ScriptableObject, IDependencies, IAvailableContexts
    {
        [Serializable]
        public struct ObjectData
        {
            public ConditionData[] SpawnConditions; // <- AND
            public ObjectActionsConfig ObjectActions;

            public ExposedReference<GameObject> Link;
            public GameObject Prefab;
            public TestPlacing TestPlacing;

            public Vector3 LocalPosition;
            public Vector3 LocalRotation;
            public Vector3 Scale;

#if UNITY_EDITOR
            [NonSerialized] public GameObject Editor_Instance;
            [NonSerialized] public int Editor_InstanceID;
#endif
        }

        [SerializeField] GameObject m_resolverPrefab;

        [SerializeField] ObjectData[] m_objectData;
        [FormerlySerializedAs("m_room")] 
        [SerializeField] ContextAsset m_roomContextAsset;

        // can be null:
        [SerializeField] RefIAvailableContext m_availableContext;

        public IEnumerable<ObjectData> GetObjectData() => m_objectData;


        // public GameObject ObjectsPrefabInstance => RoomObjectMapper.gameObject;
        public RoomObjectMapper GetOrInstantiateMapper(GameObject roomGo, ref RoomObjectMapper mapper)
        {
            if (mapper != null)
                return mapper;
            if (ResolverPrefab == null)
                return null;
            if (roomGo == null)
                return null;

            var parent = roomGo.transform;

            ConnectPrefabInstanceFromChildren(roomGo, out var roomResolverGo);
            if (roomResolverGo == null)
                InstantiateResolver(ResolverPrefab, parent, out roomResolverGo);
                
            roomResolverGo.TryGet(out mapper);
            return mapper;
        }

        void ConnectPrefabInstanceFromChildren(GameObject roomGo, out GameObject roomResolverGo)
        {
            roomResolverGo = null;
#if UNITY_EDITOR
            foreach (var child in roomGo.Children())
            {
                if (PrefabUtility.GetPrefabAssetType(child.gameObject) != PrefabAssetType.Regular)
                    continue;
                if (PrefabUtility.GetCorrespondingObjectFromSource(child.gameObject) != ResolverPrefab)
                    continue;

                roomResolverGo = child.gameObject;
                SceneVisibilityManager.instance.DisablePicking(roomResolverGo, false);
                break;
            }
#endif
        }

        void InstantiateResolver(GameObject prefab, Transform parent, out GameObject roomResolverGo)
        {
            roomResolverGo = null;
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                roomResolverGo = PrefabUtility.InstantiatePrefab(prefab, parent) as GameObject;
                SceneVisibilityManager.instance.DisablePicking(roomResolverGo, false);
                return;
            }            
#endif
            roomResolverGo = Instantiate(prefab, parent, true);
        }

        public GameObject ResolverPrefab => m_resolverPrefab;
        public ContextAsset ContextAsset => m_roomContextAsset;
        public ContextAsset SceneContext => m_availableContext.Result?.ContextProviders
            .FirstOrDefault(prov=> prov is ContextAsset ca && string.Equals(ca.name, "Scene")) 
            as ContextAsset;
        
        /// <summary>
        /// Sets prefab for room objects, only used for objects in Room-context
        /// </summary>
        public void SetRoomObjectsPrefab(GameObject objectsPrefab) => m_resolverPrefab = objectsPrefab;

        
        public void ClearMissingPrefabs()
        {
            using (var objDataList = SimplePool<List<ObjectData>>.I.GetScoped())
            {
                objDataList.Obj.AddRange(m_objectData.Where(o=> o.Prefab != null));
                m_objectData = objDataList.Obj.ToArray();
            }
        }
        
#if UNITY_EDITOR
        public static string Editor_ObjectDataPropName => nameof(m_objectData);
        public ref ObjectData[] Editor_ObjectData => ref m_objectData;

        public void Editor_LinkContextAssets(ContextAsset room, IAvailableContexts contexts)
        {
            m_roomContextAsset = room;
            Editor_LinkAvailableContext(contexts);
        }

        public void Editor_LinkAvailableContext(IAvailableContexts contexts)
        {
            m_availableContext = new RefIAvailableContext() {Result = contexts};

            if (m_objectData.IsNullOrEmpty())
                return;
            foreach (var objectData in m_objectData)
            {
                if (objectData.ObjectActions == null)
                    continue;
                objectData.ObjectActions.Editor_LinkAvailableContexts(this);
            }

        }
#endif
        public IEnumerable<ScriptableObject> Dependencies
        {
            get
            {
                foreach (var od in m_objectData)
                    yield return od.ObjectActions;
                yield return m_roomContextAsset;
            }
        }

        public IEnumerable<IProvider<IContext>> ContextProviders
        {
            get
            {
                if (m_availableContext.Result != null)
                {
                    foreach (var p in m_availableContext.Result.ContextProviders)
                        yield return p;
                }

                yield return m_roomContextAsset;
            }
        }


    }
}
