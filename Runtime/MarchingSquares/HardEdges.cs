﻿using System.Collections.Generic;
using UnityEngine;

namespace Level.Data
{
    public struct HardEdges
    {
        public List<Vector3Int> Data;

        public static implicit operator List<Vector3Int> (HardEdges g) => g.Data;
        public static implicit operator HardEdges(List<Vector3Int> data) => new HardEdges { Data = data };

        public int Count => Data.Count;
 
        public Vector3Int this[int i]
        {
            get => Data[i];
            set => Data[i] = value;
        }
    }
}
