﻿using System;
using UnityEngine;

namespace Level.Data
{
    /// <summary>
    /// MS = Marching Squares
    /// 
    /// When looking at a Marching Squares Tile with its data in the corners for building a mesh
    /// 3════c════2
    /// ║         ║
    /// d    e    b
    /// ║         ║
    /// 0════a════1
    /// We have uncertain cases when corners 0, 2 f.e. want to be a wall and 1, 3 want to be floor or vice versa
    /// we could build a mesh by connecting a-b and c-d OR we could connect d-a and b-c. (Flattened Corners)
    ///     Option A          Option B          Solution C
    ///  Floor══c═══╗         ╔═══c══Wall       ╔═══c═══╗
    /// Floor / Wall║         ║Floor\ ║Wall   Floor ║ Wall
    ///     d  Wall b         d Floor b         d═══e═══b
    ///     ║ Wall/ Floor  Wall \Floor║        Wall ║ Floor
    ///     ╚═══a══Floor    Wall══a═══╝         ╚═══a═══╝
    /// 
    /// To deal with this we instead connect it always with center point e (IsHard = true) todo: rename to ForceAngularCorner?
    /// and since this is the same as dealing with one corner, we split this case into its corner-cases while forcing the angular corner.
    /// Also we cannot deal with the flattened corner cases if we have a mesh that connects to the center above or below,
    /// so we set IsHard = true in these cases, which will be transmitted to above/ below platform layers in MeshBuilder,
    /// until hitting a case that will always resolve this (for example empty tile, full floor, full wall and other cases)
    ///
    /// This Config provides a simple lookup-table for how to deal with these cases
    /// </summary>
    [CreateAssetMenu(fileName = nameof(MSSpecialCasesConfig),
        menuName = "Level/Tiles/"+nameof(MSSpecialCasesConfig))]
    public class MSSpecialCasesConfig : ScriptableObject
    {
        [SerializeField] MarchingSquareSpecialCasesData[] m_configData = new MarchingSquareSpecialCasesData[16];
        public MarchingSquareSpecialCasesData Get(int conf) => m_configData[conf];
#if UNITY_EDITOR
        public const string Editor_ConfigDataPropName = nameof(m_configData);
#endif
    }

    [Serializable]
    public struct MarchingSquareSpecialCasesData
    {
        public int[] ConfigurationSplit;
        public bool IsHard;
    }
}
