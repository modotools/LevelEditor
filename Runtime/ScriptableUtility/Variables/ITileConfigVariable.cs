using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility;
using Level.Data;
using Level.Tiles.Interface;


namespace Level.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/ITileConfig", fileName = nameof(ITileConfigVariable))]
    public class ITileConfigVariable : ScriptableVariable<ITileConfig> { }
}
