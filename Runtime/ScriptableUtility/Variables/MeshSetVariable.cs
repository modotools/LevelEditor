using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility;
using Level.Data;


namespace Level.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/MeshSet", fileName = nameof(MeshSetVariable))]
    public class MeshSetVariable : ScriptableVariable<MeshSet> { }
}
