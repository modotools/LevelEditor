using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility;
using Level.Data;


namespace Level.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/ITileMeshMaterialUVProvider", fileName = nameof(ITileMeshMaterialUVProviderVariable))]
    public class ITileMeshMaterialUVProviderVariable : ScriptableVariable<ITileMeshMaterialUVProvider> { }
}
