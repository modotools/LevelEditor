using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility;
using Level.Data;
using Level.Room;


namespace Level.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/IRoom", fileName = nameof(IRoomVariable))]
    public class IRoomVariable : ScriptableVariable<IRoom> { }
}
