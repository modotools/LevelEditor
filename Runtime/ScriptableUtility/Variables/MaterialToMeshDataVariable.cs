using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility;
using Level.Data;


namespace Level.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/MaterialToMeshData", fileName = nameof(MaterialToMeshDataVariable))]
    public class MaterialToMeshDataVariable : ScriptableVariable<MaterialToMeshData> { }
}
