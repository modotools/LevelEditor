using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility;
using Level.Data;
using Level.Tiles;


namespace Level.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/TilesSetFilter", fileName = nameof(TilesSetFilterVariable))]
    public class TilesSetFilterVariable : ScriptableVariable<TilesSetFilter> { }
}
