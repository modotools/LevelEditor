using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility;
using Level.Data;


namespace Level.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/TileMeshMaterialUVProviderList", fileName = nameof(TileMeshMaterialUVProviderListVariable))]
    public class TileMeshMaterialUVProviderListVariable : ScriptableVariable<TileMeshMaterialUVProviderList> { }
}
