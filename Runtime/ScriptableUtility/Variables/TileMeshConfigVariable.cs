using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility;
using Level.Data;


namespace Level.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/TileMeshConfig", fileName = nameof(TileMeshConfigVariable))]
    public class TileMeshConfigVariable : ScriptableVariable<TileMeshConfig>, IOverrideMapping
    {
        public void AddContext(IContext context, OverrideValue value) => AddContext(context, (TileMeshConfig) value.ObjectValue);
    }
}
