using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility;
using Level.Data;
using Level.Tiles;


namespace Level.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/TilesSetListConfig", fileName = nameof(TilesSetListConfigVariable))]
    public class TilesSetListConfigVariable : ScriptableVariable<TilesSetListConfig>, IOverrideMapping
    {
        public void AddContext(IContext context, OverrideValue value) => AddContext(context, (TilesSetListConfig) value.ObjectValue);
    }
}
