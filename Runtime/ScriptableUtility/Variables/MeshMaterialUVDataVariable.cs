using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility;
using Level.Data;


namespace Level.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/MeshMaterialUVData", fileName = nameof(MeshMaterialUVDataVariable))]
    public class MeshMaterialUVDataVariable : ScriptableVariable<MeshMaterialUVData> { }
}
