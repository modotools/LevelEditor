using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility;
using Level.Tiles;


namespace Level.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/TileBorderTagList", fileName = nameof(TileBuilderTagListVariable))]
    public class TileBuilderTagListVariable : ScriptableVariable<TileBuilderTagList> { }
}
