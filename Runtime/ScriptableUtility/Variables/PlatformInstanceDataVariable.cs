using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility;
using Level.Data;
using Level.Room;


namespace Level.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/PlatformInstanceData", fileName = nameof(PlatformInstanceDataVariable))]
    public class PlatformInstanceDataVariable : ScriptableVariable<PlatformInstanceData> { }
}
