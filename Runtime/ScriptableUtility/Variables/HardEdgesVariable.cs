using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility;
using Level.Data;


namespace Level.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/HardEdges", fileName = nameof(HardEdgesVariable))]
    public class HardEdgesVariable : ScriptableVariable<HardEdges> { }
}
