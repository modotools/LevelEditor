using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility;
using Level.Data;


namespace Level.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/MeshData", fileName = nameof(MeshDataVariable))]
    public class MeshDataVariable : ScriptableVariable<MeshData> { }
}
