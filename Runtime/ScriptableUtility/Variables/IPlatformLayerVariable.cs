using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility;
using Level.Data;
using Level.PlatformLayer.Interface;


namespace Level.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/IPlatformLayer", fileName = nameof(IPlatformLayerVariable))]
    public class IPlatformLayerVariable : ScriptableVariable<IPlatformLayer> { }
}
