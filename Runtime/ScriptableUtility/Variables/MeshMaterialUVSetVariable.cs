using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility;
using Level.Data;


namespace Level.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/MeshMaterialUVSet", fileName = nameof(MeshMaterialUVSetVariable))]
    public class MeshMaterialUVSetVariable : ScriptableVariable<MeshMaterialUVSet> { }
}
