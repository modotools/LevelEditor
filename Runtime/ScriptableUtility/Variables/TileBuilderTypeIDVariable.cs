using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility;
using Level.Tiles;


namespace Level.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/TileBorderTypeID", fileName = nameof(TileBuilderTypeIDVariable))]
    public class TileBuilderTypeIDVariable : ScriptableVariable<TileBuilderTypeID> { }
}
