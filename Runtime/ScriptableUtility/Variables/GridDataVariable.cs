using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility;
using Level.Data;


namespace Level.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/GridData", fileName = nameof(GridDataVariable))]
    public class GridDataVariable : ScriptableVariable<GridData> { }
}
