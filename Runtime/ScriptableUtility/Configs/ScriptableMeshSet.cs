﻿using Level.Data;
using ScriptableUtility;
using UnityEngine;

namespace Level.ScriptableUtility
{
    [CreateAssetMenu(menuName = "ScriptableVariables/Level/MeshSet")]
    public class ScriptableMeshSet : ScriptableVariable<MeshSet>
    {
    }
}