﻿using Level.Data;
using ScriptableUtility;
using UnityEngine;

namespace Level.ScriptableUtility
{
    [CreateAssetMenu(menuName = "ScriptableVariables/Level/MeshData")]
    public class ScriptableMeshData : ScriptableVariable<MeshData> 
    {
    }
}