﻿using ScriptableUtility;
using UnityEngine;

namespace Level.ScriptableUtility
{
    [CreateAssetMenu(menuName = "ScriptableVariables/Level/Grid")]
    public class ScriptableGrid : ScriptableVariable<ushort[,,]> 
    {
    }
}