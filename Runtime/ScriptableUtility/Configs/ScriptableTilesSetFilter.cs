﻿using Level.Tiles;
using ScriptableUtility;
using UnityEngine;

namespace Level.ScriptableUtility
{
    [CreateAssetMenu(menuName = "ScriptableVariables/Level/TilesSetFilter")]
    public class ScriptableTilesSetFilter : ScriptableVariable<TilesSetFilter>
    {
    }
}