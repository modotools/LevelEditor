using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility.Events;
using Level.Data;
using Level.Room;


namespace Level.Events
{
    [EditorIcon("icon-event")]
    [CreateAssetMenu(menuName = "Scriptable/Events/PlatformInstanceData")]
    public class PlatformInstanceDataEvent : ScriptableEvent<PlatformInstanceData> {}
}
