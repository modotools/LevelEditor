using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility.Events;
using Level.Tiles;


namespace Level.Events
{
    [EditorIcon("icon-event")]
    [CreateAssetMenu(menuName = "Scriptable/Events/TileBorderTypeID")]
    public class TileBuilderTypeIDEvent : ScriptableEvent<TileBuilderTypeID> {}
}
