using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility.Events;
using Level.Data;


namespace Level.Events
{
    [EditorIcon("icon-event")]
    [CreateAssetMenu(menuName = "Scriptable/Events/MeshMaterialUVData")]
    public class MeshMaterialUVDataEvent : ScriptableEvent<MeshMaterialUVData> {}
}
