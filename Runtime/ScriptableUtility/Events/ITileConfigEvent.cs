using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility.Events;
using Level.Data;
using Level.Tiles.Interface;


namespace Level.Events
{
    [EditorIcon("icon-event")]
    [CreateAssetMenu(menuName = "Scriptable/Events/ITileConfig")]
    public class ITileConfigEvent : ScriptableEvent<ITileConfig> {}
}
