using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility.Events;
using Level.Data;
using Level.Tiles;


namespace Level.Events
{
    [EditorIcon("icon-event")]
    [CreateAssetMenu(menuName = "Scriptable/Events/TilesSetFilter")]
    public class TilesSetFilterEvent : ScriptableEvent<TilesSetFilter> {}
}
