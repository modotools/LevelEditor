using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility.Events;
using Level.Data;
using Level.PlatformLayer.Interface;


namespace Level.Events
{
    [EditorIcon("icon-event")]
    [CreateAssetMenu(menuName = "Scriptable/Events/IPlatformLayer")]
    public class IPlatformLayerEvent : ScriptableEvent<IPlatformLayer> {}
}
