using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility.Events;
using Level.Data;


namespace Level.Events
{
    [EditorIcon("icon-event")]
    [CreateAssetMenu(menuName = "Scriptable/Events/ITileMeshMaterialUVProvider")]
    public class ITileMeshMaterialUVProviderEvent : ScriptableEvent<ITileMeshMaterialUVProvider> {}
}
