using ScriptableUtility.Variables;
using ScriptableUtility;
using UnityEngine;
using Level.Data;
using Level.PlatformLayer.Interface;


namespace Level.VariableNodes
{
    public class IPlatformLayerNode : VarNode<IPlatformLayer> { }
}
