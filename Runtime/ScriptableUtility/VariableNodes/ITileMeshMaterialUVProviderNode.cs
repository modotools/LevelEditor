using ScriptableUtility.Variables;
using ScriptableUtility;
using UnityEngine;
using Level.Data;


namespace Level.VariableNodes
{
    public class ITileMeshMaterialUVProviderNode : VarNode<ITileMeshMaterialUVProvider> { }
}
