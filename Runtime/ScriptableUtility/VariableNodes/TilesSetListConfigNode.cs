using ScriptableUtility.Variables;
using ScriptableUtility;
using UnityEngine;
using Level.Data;
using Level.Tiles;


namespace Level.VariableNodes
{
    public class TilesSetListConfigNode : VarNode<TilesSetListConfig> { }
}
