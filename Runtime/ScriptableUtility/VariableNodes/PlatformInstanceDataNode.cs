using ScriptableUtility.Variables;
using ScriptableUtility;
using UnityEngine;
using Level.Data;
using Level.Room;


namespace Level.VariableNodes
{
    public class PlatformInstanceDataNode : VarNode<PlatformInstanceData> { }
}
