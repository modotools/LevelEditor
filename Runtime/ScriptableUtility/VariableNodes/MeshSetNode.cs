using ScriptableUtility.Variables;
using ScriptableUtility;
using UnityEngine;
using Level.Data;


namespace Level.VariableNodes
{
    public class MeshSetNode : VarNode<MeshSet> { }
}
