using ScriptableUtility.Variables;
using ScriptableUtility;
using UnityEngine;
using Level.Data;
using Level.Tiles.Interface;


namespace Level.VariableNodes
{
    public class ITileConfigNode : VarNode<ITileConfig> { }
}
