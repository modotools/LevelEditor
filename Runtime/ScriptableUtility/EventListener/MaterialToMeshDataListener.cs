using ScriptableUtility.Events;
using UnityEngine;
using UnityEngine.Events;
using Level.Data;


namespace ScriptableUtility.EventListener
{
    public class MaterialToMeshDataListener : MonoBehaviour, IScriptableEventListener<MaterialToMeshData>
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] ScriptableEvent<MaterialToMeshData> m_scriptableEvent;
        [SerializeField] RefIContextProvider m_contextProvider;
        [SerializeField] UnityEvent<MaterialToMeshData> m_action;
#pragma warning restore 0649 // wrong warnings for SerializeField

        IContext m_context;
        IContext Context
        {
            get
            {
                if (m_context != null)
                    return m_context;
                m_contextProvider.Result?.Get(out m_context);
                return m_context;
            }
        }

        // todo: maybe we don't want to register at start or destroy, but onEnable/ Disable
        // shouldn't fire events disabled?
        // could be, that we want to set context at runtime and then listen?
        // should events also have Context-Type?
        protected virtual void Start() => RegisterListener();
        protected virtual void OnDestroy() => UnregisterListener();

        void RegisterListener()
        {
            if (m_scriptableEvent == null)
                return;

            if (Context != null)
                m_scriptableEvent.RegisterListener(Context, this);
            else
                m_scriptableEvent.RegisterListenerGlobal(this);
        }

        void UnregisterListener()
        {
            if (m_scriptableEvent == null)
                return;

            if (Context != null)
                m_scriptableEvent.UnregisterListener(Context, this);
            else
                m_scriptableEvent.UnregisterListenerGlobal(this);
        }

        public void OnEvent(MaterialToMeshData var) => m_action.Invoke(var);
        public IScriptableEvent EventListenedTo => m_scriptableEvent;
    }
}
