namespace Level.Enums
{
    public enum GeometryType
    {
        Auto,
        Floor,
        Wall,
        Empty
    }
}
