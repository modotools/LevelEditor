namespace Level.Enums
{
    /// <summary>
    /// Bitmask LSR
    /// (Left Straight Right)
    /// </summary>
    public enum ExitConfiguration
    {
        DeadEnd = 0,
        TurnR = 1, // ╔═
        Straight = 2,
        OptionR, // ╠═
        TurnL, //
        Fork,
        OptionL, //
        All, // ═╬═
        Invalid,
    }

    // {
    // North,
    // East,
    // South,
    // West,
    // }

    public enum RestrictionMode
    {
        AllowFirst,
        RestrictFirst,
    }
}
