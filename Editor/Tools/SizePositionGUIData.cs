﻿using Level.PlatformLayer.Interface;
using Level.Room;
using UnityEngine;

namespace Level.Editor.Tools
{
    public struct SizePositionGUIData
    {
        public bool AffectRoom;

        public SizePositionCategory Category;
        public IRoomInstance Room;
        public IPlatformLayer Layer;
        public bool[] SelectedLayers;

        public Vector3 GridSnap;
        public Vector3Int SizeData;
        public bool MouseWasDown;
        public bool ClickValid;
        public double ClickTime;

        public float LeftMove;
        public float RightMove;
        public float TopMove;
        public float BottomMove;

        public bool Moved => Mathf.Abs(LeftMove) > 0.1f
                             || Mathf.Abs(RightMove) > 0.1f
                             || Mathf.Abs(TopMove) > 0.1f
                             || Mathf.Abs(BottomMove) > 0.1f;
    }
}