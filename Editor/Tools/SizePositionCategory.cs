﻿namespace Level.Editor.Tools
{
    public enum SizePositionCategory
    {
        None,
        Size,
        Position,
        Tiles
    }
}