﻿using System.Collections.Generic;
using Core.Editor.Interface;
using Core.Editor.PopupWindows;
using Core.Editor.Utility;
using Core.Editor.Utility.GUITools;
using Core.Events;
using Core.Unity.Extensions;
using Core.Unity.Utility.GUITools;
using Level.Enums;
using Level.PlatformLayer;
using Level.PlatformLayer.Interface;
using Level.Room;
using Level.Room.Operations;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;
using static Core.Editor.Utility.CustomGUI;
using static Level.PlatformLayer.Operations;

namespace Level.Editor.Tools
{
    public static class SizePositionTools
    {
        const float k_buttonSize = 1.5f;
        //const float k_pickSize = 2f;
        const float k_acceptedClickTime = 0.5f;
        const string k_undoName = "Size/ Position";

        #region Editor Prefs
        const string k_editorPrefShowSizeTools = "RoomEditor.ShowSizeTools";
        const string k_editorPrefSizeToolsAffectRoom = "RoomEditor.SizeToolsAffectRoom";
        #endregion

        #region Init
        public static void Init_SizePositionTools(AnimBool showTools, IRepaintable r, ref SizePositionGUIData guiData)
        {
            showTools.valueChanged.RemoveAllListeners();
            showTools.valueChanged.AddListener(r.Repaint);

            showTools.target = EditorPrefs.GetBool(k_editorPrefShowSizeTools, false);
            guiData.AffectRoom = EditorPrefs.GetBool(k_editorPrefSizeToolsAffectRoom, false);
        }
        
        public static void InitSizePositionGUIData(ref SizePositionGUIData guiData,
            IRoomInstance room, 
            IPlatformLayer currentPlatform,
            bool[] selectedLayers)
        {
            if (currentPlatform == null)
                return;

            guiData.Room = room;
            guiData.Layer = currentPlatform;
            guiData.SizeData = room.Size;
            guiData.SelectedLayers = selectedLayers;

            ResetMove(ref guiData);
            UpdateSnapValue(ref guiData);
        }
        #endregion

        #region GUI
        public static void SizeToolsCategoryGUI(ref SizePositionGUIData guiData)
        {
            var prevCat = guiData.Category;
            guiData.Category = (SizePositionCategory) EditorGUILayout.EnumPopup("Size/Position: ", guiData.Category);
            if (prevCat == guiData.Category) 
                return;
            if (guiData.Category == SizePositionCategory.Tiles && guiData.AffectRoom)
                guiData.AffectRoom = false;
        }

        public static void SizeToolsCategoryGUI(ref SizePositionCategory cat) 
            => cat = (SizePositionCategory) EditorGUILayout.EnumPopup("Size/Position: ", cat);

        public static void SizeToolsRoomOptionGUI(ref SizePositionGUIData guiData)
        {
            using (new GUILayout.HorizontalScope())
            {
                var prev = guiData.AffectRoom;
                if (ActivityButton(guiData.AffectRoom, "Room"))
                    guiData.AffectRoom = true;
                if (ActivityButton(!guiData.AffectRoom, "Level"))
                    guiData.AffectRoom = false;
                if (guiData.AffectRoom == prev) 
                    return;

                if (guiData.AffectRoom && guiData.Category == SizePositionCategory.Tiles)
                    guiData.Category = SizePositionCategory.Position;

                ResetMove(ref guiData);
                UpdateSnapValue(ref guiData);
                
                EditorPrefs.SetBool(k_editorPrefSizeToolsAffectRoom, guiData.AffectRoom);
            }
        }

        public static void LevelSelectGUI(ref SizePositionGUIData guiData)
        {
            using (new GUILayout.HorizontalScope())
            {
                for (var i = 0; i < guiData.SelectedLayers.Length; i++)
                    guiData.SelectedLayers[i] = GUILayout.Toggle(guiData.SelectedLayers[i], $"{i}", "Button");
            }
        }

        #endregion

        #region Scene GUI
        public static void SizePositionSlider_SceneGUI(ref SizePositionGUIData guiData)
        {
            if (guiData.Category == SizePositionCategory.None) 
                return;

            var bounds = guiData.AffectRoom
                ? guiData.Room.Bounds
                : GetBounds(guiData.Layer);

            var res = UpdateMouseDown(ref guiData);
            if (res == MouseHandlingResult.Done 
                || res == MouseHandlingResult.NotAccepted)
                ResetMove(ref guiData);

            DrawSizePositionSlider(ref guiData, bounds);
        }

        static void DrawSizePositionSlider(ref SizePositionGUIData guiData, Bounds bounds)
        {
            var isSizeTool = guiData.Category == SizePositionCategory.Size;

            GetButtonPositions(guiData.Category, bounds,
                out var left, out var right, out var top, out var bottom);
            GetButtonPositionsMoved(guiData, left, right, top, bottom,
                out var leftMoved, out var rightMoved, out var topMoved, out var bottomMoved);

            if (guiData.Moved)
                DrawMoveLabelsAndBounds(ref guiData, bounds, leftMoved, rightMoved, topMoved, bottomMoved);

            Slider(SizePositionButton.Left, ref leftMoved, Color.magenta, Vector3.left);
            Slider(SizePositionButton.Right, ref rightMoved, Color.red, Vector3.right);
            Slider(SizePositionButton.Up, ref topMoved, Color.blue, Vector3.forward);
            Slider(SizePositionButton.Down, ref bottomMoved, Color.cyan, Vector3.back);

            if (isSizeTool)
            {
                GetButton2Positions(leftMoved, rightMoved, topMoved, bottomMoved,
                    out var left2, out var right2, out var top2, out var bottom2);

                Slider(SizePositionButton.SizeRightEdgeLeft, ref right2, Color.magenta, Vector3.left);
                Slider(SizePositionButton.SizeLeftEdgeRight, ref left2, Color.red, Vector3.right);
                Slider(SizePositionButton.SizeBottomUp, ref bottom2, Color.blue, Vector3.forward);
                Slider(SizePositionButton.SizeTopDown, ref top2, Color.cyan, Vector3.back);

                ApplyButton2Positions(ref leftMoved, ref rightMoved, ref topMoved, ref bottomMoved, left2, right2, top2, bottom2);
            }

            if (guiData.MouseWasDown)
            {
                ApplyMove(ref guiData,
                    rightMoved, right,
                    leftMoved, left,
                    topMoved, top,
                    bottomMoved, bottom);
            }
        }

        static void DrawMoveLabelsAndBounds(ref SizePositionGUIData guiData,
            Bounds bounds,
            Vector3 leftMoved, Vector3 rightMoved, Vector3 topMoved, Vector3 bottomMoved)
        {
            CustomHandles.DrawText(leftMoved, 40 * Vector2.up, $"{guiData.LeftMove}", Color.green, EditorStyles.largeLabel);
            CustomHandles.DrawText(rightMoved, 40 *Vector2.up, $"{guiData.RightMove}", Color.green, EditorStyles.largeLabel);
            CustomHandles.DrawText(topMoved,40 * Vector2.right,$"{guiData.TopMove}", Color.green, EditorStyles.largeLabel);
            CustomHandles.DrawText(bottomMoved,  40 *Vector2.right,$"{guiData.BottomMove}", Color.green, EditorStyles.largeLabel);

            bounds.min = new Vector3(bounds.min.x + guiData.LeftMove, bounds.min.y, bounds.min.z + guiData.BottomMove);
            bounds.max = new Vector3(bounds.max.x + guiData.RightMove, bounds.max.y, bounds.max.z + guiData.TopMove);
            CustomHandleDraw.DrawBounds(bounds, Color.white);
        }

        #endregion

        #region Apply
        static void ApplyButton(SizePositionGUIData gDat, SizePositionButton button, int steps, bool multiplyGrid)
        {
            //Debug.Log("Apply Button!");
            if (gDat.Room is RoomInstanceComponent ric)
                Undo.RecordObjects(new Object[] {ric.Data.RoomConfig, ric.Data.GameObject}, k_undoName);
            else if(gDat.Layer is PlatformLayerConfig plc)
                Undo.RecordObject(plc, k_undoName);

            SizePositionMod mod = default;
            mod.AffectRoom = gDat.AffectRoom;

            SizeToolsButtonToData(gDat.Category, button, ref mod, steps);
            if (button == SizePositionButton.None)
                return;
            if (mod.AffectRoom)
                ApplySizeToolRoom(gDat.Room, mod, multiplyGrid, gDat.Category == SizePositionCategory.Size);
            else if(gDat.SelectedLayers == null || gDat.Room == null)
                ApplySizeTool(gDat.Layer, gDat.Layer, gDat.SizeData, mod);
            else
            {
                for (var i = 0; i < gDat.SelectedLayers.Length; i++)
                {
                    if (!gDat.SelectedLayers[i])
                        continue;

                    var layer = gDat.Room.GetPlatformLayer(i);
                    ApplySizeTool(layer, layer, gDat.SizeData, mod);
                }
            }
        }
        static void ApplySizeToolRoom(IRoomInstance room, SizePositionMod mod, 
            bool multiplyGrid, bool modPlatforms)
        {
            if (room == null)
                return;
            var grid = room.RoomSettings.Grid;
            if (multiplyGrid)
            {
                mod.SizeModifier *= grid.RoomChunkSize;
                mod.PosModifier *= grid.RoomChunkSize;
                mod.TileModifier *= grid.RoomChunkSize;
            }

            var newSize = room.Size + mod.SizeModifier.Vector3Int();
            if (newSize.x <= 0 || newSize.z <= 0)
                return;

            room.Config.RoomConfigData.Size = newSize;
            var newPos = room.Position + mod.PosModifier.Vector3();
            if (room is RoomInstanceComponent ric)
                ric.transform.position = newPos;
            room.Data.UpdateRoomPosition(newPos);
            if (!modPlatforms)
                return;

            if (mod.SpecialCase)
            {
                //Debug.Log($"size mod {mod.SizeModifier} to pos-mod");
                mod.PosModifier = mod.SizeModifier;
                mod.TileModifier = Vector2Int.zero;
            }

            mod.SizeModifier = Vector2Int.zero;
            foreach (var pl in room.PlatformLayer) 
                ApplySizeTool(pl, pl, room.Size, mod);
        }
        static void ApplySizeTool(IPlatformLayerSpatialData platform, IPlatformLayerTiles tiles, Vector3Int roomSize,
            SizePositionMod mod)
        {
            if (mod.SpecialCase)
                mod.TileModifier = (mod.PosModifier * -1);

            var sizeModifier = mod.SizeModifier;
            var offsetModifier = mod.PosModifier;
            var tileModifier = mod.TileModifier;

            var newSize = platform.TileDim + sizeModifier;
            var newPos = platform.Position + offsetModifier;

            // level cannot begin before room begins, shrink size, add offset
            if (newPos.x < 0)
            {
                tileModifier += Vector2Int.right * newPos.x;
                newSize = new Vector2Int(Mathf.Max(0, newSize.x + newPos.x), newSize.y);
                newPos = new Vector2Int(0, newPos.y);
            }
            if (newPos.y < 0)
            {
                tileModifier += Vector2Int.up * newPos.y;
                newSize = new Vector2Int(newSize.x, Mathf.Max(0, newSize.y + newPos.y));
                newPos = new Vector2Int(newPos.x, 0);
            }

            if (ApplySizeTool_CheckLevelOutsideOfRoom(platform, tiles, roomSize, newPos, newSize, out var roomTileDim))
            {
                //Debug.Log($"lvl outside room newPos {newPos} newSize {newSize}");
                return;
            }
            CutIfOverflowing(newPos, ref newSize, roomTileDim);

            var newTiles = new ushort[newSize.x * newSize.y];
            //var newTiles = new PlatformLayerTilesArray() { Tiles_ = new PlatformLayerTilesBufferElement[newSize.x * newSize.y] };
            ApplySizeTool(platform, tiles, newSize, tileModifier, newTiles);
            platform.Position = newPos;

            var dimDiff = platform.TileDim - platform.Size;
            platform.Size = newSize - dimDiff;
            tiles.SetTiles(newTiles);

            SceneView.RepaintAll();
            EventMessenger.TriggerEvent(new PlatformSizePositionChanged(){ Platform = platform });
        }

        static bool ApplySizeTool_CheckLevelOutsideOfRoom(IPlatformLayerSpatialData platform, IPlatformLayerTiles tiles,
            Vector3Int roomSize, Vector2Int newPos, Vector2Int newSize, out Vector2Int roomTileDim)
        {
            // level begins outside of room or a size-dimension is 0 -> empty level
            roomTileDim = roomSize.Vector2Int() + new Vector2Int(1, 1);
            // ReSharper disable once ComplexConditionExpression
            if (newPos.x < roomTileDim.x && newPos.y < roomTileDim.y && newSize.x > 0 && newSize.y > 0)
                return false;
            platform.Position = Vector2Int.zero;
            platform.Size = Vector2Int.one;
            var newTiles = new ushort[platform.TileDim.x * platform.TileDim.y];
            tiles.SetTiles(newTiles);
            return true;
        }
        static void ApplySizeTool(IPlatformLayerSpatialData platform, IPlatformLayerTiles tiles, Vector2Int newSize,
            Vector2Int tileModifier, IList<ushort> newTiles)
        {
            for (var x = 0; x < newSize.x; ++x)
            {
                for (var z = 0; z < newSize.y; ++z)
                {
                    var oldX = x - tileModifier.x;
                    var oldZ = z - tileModifier.y;
                    var idx = z * newSize.x + x;

                    if (!platform.IsInside(new Vector2Int(oldX, oldZ)))
                    {
                        newTiles[idx] = 0;
                        continue;
                    }

                    newTiles[idx] = GetTile(platform, tiles, new Vector2Int(oldX, oldZ), OutOfLevelBoundsAction.IgnoreTile);
                }
            }
        }
        #endregion

        #region Utility
        
        public static void UpdateSnapValue(ref SizePositionGUIData guiData)
        {
            guiData.GridSnap = guiData.AffectRoom
                ? guiData.Room.RoomSettings.Grid.RoomChunkSize3D
                : Vector3.one;
        }

        static void CutIfOverflowing(Vector2Int newPos, ref Vector2Int newSize, Vector2Int roomTileDim)
        {
            // level is not allowed to extend the roomSize, lvl is cut:
            var newExtends = newPos + newSize;
            if (newExtends.x > roomTileDim.x) 
                newSize = new Vector2Int(roomTileDim.x - newPos.x, newSize.y);

            if (newExtends.y > roomTileDim.y) 
                newSize = new Vector2Int(newSize.x, roomTileDim.y - newPos.y);
        }

        static void SizeToolsButtonToData(SizePositionCategory cat, SizePositionButton button, ref SizePositionMod mod, int steps = 1)
        {
            switch (cat)
            {
                case SizePositionCategory.Size: switch (button)
                {
                    case SizePositionButton.SizeTopUp:
                        mod.SizeModifier = steps * Vector2Int.up;
                        break;
                    case SizePositionButton.SizeTopDown:
                        mod.SizeModifier = steps * Vector2Int.down;
                        break;
                    case SizePositionButton.SizeLeftEdgeRight:
                        mod.SizeModifier = steps * Vector2Int.left;
                        mod.PosModifier = steps * Vector2Int.right;
                        mod.SpecialCase = true;
                        break;
                    case SizePositionButton.SizeLeftEdgeLeft:
                        mod.SizeModifier = steps * Vector2Int.right;
                        mod.PosModifier = steps * Vector2Int.left;
                        mod.SpecialCase = true;
                        break;
                    case SizePositionButton.SizeRightEdgeRight:
                        mod.SizeModifier = steps * Vector2Int.right;
                        break;
                    case SizePositionButton.SizeRightEdgeLeft:
                        mod.SizeModifier = steps * Vector2Int.left;
                        break;
                    case SizePositionButton.SizeBottomDown:
                        mod.SizeModifier = steps * Vector2Int.up;
                        mod.PosModifier = steps * Vector2Int.down;
                        mod.SpecialCase = true;
                        break;
                    case SizePositionButton.SizeBottomUp:
                        mod.SizeModifier = steps * Vector2Int.down;
                        mod.PosModifier = steps * Vector2Int.up;
                        mod.SpecialCase = true;
                        break;
                } break;

                case SizePositionCategory.Position: switch (button)
                {
                    case SizePositionButton.Left: mod.PosModifier = steps * Vector2Int.left; break;
                    case SizePositionButton.Up: mod.PosModifier = steps * Vector2Int.up; break;
                    case SizePositionButton.Down: mod.PosModifier = steps * Vector2Int.down; break;
                    case SizePositionButton.Right: mod.PosModifier = steps * Vector2Int.right; break;
                } break;

                case SizePositionCategory.Tiles: switch (button)
                {
                    case SizePositionButton.Left: mod.TileModifier = steps * Vector2Int.left; break;
                    case SizePositionButton.Up: mod.TileModifier = steps * Vector2Int.up; break;
                    case SizePositionButton.Down: mod.TileModifier = steps * Vector2Int.down; break;
                    case SizePositionButton.Right: mod.TileModifier = steps * Vector2Int.right; break;
                } break;
            }
        }
        static void GetButtonAndStepsFromMove(SizePositionGUIData guiData, out SizePositionButton button, out int steps)
        {
            button = SizePositionButton.None;
            steps = 0;
            if (guiData.LeftMove < -float.Epsilon)
            {
                button = SizePositionButton.Left;
                steps = Mathf.RoundToInt(Mathf.Abs(guiData.LeftMove));
            }
            else if (guiData.LeftMove > float.Epsilon)
            {
                button = SizePositionButton.SizeLeftEdgeRight;
                steps = Mathf.RoundToInt(Mathf.Abs(guiData.LeftMove));
            }
            else if (guiData.TopMove > float.Epsilon)
            {
                button = SizePositionButton.Up;
                steps = Mathf.RoundToInt(Mathf.Abs(guiData.TopMove));
            }
            else if (guiData.TopMove < -float.Epsilon)
            {
                button = SizePositionButton.SizeTopDown;
                steps = Mathf.RoundToInt(Mathf.Abs(guiData.TopMove));
            }
            else if (guiData.RightMove > float.Epsilon)
            {
                button = SizePositionButton.Right;
                steps = Mathf.RoundToInt(Mathf.Abs(guiData.RightMove));
            }
            else if (guiData.RightMove < -float.Epsilon)
            {
                button = SizePositionButton.SizeRightEdgeLeft;
                steps = Mathf.RoundToInt(Mathf.Abs(guiData.RightMove));
            }
            else if (guiData.BottomMove < -float.Epsilon)
            {
                button = SizePositionButton.Down;
                steps = Mathf.RoundToInt(Mathf.Abs(guiData.BottomMove));
            }
            else if (guiData.BottomMove > float.Epsilon)
            {
                button = SizePositionButton.SizeBottomUp;
                steps = Mathf.RoundToInt(Mathf.Abs(guiData.BottomMove));
            }
        }

        #region Handle Slider as Button
        const int k_controlIDOffset = 876423;

        static int ToControlID(SizePositionButton button)
            => (int) button + k_controlIDOffset;

        static bool IsClickSliderButton()
        {
            var id= HandleUtility.nearestControl - k_controlIDOffset;
            return id > (int) SizePositionButton.None && id <= (int) SizePositionButton.Count;
        }

        static SizePositionButton GetNearestButton() 
            => (SizePositionButton) (HandleUtility.nearestControl - k_controlIDOffset);
        #endregion

        #region Handle Utility
        static void Slider(SizePositionButton button, ref Vector3 pos, Color col, Vector3 dir)
        {
            using (new ColorScope(col))
                pos = Handles.Slider(ToControlID(button), pos, dir, k_buttonSize, Handles.ConeHandleCap, 1f);
        }
        #endregion

        #region Button Positions and Move Offset
        static void GetButtonPositions(SizePositionCategory category, Bounds bounds, 
            out Vector3 l, out Vector3 r, out Vector3 t, out Vector3 b)
        {
            var minX = bounds.min.x;
            var maxX = bounds.max.x;
            var minZ = bounds.min.z;
            var maxZ = bounds.max.z;
            var center = bounds.center;

            var isSizeTool = category == SizePositionCategory.Size;
            var offset = isSizeTool ? 5f : 2f;
            l = new Vector3(minX - offset, center.y, center.z);
            r = new Vector3(maxX + offset, center.y, center.z);
            t = new Vector3(center.x, center.y, maxZ + offset);
            b = new Vector3(center.x, center.y, minZ - offset);
        }

        static void GetButton2Positions(Vector3 left, Vector3 right, Vector3 top, Vector3 bottom, out Vector3 left2,
            out Vector3 right2, out Vector3 top2, out Vector3 bottom2)
        {
            const float btnGap = 3f;
            left2 = new Vector3(left.x + btnGap, left.y, left.z);
            right2 = new Vector3(right.x - btnGap, right.y, right.z);
            top2 = new Vector3(top.x, top.y, top.z - btnGap);
            bottom2 = new Vector3(bottom.x, bottom.y, bottom.z + btnGap);
        }

        static void ApplyButton2Positions(ref Vector3 left, ref Vector3 right, ref Vector3 top, ref Vector3 bottom,
            Vector3 left2, Vector3 right2, Vector3 top2, Vector3 bottom2)
        {
            const float btnGap = 3f;
            left.x = left2.x - btnGap;
            right.x = right2.x + btnGap;
            top.z = top2.z + btnGap;
            bottom.z = bottom2.z - btnGap;
        }
        static void ApplyMove(ref SizePositionGUIData guiData, 
            Vector3 rightMoved, Vector3 right, 
            Vector3 leftMoved, Vector3 left, 
            Vector3 topMoved, Vector3 top, 
            Vector3 bottomMoved, Vector3 bottom)
        {
            guiData.RightMove = Snapping.Snap(rightMoved.x - right.x, guiData.GridSnap.x);
            guiData.LeftMove = Snapping.Snap(leftMoved.x - left.x, guiData.GridSnap.x);
            guiData.TopMove = Snapping.Snap(topMoved.z - top.z, guiData.GridSnap.z);
            guiData.BottomMove = Snapping.Snap(bottomMoved.z - bottom.z, guiData.GridSnap.z);
        }

        static void GetButtonPositionsMoved(SizePositionGUIData guiData, Vector3 left, Vector3 right, Vector3 top, Vector3 bottom,
            out Vector3 leftMoved, out Vector3 rightMoved, out Vector3 topMoved, out Vector3 bottomMoved)
        {
            leftMoved = left + guiData.LeftMove * Vector3.right;
            rightMoved = right + guiData.RightMove * Vector3.right;
            topMoved = top + guiData.TopMove * Vector3.forward;
            bottomMoved = bottom + guiData.BottomMove * Vector3.forward;
        }     
        static void ResetMove(ref SizePositionGUIData guiData)
        {
            guiData.MouseWasDown = false;

            guiData.LeftMove = 0f;
            guiData.RightMove = 0f;
            guiData.TopMove = 0f;
            guiData.BottomMove = 0f;
        }
        #endregion

        #region Mouse Handling

        enum MouseHandlingResult
        {
            Accepted,
            Done,
            NotAccepted,
            Ignored
        }
        static MouseHandlingResult UpdateMouseDown(ref SizePositionGUIData guiData)
        {
            if (Event.current.button != 0 || Event.current.alt)
                return MouseHandlingResult.NotAccepted;

            switch (Event.current.type)
            {
                case EventType.MouseDown:
                    guiData.ClickValid = true;
                    guiData.MouseWasDown = true;
                    guiData.ClickTime = EditorApplication.timeSinceStartup;
                    return MouseHandlingResult.Accepted;
                case EventType.MouseUp:
                {
                    if (guiData.Moved && guiData.MouseWasDown)
                    {
                        //Debug.Log($"Moved {guiData.LeftMove} {guiData.BottomMove} {guiData.RightMove} {guiData.TopMove}");
                        GetButtonAndStepsFromMove(guiData, out var button, out var steps);
                        ApplyButton(guiData, button, steps, false);
                        return MouseHandlingResult.Done;
                    }

                    if(guiData.ClickValid)
                    {
                        //Debug.Log("ClickValid");
                        guiData.ClickValid =
                            EditorApplication.timeSinceStartup - guiData.ClickTime < k_acceptedClickTime;
                        guiData.ClickValid &= IsClickSliderButton();
                        if (!guiData.ClickValid)
                            return MouseHandlingResult.NotAccepted;

                        var button = GetNearestButton();
                        var gDat = guiData;
                        ResetMove(ref guiData);

                        var popup = new IntPopup(1,
                            num => ApplyButton(gDat, button, (int) num, true));
                        PopupWindow.Show(new Rect(Event.current.mousePosition, IntPopup.WindowSize), popup);
                    }
                    return MouseHandlingResult.NotAccepted;
                }
                case EventType.MouseDrag:
                    guiData.ClickValid = false;
                    break;
            }
            return MouseHandlingResult.Ignored;
        }
        #endregion
        #endregion
    }
}

// ReSharper disable once MethodTooLong
//static SizePositionButton SizeToolsInnerGUI(out SizePositionCategory cat, out SizePositionButton button)
//{
//    cat = SizePositionCategory.None;
//    button = SizePositionButton.None;
//    using (new GUILayout.HorizontalScope())
//    {
//        using (new GUILayout.VerticalScope())
//        {
//            using (new GUILayout.HorizontalScope())
//            {
//                GUILayout.Space(57);
//                if (GUILayout.Button("^", GUILayout.Width(20))) { cat = SizePositionCategory.Size; button = SizePositionButton.SizeTopUp; }
//            }
//            using (new GUILayout.HorizontalScope())
//            {
//                GUILayout.Space(57);
//                if (GUILayout.Button("v", GUILayout.Width(20))) { cat = SizePositionCategory.Size; button = SizePositionButton.SizeTopDown; }
//            }
//            using (new GUILayout.HorizontalScope())
//            {
//                if (GUILayout.Button("<", GUILayout.Width(20))) { cat = SizePositionCategory.Size; button = SizePositionButton.SizeLeftEdgeLeft; }
//                if (GUILayout.Button(">", GUILayout.Width(20))) { cat = SizePositionCategory.Size; button = SizePositionButton.SizeLeftEdgeRight; }
//                GUILayout.Label("Size", GUILayout.Width(30));
//                if (GUILayout.Button("<", GUILayout.Width(20))) { cat = SizePositionCategory.Size; button = SizePositionButton.SizeRightEdgeLeft; }
//                if (GUILayout.Button(">", GUILayout.Width(20))) { cat = SizePositionCategory.Size; button = SizePositionButton.SizeRightEdgeRight; }
//            }
//            using (new GUILayout.HorizontalScope())
//            {
//                GUILayout.Space(57);
//                if (GUILayout.Button("^", GUILayout.Width(20))) { cat = SizePositionCategory.Size; button = SizePositionButton.SizeBottomUp; }
//            }
//            using (new GUILayout.HorizontalScope())
//            {
//                GUILayout.Space(57);
//                if (GUILayout.Button("v", GUILayout.Width(20))) { cat = SizePositionCategory.Size; button = SizePositionButton.SizeBottomDown; }
//            }
//        }
//        using (new GUILayout.VerticalScope())
//        {
//            GUILayout.Space(14);
//            using (new GUILayout.HorizontalScope())
//            {
//                GUILayout.Space(42);
//                if (GUILayout.Button("^", GUILayout.Width(20))) { cat = SizePositionCategory.Position; button = SizePositionButton.Up; }
//            }
//            GUILayout.Space(10);
//            using (new GUILayout.HorizontalScope())
//            {
//                if (GUILayout.Button("<", GUILayout.Width(20))) { cat = SizePositionCategory.Position; button = SizePositionButton.Left; }
//                GUILayout.Label("Position", GUILayout.Width(50));
//                if (GUILayout.Button(">", GUILayout.Width(20))) { cat = SizePositionCategory.Position; button = SizePositionButton.Right; }
//            }
//            GUILayout.Space(10);
//            using (new GUILayout.HorizontalScope())
//            {
//                GUILayout.Space(42);
//                if (GUILayout.Button("v", GUILayout.Width(20))) { cat = SizePositionCategory.Position; button = SizePositionButton.Down; }
//            }
//        }
//        using (new GUILayout.VerticalScope())
//        {
//            GUILayout.Space(14);
//            using (new GUILayout.HorizontalScope())
//            {
//                GUILayout.Space(42);
//                if (GUILayout.Button("^", GUILayout.Width(20))) { cat = SizePositionCategory.Tiles; button = SizePositionButton.Up; }
//            }
//            GUILayout.Space(10);
//            using (new GUILayout.HorizontalScope())
//            {
//                if (GUILayout.Button("<", GUILayout.Width(20))) { cat = SizePositionCategory.Tiles; button = SizePositionButton.Left; }
//                GUILayout.Label("  Tiles", GUILayout.Width(50));
//                if (GUILayout.Button(">", GUILayout.Width(20))) { cat = SizePositionCategory.Tiles; button = SizePositionButton.Right; }
//            }
//            GUILayout.Space(10);
//            using (new GUILayout.HorizontalScope())
//            {
//                GUILayout.Space(42);
//                if (GUILayout.Button("v", GUILayout.Width(20))) { cat = SizePositionCategory.Tiles; button = SizePositionButton.Down; }
//            }
//        }
//    }
//    GUILayout.Space(10);
//    return button;
//}
// ReSharper disable once FlagArgument
//static ChangeCheck SizeToolsGUI(AnimBool showTools, out SizePositionMod mod, bool roomOption)
//{
//mod.SizeModifier = Vector2Int.zero;
//mod.PosModifier = Vector2Int.zero;
//mod.TileModifier = Vector2Int.zero;
//mod.SpecialCase = false;
//mod.AffectRoom = false;
//using (new GUILayout.VerticalScope(EditorStyles.helpBox, GUILayout.Width(Screen.width - 122)))
//{
//    var show = EditorGUILayout.Foldout(showTools.target, "Size/Position Tools", true, MiniFoldoutStyle);
//    if (show!= showTools.target)
//    {
//        showTools.target = show;
//        EditorPrefs.SetBool(k_editorPrefShowSizeTools, showTools.target);
//    }
//    using (var fade = new EditorGUILayout.FadeGroupScope(showTools.faded))
//    {
//        if (fade.visible)
//        {
//            if (roomOption)
//                SizeToolsRoomOptionGUI(ref mod.AffectRoom);
//            SizeToolsInnerGUI(out var cat, out var button);
//            SizeToolsButtonToData(cat, button, ref mod);
//        }
//    }
//}
//if (Equals(Vector2Int.zero, mod.PosModifier) && (Equals(Vector2Int.zero, mod.SizeModifier)) && (Equals(Vector2Int.zero, mod.TileModifier)))
//return ChangeCheck.NotChanged;
//return ChangeCheck.Changed;
//}
//public static void SizePositionButtons_SceneGUI(SizePositionCategory category, IPlatformLayer layer, Vector3Int roomSize)
//{
//    if (category == SizePositionCategory.None) 
//        return;
//    var bounds = GetBounds(layer);
//    var button = DrawSizePositionButtons(category, bounds);
//    SizePositionMod mod = default;
//    SizeToolsButtonToData(category, button, ref mod);
//    if (button == SizePositionButton.None)
//        return;
//    ApplySizeTool(layer, layer, roomSize, mod);
//}
//static SizePositionButton DrawSizePositionButtons(SizePositionCategory category, Bounds bounds)
//{
//    GetButtonPositions(category, bounds, 
//        out var left, out var right, out var top, out var bottom);
//    var button = SizePositionButton.None;
//    button = Button(SizePositionButton.Left, Color.magenta, button, left, -90f);
//    button = Button(SizePositionButton.Right, Color.red, button, right, 90f);
//    button = Button(SizePositionButton.Up, Color.blue, button, top, 0f);
//    button = Button(SizePositionButton.Down, Color.cyan, button, bottom, 180f);
//    var isSizeTool = category == SizePositionCategory.Size;
//    if (!isSizeTool)
//        return button;
//    GetButton2Positions(left, right, top, bottom,
//        out var left2, out var right2, out var top2, out var bottom2);
//    button = Button(SizePositionButton.SizeRightEdgeLeft, Color.magenta, button, right2, -90f);
//    button = Button(SizePositionButton.SizeLeftEdgeRight, Color.red, button, left2, 90f);
//    button = Button(SizePositionButton.SizeBottomUp, Color.blue, button, bottom2, 0f);
//    button = Button(SizePositionButton.SizeTopDown, Color.cyan, button, top2, 180f);
//    return button;
//}
//static SizePositionButton Button(SizePositionButton button, Color c, SizePositionButton current, Vector3 pos, float yRot)
//{
//    using (new ColorScope(c))
//        if (Handles.Button(pos, Quaternion.Euler(0f, yRot, 0f), k_buttonSize, k_pickSize, Handles.ConeHandleCap))
//            return button;
//    return current;
//}