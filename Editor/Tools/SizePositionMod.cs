﻿using UnityEngine;

namespace Level.Editor.Tools
{
    struct SizePositionMod
    {
        public Vector2Int SizeModifier;
        public Vector2Int PosModifier;
        public Vector2Int TileModifier;
        public bool SpecialCase;
        public bool AffectRoom;
    }
}