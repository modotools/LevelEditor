﻿namespace Level.Editor.Tools
{
    enum SizePositionButton
    {
        None,
        Up,
        Left,
        Right,
        Down,

        SizeTopUp = Up,
        SizeLeftEdgeLeft = Left,
        SizeRightEdgeRight = Right,
        SizeBottomDown = Down,

        SizeTopDown,
        SizeLeftEdgeRight,
        SizeRightEdgeLeft,
        SizeBottomUp,

        Count = SizeBottomUp + 1
    }
}