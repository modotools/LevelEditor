﻿using Level.PlatformLayer.Interface;

namespace Level.Editor.Tools
{
    public struct PlatformSizePositionChanged
    {
        public IPlatformLayerSpatialData Platform;
    }
}