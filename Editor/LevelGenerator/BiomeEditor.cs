﻿using System.Collections.Generic;
using System.Linq;
using Core.Editor.Inspector;
using Core.Unity.Utility.GUITools;
using Level.Generation;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;
using static Core.Editor.Utility.GUIStyles.CustomStyles;
using static UnityEngine.ScriptableObject;

namespace _Editor.LevelGenerator
{
    /// <summary>
    /// Editor for BiomeSetting
    /// </summary>
    [CustomEditor(typeof(BiomeSetting))]
    public class BiomeSettingInspector : BaseInspector<BiomeSettingEditor>
    {
    }
    public class BiomeSettingEditor : BaseEditor<BiomeSetting>
    {
        static readonly string[] k_excludeFromDefaultInspector = { "m_Script" };

        readonly List<System.Type> m_factoryTypes = new List<System.Type>();
        string[] m_factoryTypeNames;

        readonly List<SerializedObject> m_factories = new List<SerializedObject>();

        readonly AnimBool m_showConnectedSpawners = new AnimBool();
        static readonly Color k_factoryTypeColor = new Color(0, 0.95f, 1f);

        void InitFactoryTypes()
        {
            var type = typeof(LevelGeneratorWorkerFactory);
            var types = System.AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p) && type != p);

            m_factoryTypes.Clear();
            m_factoryTypeNames = new string[types.Count()];
            var i = 0;
            foreach (var t in types)
            {
                m_factoryTypes.Add(t);
                m_factoryTypeNames[i] = (t.Name);
                ++i;
            }
        }

        void InitEditorForCurrentSetting()
        {
            foreach (var f in Target.Factories) 
                m_factories.Add(new SerializedObject(f));
        }
        void InitAnimBools()
        {
            m_showConnectedSpawners.valueChanged.RemoveAllListeners();
            m_showConnectedSpawners.valueChanged.AddListener(Repaint);
        }

        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);

            InitAnimBools();
            InitFactoryTypes();
            InitEditorForCurrentSetting();
        }

        public override void OnGUI(float width)
        {
            Width = width;

            base.OnGUI(width);
            
            AddFactoriesGUI();
            AllFactoriesGUI();
            // InitialFactoriesGUI();
        }

        // void InitialFactoriesGUI()
        // {
        //     using var scope = new EditorGUILayout.VerticalScope(EditorStyles.helpBox);
        //     EditorGUILayout.LabelField($"Initial Factories: ");
        //
        //     Target.FallbackFactory = EditorGUILayout.ObjectField(Target.FallbackFactory, 
        //         typeof(WeightedWorkerFactoryConfig), false) as WeightedWorkerFactoryConfig;
        // }
        //
        
        void AllFactoriesGUI()
        {
            using var scope = new EditorGUILayout.VerticalScope(EditorStyles.helpBox);
            EditorGUILayout.LabelField($"Factories: ");
            var factoriesProp = SerializedObject.FindProperty(nameof(BiomeSetting.Factories));

            var deleteIdx = -1;
            for (var fi = 0; fi < m_factories.Count; fi++)
            {
                var fiProp = factoriesProp.GetArrayElementAtIndex(fi);
                using (new EditorGUILayout.HorizontalScope())
                {
                    if (GUILayout.Button("x", GUILayout.Width(20f)))
                        deleteIdx = fi;
                    fiProp.isExpanded = EditorGUILayout.Foldout(fiProp.isExpanded, m_factories[fi].targetObject.name, true,
                        MiniFoldoutStyle);
                }

                if (fiProp.isExpanded)
                    FactoryGUI(fi);
            }

            if (deleteIdx != -1) 
                DeleteFactory(deleteIdx);
        }

        void DeleteFactory(int deleteIdx)
        {
            Target.Factories.RemoveAt(deleteIdx);
            m_factories.RemoveAt(deleteIdx);
            OnChanged();
            AssetDatabase.SaveAssets();
        }

        void AddFactoriesGUI()
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                EditorGUILayout.LabelField($"Add Factory: ");
                var add = EditorGUILayout.Popup(-1, m_factoryTypeNames);
                if (add == -1)
                    return;
                AddFactory(add);
            }
        }

        void AddFactory(int add)
        {
            Target.Factories ??= new List<LevelGeneratorWorkerFactory>();
            var fac = CreateInstance(m_factoryTypes[add]) as LevelGeneratorWorkerFactory;
            Debug.Assert(fac != null);
            Target.Factories.Add(fac);

            AssetDatabase.AddObjectToAsset(fac, Target);
            EditorUtility.SetDirty(Target);
            AssetDatabase.SaveAssets();

            //editors.Add(CreateEditor(fac));
            m_factories.Add(new SerializedObject(fac));
        }

        void FactoryGUI(int facIdx)
        {
            var sObj = m_factories[facIdx];
            var factory = GetFactory(sObj);

            using (var ccs = new EditorGUI.ChangeCheckScope())
            {
                FactoryNameAndTitleGUI(factory);

                if (ccs.changed)
                    OnChanged();
            }

            using (var ccs = new EditorGUI.ChangeCheckScope())
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Space(15f);
                using (new GUILayout.VerticalScope())
                    BaseInspector.DrawPropertiesExcluding(sObj, k_excludeFromDefaultInspector);

                if (ccs.changed)
                {
                    sObj.ApplyModifiedProperties();
                    OnChanged();
                }
            }
        }
        
        void FactoryNameAndTitleGUI(LevelGeneratorWorkerFactory factory)
        {
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label("Name: ", EditorStyles.boldLabel);

                using (var ccs = new EditorGUI.ChangeCheckScope())
                {
                    factory.CustomName = EditorGUILayout.TextField(factory.CustomName);
                    using (new ColorScope(k_factoryTypeColor))
                        EditorGUILayout.LabelField($"{factory.EditorTitle}", EditorStyles.boldLabel);

                    if (!ccs.changed)
                        return;
                    factory.name = factory.CustomName;
                }
            }
        }

        static LevelGeneratorWorkerFactory GetFactory(SerializedObject obj)
            => obj.targetObject as LevelGeneratorWorkerFactory;
    }
}