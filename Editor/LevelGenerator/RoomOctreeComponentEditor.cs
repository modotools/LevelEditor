﻿using Core.Editor.Inspector;
using Core.Extensions;
using Core.Unity.Types;
using Level.Room;
using Level.Generation;
using UnityEditor;
using UnityEngine;

namespace _Editor.LevelGenerator
{
    /// <summary>
    /// Editor for OctreeComponent
    /// </summary>
    [CustomEditor(typeof(RoomOctreeComponent))]
    public class RoomOctreeComponentInspector : BaseInspector<RoomOctreeComponentEditor>
    {
    }
    public class RoomOctreeComponentEditor : BaseEditor<RoomOctreeComponent>
    {
        public override void OnGUI(float width)
        {
            base.OnGUI(width);
            
            Target.Get(out var octree);
            OctreeGUI(octree);
        }
        void OctreeGUI(Octree<IRoomInstance> octree)
        {
            if (octree == null)
                return;

            if (!octree.Editor_Objects.IsNullOrEmpty())
            {
                GUILayout.Label($"Objects: ");

                foreach (var o in octree.Editor_Objects)
                {
                    if (o == null)
                        continue;
                    GUILayout.Label($"{o.gameObject}");
                }
            }

            if (!octree.HasLeaves) 
                return;
            GUILayout.Label($"Leaves: ");
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Space(5f);
                using (new GUILayout.VerticalScope()) 
                    LeafGUI(octree.LeafNodes);
            }
        }

        void LeafGUI(Octree<IRoomInstance>[] octrees)
        {
            foreach (var octree in octrees) 
                OctreeGUI(octree);
        }

    }
}