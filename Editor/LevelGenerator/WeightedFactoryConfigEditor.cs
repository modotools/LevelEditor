﻿using System.Collections.Generic;
using Core.Editor.Inspector;
using Level.Generation;
using UnityEditor;
using UnityEngine;

namespace _Editor.LevelGenerator
{
    [CustomEditor(typeof(WeightedWorkerFactoryConfig))]
    public class WeightedWorkerFactoryConfigInspector : BaseInspector<WeightedWorkerFactoryConfigEditor>
    {
    }
    
    public class WeightedWorkerFactoryConfigEditor : BaseEditor<WeightedWorkerFactoryConfig>
    {
        readonly List<int> m_weights = new List<int>();
        int m_weightSum;
        bool m_selectedForSpawnWeightEdit;

        public override void OnGUI(float width)
        {
            Width = width;
        
            base.OnGUI(width);
            SpawnerGUI();
        }

        void SpawnerGUI()
        {
            var weightSel = m_selectedForSpawnWeightEdit;
        
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Space(15f);
                if (!weightSel && GUILayout.Button("ModifyWeights"))
                    SetupWeightModification();
                else if (weightSel && GUILayout.Button("ApplyWeights"))
                    ApplyWeightModification();
        
                if (weightSel)
                    EditorGUILayout.LabelField("weightSum:" + m_weightSum);
            }

            for (var i = 0; i < Target.WeightedWorkerFactories.Length; i++)
                ConnectedSpawnerGUI(i, weightSel);
        }
 
        void SetupWeightModification()
        {
            m_weights.Clear();
            m_weightSum = 0;
            
            foreach (var factory in Target.WeightedWorkerFactories)
            {
                var w = (int)(factory.Weight * 10000);
                m_weights.Add(w);
                m_weightSum += w;
            }
        
            m_selectedForSpawnWeightEdit = true;
        }
        
        void ApplyWeightModification()
        {
            if (m_weightSum == 0)
                return;
        
            for (var si = 0; si < Target.WeightedWorkerFactories.Length; ++si)
            {
                ref var fac = ref Target.WeightedWorkerFactories[si];
                var chance = (float)m_weights[si] / m_weightSum;
                fac.Weight = chance;
            }
        
            m_selectedForSpawnWeightEdit = false;
        }

        void ConnectedSpawnerGUI(int idx, bool modWeights)
        {
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Space(15f);
                var el = Target.WeightedWorkerFactories[idx];
                var factory = el.Factory;
                var name = factory == null
                    ? "null"
                    : Target.WeightedWorkerFactories[idx].Factory.CustomName;
                EditorGUILayout.LabelField(name, GUILayout.Width(150));

                if (!modWeights)
                {
                    EditorGUILayout.LabelField($"weight: {el.Weight}", GUILayout.Width(50));
                    return;
                }
                
                EditorGUILayout.LabelField("weight: ", GUILayout.Width(50));
                var newWeight = EditorGUILayout.IntField(m_weights[idx]);
                if (m_weightSum != 0)
                    EditorGUILayout.LabelField("p: " + ((float)m_weights[idx] / m_weightSum));
                m_weightSum += newWeight - m_weights[idx];
                m_weights[idx] = newWeight;
            }
        }
    }
}