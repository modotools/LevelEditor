using System;
using UnityEngine;
using UnityEditor;

using System.Collections.Generic;
using System.Linq;
using Core.Editor.Extensions;
using Core.Extensions;
using Core.Unity.Extensions;
using Core.Unity.Utility.Debug;
using Level.Generation;
using Level.Generation.Data;
using Level.Room;
using UnityEditor.AnimatedValues;

using static Level.Generation.Operations.WorkerOperations;
using static Core.Editor.Utility.GUIStyles.CustomStyles;
using static Core.Editor.Utility.GUIStyles.RoundNodes;
using static Core.Utils;

using UEditor = UnityEditor.Editor;
namespace _Editor.LevelGenerator
{
    /// <summary>
    /// Inspector for LevelGenerator
    /// </summary>
    public class LevelGeneratorInspector : EditorWindow
    {
        //static readonly string[] k_excludeFromDefaultInspector = new[] { "m_Script" };

        static LevelGeneratorInspector m_currentEditor;
        static LevelManager m_levelManager;

        UEditor m_levelManagerInspector;
        UEditor m_biomeInspector;

        //bool m_drawOctTree;
        //Color m_octTreeColor;

        bool m_paused;
        //bool m_smallStep = false;

        readonly AnimBool m_showLevelManagerInspector = new AnimBool();
        readonly AnimBool m_showBiomeSettings = new AnimBool();
        readonly AnimBool m_showColorSettings = new AnimBool();

        bool m_showNrAndPos;
        bool m_pauseOnSelection;
        int m_selectedWorker;

        Vector2 m_guiScrollPos;
        Vector2 m_scrollWorkerDebugGUI;

        #region Draw Bounds
        struct DrawBoundsData
        {
            public BoundTagID ID;
            public Bounds Bounds;
        }
        readonly List<DrawBoundsData> m_drawBounds = new List<DrawBoundsData>();

        struct DrawBoundsSetting
        {
            public BoundTagID ID;
            public bool ShouldDraw;
            public Color DrawColor;
        }
        DrawBoundsSetting[] m_drawSettings;

        BoundTagID m_octTreeBoundID;
        BoundTagID m_debugInfoOpBoundID;
        BoundTagID m_debugInfoErrorBoundID;
        BoundTagID m_heroRoomGridPosBoundID;
        BoundTagID m_heroRoomBoundID;
        BoundTagID m_currentGridBoundID;

        #endregion
        #region Editor Prefs
        const string k_editorPref_shouldDrawBoundTagPrefix = "LevelGenerator.ShouldDrawBoundTag";
        const string k_editorPref_boundTagColorPrefix = "LevelGenerator.BoundTagColor";

        const string k_editorPref_showNrAndPos = "LevelGenerator.ShowWorkerNrAndPos";
        const string k_editorPref_pauseOnSelection = "LevelGenerator.PauseOnSelection";

        void InitBoundTagIds()
        {
            var allBoundTagGuids = AssetDatabase.FindAssets($"t:{nameof(BoundTagID)}").Distinct().ToArray();
            var boundTagIds = new List<BoundTagID>();

            foreach (var guid in allBoundTagGuids)
            {
                var objs = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GUIDToAssetPath(guid));
                foreach (var o in objs)
                {
                    if (!(o is BoundTagID boundTagID))
                        continue;
                    if (boundTagIds.Contains(boundTagID))
                        continue;
                    boundTagIds.Add(boundTagID);
                }
            }
            boundTagIds.Sort((a, b) => string.Compare(a.name, b.name,
                StringComparison.Ordinal));
            m_drawSettings = new DrawBoundsSetting[boundTagIds.Count];
            for (var i = 0; i < boundTagIds.Count; i++)
                m_drawSettings[i].ID = boundTagIds[i];
        }

        void LoadPrefs()
        {
            InitBoundTagIds();
            for (var i = 0; i < m_drawSettings.Length; i++)
            {
                m_drawSettings[i].DrawColor = CustomEditorPrefs.GetColor($"{k_editorPref_boundTagColorPrefix}_{i}");
                m_drawSettings[i].ShouldDraw = EditorPrefs.GetBool($"{k_editorPref_shouldDrawBoundTagPrefix}_{i}");
            }

            // m_octTreeColor = CustomEditorPrefs.GetColor(k_editorPref_boundTagOctTreeColor);
            // m_drawOctTree = EditorPrefs.GetBool(k_editorPref_shouldDrawOctTree);
            m_showNrAndPos = EditorPrefs.GetBool(k_editorPref_showNrAndPos);
            m_pauseOnSelection = EditorPrefs.GetBool(k_editorPref_pauseOnSelection);
        }

        void SavePrefs()
        {
            for (var i = 0; i < m_drawSettings.Length; i++)
            {
                CustomEditorPrefs.SetColor($"{k_editorPref_boundTagColorPrefix}_{i}", m_drawSettings[i].DrawColor);
                EditorPrefs.SetBool($"{k_editorPref_shouldDrawBoundTagPrefix}_{i}", m_drawSettings[i].ShouldDraw);
            }

            // EditorPrefs.SetBool(k_editorPref_shouldDrawOctTree, m_drawOctTree);
            // CustomEditorPrefs.SetColor(k_editorPref_boundTagOctTreeColor, m_octTreeColor);
            EditorPrefs.SetBool(k_editorPref_showNrAndPos, m_showNrAndPos);
            EditorPrefs.SetBool(k_editorPref_pauseOnSelection, m_pauseOnSelection);
        }
        #endregion

        #region Unity Calls
        [MenuItem("Tools/LevelGeneratorSettings")]
        public static void OpenWindow()
        {
            var roomEditor = GetWindow(typeof(LevelGeneratorInspector));
            roomEditor.Show();
        }

        void OnEnable()
        {
            //Debug.Log("OnEnable");
            SceneView.duringSceneGui += OnSceneGUI;
            EditorApplication.playModeStateChanged += PlayModeChanged;

            m_currentEditor = this;

            InitAnimBools();
            LoadPrefs();
            InitLevelManager();
        }
        void OnDisable()
        {
            DestroyNestedEditors();

            SceneView.duringSceneGui -= OnSceneGUI;
            LevelManager.AddDrawBoundsEvent -= AddDrawBounds;

            m_currentEditor = null;
        }

        void OnGUI()
        {
            if (m_currentEditor != this || m_levelManager == null)
            {
                //Debug.Log("OnGUI will Init, " + (_CurrentEditor!= this)+" "+(_LevelManager == null));
                InitLevelManager();
                m_currentEditor = this;
                if (m_levelManager == null)
                    return;
            }
            // ReSharper disable once ConvertToUsingDeclaration
            using (var scrollViewScope = new GUILayout.ScrollViewScope(m_guiScrollPos))
            {
                m_guiScrollPos = scrollViewScope.scrollPosition;

                DrawGUI();
            }
        }

        // This will only get called 10 times per second.
        void OnInspectorUpdate() => Repaint();
        #endregion

        void PlayModeChanged(PlayModeStateChange change)
        {
            ClearDrawBounds();

            // todo: just use a toggle to auto-load would be easier
            if (change == PlayModeStateChange.EnteredEditMode && m_levelManager != null)
                m_levelManager.LoadHistoryOnStartPlay(false);
        }

        #region Init/ Release
        void InitAnimBools()
        {
            m_showLevelManagerInspector.valueChanged.RemoveAllListeners();
            m_showLevelManagerInspector.valueChanged.AddListener(Repaint);
            m_showBiomeSettings.valueChanged.RemoveAllListeners();
            m_showBiomeSettings.valueChanged.AddListener(Repaint);
            m_showColorSettings.valueChanged.RemoveAllListeners();
            m_showColorSettings.valueChanged.AddListener(Repaint);
        }

        void InitLevelManager()
        {
            m_levelManager = FindObjectOfType<LevelManager>();

            if (m_levelManager == null)
                return;

            //Debug.Log("Init set _LevelManager.DEBUG_Pause to "+ Paused);
            m_levelManager.DEBUG_Pause = m_paused;

            CacheNestedEditors();

            LevelManager.AddDrawBoundsEvent -= AddDrawBounds;
            LevelManager.AddDrawBoundsEvent += AddDrawBounds;
        }

        void CacheNestedEditors()
        {
            DestroyNestedEditors();

            m_levelManagerInspector = UEditor.CreateEditor(m_levelManager);

            if (m_levelManager.StartingBiome != null)
                m_biomeInspector = UEditor.CreateEditor(m_levelManager.StartingBiome);
        }

        void DestroyNestedEditors()
        {
            if (m_levelManagerInspector != null)
                m_levelManagerInspector.DestroyEx();

            if (m_biomeInspector != null)
                m_biomeInspector.DestroyEx();
        }


        #endregion

        void DrawGUI()
        {
            ColorSelectionGUI();
            OtherSettingsGUI();
            StepControlsGUI();
            LoadSaveHistoryGUI();

            NestedGUI();

            WorkerDebugGUI();
        }

        void NestedGUI()
        {
            NestedLevelManagerGUI();
            NestedBiomeSettingsGUI();
        }

        void NestedBiomeSettingsGUI()
        {
            using (new GUILayout.VerticalScope(EditorStyles.helpBox, GUILayout.Width(position.size.x - 22)))
            {
                m_showBiomeSettings.target = EditorGUILayout.Foldout(m_showBiomeSettings.target,
                    nameof(BiomeSetting), true, MiniFoldoutStyle);

                using (var fade = new EditorGUILayout.FadeGroupScope(m_showBiomeSettings.faded))
                {
                    if (!fade.visible)
                        return;
                    //EditorGUILayout.LabelField("BiomeSetting", EditorStyles.boldLabel);
                    if (m_biomeInspector != null)
                        m_biomeInspector.OnInspectorGUI();
                }
            }
        }

        void NestedLevelManagerGUI()
        {
            using (new GUILayout.VerticalScope(EditorStyles.helpBox, GUILayout.Width(position.size.x - 22)))
            {
                m_showLevelManagerInspector.target = EditorGUILayout.Foldout(m_showLevelManagerInspector.target,
                    nameof(LevelManager), true, MiniFoldoutStyle);

                using (var fade = new EditorGUILayout.FadeGroupScope(m_showLevelManagerInspector.faded))
                {
                    if (!fade.visible)
                        return;
                    EditorGUI.indentLevel++;
                    if (m_levelManagerInspector != null)
                        m_levelManagerInspector.OnInspectorGUI();
                    EditorGUI.indentLevel--;
                }
            }
        }

        void LoadSaveHistoryGUI()
        {
            // ReSharper disable once ConvertToUsingDeclaration
            using (new EditorGUILayout.HorizontalScope())
            {
                if (GUILayout.Button("Save"))
                    m_levelManager.SaveHistory();

                using (new EditorGUI.DisabledScope(EditorApplication.isPlaying || EditorApplication.isPaused))
                {
                    if (GUILayout.Button("Load"))
                        m_levelManager.LoadHistoryOnStartPlay(true);
                }
            }
        }

        void OtherSettingsGUI()
        {
            var generatorData = m_levelManager.DEBUG_GET_GeneratorData();

            using (new EditorGUILayout.HorizontalScope())
            {
                EditorGUILayout.LabelField("Show Worker #Nr & Pos (Scene)", GUILayout.Width(200));
                using (var cs = new EditorGUI.ChangeCheckScope())
                {
                    m_showNrAndPos = EditorGUILayout.Toggle(m_showNrAndPos);
                    if (cs.changed) SavePrefs();
                }
            }

            using (new EditorGUILayout.HorizontalScope())
            {
                EditorGUILayout.LabelField("Save History each iteration", GUILayout.Width(200));
                m_levelManager.DEBUG_SaveHistoryEveryIteration = EditorGUILayout.Toggle(m_levelManager.DEBUG_SaveHistoryEveryIteration);
            }

            using (new EditorGUILayout.HorizontalScope())
            {
                EditorGUILayout.LabelField($"Current Iteration: {m_levelManager.DEBUG_iteration}",
                    GUILayout.Width(200));
                if (generatorData != null)
                    EditorGUILayout.LabelField($"Open Ends: {generatorData.OpenEnds}",
                        GUILayout.Width(200));
            }
            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                using (new EditorGUILayout.HorizontalScope())
                {
                    var enabled = m_levelManager.DEBUG_PauseOnIteration > -1;
                    var nowEnabled = EditorGUILayout.Toggle(enabled, BreakpointToggle, GUILayout.Width(20));
                    if (nowEnabled != enabled)
                        m_levelManager.DEBUG_PauseOnIteration = nowEnabled ? 0 : -1;

                    using (new EditorGUI.DisabledScope(!nowEnabled))
                    {
                        EditorGUILayout.LabelField("on #Iteration:", GUILayout.Width(150));
                        m_levelManager.DEBUG_PauseOnIteration = EditorGUILayout.IntField(m_levelManager.DEBUG_PauseOnIteration);
                    }
                }

                using (new EditorGUILayout.HorizontalScope())
                {
                    var enabled = m_levelManager.DEBUG_PauseOnWorkerNr > -1;
                    var nowEnabled = EditorGUILayout.Toggle(enabled, BreakpointToggle, GUILayout.Width(20));
                    if (nowEnabled != enabled)
                        m_levelManager.DEBUG_PauseOnWorkerNr = nowEnabled ? 0 : -1;

                    using (new EditorGUI.DisabledScope(!nowEnabled))
                    {
                        EditorGUILayout.LabelField("on #Nr:", GUILayout.Width(150));
                        m_levelManager.DEBUG_PauseOnWorkerNr = EditorGUILayout.IntField(m_levelManager.DEBUG_PauseOnWorkerNr);
                    }
                }

                using (new EditorGUILayout.HorizontalScope())
                {
                    using (var cs = new EditorGUI.ChangeCheckScope())
                    {
                        m_pauseOnSelection = EditorGUILayout.Toggle(m_pauseOnSelection, BreakpointToggle, GUILayout.Width(20));
                        if (cs.changed) SavePrefs();
                    }

                    using (new EditorGUI.DisabledScope(!m_pauseOnSelection))
                        EditorGUILayout.LabelField("on Selected Worker--------^^^^", GUILayout.Width(250));
                }
            }
        }

        void WorkerDebugGUI()
        {
            var dat = m_levelManager.DEBUG_GET_GeneratorData();
            if (dat == null)
                return;

            // ReSharper disable once ConvertToUsingDeclaration
            using (var vs = new GUILayout.ScrollViewScope(m_scrollWorkerDebugGUI, EditorStyles.helpBox,
                GUILayout.Height(150)))
            {
                m_scrollWorkerDebugGUI = vs.scrollPosition;

                var workers = dat.Worker;
                var count = dat.Worker.Count;
                // lazy code, change to array
                var worker = new GUIContent[count];

                for (var i = 0; i < count; ++i)
                {
                    var w = workers[i];
                    var workerName = w.Dead ? "DEAD_" : "";
                    workerName += $"<color=green>#{w.WorkerNr}</color> " +
                                  $"<color=cyan>{w.FactoryName}</color> " +
                                  $"<color=blue>{w.TypeName}</color> " +
                                  $" {w.Position}";

                    var iconNam = w.Dead ? w.DeadIconName : w.IconName;
                    const string folderNam = "Assets/Gizmos/LevelGenerator";
                    var icon = (Texture2D)AssetDatabase.LoadMainAssetAtPath($"{folderNam}/{iconNam}");
                    worker[i] = (new GUIContent(workerName, icon));
                }
                var rect = EditorGUILayout.GetControlRect(GUILayout.Width(position.size.x - 20),
                    GUILayout.Height(count * 25));

                var newSelection = GUI.SelectionGrid(rect, m_selectedWorker, worker, 1, SelectableLabel);
                if (newSelection != m_selectedWorker)
                {
                    m_selectedWorker = newSelection;
                    if (m_pauseOnSelection)
                        m_levelManager.DEBUG_PauseOnWorkerNr = workers[m_selectedWorker].WorkerNr;
                }
                if (GUILayout.Button("focus") && workers.IsIndexInRange(m_selectedWorker))
                    SceneView.lastActiveSceneView.LookAt(workers[m_selectedWorker].Position);
            }
        }

        void ColorSelectionGUI()
        {
            using (new GUILayout.VerticalScope(EditorStyles.helpBox, GUILayout.Width(position.size.x - 22)))
            {
                m_showColorSettings.target = EditorGUILayout.Foldout(m_showColorSettings.target,
                    "Color Settings", true, MiniFoldoutStyle);

                using (var fade = new EditorGUILayout.FadeGroupScope(m_showColorSettings.faded))
                {
                    if (!fade.visible)
                        return;

                    for (var i = 0; i < m_drawSettings.Length; ++i)
                    {
                        // ReSharper disable once ConvertToUsingDeclaration
                        using (new GUILayout.HorizontalScope())
                        {
                            var tag = m_drawSettings[i].ID;

                            EditorGUILayout.LabelField(tag.name, GUILayout.Width(200));

                            using (var cs = new EditorGUI.ChangeCheckScope())
                            {
                                m_drawSettings[i].ShouldDraw = EditorGUILayout.Toggle(m_drawSettings[i].ShouldDraw);
                                m_drawSettings[i].DrawColor = EditorGUILayout.ColorField(m_drawSettings[i].DrawColor);

                                if (cs.changed)
                                    SavePrefs();
                            }
                        }
                    }
                }
            }
        }

        void StepControlsGUI()
        {
            // ReSharper disable once ConvertToUsingDeclaration
            using (new GUILayout.HorizontalScope())
            {
                var newPaused = GUILayout.Toggle(m_levelManager.DEBUG_Pause, "||", "Button");

                if (newPaused != m_paused)
                {
                    //Debug.Log("StepControlsGUI set _LevelManager.DEBUG_Pause to " + newPaused);
                    m_levelManager.DEBUG_Pause = newPaused;
                }
                m_paused = newPaused;

                if (GUILayout.Button(">"))
                {
                    ClearDrawBounds();
                    m_levelManager.DEBUG_StepNext = true;
                }
                if (GUILayout.Button(">>"))
                {
                    ClearDrawBounds();
                    m_levelManager.DEBUG_StepAllNext = true;
                }
            }
        }

        void SceneGUI(SceneView sceneView, Event e)
        {
            if (m_levelManager == null)
                return;

            DrawOctTreeBoxLines();
            DrawAllBounds();

            if (m_showNrAndPos)
                ShowWorkerNrAndPositions();
        }

        void ShowWorkerNrAndPositions()
        {
            if (m_levelManager.DEBUG_GET_GeneratorData() == null)
                return;
            var ct = SceneView.currentDrawingSceneView.camera.transform;
            var cp = ct.position;

            var workers = m_levelManager.DEBUG_GET_GeneratorData().Worker;
            for (var i = 0; i < m_levelManager.DEBUG_GET_GeneratorData().Worker.Count; ++i) //(var w in _LevelManager.DEBUG_GET_GeneratorData.Worker)
            {
                var w = workers[i];
                if (!w.HasDebugPos)
                    continue;

                var p = (w.DebugPos - cp).normalized + cp;
                Handles.Label(p - 0.075f * ct.right, $"#{w.WorkerNr}: {w.Position}"); 
            }
        }

        static void OnSceneGUI(SceneView sceneView)
        {
            if (m_currentEditor != null)
                m_currentEditor.SceneGUI(sceneView, Event.current);
        }



        /// <summary>
        /// Draws OctTree-BoxLines
        /// </summary>
        void DrawOctTreeBoxLines()
        {
            var settings = m_drawSettings.FirstOrDefault(a
                => string.Equals(a.ID.name, nameof(BoundTags.OctTree)));

            if (!settings.ShouldDraw)
                return;
            if (m_levelManager == null)
                return;
            
            OctreeInstance.Rooms?.DrawBoxLines(settings.DrawColor);
        }

        void ClearDrawBounds() => m_drawBounds.Clear();
        void DrawAllBounds()
        {
            for (var i = 0; i < m_drawBounds.Count; ++i)
            {
                var id = m_drawBounds[i].ID;
                var setting = m_drawSettings.FirstOrDefault(a => a.ID == id);
                if (!setting.ShouldDraw)
                    continue;
                var dur = m_paused ? 0.0f : 0.3f;
                CustomDebugDraw.DrawBounds(m_drawBounds[i].Bounds, setting.DrawColor, dur);
            }

            DrawDebugInfoBounds();
            DrawDefaultBounds();

            if (!m_paused)
                ClearDrawBounds();
        }

        void DrawDefaultBounds()
        {
            var heroRoomGridPosSettings = m_drawSettings.FirstOrDefault(a =>
                string.Equals(a.ID.name, nameof(BoundTags.HeroRoomGridPos)));
            var heroRoomBoundSettings = m_drawSettings.FirstOrDefault(a =>
                string.Equals(a.ID.name, nameof(BoundTags.HeroRoomBounds)));
            var currentGridBoundSettings = m_drawSettings.FirstOrDefault(a =>
                string.Equals(a.ID.name, nameof(BoundTags.CurrentGridBounds)));

            var genData = m_levelManager.DEBUG_GET_GeneratorData();
            if (NULL.IsAny(m_levelManager, genData))
                return;

            if (genData.HeroRoom == null)
                return;
            genData.HeroRoom.Get(out var heroRoomBounds);
            if (heroRoomGridPosSettings.ShouldDraw)
                CustomDebugDraw.DrawPosition(genData.HeroRoomGridPos, heroRoomGridPosSettings.DrawColor);
            if (heroRoomBoundSettings.ShouldDraw)
                CustomDebugDraw.DrawBounds(heroRoomBounds, heroRoomBoundSettings.DrawColor);
            if (currentGridBoundSettings.ShouldDraw)
                CustomDebugDraw.DrawBounds(genData.CurrentGridBounds, currentGridBoundSettings.DrawColor);
        }

        void DrawDebugInfoBounds()
        {
            var errBounds = DebugInfo.ErrorBounds;
            var opBounds = DebugInfo.OperationBounds;
            if (errBounds.IsNullOrEmpty())
                return;

            var debugInfoOpSettings = m_drawSettings.FirstOrDefault(a =>
                string.Equals(a.ID.name, nameof(BoundTags.DebugInfoOperationBounds)));
            var debugInfoErrSettings = m_drawSettings.FirstOrDefault(a =>
                string.Equals(a.ID.name, nameof(BoundTags.DebugInfoErrorBounds)));

            m_levelManager.DEBUG_Pause = true;
            for (var i = 0; i < errBounds.Count; ++i)
            {
                if (debugInfoOpSettings.ShouldDraw)
                    CustomDebugDraw.DrawBounds(opBounds[i], debugInfoOpSettings.DrawColor);
                if (debugInfoErrSettings.ShouldDraw)
                    CustomDebugDraw.DrawBounds(errBounds[i], debugInfoOpSettings.DrawColor);
            }
        }

        void AddDrawBounds(BoundTags boundTag, Bounds currentExitBounds)
        {
            var setting = m_drawSettings.FirstOrDefault(a => string.Equals(a.ID.name, boundTag.ToString()));

            m_drawBounds.Add(new DrawBoundsData()
            {
                ID = setting.ID,
                Bounds = currentExitBounds
            });
        }
    }
}