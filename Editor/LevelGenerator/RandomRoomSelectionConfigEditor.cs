﻿using System.Collections.Generic;
using Core.Editor.Inspector;
using Level.Room;
using Level.Generation;
using UnityEditor;
using UnityEngine;

namespace _Editor.LevelGenerator
{
    /// <summary>
    /// Editor for BiomeSetting
    /// </summary>
    [CustomEditor(typeof(RandomRoomSelectionConfig))]
    public class RandomRoomSelectionConfigInspector : BaseInspector<RandomRoomSelectionConfigEditor>
    {
    }
    public class RandomRoomSelectionConfigEditor : BaseEditor<RandomRoomSelectionConfig>
    {
        readonly List<int> m_weights = new List<int>();
        int m_weightSum;
        bool m_weightEdit;

        public override void OnGUI(float width)
        {
            Width = width;

            // base.OnGUI(width);
            EditorGUILayout.LabelField($"Templates: ");
            TemplatesGUI();
        }

        void TemplatesGUI()
        {
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Space(15f);
                if (!m_weightEdit && GUILayout.Button("ModifyWeights"))
                    SetupWeightModification();
                else if (m_weightEdit && GUILayout.Button("ApplyWeights"))
                    ApplyWeightModification();

                if (m_weightEdit)
                    EditorGUILayout.LabelField("weightSum:" + m_weightSum);
            }

            var removeIdx = -1;
            for (var cf = 0; cf < Target.Editor_Templates.Count; ++cf)
                TemplateGUI(cf, m_weightEdit, ref removeIdx);

            if (removeIdx != -1)
            {
                Target.Editor_SpawnChances.RemoveAt(removeIdx);
                Target.Editor_Templates.RemoveAt(removeIdx);
                m_weightEdit = false;
                OnChanged();
            }

            AddTemplateGUI();
        }
        
        void AddTemplateGUI()
        {
            var add = GUILayout.Button($"+", GUILayout.Width(25f));
            if (!add)
                return;
            Target.Editor_Templates.Add(null); 
            Target.Editor_SpawnChances.Add(0);
            OnChanged();
        }

        void SetupWeightModification()
        {
            m_weights.Clear();
            m_weightSum = 0;
            foreach (var sp in Target.Editor_SpawnChances)
            {
                var w = (int)(sp * 10000);
                m_weights.Add(w);
                m_weightSum += w;
            }
            m_weightEdit = true;
        }

        void ApplyWeightModification()
        {
            if (m_weightSum == 0)
                return;

            for (var si = 0; si < Target.Editor_SpawnChances.Count; ++si)
            {
                var chance = (float)m_weights[si] / m_weightSum;
                Target.Editor_SpawnChances[si] = chance;
            }
            m_weightEdit = false;
            OnChanged();
        }

        void TemplateGUI(int idx, bool modWeights, ref int removeIdx)
        {
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Space(15f);
                if (GUILayout.Button("X", GUILayout.Width(20)))
                    removeIdx = idx;

                Target.Editor_Templates[idx] = (RoomConfig) EditorGUILayout.ObjectField(Target.Editor_Templates[idx], 
                    typeof(RoomConfig), false, GUILayout.Width(150));

                if (modWeights)
                {
                    EditorGUILayout.LabelField("weight: ", GUILayout.Width(50));
                    var newWeight = EditorGUILayout.IntField(m_weights[idx]);
                    if (m_weightSum != 0)
                        EditorGUILayout.LabelField("p: " + ((float)m_weights[idx] / m_weightSum));
                    m_weightSum += newWeight - m_weights[idx];
                    m_weights[idx] = newWeight;
                }
                else EditorGUILayout.LabelField($"{Target.Editor_SpawnChances[idx]}");
            }
        }
    }
}