﻿using System.Collections.Generic;
using System.Linq;
using Core.Editor.Inspector;
using Level.Editor.Room.Operations;
using Level.Generation;
using Level.Room;
using UnityEditor;
using UnityEngine;

namespace _Editor.LevelGenerator
{
    /// <summary>
    /// Editor for LevelManager
    /// </summary>
    [CustomEditor(typeof(LevelManager))]
    public class LevelManagerInspector : BaseInspector<LevelManagerEditor>
    {
    }
    public class LevelManagerEditor : BaseEditor<LevelManager>
    {
        readonly List<RoomInstanceComponent> m_previousInitRooms = new List<RoomInstanceComponent>();
        readonly List<RoomInstanceComponent> m_nowInitRooms = new List<RoomInstanceComponent>();
        
        public override void OnGUI(float width)
        {
            Width = width;
            
            PrevInitAll(out var prevInitAll);

            using var check =new EditorGUI.ChangeCheckScope();
            base.OnGUI(width);

            if (!check.changed)
                return;
            
            UpdateHighlighter(prevInitAll);
        }
        
        void UpdateHighlighter(bool prevInitAll)
        {
            SerializedObject.ApplyModifiedProperties();
            m_nowInitRooms.Clear();
            if (!Target.Editor_InitWithAllRoomsInScene)
                m_nowInitRooms.AddRange(Target.Editor_InitialRooms);
            if (prevInitAll != Target.Editor_InitWithAllRoomsInScene)
            {
                if (prevInitAll)
                    m_previousInitRooms.AddRange(Object.FindObjectsOfType<RoomInstanceComponent>());
                else
                    m_nowInitRooms.AddRange(Object.FindObjectsOfType<RoomInstanceComponent>());
            }

            foreach (var newRoomLoaded in m_nowInitRooms.Where(a => a != null && !m_previousInitRooms.Contains(a)))
                newRoomLoaded.UpdateHighlighter(true);
            foreach (var notLoaded in m_previousInitRooms.Where(a => a != null && !m_nowInitRooms.Contains(a)))
                notLoaded.UpdateHighlighter(false);
        }

        void PrevInitAll(out bool prevInitAll)
        {
            m_previousInitRooms.Clear();
            prevInitAll = Target.Editor_InitWithAllRoomsInScene;
            if (!Target.Editor_InitWithAllRoomsInScene && Target.Editor_InitialRooms!= null)
                m_previousInitRooms.AddRange(Target.Editor_InitialRooms);
        }
    }
}