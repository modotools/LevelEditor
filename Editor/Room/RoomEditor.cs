using System;

using Core.Editor.Inspector;
using Core.Editor.Interface;
using Core.Editor.PopupWindows;
using Core.Editor.Utility;
using Core.Editor.Utility.GUITools;
using Core.Extensions;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Utility.GUITools;
using Level.Editor.Room.Editors;
using Level.Editor.Room.States;
using Level.Editor.Room.Operations;
using Level.Room;
using Level.Room.Operations;
using Level.Generation.Operations;
using ScriptableUtility;

using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

using static Core.Editor.Utility.CustomGUI;
using static Core.Extensions.CollectionExtensions;
using static Core.Unity.Operations.SceneRootOperations;
using static Level.Editor.Settings.LevelEditorSettingsAccess;

using Color = UnityEngine.Color;
using Event = UnityEngine.Event;
using Core.Unity;
using Core.Unity.Utility;
using Level.Editor.Settings;
using Level.Objects;
using LevelEditor;
using UnityEditor.SceneManagement;
using RoomOperations = Level.Room.Operations.RoomOperations;

namespace Level.Editor.Room
{
    public class RoomEditor : StateEditor
    {
        #region Properties
        public override string Name => StaticName;
        public static string StaticName => "Room Editor";

        public static RoomEditor CurrentEditor { get; private set; }

        public IRoomInstance RoomInstance => m_instanceComponent;
        public ref RoomInstanceData RoomInstanceData => ref m_instanceComponent.Data;

        public bool RoomInitialized => m_instanceComponent != null 
                                       && RoomInstanceData.GameObject != null;

        public GameObject CurrentRoomGo => m_instanceComponent == null ? null : RoomInstanceData.GameObject;

        protected override Type ThisType => typeof(RoomEditor);

        RoomBuilder Builder => !RoomInitialized ? null 
            : RoomInstanceData.RoomConfig != null 
                ? RoomInstanceData.RoomConfig.Builder 
                : null;
        bool BuildValid => m_instanceComponent != null && Builder != null;
        RoomBuilder_RoomPreview Preview
        {
            get => m_preview;
            set
            {
                m_preview?.Terminate();
                m_preview = value;
            }
        }
        string SceneRoomPath
        {
            get
            {
                var scene = SceneManager.GetActiveScene();
                var scenePath = scene.path;
                var sceneAlreadyInSubfolder = scenePath.Contains($"{scene.name}/");
                var roomPath = sceneAlreadyInSubfolder
                    ? scenePath.Replace($"/{scene.name}.unity", "/Rooms/")
                    : scenePath.Replace(".unity", "/Rooms/");

                return roomPath;
            }
        }

        #endregion

        #region Editor Prefs
        static readonly string k_editorPref_editorState = $"{nameof(RoomEditor)}.{nameof(m_state)}";
        #endregion

        RoomEditor_HeaderState m_header;
        RoomInstanceComponent m_instanceComponent;
        RoomBuilder_RoomPreview m_preview;

        RoomInstanceComponent m_lastSelected;

        enum PlacingContext
        {
            Scene,
            CustomStage
        }
        PlacingContext m_placingContext;
        string m_createRoomPath;

        static RoomConfig m_loadRoomConfig;
        internal static Vector3Int m_initialPosition;
        
        #region Init & Destroy
        public override void Init(object parentContainer)
        {
            CurrentEditor = this;
            m_header = new RoomEditor_HeaderState(this);
            m_header.Init(this);

            base.Init(parentContainer);

            var stateIdx = EditorPrefs.GetInt(k_editorPref_editorState, 0);

            if (stateIdx.IsInRange(m_editorStates) && m_editorStates[stateIdx].IsApplicable)
                m_state = m_editorStates[stateIdx];

            if (RoomInitialized)
                RoomInstanceData.UpdateVisuals();
        }

        public override void Terminate()
        {
            base.Terminate();
            CurrentEditor = null;
        }

        public override void UndoRedoPerformed()
        {
            base.UndoRedoPerformed();
            //Debug.Log("UndoRedoPerformed");
            if (m_instanceComponent != null)
                m_instanceComponent.UpdateExits();

            RoomEditor_Platforms.Current?.ClampCurrentLevel();
            UpdateAndBuildPreview();
        }

        public void SetInstance(RoomInstanceComponent @value)
        {
            var wasBuilding = SceneRoomManagement.IsBuildingRooms;
            if (wasBuilding)
                SceneRoomManagement.AbortBuildingRooms();

            var go = value.gameObject;
            Selection.activeGameObject = go;

            var context = StageUtility.GetStage(go) == StageUtility.GetMainStage()
                ? PlacingContext.Scene
                : PlacingContext.CustomStage; 
            
            m_placingContext = context;
            m_instanceComponent = @value;
            if (m_instanceComponent != null 
                && m_instanceComponent.TryGetComponent(out ContextComponent cc))
            {
                //Debug.Log($"SetInstance Init");
                cc.Init();
            }

            UpdateAndBuildPreview();
            m_initialPosition = m_instanceComponent.Position;
            // Debug.Log($"set m_initialPosition {m_initialPosition}");
            if (wasBuilding)
                SceneRoomManagement.StartBuildAllRooms();
        }

        public void LoadRoomConfig(RoomConfig rc)
        {
            m_placingContext = PlacingContext.CustomStage;
            LevelObjectManagement.PlacingContext = ObjectContextMarker.ObjectPlacingContext.Room;
            
            CreateRoomWithConfig(rc);
            UpdateAndBuildPreview();
        }

        public void CloseCurrent()
        {
            m_instanceComponent = null;
            m_state = m_editorStates[0];
            UpdatePreview();
            EditorRoomFlagFromRoomPath();

            if (m_placingContext == PlacingContext.CustomStage)
            {
                StageUtility.GoBackToPreviousStage();
            }
        }

        const string k_undoKey_destroyRoom = "Destroy Room";
        void CloseAndDestroyCurrent()
        {
            Undo.SetCurrentGroupName(k_undoKey_destroyRoom);
            // without these two lines object will be invisible on undo:
            Undo.RecordObject(RoomInstanceData.GameObject, k_undoKey_destroyRoom);
            RoomInstanceData.GameObject.SetActive(false);
            // --

            RoomInstanceData.Destroy(true);
            m_state = m_editorStates[0];
        }

        #endregion

        #region GUI
        public override void MainEditorGUI(float width)
        {
            base.MainEditorGUI(width);

            CurrentEditor = this;
            m_header.OnGUI(width);

            if (!RoomInitialized)
            {
                NoRoomGUI(width);

                if (!RoomInitialized)
                    return;
            }

            RoomGUI(width);

            if (Preview != null && Preview.IsDirty)
                Preview.Build();
        }

        void CurrentRoomSelectionGUI()
        {
            GetRoomInstanceFromSelection(out var selection, out var rc);
            m_lastSelected = rc;

            var selectAvailable = (rc != null && CurrentRoomGo != selection);
            if (selectAvailable && ButtonOrReturnKey($"Select {selection}"))
                SetInstance(rc);
        }

        void NoRoomGUI(float width)
        {

            RoomConfig r = null;
            using (new GUILayout.VerticalScope("Room Instance:", EditorStyles.helpBox, GUILayout.Width(width - 22)))
            {
                GUILayout.Space(15);

                var sceneRootGo = GetSceneRoot(SceneManager.GetActiveScene(), out _);
                if (sceneRootGo == null)
                {
                    EditorGUILayout.HelpBox("No Scene Root!", MessageType.Warning);
                    if (GUILayout.Button("Create Scene Root"))
                        new GameObject("_Scene", typeof(SceneRoot));
                    return;
                }

                RoomPathGUI();

                var verification = VerificationResult.Default;
                var ok = RoomSettingsOK(ref verification);
                if (!ok)
                    BaseEditor.VerificationGUI(verification);

                using (new GUILayout.HorizontalScope())
                {
                    CurrentRoomSelectionGUI();
                    
                    using (new EditorGUI.DisabledGroupScope(!ok))
                    {
                        var rd = RoomEditor_Properties.NewRoomConfigData;

                        if (GUILayout.Button($"Create Room '{rd.Name}'"))
                        {
                            r = rd.CreateRoomAsset(RoomDefaultPath);
                            // r.UpdateResulting(0);
                            r.RoomConfigData = rd; // Global.Resources.Instance.StandardLevelSetting;
                        }
                    }

                    if (GUILayout.Button("Load Room >> ")) 
                        r = m_loadRoomConfig;
                    m_loadRoomConfig = EditorGUILayout.ObjectField(m_loadRoomConfig, typeof(RoomConfig), false) as RoomConfig;
                }
            }

            CreateRoomWithConfig(r);
            if (m_instanceComponent != null && m_placingContext == PlacingContext.Scene)
            {
                // Debug.Log($"Applying initial position {m_initialPosition}");
                m_instanceComponent.transform.position = m_initialPosition;
            }   
            UpdateAndBuildPreview();
        }

        enum PathType
        {
            Default,
            Scene,
            Custom
        }
        
        void RoomPathGUI()
        {
            GetPathType(out var pathType, out var pathInfo);
            using (new GUILayout.HorizontalScope())
            {
                if (ActivityButton(pathType == PathType.Default, "Default Room Path"))
                {
                    m_createRoomPath = RoomDefaultPath;
                    EditorRoomFlagFromRoomPath();
                }
                if (ActivityButton(pathType == PathType.Scene, "Scene Room Path"))
                {
                    m_createRoomPath = SceneRoomPath;
                    EditorRoomFlagFromRoomPath();
                }
                if (ActivityButton(pathType == PathType.Custom, "Custom Path"))
                    m_createRoomPath = EditorUtility.OpenFolderPanel("Room Path",
                        RoomDefaultPath, "") + "/";
            }
            GUILayout.Label($"Path: " + m_createRoomPath);
            EditorGUILayout.HelpBox(pathInfo, MessageType.Info);
            
            using (new EditorGUILayout.HorizontalScope())
            {
                if (ActivityButton(m_placingContext == PlacingContext.Scene, "Place In Scene"))
                    m_placingContext = PlacingContext.Scene;
                if (ActivityButton(m_placingContext == PlacingContext.CustomStage, "Separate Context"))
                    m_placingContext = PlacingContext.CustomStage;
            }
            EditorGUILayout.HelpBox(m_placingContext == PlacingContext.CustomStage ? 
                "Work on the room in a separate context.": 
                "The room is placed in the scene", MessageType.Info);
            if (m_placingContext == PlacingContext.Scene) 
                m_initialPosition = EditorGUILayout.Vector3IntField("position: ", m_initialPosition);
        }

        void GetPathType(out PathType pathType, out string pathInfo)
        {
            if (m_createRoomPath.IsNullOrEmpty())
            {
                m_createRoomPath = RoomDefaultPath;
                EditorRoomFlagFromRoomPath();
            }
            
            var isDefaultPath = string.Equals(m_createRoomPath, RoomDefaultPath, StringComparison.Ordinal);
            var isScenePath = string.Equals(m_createRoomPath, SceneRoomPath, StringComparison.Ordinal);
            pathType = isDefaultPath ? PathType.Default : isScenePath ? PathType.Scene : PathType.Custom;
            pathInfo = null;
            switch (pathType)
            {
                case PathType.Default:
                    pathInfo = "The room will be saved in the Default location for room configs. (ProjectSettings/ Level Editor Settings) \n " +
                               "Use this option to reuse this room, f.e. in procedural generation."; break;
                case PathType.Scene:
                    pathInfo = "The room will be saved in a folder next to the scene.\n " +
                               "Use this option when this room is unique to this scene and placed/ saved into this scene"; break;
                case PathType.Custom:
                    pathInfo = "The room is saved in a custom location."; break;
            }
        }
        
        void EditorRoomFlagFromRoomPath()
        {
            var isScenePath = string.Equals(m_createRoomPath, SceneRoomPath, StringComparison.Ordinal);
            m_placingContext = isScenePath? PlacingContext.Scene: PlacingContext.CustomStage;
        }

        static bool RoomSettingsOK(ref VerificationResult r)
        {
            var rd = RoomEditor_Properties.NewRoomConfigData;
            if (rd.Name.IsNullOrEmpty())
            {
                r.Error("Name is null or empty!", null);
                return false;
            }            
            if (rd.Settings == null)
            {
                r.Error("Settings not set!", null);
                return false;
            }   
            rd.Settings.Editor_Verify(ref r);
            return r.Errors <= 0 && r.Warnings <= 0;
        }

        void RoomGUI(float width)
        {
            m_instanceComponent.UpdatePosition();

            using (new GUILayout.VerticalScope("Room Instance:", EditorStyles.helpBox, GUILayout.Width(width - 22)))
            {
                GUILayout.Space(15);

                var config = m_instanceComponent.Config;
                var configPath = AssetDatabase.GetAssetPath(config);

                UnsavedInSceneGUI(configPath, config);

                GUILayout.Label($"Path: " + configPath);

                using (new GUILayout.HorizontalScope())
                {
                    CurrentRoomSelectionGUI();

                    var savTx = PackageUtil.Load($"Icons/save.png") as Texture2D;
                    var closeTx = PackageUtil.Load($"Icons/close.png") as Texture2D;
                    var killTx = PackageUtil.Load($"Icons/kill.png") as Texture2D;

                    if (GUILayout.Button(new GUIContent(" Save Room", savTx), GUILayout.Width(100)))
                        SaveRoom();
                    if (GUILayout.Button(new GUIContent(" Close", closeTx), GUILayout.Width(100)))
                        CloseCurrent();

                    var closeDestroyLocked = new GUIContent(" Close & Destroy", killTx);
                    var closeDestroyUnlocked = new GUIContent("Sure? ", killTx);

                    if (SafetyButton("RoomEditor.Close_Destroy",
                        new CustomGUIData(closeDestroyLocked, Color.gray), 
                        new CustomGUIData(closeDestroyUnlocked, Color.yellow), 
                        this, GUILayout.Width(150)))
                        CloseAndDestroyCurrent();
                }

                InitVisibleGUI();
            }
            GUILayout.Space(15);

            // LevelGUI();
            using (new GUILayout.VerticalScope("Editing Mode:", EditorStyles.helpBox, GUILayout.Width(width - 22)))
            {
                GUILayout.Space(15.0f);

                EditorStateButtonsGUI();
            }
        }

        void InitVisibleGUI()
        {
            if (m_instanceComponent== null)
                return;
            var visible = m_instanceComponent.Editor_InitVisibleOnStart;
            if (m_placingContext == PlacingContext.Scene)
            {
                if (RoomEditor_Scene.Current.LevelManager == null)
                {
                    visible = EditorGUILayout.Toggle("Visible on Play", visible);
                    if (!visible)
                    {
                        if (ParentContainer is EditorState editorState) editorState.Expand(RoomEditor_Scene.Current);
                        EditorGUILayout.HelpBox(
                            "Room will not be visible on pressing play. Consider using a Level Manager.",
                            MessageType.Warning);
                    } 
                }
                else if (!RoomEditor_Scene.Current.LevelManager.Editor_IsInitVisible(m_instanceComponent))
                {
                    EditorGUILayout.HelpBox("Room will not be visible on pressing play. Check your Level Manager.",
                        MessageType.Info);
                }
                // m_instanceComponent.
            }
            else visible = false;

            if (m_instanceComponent.Editor_InitVisibleOnStart == visible) 
                return;
            m_instanceComponent.Editor_InitVisibleOnStart = visible;
            m_instanceComponent.UpdateHighlighter(visible);
        }


        void UnsavedInSceneGUI(string configPath, RoomConfig config)
        {
            var prevCtx = m_placingContext;
            using (new EditorGUILayout.HorizontalScope())
            {
                if (ActivityButton(m_placingContext == PlacingContext.Scene, "Place In Scene"))
                    m_placingContext = PlacingContext.Scene;
                if (ActivityButton(m_placingContext == PlacingContext.CustomStage, "Separate Context"))
                    m_placingContext = PlacingContext.CustomStage;
            }

            if (m_placingContext == prevCtx)
                return;

            var instanceGo = m_instanceComponent.gameObject;

            if (m_placingContext == PlacingContext.CustomStage)
                PlaceCustomStage(instanceGo);
            else
            {
                var sceneRootGo = GetSceneRoot(SceneManager.GetActiveScene(), out _);
                instanceGo.transform.SetParent(sceneRootGo.transform);
                StageUtility.GoBackToPreviousStage();
                StageUtility.PlaceGameObjectInCurrentStage(instanceGo);
                InitVisible(m_instanceComponent);
            }

            var unsaved = m_placingContext == PlacingContext.CustomStage;
            if (unsaved && instanceGo.TryGetComponent(out RoomObjectsDataComponent roomObjects))
                    roomObjects.LvlObjectsConfigScene = null;
            
            var supposedPath = m_placingContext == PlacingContext.CustomStage 
                ? RoomDefaultPath 
                : SceneRoomPath;
            if (configPath.Contains(supposedPath))
                return;

            var move = EditorUtility.DisplayDialog(
                "Move?",
                $"Move config to path {supposedPath}? (current path: {configPath})",
                "Move",
                "Do Not Move");
            
            if (!move)
                return;

            //var filePathName = $"{supposedPath}{config.name}.asset";
            Operations.RoomOperations.MoveRoomAsset(config, supposedPath);
        }

        void EditorStateButtonsGUI()
        {
            using (new GUILayout.HorizontalScope())
            {
                for (var i = 0; i < m_editorStates.Length; ++i)
                {
                    if (!m_editorStates[i].IsApplicable)
                        continue;

                    if (!ActivityButton(m_state == m_editorStates[i], m_editorStates[i].Name, true, null,
                        GUILayout.Width(100)))
                        continue;

                    m_state = m_editorStates[i];
                    EditorPrefs.SetInt(k_editorPref_editorState, i);
                }
            }
        }
        #endregion

        #region SceneGUI
        public override void OnSceneGUI(SceneView sceneView)
        {
            var e = Event.current;

            var keyType = e.type == EventType.KeyDown || e.type == EventType.KeyUp;
            if (keyType && e.keyCode == KeyCode.Escape)
            {
                e.Use();
                m_state = m_editorStates[0];
                EditorPrefs.SetInt(k_editorPref_editorState, 0);

                Repaint();
            }

            GetRoomInstanceFromSelection(out var selection, out var rc);
            if (rc != m_lastSelected)
            {
                m_lastSelected = rc;
                Repaint();
            }

            var selectAvailable = (rc != null && CurrentRoomGo != selection);
            // Debug.Log(selectAvailable);

            if (selectAvailable && 
                e.type == EventType.KeyDown && e.keyCode == KeyCode.Return)
            {
                e.Use();
                SetInstance(rc);
                Repaint();
            }

            base.OnSceneGUI(sceneView);

            OnSceneGUI(m_header, sceneView);

            if (m_instanceComponent == null)
                return;

            m_instanceComponent.UpdatePosition();
            
            if (m_state is RoomEditor_DefaultState)
            {
                if (DrawCreateRoomButtons(RoomInstanceData.Bounds, out var newPos))
                {
                    var popup = new NamePopup((name) => CurrentEditor.CreateRoom(newPos, (string) name));
                    PopupWindow.Show(new Rect(Event.current.mousePosition, NamePopup.WindowSize), popup);
                }
            }
            if (m_instanceComponent.transform.hasChanged) 
                RoomInstanceData.UpdateVisuals();

            if (Preview != null && Preview.IsDirty)
                Preview.Build();
        }

        static bool DrawCreateRoomButtons(Bounds bounds, out Vector3Int newPos)
        {
            newPos = default;

            var minX = bounds.min.x;
            var maxX = bounds.max.x;
            var minZ = bounds.min.z;
            var maxZ = bounds.max.z;
            var c = bounds.center;

            const float size = 0.5f;
            const float pickSize = 0.75f;
            const float offset = 2f;
            var l = new Vector3(minX - offset, c.y, c.z);
            var r = new Vector3(maxX + offset, c.y, c.z);
            var t = new Vector3(c.x, c.y, maxZ + offset);
            var b = new Vector3(c.x, c.y, minZ - offset);

            using (new ColorScope(Color.white))
            {
                var sv = SceneView.currentDrawingSceneView;
                CustomHandles.DrawText(l, "+", Color.white);
                //Handles.Label(l-txtOffset, "+");
                if (Handles.Button(l, Quaternion.Euler(90f, -90f, 0f), size, pickSize, Handles.RectangleHandleCap))
                {
                    var posVec = bounds.min - new Vector3(bounds.size.x, 0, 0);
                    newPos = posVec.Vector3Int();
                    return true;
                }
                CustomHandles.DrawText(r, "+", Color.white);
                if (Handles.Button(r, Quaternion.Euler(90f, 90f, 0f), size, pickSize, Handles.RectangleHandleCap))
                {
                    var posVec = bounds.min + new Vector3(bounds.size.x, 0, 0);
                    newPos = posVec.Vector3Int();
                    return true;
                }
                CustomHandles.DrawText(t, "+", Color.white);
                if (Handles.Button(t, Quaternion.Euler(90f, 0f, 0f), size, pickSize, Handles.RectangleHandleCap))
                {
                    var posVec = bounds.min + new Vector3(0, 0, bounds.size.z);
                    newPos = posVec.Vector3Int();
                    return true;
                }
                CustomHandles.DrawText(b, "+", Color.white);
                if (Handles.Button(b, Quaternion.Euler(90f, 180f, 0f), size, pickSize, Handles.RectangleHandleCap))
                {
                    var posVec = bounds.min - new Vector3(0, 0, bounds.size.z);
                    newPos = posVec.Vector3Int();
                    return true;
                }
            }

            return false;
        }

        
        static void GetRoomInstanceFromSelection(out GameObject selection, out RoomInstanceComponent rc)
        {
            rc = null;
            selection = Selection.activeGameObject;
            if (selection == null)
                return;

            selection.TryGetComponent(out rc);
            if (rc != null 
                || selection.layer != RoomEditor_Properties.DefaultRoomGroundLayer) 
                return;
            // selected had DefaultRoomGroundLayer, try get parent room then
            while (rc == null && selection.transform.parent != null)
            {
                selection = selection.transform.parent.gameObject;
                selection.TryGetComponent(out rc);
            }
        }
        #endregion

        #region Utility
        #region Create, Load, Save
        void CreateRoom(Vector3Int pos, string name)
        {
            var rd = RoomEditor_Properties.NewRoomConfigData;
            rd.Size = RoomInstanceData.Size;
            rd.Name = name;
            // Debug.Log(m_createRoomPath);
            var r = rd.CreateRoomAsset(m_createRoomPath);
            CreateRoomWithConfig(r);
            RoomInstanceData.Transform.position = pos;
            RoomInstanceData.UpdateRoomPosition(pos);
            RoomInstanceData.UpdateVisuals();

            UpdateAndBuildPreview();
        }

        void CreateRoomWithConfig(RoomConfig r)
        {
            if (r == null)
                return;

            var sceneRootGo = GetSceneRoot(SceneManager.GetActiveScene(), out _);
            if (sceneRootGo == null)
            {
                Debug.LogError("No Scene Root!");
                return;
            }
            var go = new GameObject(r.RoomConfigData.Name, typeof(ContextComponent),
                typeof(RoomInstanceComponent));

            if (m_placingContext == PlacingContext.Scene)
                go.transform.SetParent(sceneRootGo.transform);
            // typeof(CleanupMarker)) { hideFlags = HideFlags.DontSave };

            Selection.activeGameObject = go;
            if (!go.TryGetComponent(out RoomInstanceComponent rc)
                || !go.TryGetComponent(out ContextComponent cc))
                return;

            if (m_placingContext == PlacingContext.CustomStage) 
                PlaceCustomStage(go);
            
            //LevelObjectManagement.InitScene_GatherRooms();

            m_instanceComponent = rc;
            rc.SetData(new RoomInstanceData(r, go));
            Debug.Assert(cc.Editor_Containers != null, "Editor_Containers null");
            Debug.Assert(rc.Data.RoomConfig.Builder != null, "Builder null");
            Add(ref cc.Editor_Containers, 
                new RefIVariableContainer(){ Result = rc.Data.RoomConfig.Builder });
            cc.Init();

            var roomPl = RoomEditor_Platforms.Current;
            roomPl?.ClampCurrentLevel();
            UpdatePreview();
            Undo.RegisterCreatedObjectUndo(go, "Create Room");

            if (m_placingContext == PlacingContext.Scene) 
                InitVisible(rc);

            Repaint();
            SceneView.RepaintAll();
        }

        void InitVisible(IRoomInstance rc)
        {
            var visible = false;
            if (RoomEditor_Scene.Current.LevelManager != null)
                visible = RoomEditor_Scene.Current.LevelManager.Editor_IsInitVisible(rc);
            m_instanceComponent.UpdateHighlighter(visible);
        }

        static void PlaceCustomStage(GameObject go)
        {
            var stage = ScriptableObject.CreateInstance<CustomStage>();
            stage.TitleContent = new GUIContent
            {
                text = go.name,
                image = EditorGUIUtility.IconContent("GameObject Icon").image
            };

            StageUtility.GoToStage(stage, true);
            stage.SetupStage(go);
        }

        void SaveRoom()
        {
            var r = RoomInstanceData.RoomConfig;
            var path = AssetDatabase.GetAssetPath(r);
            if (path.IsNullOrEmpty())
            {
                path = EditorUtility.SaveFilePanel("Save Room", "Rooms", r.name + ".asset", "asset");
                if (!path.IsNullOrEmpty())
                {
                    path = path.Replace(Application.dataPath, "Assets");
                    AssetDatabase.CreateAsset(r, path);
                    AssetDatabase.SaveAssets();
                    return;
                }
            }

            EditorUtility.SetDirty(r);
            AssetDatabase.SaveAssets();
        }
        #endregion

        public void UpdateAndBuildPreview()
        {
            if (RoomInitialized)
                RoomInstanceData.UpdateVisuals();

            UpdatePreview();
            if (RoomInitialized)
                Preview?.Build();
        }

        public void UpdatePreview()
        {
            if (!RoomInitialized)
                return;
            var builder = m_preview?.Builder;
            var instance = m_preview?.RoomInstance;

            // nothing changed
            if (builder == Builder && m_instanceComponent == instance)
                return;

            Preview = BuildValid
                ? new RoomBuilder_RoomPreview(m_instanceComponent) 
                : null;
        }
        #endregion


    }
}
