﻿using System;
using Core.Editor.Attribute;
using Core.Editor.Interface;
using Level.Room;
using UnityEditor;
using UnityEditor.ShortcutManagement;

namespace Level.Editor.Room.States
{
    [ParentEditor(typeof(RoomEditor))]
    public class RoomEditor_World : EditorState
    {
        protected override Type ThisType => typeof(RoomEditor_World);

        [Shortcut("RoomEditor.World")]
        public static void SetActive()
        {
            RoomEditor.CurrentEditor?.SetState(typeof(RoomEditor_World));
            RoomEditor.CurrentEditor?.Repaint();
        }

        protected override void HeaderGUI(float width)
        {
            EditorGUILayout.LabelField("World");
        }

        public override string Name => "World";
        public override Type ParentEditor => typeof(RoomEditor);

        public override bool IsApplicable => RoomEditor.CurrentEditor != null
                                             && RoomEditor.CurrentEditor.RoomInitialized &&
                                             OctreeInstance.Rooms != null;
    }
}
