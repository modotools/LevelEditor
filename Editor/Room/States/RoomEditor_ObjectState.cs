﻿using System;
using Core.Editor.Attribute;
using Core.Editor.Interface;
using UnityEditor;
using UnityEngine;

namespace Level.Editor.Room.States
{
    [ParentEditor(typeof(RoomEditor))]
    public class RoomEditor_ObjectState : EditorState
    {
        protected override Type ThisType => typeof(RoomEditor_ObjectState);

        protected override void HeaderGUI(float width)
        {
            EditorGUILayout.LabelField("Objects");
        }

        public override string Name => "Objects";
        public override Type ParentEditor => typeof(RoomEditor);
        public override bool IsApplicable => RoomEditor.CurrentEditor != null 
                                             && RoomEditor.CurrentEditor.RoomInitialized;
    }
}
