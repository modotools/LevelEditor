﻿using System;
using Core.Editor.Attribute;
using Core.Editor.Interface;
using UnityEditor;
using UnityEngine;

namespace Level.Editor.Room.States
{
    [ParentEditor(typeof(RoomEditor))]
    public class RoomEditor_DefaultState : EditorState
    {
        protected override Type ThisType => typeof(RoomEditor_DefaultState);

        protected override void HeaderGUI(float width)
        {
            EditorGUILayout.LabelField("Default");
        }

        public override string Name => "Default";
        public override Type ParentEditor => typeof(RoomEditor);
        public override bool IsApplicable => RoomEditor.CurrentEditor != null 
                                             && RoomEditor.CurrentEditor.RoomInitialized;
    }
}
