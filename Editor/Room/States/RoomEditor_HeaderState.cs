using System;
using Core.Editor.Interface;
using UnityEngine;

namespace Level.Editor.Room.States
{
    public class RoomEditor_HeaderState : EditorState
    {
        public RoomEditor_HeaderState(IEditor parent) 
            => SetParent(parent);

        protected override Type ThisType => typeof(RoomEditor_HeaderState);

        protected override void HeaderGUI(float width)
        {
        }

        public override string Name => "Header";
        public override Type ParentEditor => typeof(RoomEditor);
        public override bool IsApplicable => true;
    }
}
