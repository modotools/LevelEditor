﻿using System;
using Core.Editor.Attribute;
using Core.Editor.Interface;
using UnityEditor;
using UnityEngine;
using UnityEditor.ShortcutManagement;

namespace Level.Editor.Room.States
{
    [ParentEditor(typeof(RoomEditor))]
    public class RoomEditor_TilesState : EditorState
    {
        protected override Type ThisType => typeof(RoomEditor_TilesState);

        [Shortcut("RoomEditor.TilesState")]
        public static void SetActive()
        {
            RoomEditor.CurrentEditor?.SetState(typeof(RoomEditor_TilesState));
            RoomEditor.CurrentEditor?.Repaint();
        }

        protected override void HeaderGUI(float width)
        {
            EditorGUILayout.LabelField("Tiles");
        }

        public override string Name => "Tiles";
        public override Type ParentEditor => typeof(RoomEditor);
        public override bool IsApplicable => RoomEditor.CurrentEditor != null 
                                             && RoomEditor.CurrentEditor.RoomInitialized;
    }
}
