﻿using Core.Editor.Inspector;
using Level.Room;
using Level.Room.Operations;
using UnityEditor;
using UnityEngine;

namespace Level.Editor.Room
{
    [CustomEditor(typeof(RoomInstanceComponent))]
    public class RoomInstance_Inspector : BaseInspector<RoomInstanceEditor>{}
    public class RoomInstanceEditor: BaseEditor<RoomInstanceComponent>
    {
        public override void OnTargetChanged()
        {
            base.OnTargetChanged();
            Target.Data.UpdateVisuals();
        }

        public override void OnGUI(float width)
        {
            using (new EditorGUI.DisabledScope(true))
                base.OnGUI(width);

            Target.UpdatePosition();

            if (RoomEditor_Window.IsOpen(Target))
            {
                if (GUILayout.Button("Close in Room Editor")) 
                    RoomEditor_Window.CloseCurrentRoom();
            }
            else
            {
                Target.Data.RoomConfig = (RoomConfig) EditorGUILayout.ObjectField("Room Config: ", Target.Data.RoomConfig, 
                    typeof(RoomConfig), false);
                if (GUILayout.Button("Open in Room Editor")) 
                    RoomEditor_Window.Open(Target);
            }

        }
    }
}