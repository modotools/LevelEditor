﻿using System.Linq;
using Core.Editor.Inspector;
using Core.Editor.Utility;
using Level.Room;
using UnityEditor;
using UnityEngine;
using static Level.Editor.Room.Operations.RoomOperations;

namespace Level.Editor.Room
{
    [CustomEditor(typeof(RoomConfig))]
    public class RoomConfig_Inspector : BaseInspector<RoomConfigEditor>
    {
    }

    public class RoomConfigEditor : BaseEditor<RoomConfig>
    {
        public override void OnGUI(float width)
        {
            base.OnGUI(width);
            if (Target.PlatformLayer.Contains(null) && GUILayout.Button("Fix Null Entries"))
                FixNullEntriesInPlatformLayer();

            if (GUILayout.Button("Fix Names of child assets")) 
                FixNames();

            if (GUILayout.Button("Open"))
                RoomEditor_Window.Open(Target);
        }

        void FixNames()
        {
            for (var i = 0; i < Target.PlatformLayerConfigs.Count; i++)
            {
                var platform = Target.PlatformLayerConfigs[i];
                platform.name = $"{Target.name} lvl {i}";
            }

            Target.RoomObjects.name = $"{Target.name}_lvlObjects";
            OnChanged();
            Target.Reimport();
        }

        void FixNullEntriesInPlatformLayer()
        {
            for (var i = 0; i < Target.PlatformLayerConfigs.Count; i++)
            {
                if (Target.PlatformLayerConfigs[i] == null)
                    AddPlatformLayerConfig(Target, i, Target.RoomConfigData);
            }
        }
        
        [UnityEditor.Callbacks.OnOpenAsset(1)]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            if (Selection.activeObject is RoomConfig rc) {
                RoomEditor_Window.Open(rc);
                return true; //catch open file
            }        
     
            return false; // let unity open the file
        }
    }
}