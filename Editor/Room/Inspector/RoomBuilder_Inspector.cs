using Core.Editor.Inspector;
using Level.PlatformLayer;
using Level.Room;
using ScriptableUtility;
using ScriptableUtility.Editor.Actions;
using UnityEditor;
using UnityEngine;

namespace Level.Editor.Room
{
    [CustomEditor(typeof(RoomBuilder))]
    public class RoomBuilder_Inspector : BaseInspector<RoomBuilderEditor>{}
    public class RoomBuilderEditor : BaseEditor<RoomBuilder>
    {
        public override void SetTarget(RoomBuilder t, SerializedObject s)
        {
            if(t == Target)
                return;
            base.SetTarget(t, s);
            UpdatePreview();
        }

        Vector2 m_scrollPos;

        PlatformLayerConfig m_previewLayer;

        bool BuildValid => Target != null &&
            m_previewLayer != null;

        bool PreviewPlatform => ParentContainer is RoomBuilder_Inspector;

        RoomBuilder_PlatformPreview Preview
        {
            get => m_preview;
            set
            {
                m_preview?.Terminate();
                m_preview = value;
            }
        }

        RoomBuilder_PlatformPreview m_preview;
        #region UnityMethods
        public override void Terminate()
        {
            base.Terminate();
            Preview?.Terminate();
        }

        public override void OnGUI(float width)
        {
            if (base.Target == null)
                return;
            base.OnGUI(width);

            using (var scroll = new EditorGUILayout.ScrollViewScope(m_scrollPos))
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_scrollPos = scroll.scrollPosition;
                EditorGUILayout.HelpBox("A RoomBuilder is a set of action to generate a Room", MessageType.Info);

                if (PreviewPlatform)
                {
                    m_previewLayer = (PlatformLayerConfig) EditorGUILayout.ObjectField("Preview w. platform", m_previewLayer, 
                        typeof(PlatformLayerConfig), false);
                }

                if (check.changed) 
                    UpdatePreview();
            }

            using (new EditorGUI.DisabledGroupScope(!BuildValid))
            {
                if (GUILayout.Button("Build")) 
                    Preview?.Build();
            }
        }
        #endregion

        void UpdatePreview()
        {
            var builder = m_preview?.Builder;
            var conf = m_preview?.PlatformLayerConfig;

            // nothing changed
            if (builder == Target && m_previewLayer == conf)
                return;

            Preview = BuildValid 
                ? new RoomBuilder_PlatformPreview(Target, m_previewLayer, PreviewPlatform) 
                : null;
        }
        public void SetInputData(GameObject go, PlatformLayerConfig plc, Mesh m, IContext c)
        {
            m_previewLayer = plc;
            UpdatePreview();

            if (!BuildValid)
                return;
            m_preview.SetInputData(go, m, c);
        }
    }
}