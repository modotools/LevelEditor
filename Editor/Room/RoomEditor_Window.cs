﻿using Core.Types;
using Level.Room;
using UnityEditor;

namespace Level.Editor.Room
{
    public class RoomEditor_Window : EditorWindow
    {
        static RoomEditor m_roomEditor;

        bool m_sceneViewWasMax;

        static SceneView m_sceneView;

        static SceneView CurrentSceneView
        {
            get
            {
                if (m_sceneView != null)
                    return m_sceneView;
                m_sceneView = GetWindow<SceneView>();
                return m_sceneView;
            }
        }

        bool SceneViewMaxJustChanged
        {
            get
            {
                if (EditorApplication.isPlayingOrWillChangePlaymode)
                    return false;
                var returnVal = CurrentSceneView.maximized != m_sceneViewWasMax;
                m_sceneViewWasMax = CurrentSceneView.maximized;
                return returnVal;
            }
        }

        public bool IsSubstituteWindow;

        [MenuItem("Tools/RoomEditor")]
        public static void OpenWindow()
        {
            var roomEditor = GetWindow(typeof(RoomEditor_Window), false, RoomEditor.StaticName, true);
            roomEditor.Show();
        }
        public static void Open(RoomConfig rc)
        {
            OpenWindow();
            m_roomEditor.LoadRoomConfig(rc);
        }
        
        public static void Open(RoomInstanceComponent ric)
        {
            OpenWindow();
            m_roomEditor.SetInstance(ric);
        }

        public static bool IsOpen(RoomInstanceComponent ric)
        {
            if (m_roomEditor != null)
                return m_roomEditor.CurrentRoomGo == ric.gameObject;
            return false;
        }
        public static void CloseCurrentRoom() => m_roomEditor?.CloseCurrent();

        void OnEnable()
        {
            if (m_roomEditor != null) 
                return;
            m_roomEditor = new RoomEditor();
            m_roomEditor.Init(this);
        }

        void OnDisable()
        {
            if (OnMaximizeSubstituteWindow() == FlowResult.Stop) 
                return;
            if (IsSubstituteWindow)
                return;

            m_roomEditor.Terminate();
            m_roomEditor = null;
        }

        void OnGUI()
        {
            if (m_roomEditor == null)
                return;


            if (CloseSubstituteWhenNotMax() == FlowResult.Stop) 
                return;
            
            m_roomEditor.OnGUI(position.width);
        }
        
        void Update()
        {
            if (m_roomEditor == null)
                return;

            if (CloseSubstituteWhenNotMax() == FlowResult.Stop) 
                return;

            m_roomEditor.Update();
        }


        FlowResult OnMaximizeSubstituteWindow()
        {
            if (!SceneViewMaxJustChanged 
                || IsSubstituteWindow) 
                return FlowResult.Continue;
            if (!CurrentSceneView.maximized) 
                return FlowResult.Continue;
            if (EditorApplication.isPlayingOrWillChangePlaymode)
                return FlowResult.Continue;

            var roomEditor = CreateInstance<RoomEditor_Window>();
            roomEditor.name = RoomEditor.StaticName + "Substitute";
            roomEditor.ShowUtility();
            roomEditor.IsSubstituteWindow = true;
            return FlowResult.Stop;
        }
        FlowResult CloseSubstituteWhenNotMax()
        {
            if (!SceneViewMaxJustChanged || CurrentSceneView.maximized || !IsSubstituteWindow)
                return FlowResult.Continue;

            Close();
            return FlowResult.Stop;
        }

    }
}
