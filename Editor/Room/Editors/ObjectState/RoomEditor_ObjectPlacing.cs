﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Editor.Attribute;
using Core.Editor.Inspector;
using Core.Editor.Interface;
using Core.Editor.PopupWindows;
using Core.Editor.Utility;
using Core.Editor.Utility.GUITools;
using Core.Extensions;
using Core.Types;
using Core.Unity.Extensions;
using Core.Unity.Utility.Debug;
using Level.Editor.Room.States;
using JetBrains.Annotations;
using Level.Editor.Data;
using Level.Objects;
using Level.PlatformLayer;
using Level.Room;
using ScriptableUtility;
using ScriptableUtility.Behaviour;
using ScriptableUtility.Editor.Inspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using static Core.Editor.Utility.GUITools.CommonGUIOperations;
using static Level.PlatformLayer.Operations;
using Color = UnityEngine.Color;

using static Level.Editor.Room.LevelObjectManagement;
using static Level.Editor.Room.LevelObjects_SceneContext;

namespace Level.Editor.Room.Editors
{
    [EditorState(typeof(RoomEditor_ObjectState), 10)]
    [UsedImplicitly]
    public class RoomEditor_ObjectPlacing : IEditor, IEditorExtras, ISceneGUICallable
    {
        #region Custom Editor Data Definitions
        struct CursorObjectData
        {
            public GameObject GameObject;
            public ObjectContextMarker ObjectContextMarker;
            public IActionBehaviour ActionBehaviour;
        }

        struct ObjectActionData
        {
            public GameObject ObjectInstance;
            public ObjectActionsConfig ObjectActionsConfig;
        }
        #endregion
        #region constants
        static Color ActionColor => new Color(1f, 0.15f, 0f, 1f);
        #endregion

        #region Properties
        #region IEditor, IEditorExtras
        public string Name => "Object Mode";
        public object ParentContainer { get; private set; }
        public bool ShowAlways => true;
        public bool ShowLabel => false;
        #endregion

        static bool RoomInitialized => RoomEditor.CurrentEditor?.RoomInitialized?? false;

        static RoomInstanceData RoomInstanceData => RoomEditor.CurrentEditor?.RoomInstanceData?? default;
        static IRoomInstance RoomInstance => RoomEditor.CurrentEditor?.RoomInstance;

        static PlatformLayerConfig LayerConfig => RoomEditor_Platforms.Current.CurrentPlatformLayer;

        static RoomObjectsDataComponent RoomObjectsDataComponent
        {
            get
            {
                if (!RoomInstanceData.GameObject.TryGet<RoomObjectsDataComponent>(out var sceneRoomObjectsData))
                    sceneRoomObjectsData = RoomInstanceData.GameObject.AddComponent<RoomObjectsDataComponent>();
                return sceneRoomObjectsData;
            }
        }
        #endregion

        #region Member
        Vector2Int m_cursor;
        //ObjectContextMarker.ObjectPlacingContext m_ctx;

        CursorObjectData[] m_cursorObjects = new CursorObjectData[0];

        ContextAsset m_objectContext;
        GameObject m_selectedGo;
        ActionsConfigEditor m_editor;
        LevelObjectsConfigEditor m_objectsEditor;

        readonly Dictionary<LevelObjectsConfig, SerializedObject> m_levelObjectConfigToSerialized = 
            new Dictionary<LevelObjectsConfig, SerializedObject>();
        #endregion

        #region Init
        public void Init(object parentContainer)
        {
            ParentContainer = parentContainer;
            EditorApplication.hierarchyChanged += OnHierarchyChanged;
        } 

        public void Terminate() => EditorApplication.hierarchyChanged -= OnHierarchyChanged;
        public void SetActive(bool active) { }

        #endregion

        #region GUI

        public void OnGUI(float width)
        {
            //Debug.Log(RoomPlatformsEditor.Current.CurrentLevel);
            var sceneValid = SceneValid(out _);
            if (!sceneValid)
                EditorGUILayout.HelpBox("Scene is not valid. Please make sure that the scene is saved!", MessageType.Info);

            using (new EditorGUILayout.HorizontalScope())
            {
                using (new EditorGUI.DisabledScope(!sceneValid))
                    ContextButtonGUI(ObjectContextMarker.ObjectPlacingContext.Scene);

                ContextButtonGUI(ObjectContextMarker.ObjectPlacingContext.Room);
            }

            DrawLvlObjectsConfig(width);

            EditorGUILayout.LabelField($"Objects at Cursor {m_cursor}:");

            CursorObjectGUI(width);
        }

        
        void DrawLvlObjectsConfig(float width)
        {
            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                var config = PlacingContext == ObjectContextMarker.ObjectPlacingContext.Scene
                    ? RoomObjectsDataComponent.LvlObjectsConfigScene
                    : RoomObjectsDataComponent.LvlObjectsConfigRoom;

                if (config == null)
                {
                    GUILayout.Label($"Config is null! {PlacingContext}");
                    return;
                }
                m_objectsEditor ??= new LevelObjectsConfigEditor();
                if (m_objectsEditor.Target != config)
                    m_objectsEditor.SetTarget(config, new SerializedObject(config));
                // UnityEditor.Editor.CreateCachedEditor(config, null, ref m_objectsEditor);
                m_objectsEditor?.OnGUI(width);
            }
        }

        void ContextButtonGUI(ObjectContextMarker.ObjectPlacingContext ctx)
        {
            if (!CustomGUI.ActivityButton(PlacingContext == ctx, ctx.ToString())) 
                return;

            Selection.activeGameObject = null;
            PlacingContext = ctx;
        }

        void CursorObjectGUI(float width)
        {
            for (var i = 0; i < m_cursorObjects.Length; i++)
            {
                if (m_cursorObjects[i].GameObject == null)
                    continue;
                CursorObjectGUI(width, ref m_cursorObjects[i]);
            }
        }

        void CursorObjectGUI(float width, ref CursorObjectData cursorObjData)
        {
            var cursorGo = cursorObjData.GameObject;
            var selected = cursorGo == m_selectedGo;

            if (!CursorObjectValidGUI(cursorObjData, out var ctxMarker, out var lvlObjectsConfig, out var objIdx)) 
                return;

            var hasActionBehaviour = cursorObjData.ActionBehaviour != null;
            var hasActionsDefined = ctxMarker.Actions != null;
            //ref var objData = ref lvlObjectsConfig.Editor_ObjectData[objIdx];

            using (new GUILayout.HorizontalScope())
            {
                CursorObjectSelectionButtonGUI(cursorGo, hasActionBehaviour, hasActionsDefined, selected);
                GUILayout.Label(PlacingContext.ToString(), EditorStyles.toolbarButton);
            }

            if (selected) 
                SelectedCursorObjectGUI(width, lvlObjectsConfig, objIdx, 
                    hasActionsDefined, hasActionBehaviour);
        }

        void SelectedCursorObjectGUI(float width, LevelObjectsConfig lvlObjectsConfig, int objIdx, 
            bool hasActionsDefined, bool hasActionBehaviour)
        {
            SpawnConditionsGUI(lvlObjectsConfig, objIdx);

            if (!hasActionsDefined)
            {
                if (hasActionBehaviour && GUILayout.Button("Add Actions")) 
                    AddOrSelectActionsConfig();
            }
            else
                DrawActionStatesGUI(width);

            //using (new EditorGUI.DisabledScope(!hasActionBehaviour))
            //using (new ActivityFoldoutScope(ref selected, "Actions", EditorStyles.toolbarButton))
            //{
            //    DrawActionStatesGUI(width);
            //}
        }

        void SpawnConditionsGUI(LevelObjectsConfig lvlObjectsConfig, int objIdx)
        {
            GetCachedSerializedObject(lvlObjectsConfig, out var serializedObject);
            serializedObject.Update();
            var editorObjProp = serializedObject.FindProperty(LevelObjectsConfig.Editor_ObjectDataPropName);
            var elementProp = editorObjProp.GetArrayElementAtIndex(objIdx);
            var spawnConditionsProp = elementProp.FindPropertyRelative(nameof(LevelObjectsConfig.ObjectData.SpawnConditions));
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(spawnConditionsProp, true);

                if (check.changed)
                    serializedObject.ApplyModifiedProperties();
            }
        }

        void CursorObjectSelectionButtonGUI(GameObject cursorGo, bool hasActionBehaviour, bool hasActionsDefined, bool selected)
        {
            var content = GetCursorGoButtonContent(cursorGo, hasActionBehaviour, hasActionsDefined);
            var clicked = CustomGUI.ActivityButton(selected, content, false, EditorStyles.toolbarButton);
            if (!clicked) 
                return;

            if (selected)
                Deselect();
            else
                SelectGameObject(cursorGo);
        }
        
        static GUIContent GetCursorGoButtonContent(GameObject cursorGo, bool hasActionBehaviour, bool hasActionsDefined)
        {
            Texture2D tex = null;

            if (hasActionsDefined)
            {
                FindGUIDFromIconName("icon-action", out var guid);
                tex = AssetDatabase.LoadAssetAtPath<Texture2D>(AssetDatabase.GUIDToAssetPath(guid));
            }
            else if (hasActionBehaviour)
            {
                //todo: different icon
                FindGUIDFromIconName("icon-ctx", out var guid);
                tex = AssetDatabase.LoadAssetAtPath<Texture2D>(AssetDatabase.GUIDToAssetPath(guid));
            }

            var content = new GUIContent($"{cursorGo.name}", tex);
            return content;
        }

        bool CursorObjectValidGUI(CursorObjectData cursorObjData,
            out ObjectContextMarker ctxMarker,
            out LevelObjectsConfig lvlObjectsConfig,
            out int objectIdx)
        {
            var cursorGo = cursorObjData.GameObject;
            ctxMarker = cursorObjData.ObjectContextMarker;
            lvlObjectsConfig = null;
            objectIdx = -1;

            if (ctxMarker == null)
            {
                EditorGUILayout.HelpBox("ObjectContextMarker is missing!!", MessageType.Error);
                return false;
            }

            var placingCtx = ctxMarker.PlacingContext;
            var operationResult = GetLevelObjectsConfig(placingCtx, RoomInstance, out lvlObjectsConfig);
            if (operationResult == OperationResult.Error)
            {
                EditorGUILayout.HelpBox($"Could not get {nameof(LevelObjectsConfig)} for {placingCtx}!!", MessageType.Error);
                return false;
            }

            objectIdx = Array.FindIndex(lvlObjectsConfig.Editor_ObjectData, od => od.Editor_Instance == cursorGo);
            if (objectIdx == -1)
            {
                EditorGUILayout.HelpBox($"Could not find {cursorGo} in {nameof(LevelObjectsConfig)} of {placingCtx}!!",
                    MessageType.Error);
                return false;
            }

            return true;
        }

        void DrawActionStatesGUI(float width) => m_editor.OnGUI(width);
        #endregion

        #region Scene GUI
        public void OnSceneGUI(SceneView sceneView)
        {
            if (AvoidSceneGUIConflicts(out var e) == FlowResult.Stop) 
                return;

            KeyHandling(sceneView, e);
            DrawCursor();
            DrawActionIcons();
        }

        void DrawActionIcons()
        {
            foreach (var objectAction in AllObjectsActions())
            {
                if (objectAction.ObjectActionsConfig == null || objectAction.ObjectInstance == null)
                    continue;

                FindGUIDFromIconName("icon-actionmap", out var guid);
                var tex = AssetDatabase.LoadAssetAtPath<Texture2D>(AssetDatabase.GUIDToAssetPath(guid));

                var center = objectAction.ObjectInstance.transform.position;
                var w = 0.5f * tex.width;
                var scrPos = CustomHandles.CalculateScreenPosition(center, out var z);
                var wPos = CustomHandles.ScreenToWorldPosition(scrPos + new Vector2(-w, 1.5f * w), z);

                Handles.Label(wPos, new GUIContent() {image = tex});

                var tileBounds = GetTileBounds(center);
                CustomDebugDraw.DrawBounds(tileBounds, ActionColor, 0f);
            }
        }

        void DrawCursor()
        {
            CustomDebugDraw.DrawBounds(CursorBounds(m_cursor), Color.white);
        }

        void KeyHandling(SceneView sceneView, Event e)
        {
            if (!e.isKey || e.type != EventType.KeyDown) 
                return;
            
            if (e.keyCode == KeyCode.Return)
            {
                e.Use();

                OnEnterPressed();
                return;
            }
            if (e.keyCode == KeyCode.Backspace)
            {
                e.Use();

                OnBackSpacePressed();
                return;
            }
            var moveKey = e.keyCode.IsAny(KeyCode.LeftArrow, KeyCode.RightArrow, KeyCode.DownArrow, KeyCode.UpArrow,
                KeyCode.W, KeyCode.A, KeyCode.S, KeyCode.D);

            if (!moveKey) 
                return;

            var moveVector = Vector2.zero;
            if (e.keyCode == KeyCode.LeftArrow || e.keyCode == KeyCode.A)
                moveVector.x += -1;
            else if (e.keyCode == KeyCode.RightArrow || e.keyCode == KeyCode.D)
                moveVector.x += 1;
            else if (e.keyCode == KeyCode.DownArrow || e.keyCode == KeyCode.S)
                moveVector.y += -1;
            else if (e.keyCode == KeyCode.UpArrow || e.keyCode == KeyCode.W)
                moveVector.y += 1;

            var ang = VectorExtensions.AngleBetween(sceneView.camera.transform.forward.Vector2(), Vector2.up);

            var newCursor = m_cursor + moveVector.Rotate(ang).Vector2Int();
            var b = GetBounds(LayerConfig);

            if (b.GetIntersectionType(CursorBounds(newCursor)) == IntersectionType.Contains)
                m_cursor = newCursor;
            else if (b.GetIntersectionType(CursorBounds(m_cursor)) != IntersectionType.Contains)
                m_cursor = Vector2Int.zero;

            UpdateCursorObjects();
            Deselect();

            e.Use();
            Repaint();
            EditorApplication.QueuePlayerLoopUpdate();
        }


        void OnEnterPressed()
        {
            //Debug.Log("OnEnterPressed");
            if (m_selectedGo == null)
            {
                SelectFromAvailable();
                return;
            }

            var objCtxMarker = m_selectedGo.Get<ObjectContextMarker>();
            var actionBehaviour = m_selectedGo.Get<ActionBehaviour>();

            if (objCtxMarker.Actions == null)
            {
                if (actionBehaviour != null)
                    AddOrSelectActionsConfig();
            }
            else
            {
                m_editor?.OnEnterPressed();
                Repaint();
            }
        }

        void OnBackSpacePressed()
        {
            //Debug.Log("OnBackSpacePressed");

            if (m_editor != null && m_editor.IsStateActive())
            {
                m_editor.DeselectState();
                Repaint();
                return;
            }
            //Debug.Log("Deselect");

            Deselect();
        }

        void SelectFromAvailable()
        {            
            //Debug.Log("SelectFromAvailable");

            var availableObjects = m_cursorObjects.Where(co => co.ActionBehaviour != null);
            if (availableObjects.Count() > 1)
            {
                var popup = new SelectObjectPopup((dta) => SelectGameObject((GameObject) dta),
                    availableObjects.Select(o => o.GameObject).ToArray());

                PopupWindow.Show(new Rect(Event.current.mousePosition, Vector2.one), popup);
            }
            else if (availableObjects.Any())
                SelectGameObject(availableObjects.FirstOrDefault().GameObject);

            Repaint();
        }
        #endregion

        #region Actions
        void Deselect()
        {
            m_selectedGo = null;
            Repaint();
        }

        void SelectGameObject(GameObject go)
        {
            m_selectedGo = go;
            if (m_objectContext != null)
                m_objectContext.SetContextLink(m_selectedGo);

            //Debug.Log($"OnEnterActionBehaviourObject {selected}");

            var ab = go.Get<ActionBehaviour>();
            if (ab != null) 
                SelectActionsConfigIfAvailable();
        }

        void SelectActionsConfigIfAvailable()
        {
            var oad = AllObjectsActions().FirstOrDefault(oa => oa.ObjectInstance == m_selectedGo);
            if (oad.ObjectActionsConfig != null)
            {
                var marker = oad.ObjectInstance.Get<ObjectContextMarker>();
                var operationResult = GetLevelObjectsConfig(marker.PlacingContext, RoomInstance, out var lvlObjectsConfig);
                if (operationResult == OperationResult.Error)
                    return;
                m_objectContext = lvlObjectsConfig
                    .ChildAssets().FirstOrDefault(s => s is ContextAsset ca
                                                       && string.Equals(ca.name, "Object")) as ContextAsset;
                if (m_objectContext == null)
                    return;
                m_objectContext.SetContextLink(m_selectedGo);
                oad.ObjectActionsConfig.Editor_LinkContextAssets(m_objectContext, lvlObjectsConfig);
            }

            BaseEditor.CreateEditor(oad.ObjectActionsConfig, ref m_editor);
            m_editor?.Init(this);
            Repaint();
        }

        void AddOrSelectActionsConfig()
        {
            //Debug.Log("AddOrSelectActionsConfig");

            var oad = AllObjectsActions().FirstOrDefault(oa => oa.ObjectInstance == m_selectedGo);
            ObjectActionsConfig config;
            if (oad.ObjectActionsConfig == null)
            {
                if (AddActionsConfig(out config) == OperationResult.Error) 
                    return;
            }
            else 
                config = oad.ObjectActionsConfig;

            BaseEditor.CreateEditor(config, ref m_editor);
            m_editor.Init(this);

            Repaint();
        }

        void UpdateLevelObjects()
        {
            if (GetLevelObjectsConfigForScene(RoomInstance, out var sceneObjConfig) == OperationResult.OK)
                UpdateLevelObjects(sceneObjConfig, SceneContext.SceneRoot.gameObject);
            
            if (GetLevelObjectsConfigForRoom(RoomInstance, out var roomObjectsConfig) == OperationResult.OK)
                UpdateLevelObjects(roomObjectsConfig, RoomInstanceData.GameObject);
        }

        static void UpdateLevelObjects(LevelObjectsConfig lvlObjectsConfig, GameObject parent)
        {
            if (parent == null 
                || lvlObjectsConfig == null 
                || lvlObjectsConfig.Editor_ObjectData == null)
                return;

            var parentTr = parent.transform;

            for (var i = 0; i < lvlObjectsConfig.Editor_ObjectData.Length; i++)
            {
                ref var objData = ref lvlObjectsConfig.Editor_ObjectData[i];
                var inst = objData.Editor_Instance;
                if (inst == null)
                    continue;

                objData.LocalPosition = parentTr.InverseTransformPoint(inst.transform.position);
                objData.LocalRotation = (Quaternion.Inverse(parentTr.rotation) * inst.transform.rotation).eulerAngles;
                objData.Scale = inst.transform.lossyScale;
            }
        }
        #endregion

        #region Utility, Helper
        void Repaint()
        {
            if (ParentContainer is IRepaintable r)
                r.Repaint();
        }

        static Bounds CursorBounds(Vector2Int cursor)
        {
            var v3Cursor = new Vector3Int((int) (cursor.x + LayerConfig.PositionOffset.x), (int) LayerConfig.PositionOffset.y,
                (int) (cursor.y + LayerConfig.PositionOffset.z));

            var size = RoomInstanceData.RoomConfig.Grid.TileSize3D;
            return new Bounds(v3Cursor + 0.5f * size, size);
        }

        static Bounds GetTileBounds(Vector3 pos)
        {
            var size = RoomInstanceData.RoomConfig.Grid.TileSize3D;
            size.y = 0.05f;
            var flooredPos = new Vector3(Mathf.FloorToInt(pos.x), Mathf.FloorToInt(pos.y), Mathf.FloorToInt(pos.z));

            return new Bounds(flooredPos + 0.5f * size, size);
        }

        void UpdateCursorObjects()
        {
            using (var cursorObjListScoped = SimplePool<List<CursorObjectData>>.I.GetScoped())
            {
                for (var i = 0; i < RoomInstanceData.PrefabParent.childCount; i++)
                {
                    var childTr = RoomInstanceData.PrefabParent.GetChild(i);
                    if (CursorBounds(m_cursor).Contains(childTr.position))
                    {
                        var childGo = childTr.gameObject;
                        cursorObjListScoped.Obj.Add(new CursorObjectData()
                        {
                            GameObject = childGo,
                            ObjectContextMarker = childGo.Get<ObjectContextMarker>(),
                            ActionBehaviour = childGo.Get<IActionBehaviour>()
                        });
                    };
                }

                m_cursorObjects = cursorObjListScoped.Obj.ToArray();
            }
        }

        static IEnumerable<ObjectActionData> AllObjectsActions()
        {
            foreach (var c in RoomInstanceData.PrefabParent.Children())
            {
                if (!c.TryGetComponent<ObjectContextMarker>(out var ocm))
                    continue;
                if (ocm.Actions == null)
                    continue;
                yield return new ObjectActionData()
                {
                    ObjectActionsConfig = ocm.Actions,
                    ObjectInstance = ocm.gameObject
                };
            }
        }

        static bool FindGUIDFromIconName(string iconName, out string guid)
        {
            // Find guid based on icon name from attr
            var iconGuids = AssetDatabase.FindAssets($"{iconName} t:texture2D");
            var iconGuidsList = iconGuids.ToList();
            guid = iconGuidsList.FirstOrDefault();

            return !string.IsNullOrEmpty(guid);
        }

        void GetCachedSerializedObject(LevelObjectsConfig lvlObjectsConfig, out SerializedObject serializedObject)
        {
            if (m_levelObjectConfigToSerialized.TryGetValue(lvlObjectsConfig, out serializedObject)) 
                return;

            serializedObject = new SerializedObject(lvlObjectsConfig);
            m_levelObjectConfigToSerialized.Add(lvlObjectsConfig, serializedObject);
        }

        #region Asset Setup
        OperationResult AddActionsConfig(out ObjectActionsConfig config)
        {
            config = null;
            if (!m_selectedGo.TryGet(out ObjectContextMarker objCtxMarker))
                return OperationResult.Error;

            var objectPlacingContext = objCtxMarker.PlacingContext;

            var operationResult = GetLevelObjectsConfig(objectPlacingContext, RoomInstance, out var lvlObjectsConfig);
            if (operationResult == OperationResult.Error)
                return OperationResult.Error;

            var idx = Array.FindIndex(lvlObjectsConfig.Editor_ObjectData, od => od.Editor_Instance == m_selectedGo);
            if (idx == -1)
                return OperationResult.Error;

            var parentAsset = lvlObjectsConfig.Parent();
            config = ScriptableObject.CreateInstance<ObjectActionsConfig>();
            config.AddToAsset(parentAsset);

            m_objectContext = lvlObjectsConfig
                .ChildAssets().FirstOrDefault(s => s is ContextAsset ca
                                                   && string.Equals(ca.name, "Object")) as ContextAsset;
            if (m_objectContext == null)
            {
                m_objectContext = ScriptableObject.CreateInstance<ContextAsset>();
                m_objectContext.name = "Object";
                m_objectContext.AddToAsset(parentAsset);
            }
            config.Editor_LinkContextAssets(m_objectContext, lvlObjectsConfig);
            m_objectContext.SetContextLink(m_selectedGo);

            AssetDatabase.SaveAssets();

            lvlObjectsConfig.Editor_ObjectData[idx].ObjectActions = config;
            objCtxMarker.Actions = config;

            return OperationResult.OK;
        }
        #endregion

        static bool SceneValid(out Scene scene)
        {
            scene = SceneManager.GetActiveScene();
            return !scene.path.IsNullOrEmpty();
        }
        #endregion

        #region Listener Events
        void OnHierarchyChanged()
        {
            if (!RoomInitialized)
                return;

            //NestObjectsInRoomBounds();
            UpdateLevelObjects();
            UpdateCursorObjects();
        }
        #endregion
    }
}