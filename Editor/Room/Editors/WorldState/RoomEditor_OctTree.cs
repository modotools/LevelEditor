using Core.Editor.Attribute;
using Core.Editor.Extensions;
using Core.Editor.Interface;
using Core.Extensions;
using Core.Types;
using Core.Unity.Utility.Debug;
using Level.Editor.Room.States;
using Generation.Data;
using JetBrains.Annotations;
using Level.Room;
using Level.Generation.Operations;
using UnityEditor;
using UnityEngine;

using static Level.Generation.Operations.WorkerOperations;

namespace Level.Editor.Room.Editors
{
    [EditorState(typeof(RoomEditor_World))]
    [UsedImplicitly]
    public class RoomEditor_Octree : IEditor, ISceneGUICallable
    {
        public string Name => "Octree";

        static bool RoomInitialized => RoomEditor.CurrentEditor?.RoomInitialized == true;
        static IRoomInstance RoomInstance => RoomEditor.CurrentEditor.RoomInstance;
        static ref RoomInstanceData RoomInstanceData => ref RoomEditor.CurrentEditor.RoomInstanceData;
        public object ParentContainer { get; private set; }
        static int CurrentLevel => RoomEditor_Platforms.Current.CurrentLevel;

        #region Editor Prefs
        static readonly string k_editorPref_testNeighborBoundsColor = $"{nameof(RoomEditor)}.{nameof(m_testNeighborBoundsColor)}";
        static readonly string k_editorPref_activeNeighborBoundsColor = $"{nameof(RoomEditor)}.{nameof(m_activeNeighborBoundsColor)}";

        static readonly Color k_testNeighborBoundsColorDefault = new Color(1f, 1f, 0.75f);
        static readonly Color k_activeNeighborBoundsColorDefault = new Color(1f, 1f, 0f);
        #endregion

        Color m_activeNeighborBoundsColor;
        Color m_testNeighborBoundsColor;

        IRoomInstance m_lastInstance;
        readonly NeighborData[] m_neighborData = new NeighborData[4];
        readonly bool[] m_munchAtCorner = new bool[4];

        public void Init(object parentContainer)
        {
            ParentContainer = parentContainer;
            LoadDebugSettings();
        }

        public void Terminate() { }
        public void SetActive(bool active) { }
        void Reset()
        {
            m_lastInstance = RoomInstance;
            for (var i = 0; i < m_neighborData.Length; i++) 
                m_neighborData[i] = NeighborData.Default;
        }

        public void OnGUI(float width)
        {
            if (m_lastInstance != RoomInstance)
                Reset();

            PrefsGUI();

            if (GUILayout.Button("Init Tree with all rooms"))
                InitAllRooms();

            var hasRoom = OctreeInstance.Rooms.HasObject(RoomInstance);
            using (new EditorGUI.DisabledScope(hasRoom))
            {
                if (GUILayout.Button("Add Room to Tree")) 
                    OctreeInstance.Rooms.AddObject(RoomInstance);
            }

            using (new EditorGUI.DisabledScope(!hasRoom))
            {
                if (GUILayout.Button("Gather Neighbors"))
                {
                    Reset();
                    for (var i = 0; i < 4; ++i)
                        GatherNeighbors(RoomInstance, OctreeInstance.Rooms, (Cardinals) i, m_neighborData);
                }

                if (GUILayout.Button("Copy Edge Tiles")) 
                    CopyAllEdgeTiles();

                
                if (GUILayout.Button("Munch Corners"))
                {
                    CornerMunching(RoomInstance, CurrentLevel, CornerMunchingData.Default, m_neighborData,
                        m_munchAtCorner);
                    RoomEditor.CurrentEditor.UpdateAndBuildPreview();
                }
                if (GUILayout.Button("Update Exits with neighbors"))
                {
                    RoomInstance.OpenExits.Clear();
                    RoomInstance.OpenExitsFromTiles(RoomInstance.OpenExits);
                    FilterExitsAndCloseNeighborExits(RoomInstance, RoomInstance.OpenExits, m_neighborData);
                }
            }
            
        }

        void CopyAllEdgeTiles()
        {
            for (var i = 0; i < 4; ++i)
            {
                var neighborDir = (Cardinals) i;
                var exitDir = (Cardinals) ((i + 2) % 4);

                var neighbor = m_neighborData[i];
                if (neighbor.Rooms == null)
                    continue;
                foreach (var r in neighbor.Rooms)
                    CopyEdgeTiles(RoomInstance, r, neighborDir);
            }

            RoomEditor.CurrentEditor.UpdateAndBuildPreview();
        }

        void InitAllRooms()
        {
            OctreeInstance.Rooms.Clear();
            var rooms = Object.FindObjectsOfType<RoomInstanceComponent>();
            foreach (var r in rooms) 
                OctreeInstance.Rooms.AddObject(r);
        }

        void PrefsGUI()
        {
            using (new EditorGUILayout.HorizontalScope())
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_testNeighborBoundsColor = EditorGUILayout.ColorField("Test Neighbor Bounds", m_testNeighborBoundsColor);
                m_activeNeighborBoundsColor = EditorGUILayout.ColorField("Active Neighbor Bounds", m_activeNeighborBoundsColor);
                // m_exitsColor = EditorGUILayout.ColorField("Exits", m_exitsColor);

                if (!check.changed)
                    return;

                SaveDebugSettings();
            }
        }

        void LoadDebugSettings()
        {
            m_testNeighborBoundsColor = CustomEditorPrefs.GetColor(k_editorPref_testNeighborBoundsColor, k_testNeighborBoundsColorDefault);
            m_activeNeighborBoundsColor = CustomEditorPrefs.GetColor(k_editorPref_activeNeighborBoundsColor, k_activeNeighborBoundsColorDefault);
        }
        void SaveDebugSettings()
        {
            CustomEditorPrefs.SetColor(k_editorPref_testNeighborBoundsColor, m_testNeighborBoundsColor);
            CustomEditorPrefs.SetColor(k_editorPref_activeNeighborBoundsColor, m_activeNeighborBoundsColor);
        }

        public void OnSceneGUI(SceneView sceneView)
        {
            if (!RoomInitialized)
                return;
            
            //Debug.Log($"RoomEditor_Octree OnSceneGUI");

            foreach (var n in m_neighborData)
            {
                CustomDebugDraw.DrawBounds(n.Bounds, m_testNeighborBoundsColor);
                if (!n.Rooms.IsNullOrEmpty())
                {
                    foreach (var r in n.Rooms) 
                        CustomDebugDraw.DrawBounds(r.Bounds, m_activeNeighborBoundsColor);
                }
            }
        }
    }
}