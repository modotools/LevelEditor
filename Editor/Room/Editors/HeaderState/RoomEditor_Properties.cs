using Core.Editor.Attribute;
using Core.Editor.Extensions;
using Core.Editor.Interface;
using Core.Extensions;
using Core.Types;
using Core.Unity.Utility.GUITools;
using Level.Editor.Room.States;
using JetBrains.Annotations;
using Level.Room;
using Level.Room.Operations;
using Level.Tiles;
using UnityEditor;
using UnityEngine;

namespace Level.Editor.Room.Editors
{
    [EditorState(typeof(RoomEditor_HeaderState))]
    [UsedImplicitly]
    public class RoomEditor_Properties : IEditor
    {
        public string Name => "Room Properties";

        static bool RoomInitialized => RoomEditor.CurrentEditor?.RoomInitialized == true;
        static ref RoomInstanceData RoomInstance => ref RoomEditor.CurrentEditor.RoomInstanceData;
        public object ParentContainer { get; private set; }

        static ref RoomConfigData GetRoomData()
        {
            if (RoomInitialized)
                return ref RoomInstance.GetRoomData();
            return ref m_newRoomConfigData;
        }
        static RoomConfigData m_newRoomConfigData;
        public static RoomConfigData NewRoomConfigData => m_newRoomConfigData;

        public static int DefaultRoomGroundLayer
        {
            get
            {
                if (NewRoomConfigData.Settings == null)
                    return -1;
                return NewRoomConfigData.Settings.GroundLayer;
            }
        }

        #region Editor Prefs
        static readonly string k_editorPref_roomSize = $"{nameof(RoomEditor)}.{nameof(RoomConfigData.Size)}";
        static readonly string k_editorPref_defaultRoomSettings = $"{nameof(RoomEditor)}.{nameof(RoomSettings)}";
        #endregion

        public void Init(object parentContainer)
        {
            ParentContainer = parentContainer;
            LoadDefaultRoomSettings();
        }

        public void Terminate() { }
        public void SetActive(bool active) { }

        public void OnGUI(float width)
        {
            var data = GetRoomData();

            if (data.Name.IsNullOrEmpty() && ParentContainer is EditorState editorState)
                editorState.Expand(this);
            if (RoomDataGUI(width, ref data) != ChangeCheck.Changed) 
                return;
            if (RoomInitialized)
            {
                ref var roomInst = ref RoomInstance;
                Undo.RecordObjects(new Object[] { roomInst.RoomConfig, roomInst.GameObject },
                    "Room-Data");
                roomInst.UpdateData(data);
            }
            else m_newRoomConfigData = data;

            RoomEditor.CurrentEditor.UpdatePreview();
        }

        ChangeCheck RoomDataGUI(float width, ref RoomConfigData configData)
        {
            var changed = ChangeCheck.NotChanged;
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                GUILayout.Space(15.0f);
                using (new GUILayout.HorizontalScope())
                {
                    using (new ColorScope(configData.Name.IsNullOrEmpty() ? new Color(1f,0.3f,0.2f) : Color.white))
                        GUILayout.Label("Name: ", EditorStyles.boldLabel, GUILayout.Width(50));
                    configData.Name = EditorGUILayout.TextField(configData.Name);
                    GUILayout.FlexibleSpace();
                }
                GUILayout.Space(5.0f);

                if (RoomDataSettingsGUI(ref configData.Settings) == ChangeCheck.Changed) 
                    changed = ChangeCheck.Changed;

                var settings = configData.Settings;

                GridSettings gs = null;
                if (settings != null)
                    gs = settings.Grid;
                var size = configData.Size;
                if (gs != null)
                {
                    // size of room
                    size = new Vector3Int(configData.Size.x / gs.RoomChunkSize, configData.Size.y / gs.RoomYChunkSize,
                        configData.Size.z / gs.RoomChunkSize);
                }

                if (RoomSizeGUI(ref size) == ChangeCheck.Changed)
                {
                    changed = ChangeCheck.Changed;
                    var newSize = new Vector3Int(size.x * gs.RoomChunkSize, size.y * gs.RoomYChunkSize,
                        size.z * gs.RoomChunkSize);
                    configData.Size = newSize;
                }

                if (check.changed)
                    changed = ChangeCheck.Changed;

                return changed;
            }
        }

        static ChangeCheck RoomDataSettingsGUI(ref RoomSettings settings)
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                settings = (RoomSettings) EditorGUILayout.ObjectField(settings, 
                    typeof(RoomSettings), false);
                if (settings != null)
                {
                    settings.TileSet = (TilesSetListConfig)EditorGUILayout.ObjectField("Tile Set", settings.TileSet,
                        typeof(TilesSetListConfig), false);
                    settings.Grid = (GridSettings)EditorGUILayout.ObjectField("Grid Settings", settings.Grid,
                        typeof(GridSettings), false);
                    settings.Builder = (RoomBuilder)EditorGUILayout.ObjectField("Builder", settings.Builder,
                        typeof(RoomBuilder), false);

                    settings.CeilingMaterial = (Material)EditorGUILayout.ObjectField("Ceiling Material", settings.CeilingMaterial,
                        typeof(Material), false);
                }

                if (!check.changed)
                    return ChangeCheck.NotChanged;
                SaveDefaultRoomSettings(ref settings);
                return ChangeCheck.Changed;
            }
        }

        static void LoadDefaultRoomSettings()
        {
            m_newRoomConfigData.Size = CustomEditorPrefs.GetVector3Int(k_editorPref_roomSize, Vector3Int.one);
            m_newRoomConfigData.Settings = CustomEditorPrefs.GetAssetFromGuid<RoomSettings>(k_editorPref_defaultRoomSettings);
        }

        static void SaveDefaultRoomSettings(ref RoomSettings settings) 
            => CustomEditorPrefs.SetGuid(k_editorPref_defaultRoomSettings, settings);

        static ChangeCheck RoomSizeGUI(ref Vector3Int size)
        {
            ref var rd = ref GetRoomData();
            var changed = ChangeCheck.NotChanged;

            using (new GUILayout.HorizontalScope())
            {
                EditorGUILayout.LabelField("Size-X", GUILayout.Width(50.0f));
                var xSize = Mathf.Max(1, EditorGUILayout.IntField(size.x, GUILayout.Width(25.0f)));
                EditorGUILayout.LabelField("Size-Y", GUILayout.Width(50.0f));
                var ySize = Mathf.Max(1, EditorGUILayout.IntField(size.y, GUILayout.Width(25.0f)));
                EditorGUILayout.LabelField("Size-Z", GUILayout.Width(50.0f));
                var zSize = Mathf.Max(1, EditorGUILayout.IntField(size.z, GUILayout.Width(25.0f)));

                if (xSize != size.x || zSize != size.z || ySize != size.y)
                {
                    changed = ChangeCheck.Changed;
                    size = new Vector3Int(xSize, ySize, zSize);
                }

                var gs = rd.Settings == null ? null : rd.Settings.Grid;
                if (gs != null)
                    EditorGUILayout.LabelField($"Actual size: x {xSize * gs.RoomChunkSize} y {ySize * gs.RoomYChunkSize} z { zSize * gs.RoomChunkSize}");
            }

            if (changed != ChangeCheck.Changed)
                return changed;

            CustomEditorPrefs.SetVector3Int(k_editorPref_roomSize, size);
            return changed;
        }
    }
}