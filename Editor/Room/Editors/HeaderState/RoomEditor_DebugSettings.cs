using System.Collections.Generic;
using System.Linq;
using Core.Editor.Attribute;
using Core.Editor.Extensions;
using Core.Editor.Interface;
using Core.Editor.Utility;
using Core.Extensions;
using Core.Unity.Extensions;
using Core.Unity.Utility.GUITools;
using Level.Editor.Room.States;
using JetBrains.Annotations;
using Level.Room;
using UnityEditor;
using UnityEngine;
using static Level.Room.ExitOperations;

namespace Level.Editor.Room.Editors
{
    [EditorState(typeof(RoomEditor_HeaderState))]
    [UsedImplicitly]
    public class RoomEditor_DebugSettings : IEditor, ISceneGUICallable
    {
        public string Name => "Debug Settings";

        static bool RoomInitialized => RoomEditor.CurrentEditor?.RoomInitialized == true;
        static IRoomInstance RoomInstance => RoomEditor.CurrentEditor.RoomInstance;
        static ref RoomInstanceData RoomInstanceData => ref RoomEditor.CurrentEditor.RoomInstanceData;
        public object ParentContainer { get; private set; }

        #region Editor Prefs
        static readonly string k_editorPref_roomBoundsColor = $"{nameof(RoomEditor)}.{nameof(m_roomBoundsColor)}";
        static readonly string k_editorPref_openExitsColor = $"{nameof(RoomEditor)}.{nameof(m_openExitsColor)}";
        static readonly string k_editorPref_optionalExitColor = $"{nameof(RoomEditor)}.{nameof(m_optionalExitsColor)}";
        static readonly string k_editorPref_blockedExitColor = $"{nameof(RoomEditor)}.{nameof(m_blockedExitsColor)}";
        static readonly string k_editorPref_closedExitsColor = $"{nameof(RoomEditor)}.{nameof(m_closedExitsColor)}";

        static readonly Color k_roomBoundsColorDefault = Color.magenta;
        static readonly Color k_openExitColorDefault = Color.green;
        static readonly Color k_optionalExitColorDefault = Color.yellow;
        static readonly Color k_blockedExitColorDefault = Color.red;
        static readonly Color k_closedExitColorDefault = Color.blue;
        #endregion

        Color m_roomBoundsColor = k_roomBoundsColorDefault;
        Color m_openExitsColor = k_openExitColorDefault;
        Color m_closedExitsColor = k_closedExitColorDefault;
        Color m_optionalExitsColor = k_optionalExitColorDefault;
        Color m_blockedExitsColor = k_blockedExitColorDefault;
        
        public void Init(object parentContainer)
        {
            ParentContainer = parentContainer;
            LoadDebugSettings();
        }

        public void Terminate() { }
        public void SetActive(bool active) { }

        public void OnGUI(float width) => RoomDebugSettingsGUI();

        void RoomDebugSettingsGUI()
        {
            using var check = new EditorGUI.ChangeCheckScope();
            m_roomBoundsColor = EditorGUILayout.ColorField("Room Bounds", m_roomBoundsColor);
            m_openExitsColor = EditorGUILayout.ColorField("Open Exits", m_openExitsColor);
            m_closedExitsColor = EditorGUILayout.ColorField("Closed Exits", m_closedExitsColor);

            if (!check.changed)
                return;

            SaveDebugSettings();
        }

        void LoadDebugSettings()
        {
            m_roomBoundsColor = CustomEditorPrefs.GetColor(k_editorPref_roomBoundsColor, k_roomBoundsColorDefault);
            m_openExitsColor = CustomEditorPrefs.GetColor(k_editorPref_openExitsColor, k_openExitColorDefault);
            m_closedExitsColor = CustomEditorPrefs.GetColor(k_editorPref_closedExitsColor, k_closedExitColorDefault);
            m_optionalExitsColor = CustomEditorPrefs.GetColor(k_editorPref_optionalExitColor, k_optionalExitColorDefault);
            m_blockedExitsColor = CustomEditorPrefs.GetColor(k_editorPref_blockedExitColor, k_blockedExitColorDefault);
        }
        void SaveDebugSettings()
        {
            CustomEditorPrefs.SetColor(k_editorPref_roomBoundsColor, m_roomBoundsColor);
            CustomEditorPrefs.SetColor(k_editorPref_openExitsColor, m_openExitsColor);
            CustomEditorPrefs.SetColor(k_editorPref_closedExitsColor, m_closedExitsColor);
            CustomEditorPrefs.SetColor(k_editorPref_optionalExitColor, m_optionalExitsColor);
            CustomEditorPrefs.SetColor(k_editorPref_blockedExitColor, m_blockedExitsColor);
        }

        public void OnSceneGUI(SceneView sceneView)
        {
            if (!RoomInitialized)
            {
                var min = RoomEditor.m_initialPosition;
                var size = RoomEditor_Properties.NewRoomConfigData.Size;
                var center = min + 0.5f * size.Vector3();
                CustomHandleDraw.DrawBounds(new Bounds(center, size), m_roomBoundsColor);
                return;
            }
            
            CustomHandleDraw.DrawBounds(RoomInstanceData.Bounds, m_roomBoundsColor);
            DrawExits(RoomInstance.OpenExits, m_openExitsColor);
            DrawExits(RoomInstance.ClosedExits, m_closedExitsColor);


            var optional = RoomInstance.ExitRestrictions.Where(e =>
                e.Restriction == null || e.Restriction.Type != RestrictionType.AlwaysBlocked);
            var blocked= RoomInstance.ExitRestrictions.Where(e =>
                e.Restriction != null && e.Restriction.Type == RestrictionType.AlwaysBlocked);
            DrawExits(optional, m_optionalExitsColor);
            DrawExits(blocked, m_blockedExitsColor);

        }

        static void DrawExits(IEnumerable<ExitInfo> exits, Color exitColor)
        {
            if (exits.IsNullOrEmpty())
                return;

            using (new ColorScope(exitColor))
            {
                foreach (var exit in exits) 
                    Editor_DrawExit(RoomInstance, exit);
            }
        }
    }
}