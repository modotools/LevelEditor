using System.Collections.Generic;
using System.Linq;
using Core.Editor.Attribute;
using Core.Editor.Inspector;
using Core.Editor.Interface;
using Core.Editor.Utility.GUIStyles;
using Core.Unity;
using Core.Unity.Extensions;
using Level.Editor.Room.States;
using JetBrains.Annotations;
using Level.Generation;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using static Core.Unity.Operations.SceneRootOperations;

namespace Level.Editor.Room.Editors
{
    [EditorState(typeof(RoomEditor_HeaderState))]
    [UsedImplicitly]
    public class RoomEditor_Scene : IEditor
    {
        public string Name => "Scene";

        public static RoomEditor_Scene Current { get; private set; }
        public object ParentContainer { get; private set; }

        GameObject m_lastManagerObject;
        bool m_showLevelManager;
        public ILevelManager LevelManager;
        UnityEditor.Editor m_levelManagerEditor;

        public void Init(object parentContainer)
        {
            ParentContainer = parentContainer;
            Current = this;
        }

        public void Terminate() { }
        public void SetActive(bool active) { }

        public void OnGUI(float width)
        {
            SceneValidationGUI();
            LevelManagerGUI(width);
        }

        void LevelManagerGUI(float width)
        {
            if (LevelManager != null)
                UnityEditor.Editor.CreateCachedEditor(LevelManager as Component, null, ref m_levelManagerEditor);
            else if (m_levelManagerEditor != null) 
                m_levelManagerEditor.DestroyEx();
            
            if (m_levelManagerEditor == null) 
                return;
            
            m_showLevelManager = EditorGUILayout.Foldout(m_showLevelManager, "Level Manager",
                true, CustomStyles.MiniFoldoutStyle);
            
            if (m_showLevelManager && m_levelManagerEditor is IBaseInspector inspector)
                inspector.GetEditor().OnGUI(width);
        }

        void SceneValidationGUI()
        {
            // ReSharper disable ObjectCreationAsStatement
            var sceneRootGo = GetSceneRoot(SceneManager.GetActiveScene(), out _);
            if (sceneRootGo == null)
            {
                EditorGUILayout.HelpBox("No Scene Root!", MessageType.Warning);
                if (GUILayout.Button("Create Scene Root"))
                    new GameObject("_Scene", typeof(SceneRoot));

                if (ParentContainer is EditorState editorState)
                    editorState.Expand(this);
                return;
            }

            m_lastManagerObject =
                EditorGUILayout.ObjectField("Manager: ", m_lastManagerObject, typeof(GameObject), true) as GameObject;
            if (m_lastManagerObject == null)
            {
                CacheManager(sceneRootGo.transform);
                EditorGUILayout.HelpBox("No Manager-Object!", MessageType.Warning);
                if (GUILayout.Button("Create Manager-Object"))
                {
                    m_lastManagerObject = new GameObject("LevelManager");
                    m_lastManagerObject.transform.SetParent(sceneRootGo.transform);
                }

                return;
            }
            // ReSharper restore ObjectCreationAsStatement

            if (!m_lastManagerObject.TryGet(out LevelManager))
            {
                EditorGUILayout.HelpBox("No LevelManager-component!", MessageType.Warning);
                if (GUILayout.Button("Add LevelManager-component"))
                    m_lastManagerObject.AddComponent<LevelManager>();
            }

            if (!m_lastManagerObject.TryGet<RoomOctreeComponent>(out _))
            {
                EditorGUILayout.HelpBox("No RoomOctreeComponent-component!", MessageType.Warning);
                if (GUILayout.Button("Add RoomOctreeComponent-component"))
                    m_lastManagerObject.AddComponent<RoomOctreeComponent>();
            }

            if (!m_lastManagerObject.TryGet<WorkerAreaOctreeComponent>(out _))
            {
                EditorGUILayout.HelpBox("No WorkerAreaOctreeComponent-component!", MessageType.Warning);
                if (GUILayout.Button("Add WorkerAreaOctreeComponent-component"))
                    m_lastManagerObject.AddComponent<WorkerAreaOctreeComponent>();
            }
        }

        void CacheManager(Transform expectedParent)
        {
            var levelManagers = Object.FindObjectsOfType<Component>().Where(c => c is ILevelManager);
            var roomOctrees = Object.FindObjectsOfType<RoomOctreeComponent>();
            var workerAreaOctrees = Object.FindObjectsOfType<WorkerAreaOctreeComponent>();
            
            if (TryCacheManagerObject(expectedParent, levelManagers)) 
                return;
            if (TryCacheManagerObject(expectedParent, roomOctrees)) 
                return;
            TryCacheManagerObject(expectedParent, workerAreaOctrees);
        }

        bool TryCacheManagerObject(Transform expectedParent, IEnumerable<Component> components)
        {
            foreach (var component in components)
            {
                if (component.transform.parent != expectedParent)
                    continue;
                m_lastManagerObject = component.gameObject;
                return true;
            }

            return false;
        }
    }
}