using System.Collections.Generic;
using Core.Editor.Attribute;
using Core.Editor.Interface;
using Core.Editor.Utility;
using Core.Extensions;
using Core.Unity.Extensions;
using Core.Unity.Utility;
using Level.Editor.Room.States;
using JetBrains.Annotations;
using Level.Editor.Room.Operations;
using Level.PlatformLayer;
using Level.Room;
using Level.Room.Operations;
using UnityEditor;
using UnityEngine;

using static Core.Editor.Utility.CustomGUI;
using static Level.Editor.Settings.LevelEditorSettingsAccess;

namespace Level.Editor.Room.Editors
{
    [EditorState(typeof(RoomEditor_HeaderState), 10)]
    [UsedImplicitly]
    public class RoomEditor_Platforms : IEditor, IEditorExtras, IRepaintable, ISceneGUICallable
    {
        public static RoomEditor_Platforms Current;

        #region Properties
        public string Name => "Platforms";
        public object ParentContainer { get; private set; }

        #region Room Properties
        static bool RoomInitialized => RoomEditor.CurrentEditor?.RoomInitialized == true;
        static ref RoomInstanceData RoomInstance => ref RoomEditor.CurrentEditor.RoomInstanceData;
        static RoomConfig Config => RoomInstance.RoomConfig;
        #endregion

        #region Platform Properties
        static List<PlatformLayerConfig> PlatformLayerList => Config != null ? Config.PlatformLayerConfigs : null;
        static int Count => PlatformLayerList?.Count ?? 0;
        public int CurrentLevel { get; private set; }

        bool m_currentVisibleOnly;
        bool m_useDarkPlane;

        GameObject m_darkPlane;

        public PlatformLayerConfig CurrentPlatformLayer
        {
            get
            {
                var layerList = PlatformLayerList;
                if (layerList != null && CurrentLevel.IsInRange(layerList))
                    return layerList[CurrentLevel];
                return null;
            }
        }
        #endregion
        #endregion
        
        
        public void Init(object parentContainer)
        {
            ParentContainer = parentContainer;
            Current = this;
            
            //m_func = sceneView => OnSceneGUI();
            //SceneView.duringSceneGui += m_func;
        }

        public void OnSceneGUI(SceneView sv)
        {
            Current = this;

            if (!RoomInitialized)
                return;
            
            var e = Event.current;
            ScrollThroughLevels(e);
        }

        void ScrollThroughLevels(Event e)
        {
            if (!e.shift || e.type != EventType.ScrollWheel) 
                return;

            e.Use();
            if (e.delta.y >= 1)
                SetCurrentLevel(CurrentLevel + 1);
            else if (e.delta.y <= -1)
                SetCurrentLevel(CurrentLevel - 1);
            Repaint();
            SceneView.RepaintAll();
        }

        public void Terminate()
        {
            Current = null;
            DestroyPlane();
        }

        public void SetActive(bool active)
        {
            if (!active)
                DestroyPlane();
        }
        
        void DestroyPlane()
        {
            if (m_darkPlane != null)
                m_darkPlane.DestroyEx();
            m_darkPlane = null;
        }

        public void OnGUI(float width)
        {
            if (!RoomInitialized)
            {
                DestroyPlane();
                return;
            }
            Current = this;

            // how many levels does the room have
            using (new GUILayout.HorizontalScope())
            {
                var levels = EditorGUILayout.IntField("Levels", Count);
                if (GUILayout.Button("+", GUILayout.Width(50))) { levels++; }
                if (GUILayout.Button("-", GUILayout.Width(50))) { levels--; }
                ChangeLevelCount(levels);

                GUILayout.Space(15.0f);
                if (GUILayout.Button("^", GUILayout.Width(25))) { MoveLevelUp(); }
                if (GUILayout.Button("v", GUILayout.Width(25))) { MoveLevelDown(); }
                if (GUILayout.Button("x", GUILayout.Width(25))) { DeleteLevel(); }
            }

            using (new GUILayout.HorizontalScope())
            {
                EditorGUILayout.LabelField($"Current Platform {CurrentLevel}", GUILayout.Width(125.0f));
                for (var i = 0; i < Count; ++i)
                {
                    if (ActivityButton(CurrentLevel == i, i.ToString()))
                        SetCurrentLevel(i);
                    
                    if (i <= 0 || i % 5 != 0) 
                        continue;
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal();
                }
            }

            using (new GUILayout.HorizontalScope())
            {
                var newVisible = GUILayout.Toggle(m_currentVisibleOnly, "Current Visible Only");
                if (newVisible != m_currentVisibleOnly)
                {
                    m_currentVisibleOnly = newVisible;
                    ClampCurrentLevel();
                }

                var useDarkenPlane = GUILayout.Toggle(m_useDarkPlane, "Use Dark Plane");
                var darkPlaneUsed = m_darkPlane != null;
                if (useDarkenPlane != darkPlaneUsed)
                {
                    m_useDarkPlane = useDarkenPlane;
                    if (m_useDarkPlane)
                    {
                        m_darkPlane = GameObject.CreatePrimitive(PrimitiveType.Plane);
                        m_darkPlane.AddComponent<CleanupMarker>();
                        m_darkPlane.hideFlags = HideFlags.HideAndDontSave;
                        m_darkPlane.GetComponent<MeshRenderer>().sharedMaterial = DarkPlaneMaterial;
                    }
                    else
                        m_darkPlane.DestroyEx();

                    ClampCurrentLevel();
                }
            }
        }

        public void ClampCurrentLevel() => SetCurrentLevel(CurrentLevel);

        public void SetCurrentLevel(int currentLevel)
        {
            CurrentLevel = Mathf.Clamp(currentLevel, 0, Count - 1);
            ref var inst = ref RoomInstance;
            inst.SetLevel(m_currentVisibleOnly ? CurrentLevel : 0, 
                CurrentLevel);

            if (m_darkPlane == null) 
                return;
            var center = RoomInstance.Bounds.center;
            var pos = new Vector3(center.x,RoomInstance.Position.y + CurrentLevel-0.01f, center.z);
            m_darkPlane.transform.position = pos;
            m_darkPlane.transform.localScale = 0.1f * RoomInstance.Bounds.size;

        }

        static void MoveLevel(int a, int b)
        {
            var pl = PlatformLayerList;
            var aLvl = pl[a];
            var bLvl = pl[b];
            pl[a] = bLvl;
            pl[b] = aLvl;
        }

        void MoveLevelUp()
        {
            if (CurrentLevel >= Count - 1)
                return;

            Undo.RecordObject(Config, nameof(MoveLevelUp));
            MoveLevel(CurrentLevel, CurrentLevel + 1);

            ref var inst = ref RoomInstance;
            inst.UpdateVisuals();
            SetCurrentLevel(CurrentLevel + 1);

            var roomEd = RoomEditor.CurrentEditor;
            roomEd.UpdateAndBuildPreview();
        }

        void MoveLevelDown()
        {
            if (CurrentLevel <= 0 || Count <= 1)
                return;

            Undo.RecordObject(Config, nameof(MoveLevelDown));
            MoveLevel(CurrentLevel, CurrentLevel - 1);

            ref var inst = ref RoomInstance;
            inst.UpdateVisuals();
            SetCurrentLevel(CurrentLevel - 1);

            var roomEd = RoomEditor.CurrentEditor;
            roomEd.UpdateAndBuildPreview();
        }

        void DeleteLevel()
        {
            if (Count <= 1)
                return;

            Undo.RecordObject(Config, nameof(DeleteLevel));

            var platform = PlatformLayerList[CurrentLevel];
            if (ReferenceEquals(platform.Parent(), Config))
                platform.DestroyEx();

            PlatformLayerList.RemoveAt(CurrentLevel);
            AssetDatabase.SaveAssets();

            SetCurrentLevel(CurrentLevel - 1);

            ref var inst = ref RoomInstance;

            inst.UpdateVisuals();
            var roomEd = RoomEditor.CurrentEditor;
            roomEd.UpdateAndBuildPreview();
        }

        void ChangeLevelCount(int newLevelCount)
        {
            var prevCount = Count;

            // how many levels does the room have
            newLevelCount = Mathf.Clamp(newLevelCount, 1, Config.MaxPlatformCount);
            if (newLevelCount == Count)
                return;

            Undo.RecordObject(Config, nameof(ChangeLevelCount));
            Config.ChangeLevelCount(newLevelCount);
            SetCurrentLevel(CurrentLevel);

            ref var inst = ref RoomInstance;

            inst.UpdateVisuals();

            var roomEd = RoomEditor.CurrentEditor;
            roomEd.UpdateAndBuildPreview();
        }

        public bool ShowAlways => true;
        public bool ShowLabel => false;


        public void Repaint()
        {
            switch (ParentContainer)
            {
                case EditorWindow w: w.Repaint(); break;
                case UnityEditor.Editor e: e.Repaint(); break;
                case IRepaintable r: r.Repaint(); break;
            }
        }
    }
}