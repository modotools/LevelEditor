﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Editor.Attribute;
using Core.Editor.Extensions;
using Core.Editor.Interface;
using Core.Editor.Utility;
using Core.Editor.Utility.GUIStyles;
using Core.Events;
using Core.Extensions;
using Core.Types;
using Core.Unity.Utility.GUITools;
using Level.Editor.Room.States;
using Level.Editor.Tiles;
using JetBrains.Annotations;
using Level;
using Level.Enums;
using Level.Generation.Operations;
using Level.Room;
using UnityEditor;
using UnityEngine;
using static Level.Room.ExitOperations;

namespace Level.Editor.Room.Editors
{
    [EditorState(typeof(RoomEditor_TilesState))]
    [UsedImplicitly]
    public class RoomEditor_ExitTools : IEditor, ISceneGUICallable, IEventListener<TileDrawing.TilesDrawn>
    {
        public string Name => "Exit Tools";
        public object ParentContainer { get; private set; }

        static bool RoomInitialized => RoomEditor.CurrentEditor?.RoomInitialized == true;
        IRoomInstance RoomInstance => RoomEditor.CurrentEditor.RoomInstance;
        static ref RoomInstanceData RoomInstanceData => ref RoomEditor.CurrentEditor.RoomInstanceData;
        static RoomConfig RoomConfig => RoomInitialized? RoomInstanceData.RoomConfig : null;
        static int CurrentLevel => RoomEditor_Platforms.Current.CurrentLevel;
        
        #region Editor Prefs
        static readonly string k_editorPref_selectedExitsColor = $"{nameof(RoomEditor)}.{nameof(m_selectedExitColor)}";
        static readonly Color k_exitColorSelectedDefault = Color.green;
        #endregion

        Color m_selectedExitColor = k_exitColorSelectedDefault;
        float m_width;


        bool m_isDirty;
        int m_prevLevel;

        public struct ExitGUIData
        {
            public bool EditExitsAllowed;

            public ExitInfo[] CachedExits;
            public GUIContent[] ExitContents;
        }

        ExitInfo m_selectedExit;
        Bounds m_selectedExitBounds;

        ExitGUIData m_openExits = new ExitGUIData(){ EditExitsAllowed = false };
        ExitGUIData m_exitRestrict = new ExitGUIData(){ EditExitsAllowed = true };

        public void Init(object parentContainer)
        {
            ParentContainer = parentContainer;
            EventMessenger.RemoveListener(this);
            EventMessenger.AddListener(this);
            LoadPrefs();
        }

        public void Terminate()
        {
            EventMessenger.RemoveListener(this);
        }

        public void SetActive(bool active) { }

        public void OnGUI(float width)
        {
            m_width = width;
            if (!RoomInitialized)
                return;

            ExitToolsGUI();
        }

        void LoadPrefs()
            => m_selectedExitColor = CustomEditorPrefs.GetColor(k_editorPref_selectedExitsColor, k_exitColorSelectedDefault);

        void SavePrefs()
            => CustomEditorPrefs.SetColor(k_editorPref_selectedExitsColor, m_selectedExitColor);
        
        void ExitToolsGUI()
        {
            SettingsGUI();
            
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_selectedExitColor = EditorGUILayout.ColorField("Selected Exits", m_selectedExitColor);

                if (check.changed)
                    SavePrefs();
            }

            ExitSelectionGridGUI("Open Exits on current lvl: ", ref m_openExits);
            ExitSelectionGridGUI("Exit Restrictions on current lvl: ", ref m_exitRestrict);

            SelectedExitGUI(ref m_openExits);
            SelectedExitGUI(ref m_exitRestrict);

            using (new EditorGUI.DisabledScope(true))
                EditorGUILayout.BoundsField("Selected Bounds: ", m_selectedExitBounds);

            EditorGUILayout.LabelField("ValidExitConfigurations: ");
            foreach (var exitConfiguration in RoomInstance.ValidExitConfiguration)
                EditorGUILayout.LabelField($"{exitConfiguration}");

            foreach (var @int in RoomInstance.ValidCardinalMap)
            {
                var n = (@int & 1) == 1 ? "N": "";
                var e = (@int & 2) == 2 ? "E": "";
                var s = (@int & 4) == 4 ? "S": "";
                var w = (@int & 8) == 8 ? "W": "";
                EditorGUILayout.LabelField($"{n}{s}{e}{w}");
            }
        }

        void SettingsGUI()
        {
            if (RoomConfig == null)
                return;
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                RoomConfig.AllowMirror = EditorGUILayout.Toggle("Allow Mirror", RoomConfig.AllowMirror);
                RoomConfig.AllowRotate = EditorGUILayout.Toggle("Allow Rotate", RoomConfig.AllowRotate);
                RoomConfig.RestrictionModeEntry = (RestrictionMode) EditorGUILayout.EnumPopup("Restriction Mode Entry", RoomConfig.RestrictionModeEntry);
                RoomConfig.RestrictionModeExit = (RestrictionMode) EditorGUILayout.EnumPopup("Restriction Mode Exit", RoomConfig.RestrictionModeExit);
                
                if (!check.changed)
                    return;

                RoomInstance.UpdateExitConfiguration();
            }
        }
        
        void SelectedExitGUI(ref ExitGUIData exitGUIData)
        {
            var selectedInGUIData = exitGUIData.CachedExits.Any(a => a == m_selectedExit);
            if (!selectedInGUIData || m_selectedExit == null)
                return;
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_selectedExit.Restriction = (ExitRestrictionTypeID) EditorGUILayout.ObjectField(m_selectedExit.Restriction, typeof(ExitRestrictionTypeID), false);
                var blocker = m_selectedExit.Restriction != null &&
                              m_selectedExit.Restriction.Type != RestrictionType.None;
                m_selectedExit.PossibleEntry = !blocker && EditorGUILayout.Toggle("Possible Entry", m_selectedExit.PossibleEntry);
                
                if (exitGUIData.EditExitsAllowed)
                {
                    m_selectedExit.ExitDirection = (Cardinals) EditorGUILayout.EnumPopup("ExitDirection: ", m_selectedExit.ExitDirection);
                    // todo: clamp values
                    m_selectedExit.Level = EditorGUILayout.IntField("Level: ", m_selectedExit.Level);
                    m_selectedExit.FromIdx = EditorGUILayout.IntField("From: ", m_selectedExit.FromIdx);
                    m_selectedExit.ToIdx = EditorGUILayout.IntField("From: ", m_selectedExit.ToIdx);

                    if (GUILayout.Button("Remove"))
                    {
                        RoomInstance.ExitRestrictions.Remove(m_selectedExit);
                        m_isDirty = true;
                    }
                }
                
                if (check.changed)
                    m_isDirty = true;
            }
        }

        void CacheContents()
        {
            var openExits = RoomInstance.OpenExits.Where(e => e.Level == CurrentLevel);
            var exitRestrictions = RoomInstance.ExitRestrictions.Where(e => e.Level == CurrentLevel);

            var needCache = m_openExits.ExitContents == null || m_exitRestrict.ExitContents == null
                 || m_prevLevel != CurrentLevel
                 || openExits.Count() != m_openExits.CachedExits.Length
                 || exitRestrictions.Count() != m_exitRestrict.CachedExits.Length;


            if (!needCache && !m_isDirty)
                return;

            m_isDirty = false;
            m_openExits.CachedExits = openExits.ToArray();
            m_exitRestrict.CachedExits = exitRestrictions.ToArray();
            m_prevLevel = CurrentLevel;

            CreateExitGUIContent(ref m_openExits);
            CreateExitGUIContent(ref m_exitRestrict);
            
            RoomInstance.UpdateExitConfiguration();
        }

        void CreateExitGUIContent(ref ExitGUIData guiData)
        {
            using (var exitContentObj = SimplePool<List<GUIContent>>.I.GetScoped())
            {
                foreach (var e in guiData.CachedExits)
                {
                    var exitName = $"<color=yellow>#{e.ExitDirection}</color> " +
                                   $"From <color=green>{e.FromIdx}</color> " +
                                   $"To <color=green>{e.ToIdx}</color> " +
                                   $"<color=orange>{e.Restriction}</color>";
                    exitContentObj.Obj.Add(new GUIContent(exitName));
                }

                guiData.ExitContents = exitContentObj.Obj.ToArray();
            }
        }

        void ExitSelectionGridGUI(string title, ref ExitGUIData guiData)
        {
            CacheContents();

            GUILayout.Label($"{title} {guiData.ExitContents.Length}");
            var rect = EditorGUILayout.GetControlRect(GUILayout.Width(m_width - 20),
                GUILayout.Height(guiData.ExitContents.Length * 25));

            var idxOfExit = guiData.CachedExits.FirstIndexWhere(e=>e == m_selectedExit);
            var newSelection = GUI.SelectionGrid(rect, idxOfExit, guiData.ExitContents, 1, CustomStyles.SelectableLabel);
            if (newSelection != idxOfExit) 
                SelectExit(guiData, newSelection);
            if (guiData.EditExitsAllowed && GUILayout.Button("Add"))
            {
                RoomInstance.ExitRestrictions.Add(new ExitInfo(){ Level = CurrentLevel });
                m_isDirty = true;
            }
        }

        void SelectExit(ExitGUIData guiData, int newSelection)
        {
            m_selectedExit = newSelection == -1 
                ? null 
                : guiData.CachedExits[newSelection];

            m_selectedExitBounds = m_selectedExit != null 
                    ? BoundsOperations.GetBoundsContainingExitTiles(RoomInstance.Bounds, m_selectedExit) 
                    : default;
        }

        public void OnSceneGUI(SceneView sceneView) => DrawSelectedExit();

        void DrawSelectedExit()
        {
            if (m_selectedExit == null)
                return;
            using (new ColorScope(m_selectedExitColor)) 
                Editor_DrawExit(RoomInstance, m_selectedExit);
            CustomHandleDraw.DrawBounds(m_selectedExitBounds, m_selectedExitColor);
        }

        public void OnEvent(TileDrawing.TilesDrawn eventType) => m_isDirty = true;
    }
}