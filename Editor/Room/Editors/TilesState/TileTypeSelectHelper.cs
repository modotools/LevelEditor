﻿using System.Linq;
using Core.Extensions;
using Core.Types;
using Level.Room;
using Level.Tiles;
using Level.Tiles.Interface;
using UnityEditor;

namespace Level.Editor.Room.Editors
{
    public static class TileTypeSelectHelper
    {
        public static ChangeCheck SelectTileConfigGUI(ITileConfig[] configs, string label, string[] tileConfigOptions, ref ITileConfig selectedConfig)
        {
            var sel = selectedConfig;
            var tileConfigIdx = configs.FirstIndexWhere(c => c == sel);
            var newTileConfigIdx = EditorGUILayout.Popup(label, tileConfigIdx, tileConfigOptions);
            if (newTileConfigIdx == tileConfigIdx)
                return ChangeCheck.NotChanged;

            selectedConfig = newTileConfigIdx.IsInRange(configs) ? configs[newTileConfigIdx] : null;

            return ChangeCheck.Changed;
        }

        public static void _OnConfigSelected(RoomConfig rc, ITileConfig selected, out TilesSetData tilesSetData, out string[] tileTypeOptions)
        {
            //m_replaceType.TileConfig = new RefITileConfig(){ Result = selected };
            //m_replaceWithType.TileConfig = new RefITileConfig(){ Result = selected };
            var setListConfig = rc.TileSetListConfig;
            tilesSetData = setListConfig.TileDataSets.FirstOrDefault(set => set.TileConfig.Result == selected);

            if (selected == null)
            {
                tileTypeOptions = null;
                return;
            }      
            tileTypeOptions = new string[selected.Count];
            for (var i = 0; i < selected.Count; i++) 
                tileTypeOptions[i] = selected[i].TileName;
        }

        public static void CacheTileConfigs(RoomConfig rc, out ITileConfig[] configs, out string[] tileConfigOptions)
        {
            var setListConfig = rc.TileSetListConfig;
            configs = setListConfig.TileDataSets.Select(set => set.TileConfig.Result).ToArray();
            tileConfigOptions = configs.Select(a => a.Name).ToArray();
        }
    }
}