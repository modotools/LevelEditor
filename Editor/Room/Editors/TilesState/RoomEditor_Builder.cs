using Core.Editor.Attribute;
using Core.Editor.Interface;
using Core.Extensions;
using Level.Editor.Room.States;
using JetBrains.Annotations;
using Level.Room;
using UnityEditor;

namespace Level.Editor.Room.Editors
{
    [EditorState(typeof(RoomEditor_TilesState), 10)]
    [UsedImplicitly]
    public class RoomEditor_Builder : IEditor
    {
        RoomBuilderEditor m_editor;

        static ref RoomInstanceData RoomInstance => ref RoomEditor.CurrentEditor.RoomInstanceData;
        static RoomBuilder Builder => RoomInstance.RoomConfig != null ? RoomInstance.RoomConfig.Builder : null;

        public string Name => "Builder";
        public object ParentContainer { get; private set; }

        public void Init(object parentContainer)
        {
            ParentContainer = parentContainer;
            m_editor = new RoomBuilderEditor();
            m_editor.Init(this);
        }

        public void Terminate() => m_editor?.Terminate();
        public void SetActive(bool active) { }

        public void OnGUI(float width)
        {
            if (RoomEditor.CurrentEditor == null)
                return;
            m_editor.SetTarget(Builder, new SerializedObject(Builder));

            if (RoomEditor_Platforms.Current == null)
                return;
            var lvl = RoomEditor_Platforms.Current.CurrentLevel;
            
            ref var roomInst = ref RoomInstance;
            if (!roomInst.VisualData.IsValid)
                return;
            var layerList = roomInst.RoomConfig.PlatformLayerConfigs;
            if (!layerList.IsIndexInRange(lvl))
                return;

            var visObj = roomInst.VisualData.GetPlatformInstance(lvl);
            //m_editor.SetInputData(visObj.Go, layerList[lvl], visObj.Mesh, roomInst.RoomContext);
            m_editor?.OnGUI(width);
        }
    }
}