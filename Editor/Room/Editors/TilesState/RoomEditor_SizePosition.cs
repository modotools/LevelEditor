using Core.Editor.Attribute;
using Core.Editor.Interface;
using Level.Editor.Room.States;
using JetBrains.Annotations;
using Level.Editor.Tools;
using Level.PlatformLayer;
using Level.Room;
using UnityEditor;

using static Level.Editor.Tools.SizePositionTools;

namespace Level.Editor.Room.Editors
{
    [EditorState(typeof(RoomEditor_TilesState), 10)]
    [UsedImplicitly]
    public class RoomEditor_SizePosition : 
        IEditor, ISceneGUICallable
    {
        #region static Properties
        static RoomEditor CurrentEditor => RoomEditor.CurrentEditor;
        static RoomEditor_Platforms PlatformEditor => RoomEditor_Platforms.Current;
        static int CurrentLevel => PlatformEditor.CurrentLevel;
        static PlatformLayerConfig CurrentPlatform => PlatformEditor.CurrentPlatformLayer;
        
        static ref RoomInstanceData RoomInstanceData => ref CurrentEditor.RoomInstanceData;
        static IRoomInstance RoomInstance => CurrentEditor.RoomInstance;
        //static RoomBuilder Builder => RoomInstanceData.RoomConfig!= null? RoomInstanceData.RoomConfig.Builder : null;
        #endregion

        SizePositionGUIData m_guiData;
        bool[] m_selectedPlatform;
        int m_prevLevel;

        IRoomInstance m_prevRoom;
        
        public string Name => "Size/Position";
        public object ParentContainer { get; private set; }

        public void Init(object parentContainer)
        {
            ParentContainer = parentContainer;
        }

        public void Terminate()
        {
        }

        public void SetActive(bool active) { }

        public void OnGUI(float width)
        {
            if (CurrentEditor == null || PlatformEditor == null)
                return;

            if (m_prevLevel != CurrentLevel)
                LevelChanged();
            UpdateSize();

            SizeToolsRoomOptionGUI(ref m_guiData);
            if (!m_guiData.AffectRoom)
                LevelSelectGUI(ref m_guiData);

            var prevCategory = m_guiData.Category;
            SizeToolsCategoryGUI(ref m_guiData);

            if (prevCategory != m_guiData.Category || m_prevRoom != RoomInstance) 
                _InitSizePositionGUIData();
        }

        void LevelChanged()
        {
            m_prevLevel = CurrentLevel;
            ResizeSelectPlatformsArrayIfNeeded();
            CurrentLevelToSelected();
        }

        public void OnSceneGUI(SceneView sceneView)
        {
            UpdateSize();
            SizePositionSlider_SceneGUI(ref m_guiData);
        }

        void UpdateSize()
        {
            var diff = m_guiData.SizeData - RoomInstance.Size;
            if (diff.sqrMagnitude > float.Epsilon)
                _InitSizePositionGUIData();
        }


        void _InitSizePositionGUIData()
        {
            m_prevRoom = RoomInstance;
            ResizeSelectPlatformsArrayIfNeeded();
            InitSizePositionGUIData(ref m_guiData, RoomInstance, CurrentPlatform, m_selectedPlatform);
        }

        void ResizeSelectPlatformsArrayIfNeeded()
        {
            if (m_selectedPlatform != null 
                && m_selectedPlatform.Length == RoomInstance.PlatformCount) 
                return;

            m_selectedPlatform = new bool[RoomInstance.PlatformCount];

            CurrentLevelToSelected();
        }
        void CurrentLevelToSelected()
        {
            for (var i = 0; i < m_selectedPlatform.Length; i++)
                m_selectedPlatform[i] = i == CurrentLevel;
        }
    }
}