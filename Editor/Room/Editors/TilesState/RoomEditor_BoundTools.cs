﻿using System.Linq;
using Core.Editor.Attribute;
using Core.Editor.Extensions;
using Core.Editor.Interface;
using Core.Editor.Utility;
using Core.Extensions;
using Core.Types;
using Level.Editor.Room.States;
using JetBrains.Annotations;
using Level;
using Level.Room;
using Level.Tiles;
using Level.Tiles.Interface;
using UnityEditor;
using UnityEngine;
using static Level.PlatformLayer.Operations;

namespace Level.Editor.Room.Editors
{
    [EditorState(typeof(RoomEditor_TilesState))]
    [UsedImplicitly]
    public class RoomEditor_BoundTools : IEditor, ISceneGUICallable
    {
        public string Name => "Bound Tools";
        public object ParentContainer { get; private set; }

        static bool RoomInitialized => RoomEditor.CurrentEditor?.RoomInitialized == true;
        IRoomInstance RoomInstance => RoomEditor.CurrentEditor.RoomInstance;
        static ref RoomInstanceData RoomInstanceData => ref RoomEditor.CurrentEditor.RoomInstanceData;
        static RoomConfig RoomConfig => RoomInitialized? RoomInstanceData.RoomConfig : null;
        static int CurrentLevel => RoomEditor_Platforms.Current.CurrentLevel;
        
        #region Editor Prefs
        static readonly string k_editorPref_boundsColor = $"{nameof(RoomEditor)}.{nameof(m_editBoundsColor)}";
        static readonly Color k_editBoundsDefaultColor = Color.cyan;
        #endregion

        Color m_editBoundsColor = k_editBoundsDefaultColor;
        Bounds m_editBounds;
        float m_width;

        #region Setting Tile Types
        string[] m_tileConfigOptions;
        string[] m_tileTypeOptions;
        ITileConfig m_selectedConfig;
        TilesSetData m_selectedSetData;
        TileTypeIdentifier m_setType;
        #endregion

        #region Default Editor Calls
        public void Init(object parentContainer)
        {
            ParentContainer = parentContainer;
            LoadPrefs();
        }

        public void Terminate()
        {
        }
        
        public void OnSceneGUI(SceneView sceneView) => DrawEditBounds();

        public void SetActive(bool active) { }

        public void OnGUI(float width)
        {
            m_width = width;
            if (!RoomInitialized)
                return;

            EditBoundsGUI();
            SetTilesGUI();
        }
        #endregion

        #region Prefs
        void LoadPrefs()
            => m_editBoundsColor = CustomEditorPrefs.GetColor(k_editorPref_boundsColor, k_editBoundsDefaultColor);

        void SavePrefs()
            => CustomEditorPrefs.SetColor(k_editorPref_boundsColor, m_editBoundsColor);
        #endregion

        void SetTilesGUI()
        {
            bool doSet;

            CacheTileConfigs(out var configs);

            var tileConfigIdx = configs.FirstIndexWhere(c => c == m_selectedConfig);
            var newTileConfigIdx = EditorGUILayout.Popup("Config", tileConfigIdx, m_tileConfigOptions);
            if (newTileConfigIdx != tileConfigIdx)
            {
                m_selectedConfig = newTileConfigIdx.IsInRange(configs) ? configs[newTileConfigIdx] : null;
                OnConfigSelected();
            }
            if (m_selectedConfig == null)
                return;

            using (new GUILayout.HorizontalScope())
            {
                m_setType.TileIdx = (ushort) EditorGUILayout.Popup("Set Type",
                    m_setType.TileIdx, m_tileTypeOptions);

                doSet = GUILayout.Button("Set!", GUILayout.Width(100));
            }

            if (!doSet) 
                return;

            DoSetTiles();
        }

        void DoSetTiles()
        {
            Undo.RecordObject(RoomConfig, "Set Tiles in Bounds");

            for (var i = 0; i < RoomConfig.PlatformCount; i++)
            {
                var lvl = RoomConfig.PlatformLayerConfigs[i];
                if (lvl.PositionOffset.y < m_editBounds.min.y)
                {
                    // Debug.Log($"ignoring lvl {i} pos-offset-y {lvl.PositionOffset.y}");
                    continue;
                }   
                if (lvl.PositionOffset.y > m_editBounds.max.y)
                {
                    // Debug.Log($"ignoring lvl {i} pos-offset-y {lvl.PositionOffset.y}");
                    continue;
                }
                // Debug.Log($"pos-offset-y {lvl.PositionOffset.y}");
                for (var x = 0; x < lvl.TileDim.x; ++x)
                for (var z = 0; z < lvl.TileDim.y; ++z)
                {
                    var tilePos = lvl.PositionOffset + new Vector3(x, 0, z);
                    if (!m_editBounds.Contains(tilePos)) 
                        continue;

                    // Debug.Log($"affecting tile [{x}, {z}] tilePos {tilePos}");
                    SetTile(lvl, lvl, new Vector2Int(x, z), m_setType.TileIdx,
                        m_selectedSetData.Mask, m_selectedSetData.Shift);
                }
            }

            var roomEd = RoomEditor.CurrentEditor;
            roomEd.UpdateAndBuildPreview();
        }


        void EditBoundsGUI()
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_editBoundsColor = EditorGUILayout.ColorField("Edit Bounds Color", m_editBoundsColor);

                if (check.changed)
                    SavePrefs();
            }

            var cardinals = (Cardinals) EditorGUILayout.EnumPopup("Set to Neighbor room bounds", Cardinals.Invalid);
            if (cardinals != Cardinals.Invalid)
                BoundsOperations.GetBoundsNextTo(RoomInstance.Bounds, cardinals, 0, RoomInstance.RoomSettings.Grid.RoomChunkSize3D, 
                    out m_editBounds);

            m_editBounds = EditorGUILayout.BoundsField("Edit Bounds: ", m_editBounds);
        }

        void DrawEditBounds()
            => CustomHandleDraw.DrawBounds(m_editBounds, m_editBoundsColor);

        #region Cache
        void OnConfigSelected()
        {
            m_setType.TileConfig = new RefITileConfig(){ Result = m_selectedConfig };
            var setListConfig = RoomConfig.TileSetListConfig;
            m_selectedSetData =
                setListConfig.TileDataSets.FirstOrDefault(set => set.TileConfig.Result == m_selectedConfig);

            if (m_selectedConfig == null)
            {
                m_tileTypeOptions = null;
                return;
            }      
            m_tileTypeOptions = new string[m_selectedConfig.Count];
            for (var i = 0; i < m_selectedConfig.Count; i++) 
                m_tileTypeOptions[i] = m_selectedConfig[i].TileName;
        }
        void CacheTileConfigs(out ITileConfig[] configs)
        {
            var setListConfig = RoomConfig.TileSetListConfig;
            configs = setListConfig.TileDataSets.Select(set => set.TileConfig.Result).ToArray();
            m_tileConfigOptions = configs.Select(a => a.Name).ToArray();
        }
        #endregion
    }
}