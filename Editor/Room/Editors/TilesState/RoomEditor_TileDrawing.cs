﻿using Core.Editor.Attribute;
using Core.Editor.Interface;
using Level.Editor.PlatformLayer;
using Level.Editor.Room.States;
using JetBrains.Annotations;
using Level.PlatformLayer;
using UnityEditor;

namespace Level.Editor.Room.Editors
{
    [EditorState(typeof(RoomEditor_TilesState), 10)]
    [UsedImplicitly]
    public class RoomEditor_TileDrawing : IEditor, IEditorExtras, ISceneGUICallable
    {
        //public static TileDrawing Current;
        //static RoomInstanceData RoomInstance => RoomEditor.CurrentEditor.RoomInstance;
        static PlatformLayerConfig LayerConfig => RoomEditor_Platforms.Current.CurrentPlatformLayer;

        PlatformEditor m_editor;

        public string Name => "Tile Drawing";
        public object ParentContainer { get; private set; }
        public bool ShowAlways => true;
        public bool ShowLabel => false;

        SerializedObject m_layerConfigSO;
        
        public void Init(object parentContainer)
        {
            ParentContainer = parentContainer;
            m_editor = new PlatformEditor();
            m_editor.Init(this);
        }

        public void Terminate() => m_editor?.Terminate();
        public void SetActive(bool active) { }

        public void OnGUI(float width)
        {
            //Debug.Log(RoomPlatformsEditor.Current.CurrentLevel);
            if (LayerConfig == null)
                return;
            if (m_layerConfigSO == null || m_layerConfigSO.targetObject != LayerConfig)
            {
                m_layerConfigSO = new SerializedObject(LayerConfig);
                m_editor?.SetTarget(LayerConfig, m_layerConfigSO);
            }
            
            m_editor?.OnGUI(width);
        }

        public void OnSceneGUI(SceneView sceneView) => m_editor?.OnSceneGUI();
    }
}