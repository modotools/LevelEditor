﻿using Core.Editor.Attribute;
using Core.Editor.Interface;
using Core.Events;
using Level.Editor.Room.States;
using Level.Editor.Tiles;
using JetBrains.Annotations;
using Level.Room;
using UnityEditor;
using UnityEngine;

namespace Level.Editor.Room.Editors
{
    [EditorState(typeof(RoomEditor_TilesState))]
    [UsedImplicitly]
    public class RoomEditor_SmartPlacingTools : IEditor, IEventListener<TileDrawing.TilesDrawn>
    {
        public string Name => "Smart Placing Tools";
        public object ParentContainer { get; private set; }

        static bool RoomInitialized => RoomEditor.CurrentEditor?.RoomInitialized == true;
        IRoomInstance RoomInstance => RoomEditor.CurrentEditor.RoomInstance;
        static ref RoomInstanceData RoomInstanceData => ref RoomEditor.CurrentEditor.RoomInstanceData;
        static RoomConfig RoomConfig => RoomInitialized? RoomInstanceData.RoomConfig : null;
        static int CurrentLevel => RoomEditor_Platforms.Current.CurrentLevel;
        
        #region Editor Prefs
        const string k_editorPref_modifySurroundingLevels = "RoomEditor.ModifySurroundingLevels";
        const string k_editorPref_AvoidOneSpaceTiles = "RoomEditor.AvoidOneSpaceTiles";
        const string k_editorPref_fixVisibilityIssues = "RoomEditor.FixVisibilityIssues";
        #endregion

        bool m_avoidOneSpaceTiles;
        bool m_modifySurroundingLevels;
        bool m_fixVisibilityIssues;

        public void Init(object parentContainer)
        {
            ParentContainer = parentContainer;

            EventMessenger.RemoveListener(this);
            EventMessenger.AddListener(this);

            LoadPrefs();
        }

        public void Terminate() => EventMessenger.RemoveListener(this);

        public void SetActive(bool active) { }

        public void OnGUI(float width)
        {
            if (!RoomInitialized)
                return;

            SmartTilePlacingToolsGUI();
        }

        void LoadPrefs()
        {
            m_avoidOneSpaceTiles = EditorPrefs.GetBool(k_editorPref_AvoidOneSpaceTiles, false);
            m_modifySurroundingLevels = EditorPrefs.GetBool(k_editorPref_modifySurroundingLevels, false);
            m_fixVisibilityIssues = EditorPrefs.GetBool(k_editorPref_fixVisibilityIssues, false);
        }

        void SmartTilePlacingToolsGUI()
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                using (new GUILayout.HorizontalScope())
                {
                    GUILayout.Label("modify surrounding levels to fix mesh issues", GUILayout.Width(275.0f));
                    m_modifySurroundingLevels =
                        EditorGUILayout.Toggle(m_modifySurroundingLevels, GUILayout.Width(25.0f));
                }

                using (new EditorGUI.DisabledScope(!m_modifySurroundingLevels))
                {
                    using (new GUILayout.HorizontalScope())
                    {
                        GUILayout.Label("Also fix vissibility issues", GUILayout.Width(275.0f));
                        m_fixVisibilityIssues = EditorGUILayout.Toggle(m_fixVisibilityIssues, GUILayout.Width(25.0f));
                    }

                    using (new GUILayout.HorizontalScope())
                    {
                        GUILayout.Label("Fill one-space-empty tiles", GUILayout.Width(275.0f));
                        m_avoidOneSpaceTiles = EditorGUILayout.Toggle(m_avoidOneSpaceTiles, GUILayout.Width(25.0f));
                    }
                }

                if (check.changed)
                {
                    EditorPrefs.SetBool(k_editorPref_modifySurroundingLevels, m_modifySurroundingLevels);
                    EditorPrefs.SetBool(k_editorPref_fixVisibilityIssues, m_fixVisibilityIssues);
                    EditorPrefs.SetBool(k_editorPref_AvoidOneSpaceTiles, m_avoidOneSpaceTiles);
                }
            }

            if (GUILayout.Button("Apply on Level!"))
            {
                ApplySmartPlacingToolsOnLevel(CurrentLevel);
                RoomEditor.CurrentEditor.UpdateAndBuildPreview();
            }
            if (GUILayout.Button("Apply on all Levels!"))
            {
                ApplySmartPlacingToolsOnAllLevels();
                RoomEditor.CurrentEditor.UpdateAndBuildPreview();
            }
        }

        void ApplySmartPlacingToolsOnAllLevels()
        {
            var plCount = RoomInstance.PlatformCount;
            for (var i = 0; i < plCount; i++) 
                ApplySmartPlacingToolsOnLevel(i);
        }

        void ApplySmartPlacingToolsOnLevel(int lvl)
        {
            for (var x = 0; x < RoomInstance.TileDim.x; ++x)
            for (var z = 0; z < RoomInstance.TileDim.y; ++z)
                ModifySurroundingLevel(new Vector2Int(x, z), lvl);
        }

        void ModifySurroundingLevel(Vector2Int pos, int lvl)
        {
            if (!m_modifySurroundingLevels)
                return;

            RoomInstance.ModifySurroundingLevel(pos, lvl, m_fixVisibilityIssues, m_avoidOneSpaceTiles);
        }

        public void OnEvent(TileDrawing.TilesDrawn eventType)
        {
            if (!m_modifySurroundingLevels)
                return;

            foreach (var p in eventType.Positions) 
                ModifySurroundingLevel(p, CurrentLevel);
        }
    }
}