﻿using Core;
using Core.Editor.Attribute;
using Core.Editor.Interface;
using Level.Editor.Room.States;
using JetBrains.Annotations;
using Level.PlatformLayer;
using UnityEditor;
using UnityEngine;
using Level.Room;
using static Level.PlatformLayer.Operations;

namespace Level.Editor.Room.Editors
{
    [EditorState(typeof(RoomEditor_TilesState), 10)]
    [UsedImplicitly]
    public class RoomEditor_PerlinNoise : IEditor, ISceneGUICallable
    {
        static PlatformLayerConfig LayerConfig => RoomEditor_Platforms.Current.CurrentPlatformLayer;

        IRoomInstance RoomInstance => RoomEditor.CurrentEditor.RoomInstance;

        public string Name => "Perlin Noise";
        public object ParentContainer { get; private set; }

        const int k_texWidth = 100;

        float m_scaleXZ = 1.0f;
        float m_scaleHeight = 1.0f;
        Texture2D m_noiseTex;
        Texture2D m_resizedTex;
        Color[] m_colors;

        public void Init(object parentContainer) => ParentContainer = parentContainer;

        public void Terminate() { }
        public void SetActive(bool active) { }

        public void OnGUI(float width)
        {
            if (m_noiseTex) 
                GUILayout.Label(new GUIContent(m_resizedTex));

            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label("XZ Scale");
                m_scaleXZ = EditorGUILayout.Slider(m_scaleXZ, 0.0f, 100.0f);
            }

            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label("Height");
                m_scaleHeight = EditorGUILayout.Slider(m_scaleHeight, 0.0f, 100.0f);
            }

            if (GUILayout.Button("Generate"))
                GeneratePerlinNoise(RoomInstance.Position.x, RoomInstance.Position.z, 
                    RoomInstance.TileDim.x,
                    RoomInstance.TileDim.y);

            if (GUILayout.Button("Apply")) 
                ApplyPerlinNoise();
        }

        public void OnSceneGUI(SceneView sceneView) { }

        void GeneratePerlinNoise(int originX, int originY, int width, int height)
        {
            m_noiseTex = new Texture2D(width, height);
            m_colors = new Color[width * height];

            //DebugString debugString = default;
            //debugString.AddLog($"originX {originX} originY {originY} Width {width} Height {height}");
            for (var y = 0; y < height; y++)
            for (var x = 0; x < width; x++)
            {
                var xPos = (float)(originX + x) / width * m_scaleXZ;
                var yPos = (float)(originY + y) / height * m_scaleXZ;
                var sample = Mathf.PerlinNoise(xPos, yPos);
                //debugString.AddLog($"x {x} y {y} xPos {xPos} yPos {yPos} {Mathf.PerlinNoise(xPos, yPos)} sample {sample}");

                m_colors[y * width + x] = new Color(sample, sample, sample);
            }
            //debugString.Print();

            m_noiseTex.SetPixels(m_colors);
            m_noiseTex.Apply();
            m_resizedTex = Resize(m_noiseTex, k_texWidth, (int) (k_texWidth * (float)height / width));
        }

        static Texture2D Resize(UnityEngine.Texture texture, int targetX, int targetY)
        {
            var rt = new RenderTexture(targetX, targetY, 24);
            RenderTexture.active = rt;
            Graphics.Blit(texture, rt);
            var result = new Texture2D(targetX, targetY);
            result.ReadPixels(new Rect(0, 0, targetX, targetY), 0, 0);
            result.Apply();
            return result;
        }

        void ApplyPerlinNoise()
        {
            var roomSettings = RoomInstance.RoomSettings;
            var tileDim = RoomInstance.TileDim;
            var offset = RoomInstance.Position;

            var emptyFilter = roomSettings.GetFilter(Enums.GeometryType.Empty);
            var floorFilter = roomSettings.GetFilter(Enums.GeometryType.Floor);
            var wallFilter = roomSettings.GetFilter(Enums.GeometryType.Wall);

            //DebugString debugString = default;
            //debugString.AddLog($"originX {offset.x} originY {offset.z} Width {tileDim.x} Height {tileDim.y}");
            for (var y = 0; y < tileDim.y; y++)
            for (var x = 0; x < tileDim.x; x++)
            {
                var xPos = (float)(offset.x + x) / tileDim.x * m_scaleXZ;
                var yPos = (float)(offset.z + y) / tileDim.y * m_scaleXZ;
                var sample = Mathf.PerlinNoise(xPos, yPos) * m_scaleHeight;

                var roundedSample = Mathf.RoundToInt(sample);
                //debugString.AddLog($"x {x} y {y} xPos {xPos} yPos {yPos} sample {Mathf.PerlinNoise(xPos, yPos)} scaled {sample} roundedSample {roundedSample}\n");

                var layerHeight = 0;
                foreach (var layer in RoomInstance.PlatformLayer)
                {
                    var tile = layerHeight == roundedSample
                        ? floorFilter
                        : (layerHeight < roundedSample ? wallFilter : emptyFilter);
                    SetTile(layer, layer, new Vector2Int(x, y), tile);
                    layerHeight++;
                }
            }
            //debugString.Print();
            RoomEditor.CurrentEditor.UpdateAndBuildPreview();
        }
    }
}