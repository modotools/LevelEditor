﻿using System.Linq;
using Core.Editor.Attribute;
using Core.Editor.Interface;
using Core.Extensions;
using Core.Types;
using Level.Editor.Room.States;
using JetBrains.Annotations;
using Level.Room;
using Level.Tiles;
using Level.Tiles.Interface;
using UnityEditor;
using UnityEngine;

using static Level.PlatformLayer.Operations;
using static Level.Editor.Room.Editors.TileTypeSelectHelper;

namespace Level.Editor.Room.Editors
{
    [EditorState(typeof(RoomEditor_TilesState))]
    [UsedImplicitly]
    public class RoomEditor_TileReplacement : IEditor
    {
        public string Name => "Tile Replacement";
        public object ParentContainer { get; private set; }

        static bool RoomInitialized => RoomEditor.CurrentEditor?.RoomInitialized == true;
        static ref RoomInstanceData RoomInstance => ref RoomEditor.CurrentEditor.RoomInstanceData;
        static RoomConfig RoomConfig => RoomInitialized? RoomInstance.RoomConfig : null;

        string[] m_tileConfigOptions;
        string[] m_tileTypeOptions;

        ITileConfig m_selectedConfig;
        TilesSetData m_selectedSetData;

        TileTypeIdentifier m_replaceType;
        TileTypeIdentifier m_replaceWithType;

        public void Init(object parentContainer) => ParentContainer = parentContainer;

        public void Terminate() { }

        public void SetActive(bool active) { }

        public void OnGUI(float width)
        {
            if (!RoomInitialized)
                return;

            bool doReplace;

            _CacheTileConfigs(out var configs);
            _SelectTileConfigGUI(configs);

            if (m_selectedConfig == null)
                return;

            using (new GUILayout.HorizontalScope())
            {
                m_replaceType.TileIdx = (ushort) EditorGUILayout.Popup("Replace Type",
                    m_replaceType.TileIdx, m_tileTypeOptions);
                m_replaceWithType.TileIdx = (ushort) EditorGUILayout.Popup("with Type", 
                    m_replaceWithType.TileIdx, m_tileTypeOptions);
                doReplace = GUILayout.Button("Replace!", GUILayout.Width(100));
            }

            if (!doReplace) 
                return;

            Undo.RecordObject(RoomConfig, "Tile-Replacement");

            // todo: all levels
            var lvlIdx = RoomEditor_Platforms.Current.CurrentLevel;

            var lvl = RoomConfig.PlatformLayerConfigs[lvlIdx];
            for (var x = 0; x < lvl.TileDim.x; ++x)
            for (var z = 0; z < lvl.TileDim.y; ++z)
            {
                // todo: improve getting and setting tiles, operations, cleanup

                var tile = m_selectedSetData.GetTileIdx(GetTile(lvl, lvl, new Vector2Int(x, z)));
                if (tile == m_replaceType.TileIdx)
                    SetTile(lvl, lvl, new Vector2Int(x, z), m_replaceWithType.TileIdx, 
                        m_selectedSetData.Mask, m_selectedSetData.Shift);
            }

            var roomEd = RoomEditor.CurrentEditor;
            roomEd.UpdateAndBuildPreview();
        }

        void _SelectTileConfigGUI(ITileConfig[] configs)
        {
            if (SelectTileConfigGUI(configs, "Config", m_tileConfigOptions, ref m_selectedConfig) == ChangeCheck.Changed)
                OnConfigSelected();
        }

        void OnConfigSelected()
        {
            m_replaceType.TileConfig = new RefITileConfig(){ Result = m_selectedConfig };
            m_replaceWithType.TileConfig = new RefITileConfig(){ Result = m_selectedConfig };

            _OnConfigSelected(RoomConfig, m_selectedConfig, out m_selectedSetData, out m_tileTypeOptions);
        }

        void _CacheTileConfigs(out ITileConfig[] configs) => CacheTileConfigs(RoomConfig, out configs, out m_tileConfigOptions);
    }
}