﻿using Core.Editor.Attribute;
using Core.Editor.Interface;
using Core.Extensions;
using Core.Types;
using Level.Editor.Room.States;
using JetBrains.Annotations;
using Level.Room;
using Level.Enums;
using Level.Generation.Operations;
using Level.Tiles;
using Level.Tiles.Interface;
using UnityEditor;
using UnityEngine;

using static Level.Editor.Room.Editors.TileTypeSelectHelper;

namespace Level.Editor.Room.Editors
{
    [EditorState(typeof(RoomEditor_TilesState))]
    [UsedImplicitly]
    public class RoomEditor_GeneratorTools : IEditor
    {
        public string Name => "Generator Tools";
        public object ParentContainer { get; private set; }

        static bool RoomInitialized => RoomEditor.CurrentEditor?.RoomInitialized == true;
        IRoomInstance RoomInstance => RoomEditor.CurrentEditor.RoomInstance;
        static ref RoomInstanceData RoomInstanceData => ref RoomEditor.CurrentEditor.RoomInstanceData;
        static RoomConfig RoomConfig => RoomInitialized? RoomInstanceData.RoomConfig : null;
        static int CurrentLevel => RoomEditor_Platforms.Current.CurrentLevel;

        string m_seed;
        bool m_useRandomSeed;

        [Range(0, 100)] int m_randomFillPercent;

        int m_bRegionsTooSmallThreshold;
        int m_aRegionTooSmallThreshold;

        int m_smoothAThreshold;
        int m_smoothBThreshold;

        #region Editor Prefs
        const string k_editorPref_useRandomSeed = "RoomEditor.UseRandomSeed";
        const string k_editorPref_seed = "RoomEditor.Seed";
        const string k_editorPref_randomFillPercent = "RoomEditor.RandomFillPercent";
        const string k_editorPref_bRegionsTooSmallThreshold = "RoomEditor.BRegionsTooSmallThreshold";
        const string k_editorPref_aRegionsTooSmallThreshold = "RoomEditor.ARegionTooSmallThreshold";
        const string k_editorPref_smoothBThreshold = "RoomEditor.SmoothBThreshold";
        const string k_editorPref_smoothAThreshold = "RoomEditor.SmoothAThreshold";

        #endregion

        TilesSetFilter m_tilesSetFilterA;
        TilesSetFilter m_tilesSetFilterB;

        string[] m_tileConfigOptions;
        ITileConfig m_selectedConfig;
        TilesSetData m_selectedSetData;
        string[] m_tileTypeOptions;

        public void Init(object parentContainer)
        {
            ParentContainer = parentContainer;
            LoadPrefs();
        }

        public void Terminate() { }

        public void SetActive(bool active) { }

        public void OnGUI(float width)
        {
            if (!RoomInitialized)
                return;

            GeneratorTools();
        }

        void LoadPrefs()
        {
            m_useRandomSeed = EditorPrefs.GetBool(k_editorPref_useRandomSeed, true);
            m_seed = EditorPrefs.GetString(k_editorPref_seed, "");

            m_randomFillPercent = EditorPrefs.GetInt(k_editorPref_randomFillPercent, 50);
            m_bRegionsTooSmallThreshold = EditorPrefs.GetInt(k_editorPref_bRegionsTooSmallThreshold, 1);
            m_aRegionTooSmallThreshold = EditorPrefs.GetInt(k_editorPref_aRegionsTooSmallThreshold, 1);

            m_smoothAThreshold = EditorPrefs.GetInt(k_editorPref_smoothAThreshold, 4);
            m_smoothBThreshold = EditorPrefs.GetInt(k_editorPref_smoothBThreshold, 5);
        }

        void GeneratorTools()
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                if (GUILayout.Button("Reset Default Values", GUILayout.Width(150)))
                {
                    DeletePrefs();
                    LoadPrefs();
                }
                
                _GeneratorToolsInnerGUI();
                
                if (check.changed)
                {
                    SavePrefs();

                    RoomEditor.CurrentEditor.UpdateAndBuildPreview();
                }
            }
        }

        static void DeletePrefs()
        {
            EditorPrefs.DeleteKey(k_editorPref_useRandomSeed);
            EditorPrefs.DeleteKey(k_editorPref_seed);
            EditorPrefs.DeleteKey(k_editorPref_randomFillPercent);
            EditorPrefs.DeleteKey(k_editorPref_bRegionsTooSmallThreshold);
            EditorPrefs.DeleteKey(k_editorPref_aRegionsTooSmallThreshold);
            EditorPrefs.DeleteKey(k_editorPref_smoothBThreshold);
            EditorPrefs.DeleteKey(k_editorPref_smoothAThreshold);
        }

        void SavePrefs()
        {
            EditorPrefs.SetBool(k_editorPref_useRandomSeed, m_useRandomSeed);
            EditorPrefs.SetString(k_editorPref_seed, m_seed);
            EditorPrefs.SetInt(k_editorPref_randomFillPercent, m_randomFillPercent);
            EditorPrefs.SetInt(k_editorPref_bRegionsTooSmallThreshold, m_bRegionsTooSmallThreshold);
            EditorPrefs.SetInt(k_editorPref_aRegionsTooSmallThreshold, m_aRegionTooSmallThreshold);
            EditorPrefs.SetInt(k_editorPref_smoothBThreshold, m_smoothBThreshold);
            EditorPrefs.SetInt(k_editorPref_smoothAThreshold, m_aRegionTooSmallThreshold);
        }

        void _GeneratorToolsInnerGUI()
        {
            FilterGUI();
                        
            if (m_selectedConfig == null)
                return;

            if (m_tileTypeOptions == null)
                return;
            if (!m_tilesSetFilterA.FilterIdx.IsInRange(m_tileTypeOptions)
                || !m_tilesSetFilterB.FilterIdx.IsInRange(m_tileTypeOptions))
                return;

            var nameA = m_tileTypeOptions[m_tilesSetFilterA.FilterIdx];
            var nameB = m_tileTypeOptions[m_tilesSetFilterB.FilterIdx];

            using (new GUILayout.VerticalScope(EditorStyles.helpBox))
            {
                using (new GUILayout.HorizontalScope())
                {
                    if (GUILayout.Button("Random-Fill"))
                    {
                        Undo.RecordObject(RoomConfig, "Random Fill");
                        RoomInstance.RandomFillRoomLevel(CurrentLevel, m_randomFillPercent, 
                            m_tilesSetFilterA, m_tilesSetFilterB,
                            m_useRandomSeed ? null : m_seed);
                    }

                    using (var disableScope = new EditorGUI.DisabledScope(m_useRandomSeed))
                    {
                        GUILayout.Label("Seed: ", GUILayout.Width(50));
                        m_seed = EditorGUILayout.TextField(m_seed);
                    }

                    m_useRandomSeed = EditorGUILayout.Toggle("Use Random Seed", m_useRandomSeed);
                }

                m_randomFillPercent = EditorGUILayout.IntSlider("Fill Percentage: ", m_randomFillPercent, 0, 100);
            }

            GUILayout.Space(5.0f);
            //GUILayout.BeginVertical(EditorStyles.helpBox);
            using (new GUILayout.HorizontalScope(EditorStyles.helpBox))
            {
                if (GUILayout.Button("Smooth"))
                {
                    Undo.RecordObject(RoomConfig, "Smooth");
                    RoomInstance.SmoothMap(CurrentLevel, m_tilesSetFilterA, m_tilesSetFilterB, 
                        m_smoothAThreshold, m_smoothBThreshold);
                }

                float min = m_smoothAThreshold;
                float max = m_smoothBThreshold;
                EditorGUILayout.MinMaxSlider($"A|B Threshold ({nameA}|{nameB}) ", ref min, ref max, 0, 8);
                m_smoothAThreshold = Mathf.RoundToInt(min);
                m_smoothBThreshold = Mathf.RoundToInt(max);
            }

            //GUILayout.EndVertical();
            GUILayout.Space(5.0f);
            using (new GUILayout.VerticalScope(EditorStyles.helpBox))
            {
                using (new GUILayout.HorizontalScope())
                {
                    if (GUILayout.Button($"Destroy Small B-Regions ({nameB})"))
                    {
                        Undo.RecordObject(RoomConfig, $"Destroy Small B-Regions");
                        RoomInstance.DestroyRegionsThatAreTooSmall(CurrentLevel, m_tilesSetFilterB, m_tilesSetFilterA,
                            m_bRegionsTooSmallThreshold);
                    }

                    GUILayout.Label("Threshold: ", GUILayout.Width(75));
                    m_bRegionsTooSmallThreshold = EditorGUILayout.IntSlider(m_bRegionsTooSmallThreshold, 0, 50);
                }

                using (new GUILayout.HorizontalScope())
                {
                    if (GUILayout.Button($"Destroy Small A-Regions ({nameA})"))
                    {
                        Undo.RecordObject(RoomConfig, $"Destroy Small A-Regions");
                        RoomInstance.DestroyRegionsThatAreTooSmall(CurrentLevel, m_tilesSetFilterA, m_tilesSetFilterB,
                            m_aRegionTooSmallThreshold);
                    }

                    GUILayout.Label("Threshold: ", GUILayout.Width(75));
                    m_aRegionTooSmallThreshold = EditorGUILayout.IntSlider(m_aRegionTooSmallThreshold, 0, 50);
                }
            }

            GUILayout.Space(5.0f);

            if (GUILayout.Button($"Connect Areas ({nameA})", GUILayout.Width(150)))
            {
                Undo.RecordObject(RoomConfig, "Connect Areas");
                RoomInstance.ConnectClosestAreas(CurrentLevel, m_tilesSetFilterA);
            }
        }
        void FilterGUI()
        {
            CacheTileConfigs(RoomConfig, out var configs, out m_tileConfigOptions);
            _SelectTileConfigGUI(configs);

            if (m_tilesSetFilterA.Data.TileConfig?.Result == null)
                m_tilesSetFilterA = RoomInstance.RoomSettings.GetFilter(GeometryType.Floor);
            if (m_tilesSetFilterB.Data.TileConfig?.Result == null)
                m_tilesSetFilterB = RoomInstance.RoomSettings.GetFilter(GeometryType.Auto);

            if (m_tileTypeOptions == null)
                return;
            using (new GUILayout.HorizontalScope())
            {
                var idxA = (ushort) EditorGUILayout.Popup("Filter A", m_tilesSetFilterA.FilterIdx, m_tileTypeOptions);
                var idxB = (ushort) EditorGUILayout.Popup("Filter B", m_tilesSetFilterB.FilterIdx, m_tileTypeOptions);
                if (idxA != m_tilesSetFilterA.FilterIdx || idxB != m_tilesSetFilterB.FilterIdx)
                    UpdateFilters(idxA, idxB);
            }
        }

        void UpdateFilters(ushort idxA, ushort idxB)
        {
            m_tilesSetFilterA = new TilesSetFilter(m_selectedSetData, idxA);
            m_tilesSetFilterB = new TilesSetFilter(m_selectedSetData, idxB);
        }

        void _SelectTileConfigGUI(ITileConfig[] configs)
        {
            if (SelectTileConfigGUI(configs, "Config", m_tileConfigOptions, ref m_selectedConfig) == ChangeCheck.Changed)
                _OnConfigSelected(RoomConfig, m_selectedConfig, out m_selectedSetData, out m_tileTypeOptions);
        }
    }
}