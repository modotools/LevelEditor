using System.IO;
using Core.Editor.Utility;
using Core.Extensions;
using Core.Unity.Extensions;
using Core.Unity.Utility;
using Level.PlatformLayer;
using Level.Room;
using UnityEditor;
using UnityEngine;
using static Level.Editor.Settings.LevelEditorSettingsAccess;

namespace Level.Editor.Room.Operations
{
    public static class RoomOperations
    {
        const string k_defaultRoomName = "Room";

        public static RoomConfig CreateRoomAsset(this RoomConfigData configData, string path)
        {
            FixNameIfNeeded(ref configData);

            var room = ScriptableObject.CreateInstance<RoomConfig>();
            room.RoomConfigData = configData;

            CreateRoomDirectoryAndFile(path, configData.Name, room);
            AddPlatformLayerConfig(room, 0, configData);

            AssetDatabase.SaveAssets();
            return room;
        }

        static void FixNameIfNeeded(ref RoomConfigData configData)
        {
            if (configData.Name.IsNullOrEmpty())
                configData.Name = k_defaultRoomName;
        }


        public static void MoveRoomAsset(RoomConfig config, string path)
        {
            CreateRoomDirectory(path, config.name, out var filePathName);
            // Debug.Log(filePathName);
            AssetDatabase.MoveAsset(AssetDatabase.GetAssetPath(config), filePathName);
            AssetDatabase.SaveAssets();
        }

        static void CreateRoomDirectory(string path, string name, out string filePathName)
        {
            // Debug.Log(path);
            Directory.CreateDirectory(path);

            filePathName = $"{path}{name}.asset";
            var fileNr = 0;
            while (File.Exists(filePathName))
            {
                filePathName = $"{path}{name}({fileNr}).asset";
                ++fileNr;
            }
        }

        static void CreateRoomDirectoryAndFile(string path, string name, RoomConfig room)
        {
            CreateRoomDirectory(path, name, out var filePathName);
            AssetDatabase.CreateAsset(room, $"{filePathName}");
        }

        public static void ChangeLevelCount(this RoomConfig config, int newLevelCount)
        {
            var data = config.RoomConfigData;
            var prevCount = config.PlatformLayerConfigs.Count;

            // how many levels does the room have
            newLevelCount = Mathf.Clamp(newLevelCount, 1, config.MaxPlatformCount);
            if (newLevelCount == prevCount)
                return;

            for (var i = prevCount; i < newLevelCount; i++) 
                AddPlatformLayerConfig(config, i, data);

            for (var i = prevCount; i > newLevelCount; i--)
            {
                var platform = config.PlatformLayerConfigs[i];
                if (ReferenceEquals(platform.Parent(), config))
                    platform.DestroyEx();

                config.PlatformLayerConfigs.RemoveAt(i - 1);
            }
            AssetDatabase.SaveAssets();
        }

        public static void AddPlatformLayerConfig(RoomConfig config, int lvl, RoomConfigData data)
        {
            var platform = ScriptableObject.CreateInstance<PlatformLayerConfig>();
            platform.name = $"{config.name} lvl {lvl}";
            platform.SetTilesSet(data.Settings.TileSet);
            platform.SetGrid(data.Settings.Grid);
            platform.Position = Vector2Int.zero;
            platform.InitSize(data.Size.Vector2Int());
            platform.AddToAsset(config);

            if (lvl < config.PlatformLayerConfigs.Count)
                config.PlatformLayerConfigs[lvl] = platform;
            else
            {
                Debug.Assert(lvl == config.PlatformLayerConfigs.Count);
                config.PlatformLayerConfigs.Add(platform);
            }
        }
        
        
        public static void UpdateHighlighter(this RoomInstanceComponent roomInstanceComponent, bool loaded)
        {
            var styles = loaded
                ? RoomHierarchyStyleInitiallyLoaded
                : RoomHierarchyStyleNotLoaded;

            var hasHighlighter = roomInstanceComponent.TryGetComponent<HierarchyHighlighter>(out var highlighter);
            if (!styles.AnyActive)
            {
                if (hasHighlighter)
                    highlighter.DestroyEx();
                return;
            }

            if (!hasHighlighter)   
                highlighter = roomInstanceComponent.gameObject.AddComponent<HierarchyHighlighter>();
            highlighter.SetStyles(styles);
        }

    }
}