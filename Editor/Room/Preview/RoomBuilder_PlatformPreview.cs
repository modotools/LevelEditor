using Core.Events;
using Core.Unity.Extensions;
using Core.Unity.Utility;
using Level.Editor.Tiles;
using Level.PlatformLayer;
using Level.Room;
using ScriptableUtility;
using UnityEngine;
using static Core.Extensions.CollectionExtensions;

namespace Level.Editor.Room
{
    public class RoomBuilder_PlatformPreview : IEventListener<TileDrawing.TilesDrawn>
    {
        public RoomBuilder Builder { get; }
        public PlatformLayerConfig PlatformLayerConfig { get; }

        readonly bool m_createPreviewObjects;

        GameObject m_previewObject;
        IContext m_previewContext;
        Mesh m_previewMesh;

        IDefaultAction m_buildPlatformAction;

        #region Init
        public RoomBuilder_PlatformPreview(RoomBuilder builder, 
            PlatformLayerConfig platformLayerConfig,
            bool createPreviewObjects)
        {
            Builder = builder;
            PlatformLayerConfig = platformLayerConfig;
            m_createPreviewObjects = createPreviewObjects;

            EventMessenger.AddListener(this);

            EnsurePreviewObjects();
            //InitMeshData();
        }

        //void InitMeshData()
        //{
        //    Builder.MeshData.Value = MeshData.Default;
        //    Builder.TempMeshData.Value = MeshData.Default;
        //}

        public void Terminate()
        {
            if (m_createPreviewObjects)
            {
                if (m_previewObject != null)
                    m_previewObject.DestroyEx();
                if (m_previewMesh != null)
                    m_previewMesh.DestroyEx();

                m_previewObject = null;
                m_previewMesh = null;
            }

            EventMessenger.RemoveListener(this);
        }
        #endregion
        #region Setup
        void EnsurePreviewObjects()
        {
            if (!m_createPreviewObjects)
                return;
            if (m_previewMesh == null)
                m_previewMesh = new Mesh();
            if (m_previewObject == null)
            {
                //Debug.Log(ParentContainer.GetHashCode());
                m_previewObject = new GameObject("Preview") {hideFlags = HideFlags.DontSave | HideFlags.NotEditable};
                m_previewObject.AddComponent<CleanupMarker>();

                var mf = m_previewObject.AddComponent<MeshFilter>();
                mf.sharedMesh = m_previewMesh;

                var mr = m_previewObject.AddComponent<MeshRenderer>();
                mr.sharedMaterial = Builder.Material;

                var prevContext = m_previewObject.AddComponent<ContextComponent>();

                Add(ref prevContext.Editor_Containers, 
                    new RefIVariableContainer(){ Result = Builder });

                m_previewContext = prevContext;
            }
            else if (m_previewObject.TryGetComponent(out MeshFilter mf))
                mf.sharedMesh = m_previewMesh;

            m_previewContext.Init();
            Builder.SetContext(m_previewContext);
        }
        void EnsureContextAndVariables()
        {
            // make sure context is correct
            Builder.SetContext(m_previewContext);

            // make sure variable point to correct objects
            Builder.Platform.Value = PlatformLayerConfig;
        }
        void EnsureAction()
        {
            if (m_buildPlatformAction == null)
                m_buildPlatformAction = Builder.CreatePlatformBuilderAction();
        }
        #endregion

        public void Build()
        {
            EnsurePreviewObjects();
            EnsureContextAndVariables();
            EnsureAction();
            // todo: init this in graph
            Builder.Edges.Value.Data.Clear();

            //Debug.Log($"m_buildPlatformAction {m_buildPlatformAction}");
            m_buildPlatformAction?.Invoke();
        }

        public void OnEvent(TileDrawing.TilesDrawn eventType) => Build();

        public void SetInputData(GameObject go, Mesh m, IContext c)
        {
            m_previewObject = go;
            //Debug.Log($"{go.transform.parent.parent}/{go} {c}");
            m_previewMesh = m;
            m_previewContext = c;
        }
    }
}