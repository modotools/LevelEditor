using Core.Events;
using Level.Editor.Tiles;
using Level.Editor.Tools;
using Level.Room;
using Level.Room.Operations;
using ScriptableUtility;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Level.Editor.Room
{
    public class RoomBuilder_RoomPreview : 
        IEventListener<PlatformSizePositionChanged>,
        IEventListener<TileDrawing.TilesDrawn>
    {
        public RoomBuilder Builder => RoomInstance.Builder;
        public RoomInstanceComponent RoomInstance { get; }

        public bool IsDirty => m_isDirty;
        bool m_isDirty;

        IContext m_context;
        #region Init
        public RoomBuilder_RoomPreview(RoomInstanceComponent roomInstanceComponent)
        {
            RoomInstance = roomInstanceComponent;
            EventMessenger.AddListener<TileDrawing.TilesDrawn>(this);
            EventMessenger.AddListener<PlatformSizePositionChanged>(this);

            InitContext();
        }

        void InitContext()
        {
            RoomInstance.Get(out m_context);
            if (m_context == null)
            {
                Debug.LogError($"RoomInstance {RoomInstance} m_context == null!");
                return;
            }   

            m_context.Init();
            Builder.SetContext(m_context);
        }

        public void Terminate()
        {
            EventMessenger.RemoveListener<TileDrawing.TilesDrawn>(this);
            EventMessenger.RemoveListener<PlatformSizePositionChanged>(this);
        }

        #endregion

        public void Build()
        {
            m_isDirty = false;
            RoomInstance.Build();
            EditorSceneManager.MarkSceneDirty(RoomInstance.gameObject.scene);
        }

        public void OnEvent(TileDrawing.TilesDrawn eventType) => m_isDirty = true;
        public void OnEvent(PlatformSizePositionChanged eventType) => m_isDirty = true;
    }
}