﻿using Core.Editor.Inspector;
using Core.Editor.Utility;
using Core.Extensions;
using Core.Types;
using Core.Editor.Interface;
using Level.Texture;
using UnityEditor;
using UnityEngine;

namespace Level.Editor.Texture
{
    [CustomEditor(typeof(UVLayoutConfig))]
    public class UVLayoutConfigInspector : BaseInspector<UVLayoutConfigEditor>
    {
        public override DefaultGUISettings DefaultGUISettings => new DefaultGUISettings
        {
            DrawDefaultGUIFoldout = true, CanUnlockDefaultGUI = true, DefaultGUIEnabled = false
        };
    }

    public class UVLayoutConfigEditor : BaseEditor<UVLayoutConfig>
    {
        public override void OnGUI(float width)
        {
            base.OnGUI(width);

            var texTypeData = SerializedObject.FindProperty(UVLayoutConfig.Editor_UVLayoutPropName);
            var deleteIdx = -1;
            var change = false;

            for (var i = 0; i < Target.Editor_UVLayoutData.Length; i++)
            {
                var elementProp = texTypeData.GetArrayElementAtIndex(i);
                var expanded = elementProp.isExpanded;
                using (new FoldoutScope(ref expanded, Target.Editor_UVLayoutData[i].Name))
                {
                    elementProp.isExpanded = expanded;
                    if (!expanded) 
                        continue;

                    ref var txType = ref Target.Editor_UVLayoutData[i];

                    var nameProp = elementProp.FindPropertyRelative(nameof(UVLayoutConfig.UVLayoutData.Name));
                    var idProp = elementProp.FindPropertyRelative(nameof(UVLayoutConfig.UVLayoutData.IDs));
                    var dimProp = elementProp.FindPropertyRelative(nameof(UVLayoutConfig.UVLayoutData.Dimensions));
                    var splitProp = elementProp.FindPropertyRelative(nameof(UVLayoutConfig.UVLayoutData.SplitTiles));

                    using (var check = new EditorGUI.ChangeCheckScope())
                    {
                        using (new EditorGUILayout.HorizontalScope())
                        {
                            if (GUILayout.Button("X", GUILayout.Width(20f)))
                                deleteIdx = i;
                            GUILayout.Space(15f);
                            EditorGUILayout.PropertyField(nameProp);
                        }
                        EditorGUILayout.PropertyField(idProp, true);
                        EditorGUILayout.PropertyField(dimProp);
                        EditorGUILayout.PropertyField(splitProp);

                        change |= check.changed;
                        if (check.changed)
                            SerializedObject.ApplyModifiedProperties();
                    }

                    var usedConfiguration = txType.Configuration_TextureOffsets != null &&
                                            txType.Configuration_TextureOffsets.Length == 16;

                    var useConfiguration = GUILayout.Toggle(usedConfiguration, "Use configuration");

                    if (useConfiguration != usedConfiguration)
                    {
                        txType.Configuration_TextureOffsets = useConfiguration
                            ? new Vector2[16]
                            : null;

                        change = true;
                    }

                    if (usedConfiguration)
                        change |= ConfigOffsetGUI(elementProp) == ChangeCheck.Changed;
                }
            }

            if (deleteIdx != -1)
            {
                CollectionExtensions.RemoveAt(ref Target.Editor_UVLayoutData, deleteIdx);
                change = true;
            }
            if (GUILayout.Button("Add"))
            {
                CollectionExtensions.Add(ref Target.Editor_UVLayoutData, new UVLayoutConfig.UVLayoutData());
                change = true;
            }
            if (change)
                OnChanged();
        }

        ChangeCheck ConfigOffsetGUI(SerializedProperty elementProp)
        {
            var changed = false;
            for (var i = 0; i < 16; i++)
            {
                //ref var meshData = ref target.Editor_Get(i);
                using (new EditorGUILayout.HorizontalScope())
                {
                    EditorGUILayout.LabelField($"{i}",GUILayout.Width(20f));
                    ConfigBoxGUI.DrawConfigurationBox(i);

                    using (new EditorGUILayout.VerticalScope())
                    {
                        var offsetProp = elementProp
                            .FindPropertyRelative(nameof(UVLayoutConfig.UVLayoutData.Configuration_TextureOffsets))
                            .GetArrayElementAtIndex(i);

                        using (var check = new EditorGUI.ChangeCheckScope())
                        {
                            EditorGUILayout.PropertyField(offsetProp, GUIContent.none);
                            changed |= check.changed;
                            if (check.changed) 
                                SerializedObject.ApplyModifiedProperties();
                        }
                    }
                }
                CustomGUI.HSplitter();
                GUILayout.Space(5f);
            }

            return changed ? ChangeCheck.Changed : ChangeCheck.NotChanged;
        }
    }
}
