using Core;
using Core.Editor.Utility;
using Core.Editor.Utility.GUITools;
using Level.Objects;
using UnityEditor;
using UnityEngine;
using static Level.Objects.ObjectContextMarker;

namespace Level.Editor.Room
{
    [CustomEditor(typeof(ObjectContextMarker))]
    [CanEditMultipleObjects]
    public class RoomContextTool : UnityEditor.Editor
    {
        ObjectContextMarker m_lastTarget;
        ObjectPlacingContext m_lastPlacingContext;
        TestPlacing m_lastTestPlacing;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (target == null)
                return;
            if (m_lastTarget == null)
                InitTarget();

            DebugString debugString = default;
            if (m_lastTarget.TestPlacing != m_lastTestPlacing)
                LevelObjectManagement.SwitchPlacing(ref debugString, m_lastTarget.TestPlacing);
            if (m_lastTarget.PlacingContext != m_lastPlacingContext)
                LevelObjectManagement.SwitchSelectionToContext(ref debugString, m_lastTarget.PlacingContext);
        }

        void InitTarget()
        {
            m_lastTarget = target as ObjectContextMarker;
            Debug.Assert(m_lastTarget != null);
            m_lastPlacingContext = m_lastTarget.PlacingContext;
            m_lastTestPlacing = m_lastTarget.TestPlacing;
        }

        public void OnSceneGUI()
        {
            if (!(target is ObjectContextMarker marker))
                return;

            if (Selection.activeGameObject != marker.gameObject)
                return;

            LevelObjectManagement.PlacingContext = marker.PlacingContext;

            var markerPos = marker.transform.position;

            var screenPos = CustomHandles.CalculateScreenPosition(markerPos, out var z);
            screenPos.y = Screen.height - screenPos.y;

            const string spacing = "   |   ";
            const string sceneTxt = nameof(ObjectPlacingContext.Scene);
            const string roomTxt = nameof(ObjectPlacingContext.Room);
            const string noneTxt = nameof(ObjectPlacingContext.None);

            const string onGroundTxt = nameof(TestPlacing.OnGround);
            const string noWallTxt = nameof(TestPlacing.NoWall);

            var r = CustomHandles.GetTextRect(screenPos, $"{sceneTxt}{spacing}{roomTxt}{spacing}{noneTxt}\n", EditorStyles.toolbarButton);
            r.height *= 2f;

            Handles.BeginGUI();
            using (new GUILayout.AreaScope(r))
            {
                using (new GUILayout.VerticalScope())
                {
                    PlacingContextGUI(marker, sceneTxt, roomTxt, noneTxt);
                    if (marker.PlacingContext != ObjectPlacingContext.None)
                        TestPlacingGUI(marker, onGroundTxt, noWallTxt, noneTxt);
                }
            }
            Handles.EndGUI();
        }

        void PlacingContextGUI(ObjectContextMarker marker, string sceneTxt, string roomTxt, string noneTxt)
        {
            using (new GUILayout.HorizontalScope())
            {
                var ctx = marker.PlacingContext;
                if (CustomGUI.ActivityButton(ctx == ObjectPlacingContext.Scene, sceneTxt,
                    options: GUILayout.Width(50f)))
                    ctx = ObjectPlacingContext.Scene;
                if (CustomGUI.ActivityButton(ctx == ObjectPlacingContext.Room, roomTxt,
                    options: GUILayout.Width(50f)))
                    ctx = ObjectPlacingContext.Room;
                if (CustomGUI.ActivityButton(ctx == ObjectPlacingContext.None, noneTxt,
                    options: GUILayout.Width(50f)))
                    ctx = ObjectPlacingContext.None;
                if (ctx != marker.PlacingContext)
                    EditorApplication.delayCall += () =>
                    {
                        DebugString debugString = default;
                        m_lastPlacingContext = ctx;
                        LevelObjectManagement.SwitchSelectionToContext(ref debugString, ctx);
                        debugString.Print();
                    };
            }
        }

        void TestPlacingGUI(ObjectContextMarker marker, string onGroundTxt, string noWallTxt, string noneTxt)
        {
            using (new GUILayout.HorizontalScope())
            {
                var testPlacing = marker.TestPlacing;
                if (CustomGUI.ActivityButton(testPlacing == TestPlacing.OnGround, onGroundTxt,
                    options: GUILayout.Width(50f)))
                    testPlacing = TestPlacing.OnGround;
                if (CustomGUI.ActivityButton(testPlacing == TestPlacing.NoWall, noWallTxt,
                    options: GUILayout.Width(50f)))
                    testPlacing = TestPlacing.NoWall;
                if (CustomGUI.ActivityButton(testPlacing == TestPlacing.None, noneTxt,
                    options: GUILayout.Width(50f)))
                    testPlacing = TestPlacing.None;
                if (testPlacing != marker.TestPlacing)
                    EditorApplication.delayCall += () =>
                    {
                        DebugString debugString = default;
                        m_lastTestPlacing = testPlacing;
                        LevelObjectManagement.SwitchPlacing(ref debugString, testPlacing);
                        // debugString.Print();
                    };
            }
        }
    }
}