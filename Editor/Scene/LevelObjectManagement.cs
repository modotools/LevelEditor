using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Core;
using Core.Editor.Utility;
using Core.Extensions;
using Core.Types;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using Core.Unity.Utility.GUITools;
using JetBrains.Annotations;
using Level.Objects;
using Level.Room;
using ScriptableUtility;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using static Core.Extensions.CollectionExtensions;
using static Level.Editor.Room.LevelObjects_SceneContext;
using static Level.Editor.Room.SceneRoomManagement;
using static Level.Objects.ObjectContextMarker;
using Object = UnityEngine.Object;

namespace Level.Editor.Room
{
    public static class LevelObjectManagement
    {
        public static ObjectPlacingContext PlacingContext;
        public static bool AutoUpdateRoomBinding;
        static readonly List<RoomObjectSelection> k_selection = new List<RoomObjectSelection>();

        static bool m_workingOnSelection;

        const string k_autoUpdatePrefKey = nameof(LevelObjectManagement) + "." + nameof(AutoUpdateRoomBinding);
        //static DebugString m_debugString;

        #region Fluid Mouse Drag Handling
        // we postpone hierarchy-movements to when mouse is released or you wont have fluid mouse movement
        static bool m_mouseDown;
        public static void DuringSceneGUI()
        {
            using (var scope = new HandleGUIScope())
            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("Room Binding", GUILayout.Width(100f));
                var newAutoUpdate = GUILayout.Toggle(AutoUpdateRoomBinding,"Auto Update", "Button", 
                    GUILayout.Width(100f));
                if (newAutoUpdate != AutoUpdateRoomBinding)
                {
                    AutoUpdateRoomBinding = newAutoUpdate;
                    EditorPrefs.SetBool(k_autoUpdatePrefKey, AutoUpdateRoomBinding);
                }
                if (!AutoUpdateRoomBinding && GUILayout.Button("Update", GUILayout.Width(75f)))
                {
                    DebugString debugString = default;
                    UpdateRoomObjectSelection(ref debugString);
                }
            }
            if (!AutoUpdateRoomBinding)
                return;
            
            m_mouseDown = Event.current.type switch
            {
                EventType.MouseDown => true,
                EventType.MouseUp => false,
                _ => m_mouseDown
            };

            if (Event.current.isKey && Event.current.keyCode == KeyCode.Delete)
                DeleteSelectedRoomObjects();
        }

        static void DeleteSelectedRoomObjects()
        {
            if (m_workingOnSelection)
                return;
            m_workingOnSelection = true;
            
            for (var i = 0; i < k_selection.Count; i++)
            {
                var s = k_selection[i];
                if (s.Object == null || s.Marker == null)
                    continue;
                if (s.Marker.PlacingContext != ObjectPlacingContext.Room)
                    continue;

                Event.current.Use();
                RemoveFromRoomObjects(ref s, false);
                k_selection[i] = s;
            }

            m_workingOnSelection = false;
        }

        #endregion

        #region Hooks
        public static void Update(ref DebugString debugString)
        {
            if (!AutoUpdateRoomBinding)
                return;
            if (m_mouseDown || m_workingOnSelection)
                return;
            m_workingOnSelection = true;
            // debugString.AddLog($"k_selection.Count {k_selection.Count}");
            var objsChanged = false;
            for (var i = 0; i < k_selection.Count; i++)
            {
                var s = k_selection[i];
                if (s.Object == null)
                    continue;
                var diff = s.Object.transform.position - s.LastPosition;
                if (diff.sqrMagnitude < float.Epsilon)
                    continue;

                var prevObj = s.Object;
                UpdateRoomObject(ref debugString, ref s, out _);
                objsChanged |= prevObj != s.Object;
                
                if (s.Object.TryGetComponent(out s.Marker))
                    s.LevelObjectsConfig = s.Marker.LevelObjectsConfig;

                var tr = s.Object.transform;
                s.LastPosition = tr.position;
                k_selection[i] = s;
            }
            foreach (var s in k_selection)
                UpdatePosition(s);
            
            if (objsChanged)
                Selection.objects = k_selection.Select(a => a.Object as Object).ToArray();
            m_workingOnSelection = false;
        }

        public static void OnSelectionChanged(ref DebugString debugString)
        {
            if (!AutoUpdateRoomBinding)
                return;
            UpdateRoomObjectSelection(ref debugString);
        }

        public static void OnHierarchyChanged(ref DebugString debugString)
        {
            if (!AutoUpdateRoomBinding)
                return;
            UpdateRoomObjectSelection(ref debugString);
        }

        #endregion

        public static void InitScene(ref DebugString debugString)
        {
            AutoUpdateRoomBinding = EditorPrefs.GetBool(k_autoUpdatePrefKey);
            EnsureSceneMapper();

            var sceneRooms = Object.FindObjectsOfType<RoomObjectsDataComponent>();
            foreach (var r in sceneRooms)
            {
                EnsureRoomReferenceContext(r);
                r.Editor_SpawnObjects();
            }
            RemoveObsoleteLevelObjectConfigs(ref debugString, sceneRooms);
        }
        
        static void EnsureSceneMapper()
        {
            var sceneGo = SceneContext.SceneRoot?.gameObject;
            if (sceneGo != null && !sceneGo.TryGet(out RoomObjectMapper mapper))
                sceneGo.AddComponent<RoomObjectMapper>();
        }

        public static void RemoveObsoleteLevelObjectConfigs(ref DebugString debugString, 
            IEnumerable<RoomObjectsDataComponent> sceneRooms)
        {
            using (var validLevelObjectsScoped = SimplePool<List<LevelObjectsConfig>>.I.GetScoped())
            {
                var validLevelObjects = validLevelObjectsScoped.Obj;
                validLevelObjects.AddRange(from sr in sceneRooms select sr.LvlObjectsConfigScene);
                var sceneConfig = SceneContext.LevelObjectsConfig;
                if (sceneConfig == null)
                {
                    debugString.AddError("Scene config null!");
                    return;
                }
                ref var sceneLvlObjectsConfigs = ref sceneConfig.Editor_LevelObjectsConfigs;
                for (var i = sceneLvlObjectsConfigs.Length - 1; i >= 0; i--)
                {
                    var lvlObjectsConfig = sceneLvlObjectsConfigs[i];
                    if (validLevelObjects.Contains(lvlObjectsConfig))
                        continue;

                    debugString.AddLog($"Removing {lvlObjectsConfig}, because Room not in the scene\n");
                    RemoveAt(ref sceneLvlObjectsConfigs, i);
                    lvlObjectsConfig.DestroyEx();
                    EditorUtility.SetDirty(sceneConfig);
                }

                if (EditorUtility.IsDirty(sceneConfig))
                    AssetDatabase.SaveAssets();
            }
        }

        public static void SwitchSelectionToContext(ref DebugString debugString, ObjectPlacingContext ctx)
        {
            //m_debugString.Reset();
            if (m_workingOnSelection)
                return;
            m_workingOnSelection = true;
            var objsChanged = false;
            debugString.AddLog($"SwitchSelectionToContext {ctx}");

            PlacingContext = ctx;
            for (var i = 0; i < k_selection.Count; i++)
            {
                var s = k_selection[i];
                if (s.Marker == null)
                {
                    debugString.AddLog($"Ignoring {s.Object} because Marker is not set\n");
                    continue;
                }
                
                if (TryRemoveObjectFromLevelObjects(ref debugString, s) == OperationResult.Error) 
                    continue;
                
                var source = PrefabUtility.GetCorrespondingObjectFromSource(s.Object);
                if (source == null)
                {
                    debugString.AddLog($"Ignoring {s.Object} because it is not a prefab!\n");
                    continue;
                }

                var prevObj = s.Object;
                if (ctx == ObjectPlacingContext.None)
                    ClearObjectContext(ref debugString, ref s);
                else
                {
                    if (s.Marker.PlacingContext == ObjectPlacingContext.None)
                    {
                        s.Marker.PlacingContext = ctx;
                        if (TryGetRoomInstanceContaining(ref debugString, s.Object.transform.position, out var room))
                            TryNestObjectsInRoomBounds(ref debugString, s.Object, source, room);
                    }
                    else
                    {
                        if (s.Marker.PlacingContext == ObjectPlacingContext.Room)
                            RemoveFromRoomObjects(ref s);
                        // add to new level objects config
                        TryNestObjectsInRoomBounds(ref debugString, s.Object, source, s.Marker.CurrentRoom);
                    }
                }

                objsChanged |= s.Object != prevObj;
                s.LevelObjectsConfig = s.Marker.LevelObjectsConfig;
                k_selection[i] = s;
            }

            if (objsChanged)
                Selection.objects = k_selection.Select(a => a.Object as Object).ToArray();
            m_workingOnSelection = false;
        }


        public static void SwitchPlacing(ref DebugString debugString, TestPlacing testPlacing)
        {
            if (m_workingOnSelection)
                return;
            debugString.AddLog($"SwitchPlacing {testPlacing}");

            foreach (var s in k_selection)
            {
                if (s.Marker == null)
                {
                    debugString.AddLog($"Ignoring {s.Object} because Marker is not set\n");
                    continue;
                }
                if (s.LevelObjectsConfig == null)
                {
                    debugString.AddLog($"Ignoring {s.Object} because LevelObjectsConfig is null\n");
                    continue;
                }

                var idx = Array.FindIndex(s.LevelObjectsConfig.Editor_ObjectData, a => a.Editor_InstanceID == s.InstanceID);
                if (idx == -1)
                {
                    debugString.AddLog($"Ignoring {s.Object} because it was not found in LevelObjectsConfig {s.LevelObjectsConfig}\n");
                    continue;
                }

                s.Marker.TestPlacing = testPlacing;
                s.LevelObjectsConfig.Editor_ObjectData[idx].TestPlacing = testPlacing;
            }
        }

        static void UpdateRoomObjectSelection(ref DebugString debugString)
        {
            if (m_workingOnSelection) 
                return;
            
            if (OctreeInstance.Rooms == null && RoomInstances.IsNullOrEmpty())
            {
                debugString.AddLog("No Octree or rooms\n");
                return;
            }

            //using (var prevSelectionScoped = SimplePool<List<RoomObjectSelection>>.I.GetScoped())
            //{
            //    var prevSelection = prevSelectionScoped.Obj;
            //    prevSelection.AddRange(k_selection);
            RemoveDeletedObjects(ref debugString, k_selection);
            k_selection.Clear();

            foreach (var gameObject in Selection.gameObjects)
            {
                if (gameObject == null)
                    continue;
                // ignore prefab and project assets
                var scene = gameObject.scene;
                // var projAsset = scene.handle == 0 
                //                 && scene.rootCount == 0
                //                 && scene.isSubScene;
                
                if (scene.rootCount <= 0)
                    continue;
                
                var sel = ToRoomObjectSelection(gameObject);
                UpdateRoomObject(ref debugString, ref sel, out var potentialRoomObject);
                // debugString.AddLog($"{gameObject} potentialRoomObject {potentialRoomObject}");

                if (potentialRoomObject)
                    AddToSelection(sel);
            }

            foreach (var s in k_selection)
                UpdatePosition(s);

            //m_debugString.Print();
            //}
        }
        
        static void UpdateRoomObject(ref DebugString debugString, ref RoomObjectSelection sel, out bool potentialRoomObject)
        {
            var hasContextMarker = sel.Marker != null;
            potentialRoomObject = hasContextMarker;
            if (hasContextMarker) // already a Room-Object
                UpdateRoomObject(ref debugString, ref sel);
            else
                UpdateUnmanagedObject(ref debugString, ref sel, out potentialRoomObject);
        }

        static void UpdateUnmanagedObject(ref DebugString debugString, ref RoomObjectSelection sel, out bool isValidForNesting)
        {
            isValidForNesting = false;
            var testPos = GetTestPos(sel.Object.transform);
            if (!TryGetRoomInstanceContaining(ref debugString, testPos, out var room))
            {
                debugString.AddLog($"TryGetRoomInstanceContaining failed! {sel.Object}\n");
                return;
            }
            
            isValidForNesting = ObjectForNestingValid(ref debugString, sel.Object, out var prefab,
                room);
            if (!isValidForNesting) 
                return;
            debugString.AddLog($"Object for nesting is valid {sel.Object}\n");


            if (!TryNestObjectsInRoomBounds(ref debugString, sel.Object, prefab, room))
            {
                debugString.AddLog($"TryNestObjectsInRoomBounds failed! {sel.Object}\n");
                return;
            }
        }

        static void UpdateRoomObject(ref DebugString debugString, ref RoomObjectSelection sel)
        {
            if (sel.Marker.PlacingContext == ObjectPlacingContext.None)
                return;

            debugString.AddLog($"adding Object with ObjectContextMarker to selection {sel.Object}\n");

            var testPos = GetTestPos(sel.Object.transform);
            var currentBoundsOK = sel.Marker.CurrentRoom != null && sel.Marker.CurrentRoom.Bounds.Contains(testPos);
            
            if (currentBoundsOK)
            {
                var parent = GetParent(sel.Marker.PlacingContext, sel.Marker.CurrentRoom);
                var parentOK = sel.Object.transform.parent == parent;
                
                if (!parentOK)
                    debugString.AddLog($"Parent {parent} expected for {sel.Object}\n");

                return;
            }
            debugString.AddLog($"Out of Current room Bounds {sel.Object}\n");
            
            RemoveObjectFromLevelObjectsConfig(ref debugString, sel);
            var source = PrefabUtility.GetCorrespondingObjectFromSource(sel.Object);

            if (!TryGetRoomInstanceContaining(ref debugString, testPos, out var room))
                debugString.AddLog($"TryGetRoomInstanceContaining failed! {sel.Object}\n");
            else if (!TryNestObjectsInRoomBounds(ref debugString, sel.Object, source, room))
                debugString.AddLog($"TryNestObjectsInRoomBounds failed! {sel.Object}\n");
            else // all ok
                return;
            // error
            UndoRoomManagement(ref sel);
        }

        static void UpdatePosition(RoomObjectSelection selection)
        {
            var marker = selection.Marker;
            if (selection.LevelObjectsConfig == null || marker == null)
                return;

            Debug.Assert(selection.Object != null);
            var ctx = marker.PlacingContext;
            var tr = selection.Object.transform;

            var parentGo = ctx == ObjectPlacingContext.Room 
                ? marker.CurrentRoom.gameObject 
                : SceneContext.SceneRoot.gameObject;
            
            Debug.Assert(parentGo != null);

            var idx = Array.FindIndex(selection.LevelObjectsConfig.Editor_ObjectData, a => a.Editor_InstanceID == selection.InstanceID);
            if (idx == -1)
                return;

            var parentTr = parentGo.transform;
            ref var od = ref selection.LevelObjectsConfig.Editor_ObjectData[idx];
            od.LocalPosition = parentTr.InverseTransformPoint(tr.position);
            od.LocalRotation = (Quaternion.Inverse(parentTr.rotation) * tr.rotation).eulerAngles;
            od.Scale = tr.lossyScale;

            EditorUtility.SetDirty(selection.LevelObjectsConfig);
        }

        static Vector3 GetTestPos(Transform tr)
        {
            var pos = tr.position;
            // hack for when object is slightly below ground: 
            const float epsilon = 0.001f;
            var epsilonUp = epsilon * Vector3.up;
            var testPos = pos + epsilonUp;

            return testPos;
        }

        static void RemoveDeletedObjects(ref DebugString debugString, IEnumerable<RoomObjectSelection> prevSelection)
        {
            foreach (var s in prevSelection)
            {
                var objectDeleted = s.Object == null;
                if (!objectDeleted)
                    continue;
                RemoveObjectFromLevelObjectsConfig(ref debugString, s);
            }
        }

        static OperationResult TryRemoveObjectFromLevelObjects(ref DebugString debugString, RoomObjectSelection s)
        {
            if (s.Marker.PlacingContext == ObjectPlacingContext.None)
                return OperationResult.OK;
            if (s.Marker.CurrentRoom == null)
            {
                debugString.AddLog($"Ignoring {s.Object} because Room is not set\n");
                return OperationResult.Error;
            }

            if (StageUtility.GetStage(s.Marker.CurrentRoom.gameObject) != StageUtility.GetMainStage())
            {
                debugString.AddLog($"Ignoring {s.Object} because Room is not saved in scene\n");
                return OperationResult.Error;
            }

            RemoveObjectFromLevelObjectsConfig(ref debugString, s);
            return OperationResult.OK;
        }

        static void RemoveObjectFromLevelObjectsConfig(ref DebugString debugString, RoomObjectSelection sel)
        {
            if (sel.LevelObjectsConfig == null)
            {
                debugString.AddError($"Cannot remove from config, because config is null!");
                return;
            }
            
            Debug.Assert(sel.Marker != null);
            if (sel.Marker.RoomObjects == null)
            {
                debugString.AddError($"Cannot remove from config, because RoomObjectDataComponent is null!");
                return;
            }
            // Debug.Assert(sel.Marker.RoomObjects != null);
            var mapper = sel.Marker.PlacingContext == ObjectPlacingContext.Room
                ? sel.Marker.RoomObjects.MapperRoom
                : sel.Marker.RoomObjects.MapperScene;
            
            RemoveObjectFromLevelObjectsConfig(ref debugString, sel.InstanceID, sel.LevelObjectsConfig, 
                mapper, sel.Object);
        }

        static void RemoveObjectFromLevelObjectsConfig(ref DebugString debugString, int objectInstanceID, 
            LevelObjectsConfig fromConfig, RoomObjectMapper mapper,
            [CanBeNull] GameObject gameObject)
        {
            debugString.AddLog($"RemoveObject id {objectInstanceID} fromConfig {fromConfig}\n");

            if (fromConfig == null)
            {
                debugString.AddError($"Could not remove Object instance id {objectInstanceID}, fromConfig is null! \n");
                return;
            }
            var objectIdx = Array.FindIndex(fromConfig.Editor_ObjectData, od => od.Editor_InstanceID == objectInstanceID);
            if (objectIdx == -1)
            {
                debugString.AddError($"Could not remove Object instance id {objectInstanceID} not found\n");
                return;
            }

            if (mapper != null)
            {
                if (gameObject != null && mapper.TryGetPropertyName(gameObject, out var propertyName))
                    mapper.ClearReferenceValue(propertyName);
                else
                    mapper.ClearMissingProperties();
            }
            
            RemoveAt(ref fromConfig.Editor_ObjectData, objectIdx);
            EditorUtility.SetDirty(fromConfig);
        }

        static RoomObjectSelection ToRoomObjectSelection(GameObject gameObject)
        {
            LevelObjectsConfig levelObjectsConfig = null;
            if (gameObject.TryGetComponent(out ObjectContextMarker ocm))
                levelObjectsConfig = ocm.LevelObjectsConfig;
            var selection = new RoomObjectSelection
            {
                InstanceID = gameObject.GetInstanceID(),
                Marker = ocm,
                LevelObjectsConfig = levelObjectsConfig,
                Object = gameObject,
                LastPosition = gameObject.transform.position
            };
            return selection;
        }

        static void AddToSelection(RoomObjectSelection selection)
        {
            if (m_workingOnSelection) 
                return;
            k_selection.Add(selection);
        }

        static void AddToSelection(GameObject gameObject) => AddToSelection(ToRoomObjectSelection(gameObject));

        static bool TryGetRoomInstanceContaining(ref DebugString debugString, Vector3 pos, out IRoomInstance room)
        {
            room = null;

            if (StageUtility.GetCurrentStage() != StageUtility.GetMainStage())
            {
                var roomInstance = RoomEditor.CurrentEditor.RoomInstance;
                if (roomInstance == null || !roomInstance.Bounds.Contains(pos))
                    return false;
                
                room = roomInstance;
                return true;
            }
            
            if (OctreeInstance.Rooms != null)
            {
                debugString.AddLog($"OctreeInstance.Instance exists\n");
                if (OctreeInstance.Rooms.GetObjectEnclosing(pos, out room) == FindResult.Found)
                {
                    if (room.gameObject != null && room.gameObject.activeInHierarchy)
                        return true;
                }
            }
            else debugString.AddLog($"OctreeInstance.Instance does not exists\n");

            debugString.AddLog($"Checking RoomInstances-List\n");
            foreach (var r in RoomInstances)
            {
                if (!r.Bounds.Contains(pos))
                    continue;
                if (!r.gameObject.activeInHierarchy)
                    continue;
                room = r;
                return true;
            }

            return false;
        }

        static bool ObjectForNestingValid(ref DebugString debugString, GameObject selectedGO, out GameObject source, 
            IRoomInstance roomInstance)
        {
            source = null;

            selectedGO.TryGet(out IRoomInstance ric);
            var isRoomObj = ric != null;
            if (isRoomObj)
                return false;

            var currParent = selectedGO.transform.parent;
            var targetParent = GetParent(PlacingContext, roomInstance);
            if (currParent == targetParent)
                return true;
            
            var isRootLvl = currParent == null;
            if (!isRootLvl)
            {
                debugString.AddLog($"Ignoring Room-Object-Management for {selectedGO} because it is not at root level\n");
                return false;
            }   
            source = PrefabUtility.GetCorrespondingObjectFromSource(selectedGO);
            if (source == null)
            {
                debugString.AddLog($"Ignoring Room-Object-Management for {selectedGO} because it is not a prefab\n");
                return false;
            }
            return true;
        }

        static void ClearObjectContext(ref DebugString debugString, ref RoomObjectSelection s)
        {
            GetOrAddMarker(s.Object, out var marker);

            var prevCtx = marker.PlacingContext;
            if (prevCtx == ObjectPlacingContext.Room) 
                RemoveFromRoomObjects(ref s);
            
            marker.ClearPlacingContext();
            UndoRoomManagement(ref s);

            debugString.AddLog($"Cleared PlacingContext for {s.Object}\n");
        }

        static void RemoveFromRoomObjects(ref RoomObjectSelection s, bool cloneObject = true)
        {
            var config = s.Marker.LevelObjectsConfig;
            var mapper = s.Marker.PlacingContext == ObjectPlacingContext.Room
                ? s.Marker.RoomObjects.MapperRoom
                : s.Marker.RoomObjects.MapperScene;
            
            if (RemoveFromRoomObjects(config, mapper, s.Object, cloneObject, out var obj, out var marker) !=
                OperationResult.OK) 
                return;
            
            s.Object = obj;
            s.Marker = marker;
        }

        static OperationResult RemoveFromRoomObjects(LevelObjectsConfig config, RoomObjectMapper mapper, 
            GameObject go, bool cloneObject,
            out GameObject clone, out ObjectContextMarker marker)
        {
            clone = null;
            marker = null;
            PrefabUtility.ApplyPrefabInstance(mapper.gameObject, InteractionMode.AutomatedAction);
            var path = AssetDatabase.GetAssetPath(config.ResolverPrefab);
            var prefabRootGo = PrefabUtility.LoadPrefabContents(path);
            var child = prefabRootGo.transform.Find(go.name);
            if (child != null)
            {
                if (cloneObject)
                {
                    clone = DuplicatePrefabInstance(go);
                    clone.name = go.name;
                    clone.transform.parent = null;
                }
                child.gameObject.DestroyEx();
                PrefabUtility.SaveAsPrefabAsset(prefabRootGo, path);
            }            

            PrefabUtility.UnloadPrefabContents(prefabRootGo);
            if (child == null) // did not find in prefab
                return OperationResult.Error;
            
            marker = clone != null ? clone.GetComponent<ObjectContextMarker>() : null;
            return OperationResult.OK;
        }

        //This method finds the first EditorWindow that's open, and is of the given type.
        //For example, this is how we can search for the "SceneHierarchyWindow" that's currently open (hopefully it *is* actually open).
        public static EditorWindow FindFirst(Type editorWindowType) {
            if (editorWindowType == null)
                throw new ArgumentNullException(nameof(editorWindowType));
            if (!typeof(EditorWindow).IsAssignableFrom(editorWindowType))
                throw new ArgumentException("The given type (" + editorWindowType.Name + ") does not inherit from " + nameof(EditorWindow) + ".");
 
            Object[] openWindowsOfType = Resources.FindObjectsOfTypeAll(editorWindowType);
            if (openWindowsOfType.Length <= 0)
                return null;
 
            EditorWindow window = (EditorWindow) openWindowsOfType[0];
            return window;
        }
 
        //Works with prefab modifications, AND added GameObjects/Components!
        //The PrefabUtility API does not have a method that does this as of Unity 2020.1.3f1 (August 24, 2020). For shame.
        public static GameObject DuplicatePrefabInstance(GameObject prefabInstance) {
            Object[] previousSelection = Selection.objects;
            Selection.objects = new Object[] { prefabInstance };
            Selection.activeGameObject = prefabInstance;
 
            //For performance, you might want to cache this Reflection:
            Type hierarchyViewType = Type.GetType("UnityEditor.SceneHierarchyWindow, UnityEditor");
            EditorWindow hierarchyView = FindFirst(hierarchyViewType);
 
            //Using the Unity Hierarchy View window, we can duplicate our selected objects!
            hierarchyView.SendEvent(EditorGUIUtility.CommandEvent("Duplicate"));
 
            GameObject clone = Selection.activeGameObject;
            Selection.objects = previousSelection;
            return clone;
        }

        static bool TryNestObjectsInRoomBounds(ref DebugString debugString, GameObject go, GameObject prefab, IRoomInstance instance)
        {
            var ctx = PlacingContext;
            if (StageUtility.GetStage(instance.gameObject) != StageUtility.GetMainStage())
                ctx = ObjectPlacingContext.Room;
            
            var operationResult = GetLevelObjectsConfig(ctx, instance, out var lvlObjectsConfig);
            if (operationResult == OperationResult.Error || lvlObjectsConfig == null)
            {
                debugString.AddError($"GetLevelObjectsConfig failed!\n");
                return false;
            }

            var parent = GetParent(ctx, instance);
            go.transform.SetParent(parent);

            GetOrAddMarker(go, out var marker);
            marker.SetPlacingContext(ctx, instance as RoomInstanceComponent, lvlObjectsConfig);
            debugString.AddLog($"Adding {go} to {lvlObjectsConfig}\n");

            // var idx = lvlObjectsConfig.Editor_ObjectData.Length;
            var propName = new PropertyName($"{go.name}_{go.GetInstanceID()}");
            Add(ref lvlObjectsConfig.Editor_ObjectData, new LevelObjectsConfig.ObjectData()
            {
                Editor_InstanceID = go.GetInstanceID(),
                Editor_Instance = go,
                Prefab = prefab,
                Link = new ExposedReference<GameObject> { exposedName = propName, defaultValue = null }
            });

            var mapper = lvlObjectsConfig.GetOrInstantiateMapper(instance.gameObject, ref marker.RoomObjects.MapperRoom);
            if (mapper != null)
                mapper.SetReferenceValue(propName, go);
            
            EditorUtility.SetDirty(lvlObjectsConfig);
            if (ctx == ObjectPlacingContext.Room)
                PrefabUtility.ApplyPrefabInstance(mapper.gameObject, InteractionMode.AutomatedAction);

            return true;
        }

        static Transform GetParent(ObjectPlacingContext ctx, IRoomInstance instance)
        {
            switch (ctx)
            {
                case ObjectPlacingContext.Room:
                {
                    var roomComponent = (Component) instance;
                    roomComponent.TryGetComponent(out RoomObjectsDataComponent roomObjectsDataComponent);
                    return roomObjectsDataComponent.MapperRoom.transform;
                }
                case ObjectPlacingContext.Scene: return instance.PrefabParent;
            }

            return null;
        }

        static void GetOrAddMarker(GameObject go, out ObjectContextMarker objCtxMarker)
        {
            if (go.TryGetComponent(out objCtxMarker)) 
                return;

            objCtxMarker = go.AddComponent<ObjectContextMarker>();
            objCtxMarker.hideFlags = HideFlags.HideAndDontSave;
        }

        static void UndoRoomManagement(ref RoomObjectSelection sel)
        {
            if (sel.Marker != null && sel.Marker.PlacingContext == ObjectPlacingContext.Room)
            {
                var roomObjDataComp = sel.Marker.CurrentRoom.GetComponent<RoomObjectsDataComponent>();
                RemoveFromRoomObjects(sel.LevelObjectsConfig, roomObjDataComp.MapperRoom, sel.Object, true, out sel.Object, out sel.Marker);
            }   
            if (sel.Marker != null && sel.Marker.PlacingContext != ObjectPlacingContext.None)
                sel.Marker.DestroyEx();

            if (sel.Object != null)
                sel.Object.transform.SetParent(null);
        }

        #region public get
        public static OperationResult GetLevelObjectsConfig(ObjectPlacingContext placingCtx, 
            IRoomInstance room, out LevelObjectsConfig lvlObjectsConfig)
        {
            var operationResult = placingCtx == ObjectPlacingContext.Scene
                ? GetLevelObjectsConfigForScene(room, out lvlObjectsConfig)
                : GetLevelObjectsConfigForRoom(room, out lvlObjectsConfig);
            return operationResult;
        }

        // ReSharper disable once SuggestBaseTypeForParameter
        static RoomObjectsDataComponent GetObjectsDataComponent(IRoomInstance room)
        {
            if (!room.gameObject.TryGet<RoomObjectsDataComponent>(out var roomObjectsData))
                roomObjectsData = room.gameObject.AddComponent<RoomObjectsDataComponent>();
            return roomObjectsData;
        }
        #endregion

        #region Asset Setup
        const string k_lvlObjectsPostfix = "_lvlObjects";
        const string k_contextAssetName = "Room";

        public static OperationResult GetLevelObjectsConfigForScene(IRoomInstance room, out LevelObjectsConfig lvlObjectsConfig)
        {
            lvlObjectsConfig = null;
            // 
            if (StageUtility.GetStage(room.gameObject) != StageUtility.GetMainStage())
                return OperationResult.Error;
            if (UpdateSceneContext() == OperationResult.Error)
                return OperationResult.Error;

            var sceneLevelObjectsConfig = SceneContext.LevelObjectsConfig;
            var roomObjectsDataComponent = GetObjectsDataComponent(room);

            GetOrCreateLevelObjectsConfigForScene(sceneLevelObjectsConfig, roomObjectsDataComponent, out lvlObjectsConfig);
            lvlObjectsConfig.Editor_ObjectData ??= new LevelObjectsConfig.ObjectData[0];

            var roomCtx = GetOrCreateRoomContextAsset(lvlObjectsConfig, sceneLevelObjectsConfig);

            lvlObjectsConfig.Editor_LinkContextAssets(roomCtx, sceneLevelObjectsConfig);
            roomCtx.SetContextLink(room.gameObject);
            AssetDatabase.SaveAssets();

            AssertAssetSetupOK(sceneLevelObjectsConfig, roomObjectsDataComponent.LvlObjectsConfigScene);
            return OperationResult.OK;
        }
        
        public static OperationResult GetLevelObjectsConfigForRoom(IRoomInstance room, out LevelObjectsConfig lvlObjectsConfig)
        {
            var roomObjectsDataComponent = GetObjectsDataComponent(room);

            GetOrCreateLevelObjectsConfigForRoom(room.Config, roomObjectsDataComponent, out lvlObjectsConfig);
            lvlObjectsConfig.Editor_ObjectData ??= new LevelObjectsConfig.ObjectData[0];

            var roomCtx = GetOrCreateRoomContextAsset(lvlObjectsConfig, room.Config);

            lvlObjectsConfig.Editor_LinkContextAssets(roomCtx, null);
            roomCtx.SetContextLink(room.gameObject);
            AssetDatabase.SaveAssets();

            AssertAssetSetupOK(room.Config, roomObjectsDataComponent.LvlObjectsConfigRoom);
            return OperationResult.OK;
        }
        
        static void GetOrCreateLevelObjectsConfigForScene(SceneLevelObjectsConfig sceneLevelObjectsConfig,
            RoomObjectsDataComponent roomObjectsDataComponent,
            out LevelObjectsConfig lvlObjectsConfig)
        {
            lvlObjectsConfig = roomObjectsDataComponent.LvlObjectsConfigScene;
            if (lvlObjectsConfig != null)
            {
                EnsureSceneReferencesContext(roomObjectsDataComponent);
                return;
            }
            var roomGo = roomObjectsDataComponent.gameObject;

            // var scene = EditorSceneManager.CreateScene($"{SceneContext.Current.name} {roomGo.name}{k_lvlObjectsPostfix}");
            
            lvlObjectsConfig = ScriptableObject.CreateInstance<LevelObjectsConfig>();
            lvlObjectsConfig.name = $"{SceneContext.Current.name} {roomGo.name}{k_lvlObjectsPostfix}";
            lvlObjectsConfig.AddToAsset(sceneLevelObjectsConfig);
            
            EnsureSceneReferencesContext(roomObjectsDataComponent);

            // add to component for GameObject in scene
            roomObjectsDataComponent.LvlObjectsConfigScene = lvlObjectsConfig;
            // add to scene config
            Add(ref sceneLevelObjectsConfig.Editor_LevelObjectsConfigs, lvlObjectsConfig);

            EditorUtility.SetDirty(sceneLevelObjectsConfig);
        }

        static void EnsureSceneReferencesContext(RoomObjectsDataComponent roomObjectsDataComponent)
        {
            if (roomObjectsDataComponent.MapperScene != null) 
                return;
            SceneContext.SceneRoot.gameObject.TryGet<RoomObjectMapper>(out var sceneRefs);
            if (sceneRefs == null)
                sceneRefs = SceneContext.SceneRoot.gameObject.AddComponent<RoomObjectMapper>();

            roomObjectsDataComponent.MapperScene = sceneRefs;
        }

        static void GetOrCreateLevelObjectsConfigForRoom(RoomConfig roomConfig, 
            RoomObjectsDataComponent roomObjectsDataComponent, 
            out LevelObjectsConfig lvlObjectsConfig)
        {
            var name = $"{roomConfig.name}{k_lvlObjectsPostfix}";
            lvlObjectsConfig = roomObjectsDataComponent.LvlObjectsConfigRoom;

            if (lvlObjectsConfig == null)
            {
                lvlObjectsConfig = roomConfig.RoomObjects;
                roomObjectsDataComponent.LvlObjectsConfigRoom = lvlObjectsConfig;
            }

            var go = roomObjectsDataComponent.gameObject;
            if (lvlObjectsConfig != null)
            {
                EnsureRoomReferenceContext(go, roomConfig, roomObjectsDataComponent, lvlObjectsConfig, name);
                lvlObjectsConfig.name = name;
                return;
            }

            lvlObjectsConfig = ScriptableObject.CreateInstance<LevelObjectsConfig>();
            lvlObjectsConfig.name = name;

            EnsureRoomReferenceContext(go, roomConfig, roomObjectsDataComponent, lvlObjectsConfig, name);

            // part of RoomConfig asset
            lvlObjectsConfig.AddToAsset(roomConfig);
            // add to component for preview-GameObject in scene
            roomObjectsDataComponent.LvlObjectsConfigRoom = lvlObjectsConfig;
            // add to RoomConfig
            roomConfig.RoomObjects = lvlObjectsConfig;

            EditorUtility.SetDirty(roomObjectsDataComponent);
            EditorUtility.SetDirty(roomConfig);
        }

        static void EnsureRoomReferenceContext(RoomObjectsDataComponent r)
        {
            r.TryGet(out RoomInstanceComponent ric);
            var name = $"{ric.Config.name}{k_lvlObjectsPostfix}";
            EnsureRoomReferenceContext(r.gameObject, ric.Config, r, r.LvlObjectsConfigRoom, name);
        }
        
        static void EnsureRoomReferenceContext(GameObject roomGo, RoomConfig roomConfig, 
            RoomObjectsDataComponent roomObjectsDataComponent,
            LevelObjectsConfig lvlObjectsConfig, string name)
        {
            var refCtx = lvlObjectsConfig.GetOrInstantiateMapper(roomGo, ref roomObjectsDataComponent.MapperRoom);
            if (refCtx != null)
            {
                if (roomGo.TryGet(out RoomObjectsDataComponent rodc))
                    rodc.Editor_SpawnObjects(lvlObjectsConfig, refCtx, ObjectPlacingContext.Room, roomGo);
                return;
            }   
            if (lvlObjectsConfig.ResolverPrefab != null)
            {
                Debug.LogError("ResolverPrefab is set but creating ReferencesContext failed!");
                return;
            }
            var lvlObjectsPrefab = new GameObject(name, typeof(RoomObjectMapper));
            lvlObjectsPrefab.transform.SetParent(roomGo.transform);
            
            var dir = Path.GetDirectoryName(roomConfig.Path().Replace("//", "\\\\"))?.Replace("\\\\", "//");
            var objsPrefab = PrefabUtility.SaveAsPrefabAssetAndConnect(lvlObjectsPrefab, 
                $"{dir}/{name}.prefab", 
                InteractionMode.AutomatedAction);
            
            lvlObjectsConfig.SetRoomObjectsPrefab(objsPrefab);
            lvlObjectsPrefab.TryGet(out RoomObjectMapper references);
            roomObjectsDataComponent.MapperRoom = references;
        }

        static ContextAsset GetOrCreateRoomContextAsset(LevelObjectsConfig lvlObjectsConfig,
            IScriptableObject sceneLevelObjectsConfig)
        {
            // for easily linking Room context in VarReferences of objects (conditions etc.):
            var roomCtx = lvlObjectsConfig.ContextAsset;
            if (roomCtx != null) 
                return roomCtx;

            roomCtx = lvlObjectsConfig
                .ChildAssets().FirstOrDefault(s => s is ContextAsset ca
                                                   && string.Equals(ca.name, k_contextAssetName)) as ContextAsset;
            if (roomCtx != null) 
                return roomCtx;

            roomCtx = ScriptableObject.CreateInstance<ContextAsset>();
            roomCtx.name = k_contextAssetName;
            roomCtx.AddToAsset(sceneLevelObjectsConfig);

            return roomCtx;
        }

        static void AssertAssetSetupOK(RoomConfig roomConfig,
            LevelObjectsConfig roomObjects)
        {
            var roomObjsChildAssets = roomConfig.ChildAssets();
            var isChild = roomObjsChildAssets.Any(soc =>
                ReferenceEquals(soc, roomObjects));
            var isSetInRoom = roomConfig.RoomObjects == roomObjects;
            if (!isChild)
                Debug.LogError($"{roomObjects} not a child of {roomConfig}!");
            if (!isSetInRoom)
                Debug.LogError($"{roomObjects} not part of array {roomConfig}!");
        }

        static void AssertAssetSetupOK(SceneLevelObjectsConfig sceneLevelObjectsConfig, LevelObjectsConfig sceneObjects)
        {
            var sceneObjsChildAssets = sceneLevelObjectsConfig.ChildAssets();
            var isChild = sceneObjsChildAssets.Any(soc =>
                ReferenceEquals(soc, sceneObjects));
            var isPartOfArray = sceneLevelObjectsConfig.Editor_LevelObjectsConfigs.Any(loc =>
                ReferenceEquals(loc, sceneObjects));
            if (!isChild)
                Debug.LogError($"{sceneObjects} not a child of {sceneLevelObjectsConfig}!");
            if (!isPartOfArray)
                Debug.LogError($"{sceneObjects} not part of array {sceneLevelObjectsConfig}!");
        }

        #endregion

    }
}