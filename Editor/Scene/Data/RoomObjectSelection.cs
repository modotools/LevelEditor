using Level.Objects;
using Level.Room;
using UnityEngine;

namespace Level.Editor.Room
{
    internal struct RoomObjectSelection
    {
        public int InstanceID;
        public GameObject Object;
        public ObjectContextMarker Marker;
        public Vector3 LastPosition;

        public LevelObjectsConfig LevelObjectsConfig;
        public RoomObjectsDataComponent RoomObjectsDataComponent;
    }
}
