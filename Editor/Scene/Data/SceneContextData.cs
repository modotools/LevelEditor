using Core.Unity.Interface;
using Level.Objects;
using ScriptableUtility;
using UnityEngine.SceneManagement;

namespace Level.Editor.Room
{
    public struct SceneContextData
    {
        public Scene Current;
        public SceneLevelObjectsConfig LevelObjectsConfig;
        public ContextAsset ContextAsset;
        public ISceneRoot SceneRoot;
    }

}