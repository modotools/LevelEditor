using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Core;
using Core.Editor.Utility.GUITools;
using Core.Extensions;
using Core.Types;
using Core.Unity.Utility;
using Core.Unity.Utility.GUITools;
using Cysharp.Threading.Tasks;
using Level.Generation;
using Level.Room;
using ScriptableUtility;
using UnityEditor;
using UnityEngine;
using static Level.Editor.Room.LevelObjects_SceneContext;
using Object = UnityEngine.Object;

namespace Level.Editor.Room
{
    public static class SceneRoomManagement
    {
        public static readonly List<IRoomInstance> RoomInstances = new List<IRoomInstance>();

        public static bool IsRoomScene => !RoomInstances.IsNullOrEmpty()
                                          || OctreeInstance.Rooms != null;

        #region Build Rooms

        public static int RoomsToBuild;
        public static int RoomsBuilt;
        public static float Progress => (float) RoomsBuilt / RoomsToBuild;
        public static bool IsBuildingRooms => CurrentlyBuilding != null;

        public static IRoomInstance CurrentlyBuilding;
        static CancellationTokenSource m_cancellation;

        public static void AbortBuildingRooms() => m_cancellation?.Cancel();

        public static async void StartBuildAllRooms()
        {
            m_cancellation = new CancellationTokenSource();
            try
            {
                await BuildAllRooms(m_cancellation.Token);
            }
            catch (OperationCanceledException e)
            {
                Debug.Log("Task canceled");
            }
        }

        public static void InitRoomsToBuilt()
        {
            RoomsBuilt = RoomInstances.Count(r => r.IsBuilt);
            RoomsToBuild = RoomInstances.Count;
        }

        public static async UniTask BuildAllRooms(CancellationToken ct)
        {
            ResetFrameSplitter();

            RoomsBuilt = 0;
            using (var scoped = SimplePool<List<IRoomInstance>>.I.GetScoped())
            {
                var instances = scoped.Obj;
                instances.AddRange(RoomInstances.Where(r => !r.IsBuilt));
                RoomsToBuild = instances.Count;
                foreach (var r in instances)
                {
                    if (ct.IsCancellationRequested)
                        throw new OperationCanceledException();

                    CurrentlyBuilding = r;
                    await CurrentlyBuilding.AsyncBuild(ct);
                    RoomsBuilt++;
                    CurrentlyBuilding = null;
                }
            }
        }

        static void ResetFrameSplitter()
        {
            var guids = AssetDatabase.FindAssets($"t: {nameof(StopwatchFrameSplitter)}");
            foreach (var g in guids)
            {
                var path = AssetDatabase.GUIDToAssetPath(g);
                var splitter =
                    AssetDatabase.LoadAssetAtPath(path, typeof(StopwatchFrameSplitter)) as StopwatchFrameSplitter;
                if (splitter == null)
                    continue;
                splitter.ResetWatch();
            }
        }

        #endregion

        //static DebugString m_debugString;

        #region Hooks

        public static void DuringSceneGUI()
        {
            using (var scope = new HandleGUIScope())
            {
                if (CurrentlyBuilding == null)
                {
                    if (!IsRoomScene || RoomsToBuild <= RoomsBuilt)
                        return;
                    if (GUILayout.Button("Built Rooms!", GUILayout.Width(100f)))
                        StartBuildAllRooms();
                    return;
                }

                var text = $"Building: {CurrentlyBuilding.gameObject} Progress: {RoomsBuilt} / {RoomsToBuild}";
                var r = CustomHandles.GetTextRect(new Vector2(0, 0), text);
                r.x += 0.5f * r.width;

                using (new GUILayout.AreaScope(r))
                    GUILayout.Label(text);
            }
        }

        public static void OnReloadScripts() => SetContextComponentsDirty();

        public static void OnHierarchyChanged(ref DebugString debugString)
        {
            debugString.AddLog("OnHierarchyChanged\n");

            var recache = RoomInstances.Contains(null);
            if (recache)
                debugString.AddLog("Recache because of null entries");
            foreach (var gameObject in Selection.gameObjects)
            {
                if (!gameObject.TryGetComponent(out IRoomInstance room)
                    || RoomInstances.Contains(room))
                    continue;

                debugString.AddLog("Recache because new room");
                recache = true;
                break;
            }

            if (!recache)
                return;
            CacheAllRooms(ref debugString);
            PutAllRoomsToOctree(ref debugString);
        }

        #endregion

        public static void InitScene_GatherRooms(ref DebugString debugString)
        {
            debugString.AddLog("InitScene_GatherRooms\n");

            CacheAllRooms(ref debugString);
            PutAllRoomsToOctree(ref debugString);

            if (!IsRoomScene)
            {
                debugString.AddLog("Not a Room-Scene\n");
                return;
            }

            UpdateSceneContext();
        }


        static void CacheAllRooms(ref DebugString debugString)
        {
            debugString.AddLog("CacheAllRooms\n");
            RoomInstances.Clear();
            var rooms = Resources.FindObjectsOfTypeAll<RoomInstanceComponent>();
            foreach (var r in rooms)
            {
                debugString.AddLog($"add room {r.gameObject}\n");
                RoomInstances.Add(r);
            }
        }

        static void PutAllRoomsToOctree(ref DebugString debugString)
        {
            debugString.AddLog("PutAllRoomsToOctree\n");

            //Debug.Log($"PutAllRoomsToOctree {OctreeInstance.Instance}");
            if (OctreeInstance.Rooms == null)
            {
                var octree = Object.FindObjectOfType<RoomOctreeComponent>();
                if (octree == null)
                    return;

                octree.InitOctree();
                Debug.Assert(OctreeInstance.Rooms != null);
            }

            OctreeInstance.Rooms.Clear();
            foreach (var r in RoomInstances)
                OctreeInstance.Rooms.AddObject(r);
        }

        /// <summary>
        /// Makes sure ContextComponent reinitialize their variables, f.e. needed for rooms when variables in container change
        /// </summary>
        static void SetContextComponentsDirty()
        {
            var ccs = Object.FindObjectsOfType<ContextComponent>();
            foreach (var cc in ccs) cc.SetDirty();
        }
    }
}