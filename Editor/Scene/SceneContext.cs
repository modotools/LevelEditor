using System.Linq;
using Core.Editor.Utility;
using Core.Extensions;
using Core.Types;
using Core.Unity.Interface;
using Core.Unity.Operations;
using Level.Objects;
using ScriptableUtility;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
 
namespace Level.Editor.Room
{
    public static class LevelObjects_SceneContext
    {
        public static SceneContextData SceneContext;
        
        const string k_contextAssetName = "Scene";
        const string k_unityExt = ".unity";
        const string k_assetExt = ".asset";
        const string k_lvlObjectsPostfix = "_lvlObjects";

        /// <summary>
        /// Makes sure valid SceneLevelObjectsConfig and its ContextAsset exists for current scene,
        /// and everything is linked
        /// </summary>
        public static OperationResult UpdateSceneContext()
        {
            //Debug.Log("Updating Scene Context");
            if (!SceneValid(out var scene))
            {
                Debug.LogError("Scene not valid!");
                return OperationResult.Error;
            }
            var sceneGo = SceneRootOperations.GetSceneRoot(scene, out var sceneRoot);
            if (sceneGo == null)
            {
                Debug.LogError("No Scene-Root!");
                return OperationResult.Error;
            }
            if (SceneContext.Current == scene 
                && SceneContext.LevelObjectsConfig != null)
                return OperationResult.OK;

            // todo only when containing rooms or Octree
            var sceneLevelObjectsConfig = GetOrCreateSceneLevelObjectsConfig(scene);
            var sceneCtx = GetOrCreateSceneContextAsset(sceneLevelObjectsConfig);
            sceneLevelObjectsConfig.Editor_LinkContextAssets(sceneCtx);
            sceneCtx.SetContextLink(sceneGo);

            SetSceneContextData(sceneLevelObjectsConfig, scene, sceneRoot);

            //Debug.Log("Save Assets");
            AssetDatabase.SaveAssets();
            return OperationResult.OK;
        }

        static void SetSceneContextData(SceneLevelObjectsConfig sceneLevelObjectsConfig, Scene scene, ISceneRoot sceneRoot)
        {
            SceneContext.LevelObjectsConfig = sceneLevelObjectsConfig;
            SceneContext.ContextAsset = sceneLevelObjectsConfig.ContextAsset;
            SceneContext.Current = scene;
            SceneContext.SceneRoot = sceneRoot;
        }

        static ContextAsset GetOrCreateSceneContextAsset(SceneLevelObjectsConfig sceneLevelObjectsConfig)
        {
            // for easily linking scene context in VarReferences of objects (conditions etc.):
            var sceneCtx = sceneLevelObjectsConfig.ContextAsset;
            if (sceneCtx != null) 
                return sceneCtx;

            sceneCtx = sceneLevelObjectsConfig
                .ChildAssets().FirstOrDefault(s => s is ContextAsset ca
                                                   && string.Equals(ca.name, k_contextAssetName)) as ContextAsset;

            if (sceneCtx != null)
                return sceneCtx;

            sceneCtx = ScriptableObject.CreateInstance<ContextAsset>();
            sceneCtx.name = k_contextAssetName;
            sceneCtx.AddToAsset(sceneLevelObjectsConfig);

            return sceneCtx;
        }

        static SceneLevelObjectsConfig GetOrCreateSceneLevelObjectsConfig(Scene scene)
        {
            var assetPath = scene.path.Replace(k_unityExt, $"{k_lvlObjectsPostfix}{k_assetExt}");
            var sceneLevelObjectsConfig = AssetDatabase.LoadAssetAtPath<SceneLevelObjectsConfig>(assetPath);
            if (sceneLevelObjectsConfig == null)
            {
                sceneLevelObjectsConfig = ScriptableObject.CreateInstance<SceneLevelObjectsConfig>();
                sceneLevelObjectsConfig.name = $"{SceneContext.Current.name}{k_lvlObjectsPostfix}";

                AssetDatabase.CreateAsset(sceneLevelObjectsConfig, assetPath);
            }

            sceneLevelObjectsConfig.Editor_LevelObjectsConfigs ??= new LevelObjectsConfig[0];
            return sceneLevelObjectsConfig;
        }

        static bool SceneValid(out Scene scene)
        {
            scene = SceneManager.GetActiveScene();
            return !scene.path.IsNullOrEmpty();
        }
    }
}