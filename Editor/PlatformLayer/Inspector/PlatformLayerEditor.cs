﻿using System;
using Core.Editor.Extensions;
using Core.Editor.Inspector;
using Level.Editor.Room;
using Level.Editor.Tiles;
using Level.Data;
using Level.Editor.Tools;
using Level.PlatformLayer;
using Level.PlatformLayer.Interface;
using Level.Room;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEditor.ShortcutManagement;
using UnityEngine;
using static Level.Editor.Tools.SizePositionTools;
using static Level.Editor.Tiles.TileDrawing;
using static Core.Editor.Utility.GUIStyles.CustomStyles;

namespace Level.Editor.PlatformLayer
{
    public class PlatformEditor : BaseEditor<PlatformLayerConfig>
    {
        public override void SetTarget(PlatformLayerConfig t, SerializedObject s)
        {
            base.SetTarget(t, s);

            m_tileDrawingData.Platform = t;
            m_tileDrawingData.Tiles = t;           
            if (Target != null)
            {
                // Debug.Log("Set Grid");
                Target.Get(out m_tileDrawingData.OriginalGridData);
                m_tileDrawingData.GridData = m_tileDrawingData.OriginalGridData;
                m_tileDrawingData.TilesSet = Target.TilesSet;
            }
            InitDebugData();
            UpdatePreview();
        }

        static readonly Vector3Int k_roomSizeDat = new Vector3Int(int.MaxValue-1, int.MaxValue-1, int.MaxValue-1);

        static bool m_debugDrawEnabled = true;

        Color m_drawBoundsColor = Color.green;
        TileDrawingOpData m_tileDrawingData;
        readonly AnimBool m_showSizeTools = new AnimBool();

        Action<SceneView> m_func;

        TileDebugDrawSettings m_debugDrawSettings;

        SizePositionCategory m_sizePositionCategory;

        RoomBuilder m_previewBuilder;
        RoomBuilder_PlatformPreview m_preview;

        bool SceneGUISelfManaged => ParentContainer is PlatformLayerConfigInspector;

        bool PreviewWithBuilder => SceneGUISelfManaged;
        bool PreviewBuildValid => PreviewWithBuilder && Target != null && m_previewBuilder != null;
        RoomBuilder_PlatformPreview Preview
        {
            get => m_preview;
            set
            {
                m_preview?.Terminate();
                m_preview = value;
            }
        }
        //int m_currentlySelected;
        
        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);

            Init_SizePositionTools(m_showSizeTools, this, ref m_guiData);
            InitSizePositionGUIData();

            InitDebugData();

            m_tileDrawingData.Platform = Target;
            m_tileDrawingData.Tiles = Target;
            if (Target != null)
            {
                Target.Get(out m_tileDrawingData.OriginalGridData);
                m_tileDrawingData.GridData = m_tileDrawingData.OriginalGridData;
                m_tileDrawingData.TilesSet = Target.TilesSet;
            }           
            Init_TilesMode(ref m_tileDrawingData, this);

            if (SceneGUISelfManaged)
            {
                m_func = sceneView => OnSceneGUI();
                SceneView.duringSceneGui += m_func;
            }
        }

        void InitDebugData()
        {
            m_drawBoundsColor = CustomEditorPrefs.GetColor($"{nameof(PlatformLayerConfigInspector)}.{nameof(m_drawBoundsColor)}");
            m_debugDrawSettings = CustomEditorPrefs.GetAssetFromGuid<TileDebugDrawSettings>
                ($"{nameof(PlatformLayerConfigInspector)}.{nameof(m_debugDrawSettings)}");
        }

        public override void Terminate()
        {
            base.Terminate();
            // ReSharper disable once DelegateSubtraction
            if (m_func != null)
                SceneView.duringSceneGui -= m_func;
            m_func = null;
            Preview?.Terminate();
        }

        public void OnSceneGUI()
        {
            if (Target == null || Target.TilesSet == null)
                return;

            //Debug.Log($"PlatformLayer OnSceneGUI {Target.name}");
            if (SceneView.lastActiveSceneView != null)
                m_tileDrawingData.Is2DMode = SceneView.lastActiveSceneView.in2DMode;

            SceneGUIDraw();
            SceneGUIEdit();
        }

        void SceneGUIEdit()
        {
            var e = Event.current;

            if (e.alt)
                return;
            if (e.button == 2) // middle mouse button
                return;
            // prevent default SceneGUI stuff part 1, e.Use() for part2
            var controlId = GUIUtility.GetControlID(FocusType.Passive);
            if (e.type == EventType.Layout)
                HandleUtility.AddDefaultControl(controlId);

            if (SceneGUISelfManaged)
                SizePositionSlider_SceneGUI(ref m_guiData);
            //UpdateMouseDown(m_sizePositionCategory, Target, k_roomSizeDat, ref m_clickValid, ref m_clickTime);
            //if (m_sizePositionCategory != SizePositionCategory.None)
            //    DrawSizePositionSlider(m_sizePositionCategory, 
            //        ref m_left, ref m_right, ref m_top, ref m_bottom);
            
            // SizePositionButtons_SceneGUI(m_sizePositionCategory, Target, k_roomSizeDat);

            var groundPlane = new Plane(m_tileDrawingData.Is2DMode ? Vector3.back : Vector3.up, 
                Target.PositionOffset.y * (m_tileDrawingData.Is2DMode ? Vector3.back : Vector3.up));

            var ray = HandleUtility.GUIPointToWorldRay(new Vector2(e.mousePosition.x, e.mousePosition.y));
            // Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x, sceneView.position.height - e.mousePosition.y - 16, 0.0f));
            if (!groundPlane.Raycast(ray, out var distance))
                return;
            var wPos = ray.GetPoint(distance);
            var cursorPos = wPos;
            var tilePos = wPos - new Vector3(Target.PositionOffset.x, 0, Target.PositionOffset.z);
            if (m_tileDrawingData.Is2DMode)
            {
                var y = tilePos.y;
                var z = tilePos.z;
                tilePos = new Vector3(tilePos.x, z, y);
            }
            SceneGUI_TilesMode(Event.current, ref m_tileDrawingData, tilePos, cursorPos);

            //if (e.type!= EventType.Repaint && e.type!= EventType.Layout)
            //{
            //    Debug.Log("Repainting");
            //    SceneView.RepaintAll();
            //}

        }

        void SceneGUIDraw()
        {
            if (!m_debugDrawEnabled)
                return;

            var count = Target.TilesSet.TileDataSets.Count;
            for (var i = 0; i < count; i++)
            {
                var set = Target.TilesSet.TileDataSets[i];
                var tileConf = set.TileConfig.Result;
                if (tileConf == null || m_debugDrawSettings == null)
                    continue;
                var usingSet = (set.Mask & m_tileDrawingData.TileTypesMask) != 0;
                ref var debug = ref m_debugDrawSettings.Get(tileConf);

                if (debug.DrawOnlyWhenUsing && !usingSet)
                    continue;

                debug.Settings.Is2D = m_tileDrawingData.Is2DMode;
                TileDebugDraw.DrawGeometryType(Target, set, debug.Settings);
            }

            if (m_tileDrawingData.Is2DMode)
                return;

            Target.Get(out IGridData gridData);
            TileDebugDraw.DrawPlatformBounds(Target, Target.PositionOffset, gridData, m_drawBoundsColor);
        }

        public override void OnGUI(float w)
        {
            if (Target == null)
                return;
            base.OnGUI(w);

            m_debugDrawEnabled = GUILayout.Toggle(m_debugDrawEnabled, "Debug Draw Enabled");
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_drawBoundsColor = EditorGUILayout.ColorField("Bounds-Color: ", m_drawBoundsColor);
                m_debugDrawSettings = (TileDebugDrawSettings) EditorGUILayout.ObjectField("TileDebugDrawSettings: ", 
                    m_debugDrawSettings, typeof(TileDebugDrawSettings), false);

                if (check.changed)
                {
                    CustomEditorPrefs.SetColor($"{nameof(PlatformLayerConfigInspector)}.{nameof(m_drawBoundsColor)}",
                        m_drawBoundsColor);
                    CustomEditorPrefs.SetGuid($"{nameof(PlatformLayerConfigInspector)}.{nameof(m_debugDrawSettings)}", m_debugDrawSettings);
                }
            }

            if (ParentContainer is UnityEditor.Editor ed)
                ed.DrawDefaultInspector();

            GUILayout.Label($"Position: {Target.Position} Size: {Target.Size}", EditorStyles.helpBox);

            if (Target.TilesSet == null)
                return;

            if (SceneGUISelfManaged)
            {
                var prevCategory = m_sizePositionCategory;
                SizeToolsCategoryGUI(ref m_sizePositionCategory);
                if (prevCategory != m_sizePositionCategory)
                    InitSizePositionGUIData();
            }

            //SizeToolsGUI(m_showSizeTools, Target, Target, k_roomSizeDat);

            var count = Target.TilesSet.TileDataSets.Count; //Mathf.Min(m_debugData.Length, Target.TilesSet.TileDataSets.Count);
            for (var i = 0; i < count; i++)
            {
                var set = Target.TilesSet.TileDataSets[i];
                var tileConf = set.TileConfig.Result;
                if (tileConf == null || m_debugDrawSettings == null)
                    continue;
                ref var debug = ref m_debugDrawSettings.Get(tileConf);

                bool active;
                using (new EditorGUILayout.HorizontalScope(GUILayout.Width(Screen.width)))
                {
                    //GUILayout.Label($"{set.TileConfig.name}: ",GUILayout.Width(150f));
                    active = TileTypesActiveGUI(ref m_tileDrawingData, set);
                    GUILayout.Space(15f);
                    debug.Foldout = EditorGUILayout.Foldout(debug.Foldout, "debug settings", true, MiniFoldoutStyle);
                    if (debug.Foldout)
                    {
                        using (var check = new EditorGUI.ChangeCheckScope())
                        using (new EditorGUILayout.VerticalScope())
                        {
                            debug.DrawOnlyWhenUsing =
                                EditorGUILayout.Toggle("Draw only when using", debug.DrawOnlyWhenUsing);
                            DebugSettings(ref debug.Settings);
                            if (check.changed) 
                                EditorUtility.SetDirty(m_debugDrawSettings);
                        }
                    }
                }
                if (active)
                    TileSelectionGUI(ref m_tileDrawingData, set);
            }

            BrushTypeSizeGUI(ref m_tileDrawingData);

            if (!PreviewWithBuilder)
                return;

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_previewBuilder = (RoomBuilder) EditorGUILayout.ObjectField("Preview w. Builder", m_previewBuilder, 
                    typeof(RoomBuilder), false);
                if (check.changed) 
                    UpdatePreview();
            }
        }

        static void DebugSettings(ref TileDebugDraw.DebugDrawSettings settings)
        {
            settings.DrawLabel = EditorGUILayout.Toggle("Draw Label", settings.DrawLabel);
            settings.DrawDots = EditorGUILayout.Toggle("Draw Dots", settings.DrawDots);
            settings.DrawDebugTiles =
                EditorGUILayout.Toggle("Draw Debug-Tiles", settings.DrawDebugTiles);
            //if (m_debugData[i].DrawDebugTiles)
            using (new EditorGUI.DisabledScope(!settings.DrawDebugTiles))
            {
                settings.DebugTileSize =
                    EditorGUILayout.FloatField("Debug TileSize: ", settings.DebugTileSize);
            }
            settings.ColorAlpha = EditorGUILayout.FloatField("Color Alpha: ", settings.ColorAlpha);
            settings.LineAlpha = EditorGUILayout.FloatField("Line Alpha: ", settings.LineAlpha);
        }

        void UpdatePreview()
        {
            var builder = m_preview?.Builder;
            var conf = m_preview?.PlatformLayerConfig;

            // nothing changed
            if (builder == m_previewBuilder && Target == conf)
                return;

            Preview = PreviewBuildValid 
                ? new RoomBuilder_PlatformPreview(m_previewBuilder, Target, true) 
                : null;

            InitSizePositionGUIData();
        }

        [Shortcut("RoomEditor.ToggleDebugDraw")]
        public static void ToggleDebugDraw() 
            => m_debugDrawEnabled = !m_debugDrawEnabled;

        SizePositionGUIData m_guiData;
        void InitSizePositionGUIData()
        {
            if (Target == null)
                return;

            m_guiData.Category = m_sizePositionCategory;
            m_guiData.Layer = Target;
            m_guiData.SizeData = k_roomSizeDat;
        }
    }
}