﻿using Core.Editor.Inspector;
using Level.PlatformLayer;
using UnityEditor;

namespace Level.Editor.PlatformLayer
{
    [CustomEditor(typeof(PlatformLayerConfig))]
    public class PlatformLayerConfigInspector : BaseInspector<PlatformEditor> { }
}