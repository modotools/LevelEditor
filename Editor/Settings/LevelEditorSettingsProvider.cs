﻿using System.IO;
using UnityEditor;
using UnityEngine.UIElements;

namespace Level.Editor.Settings
{
    internal class LevelEditorSettingsProvider : SettingsProvider
    {
        UnityEditor.Editor m_coreSettingsEditor;

        public LevelEditorSettingsProvider(string path, SettingsScope scope = SettingsScope.Project)
            : base(path, scope) {}

        public static bool IsSettingsAvailable() => File.Exists(LevelEditorSettings.K_ConfigLinksPath);

        void InitEditor()
        {
            LevelEditorSettingsUpdater.GetOrCreateLevelEditorSettings(out var settings);
            if (settings != null)
                m_coreSettingsEditor = UnityEditor.Editor.CreateEditor(settings);
        }

        public override void OnActivate(string searchContext, VisualElement rootElement)
        {
            // This function is called when the user clicks on the MyCustom element in the Settings window.
            // m_configLinks = GetConfigLinkSerialized();
            // Debug.Log("Activated");
            InitEditor();
        }

        public override void OnGUI(string searchContext)
        {
            if (m_coreSettingsEditor == null)
                InitEditor();
            else m_coreSettingsEditor.OnInspectorGUI();
        }

        // Register the SettingsProvider
        [SettingsProvider]
        public static SettingsProvider CreateLevelEditorSettingsProvider()
        {
            if (!IsSettingsAvailable())
                return null;
            var provider = new LevelEditorSettingsProvider("Project/Level Editor Settings");

            // Automatically extract all keywords from the Styles.
            // provider.keywords = GetSearchKeywordsFromGUIContentProperties<Styles>();
            return provider;

            // Settings Asset doesn't exist yet; no need to display anything in the Settings window.
        }
    }
}