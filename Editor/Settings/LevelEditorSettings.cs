﻿using System;
using System.IO;
using Core.Extensions;
using Core.Types;
using Core.Unity.Utility;
using UnityEditor;
using UnityEngine;

namespace Level.Editor.Settings
{
    internal class LevelEditorSettings : ScriptableObject
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField] internal DefaultAsset m_levelMaterials;
        [SerializeField] internal DefaultAsset m_levelSourceTextures;
        [SerializeField] internal DefaultAsset m_levelTextureAtlas;
        [SerializeField] internal DefaultAsset m_roomAssetsDefaultPath;

        [SerializeField] internal Material m_darkPlaneMaterial;
        [SerializeField] internal HierarchyStyles m_roomHierarchyStyleInitiallyLoaded;
        [SerializeField] internal HierarchyStyles m_roomHierarchyStyleNotLoaded;
        [SerializeField] internal HierarchyStyles m_workerAreaStyleInitiallyLoaded;
        [SerializeField] internal HierarchyStyles m_workerAreaStyleNotLoaded;
#pragma warning restore 0649 // wrong warning

        const string k_configsPath = "Assets/Settings/";
        internal const string K_ConfigLinksPath = k_configsPath + "LevelEditorSettings.asset";

        internal static readonly string K_FullConfigLinksPath = Path.GetFullPath(".") + Path.DirectorySeparatorChar + K_ConfigLinksPath;
    }

    public static class LevelEditorSettingsAccess
    {
        static LevelEditorSettings I => AssetDatabase.LoadAssetAtPath<LevelEditorSettings>(LevelEditorSettings.K_ConfigLinksPath);
        const string k_defaultSourceTexturesDirectory = "Assets/Art/LevelTextures/";
        const string k_defaultTextureAtlasDirectory = "Assets/Art/LevelTextureAtlas/";
        const string k_defaultLevelMaterialsDirectory = "Assets/Art/LevelMaterials/";
        const string k_defaultRoomsDirectory = "Assets/Configs/Rooms/";

        public static string LevelMaterialsPath => I.m_levelMaterials == null ? k_defaultLevelMaterialsDirectory 
            : $"{AssetDatabase.GetAssetPath(I.m_levelMaterials)}/";
        public static string LevelSourceTexturesPath => I.m_levelSourceTextures == null ? k_defaultSourceTexturesDirectory
            : $"{AssetDatabase.GetAssetPath(I.m_levelSourceTextures)}/";
        public static string LevelTextureAtlasPath => I.m_levelTextureAtlas == null ? k_defaultTextureAtlasDirectory
            : $"{AssetDatabase.GetAssetPath(I.m_levelTextureAtlas)}/";
        public static string RoomDefaultPath => I.m_roomAssetsDefaultPath == null ? k_defaultRoomsDirectory
            : $"{AssetDatabase.GetAssetPath(I.m_roomAssetsDefaultPath)}/";

        public static Material DarkPlaneMaterial => I.m_darkPlaneMaterial;

        public static HierarchyStyles RoomHierarchyStyleInitiallyLoaded => I.m_roomHierarchyStyleInitiallyLoaded;
        public static HierarchyStyles RoomHierarchyStyleNotLoaded => I.m_roomHierarchyStyleNotLoaded;
        public static HierarchyStyles WorkerAreaStyleInitiallyLoaded => I.m_workerAreaStyleInitiallyLoaded;
        public static HierarchyStyles WorkerAreaStyleNotLoaded => I.m_workerAreaStyleNotLoaded;
    }

    internal static class LevelEditorSettingsUpdater
    {
        const string k_LevelEditorSettingsInit = "LevelEditorSettingsInitialized";
        const string k_packagePath = "Packages/com.modotools.leveleditor/";

        [InitializeOnLoadMethod]
        static void InitLevelEditorUtility()
        {
            if (SessionState.GetBool(k_LevelEditorSettingsInit, false))
                return;
            if (GetOrCreateLevelEditorSettings(out _) != OperationResult.OK) 
                return;
            SessionState.SetBool(k_LevelEditorSettingsInit, true);
        }

        static void EnsureCorrectScriptGuidInAsset()
        {
            var contents = File.ReadAllText(LevelEditorSettings.K_FullConfigLinksPath);
            
            var scriptIdx = contents.IndexOf("m_Script: ", StringComparison.Ordinal);
            var guidIdx = scriptIdx + contents.Substring(scriptIdx).IndexOf("guid: ", StringComparison.Ordinal) + 6;
            var endGuidIdx = guidIdx + contents.Substring(guidIdx).IndexOf(",", StringComparison.Ordinal);
            var oldGuid = contents.Substring(guidIdx, endGuidIdx-guidIdx);
            var newGuid = AssetDatabase.AssetPathToGUID(k_packagePath + "Editor/Settings/LevelEditorSettings.cs");
            if (string.Equals(oldGuid, newGuid, StringComparison.Ordinal))
                return;
            Debug.LogWarning($"LevelEditorSettings with wrong guid: {oldGuid} Should be {newGuid}!");
            contents = contents.Replace(oldGuid, newGuid);
            File.WriteAllText(LevelEditorSettings.K_FullConfigLinksPath, contents);
        }

        internal static OperationResult GetOrCreateLevelEditorSettings(out LevelEditorSettings settings)
        {
            if (File.Exists(LevelEditorSettings.K_FullConfigLinksPath))
            {
                EnsureCorrectScriptGuidInAsset();
                AssetDatabase.Refresh();
                Debug.Log($"LevelEditorSettings exist!");

                //repeat
                settings = AssetDatabase.LoadAssetAtPath<LevelEditorSettings>(LevelEditorSettings.K_ConfigLinksPath);
                if (settings != null)
                {
                    Debug.Log($"LevelEditorSettings found!");
                    return OperationResult.OK;
                }

                EditorApplication.delayCall += InitLevelEditorUtility;
                return OperationResult.Error;
            }

            Debug.LogWarning("No LevelEditor-Settings found... creating");
            settings = ScriptableObject.CreateInstance<LevelEditorSettings>();

            var dir = Path.GetDirectoryName(LevelEditorSettings.K_ConfigLinksPath);
            // ReSharper disable once AssignNullToNotNullAttribute
            if (!dir.IsNullOrEmpty() && !Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            AssetDatabase.CreateAsset(settings, LevelEditorSettings.K_ConfigLinksPath);
            AssetDatabase.SaveAssets();
            return OperationResult.OK;
        }
    }
}