﻿using Core.Editor.Utility.GUIStyles;
using Core.Unity.Utility.GUITools;
using UnityEditor;
using UnityEngine;

namespace Level.Editor
{
    public static class ConfigBoxGUI
    {
        public static void DrawConfigurationBox(int i)
        {
            using (new EditorGUILayout.VerticalScope(GUILayout.Width(55f)))
            {
                using (new EditorGUILayout.HorizontalScope(GUILayout.Width(50f)))
                {
                    using (new BackgroundColorScope((i & 8) == 8 ? Color.white : Color.black))
                        GUILayout.Box(GUIContent.none, CustomStyles.Gray, GUILayout.Width(20f));
                    using (new BackgroundColorScope((i & 4) == 4 ? Color.white : Color.black))
                        GUILayout.Box(GUIContent.none, CustomStyles.Gray, GUILayout.Width(20f));
                }

                using (new EditorGUILayout.HorizontalScope(GUILayout.Width(50f)))
                {
                    using (new BackgroundColorScope((i & 1) == 1 ? Color.white : Color.black))
                        GUILayout.Box(GUIContent.none, CustomStyles.Gray, GUILayout.Width(20f));
                    using (new BackgroundColorScope((i & 2) == 2 ? Color.white : Color.black))
                        GUILayout.Box(GUIContent.none, CustomStyles.Gray, GUILayout.Width(20f));
                }
            }
        }
    }
}
