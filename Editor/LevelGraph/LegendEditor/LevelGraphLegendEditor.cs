using System.Linq;

using System.Collections.Generic;
using Core.Editor.Inspector;
using Core.Editor.Utility;
using Core.Unity.Utility.GUITools;
using UnityEditor;
using UnityEngine;
using Level.Procedural;
using UnityEngine.Experimental.Rendering;
using static _Editor.Level.Procedural.LevelGraphStyleOperations;

namespace _Editor.Level.Procedural
{
    /// <inheritdoc />
    /// <summary> LevelGraphEditor </summary>
    [CustomEditor(typeof(LevelGraphLegendConfig))]
    public class LevelGraphLegendInspector : BaseInspector<LevelGraphLegendEditor>
    {
    }

    public class LevelGraphLegendEditor : BaseEditor<LevelGraphLegendConfig>
    {
        class TestNode
        {
            public NodeTypeData TypeData;
            public KeyStyleData KeyStyleData;
            public GUIStyle NodeStyle;
        }

        TestNode m_selectedNodeType;
        string m_prevType;
        string m_prevKey;
        int m_prevColor;

        const float k_nodePreviewHeight = 250.0f;

        bool m_previewSelected;

        const float k_conPSize = 12f;
        static readonly Vector2 k_conPSizeVec = new Vector2(k_conPSize, k_conPSize);

        string[] m_keyDataNames;
        string[] m_nodeTypeNames;

        Color m_color;
        
        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);
            CacheNames();
        }

        protected override void OnChanged()
        {
            base.OnChanged();
            CacheNames();
        }

        void CacheNames()
        {
            m_keyDataNames = new string[Target.KeyData.Length];
            for (var i = 0; i < Target.KeyData.Length; i++)
            {
                if (Target.KeyData[i].Key == null)
                    continue;
                
                m_keyDataNames[i] = Target.KeyData[i].Key.name;
            }   
            m_nodeTypeNames = new string[Target.NodeTypeData.Length];
            for (var i = 0; i < Target.NodeTypeData.Length; i++) 
                m_nodeTypeNames[i] = Target.NodeTypeData[i].Type != null
                    ? Target.NodeTypeData[i].Type.name
                    : "";
        }

        /// <summary> OnInspectorGUI </summary>
        public override void OnGUI(float width)
        {
            base.OnGUI(width);

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                if (m_nodeTypeNames == null || m_keyDataNames == null)
                    return;
                var typeIdx = System.Array.IndexOf(m_nodeTypeNames, m_prevType);
                typeIdx = EditorGUILayout.Popup(typeIdx, m_nodeTypeNames);

                var keyIdx = System.Array.IndexOf(m_keyDataNames, m_prevKey);
                keyIdx = EditorGUILayout.Popup(keyIdx, m_keyDataNames);

                var prevColor = keyIdx == -1 ? m_prevColor : Target.KeyData[keyIdx].ColorIdx;

                using (new EditorGUILayout.HorizontalScope())
                {
                    var colorIdx = prevColor;
                    for (var i = 0; i < Target.Colors.Length; i++)
                    {
                        var color = Target.Colors[i];
                        using (new ColorScope(color))
                        {
                            var useColor = CustomGUI.ActivityButton(i == prevColor ,$"{i}", true);
                            if (useColor)
                                colorIdx = i;
                        }
                    }

                    m_prevColor = colorIdx;
                    using (new EditorGUI.DisabledScope(true))
                        m_color = EditorGUILayout.ColorField(Target.Colors[colorIdx]);
                }

                if (check.changed)
                    m_selectedNodeType = PreviewGUIChanged(m_nodeTypeNames, m_keyDataNames, typeIdx, keyIdx);
            }

            DrawSelectedNode();
        }

        TestNode PreviewGUIChanged(IReadOnlyList<string> types, IReadOnlyList<string> keys, 
            int typeIdx, int keyIdx)
        {
            if (typeIdx != -1)
                m_prevType = types[typeIdx];
            else return null;

            if (keyIdx != -1)
                m_prevKey = keys[keyIdx];
            else return null;

            ref var typeData = ref Target.NodeTypeData[typeIdx];
            ref var keyData = ref Target.KeyData[keyIdx];
            keyData.ColorIdx = m_prevColor;
            
            var bgData = typeData.NodeTextures;
            if (bgData == null)
                return null;

            var testNode = new TestNode
            {
                KeyStyleData = keyData,
                TypeData = typeData,
                NodeStyle = new GUIStyle()
            };

            GetColoredBGData(bgData, m_color, out var defaultTex, out var activeTex, out var clickedTex);

            if (bgData.BackgroundStyleClicked != null)
                StyleHelpers.InitStyleOnOffPress(testNode.NodeStyle, defaultTex, activeTex, clickedTex);
            else
                StyleHelpers.InitStyleOnOff(testNode.NodeStyle, defaultTex, activeTex);

            return testNode;
        }
        
        void DrawSelectedNode()
        {
            if (m_selectedNodeType == null)
                return;

            var rect = EditorGUILayout.GetControlRect(GUILayout.Height(k_nodePreviewHeight));
            using var gs = new GUI.GroupScope(rect);

            NodeGUI.DrawGrid(Vector2.zero, 20, 0.2f, Color.gray);
            NodeGUI.DrawGrid(Vector2.zero, 100, 0.4f, Color.gray);

            var inspectorWidth = Screen.width * 0.8f;
            var x = (inspectorWidth - m_selectedNodeType.TypeData.NodeSize.x) * 0.5f;
            var pos = new Vector2(x, k_nodePreviewHeight * 0.5f);
            var nodeRect = NodeGUI.GetNodePos(pos, m_selectedNodeType.TypeData.NodeSize, Vector2.zero);

            var lPos = NodeGUI.GetConnectionPointLPos(nodeRect, k_conPSizeVec);
            lPos.x -= m_selectedNodeType.TypeData.ConnectorOffsets.x;
            NodeGUI.DrawConnectionPoint(lPos);

            var tPos = NodeGUI.GetConnectionPointTPos(nodeRect, k_conPSizeVec);
            tPos.x += m_selectedNodeType.TypeData.ConnectorOffsets.y;
            NodeGUI.DrawConnectionPoint(tPos);

            var rPos = NodeGUI.GetConnectionPointRPos(nodeRect, k_conPSizeVec);
            rPos.x += m_selectedNodeType.TypeData.ConnectorOffsets.z;
            NodeGUI.DrawConnectionPoint(rPos);

            var dPos = NodeGUI.GetConnectionPointDPos(nodeRect, k_conPSizeVec);
            dPos.y += m_selectedNodeType.TypeData.ConnectorOffsets.w;
            NodeGUI.DrawConnectionPoint(dPos);
            
            m_previewSelected = NodeGUI.DrawNode(nodeRect, m_previewSelected, m_selectedNodeType.KeyStyleData.Key.name, m_selectedNodeType.KeyStyleData.Icon, m_selectedNodeType.NodeStyle,
                m_selectedNodeType.TypeData.IconSize, m_selectedNodeType.TypeData.TextOffset);
        }
    }
}