using Core.Types;
using Level.Procedural;

namespace _Editor.Level.Procedural
{
    internal struct DrawLevelGraphNodeData
    {
        public LevelGraphNode Node;
        // previousNode is needed for vertical position and context-click commands 
        public LevelGraphNode PreviousNode;
        // when player is not able to continue past a lock FlowResult is Stop and color will turn red
        public FlowResult Flow;
    }
}