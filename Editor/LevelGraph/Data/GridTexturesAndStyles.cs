using System.Collections.Generic;
using UnityEngine;

namespace _Editor.Level.Procedural
{
    public struct GridTexturesAndStyles
    {
        public Dictionary<LevelNodeGridLayoutData, GUIStyle> LayoutStyleMap;
        public Dictionary<NodeIDData, GUIStyle> NodeStyleMap;

        public Texture2D RoomTex;
        public Texture2D ConnectorTex;
        
        public Texture2D[] PathWay;
        public Texture2D[] RoomExtensions;
        
        public Texture2D[] Entry;
        public Texture2D[] Exit;
        public GUIStyle[] EntryStyle;
        public GUIStyle[] ExitStyle;
        public bool TextureNotFound;
    }
}