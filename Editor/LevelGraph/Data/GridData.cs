using UnityEngine;

namespace _Editor.Level.Procedural
{
    public struct GUIGridData
    {
        public Rect GridRect;
        public Vector2 Offset;
        public float GridHeight;

        public float SplitterWidth;
        public Rect SplitterRect;
        public bool SplitterDragging;

        public static GUIGridData Default => new GUIGridData()
        {
            SplitterWidth = 5f, 
            GridHeight = 200f
        };
    }
}