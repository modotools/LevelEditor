using System.Collections.Generic;
using Core.Editor.Utility;
using UnityEditor;
using UnityEngine;
using Core.Extensions;
using Level.Procedural;

namespace _Editor.Level.Procedural
{
    public partial class LevelGraphEditor
    {
        List<List<int>> m_selectablePermutations = new List<List<int>>();
        readonly List<LevelGraphNode> m_permutations = new List<LevelGraphNode>();
        List<int> m_permutationOrder = new List<int>();
        int m_selectedPermutation;
        int m_selectedRun;

        void RunGUI()
        {
            if (Target.RunData.IsNullOrEmpty())
                return;
            
            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("Run: ");

                for (var i = 0; i < Target.RunData.Length; i++)
                {
                    if (!CustomGUI.ActivityButton(m_selectedRun == i, $"{i}", true)) 
                        continue;
                    m_selectedRun = i;
                }
            }
        }
        void PermutationGUI()
        {
            if (m_permutations.IsNullOrEmpty())
                return;
            
            var removedOrAdded = m_permutationOrder.RemoveAll(n => n > m_permutations.Count || n < 0);
            for (var i = 0; i < m_permutations.Count; i++)
            {
                if (m_permutationOrder.Contains(i))
                    continue;
                m_permutationOrder.Add(i);
                removedOrAdded++;
            }

            if (removedOrAdded > 0)
                GenerateSelectablePermutations();

            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("Permutations: ");

                for (var i = 0; i < m_selectablePermutations.Count; i++)
                {
                    if (!CustomGUI.ActivityButton(m_selectedPermutation == i, $"{i}", true)) continue;
                    m_selectedPermutation = i;
                    m_permutationOrder = m_selectablePermutations[i];
                }
            }
        }

        void GenerateSelectablePermutations()
        {
            m_permutationOrder.Sort();
            PermutationCounting.GeneratePermutations(m_permutationOrder, ref m_selectablePermutations);
        }
    }
}