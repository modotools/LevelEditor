using Core.Editor.Interface;
using Core.Editor.Utility;
using Core.Editor.Utility.GUITools;
using Core.Extensions;
using UnityEditor;
using UnityEngine;
using Core.Types;
using Core.Unity.Extensions;
using Core.Unity.Utility.Debug;
using Level.Procedural;
using static _Editor.Level.Procedural.LevelGraphStyleOperations;
using static Core.Editor.Utility.GUITools.CommonGUIOperations;


namespace _Editor.Level.Procedural
{
    public partial class LevelGraphEditor : ISceneGUICallable
    {
        public void OnSceneGUI(SceneView sceneView)
        {
            if (AvoidSceneGUIConflicts(out var e) == FlowResult.Stop) 
                return;
            SceneGUI();

            if (e.type != EventType.Repaint && e.type != EventType.Layout)
                SceneView.RepaintAll();
        }

        LevelGraphNode m_lastSelectedLevelGraphNode;
        void SceneGUI()
        {
            if (m_selectedLevelGraphNode == null)
                return;
            
            if (m_lastSelectedLevelGraphNode != m_selectedLevelGraphNode)
            {
                m_lastSelectedLevelGraphNode = m_selectedLevelGraphNode;
                CalculateSizes(m_selectedLevelGraphNode);
            }            
            // todo GRAPH: cleanup trash debug code (make better)
            // var branchSize = m_selectedNode.Branch?.MinSize ?? Vector3Int.zero;
            // var nextNodeSize = m_selectedNode.Next?.MinSize ?? Vector3Int.zero;
            var roomChunkSize = Target.GridSettings.RoomChunkSize;
            var size = m_selectedLevelGraphNode.MinSize.Vector3() * roomChunkSize;
            var b = new Bounds(-size * 0.5f, size);
            var oneRoomXZ = new Vector3(roomChunkSize, 0, roomChunkSize);
            var oneRoomZ = new Vector3(0, 0, roomChunkSize);
            
            // var combined = CalcMinNodeSpaceNeededCombineBranch(branchSize, nextNodeSize, b.min + oneRoomXZ + 2 * oneRoomZ);
            //var addCombine = 
            // CalcMinNodeSpaceNeededCombineNext(new Vector3Int(1, 1, 1), combined, b.min);
            
            //var nodeSize = Vector3.one * roomChunkSize;
            CustomHandleDraw.DrawBounds(b, Color.yellow);
            DrawExampleLayout(m_selectedLevelGraphNode, Vector2Int.zero);

            //CustomHandleDraw.DrawBounds(new Bounds(b.min+ oneRoomXZ + nodeSize * 0.5f, nodeSize), Color.red);
            //CustomHandleDraw.DrawBounds(b, Color.yellow);
        }
        
        void DrawExampleLayout(LevelGraphNode levelGraphNode, Vector2Int pos)
        {
            var nodeId = new NodeIDData {NodeTypeID = levelGraphNode.Type, LevelKeyID = levelGraphNode.KeyLockID};
            GetKeyAndNodeData(LegendConfig, nodeId, out _, out var keyStyle);
            var icon = keyStyle.Icon;

            if (icon == null && levelGraphNode.FactoryProvider != null)
                icon = levelGraphNode.FactoryProvider.Editor_FactoryIcon;
            if (levelGraphNode.IconOverride != null)
                icon = levelGraphNode.IconOverride;
            if (icon != null)
            {
                var screenPosition = CustomHandles.CalculateScreenPosition(new Vector3(pos.x, pos.y), out _);
                // Debug.Log($"Drawing {icon} at {screenPosition}");
                Handles.BeginGUI();
                GUI.DrawTexture(new Rect(screenPosition.x, Screen.height-screenPosition.y, 50, 50), icon, ScaleMode.ScaleToFit, true);
                Handles.EndGUI();
            }
            // var roomChunkSize = Target.GridSettings.RoomChunkSize;
            // for (var i = 0; i < node.ConnectedTo.Length; i++)
            // {
            //     
            //     DrawExampleLayout();
            // }
            //
            // if (Target.EntryNode.KeyLockID != null)
            //     KeyData.
        }

        void CalculateSizes(LevelGraphNode levelGraphNode)
        {
            foreach (var n in levelGraphNode.ConnectedTo) 
                CalculateSizes(n);

            var nodeSize = new Vector3Int(1, 1, 1);
            levelGraphNode.MinSize = nodeSize;
            if (levelGraphNode.ConnectedTo.IsNullOrEmpty()) 
                return;
            
            foreach (var connected in levelGraphNode.ConnectedTo)
            {
                var nextNodeSize = connected.MinSize;
                nodeSize = CalcMinNodeSpaceNeededCombineBranch(nodeSize, nextNodeSize);
                levelGraphNode.MinSize = new Vector3Int(nodeSize.x, nodeSize.y, nodeSize.z);
            }
        }

        // ReSharper disable CommentTypo
        /// <summary> Calculates minimum needed space for combining two node paths 
        /// We only need this kind of packing as fallback, when we have space issues, otherwise we can generate away!
        /// ---------------------------------------------------------------------- Restricted --------------------------------------------------------------------------------
        /// Example (worst case) Branching-Combination of two node areas A & B, With Restriction: Exits are upper-right side
        /// ╔>.(Exit A)...               . free space, ┼ ─ ┐ potential pathways, ╬ ═ ╝ example pathway
        /// ║AAAA┐┌BBBBBBB               we combine by imagining entrance somewhere lower left of node-area. We pick longest local z-side 
        /// ║AAAA┤├BBBBBBB               of the two Nodes to be B and put the other node-area A in front of it (flipped on side or not)
        /// ╚AAAA╗╔BBBBBBB═>(Exit B)     the new entrance could be somewhere left or in the bottom area.
        /// ╤╪╪╪╪╬╝┼┼┼┼┼┼┤               (example: B (x 3, z 7) has longest z-axis, A (4, 3), result (14, 4) exit spaces in upper right areas are ignored! A is flipped because
        ///                              we don't waste free-space-area in this Bounds other then for the entrance/ pathways otherwise it would be one space/dimension waste)                                            
        ///       (Exit A)
        /// .....╔═>......
        /// ┬AAAA║┌BBBBBBB               In the case of A (3,4) A is not flipped. In any case we add one to length of the calculated Bounds. If exit of A and entry of B
        /// ╔AAAA║╔BBBBBBB=>(Exit B)     is in the same space, I don't want to deal with problems of managing two different pathways from random directions in the same
        /// ╫AAAA╝╟BBBBBBB               roomspace. So lets just add the minimum needed space.
        /// ╬╪╪╪╪╪╝┼┼┼┼┼┼┤
        ///
        /// Example FollowUp-Combination of two node areas A & B
        /// ...╔═╗BBBBBBB               result (13, 5)
        /// ┬AAAA╫BBBBBBB=>(Exit B)     If we combine by letting A continue into B we are not allowed to flip any node, because we want to have a short connection from a to b area. 
        /// ╔AAAA╚BBBBBBB               And we have to add extra space above A in worst case connection scenario, but don't need extra space in between the areas.
        /// ╫AAAA┼└┴┴┴┴┴┘               (without the upper-right restriction we don't have to add one extra space in local-X below A)
        /// ╬┼┼┼┤.
        /// ---------------------------------------------------------------------- NO Restrictions --------------------------------------------------------------------------------
        /// INSTEAD OF CONSIDERING THIS UN-RESTRICTED MODE WE SHOULD ALWAYS GET NODE-SIZE WITH RESTRICTIONS
        /// so in case of a node like this (exit->) ╗ (1,1) we wrap it to a ╚╗ (2, 1)-sized node and a node like this (entry->) ╗ (1,1)  we wrap to a ╗ (1, 2)
        ///                                      (entry)                                                                     (exit)                   ╚
        /// So we don't have to deal with this problem:
        /// Example (worst case) Branching-Combination of two node areas A & B without restrictions:
        /// If we don't restrict exits to upper right side we need extra space below A for worst case and between A and B
        /// result (16, 5) After we combine the nodes like this, we have exits in upper right side and can follow the restriction.
        ///                              If we don't flip and don't restrict we have the same extra spaces, same result, just the distribution is a bit different. 
        /// ╔══>.(Exit A)...             ......╔═>(Exit A)               Example FollowUp-Combination of two node areas A & B
        /// ║AAAA─┐┌─BBBBBBB             ┌─AAAA║┌─BBBBBBB                result (15, 5)                result (14, 6)   max-result = (15, 6)
        /// ║AAAA═╗╔═BBBBBBB             ╔═AAAA║╔═BBBBBBB=>(Exit B)      ┌──────┐.......              ╔════╗........ 
        /// ║AAAA╗║║╔BBBBBBB             ╫╔AAAA║╟╔BBBBBBB                ├─AAAA.┼BBBBBBB              ╟AAAA╫┌BBBBBBB  
        /// ╚╪╪╪╪╝║║╚╪╪╪╪╪╪╪> (Exit B)   ║╚╪╪╪╪╝║╚╪╪╪╪╪╪╪                ╔═AAAA╔╪BBBBBBB=>(Exit B)    ╟AAAA╚╪BBBBBBB=>(Exit B)
        /// ╤╪╪╪╪╤╬╝┼┼┼┼┼┼┼┤             ╬╪╪╪╪╪╪╝┼┼┼┼┼┼┼┤                ╫╔AAAA╫┼BBBBBBB              ╟AAAA.├BBBBBBB 
        ///                                                              ╫╚╪╪╪╪╝└┴┴┴┴┴┴┴              ╚╪╪╝║.└┴┴┴┴┴┴┴
        ///                                                                                           ╤╪╪╪╝.........
        /// </summary>
        // ReSharper restore CommentTypo
        Vector3Int CalcMinNodeSpaceNeededCombineBranch(Vector3Int aSize, Vector3Int bSize, Vector3? debugPos = null)
        {
            void Flip()
            {
                var temp = bSize;
                bSize = aSize;
                aSize = temp;
            }

            if (aSize.Equals(Vector3Int.zero))
                return bSize;
            if (bSize.Equals(Vector3Int.zero))
                return aSize;
            if (aSize.z > bSize.z)
                Flip();

            return CalcMinNodeSpaceNeededCombine(aSize, bSize, 1, 3, true, debugPos);
        }
        Vector3Int CalcMinNodeSpaceNeededCombineNext(Vector3Int aSize, Vector3Int bSize, Vector3? debugPos = null)
        {
            if (aSize.Equals(Vector3Int.zero))
                return bSize;
            return bSize.Equals(Vector3Int.zero) ? aSize
                : CalcMinNodeSpaceNeededCombine(aSize, bSize, 2, 2, false, debugPos);
        }

        // ReSharper disable once TooManyDeclarations
        Vector3Int CalcMinNodeSpaceNeededCombine(Vector3Int aSize, Vector3Int bSize, int xAdd, int zAdd, bool flippingAllowed, Vector3? debugPos = null)
        {
            // add the extra space as shown in the diagrams just to be safe, f.e. in case of loop-backs 
            var minX = bSize.x + 1;
            var xDiff = Mathf.Abs(aSize.x + 1 - minX);
            var zDiff = Mathf.Abs(aSize.z + 1 - minX);
            var xA = aSize.x;
            var zA = aSize.z;

            // if this is a direct connection (followUp) we don't allow flipping
            var flipped = flippingAllowed && xDiff >= zDiff;
            if (flipped)
            {
                xA = aSize.z;
                zA = aSize.x;
            }
            // add the extra space as shown in the diagrams
            var z = bSize.z + zA + zAdd;
            var x = Mathf.Max(xA + xAdd, minX);

            // because we want things easy, we calculate min space needed only as if there were two dimensions (x, z)
            // so in y-space we can shift however we want, if both use 1 y-space this results in 1 y-space needed
            // A-B
            // if both use 2 space we can now shift to 3 y-spaces
            //   B
            // A-B
            // A
            var y = 1 + (bSize.y - 1) + (aSize.y - 1);

            // todo GRAPH: cleanup this trash code (make better)
            if (debugPos == null)
                return new Vector3Int(x, y, z);

            var roomChunkSize = Target.GridSettings.RoomChunkSize;

            // Debug.Log($"flipped {flipped} xAdd{xAdd} zAdd{zAdd} aSize {aSize} bSize {bSize} result {new Vector3Int(x, y, z)}");
            var aSizeV3 = new Vector3(xA + xAdd, aSize.y, zA + zAdd) * roomChunkSize;
            var bSizeV3 = new Vector3(minX, bSize.y, bSize.z) * roomChunkSize;

            var a = new Bounds(debugPos.Value + aSizeV3 * 0.5f, aSizeV3);
            var b = new Bounds(debugPos.Value + new Vector3(0, 0, aSizeV3.z) + bSizeV3 * 0.5f, bSizeV3);
            CustomHandleDraw.DrawBounds(a, Color.blue);
            CustomHandleDraw.DrawBounds(b, Color.cyan);

            return new Vector3Int(x, y, z);
        }
    }
}