using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Types;
using Level.Procedural;

namespace _Editor.Level.Procedural
{
    public static class UnlockableDrawNodeOperations
    {
        internal struct UnlockableDrawNode
        {
            public int Index;
            public DrawLevelGraphNodeData DrawNodeData;
            public LevelKeyID KeyLockID;
        }
        
        internal static UnlockableDrawNode GetNextUnlockable(IReadOnlyList<DrawLevelGraphNodeData> drawNodeData,
            IReadOnlyDictionary<LevelKeyID, LevelGraphKeyInfo> keyInfo,
            List<LevelGraphNode> permutations,
            List<int> permutationOrder)
        {
            using var unlockableData = SimplePool<List<UnlockableDrawNode>>.I.GetScoped();
            unlockableData.Obj.AddRange(UnlockableDrawNodeIndices(drawNodeData, keyInfo));
            foreach (var unlockable in unlockableData.Obj)
            {
                if (unlockable.KeyLockID.Counting != KeyLockCounting.Consumable)
                    return unlockable;
                if (keyInfo[unlockable.KeyLockID].Count > 1)
                    return unlockable;
                if (unlockableData.Obj.Count(a => a.KeyLockID == unlockable.KeyLockID) == 1)
                    return unlockable;
                    
                if (!permutations.Contains(unlockable.DrawNodeData.Node))
                    permutations.Add(unlockable.DrawNodeData.Node);
            }

            if (permutationOrder.IsNullOrEmpty()) 
                return default;
                
            foreach (var pIdx in permutationOrder)
            {
                if (!pIdx.IsInRange(permutations))
                    continue;
                
                var permutation = permutations[pIdx];
                var next = unlockableData.Obj.FindIndex(a => a.DrawNodeData.Node == permutation);
                if (next == -1)
                    continue;
                var nextUnlock = unlockableData.Obj[next];
                return nextUnlock;
            }

            return default;
        }
        
        static IEnumerable<UnlockableDrawNode> UnlockableDrawNodeIndices(IReadOnlyList<DrawLevelGraphNodeData> drawNodeData,
            IReadOnlyDictionary<LevelKeyID, LevelGraphKeyInfo> keyInfos)
        {
            for (var i = 0; i < drawNodeData.Count; i++)
            {
                var data = drawNodeData[i];
                var factoryNode = data.Node?.FactoryNode;
                if (factoryNode == null)
                    continue;
                if (factoryNode.NodeID.LevelKeyID == null)
                    continue;
                if (!factoryNode.NodeID.NodeTypeID.IsLock)
                    continue;
                var key = factoryNode.NodeID.LevelKeyID;
                if (key == null || !keyInfos.ContainsKey(key))
                    continue;
                if (keyInfos[key].Count <= 0)
                    continue;
                yield return new UnlockableDrawNode
                {
                    Index = i,
                    DrawNodeData = data,
                    KeyLockID = key
                };
            }
        }
    }
}