using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Core.Editor.Utility.GUIStyles;
using Core.Extensions;
using Core.Types;
using Core.Unity.Utility.GUITools;
using Level.Procedural;
using static _Editor.Level.Procedural.GraphOperations;
using static _Editor.Level.Procedural.LevelGraphStyleOperations;
using static _Editor.Level.Procedural.UnlockableDrawNodeOperations;

namespace _Editor.Level.Procedural
{
    public partial class LevelGraphEditor
    {
        readonly List<DrawLevelGraphNodeData> m_drawNodeData = new List<DrawLevelGraphNodeData>();
        readonly Dictionary<LevelKeyID, LevelGraphKeyInfo> m_keyInfos = new Dictionary<LevelKeyID, LevelGraphKeyInfo>();
        readonly Dictionary<NodeIDData, GUIStyle> m_styleMap = new Dictionary<NodeIDData, GUIStyle>();

        const float k_pushLockAfterKeySpace = 20f;
        const float k_spaceToConnectionLine = 40f;
        const float k_defaultHorizontalSpacing = 80f;
        const float k_defaultVerticalSpacing = 80f;
        const float k_startX = 15.0f;
        const float k_startY = 50.0f;
        void GridNodeGUI() => DrawGridGUI(ref m_guiGridData, this, DrawNodesAndConnections);

        void DrawNodesAndConnections()
        {
            if (Target == null)
                return;
            
            DrawStartNodeAndConnection(out var x, out var y);

            InitRunKeys();
            DrawKeys(k_startX, k_startY + 45f);
            
            m_drawNodeData.Clear();
            m_permutations.Clear();
            
            var maxX = x;
            InitDrawNodeXPos(Target.EntryLevelGraphNode, x, ref maxX);
            
            m_drawNodeData.Add(new DrawLevelGraphNodeData()
            {
                Node = Target.EntryLevelGraphNode, 
                PreviousNode = null
            });
            while (m_drawNodeData.Count > 0)
            {
                var flow = NextDrawNodeIndex(y, out var idx, out var yAdjusted);
                var node = m_drawNodeData[idx];
                m_drawNodeData.RemoveAt(idx);
                if (node.Flow == FlowResult.Stop)
                    flow = FlowResult.Stop;
                
                DrawNodeAndConnections(node, yAdjusted, flow);
            }
            
            DrawKeys(maxX + 60f, k_startY + 45f);
            FinishRun();
            DrawEndNodeAndConnection(maxX, y);
        }

        void DrawKeys(float x, float y)
        {
            foreach (var keyInfo in m_keyInfos)
            {
                if (keyInfo.Key == null)
                    continue;
                var isConsumable = keyInfo.Key.Counting == KeyLockCounting.Consumable;
                if (isConsumable && keyInfo.Value.Count <= 0)
                    continue;

                var keyNode = new NodeIDData()
                {
                    NodeTypeID = LevelGraphNode.DefaultID,
                    LevelKeyID = keyInfo.Key,
                };

                GetData(LegendConfig, keyNode, m_styleMap, out var nodeTypeData, out var keyStyleData, out var guiStyle);
                var name = isConsumable ? $"{keyInfo.Key.name} x {keyInfo.Value.Count}": $"{keyInfo.Key.name}";
                var nodeRect = NodeGUI.GetNodePos(new Vector2(x, y), nodeTypeData.NodeSize, m_guiGridData.Offset);

                NodeGUI.DrawNode(nodeRect, false, name, keyStyleData.Icon, guiStyle,
                    nodeTypeData.IconSize, nodeTypeData.TextOffset);

                y += nodeRect.height + 20f;
            }
        }

        void InitRunKeys()
        {
            m_keyInfos.Clear();
            if (Target.RunData.IsNullOrEmpty())
                return;
            if (!m_selectedRun.IsInRange(Target.RunData))
                return;
            
            ref var run = ref Target.RunData[m_selectedRun];
            foreach (var key in run.Keys)
            {
                if (m_keyInfos.ContainsKey(key.Key))
                {
                    var prev= m_keyInfos[key.Key];
                    prev.Count += key.Count;
                    m_keyInfos[key.Key] = prev;
                    continue;
                }   
                m_keyInfos.Add(key.Key, new LevelGraphKeyInfo {Count = key.Count});
            }
            
        }

        void FinishRun()
        {
            if (Target.RunData.IsNullOrEmpty())
                return;
            if (!m_selectedRun.IsInRange(Target.RunData))
                return;

            var keys = m_keyInfos.Where(k => k.Key.Counting != KeyLockCounting.Consumable 
                                  || k.Value.Count > 0);
            ref var run = ref Target.RunData[m_selectedRun];
            run.Results = new KeyAmountData[keys.Count()];

            var i = 0;
            foreach (var keyInfo in keys)
            {
                run.Results[i] = new KeyAmountData() { Key = keyInfo.Key, Count = keyInfo.Value.Count };
                ++i;
            }
        }
        
        void DrawStartNodeAndConnection(out float x, out float y)
        {
            x = k_startX;
            y = k_startY;

            var pos = new Vector2(x, y);
            var size = new Vector2(BuiltinSkins.BreadCrumbMid.fixedWidth, BuiltinSkins.BreadCrumbMid.fixedHeight);
            var nodeRect = NodeGUI.GetNodePos(pos, size, m_guiGridData.Offset);
            NodeGUI.DrawNode(nodeRect, false, "Start", null, BuiltinSkins.BreadCrumbMid);
            var endP = new Vector2(nodeRect.xMax, nodeRect.center.y);
            x += BuiltinSkins.BreadCrumbMid.fixedWidth + k_defaultHorizontalSpacing;
            
            DrawConnection(endP, new Vector2(m_guiGridData.Offset.x + x, endP.y));
        }
        
        void DrawEndNodeAndConnection(float maxX, float y)
        {
            var pos = new Vector2(maxX, y);
            var size = new Vector2(BuiltinSkins.BreadCrumbLeft.fixedWidth, BuiltinSkins.BreadCrumbLeft.fixedHeight);
            //endP = pos + new Vector2(size.x, size.y * 0.5f);
            var nodeRect = NodeGUI.GetNodePos(pos, size, m_guiGridData.Offset);
            NodeGUI.DrawNode(nodeRect, false, "End", null, BuiltinSkins.BreadCrumbLeft);
        }
        
        FlowResult NextDrawNodeIndex(float y, out int idx, out float yAdjusted)
        {
            yAdjusted = y;

            // try find node without requirements
            idx = m_drawNodeData.FindIndex(n => n.Node == null || n.Node.Type == null 
                                                    || !n.Node.Type.IsLock || n.Node.KeyLockID == null);
            if (idx != -1)
                return FlowResult.Continue;
            if (TryGetLockNodeAvailable(y, ref yAdjusted, out idx))
                return FlowResult.Continue;

            idx = 0;
            return FlowResult.Stop;
        }
        
        bool TryGetLockNodeAvailable(float y, ref float yAdjusted, out int nextDrawNodeIndex)
        {
            nextDrawNodeIndex = -1;
            var unlockable = GetNextUnlockable(m_drawNodeData, m_keyInfos, m_permutations, m_permutationOrder);
            if (unlockable.KeyLockID == null)
                return false;
            // var prevY = y;
            yAdjusted = Mathf.Max(m_keyInfos[unlockable.KeyLockID].LastDrawn_YPos + k_pushLockAfterKeySpace, y);
            // Debug.Log($"found key m_keyInfos[key].LastDrawn_YPos {m_keyInfos[key].LastDrawn_YPos} prevY {prevY} pushed y :{y}");
            nextDrawNodeIndex = unlockable.Index;
            return true;
        }

        void InitDrawNodeXPos(LevelGraphNode levelGraphNode, float x, ref float maxX)
        {
            if (levelGraphNode == null)
                return;
            
            levelGraphNode.NodePos.x = x;
            if (levelGraphNode.ConnectedTo != null)
            {
                var nextX = x;
                foreach (var c in levelGraphNode.ConnectedTo)
                {
                    InitDrawNodeXPos(c, nextX, ref maxX);
                    nextX = maxX;
                }
            }
            
            x += k_defaultHorizontalSpacing;
            maxX = Mathf.Max(x, maxX);
        }
        
        void DrawNodeAndConnections(DrawLevelGraphNodeData nodeData, float y, FlowResult flow)
        {
            var node = nodeData.Node;
            var prevNode = nodeData.PreviousNode;
            if (node == null)
                return;
            if (prevNode != null) 
                y = Mathf.Max(y, prevNode.NodePos.y + k_defaultVerticalSpacing);
            
            if (flow == FlowResult.Continue)
                UpdateKeyConsumable(node, y);

            if (node.Type == null && node.FactoryNode != null)
                node.FactoryNode.NodeID.NodeTypeID = LevelGraphNode.DefaultID;
            GetData(LegendConfig, node, m_styleMap,
                out var nodeTypeData, out var keyStyle, out var guiStyle);
            
            node.NodePos = new Vector2(node.NodePos.x, y);
            var nodePosOffset = node.NodePos + m_guiGridData.Offset;
            var nodeRect = NodeGUI.GetNodePos(node.NodePos, nodeTypeData.NodeSize, m_guiGridData.Offset);

            if (prevNode != null)
            {
                var tCon = GetConnectionPointTPos(nodeRect, nodeTypeData);
                var prevNodePos = prevNode.NodePos + m_guiGridData.Offset;
                var connectionLinePos = new Vector2(tCon.center.x, prevNodePos.y + k_spaceToConnectionLine);
                DrawConnection(tCon.center, connectionLinePos);
                DrawConnection(connectionLinePos, new Vector2(prevNodePos.x, connectionLinePos.y));
                DrawConnectionPoint(tCon);
            }
            
            var mouseDown = Event.current?.type == EventType.MouseDown;

            if (mouseDown)
                OnNodeMouseDown(node, prevNode, nodeRect);

            // Connection to next Node
            var rCon = GetConnectionPointRPos(nodeRect, nodeTypeData);
            var dCon = GetConnectionPointDPos(nodeRect, nodeTypeData);

            AddNodeButton(node, rCon);
            rCon.x += rCon.width;
            AddExitButton(node, rCon);
            if (node.ConnectedTo.IsNullOrEmpty())
            {
                AddLoopButton(node, dCon);
                if (node.LoopBack)
                {
                    var downPos = dCon.center + new Vector2(0f, 5f);
                    var backPos = downPos - new Vector2(20f, 0);
                    var upPos = backPos - new Vector2(0f, 5f);
                    DrawConnection(dCon.center, downPos);
                    DrawConnection(downPos, backPos);
                    DrawConnection(backPos, upPos);
                    for (var i = 0; i < 10; i++)
                    {
                        backPos -= new Vector2(0, 5f);
                        upPos -= new Vector2(0, 5f);
                        if (i % 2 == 0)
                            DrawConnection(backPos, upPos);
                    }
                }
            }   
            else
            {
                DrawConnection(dCon.center, nodePosOffset + new Vector2(0,k_spaceToConnectionLine));
                DrawConnectionPoint(dCon);
                
                foreach (var c in node.ConnectedTo)
                    m_drawNodeData.Add(new DrawLevelGraphNodeData()
                    {
                        Node = c,
                        PreviousNode = node,
                        Flow = flow
                    });
            }
            
            using (new ColorScope(flow == FlowResult.Continue? Color.white : Color.red))
            {
                NodeGUI.DrawNode(nodeRect, m_selectedLevelGraphNode == node, node.Name, keyStyle.Icon, guiStyle,
                    nodeTypeData.IconSize, nodeTypeData.TextOffset);
            }
        }

        void UpdateKeyConsumable(LevelGraphNode levelGraphNode, float y)
        {
            if (levelGraphNode.Type == null)
                return;
            if (levelGraphNode.Type.IsKey && levelGraphNode.KeyLockID != null)
                AddKey(levelGraphNode, y);
            if (levelGraphNode.Type.IsLock && levelGraphNode.KeyLockID != null)
                ConsumeKey(levelGraphNode);
        }

        void AddKey(LevelGraphNode levelGraphNode, float y)
        {
            if (!m_keyInfos.ContainsKey(levelGraphNode.KeyLockID))
                m_keyInfos.Add(levelGraphNode.KeyLockID, default);
            
            var keyInfo = m_keyInfos[levelGraphNode.KeyLockID];
            keyInfo.Count++;
            keyInfo.LastDrawn_YPos = y;
            m_keyInfos[levelGraphNode.KeyLockID] = keyInfo;
        }
        
        void ConsumeKey(LevelGraphNode levelGraphNode)
        {
            if (levelGraphNode.KeyLockID.Counting != KeyLockCounting.Consumable)
                return;
            if (!m_keyInfos.ContainsKey(levelGraphNode.KeyLockID))
                return;

            var keyInfo = m_keyInfos[levelGraphNode.KeyLockID];
            keyInfo.Count--;
            m_keyInfos[levelGraphNode.KeyLockID] = keyInfo;
        }

        void AddNodeButton(LevelGraphNode levelGraphNode, Rect rCon)
        {
            var plusStyle = BuiltinSkins.LightRoundPlus;
            var rect = new Rect(rCon.center.x + 2f, rCon.y, plusStyle.fixedWidth, plusStyle.fixedHeight);
            if (GUI.Button(rect, "", plusStyle))
                Target.AddNode(levelGraphNode);
        }
        
        void AddExitButton(LevelGraphNode levelGraphNode, Rect exitBtnRect)
        {
            var exitBtnStyle = BuiltinSkins.BreadCrumbLeft;
            var rect = new Rect(exitBtnRect.center.x + 2f, exitBtnRect.y, exitBtnStyle.fixedWidth, exitBtnStyle.fixedHeight);
            
            if (GUI.Button(rect, "", exitBtnStyle))
                levelGraphNode.HasEnd = !levelGraphNode.HasEnd;

            var exitLabelRect = new Rect(exitBtnRect.x + 2f, exitBtnRect.y - 15f, 50f, 25f);
            if (levelGraphNode.HasEnd)
                GUI.Label(exitLabelRect, "Exit", EditorStyles.miniBoldLabel);
        }
        
        void AddLoopButton(LevelGraphNode levelGraphNode, Rect dCon)
        {
            var plusStyle = BuiltinSkins.LightRoundPlus;
            var rect = new Rect(dCon.center.x, dCon.center.y + 5f, plusStyle.fixedWidth, plusStyle.fixedHeight);
            if (GUI.Button(rect, "", plusStyle))
                levelGraphNode.LoopBack = !levelGraphNode.LoopBack;
        }
        
        void OnNodeMouseDown(LevelGraphNode levelGraphNode, LevelGraphNode prevLevelGraphNode, Rect nodeRect)
        {
            if (!nodeRect.Contains(Event.current.mousePosition))
                return;

            var clickLeft = Event.current.button == 0;
            var clickRight = Event.current.button == 1;
            if (clickLeft)
            {
                m_selectedLevelGraphNode = levelGraphNode;
                SceneView.RepaintAll();
            }
            else if (clickRight)
                ProcessContextMenuOnNode(levelGraphNode, prevLevelGraphNode);
        }

        void ProcessContextMenuOnNode(LevelGraphNode levelGraphNode, LevelGraphNode prevLevelGraphNode)
        {
            var genericMenu = new GenericMenu();
            genericMenu.AddItem(new GUIContent("Remove Node Safe"), false, () => Target.RemoveNodeFrom(levelGraphNode, prevLevelGraphNode));
            genericMenu.AddItem(new GUIContent("Wipe Node"), false, () => Target.WipeNodeFrom(levelGraphNode, prevLevelGraphNode));

            var idxOfNode = prevLevelGraphNode?.ConnectedTo.FirstIndexWhere(a => a == levelGraphNode) ?? -1;
            if (idxOfNode > 0)
                genericMenu.AddItem(new GUIContent("Move Left"), false, () => Target.MoveLeft(levelGraphNode, prevLevelGraphNode));
            if (idxOfNode < prevLevelGraphNode?.ConnectedTo.Length)
                genericMenu.AddItem(new GUIContent("Move Right"), false, () => Target.MoveRight(levelGraphNode, prevLevelGraphNode));
            genericMenu.ShowAsContext();
        }
    }
}