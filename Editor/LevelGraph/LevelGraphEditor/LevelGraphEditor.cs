using System.Linq;
using Core.Editor.Inspector;
using Core.Editor.Utility;
using UnityEditor;
using UnityEngine;
using Core.Unity.Extensions;
using Core.Extensions;
using Level.Procedural;
using static _Editor.Level.Procedural.GraphOperations;

namespace _Editor.Level.Procedural
{
    /// <summary> LevelGraphEditor </summary>
    [CustomEditor(typeof(LevelGraphConfig))]
    public class LevelGraphInspector : BaseInspector<LevelGraphEditor>
    {
    }

    public partial class LevelGraphEditor : BaseEditor<LevelGraphConfig>
    {
        Vector2 m_scrollPos;
        
        GUIGridData m_guiGridData = GUIGridData.Default; 
        
        LevelGraphNode m_selectedLevelGraphNode;

        UnityEditor.Editor m_legendEditor;
        
        #region properties

        LevelGraphLegendConfig LegendConfig => Target.LegendConfig;

        NodeTypeData[] NodeTypeData => LegendConfig.NodeTypeData;
        KeyStyleData[] KeyData => LegendConfig.KeyData;
        Color[] Colors => LegendConfig.Colors;

        #endregion

        #region Init + Terminate

        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);

            // m_keyLockNames = new List<string> { "" };

            FindDefaultNodeTypeID();

            SceneView.duringSceneGui += OnSceneGUI;
        }

        void FindDefaultNodeTypeID()
        {
            var guids = AssetDatabase.FindAssets($"t: {nameof(NodeTypeID)}");
            foreach (var guid in guids)
            {
                var path = AssetDatabase.GUIDToAssetPath(guid);
                var assets = AssetDatabase.LoadAllAssetsAtPath(path).OfType<NodeTypeID>();
                foreach (var a in assets)
                {
                    LevelGraphNode.DefaultID = a;
                    if (LevelGraphNode.DefaultID != null && LevelGraphNode.DefaultID.name.Contains("Default"))
                        break;
                }

                if (LevelGraphNode.DefaultID != null && LevelGraphNode.DefaultID.name.Contains("Default"))
                    break;
            }
        }

        public override void Terminate()
        {
            base.Terminate();
            SceneView.duringSceneGui -= OnSceneGUI;
            //Undo.undoRedoPerformed -= UndoRedoPerformed;
        }

        #endregion

        #region GUI

        public override void OnGUI(float width)
        {
            BaseGUI();

            DefaultNodeTypeGUI();

            GridNodeGUI();
            SplitterGUI(ref m_guiGridData);
            
            using (var svs = new GUILayout.ScrollViewScope(m_scrollPos))
            {
                m_scrollPos = svs.scrollPosition;
                DataGUI();
            }

            EndGUI();
        }

        void DefaultNodeTypeGUI()
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                EditorGUILayout.LabelField("Default NodeType: ");
                LevelGraphNode.DefaultID = EditorGUILayout.ObjectField(LevelGraphNode.DefaultID, typeof(NodeTypeID), false) as NodeTypeID;
            }
        }

        void DataGUI()
        {
            if (Target == null)
                return;

            RunGUI();
            PermutationGUI();
            SelectedNodeGUI();

            //DrawPropertiesExcluding(serializedObject, k_excludeFromDefaultInspector);

            if (LegendConfig == null)
            {
                if (m_legendEditor == null)
                    return;
                m_legendEditor.DestroyEx();
                m_legendEditor = null;
                return;
            }

            //if (m_legendEditor == null)
            //    CreateCachedEditor(Target.LegendConfig, null, ref m_legendEditor);
            if (m_legendEditor != null)
                m_legendEditor.OnInspectorGUI();
        }

        void SelectedNodeGUI()
        {
            var node = m_selectedLevelGraphNode;
            if (node == null)
                return;
            node.FactoryNode = EditorGUILayout.ObjectField(node.FactoryNode, typeof(LevelFactoryNode), false) as LevelFactoryNode;
            var facNode = node.FactoryNode;
            FactoryNodeGUI(facNode);
            CustomGUI.HSplitter();
            node.HasEnd = EditorGUILayout.Toggle("Exit: ", node.HasEnd);
            node.LoopBack = EditorGUILayout.Toggle("LoopBack: ", node.LoopBack);
        }

        void FactoryNodeGUI(LevelFactoryNode factoryNode)
        {
            if (factoryNode == null)
                return;
            
            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Space(15);
                using (new EditorGUILayout.VerticalScope())
                {
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        EditorGUILayout.LabelField("Name: ");
                        factoryNode.Name = EditorGUILayout.TextField(factoryNode.Name);
                    }

                    factoryNode.NodeID.NodeTypeID =
                        EditorGUILayout.ObjectField(factoryNode.NodeID.NodeTypeID, typeof(NodeTypeID), false) as
                            NodeTypeID;

                    KeyLockGUI(factoryNode);
                }
                
                factoryNode.IconOverride = EditorGUILayout.ObjectField(factoryNode.IconOverride, typeof(Texture2D), false, 
                    GUILayout.Width(50f), GUILayout.Height(50f)) as Texture2D;
            }
        }

        static void KeyLockGUI(LevelFactoryNode factoryNode)
        {
            var keyLock = factoryNode.NodeID.NodeTypeID != null && factoryNode.NodeID.NodeTypeID.IsKeyLock;
            if (!keyLock)
                return;

            using var horizontalScope = new EditorGUILayout.HorizontalScope();
            EditorGUILayout.LabelField("KeyLockID: ");
            using var check = new EditorGUI.ChangeCheckScope();
            factoryNode.NodeID.LevelKeyID =
                EditorGUILayout.ObjectField(factoryNode.NodeID.LevelKeyID, typeof(LevelKeyID), false) as LevelKeyID;

            if (!check.changed)
                return;

            if (factoryNode.NodeID.LevelKeyID != null && factoryNode.Name.IsNullOrEmpty())
                factoryNode.Name = factoryNode.NodeID.LevelKeyID.name;
        }

        #endregion
    }
}