using System.Collections.Generic;
using Core.Editor.Utility;
using Core.Editor.Utility.GUIStyles;
using Core.Extensions;
using Core.Types;
using UnityEditor;
using UnityEngine;

using static _Editor.Level.Procedural.LevelGraphStyleOperations;

namespace _Editor.Level.Procedural
{
    public static class GridStyleOperations
    {
        public static bool AllTexturesSetup(ref GridTexturesAndStyles texturesAndStyles)
        {
            var allTexturesSetup = true;
            
            texturesAndStyles.LayoutStyleMap ??= new Dictionary<LevelNodeGridLayoutData, GUIStyle>();
            texturesAndStyles.NodeStyleMap ??= new Dictionary<NodeIDData, GUIStyle>();
            
            texturesAndStyles.PathWay ??= new Texture2D[4];
            texturesAndStyles.RoomExtensions ??= new Texture2D[4];
            texturesAndStyles.Entry ??= new Texture2D[5];
            texturesAndStyles.Exit ??= new Texture2D[5];
            texturesAndStyles.EntryStyle ??= new GUIStyle[5];
            texturesAndStyles.ExitStyle ??= new GUIStyle[5];
            
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.RoomTex, "icon_room", ref texturesAndStyles.TextureNotFound);
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.ConnectorTex, "icon_connector", ref texturesAndStyles.TextureNotFound);
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.PathWay[(int) Cardinals.North], "path_top", ref texturesAndStyles.TextureNotFound);
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.PathWay[(int) Cardinals.West], "path_left", ref texturesAndStyles.TextureNotFound);
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.PathWay[(int) Cardinals.East], "path_right", ref texturesAndStyles.TextureNotFound);
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.PathWay[(int) Cardinals.South], "path_bottom", ref texturesAndStyles.TextureNotFound);
            
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.RoomExtensions[(int) Cardinals.North], "r_top", ref texturesAndStyles.TextureNotFound);
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.RoomExtensions[(int) Cardinals.West], "r_left", ref texturesAndStyles.TextureNotFound);
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.RoomExtensions[(int) Cardinals.East], "r_right", ref texturesAndStyles.TextureNotFound);
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.RoomExtensions[(int) Cardinals.South], "r_bottom", ref texturesAndStyles.TextureNotFound);
            
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.Entry[(int) Cardinals.North], "entry_north", ref texturesAndStyles.TextureNotFound);
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.Entry[(int) Cardinals.West], "entry_west", ref texturesAndStyles.TextureNotFound);
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.Entry[(int) Cardinals.East], "entry_east", ref texturesAndStyles.TextureNotFound);
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.Entry[(int) Cardinals.South], "entry_south", ref texturesAndStyles.TextureNotFound);
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.Entry[(int) Cardinals.Invalid], "entry_vertical", ref texturesAndStyles.TextureNotFound);

            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.Exit[(int) Cardinals.North], "exit_north", ref texturesAndStyles.TextureNotFound);
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.Exit[(int) Cardinals.West], "exit_west", ref texturesAndStyles.TextureNotFound);
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.Exit[(int) Cardinals.East], "exit_east", ref texturesAndStyles.TextureNotFound);
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.Exit[(int) Cardinals.South], "exit_south", ref texturesAndStyles.TextureNotFound);
            allTexturesSetup &= EnsureTextureSetup(ref texturesAndStyles.Exit[(int) Cardinals.Invalid], "exit_vertical", ref texturesAndStyles.TextureNotFound);
            
            allTexturesSetup &= EnsureGUIStyleSetup(ref texturesAndStyles.EntryStyle[0], texturesAndStyles.Entry[0]);
            allTexturesSetup &= EnsureGUIStyleSetup(ref texturesAndStyles.EntryStyle[1], texturesAndStyles.Entry[1]);
            allTexturesSetup &= EnsureGUIStyleSetup(ref texturesAndStyles.EntryStyle[2], texturesAndStyles.Entry[2]);
            allTexturesSetup &= EnsureGUIStyleSetup(ref texturesAndStyles.EntryStyle[3], texturesAndStyles.Entry[3]);
            allTexturesSetup &= EnsureGUIStyleSetup(ref texturesAndStyles.EntryStyle[4], texturesAndStyles.Entry[4]);
            
            allTexturesSetup &= EnsureGUIStyleSetup(ref texturesAndStyles.ExitStyle[0], texturesAndStyles.Exit[0]);
            allTexturesSetup &= EnsureGUIStyleSetup(ref texturesAndStyles.ExitStyle[1], texturesAndStyles.Exit[1]);
            allTexturesSetup &= EnsureGUIStyleSetup(ref texturesAndStyles.ExitStyle[2], texturesAndStyles.Exit[2]);
            allTexturesSetup &= EnsureGUIStyleSetup(ref texturesAndStyles.ExitStyle[3], texturesAndStyles.Exit[3]);
            allTexturesSetup &= EnsureGUIStyleSetup(ref texturesAndStyles.ExitStyle[4], texturesAndStyles.Exit[4]);
            
            return allTexturesSetup;
        }
        static bool EnsureTextureSetup(ref Texture2D tex, string textureName, ref bool texNotFound)
        {
            if (tex != null)
                return true;

            tex = LoadTexture($"t: texture2d {textureName}", ref texNotFound);
            if (tex == null)
                tex = EditorGUILayout.ObjectField($"{textureName}", tex, typeof(Texture2D), false) as Texture2D;

            return false;
        }

        static bool EnsureGUIStyleSetup(ref GUIStyle style, Texture2D tex)
        {
            if (tex == null)
            {
                style = null;
                return false;
            }
            if (style != null)
                return true;

            style ??= new GUIStyle();
            StyleHelpers.InitStyleOnOffPress(style, tex, tex, tex);
            return true;
        }
        
        static Texture2D LoadTexture(string filter, ref bool textureNotFound)
        {
            if (textureNotFound)
                return null;
            var guids = AssetDatabase.FindAssets(filter);
            if (guids.IsNullOrEmpty())
                return null;
            var path = AssetDatabase.GUIDToAssetPath(guids[0]);
            var tex = AssetDatabase.LoadAssetAtPath<Texture2D>(path);
            if (tex == null)
                textureNotFound = true;
            return tex;
        }

        static void AddTexture(Texture2D combinedTex, Texture2D addTex)
        {
            var combinedPixels = combinedTex.GetPixels();
            var addTexPixels = addTex.GetPixels();
            Debug.Assert(combinedPixels.Length == addTexPixels.Length);
            for (var i = 0; i < addTexPixels.Length; i++)
            {
                if (addTexPixels[i].a <= float.Epsilon)
                    continue;
                combinedPixels[i] = addTexPixels[i];
            }

            combinedTex.SetPixels(combinedPixels);
        }
        
        public static void UpdateStyle(LevelNodeGridLayoutData tile, 
            GridTexturesAndStyles texturesAndStyles,
            Dictionary<LevelNodeGridLayoutData, GUIStyle> layoutStyles)
        {
            if (layoutStyles.ContainsKey(tile) && layoutStyles[tile].normal.background != null)
                return;
            
            // Debug.Log($"Init Style {tile.Block} {tile.Connector} {tile.B} {tile.L} {tile.R} {tile.T}");
            var style = new GUIStyle();

            if (tile.Block)
            {
                GetColoredStyle(CustomStyles.Gray, Color.red, out var defaultTex, out var activeTex,
                    out var clickedTex);
                StyleHelpers.InitStyleOnOffPress(style, defaultTex, activeTex, clickedTex);
                // layoutStyles.Add(tile, style);
                layoutStyles[tile] = style;
                return;
            }

            if (tile.IsEmpty)
            {
                layoutStyles.Add(tile, CustomStyles.Gray);
                return;
            }

            Texture2D combinedTex; 
            if (tile.Connector)
            {
                combinedTex = new Texture2D(texturesAndStyles.ConnectorTex.width, texturesAndStyles.ConnectorTex.height);
                combinedTex.SetPixels(texturesAndStyles.ConnectorTex.GetPixels());
            }
            else
            {
                combinedTex = new Texture2D(texturesAndStyles.RoomTex.width, texturesAndStyles.RoomTex.height);
                combinedTex.SetPixels(texturesAndStyles.RoomTex.GetPixels());
            }

            AddPathOrExtension(tile.R,Cardinals.East, texturesAndStyles, combinedTex);
            AddPathOrExtension(tile.L,Cardinals.West, texturesAndStyles, combinedTex);
            AddPathOrExtension(tile.T,Cardinals.North, texturesAndStyles, combinedTex);
            AddPathOrExtension(tile.B,Cardinals.South, texturesAndStyles, combinedTex);
            
            Colorize(combinedTex, Color.gray, out var inactiveTex);
            combinedTex.Apply();
            StyleHelpers.InitStyleOnOffPress(style, inactiveTex, combinedTex, combinedTex);
            
            // if (!layoutStyles.ContainsKey(tile))
            //     layoutStyles.Add(tile, style);
            // else 
            layoutStyles[tile] = style;
        }

        static void AddPathOrExtension(ConnectionType tile, Cardinals direction, GridTexturesAndStyles texturesAndStyles, Texture2D combinedTex)
        {
            switch (tile)
            {
                case ConnectionType.Path: AddTexture(combinedTex, texturesAndStyles.PathWay[(int) direction]); break;
                case ConnectionType.RoomExtension: AddTexture(combinedTex, texturesAndStyles.RoomExtensions[(int) direction]); break;
            }
        }
    }
}