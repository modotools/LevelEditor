using System.Collections.Generic;
using System.Linq;
using Core.Editor.Utility;
using Level.Procedural;
using UnityEngine;

namespace _Editor.Level.Procedural
{
    public static class LevelGraphStyleOperations
    {
        public static void GetColoredStyle(GUIStyle style, Color color, out Texture2D defaultTex,
            out Texture2D activeTex, out Texture2D clickedTex)
        {
            Colorize(style.normal.background, color, out defaultTex);
            Colorize(style.onNormal.background, color, out activeTex);
            Colorize(style.onActive.background, color, out clickedTex);
        }

        public static void GetColoredBGData(NodeBG bgData, Color color, out Texture2D defaultTex,
            out Texture2D activeTex, out Texture2D clickedTex)
        {
            Colorize(bgData.BackgroundStyleDefault, color, out defaultTex);
            Colorize(bgData.BackgroundStyleActive, color, out activeTex);
            Colorize(bgData.BackgroundStyleClicked, color, out clickedTex);
        }

        public static void Colorize(Texture2D origDef, Color color, out Texture2D defaultTex)
        {
            defaultTex = null;
            if (origDef == null)
                return;
            defaultTex = new Texture2D(origDef.width, origDef.height);

            for (var y = 0; y < defaultTex.height; y++)
            for (var x = 0; x < defaultTex.width; x++)
            {
                var origPix = origDef.GetPixel(x, y);
                Color.RGBToHSV(origPix * color, out var hue, out var s, out var val);
                var shifted = Color.HSVToRGB(Mathf.Repeat(hue + 0.125f * val - 0.1125f, 1f), s, val);
                shifted.a = origPix.a;
                defaultTex.SetPixel(x, y, shifted);
            }

            defaultTex.Apply();
        }

        public static void GetSizeAndStyle(LevelGraphLegendConfig legendConfig,
            NodeTypeData nodeTypeData, KeyStyleData keyStyle,
            Dictionary<NodeIDData, GUIStyle> styleMap,
            out GUIStyle guiStyle)
        {
            guiStyle = default;
            if (nodeTypeData.NodeTextures == null)
                return;
            var bgData = nodeTypeData.NodeTextures;

            var styleKey = new NodeIDData()
            {
                LevelKeyID = keyStyle.Key,
                NodeTypeID = nodeTypeData.Type
            };

            if (styleMap.ContainsKey(styleKey) && styleMap[styleKey].normal.background != null)
            {
                guiStyle = styleMap[styleKey];
                return;
            }
            // Debug.Log($"Init Style {styleKey.LevelKeyID} {styleKey.NodeTypeID}");
            InitStyle(legendConfig, keyStyle, bgData, out guiStyle);
            styleMap[styleKey] = guiStyle;
        }

        static void InitStyle(LevelGraphLegendConfig legendConfig, KeyStyleData keyStyleData, NodeBG bgData,
            out GUIStyle style)
        {
            // Debug.Log($"Init Style{_info.Name}{bgData.BackgroundStyleDefault.name}");
            style = new GUIStyle();
            var colorIdx = keyStyleData.ColorIdx;
            GetColoredBGData(bgData, legendConfig.Colors[colorIdx], out var defaultTex, out var activeTex,
                out var clickedTex);
            if (bgData.BackgroundStyleClicked != null)
                StyleHelpers.InitStyleOnOffPress(style, defaultTex, activeTex, clickedTex);
            else
                StyleHelpers.InitStyleOnOff(style, defaultTex, activeTex);
        }

        public static void GetData(LevelGraphLegendConfig legendConfig,
            LevelGraphNode node,
            Dictionary<NodeIDData, GUIStyle> styleMap,
            out NodeTypeData nodeTypeData, out KeyStyleData keyStyle, out GUIStyle guiStyle) =>
            GetData(legendConfig, node.IDData,
                styleMap, out nodeTypeData, out keyStyle, out guiStyle);

        public static void GetData(LevelGraphLegendConfig legendConfig, NodeIDData nodeIDData,
            Dictionary<NodeIDData, GUIStyle> styleMap,
            out NodeTypeData nodeTypeData, out KeyStyleData keyStyle, out GUIStyle guiStyle)
        {
            GetKeyAndNodeData(legendConfig, nodeIDData, out nodeTypeData, out keyStyle);
            GetSizeAndStyle(legendConfig, nodeTypeData, keyStyle, styleMap, out guiStyle);
        }

        public static void GetKeyAndNodeData(LevelGraphLegendConfig legendConfig, NodeIDData nodeIDData,
            out NodeTypeData nodeTypeData, out KeyStyleData keyStyle)
        {
            nodeTypeData = legendConfig.NodeTypeData.FirstOrDefault(nodeType => nodeType.Type == nodeIDData.NodeTypeID);
            keyStyle = legendConfig.KeyData.FirstOrDefault(kd => kd.Key == nodeIDData.LevelKeyID);
        }
    }
}