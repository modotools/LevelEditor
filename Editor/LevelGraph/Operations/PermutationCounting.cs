using System.Collections.Generic;
using System.Linq;
using Core.Types;

namespace _Editor.Level.Procedural
{
    public static class PermutationCounting
    {
        public static List<List<T>> GeneratePermutations<T>(List<T> items, ref List<List<T>> results)
        {
            using var current_permutationScoped = SimplePool<List<T>>.I.GetScoped();
            using var in_selectionScoped = SimplePool<List<bool>>.I.GetScoped();
            for (var i = 0; i < items.Count; i++)
            {
                current_permutationScoped.Obj.Add(default);
                in_selectionScoped.Obj.Add(false);
            }   
            
            // Make an array to hold the
            // permutation we are building.
            var current_permutation = current_permutationScoped.Obj;

            // Make an array to tell whether
            // an item is in the current selection.
            var in_selection = in_selectionScoped.Obj;

            results.Clear();
            
            // Build the combinations recursively.
            PermuteItems(items, in_selection,
                current_permutation, results, 0);

            // Return the results.
            return results;
        }
        
        // Recursively permute the items that are
        // not yet in the current selection.
        static void PermuteItems<T>(IReadOnlyList<T> items, IList<bool> in_selection,
            IList<T> current_permutation, ICollection<List<T>> results,
            int next_position)
        {
            // See if all of the positions are filled.
            if (next_position == items.Count)
            {
                // All of the positioned are filled.
                // Save this permutation.
                results.Add(current_permutation.ToList());
            }
            else
            {
                // Try options for the next position.
                for (var i = 0; i < items.Count; i++)
                {
                    if (in_selection[i]) 
                        continue;
                    // Add this item to the current permutation.
                    in_selection[i] = true;
                    current_permutation[next_position] = items[i];

                    // Recursively fill the remaining positions.
                    PermuteItems(items, in_selection,
                        current_permutation, results,
                        next_position + 1);

                    // Remove the item from the current permutation.
                    in_selection[i] = false;
                }
            }
        }
    }
}