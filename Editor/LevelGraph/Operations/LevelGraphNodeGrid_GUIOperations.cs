using System.Collections.Generic;
using Core.Editor.Interface;
using Core.Editor.Utility.GUIStyles;
using Core.Extensions;
using Core.Types;
using Level.Procedural;
using UnityEditor;
using UnityEngine;
using static _Editor.Level.Procedural.GridStyleOperations;
using static _Editor.Level.Procedural.LevelGraphStyleOperations;
using static Level.Procedural.LevelLayoutGridOperations;

namespace _Editor.Level.Procedural
{
    public static class LevelGraphNodeGrid_GUIOperations
    {
        #region Setup

        public static void SetupGUI(ref LevelLayoutGrid grid)
        {
            grid.Size.x = EditorGUILayout.IntField("Grid-width: ", grid.Size.x);
            grid.Size.y = EditorGUILayout.IntField("Grid-height: ", grid.Size.y);

            if (GUILayout.Button("Create Grid"))
                CreateGrid(ref grid);
        }

        public static void CreateGrid(ref LevelLayoutGrid grid)
        {
            grid.Size.x = Mathf.Abs(grid.Size.x);
            grid.Size.y = Mathf.Abs(grid.Size.y);
            grid.Grid = new LevelNodeGridData[grid.Size.x * grid.Size.y];
        }

        #endregion

        #region Process Events

        public static void MoveViaArrows(Vector2Int gridSize, ref Vector2Int selectedTile, IRepaintable repaintable)
        {
            var e = Event.current;
            if (e.type != EventType.KeyDown)
                return;

            var up = e.keyCode == KeyCode.UpArrow;
            var down = e.keyCode == KeyCode.DownArrow;
            var left = e.keyCode == KeyCode.LeftArrow;
            var right = e.keyCode == KeyCode.RightArrow;

            Vector2Int nextTile;
            if (up) nextTile = selectedTile + Vector2Int.up;
            else if (down) nextTile = selectedTile + Vector2Int.down;
            else if (left) nextTile = selectedTile + Vector2Int.left;
            else if (right) nextTile = selectedTile + Vector2Int.right;
            else return;
            
            if (nextTile.x >= gridSize.x || nextTile.x < 0)
                return;
            if (nextTile.y >= gridSize.y || nextTile.y < 0)
                return;

            e.Use();
            selectedTile = nextTile;
            repaintable.Repaint();
        }
        
        static void OnNodeMouseDown(Vector2Int gridPos, Rect nodeRect, ref Vector2Int selectedTile)
        {
            var current = Event.current;
            if (!nodeRect.Contains(current.mousePosition))
                return;
            var clickLeft = current.button == 0;
            // var clickRight = Event.current.button == 1;
            if (!clickLeft)
                return;

            selectedTile = gridPos;
        }

        #region On Passage Changed

        static void OnTopPassageChanged(ref LevelLayoutGrid grid, Vector2Int selectedTile) => CopyTopPassageToNextTileBottom(ref grid, selectedTile);
        
        static void OnBottomPassageChanged(ref LevelLayoutGrid grid, Vector2Int selectedTile) => CopyBottomPassageToNextTileTop(ref grid, selectedTile);

        static void OnRightPassageChanged(ref LevelLayoutGrid grid, Vector2Int selectedTile) => CopyRightPassageToNextTileLeft(ref grid, selectedTile);

        static void OnLeftPassageChanged(ref LevelLayoutGrid grid, Vector2Int selectedTile) => CopyLeftPassageToNextTileRight(ref grid, selectedTile);

        #endregion

        #endregion

        #region Data GUI

        public static void SelectedTileGUI(LevelLayoutGrid grid, Vector2Int selectedTile,
            GridTexturesAndStyles gridTexturesAndStyles)
        {
            if (grid.Grid == null)
                return;
            var x = selectedTile.x;
            var y = selectedTile.y;
            if (!x.IsInRange(0, grid.Size.x))
                return;
            if (!y.IsInRange(0, grid.Size.y))
                return;

            using var check = new EditorGUI.ChangeCheckScope();
            EditorGUILayout.LabelField($"selected {x} {y}");
            ref var tile = ref grid.Grid[y * grid.Size.x + x];

            tile.RoomLevelGraphNode = EditorGUILayout.ObjectField("FactoryNode: ", tile.RoomLevelGraphNode,
                typeof(LevelFactoryNode), false) as LevelFactoryNode;

            // tile.RoomLevelGraphNode
            ref var layout = ref tile.LayoutData;
            using (new EditorGUILayout.HorizontalScope())
            {
                layout.Block = GUILayout.Toggle(layout.Block, "Block", "Button", GUILayout.Width(100f));
                layout.Connector = GUILayout.Toggle(layout.Connector,"Connector", "Button", GUILayout.Width(100f));
            }

            var controlRect = EditorGUILayout.GetControlRect(false, 150f);
            controlRect.x += 100f;

            if (PassageToggle(controlRect, new Vector2(65f, 25f),
                ref layout.T, out var pos) == ChangeCheck.Changed)
                OnTopPassageChanged(ref grid, selectedTile);
            
            PassageFactory(layout.T == ConnectionType.Path, ref tile.T, pos + new Vector2(25f, 0f));
            
            if (PassageToggle(controlRect, new Vector2(25f, 65f),
                ref layout.L, out pos) == ChangeCheck.Changed)
                OnLeftPassageChanged(ref grid, selectedTile);
            PassageFactory(layout.L == ConnectionType.Path, ref tile.L, new Vector2(0f, pos.y));
            
            DrawRoomNode(gridTexturesAndStyles, controlRect, tile);

            if (PassageToggle(controlRect, new Vector2(100f, 65f), 
                ref layout.R, out pos) == ChangeCheck.Changed)
                OnRightPassageChanged(ref grid, selectedTile);

            PassageFactory(layout.R == ConnectionType.Path, ref tile.R, pos + new Vector2(25f, 0f));
            
            if (PassageToggle(controlRect, new Vector2(65f, 100f),
                ref layout.B, out pos) == ChangeCheck.Changed)
                OnBottomPassageChanged(ref grid, selectedTile);
            
            PassageFactory(layout.B == ConnectionType.Path, ref tile.B, pos + new Vector2(25f, 0f));
            
            if (check.changed)
                UpdateStyle(tile.LayoutData, gridTexturesAndStyles, gridTexturesAndStyles.LayoutStyleMap);
        }


        static void DrawRoomNode(GridTexturesAndStyles gridTexturesAndStyles, Rect controlRect, LevelNodeGridData tile)
        {
            var pos = controlRect.position + new Vector2(50f, 50f);
            if (gridTexturesAndStyles.LayoutStyleMap == null ||
                !gridTexturesAndStyles.LayoutStyleMap.TryGetValue(tile.LayoutData, out var style))
                style = CustomStyles.Gray;

            Texture2D icon = null;
            if (tile.RoomLevelGraphNode != null)
                icon = tile.RoomLevelGraphNode.IconOverride;
            NodeGUI.DrawNode(new Rect(pos, 50f * Vector2.one), true, "", icon, style);
        }

        static void PassageFactory(bool hasPassage, ref LevelFactoryNode node, Vector2 pos)
        {
            node = hasPassage
                ? EditorGUI.ObjectField(new Rect(pos, new Vector2(125f, 25f)), "", node,
                    typeof(LevelFactoryNode), false) as LevelFactoryNode
                : null;
        }

        static ChangeCheck PassageToggle(Rect controlRect, Vector2 offset, ref ConnectionType layoutToggle, out Vector2 pos)
        {
            using var check = new EditorGUI.ChangeCheckScope();
            pos = controlRect.position + offset;
            var text = "";
            switch (layoutToggle)
            {
                case ConnectionType.Path: text = "P"; break;
                case ConnectionType.RoomExtension: text = "R"; break;
            }

            if (GUI.Button(new Rect(pos, 25f * Vector2.one), text))
                layoutToggle = (ConnectionType) (((int) layoutToggle + 1) % 3); 
            // layoutToggle = GUI.Toggle(new Rect(pos, 25f * Vector2.one), layoutToggle, "");
            return check.changed ? ChangeCheck.Changed : ChangeCheck.NotChanged;
        }

        #endregion

        #region Grid GUI

        public static void DrawTiles(LevelGraphLegendConfig legendConfig, LevelLayoutGrid grid, int tileSize,
            GUIGridData guiGridData, ref Vector2Int selectedTile,
            GridTexturesAndStyles gridTexturesAndStyles)
        {
            if (gridTexturesAndStyles.LayoutStyleMap == null)
            {
                Debug.LogError("LayoutStyleMap == null");
                return;
            }
            var gridHeight = grid.Size.y * tileSize;
            for (var y = 0; y < grid.Size.y; y++)
            for (var x = 0; x < grid.Size.x; x++)
            {
                var tile = grid.Grid[y * grid.Size.x + x];
                var pos = new Vector2Int((tileSize + 2) * x, gridHeight - (tileSize + 2) * y);
                var gridPos = new Vector2Int(x, y);
                
                var nodeRect = NodeGUI.GetNodePos(new Vector2(pos.x, pos.y), new Vector2(tileSize, tileSize),
                    guiGridData.Offset);

                var mouseDown = Event.current?.type == EventType.MouseDown;
                if (mouseDown)
                    OnNodeMouseDown(gridPos, nodeRect, ref selectedTile);

                UpdateStyle(tile.LayoutData, gridTexturesAndStyles, gridTexturesAndStyles.LayoutStyleMap);

                if (!gridTexturesAndStyles.LayoutStyleMap.TryGetValue(tile.LayoutData, out var style))
                    style = CustomStyles.Gray;

                Texture2D icon = null;
                if (tile.RoomLevelGraphNode != null)
                    icon = tile.RoomLevelGraphNode.IconOverride;
                
                var selected = gridPos == selectedTile;
                NodeGUI.DrawNode(nodeRect, selected, "", icon, style);
                var node = tile.RoomLevelGraphNode;

                DrawNode(legendConfig, node, pos, selected, guiGridData, gridTexturesAndStyles.NodeStyleMap);
            }
            
            // draw doorways on top
            for (var y = 0; y < grid.Size.y; y++)
            for (var x = 0; x < grid.Size.x; x++)
            {
                var tile = grid.Grid[y * grid.Size.x + x];
                var pos = new Vector2Int((tileSize + 2) * x, gridHeight - (tileSize + 2) * y);
                var gridPos = new Vector2Int(x, y);
                
                var selected = gridPos == selectedTile;
                if (tile.T != null)
                {
                    pos -= new Vector2Int(0, Mathf.RoundToInt(.5f * tileSize));
                    DrawNode(legendConfig, tile.T, pos, selected, guiGridData, gridTexturesAndStyles.NodeStyleMap);
                }
                if (tile.B != null)
                {
                    pos += new Vector2Int(0, Mathf.RoundToInt(.5f * tileSize));
                    DrawNode(legendConfig, tile.B, pos, selected, guiGridData, gridTexturesAndStyles.NodeStyleMap);
                }
                if (tile.R != null)
                {
                    pos += new Vector2Int(Mathf.RoundToInt(.5f * tileSize), 0);
                    DrawNode(legendConfig, tile.R, pos, selected, guiGridData, gridTexturesAndStyles.NodeStyleMap);
                }
                if (tile.L != null)
                {
                    pos -= new Vector2Int(Mathf.RoundToInt(.5f * tileSize), 0);
                    DrawNode(legendConfig, tile.L, pos, selected, guiGridData, gridTexturesAndStyles.NodeStyleMap);
                }
            }

            DrawEntryArrow(grid, guiGridData, gridTexturesAndStyles, tileSize);
            DrawExitArrows(grid, guiGridData, gridTexturesAndStyles, tileSize);
        }


        static void DrawNode(LevelGraphLegendConfig legendConfig,
            LevelFactoryNode node, Vector2Int pos, bool selected,
            GUIGridData guiGridData, Dictionary<NodeIDData, GUIStyle> nodeStyleMap)
        {
            if (node == null)
                return;
            if (node.NodeID.LevelKeyID == null || node.NodeID.NodeTypeID == null)
                return;

            GetData(legendConfig, node.NodeID, nodeStyleMap, out var nodeTypeData,
                out var keyStyle, out var guiStyle);

            var nodeRect = NodeGUI.GetNodePos(pos, new Vector2(32, 32), guiGridData.Offset);
            NodeGUI.DrawNode(nodeRect, selected, "", keyStyle.Icon, guiStyle, nodeTypeData.IconSize,
                nodeTypeData.TextOffset);
        }

        static void DrawEntryArrow(LevelLayoutGrid grid, GUIGridData guiGridData,
            GridTexturesAndStyles gridTexturesAndStyles, int tileSize)
        {
            var gridHeight = grid.Size.y * tileSize;

            var entry = grid.Entry;
            var pos = new Vector2Int((tileSize + 2) * entry.GridPos.x, gridHeight - (tileSize + 2) * entry.GridPos.y);
            var entryPos = NodeGUI.GetNodePos(new Vector2(pos.x, pos.y), new Vector2(tileSize, tileSize),
                guiGridData.Offset);

            NodeGUI.DrawNode(entryPos, false, "", null, gridTexturesAndStyles.EntryStyle[(int) entry.Direction]);
        }

        static void DrawExitArrows(LevelLayoutGrid grid, GUIGridData guiGridData,
            GridTexturesAndStyles gridTexturesAndStyles, int tileSize)
        {
            if (grid.Exits == null)
                return;
            var gridHeight = grid.Size.y * tileSize;

            foreach (var exit in grid.Exits)
            {
                var pos = new Vector2Int((tileSize + 2) * exit.GridPos.x, gridHeight - (tileSize + 2) * exit.GridPos.y);
                var entryPos = NodeGUI.GetNodePos(new Vector2(pos.x, pos.y), new Vector2(tileSize, tileSize),
                    guiGridData.Offset);

                var exitDir = (int) exit.Direction;
                if (!exitDir.IsInRange(gridTexturesAndStyles.ExitStyle))
                    continue;
                
                NodeGUI.DrawNode(entryPos, false, "", null, gridTexturesAndStyles.ExitStyle[(int) exit.Direction]);
            }
        }

        #endregion
    }
}