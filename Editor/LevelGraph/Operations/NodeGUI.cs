using Core.Editor.Utility.GUIStyles;
using UnityEditor;
using UnityEngine;

using Core.Extensions;

namespace _Editor.Level
{
    // todo: merge NodeGUI from machinations project, put to Core stuff?
    public static class NodeGUI
    {
        public static void DrawGrid(Vector2 offset, float gridSpacing, float gridOpacity, Color gridColor)
        {
            var widthDivs = Mathf.CeilToInt(Screen.width / gridSpacing);
            var heightDivs = Mathf.CeilToInt(Screen.height / gridSpacing);

            Handles.BeginGUI();
            Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

            var newOffset = new Vector3(offset.x % gridSpacing, offset.y % gridSpacing, 0);

            for (var i = 0; i < widthDivs; i++)
                Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0) + newOffset,
                    new Vector3(gridSpacing * i, Screen.height, 0f) + newOffset);

            for (var j = 0; j < heightDivs; j++)
                Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0) + newOffset,
                    new Vector3(Screen.width, gridSpacing * j, 0f) + newOffset);

            Handles.color = Color.white;
            Handles.EndGUI();
        }

        public static Rect GetNodePos(Vector2 pos, Vector2 size, Vector2 offset)
        {
            var nodX = pos.x - size.x * 0.5f;
            var nodY = pos.y - size.y * 0.5f;
            return new Rect(new Vector2(nodX, nodY) + offset, size);
        }

        public static bool DrawNode(Rect nodeRect, bool isSelected, string title, Texture2D icon, GUIStyle style,
            Vector2? iconSize = null, Vector2? textOffset = null)
        {
            style ??= RoundNodes.EmptyNode;
            var newSelected = GUI.Toggle(nodeRect, isSelected, "", style);
            if (icon != null && nodeRect.size.x > 30.0f)
            {
                iconSize ??= nodeRect.size - 20.0f * Vector2.one;

                var sizeDiff = nodeRect.size - iconSize.Value;
                var imageRect = new Rect(nodeRect.position + 0.5f * sizeDiff, iconSize.Value);
                GUI.DrawTexture(imageRect, icon);
            }

            if (title.IsNullOrEmpty())
                return newSelected;

            var textRect = new Rect(nodeRect);
            var textStyle = EditorStyles.miniBoldLabel;

            textOffset ??= Vector2.zero;

            var textContent = new GUIContent(title);
            textRect.size = textStyle.CalcSize(textContent) + 2.0f * Vector2.one;
            textRect.x += (nodeRect.width - textRect.size.x) * 0.5f + textOffset.Value.x;
            textRect.y -= textRect.size.y + textOffset.Value.y;

            GUI.Label(textRect, title, textStyle);
            return newSelected;
        }

        public static Rect GetConnectionPointLPos(Rect nodeRect, Vector2 conPSize)
        {
            var pos = new Vector2(nodeRect.x - conPSize.x + 8, nodeRect.y + (nodeRect.height * 0.5f) - conPSize.y * 0.5f);
            return new Rect(pos, conPSize);
        }
        public static Rect GetConnectionPointDPos(Rect nodeRect, Vector2 conPSize)
        {
            var pos = new Vector2(nodeRect.x + (nodeRect.width * 0.5f) - conPSize.x * 0.5f, nodeRect.y + nodeRect.height - 8);
            return new Rect(pos, conPSize);
        }
        public static Rect GetConnectionPointTPos(Rect nodeRect, Vector2 conPSize)
        {
            var pos = new Vector2(nodeRect.x + (nodeRect.width * 0.5f) - conPSize.x * 0.5f, nodeRect.y - conPSize.y + 8);
            return new Rect(pos, conPSize);
        }
        public static Rect GetConnectionPointRPos(Rect nodeRect, Vector2 conPSize)
        {
            var pos = new Vector2(nodeRect.x + nodeRect.width - 8, nodeRect.y + (nodeRect.height * 0.5f) - conPSize.y * 0.5f);
            return new Rect(pos, conPSize);
        }
        public static void DrawConnectionPoint(Rect pos)
            => GUI.Box(pos, "", BuiltinSkins.DarkSkinButton);
    }
}