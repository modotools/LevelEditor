using System;
using Core.Editor.Interface;
using Core.Editor.Utility.GUIStyles;
using Core.Types;
using Core.Unity.Utility.GUITools;
using Level.Procedural;
using UnityEditor;
using UnityEngine;

namespace _Editor.Level.Procedural
{
    public static class GraphOperations
    {
        const float k_conPSize = 12f;
        static readonly Vector2 k_conPSizeVec = new Vector2(k_conPSize, k_conPSize);

        #region Grid
        public static void DrawGridGUI(ref GUIGridData gridData, IRepaintable repaintable, SimpleDelegate innerGUI)
        {
            gridData.Offset = EditorGUILayout.Vector2Field("Grid-Offset", gridData.Offset);
            var rect = EditorGUILayout.GetControlRect(GUILayout.Height(gridData.GridHeight));

            using (new GUI.GroupScope(rect))
            {
                DrawGrid(ref gridData);
                ProcessEvents(ref gridData, Event.current, repaintable);
                innerGUI.Invoke();
            }

            if (Event.current.type == EventType.Repaint)
                gridData.GridRect = GUILayoutUtility.GetLastRect();
        }
        
        public static void DrawGrid(ref GUIGridData guiGrid)
        {
            NodeGUI.DrawGrid(guiGrid.Offset, 20, 0.2f, Color.gray);
            NodeGUI.DrawGrid(guiGrid.Offset, 100, 0.4f, Color.gray);
        }
        
        public static void ProcessEvents(ref GUIGridData guiGrid, Event e, IRepaintable repaintable)
        {
            var splitterDrag = ProcessSplitterDragging(ref guiGrid, e, repaintable);
            if (splitterDrag)
                return;

            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (e.type)
            {
                case EventType.MouseDrag:
                    if (e.button == 0) GridDrag(ref guiGrid, e);
                    break;
            }
        }

        public static void GridDrag(ref GUIGridData guiGrid, Event e)
        {
            if (!guiGrid.GridRect.Contains(e.mousePosition + new Vector2(0, guiGrid.GridRect.position.y)))
                return;

            guiGrid.Offset += e.delta;
            GUI.changed = true;
        }

        #region Splitter
        public static void SplitterGUI(ref GUIGridData guiGrid)
        {
            var rect = EditorGUILayout.GetControlRect(GUILayout.Height(guiGrid.SplitterWidth));
            EditorGUIUtility.AddCursorRect(rect, MouseCursor.ResizeVertical);

            // Splitter
            using (new GUI.GroupScope(rect))
                GUI.Box(new Rect(0, 0, Screen.width, guiGrid.SplitterWidth), "", CustomStyles.Gray);

            if (Event.current.type == EventType.Repaint)
                guiGrid.SplitterRect = GUILayoutUtility.GetLastRect();
        }
        
        public static bool ProcessSplitterDragging(ref GUIGridData guiGrid, Event e, IRepaintable repaintable = null)
        {
            // Splitter events
            if (e == null)
                return false;

            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (e.rawType)
            {
                case EventType.MouseDown:
                    if (guiGrid.SplitterRect.Contains(e.mousePosition + new Vector2(0, guiGrid.GridRect.position.y)))
                    {
                        e.Use();
                        guiGrid.SplitterDragging = true;
                    }

                    break;
                case EventType.Repaint:
                case EventType.MouseDrag:
                    if (guiGrid.SplitterDragging)
                    {
                        var repaint = e.rawType == EventType.Repaint;
                        if (!repaint)
                            e.Use();
                        guiGrid.GridHeight = Mathf.Clamp(e.mousePosition.y - guiGrid.SplitterWidth * 0.5f, 20f,
                            Screen.height * 0.5f);
                        repaintable?.Repaint();
                    }

                    break;
                case EventType.MouseUp:
                    if (guiGrid.SplitterDragging)
                        guiGrid.SplitterDragging = false;
                    break;
            }

            return guiGrid.SplitterDragging;
        }
        #endregion
        #endregion
        
        #region Connection

        // void DrawNextConnection(NodeInfo fromNode, Vector2 a, Vector2 b)
        // {
        //     if (DrawConnection(a, b) != ButtonClickResult.Clicked)
        //         return;
        //
        //     Target.AddNodeAfter(fromNode);
        //     EditorUtility.SetDirty(Target);
        // }
        // void DrawBranchConnection(NodeInfo fromNode, Vector2 a, Vector2 b)
        // {
        //     if (DrawConnection(a, b) != ButtonClickResult.Clicked)
        //         return;
        //
        //     Target.AddBranchNodeAfter(fromNode);
        //     EditorUtility.SetDirty(Target);
        // }
        public static void DrawConnection(Vector2 a, Vector2 b) => DrawConnection(a, b, Color.black);
        public static void DrawConnection(Vector2 a, Vector2 b, Color color)
        {
            using (new ColorScope(color)) 
                Handles.DrawAAPolyLine(3, a, b);
        }
        #endregion
        #region ConnectionPoint

        public static Rect GetConnectionPointLPos(Rect nodeRect, NodeTypeData nodeTypeData)
        {
            var lPos = NodeGUI.GetConnectionPointLPos(nodeRect, k_conPSizeVec);
            lPos.x -= nodeTypeData.ConnectorOffsets.x;
            return lPos;
        }

        public static Rect GetConnectionPointTPos(Rect nodeRect, NodeTypeData nodeTypeData)
        {
            var tPos = NodeGUI.GetConnectionPointTPos(nodeRect, k_conPSizeVec);
            tPos.y -= nodeTypeData.ConnectorOffsets.y;
            return tPos;
        }

        public static Rect GetConnectionPointDPos(Rect nodeRect, NodeTypeData nodeTypeData)
        {
            var dPos = NodeGUI.GetConnectionPointDPos(nodeRect, k_conPSizeVec);
            dPos.y += nodeTypeData.ConnectorOffsets.w;
            return dPos;
        }

        public static Rect GetConnectionPointRPos(Rect nodeRect, NodeTypeData nodeTypeData)
        {
            var rPos= NodeGUI.GetConnectionPointRPos(nodeRect, k_conPSizeVec);
            rPos.x += nodeTypeData.ConnectorOffsets.z;
            return rPos;
        }

        public static void DrawConnectionPoint(Rect pos) => NodeGUI.DrawConnectionPoint(pos);

        #endregion
    }
}