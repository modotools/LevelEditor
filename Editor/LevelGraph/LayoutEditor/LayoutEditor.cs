using Core.Editor.Inspector;
using Core.Editor.Utility;
using Core.Types;
using Level.Procedural;
using UnityEditor;
using UnityEngine;
using static _Editor.Level.Procedural.GraphOperations;
using static _Editor.Level.Procedural.GridStyleOperations;
using static _Editor.Level.Procedural.LevelGraphNodeGrid_GUIOperations;
using static Level.Procedural.LevelLayoutGridOperations;

namespace _Editor.Level.Procedural
{
    /// <summary> LevelGraphEditor </summary>
    [CustomEditor(typeof(LayoutConfig))]
    public class LayoutConfigInspector : BaseInspector<LayoutConfigEditor>
    {
    }

    public class LayoutConfigEditor : BaseEditor<LayoutConfig>
    {
        const int k_tileSize = 48;
        Vector2Int m_selectedTile;
        
        Vector2Int m_shiftTiles;
        Vector2Int m_newTileSize;
        
        Vector2 m_scrollPos;

        GUIGridData m_guiGridData = GUIGridData.Default;
        GridTexturesAndStyles m_gridTexturesAndStyles;

        #region Unity Hooks
        
        
        public override void OnGUI(float width)
        {
            // base.OnGUI(width);
            
            Target.Editor_LegendConfig = EditorGUILayout.ObjectField(Target.Editor_LegendConfig, 
                typeof(LevelGraphLegendConfig), false) as LevelGraphLegendConfig;

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var defaultNodeRoom = SerializedObject.FindProperty(LayoutConfig.Editor_DefaultNodeRoomPropName);
                var defaultNodeConnector = SerializedObject.FindProperty(LayoutConfig.Editor_DefaultNodeConnectorPropName);
                EditorGUILayout.PropertyField(defaultNodeRoom, true);
                EditorGUILayout.PropertyField(defaultNodeConnector, true);

                if (check.changed)
                {
                    SerializedObject.ApplyModifiedProperties();
                    OnChanged();
                }
            }

            if (Target.Editor_LegendConfig == null)
                return;

            if (!AllTexturesSetup(ref m_gridTexturesAndStyles))
                return;
            
            ref var grid = ref Target.Editor_Grid;
            if (grid.Grid == null)
            {
                SetupGUI(ref grid);
                return;
            }
            MoveViaArrows(Target.Grid.Size, ref m_selectedTile, this);
            DrawGridGUI(ref m_guiGridData, this, DrawTiles);
            SplitterGUI(ref m_guiGridData);
            DataGUI();

            using var svs = new GUILayout.ScrollViewScope(m_scrollPos);
            m_scrollPos = svs.scrollPosition;
            
            CustomGUI.HSplitter(5);
            if (GUILayout.Button("Destroy Grid"))
                grid.Grid = null;

            using (new GUILayout.HorizontalScope())
            {
                if (GUILayout.Button("Rotate Left")) Rotate(ref grid, -1);
                if (GUILayout.Button("Rotate Right")) Rotate(ref grid, 1);

            }
            m_newTileSize = EditorGUILayout.Vector2IntField("New Tile-Size", m_newTileSize);
            m_shiftTiles = EditorGUILayout.Vector2IntField("Shift Tiles", m_shiftTiles);
            if (GUILayout.Button("ApplySizeAndShift"))
            {
                ApplySizePosition(ref grid, m_newTileSize, m_shiftTiles);
                Repaint();
            }
            EditorGUILayout.LabelField("Entry: ");
            using (new GUILayout.HorizontalScope())
            {
                grid.Entry.GridPos = EditorGUILayout.Vector2IntField("", grid.Entry.GridPos);
                grid.Entry.Direction = (Cardinals) EditorGUILayout.EnumPopup(grid.Entry.Direction);
            }
            
            ExitsGUI();
            EndGUI();
        }
        
        void ExitsGUI()
        {
            using var check = new EditorGUI.ChangeCheckScope();
            var gridProp = SerializedObject.FindProperty(LayoutConfig.Editor_GridPropName);
            var exits = gridProp.FindPropertyRelative(nameof(LevelLayoutGrid.Exits));
            EditorGUILayout.PropertyField(exits, true);
            if (check.changed)
            {
                SerializedObject.ApplyModifiedProperties();
                OnChanged();
            }
        }

        void DataGUI() => SelectedTileGUI(Target.Grid, m_selectedTile, m_gridTexturesAndStyles);
        void DrawTiles() => LevelGraphNodeGrid_GUIOperations.DrawTiles(Target.Editor_LegendConfig, Target.Grid, k_tileSize, m_guiGridData,
            ref m_selectedTile, m_gridTexturesAndStyles);
        #endregion
    }
}