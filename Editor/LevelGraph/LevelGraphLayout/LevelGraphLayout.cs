using System.Collections.Generic;
using System.Linq;
using Core.Editor.Interface;
using Core.Editor.Utility;
using Core.Extensions;
using Core.Types;
using Level.Procedural;
using UnityEditor;
using UnityEngine;

using static _Editor.Level.Procedural.GraphOperations;
using static _Editor.Level.Procedural.LevelGraphNodeGrid_GUIOperations;
using static Level.Procedural.LevelLayoutGridOperations;
using static _Editor.Level.Procedural.GridStyleOperations;

namespace _Editor.Level.Procedural
{
    public class LevelGraphLayout_Window : EditorWindow, IRepaintable
    {
        const int k_tileSize = 48;

        public LevelGraphConfig LevelGraph;
        LevelLayoutGrid m_grid;

        GUIGridData m_guiGridData = GUIGridData.Default;

        Vector2 m_scrollPos;
        Vector2Int m_selectedTile;

        GridTexturesAndStyles m_gridTexturesAndStyles;

        int m_selectedDepth;
        LevelGraphNode m_selectedGrid;
        bool m_textureNotFound;

        LevelGraphNodeGrids[] m_depthData;

        #region Unity Hooks

        [MenuItem("Tools/Test LevelGraphLayout")]
        public static void OpenWindow()
        {
            var roomEditor = GetWindow(typeof(LevelGraphLayout_Window), false, "LevelGraphLayout", true);
            roomEditor.Show();
        }

        void OnGUI()
        {
            using (new EditorGUI.DisabledScope(m_grid.Grid != null))
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                LevelGraph = EditorGUILayout.ObjectField("LevelGraph", LevelGraph, typeof(LevelGraphConfig),
                    false) as LevelGraphConfig;

                if (check.changed && LevelGraph != null)
                    SetupDepthGrids();
            }

            DepthSelectionGUI();

            if (LevelGraph == null)
                return;
            
            if (!AllTexturesSetup(ref m_gridTexturesAndStyles))
                return;
            
            if (m_grid.Grid == null)
                SetupGUI(ref m_grid);

            if (m_grid.Grid != null)
            {
                DrawGridGUI(ref m_guiGridData, this, DrawTiles);
                if (GUILayout.Button("Destroy Grid"))
                    DeselectGrid();
            }

            SplitterGUI(ref m_guiGridData);
            using (var svs = new GUILayout.ScrollViewScope(m_scrollPos))
            {
                m_scrollPos = svs.scrollPosition;
                DataGUI();
            }
        }

        #endregion

        #region Grid Setup

        void SetupSelectedGrid(LevelGraphNodeGrids depthData)
        {
            var grid = depthData.Grids[m_selectedGrid];
            SelectGrid(grid);
        }

        void SetupDepthGrids()
        {
            using var depthDataScoped = SimplePool<List<LevelGraphNodeGrids>>.I.GetScoped();
            var depthData = depthDataScoped.Obj;

            var entryNode = LevelGraph.EntryLevelGraphNode;
            AddDepthData(entryNode, depthData);
            m_depthData = depthData.ToArray();

            for (var depth = m_depthData.Length - 1; depth >= 0; --depth)
            {
                var nodes = m_depthData[depth].Grids.Keys.ToArray();
                foreach (var node in nodes) 
                    InitGrid(node, depth);
            }
        }

        static void AddDepthData(LevelGraphNode levelGraphNode, IList<LevelGraphNodeGrids> depthData, int depth = 0)
        {
            var depthDataExists = depth < depthData.Count;
            var data = depthDataExists
                ? depthData[depth]
                : new LevelGraphNodeGrids
                {
                    Grids = new Dictionary<LevelGraphNode, LevelLayoutGrid>()
                };

            // Debug.Log($"{levelGraphNode.Name} | Depth Data exists {depth} {depthDataExists} {depthData.Count}");
            data.Grids.Add(levelGraphNode, new LevelLayoutGrid {LevelFactoryNode = levelGraphNode.FactoryNode});

            if (!depthDataExists)
                depthData.Add(data);

            if (levelGraphNode.ConnectedTo == null) 
                return;
            foreach (var n in levelGraphNode.ConnectedTo)
                AddDepthData(n, depthData, depth + 1);
        }

        void InitGrid(LevelGraphNode node, int currentDepth)
        {
            var grid = m_depthData[currentDepth].Grids[node];

            grid.LevelFactoryNode = node.FactoryNode;
            var props = grid.WorkerProperties;

            grid.Size = Vector2Int.one;
            grid.Grid = new LevelNodeGridData[grid.Size.x * grid.Size.y];
            grid.Grid[0] = LevelNodeGridData.Default;
            grid.Grid[0].RoomLevelGraphNode = node.FactoryNode;
            grid.Entry = new LevelLayoutGridAccess() {Direction = Cardinals.Invalid, GridPos = Vector2Int.zero};
            grid.Exits = new[] {new LevelLayoutGridAccess() {Direction = Cardinals.Invalid, GridPos = Vector2Int.zero}};
            // node.MinSize

            if (node.ConnectedTo != null)
            {
                // Debug.Log($"node at depth {currentDepth} has connections {node.ConnectedTo.Length}");
                foreach (var connection in node.ConnectedTo)
                {
                    var connectGrid = m_depthData[currentDepth + 1].Grids[connection];
                    grid = ConnectGrids(grid, connectGrid);
                }
            }

            m_depthData[currentDepth].Grids[node] = grid;
        }

        static LevelLayoutGrid ConnectGrids(LevelLayoutGrid grid, LevelLayoutGrid connectGrid)
        {
            var xAccess = connectGrid.Entry.GridPos.x == 0
                          || connectGrid.Entry.GridPos.x == connectGrid.Size.x - 1;
            var yAccess = connectGrid.Entry.GridPos.y == 0
                          || connectGrid.Entry.GridPos.y == connectGrid.Size.y - 1;

            var extendWidth = connectGrid.Size.x <= connectGrid.Size.y && xAccess;
            var extendHeight = !extendWidth && yAccess;

            Debug.Assert(extendWidth || extendHeight);

            var width = extendWidth
                ? grid.Size.x + connectGrid.Size.x
                : Mathf.Max(grid.Size.x, connectGrid.Size.x);
            var height = extendHeight
                ? grid.Size.y + connectGrid.Size.y
                : Mathf.Max(grid.Size.y, connectGrid.Size.y);
            var newSize = new Vector2Int(width, height);

            // Debug.Log($"ConnectGrids new size : {newSize}");
            var offset = new Vector2Int(extendWidth ? grid.Size.x : 0, extendHeight ? grid.Size.y : 0);
            var connectedGrid = new LevelLayoutGrid
            {
                Grid = new LevelNodeGridData[newSize.x * newSize.y],
                Size = newSize
            };

            CopyGridToModifiedGrid(ref connectGrid, ref connectedGrid, newSize, offset);
            CopyGridToModifiedGrid(ref grid, ref connectedGrid, newSize, Vector2Int.zero);

            AppendExits(connectGrid, ref connectedGrid, offset);

            var entryPos = connectedGrid.Entry.GridPos;
            ref var entryTile = ref connectedGrid.Get(entryPos);
            
            if (extendWidth)
            {
                entryTile.LayoutData.R = ConnectionType.Path;
                CopyRightPassageToNextTileLeft(ref connectedGrid, entryPos);
            }
            else // if (extendHeight)
            {
                entryTile.LayoutData.T = ConnectionType.Path;
                CopyTopPassageToNextTileBottom(ref connectedGrid, entryPos);
            }
            
            connectedGrid.Entry = new LevelLayoutGridAccess()
            {
                Direction = Cardinals.West,
                GridPos = entryPos
            };
            return connectedGrid;
            // connectGrid.LevelFactoryNode.
        }

        #endregion

        #region Inspector-GUI

        void DepthSelectionGUI()
        {
            if (m_depthData == null)
                return;
            using var check = new EditorGUI.ChangeCheckScope();

            EditorGUILayout.LabelField("Depth: ");
            using (new GUILayout.HorizontalScope())
            {
                for (var i = 0; i < m_depthData.Length; i++)
                {
                    if (CustomGUI.ActivityButton(m_selectedDepth == i, $"{i}"))
                        m_selectedDepth = i;
                }
            }

            if (!m_selectedDepth.IsInRange(m_depthData))
                return;
            var depthData = m_depthData[m_selectedDepth];
            EditorGUILayout.LabelField("Grid: ");
            using (new GUILayout.HorizontalScope())
            {
                foreach (var grid in depthData.Grids)
                {
                    if (CustomGUI.ActivityButton(m_selectedGrid == grid.Key, $"{grid.Key.Name}"))
                        m_selectedGrid = grid.Key;
                }
            }

            if (!check.changed)
                return;
            if (!depthData.Grids.ContainsKey(m_selectedGrid))
                return;
            SetupSelectedGrid(depthData);
        }

        void DataGUI() => SelectedTileGUI(m_grid, m_selectedTile, m_gridTexturesAndStyles);

        #endregion

        #region Grid GUI

        void DrawTiles()
        {
            // Debug.Log("Draw Tiles");
            LevelGraphNodeGrid_GUIOperations.DrawTiles(LevelGraph.LegendConfig, m_grid, k_tileSize,
                m_guiGridData,
                ref m_selectedTile, m_gridTexturesAndStyles);
        }

        void SelectGrid(LevelLayoutGrid grid)
        {
            // Debug.Log($"m_selectedDepth {m_selectedDepth} grid: {m_selectedGrid.Name} size {grid.Size}");
            m_grid = grid;
        }

        void DeselectGrid() => m_grid = default;

        #endregion
    }
}