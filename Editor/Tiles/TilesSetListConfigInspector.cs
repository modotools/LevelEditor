﻿using Core.Editor.Inspector;
using Core.Editor.Utility;
using Level.Tiles;
using UnityEditor;
using UnityEngine;

namespace Level.Editor.Tiles
{
    [CustomEditor(typeof(TilesSetListConfig))]
    public class TilesSetListConfigInspector : BaseInspector<TilesSetListConfigEditor> { }
    
    public class TilesSetListConfigEditor : BaseEditor<TilesSetListConfig>
    {
        public override void OnGUI(float width)
        {
            base.OnGUI(width);
            GUILayout.Label($"Digits {Target.Editor_Digits}");

            if (Result.Errors <= 0 && Result.Warnings <= 0) 
                return;

            EditorGUILayout.HelpBox("Be aware that by changing the TileSetList-Config and updating mask and shift values" +
                                    "previous rooms/ levels will be broken", MessageType.Warning);
            if (CustomGUI.SafetyButton("Update Mask and Shift", "SURE?", this)) 
                Target.UpdateMaskAndShift();
        }
    }
}