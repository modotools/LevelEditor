﻿using System;
using UnityEditor;
using UnityEngine;

using Core.Editor.Utility;
using Core.Editor.Utility.GUITools;
using Core.Unity.Extensions;
using Level.PlatformLayer.Interface;
using Level.Tiles;
using Level.Tiles.Interface;
using static Level.PlatformLayer.Operations;

namespace Level.Editor.Tiles
{
    public static class TileDebugDraw
    {
        [Serializable]
        public struct DebugDrawSettings
        {
            public bool DrawLabel;
            public bool DrawDots;
            public bool DrawDebugTiles;
            public float DebugTileSize;
            public float LineAlpha;
            public float ColorAlpha;

            [NonSerialized]
            public bool Is2D;

            public static DebugDrawSettings Default = new DebugDrawSettings()
            {
                DrawLabel = false,
                DrawDots = false,
                DrawDebugTiles = true,
                DebugTileSize = 0.45f,
                ColorAlpha = 0.5f,
                LineAlpha = 1.0f,

                Is2D = false,
            };
        }

        static readonly LayoutOverrideGrid k_overrideGrid = new LayoutOverrideGrid();

        public static void DrawGeometryType(IPlatformLayer layer,
            TilesSetData tilesSetData, DebugDrawSettings settings)
        {
            layer.Get(out IGridData grid);
            if (tilesSetData.TileConfig.Result is IDimensionLayoutOverride layoutOverride)
            {
                k_overrideGrid.Layout = layoutOverride.Layout;
                k_overrideGrid.CurrentGrid = grid;
                
                grid = k_overrideGrid;
            }
            
            var posOffset = layer.PositionOffset + grid.Offset.Vector3();
            var gridSize = grid.TileSize;

            for (var x = 0; x < layer.TileDim.x; ++x)
            for (var z = 0; z < layer.TileDim.y; ++z)
            {
                var pos = new Vector3(posOffset.x + layer.Position.x, posOffset.y, posOffset.z + layer.Position.y);
                var rx = gridSize * x + pos.x;
                var rz = gridSize * z + pos.z;

                var tile = tilesSetData.GetTileIdx(GetTile(layer, layer, new Vector2Int(x, z)));

                if (tile >= tilesSetData.Count)
                    return;

                DrawTile(tilesSetData, settings, rx, pos, rz, tile);
            }
        }

        static void DrawTile(TilesSetData tilesSetData, DebugDrawSettings settings, float rx, Vector3 pos, float rz, ushort tile)
        {
            var center = settings.Is2D ? new Vector3(rx, rz, pos.y) : new Vector3(rx, pos.y, rz);

            if (tilesSetData.Count <= tile || tilesSetData[tile] == null)
                return;

            GetDebugDrawVertices(settings, center, out var v1, out var v2, out var v3, out var v4);
            GetDebugDrawColor(settings, tilesSetData, tile, out var faceCol, out var outlineCol);

            var previewTex = tilesSetData[tile].PreviewTex;
            if (settings.DrawDebugTiles)
                Handles.DrawSolidRectangleWithOutline(new[] {v1, v2, v3, v4}, faceCol, outlineCol);

            var label = settings.DrawLabel ? tilesSetData[tile].TileName : "";
            var image = settings.DrawDots ? previewTex : null;

            if (settings.DrawDots && image != null)
            {
                var w = 0.5f* image.width;
                var scrPos = CustomHandles.CalculateScreenPosition(center, out var z);
                var wpos = CustomHandles.ScreenToWorldPosition(scrPos + new Vector2(-w,w), z);
                Handles.Label(wpos, new GUIContent() {image = image});
            }   
            if (settings.DrawLabel || settings.DrawDebugTiles)
                CustomHandles.DrawText(center, label, tilesSetData[tile].TileColor);
        }

        static void GetDebugDrawColor(DebugDrawSettings settings, TilesSetData tilesSetData, ushort tile, out Color faceCol, out Color outlineCol)
        {
            faceCol = tilesSetData[tile].TileColor;
            faceCol.a *= settings.ColorAlpha;
            outlineCol = tilesSetData[tile].TileColor;
            outlineCol.a *= settings.LineAlpha;
        }

        static void GetDebugDrawVertices(DebugDrawSettings settings, Vector3 center, out Vector3 v1, out Vector3 v2, out Vector3 v3,
            out Vector3 v4)
        {
            var debugTileSize = settings.DebugTileSize;
            var yMulti = settings.Is2D ? 1f : 0f;
            var zMulti = settings.Is2D ? 0f : 1f;
            v1 = center + new Vector3(-debugTileSize, -debugTileSize * yMulti, -debugTileSize * zMulti);
            v2 = center + new Vector3(debugTileSize, -debugTileSize * yMulti, -debugTileSize * zMulti);
            v3 = center + new Vector3(debugTileSize, debugTileSize * yMulti, debugTileSize * zMulti);
            v4 = center + new Vector3(-debugTileSize, debugTileSize * yMulti, debugTileSize * zMulti);
        }

        internal static void DrawPlatformBounds(IPlatformLayerSpatialData platform,
            Vector3 posOffset, IGridData gridData, Color drawCol)
        {
            var bounds = GetBounds(platform, posOffset, gridData);
            CustomHandleDraw.DrawBounds(bounds, drawCol);
        }
    }
}
