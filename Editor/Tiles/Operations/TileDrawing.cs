using System;
using System.Collections.Generic;
using Core.Editor.Interface;

using Core.Editor.Utility;
using Core.Events;
using Core.Extensions;
using Core.Unity.Extensions;
using Level.Editor.Room;
using Level.Enums;
using Level.PlatformLayer;
using Level.Tiles;
using Level.Generation.Operations;
using Level.PlatformLayer.Interface;
using Level.Tiles.Interface;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

using static Level.PlatformLayer.Operations;

namespace Level.Editor.Tiles
{
    public static class TileDrawing
    {
        public struct TilesDrawn
        {
            public List<Vector2Int> Positions;
        }

        static readonly List<Vector2Int> k_tempPositions = new List<Vector2Int>();
        static readonly List<Vector2Int> k_positions = new List<Vector2Int>();

        static readonly LayoutOverrideGrid k_overrideGrid = new LayoutOverrideGrid();
        
        #region Editor Prefs
        const string k_editorPrefShowLevelSettingEditor = "RoomEditor.ShowLevelSettingEditor";
        #endregion

        //todo: depending on GridSize and Setting?
        const float k_minBrushSize = 0.1f;
        const float k_maxBrushSize = 10f;

        internal static void Init_TilesMode(ref TileDrawingOpData opDat, IRepaintable r)
        {
            opDat.PaintTileTypeLeft = 0;
            opDat.PaintTileTypeRight = 0;
            opDat.BrushSize = 1;
            opDat.ShowLevelSettingEditor = new AnimBool(false);

            opDat.ShowLevelSettingEditor.valueChanged.RemoveAllListeners();
            opDat.ShowLevelSettingEditor.valueChanged.AddListener(r.Repaint);

            opDat.ShowLevelSettingEditor.target = EditorPrefs.GetBool(k_editorPrefShowLevelSettingEditor, true);
        }

        static void DrawRect(ref TileDrawingOpData opDat, Vector3 currentPos)
        {
            if (!opDat.StartPos.HasValue)
                return;
            var startPosV = opDat.StartPos.Value.CursorPos;

            if (opDat.Is2DMode)
            {
                Handles.DrawLine(currentPos, new Vector3(currentPos.x, startPosV.y, currentPos.z));
                Handles.DrawLine(currentPos, new Vector3(startPosV.x, currentPos.y, currentPos.z));
                Handles.DrawLine(startPosV, new Vector3(currentPos.x, startPosV.y, currentPos.z));
                Handles.DrawLine(startPosV, new Vector3(startPosV.x, currentPos.y, currentPos.z));
            }
            else
            {
                Handles.DrawLine(currentPos, new Vector3(currentPos.x, currentPos.y, startPosV.z));
                Handles.DrawLine(currentPos, new Vector3(startPosV.x, currentPos.y, currentPos.z));
                Handles.DrawLine(startPosV, new Vector3(currentPos.x, currentPos.y, startPosV.z));
                Handles.DrawLine(startPosV, new Vector3(startPosV.x, currentPos.y, currentPos.z));
            }
        }

        const float k_sqrt2 = 1.4142135623730950488016887242097f;
        static void DrawCursor(ref TileDrawingOpData opDat, Vector3 currentPos)
        {
            var brush = opDat.Brush;
            var brushSize = opDat.BrushSize;

            var currentGridPosVec3 = GrisPosV3(opDat, currentPos);

            // todo: Fix later
            //if (opDat.IsVariationOnly && opDat.Brush == BrushType.PointBrush)
            //{
            //    brush = BrushType.RectangleBrush;
            //    brushSize = 0.5f;
            //    currentPos = new Vector3(Mathf.CeilToInt(currentPos.x) - 0.5f, 0.0f, Mathf.CeilToInt(currentPos.z) - 0.5f);
            //}

            var prevCol = Handles.color;
            Handles.color = Color.green;

            var rot = opDat.Is2DMode ? Quaternion.identity : Quaternion.Euler(90, 0, 0);
            switch (brush)
            {
                case BrushType.Fill:
                case BrushType.PointBrush:
                    DrawCursorPointBrush(opDat, currentGridPosVec3);
                    break;
                case BrushType.RectangleBrush:
                    Handles.RectangleHandleCap(-1, currentPos, rot, brushSize, EventType.Repaint);
                    break;
                case BrushType.CircleBrush:
                    Handles.CircleHandleCap(-1, currentPos, rot, brushSize, EventType.Repaint);
                    break;
                case BrushType.DragLine:
                    DrawCursorDragLine(opDat, currentPos, rot, brushSize);
                    break;
                case BrushType.DragRectangle:
                    DrawRect(ref opDat, currentPos);
                    break;
                case BrushType.DragEllipseRect:
                case BrushType.DragEllipseDiagonal:
                    DrawCursorDragEllipseDiagonal(ref opDat, currentPos);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            Handles.color = prevCol;
        }

        static void DrawCursorDragEllipseDiagonal(ref TileDrawingOpData opDat, Vector3 currentPos)
        {
            if (!opDat.StartPos.HasValue)
                return;

            var startPosV = opDat.StartPos.Value.CursorPos;
            var halfSize = 0.5f * (currentPos - startPosV);
            var centerPos = startPosV + halfSize;
            var v2 = opDat.Brush == BrushType.DragEllipseRect;

            if (v2)
                DrawRect(ref opDat, currentPos);
            else
            {
                Handles.DrawLine(startPosV, startPosV + halfSize.normalized);
                Handles.DrawLine(currentPos, currentPos - halfSize.normalized);
            }

            var multi = v2 ? 1.0f : k_sqrt2;
            float x = 0, y = 0, z = 0;
            for (var i = 0; i < 21; ++i)
            {
                var nextX = Mathf.Cos(Mathf.Deg2Rad * i * 18) * multi * halfSize.x + centerPos.x;
                var nextY = opDat.Is2DMode ? Mathf.Sin(Mathf.Deg2Rad * i * 18) * multi * halfSize.y + centerPos.y : 0;
                var nextZ = opDat.Is2DMode ? 0 : Mathf.Sin(Mathf.Deg2Rad * i * 18) * multi * halfSize.z + centerPos.z;
                var from = new Vector3(x, y, z);
                var to = new Vector3(nextX, nextY, nextZ);
                if (i != 0)
                    Handles.DrawLine(from, to);

                x = nextX;
                y = nextY;
                z = nextZ;
            }
        }

        static void DrawCursorDragLine(TileDrawingOpData opDat, Vector3 currentPos, Quaternion rot, float brushSize)
        {
            Handles.CircleHandleCap(-1, currentPos, rot, brushSize, EventType.Repaint);
            if (!opDat.StartPos.HasValue)
                return;

            var startPosV = opDat.StartPos.Value.CursorPos;
            Handles.DrawLine(startPosV, currentPos);
            Handles.CircleHandleCap(-1, startPosV, rot, brushSize, EventType.Repaint);
        }

        static void DrawCursorPointBrush(TileDrawingOpData opDat, Vector3 currentGridPosVec3)
        {
            var fwd = opDat.Is2DMode ? Vector3.up : Vector3.forward;

            var gridRealWPos = currentGridPosVec3;
            Handles.DrawLine(gridRealWPos + fwd * 0.5f, gridRealWPos - fwd * 0.5f);
            Handles.DrawLine(gridRealWPos + Vector3.right * 0.5f, gridRealWPos - Vector3.right * 0.5f);
            if (opDat.Brush != BrushType.Fill)
                return;
            Handles.DrawLine(gridRealWPos + fwd * 0.5f + Vector3.right * 0.5f,
                gridRealWPos - fwd * 0.5f - Vector3.right * 0.5f);
            Handles.DrawLine(gridRealWPos - fwd * 0.5f + Vector3.right * 0.5f,
                gridRealWPos + fwd * 0.5f - Vector3.right * 0.5f);
        }

        internal static void SceneGUI_TilesMode(Event e, ref TileDrawingOpData opDat, Vector3 pos, Vector3 cursor)
        {
            DrawCursor(ref opDat, cursor);

            if (e.control && e.type == EventType.ScrollWheel)
            {
                opDat.BrushSize = Mathf.Clamp(opDat.BrushSize - 0.1f * e.delta.y, 0.5f, 10.0f);
                e.Use();

                //Debug.Log("Repainting");
                SceneView.RepaintAll();
            }

            var useEvent = e.type.IsAny(EventType.MouseDown, EventType.MouseDrag, EventType.MouseUp);
            if (!useEvent)
                return;

            ProcessMouseEvent(e, ref opDat, pos, cursor, out var doSetTilesNow);
            opDat.FireEvent = e.type == EventType.MouseUp;

            // only use mouse event when we hit platform, so we can use other features,
            // like double-click to maximize scene window from peek
            if (MouseOnPlatform(e, opDat, pos) || e.type == EventType.MouseDrag)
                e.Use();

            if (!doSetTilesNow)
                return;

            ApplyBrush(ref opDat, pos);
        }

        static bool MouseOnPlatform(Event e, TileDrawingOpData opDat, Vector3 pos)
        {
            var gridPos = GrisPosV2(opDat, pos);
            var lvlX = gridPos.x - opDat.Platform.Position.x;
            var lvlZ = gridPos.y - opDat.Platform.Position.y;
            var lvlPos = new Vector2Int(lvlX, lvlZ);
            return opDat.Platform.IsInside(lvlPos) || e.type == EventType.MouseDrag;
        }

        static void ApplyBrush(ref TileDrawingOpData opDat, Vector3 pos)
        {
            var e = Event.current;
            // todo: Fix Later
            //if (opDat.IsVariationOnly)
            //{
            //    pos -= Vector3.one * 0.5f;
            //    opDat.StartPos -= Vector3.one * 0.5f;
            //}
            //TODO ROOM: Fix this
            if (opDat.Tiles is PlatformLayerConfig c)
                Undo.RecordObject(c, "Paint");

            // ReSharper disable once ConvertIfStatementToSwitchStatement
            if (e.button == 0)
                ApplyBrush(ref opDat, pos, true);
            else if (e.button == 1)
                ApplyBrush(ref opDat, pos, false);

            if (opDat.FireEvent) 
                FireEvent();

            opDat.StartPos = null;
        }

        static void FireEvent()
        {
            k_tempPositions.Clear();
            k_tempPositions.AddRange(k_positions);
            EventMessenger.TriggerEvent(new TilesDrawn {Positions = k_tempPositions});
            k_positions.Clear();
        }

        static void ProcessMouseEvent(Event e, ref TileDrawingOpData opDat, Vector3 pos, Vector3 cursor, out bool doSetTilesNow)
        {
            doSetTilesNow = false;
            if (opDat.IsBrushType)
            {
                doSetTilesNow = true;
                return;
            }

            var setStartPos = e.type == EventType.MouseDown
                              || (opDat.Brush == BrushType.Fill && opDat.StartPos.HasValue);

            if (setStartPos)
                opDat.StartPos = new TileDrawingPos{TilePos = pos, CursorPos = cursor};

            if (e.type == EventType.MouseUp && opDat.StartPos.HasValue)
                doSetTilesNow = true;
        }

        #region Offset Grid 2DMode Utility
        static Vector3 Offset(TileDrawingOpData opData) => opData.Is2DMode
            ? new Vector3(opData.GridData.Offset.x, opData.GridData.Offset.y, 0)
            : new Vector3(opData.GridData.Offset.x, 0, opData.GridData.Offset.y);
        static Vector3 GrisPosV3(TileDrawingOpData opData, Vector3 pos)
        {
            var off = Offset(opData);
            return new Vector3(Mathf.RoundToInt(pos.x - off.x),
                       Mathf.RoundToInt(pos.y - off.y),
                       Mathf.RoundToInt(pos.z - off.z))
                   +off;
        }

        static Vector2Int GrisPosV2(TileDrawingOpData opData, Vector3 pos) => GrisPosV2(opData, pos.x, pos.z);
        static Vector2Int GrisPosV2(TileDrawingOpData opData, float x, float z) =>
            new Vector2Int(Mathf.RoundToInt(x - opData.GridData.Offset.x),
                Mathf.RoundToInt(z - opData.GridData.Offset.y));
        #endregion

        static void CircleAndRectangleBrush(ref TileDrawingOpData opDat, Vector3 pos, uint tileType)
        {
            var brushMin = Mathf.FloorToInt(-opDat.BrushSize);
            var brushMax = Mathf.CeilToInt(opDat.BrushSize);

            for (var x = brushMin; x <= brushMax; ++x)
            for (var z = brushMin; z <= brushMax; ++z)
            {
                var gridPos = GrisPosV2(opDat, pos.x + x, pos.z + z);
                if (gridPos.x < pos.x - opDat.BrushSize) continue;
                if (gridPos.y < pos.z - opDat.BrushSize) continue;
                if (gridPos.x > pos.x + opDat.BrushSize) continue;
                if (gridPos.y > pos.z + opDat.BrushSize) continue;

                if (opDat.Brush == BrushType.CircleBrush)
                {
                    var rX = gridPos.x - pos.x;
                    var rZ = gridPos.y - pos.z;
                    if (rX * rX + rZ * rZ > opDat.BrushSize * opDat.BrushSize) continue;
                }

                ModifyTileAtPos(ref opDat, new Vector2Int(gridPos.x, gridPos.y), tileType);
            }
        }

        static void ApplyBrush(ref TileDrawingOpData opDat, Vector3 currentPos, bool left)
        {
            var tileType = left 
                ? opDat.PaintTileTypeLeft 
                : opDat.PaintTileTypeRight;

            var currentGridPos = GrisPosV2(opDat, currentPos);
            var gridOff = opDat.GridData.Offset.Vector3();
            switch (opDat.Brush)
            {
                case BrushType.PointBrush:
                    ModifyTileAtPos(ref opDat, currentGridPos, tileType);
                    break;
                case BrushType.CircleBrush:
                case BrushType.RectangleBrush:
                    CircleAndRectangleBrush(ref opDat, currentPos - gridOff, tileType);
                    break;
                case BrushType.DragLine:
                    ApplyCircleAndRectangleBrush(ref opDat, currentPos - gridOff, tileType);
                    break;
                case BrushType.DragRectangle:
                    ApplyDragRectangleBrush(ref opDat, currentPos - gridOff, tileType);
                    break;
                case BrushType.DragEllipseRect:
                case BrushType.DragEllipseDiagonal:
                    ApplyDragEllipseDiagonalBrush(ref opDat, currentPos - gridOff, tileType);
                    break;
                case BrushType.Fill:
                    ApplyFillBrush(ref opDat, currentGridPos, tileType);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        static void ApplyFillBrush(ref TileDrawingOpData opDat, Vector2Int currentGridPos, uint tileType)
        {
            var platform = opDat.Platform;
            var startPos = new Vector2Int(currentGridPos.x - platform.Position.x,
                currentGridPos.y - platform.Position.y);
            var tiles = GetRegionTiles(platform, opDat.Tiles, startPos, compareMask: opDat.TileTypesMask);

            foreach (var tile in tiles)
            {
                //TODO ROOM:  Fix this
                ModifyTileAtPos(ref opDat, tile + platform.Position, tileType);
            }
        }

        // ReSharper disable once TooManyDeclarations
        static void ApplyDragEllipseDiagonalBrush(ref TileDrawingOpData opDat, Vector3 currentPos, uint tileType)
        {
            if (!opDat.StartPos.HasValue)
                return;

            var startPosV = opDat.StartPos.Value.TilePos;
            var halfSize = 0.5f * (currentPos - startPosV);
            var centerPos = startPosV + halfSize;

            var v2 = (opDat.Brush == BrushType.DragEllipseRect);

            var width = v2 ? Mathf.Abs(halfSize.x) : Mathf.Abs(Mathf.Cos(Mathf.Deg2Rad * 0) * k_sqrt2 * halfSize.x);
            var height = v2 ? Mathf.Abs(halfSize.z) : Mathf.Abs(Mathf.Sin(Mathf.Deg2Rad * 90) * k_sqrt2 * halfSize.z);

            var xMin = Mathf.FloorToInt(-width);
            var xMax = Mathf.CeilToInt(width);
            var zMin = Mathf.FloorToInt(-height);
            var zMax = Mathf.CeilToInt(height);

            for (var x = xMin; x <= xMax; ++x)
            for (var z = zMin; z <= zMax; ++z)
            {
                var currentGridPos = GrisPosV2(opDat, centerPos.x + x, centerPos.z + z);
                var eX = (currentGridPos.x - centerPos.x) / width;
                var eZ = (currentGridPos.y - centerPos.z) / height;
                if (eX * eX + eZ * eZ > 1)
                    continue;
                ModifyTileAtPos(ref opDat, new Vector2Int(currentGridPos.x, currentGridPos.y), tileType);
            }
        }

        static void ApplyDragRectangleBrush(ref TileDrawingOpData opDat, Vector3 currentPos, uint tileType)
        {
            if (!opDat.StartPos.HasValue)
                return;
            var startPosV = opDat.StartPos.Value.TilePos;
            var xMin = Mathf.Min(startPosV.x, currentPos.x);
            var zMin = Mathf.Min(startPosV.z, currentPos.z);
            var xMax = Mathf.Max(startPosV.x, currentPos.x);
            var zMax = Mathf.Max(startPosV.z, currentPos.z);
            var xStart = Mathf.FloorToInt(xMin);
            var xEnd = Mathf.CeilToInt(xMax);
            var zStart = Mathf.FloorToInt(zMin);
            var zEnd = Mathf.CeilToInt(zMax);

            for (var x = xStart; x <= xEnd; ++x)
            for (var z = zStart; z <= zEnd; ++z)
            {
                var currentGridPos = new Vector2Int(x, z);
                if (currentGridPos.x < xMin) continue;
                if (currentGridPos.y < zMin) continue;
                if (currentGridPos.x > xMax) continue;
                if (currentGridPos.y > zMax) continue;

                ModifyTileAtPos(ref opDat, new Vector2Int(currentGridPos.x, currentGridPos.y), tileType);
            }
        }

        static void ApplyCircleAndRectangleBrush(ref TileDrawingOpData opDat, Vector3 currentPos, uint tileType)
        {
            if (!opDat.StartPos.HasValue)
                return;

            var startPosV = opDat.StartPos.Value.TilePos;
            var lineVec = (currentPos - startPosV);
            var lineDir = lineVec.normalized;
            var magnitude = Mathf.Abs(lineVec.magnitude);
            var length = Mathf.CeilToInt(magnitude);
            for (var i = 0; i <= length; ++i)
            {
                var step = Mathf.Min(i, magnitude);
                var addPos = step * lineDir;
                CircleAndRectangleBrush(ref opDat, startPosV + addPos, tileType);
            }
        }

        static void ModifyTileAtPos(ref TileDrawingOpData opDat, Vector2Int pos, uint tileType)
        {
            var lvlX = pos.x - opDat.Platform.Position.x;
            var lvlZ = pos.y - opDat.Platform.Position.y;
            var lvlPos = new Vector2Int(lvlX, lvlZ);
            if (opDat.Platform.IsInside(lvlPos))
                SetTile(opDat.Platform, opDat.Tiles, lvlPos, (ushort)tileType, opDat.TileTypesMask, action: OutOfLevelBoundsAction.IgnoreTile);

            k_positions.Add(pos);

            //ModifySurroundingLevel(pos);
            UpdateExits(pos);

            // TODO: FIX for Room Editor
            //if (texType!= -1)
            //    _Room.SetTile(CurrentLevel, pos.x, pos.y, (ushort)texType, mask: TexMask, shift: TexShift, action: OutOfLevelBoundsAction.IgnoreTile);
            //if (variation!= -1)
            //    _Room.SetTile(CurrentLevel, pos.x, pos.y, (ushort)variation, mask: VarMask, shift: VarShift, action: OutOfLevelBoundsAction.IgnoreTile);

            // TODO: FIX for Room Editor
            //SetRoomDirty();
        }

        static void UpdateExits(Vector2Int pos)
        {
            var ed = RoomEditor.CurrentEditor;
            var isEditingRoom = ed != null && ed.RoomInitialized;
            if (!isEditingRoom)
                return;

            var roomTileDim = ed.RoomInstanceData.TileDim;
            var editedRoomEdge = (pos.x == 0 || pos.y == 0 || pos.x == (roomTileDim.x - 1) || pos.y == (roomTileDim.y - 1));
            if (!editedRoomEdge)
                return;
            ed.RoomInstance.UpdateExits();
        }

        public static void BrushTypeSizeGUI(ref TileDrawingOpData opDat)
        {
            EditorGUILayout.BeginHorizontal();
            for (var i = 0; i <= (int) BrushType.Fill; i++)
            {
                var tex = PackageUtil.Load($"Icons/Brush/{(BrushType) i}.png") as Texture2D;
                if (CustomGUI.ActivityButton(opDat.Brush == (BrushType) i, "", tex, GUILayout.Width(45)))
                    opDat.Brush = (BrushType) i;
            }

            EditorGUILayout.EndHorizontal();

            opDat.BrushSize = EditorGUILayout.Slider("Brush Size", opDat.BrushSize, k_minBrushSize, k_maxBrushSize);
        }

        public static void TileSelectionGUI(ref TileDrawingOpData opDat, TilesSetData tileSetData)
        {
            ushort tIdx = 0;
            for (; tIdx < tileSetData.Count; ++tIdx)
            {
                var current = tileSetData.TileConfig?.Result[tIdx];
                if (current == null) continue;

                if (tIdx % 5 == 0)
                    EditorGUILayout.BeginHorizontal();
                var toolName = current.TileName;

                var lIdx = tileSetData.GetTileIdx(opDat.PaintTileTypeLeft);
                var rIdx = tileSetData.GetTileIdx(opDat.PaintTileTypeRight);

                if (CustomGUI.ActivityButton2(lIdx == tIdx, rIdx == tIdx, toolName,
                    current.PreviewTex, GUILayout.Width(100)))
                {
                    if (Event.current.button == 1)
                        opDat.PaintTileTypeRight = tileSetData.GetCombinedTile(opDat.PaintTileTypeRight, tIdx);
                    else
                        opDat.PaintTileTypeLeft = tileSetData.GetCombinedTile(opDat.PaintTileTypeLeft, tIdx);
                }

                if (tIdx % 5 == 4)
                    EditorGUILayout.EndHorizontal();
            }

            if (tIdx % 5 != 0)
                EditorGUILayout.EndHorizontal();
        }

        static void DimensionLayoutOverride(ref TileDrawingOpData opDat)
        {
            var activeConfigs = 0;
            var overrides = 0;
            
            var overrideType = GridSettings.DimensionLayout.Tiles;
            var allSameOverride = true;
            
            var count = opDat.TilesSet.TileDataSets.Count; //Mathf.Min(m_debugData.Length, Target.TilesSet.TileDataSets.Count);
            for (var i = 0; i < count; i++)
            {
                var tileData = opDat.TilesSet.TileDataSets[i];
                var tileMask = tileData.Mask;
                var active = (opDat.TileTypesMask & tileMask) != 0;
                if (active)
                    ++activeConfigs;
                else continue;
                
                if (!(tileData.TileConfig?.Result is IDimensionLayoutOverride @override))
                    continue;
                if (@overrides != 0 && @override.Layout != overrideType)
                    allSameOverride = false;
                    
                ++overrides;
                overrideType = @override.Layout;
            }

            if (activeConfigs == overrides && allSameOverride)
            {
                k_overrideGrid.CurrentGrid = opDat.OriginalGridData;
                k_overrideGrid.Layout = overrideType;
                opDat.GridData = k_overrideGrid;
            }
            else
                opDat.GridData = opDat.OriginalGridData;
        }

        public static bool TileTypesActiveGUI(ref TileDrawingOpData opDat, TilesSetData tileSetData)
        {
            var active = (opDat.TileTypesMask & tileSetData.Mask) != 0;
            if (GUILayout.Toggle(active, tileSetData.TileConfig.Result?.Name, "Button", GUILayout.Width(150f)) == active) 
                return active;

            active = !active;
            if (active)
                opDat.TileTypesMask |= tileSetData.Mask;
            else opDat.TileTypesMask &= (ushort) ~tileSetData.Mask;

            // Debug.Log("TileTypesActiveGUI changed");
            DimensionLayoutOverride(ref opDat);

            return active;
            //Debug.Log($"opDat-mask {opDat.TileTypesMask} tileSet-mask: {tileSetData.Mask} active: {active}");
        }

        //static LevelTextureConfig LevelTextureConfigGUI()
        //{
        // TODO: FIX for Room Editor
        //var newSetting = EditorGUILayout.ObjectField("Level Texture Setting", _Room.TexSettingData.Setting, typeof(LevelTextureSetting), false) as LevelTextureSetting;
        //if (_Room.TexSettingData.Setting!= newSetting)
        //{
        //    //TODO ROOM:  Fix this
        //    //Undo.RecordObject(_Room, "Setting changed");
        //    //_Room.Setting = newSetting;
        //}
        //using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        //    LevelSettingsEditorGUI();
        //var setting = _Room.TexSettingData.Setting;
        //    return null;
        //}

        //static void GeometryGUI()
        //{
        //GUILayout.Label("Geometry: ", GUILayout.Width(100));
        //using (new GUILayout.HorizontalScope())
        //{
        //    for (var gIdx = -1; gIdx <= (int)Tiles.Type.Floor; ++gIdx)
        //    {
        //        var toolName = ((Tiles.Type)gIdx).ToString();
        //        if (!ActivityButton2(opDat._PaintTileTypeLeft == gIdx, opDat._PaintTileTypeRight == gIdx, toolName,
        //            GUILayout.Width(100)))
        //            continue;
        //        //var prevLeft = _PaintTileTypeLeft;
        //        //var prevRight = _PaintTileTypeRight;
        //        if (Event.current.button == 1)
        //            opDat._PaintTileTypeRight = gIdx;
        //        else
        //            opDat._PaintTileTypeLeft = gIdx;
        //        //if (_PaintTileTypeLeft == _PaintTileTypeRight && _PaintTileTypeLeft!= -1)
        //        //{
        //        //    if (Event.current.button == 1)
        //        //        _PaintTileTypeLeft = prevRight;
        //        //    else _PaintTileTypeRight = prevLeft;
        //        //}
        //        //EditorPrefs.SetInt(_EditorPrefPaintTileTypeLeft, opDat._PaintTileTypeLeft);
        //        //EditorPrefs.SetInt(_EditorPrefPaintTileTypeRight, opDat._PaintTileTypeRight);
        //    }
        //}
        //}
        //void VariationGUI()
        //{
        //    m_paintVariationLeft = EditorGUILayout.IntField("Variation Left: ", m_paintVariationLeft);
        //    m_paintVariationRight = EditorGUILayout.IntField("Variation Right: ", m_paintVariationRight);
        //}
    }

    public class LayoutOverrideGrid : IGridData
    {
        public IGridData CurrentGrid;
        public GridSettings.DimensionLayout Layout;

        public float TileSize => CurrentGrid.TileSize;
        public float TileHeight => CurrentGrid.TileHeight;
        public int ExtraDim => _ExtraDim(Layout);
        public Vector2 Offset => _Offset(Layout);
        
        static int _ExtraDim(GridSettings.DimensionLayout layout) => layout == GridSettings.DimensionLayout.Corners ? 1 : 0;
        Vector2 _Offset(GridSettings.DimensionLayout layout) => layout == GridSettings.DimensionLayout.Corners ? Vector2.zero 
            : 0.5f * TileSize * new Vector2(1,1);
    }
}