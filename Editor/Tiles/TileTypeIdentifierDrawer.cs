﻿using System;
using Core.Editor.Attribute;
using Core.Editor.types;
using Core.Extensions;
using Core.Types;
using Core.Unity.Attributes;
using Core.Unity.Types;
using Core.Unity.Types.Attribute;
using Core.Unity.Types.InterfaceContainerBase;
using Core.Unity.Utility.GUITools;
using JetBrains.Annotations;
using Level.Tiles;
using Level.Tiles.Interface;
using UnityEditor;
using UnityEngine;

using static Core.Utils;
using MessageType = UnityEditor.MessageType;

namespace Level.Editor.Tiles
{
    [CustomPropertyDrawer(typeof(TileTypeIdentifier))]
    public class TileTypeIdentifierFallbackDrawer : PropertyDrawer
    {
        TileTypeIdentifierDrawer.VarProperties m_current;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return TileTypeIdentifierDrawer.GetProperties(property, true, out m_current, out var err) ==
                   OperationResult.Error
                ? EditorStyles.helpBox.CalcSize(new GUIContent(err)).y
                :TileTypeIdentifierDrawer.GetPropertyHeight(m_current);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) 
            => TileTypeIdentifierDrawer.DoGUI(ref position, property, label);
    }

    [UsedImplicitly]
    public class TileTypeIdentifierDrawer : MultiPropertyEditorDrawer
    {
        public override Type PreviewForAttribute => typeof(TileTypeIdentifierAttribute);

        internal struct VarProperties
        {
            internal bool DrawConfig;

            internal SerializedProperty MainProperty;
            internal SerializedProperty ConfigProperty;
            internal SerializedProperty ConfigObjProperty;

            internal SerializedProperty TileIdxProperty;
        }

        VarProperties m_current;
        const float k_defaultLineHeight = 18f;

        static ITileConfig m_lastCachedConfig;
        static string[] m_cachedTilesDataNames;

        public override float GetPropertyHeight(MultiPropertyAttribute attr, 
            SerializedProperty property, GUIContent label, float height)
        {
            var tileTypeAttr = attr as TileTypeIdentifierAttribute;

            return GetProperties(property, tileTypeAttr?.DrawConfig ?? true, out m_current, out var err) ==
                   OperationResult.Error
                ? EditorStyles.helpBox.CalcSize(new GUIContent(err)).y
                : GetPropertyHeight(m_current);
        }

        internal static float GetPropertyHeight(VarProperties properties)
        {
            if (properties.ConfigObjProperty.objectReferenceValue == null || !properties.DrawConfig)
                return k_defaultLineHeight;
            return 2 * k_defaultLineHeight;
        }

        public override void OnGUI(MultiPropertyAttribute attr, ref Rect position, SerializedProperty property, GUIContent label)
        {
            var tileTypeAttr = attr as TileTypeIdentifierAttribute;
            DoGUI(ref position, property, label, tileTypeAttr?.DrawConfig ?? true);
        }

        internal static void DoGUI(ref Rect position, SerializedProperty property, GUIContent label, bool drawConfig = true)
        {
            var changes = false;
            if (GetProperties(property, drawConfig, out var varProps, out var problems) == OperationResult.Error)
            {
                EditorGUI.HelpBox(position, problems, MessageType.Error);
                return;
            }

            CacheOptions(varProps);

            using (var scope = new EditorGUI.PropertyScope(position, label, property))
                changes |= OnGUI_PropertyScoped(ref position, scope, varProps) == ChangeCheck.Changed;

            if (changes)
                varProps.MainProperty.serializedObject.ApplyModifiedProperties();
        }

        static ChangeCheck OnGUI_PropertyScoped(ref Rect position, EditorGUI.PropertyScope scope, VarProperties varProperties)
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                using (var vertical = new VerticalRectScope(ref position, position.height))
                {
                    var labelWidth = 0f;
                    if (varProperties.DrawConfig)
                    {
                        using (var horizontal = new HorizontalRectScope(ref vertical.Rect, k_defaultLineHeight))
                        {
                            DrawLabel(scope, horizontal, out labelWidth);
                            // usually draw value in same line if it fits
                            var restRect = horizontal.Get(horizontal.Rect.width);
                            EditorGUI.PropertyField(restRect, varProperties.ConfigProperty, GUIContent.none);
                        }
                    }

                    if (varProperties.ConfigObjProperty.objectReferenceValue == null || m_cachedTilesDataNames == null)
                        return check.changed ? ChangeCheck.Changed : ChangeCheck.NotChanged;

                    using (var horizontal = new HorizontalRectScope(ref vertical.Rect, k_defaultLineHeight))
                    {
                        var indent = horizontal.Get(labelWidth);
                        var restRect = horizontal.Get(horizontal.Rect.width);

                        varProperties.TileIdxProperty.intValue = EditorGUI.Popup(restRect, varProperties.TileIdxProperty.intValue, m_cachedTilesDataNames);
                    }
                }
                return check.changed ? ChangeCheck.Changed : ChangeCheck.NotChanged;
            }
        }
        static void DrawLabel(EditorGUI.PropertyScope scope, HorizontalRectScope horizontal, out float labelWidth)
        {
            var label = $"{scope.content.text}";
            var size = EditorStyles.label.CalcSize(new GUIContent(label));
            size.x = Mathf.Clamp(size.x + 15f, 100f, 200f);
            labelWidth = size.x;
            var labelRect = horizontal.Get(labelWidth);
            EditorGUI.LabelField(labelRect, label);
        }

        internal static void CacheOptions(VarProperties varProperties)
        {
            var tileConf = varProperties.ConfigObjProperty.objectReferenceValue;
            if (tileConf == null)
            {
                m_lastCachedConfig = null;
                m_cachedTilesDataNames = null;
            }
            if (ReferenceEquals(tileConf, m_lastCachedConfig))
                return;

            m_lastCachedConfig = (ITileConfig)tileConf;
            m_cachedTilesDataNames = new string[m_lastCachedConfig.Count];
            for (var i = 0; i < m_lastCachedConfig.Count; i++)
                m_cachedTilesDataNames[i] = m_lastCachedConfig[i].TileName;
        }

        internal static OperationResult GetProperties(SerializedProperty property, bool drawConfig,
            out VarProperties varProperties, out string problems)
        {
            varProperties = default;
            problems = default;

            var confProp = property.FindPropertyRelative(nameof(TileTypeIdentifier.TileConfig));
            var tileIdxProp = property.FindPropertyRelative(nameof(TileTypeIdentifier.TileIdx));
            var configObjProperty = confProp?.FindPropertyRelative(InterfaceContainerBase.Editor_ObjectFieldPropName);

            if (NULL.IsAny(confProp, tileIdxProp, configObjProperty))
            {
                problems = ($"Not all properties have been found! On {property.propertyPath} \n" +
                            $"FOUND: confProp {confProp != null} tileIdxProp {tileIdxProp != null} ctxProvObjField {configObjProperty != null}");
                return OperationResult.Error;
            }

            varProperties = new VarProperties()
            {
                DrawConfig = drawConfig,
                MainProperty = property,
                ConfigProperty = confProp,
                ConfigObjProperty = configObjProperty,
                TileIdxProperty = tileIdxProp
            };

            return OperationResult.OK;
        }
    }
}
