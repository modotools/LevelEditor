namespace Level.Editor.Tiles
{
    enum BrushType
    {
        PointBrush,
        CircleBrush,
        RectangleBrush,
        DragLine,
        DragEllipseDiagonal,
        DragEllipseRect,
        DragRectangle,
        Fill
    }
}
