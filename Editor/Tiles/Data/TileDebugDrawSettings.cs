﻿using System;
using Core.Extensions;
using UnityEngine;
using Level.Tiles.Interface;

namespace Level.Editor.Tiles
{
    [CreateAssetMenu(fileName = nameof(TileDebugDrawSettings), menuName = "Level/Tiles/"+nameof(TileDebugDrawSettings))]
    public class TileDebugDrawSettings : ScriptableObject
    {
        [Serializable]
        public struct TileConfigDebugDrawData
        {
            public RefITileConfig TileConfig;
            public TileDebugDraw.DebugDrawSettings Settings;

            public bool Foldout;
            public bool DrawOnlyWhenUsing;
        }

        [SerializeField] TileConfigDebugDrawData[] m_debugDrawData = new TileConfigDebugDrawData[0];

        public ref TileConfigDebugDrawData Get(ITileConfig config)
        {
            var idx = Array.FindIndex(m_debugDrawData, d => d.TileConfig.Result == config);
            if (idx != -1) 
                return ref m_debugDrawData[idx];
        
            CollectionExtensions.Add(ref m_debugDrawData, new TileConfigDebugDrawData(){ TileConfig = new RefITileConfig(){ Result = config }});
            idx = Array.FindIndex(m_debugDrawData, d => d.TileConfig.Result == config);
            return ref m_debugDrawData[idx];
        }
    }
}
