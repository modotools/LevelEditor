using Level.PlatformLayer.Interface;
using Level.Tiles;
using Level.Tiles.Interface;
using UnityEditor.AnimatedValues;
using UnityEngine;

namespace Level.Editor.Tiles
{
    public struct TileDrawingOpData
    {
        public bool Is2DMode;

        // todo: save/ load
        internal BrushType Brush;

        internal bool IsBrushType => Brush == BrushType.PointBrush 
                                     || Brush == BrushType.CircleBrush 
                                     || Brush == BrushType.RectangleBrush;
        
        //bool IsDragType => _Brush == BrushType.DragEllipseDiagonal || _Brush == BrushType.DragRectangle;
        //internal bool IsVariationOnly => (PaintTileTypeLeft + PaintTileTypeRight + _PaintTexLeft + _PaintTexRight == -4 
        //&& _PaintVariationLeft + _PaintVariationRight!= -2);

        internal TileDrawingPos? StartPos;
        // todo: save/ load
        internal float BrushSize;

        // we dont use TileSetData here, because we could potentially combine them
        // to paint multiple tile-data at once with combined mask etc.
        internal uint PaintTileTypeLeft;
        internal uint PaintTileTypeRight;

        internal ushort TileTypesMask;
        internal bool FireEvent;

        internal AnimBool ShowLevelSettingEditor;

        public TilesSetListConfig TilesSet;

        internal IGridData OriginalGridData;
        internal IGridData GridData;
        internal IPlatformLayerSpatialData Platform;
        internal IPlatformLayerTiles Tiles;
    }

    public struct TileDrawingPos
    {
        public Vector3 TilePos;
        public Vector3 CursorPos;
    }
}