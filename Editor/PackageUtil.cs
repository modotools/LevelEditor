﻿using UnityEditor;
using UnityEngine;

public static class PackageUtil
{
    public const string PackagePath = "Packages/com.modotools.leveleditor/";
    public const string PackagePathEditorResources = PackagePath + "Editor Resources/";
    public static Object Load(string path) => EditorGUIUtility.Load($"{PackagePathEditorResources}{path}");

}
