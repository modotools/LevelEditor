using Core.Editor.Inspector;
using Core.Editor.Interface;
using Level.Room;
using UnityEditor;
using UnityEngine;

namespace Level.Editor.Data
{
    [CustomEditor(typeof(RoomObjectMapper))]
    public class RoomObjectMapperInspector : BaseInspector<RoomObjectMapperEditor>
    {
        public override DefaultGUISettings DefaultGUISettings => new DefaultGUISettings
        {
            DrawDefaultGUIFoldout = true, CanUnlockDefaultGUI = true, DefaultGUIEnabled = false
        };
    }

    public class RoomObjectMapperEditor : BaseEditor<RoomObjectMapper>
    {
        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);
        }

        public override void OnGUI(float width)
        {
            if (GUILayout.Button("Clear missing references"))
            {
                Target.ClearMissingProperties();
            }
            
            base.OnGUI(width);
        }
        
    }
}