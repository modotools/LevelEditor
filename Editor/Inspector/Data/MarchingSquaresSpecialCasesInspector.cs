﻿using Core.Editor.Inspector;
using Core.Editor.Utility;
using Core.Types;
using Level.Data;
using UnityEditor;
using UnityEngine;

namespace Level.Editor.Tiles
{
    [CustomEditor(typeof(MSSpecialCasesConfig))]
    public class MarchingSquaresSpecialCasesInspector: BaseInspector<MarchingSquaresSpecialCasesEditor> { }

    public class MarchingSquaresSpecialCasesEditor : BaseEditor<MSSpecialCasesConfig>
    {
        public override void OnGUI(float width)
        {
            //base.OnGUI(width);
            var changes = false;
            for (var i = 0; i < 16; i++)
            {
                var elementProp = SerializedObject.FindProperty(MSSpecialCasesConfig.Editor_ConfigDataPropName)
                    .GetArrayElementAtIndex(i);
                //ref var meshData = ref target.Editor_Get(i);
                using (new EditorGUILayout.HorizontalScope())
                {
                    EditorGUILayout.LabelField($"{i}",GUILayout.Width(20f));
                    ConfigBoxGUI.DrawConfigurationBox(i);

                    changes |= TileMeshConfigDataGUI(elementProp) == ChangeCheck.Changed;
                }
                CustomGUI.HSplitter();
                GUILayout.Space(5f);
            }

            if (!changes)
                return;

            SerializedObject.ApplyModifiedProperties();
            OnChanged();
        }

        static ChangeCheck TileMeshConfigDataGUI(SerializedProperty elementProp)
        {
            using (new EditorGUILayout.VerticalScope())
            {
                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    EditorGUILayout.PropertyField(elementProp, true);
                    if (!check.changed) 
                        return ChangeCheck.NotChanged;
                }
            }

            return ChangeCheck.Changed;
        }
    }
}