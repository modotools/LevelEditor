using System.Linq;
using Core.Editor.Inspector;
using Core.Editor.Interface;
using Core.Editor.Utility;
using Core.Extensions;
using Level.Texture;
using Level.Tiles;
using UnityEditor;
using UnityEngine;
using static Core.Extensions.CollectionExtensions;

namespace Level.Editor.Data
{
    [CustomEditor(typeof(TextureCollectionConfig), true)]
    public class TextureCollectionConfigInspector : BaseInspector<TextureCollectionConfigEditor>
    {
        public override DefaultGUISettings DefaultGUISettings => new DefaultGUISettings
        {
            DrawDefaultGUIFoldout = true, CanUnlockDefaultGUI = true, DefaultGUIEnabled = false
        };
    }

    public class TextureCollectionConfigEditor : BaseEditor<TextureCollectionConfig>
    {
        int m_currentView = 0;
        Vector2 m_scrollPos;

        public override void OnGUI(float width)
        {
            using (var scrollScope = new EditorGUILayout.ScrollViewScope(m_scrollPos))
            {
                m_scrollPos = scrollScope.scrollPosition;
                base.OnGUI(width);

                using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    EditorGUILayout.LabelField("Settings", EditorStyles.boldLabel);
                    SettingsGUI();
                }

                if (Target.Layout == null || Target.AtlasData == null)
                {
                    EditorGUILayout.HelpBox("No Layout or AtlasData set!", MessageType.Warning);
                    return;
                }

                SelectChannelGUI();

                using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    for (var i = 0; i < Target.Data.Length; i++)
                    {
                        ref var dataEl = ref Target.Data[i];
                        var textureTypeID = Target.Layout.TileBuilderTypeIDs[i];

                        TexDataListGUI(textureTypeID, ref dataEl.Variations, dataEl.TileBuilderTypeID.name);
                    }
                }
            }
        }

        void SelectChannelGUI()
        {
            using (new GUILayout.HorizontalScope())
            {
                for (var i = 0; i < Target.TextureChannelNames.Length; i++)
                {
                    if (CustomGUI.ActivityButton(m_currentView == i, Target.TextureChannelNames[i]))
                        m_currentView = i;
                }
            }

            if (m_currentView >= Target.TextureChannelNames.Length)
                m_currentView = 0;
        }

        void SettingsGUI()
        {
            var atlasProp = SerializedObject.FindProperty(TextureCollectionConfig.Editor_SettingsPropertyName);
            using (new EditorGUILayout.HorizontalScope())
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(atlasProp, GUIContent.none, true);
                if (check.changed || GUILayout.Button("Update Texture Data"))
                {
                    SerializedObject.ApplyModifiedProperties();

                    UpdateLayout();
                    UpdateTexChannels();
                    OnChanged();
                }
            }
        }


        void TexDataListGUI(TileBuilderTypeID id, ref TextureData[] data, string label)
        {
            EditorGUILayout.LabelField(label);
            using (new GUILayout.HorizontalScope())
            {
                var removeIdx = -1;
                for (var i = 0; i < data.Length; ++i)
                {
                    TexDataGUI(id, data[i], out var removeAtIndex);
                    if (removeAtIndex)
                        removeIdx = i;
                }

                var remove = removeIdx != -1;
                var add = GUILayout.Button("+", GUILayout.Width(20.0f));

                var changes = add || remove;

                if (add)
                    Add(ref data, new TextureData(Target.TextureChannels, id));
                if (remove) 
                    RemoveAt(ref data, removeIdx);
                if (changes)
                    OnChanged();
            }
        }

        void TexDataGUI(TileBuilderTypeID id, TextureData data, out bool remove)
        {
            remove = false;
            float w = 50.0f, h = 50.0f;
            if (data.IsSet() && Target.Layout != null)
            {
                var currentTex = data.GetTexture(m_currentView);
                var dim = Target.Layout.UVLayoutConfig.GetDimension(id); // data.TileDimension(Target.Layout.TextureTypeConfig);
                w = 50.0f * dim.x;
                h = 50.0f * dim.y;
            }

            var rect = EditorGUILayout.GetControlRect(GUILayout.Width(w), GUILayout.Height(h + 10f));

            if (!m_currentView.IsInRange(data.Textures))
                return;

            var tex = data.GetTexture(m_currentView);
            var newTex = EditorGUI.ObjectField(new Rect(rect.x, rect.y + 10.0f, w, h), 
                    tex, typeof(Texture2D), false) as Texture2D;
            if (newTex != tex)
            {
                data.SetTexture(m_currentView, newTex);
                // Target.UpdateCollectionDataTypes();
            }

            if (m_currentView == 0 && GUI.Button(new Rect(rect.x, rect.y, w, 10.0f), "X"))
                remove = true;
        }


        void UpdateTexChannels()
        {
            if (Target.AtlasData == null)
                return;

            foreach (var td in Target)
                td.ChangeTextureTypeCount(Target.TextureChannels);
        }

        void UpdateLayout()
        {
            var oldData = Target.Data 
                          ?? new TextureCollectionConfigData[0];

            var layout = Target.Layout;
            //var config = layout!= null?layout.TextureTypeConfig : null;
            var ids =layout != null? layout.TileBuilderTypeIDs : null;
            if (ids.IsNullOrEmpty())
                return;
            Debug.Assert(ids != null);

            var texChannels = Target.TextureChannels;

            var newIDData = new TextureCollectionConfigData[ids.Length];
            for (var i = 0; i < newIDData.Length; i++)
            {
                var id = ids[i];
                Debug.Log($"UpdateLayout for ID: {id}");

                ref var newData = ref newIDData[i];
                newData.TileBuilderTypeID = id;

                var oldTexData = oldData.FirstOrDefault(d => d.TileBuilderTypeID == id);
                if (!oldTexData.Variations.IsNullOrEmpty())
                    Debug.Log($"found old variations");

                newData.Variations = oldTexData.Variations.IsNullOrEmpty() 
                    ? new TextureData[1]
                    : oldTexData.Variations;

                InitVariations(ref newData, id, texChannels);
            }

            Target.Editor_Data = newIDData;
        }

        static void InitVariations(ref TextureCollectionConfigData newID, TileBuilderTypeID id, int texChannels)
        {
            for (var i = 0; i < newID.Variations.Length; i++)
            {
                ref var variation = ref newID.Variations[i];
                if (variation != null)
                    variation.TypeID = id;
                variation ??= new TextureData(texChannels, id);
            }
        }
    }
}