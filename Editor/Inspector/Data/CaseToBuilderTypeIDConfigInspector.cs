﻿using Core.Editor.Inspector;
using Core.Editor.Utility;
using Level.Tiles;
using UnityEditor;
using UnityEngine;

namespace Level.Editor.Tiles
{
    [CustomEditor(typeof(CaseToBuilderTypeIDConfig))]
    public class CaseToBuilderTypeIDConfigInspector: BaseInspector<CaseToBuilderTypeIDConfigEditor> { }

    public class CaseToBuilderTypeIDConfigEditor : BaseEditor<CaseToBuilderTypeIDConfig>
    {
        Vector2 m_scrollPos;

        public override void OnGUI(float width)
        {
            base.OnGUI(width);
            using (var check = new EditorGUI.ChangeCheckScope())
            using (var scrollScope = new EditorGUILayout.ScrollViewScope(m_scrollPos))
            {
                m_scrollPos = scrollScope.scrollPosition;
                var listProp = SerializedObject.FindProperty(CaseToBuilderTypeIDConfig.Editor_TileBuilderTypeIDs_PropName);
                
                for (var i = 0; i < 16; i++)
                {
                    var elementProp = listProp.GetArrayElementAtIndex(i);
                    //ref var meshData = ref target.Editor_Get(i);
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        EditorGUILayout.LabelField($"{i}", GUILayout.Width(20f));
                        ConfigBoxGUI.DrawConfigurationBox(i);
                        EditorGUILayout.PropertyField(elementProp);
                    }
                    
                    CustomGUI.HSplitter();
                    GUILayout.Space(5f);
                }

                if (check.changed)
                    SerializedObject.ApplyModifiedProperties();
            }
        }
    }
}