﻿using System.Collections.Generic;
using System.Linq;
using Core.Editor.Inspector;
using Core.Editor.Utility;
using Core.Events;
using Core.Extensions;
using Core.Unity.Extensions;
using Core.Unity.Utility;
using Level.Tiles;
using UnityEditor;
using UnityEngine;
using static Core.Extensions.CollectionExtensions;

namespace Level.Editor.Tiles
{
    [CustomEditor(typeof(MSCustomMeshMaterialConfig))]
    public class CustomMeshMaterialConfigInspector: UnityEditor.Editor
    {
        MSCustomMeshMaterialConfig Target => target as MSCustomMeshMaterialConfig;

        struct CaseData
        {
            public bool Ignored;
            public bool Corner;

            public int OriginalCase;
            public float AddYRotation;
        }

        static readonly CaseData[] k_casesData =
        {
            new CaseData(){ Ignored = true }, // 0
            new CaseData(){ OriginalCase = 1, Corner = true}, // 1
            new CaseData(){ OriginalCase = 1, Corner = true, AddYRotation = -90 }, // 2
            new CaseData(){ OriginalCase = 3 }, // 3
            new CaseData(){ OriginalCase = 1, Corner = true, AddYRotation = 180 }, // 4
            new CaseData(){ Ignored = true }, // 5
            new CaseData(){ OriginalCase = 3, AddYRotation = -90}, // 6
            new CaseData(){ OriginalCase = 14, Corner = true, AddYRotation = 90  }, // 7
            new CaseData(){ OriginalCase = 1, Corner = true, AddYRotation = 90 }, // 8
            new CaseData(){ OriginalCase = 3, AddYRotation = 90 }, // 9
            new CaseData(){ Ignored = true }, // 10
            new CaseData(){ OriginalCase = 14, Corner = true, AddYRotation = 180 }, // 11
            new CaseData(){ OriginalCase = 3, AddYRotation = 180 }, // 12
            new CaseData(){ OriginalCase = 14, Corner = true, AddYRotation = -90 }, // 13
            new CaseData(){ OriginalCase = 14, Corner = true }, // 14
            new CaseData(){ OriginalCase = 15 }, // 15
        };


        Vector2 m_scrollPos;
        GameObject m_previewParent;
        TileBuilderTypeID m_previewId;

        void OnDisable() => ClearPreviewObjects();

        void ClearPreviewObjects()
        {
            if (m_previewParent == null)
                return;

            m_previewParent.DestroyEx();
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var autoRotate = Target.Editor_AutoRotateCases;

            using (var scrollScope = new EditorGUILayout.ScrollViewScope(m_scrollPos))
            {
                m_scrollPos = scrollScope.scrollPosition;

                SelectPreviewTagID_GUI();

                var listProp = serializedObject.FindProperty(MSCustomMeshMaterialConfig.Editor_ConfigDataPropName);
                for (var i = 0; i < 16; i++)
                {
                    var caseData = k_casesData[i];
                    var copyFrom = caseData.OriginalCase;
                    var ignored = caseData.Ignored;
                    if (ignored)
                        continue;
                    var copyData = autoRotate && i != copyFrom;
                    if (copyData) 
                        CopyWithRotation(copyFrom, i, caseData.AddYRotation);

                    var elementProp = listProp.GetArrayElementAtIndex(i);
                    //ref var meshData = ref target.Editor_Get(i);
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        EditorGUILayout.LabelField($"{i}", GUILayout.Width(20f));
                        ConfigBoxGUI.DrawConfigurationBox(i);
                        using (new EditorGUILayout.VerticalScope())
                        {
                            using (new EditorGUI.DisabledGroupScope(copyData))
                                ConfigDataGUI(elementProp);

                            var ids = Target.Editor_GetSourceData(i).MeshData.Select(a => a.BuilderTypeID).Distinct();
                            TileBuilderTypeID doPreview = null;
                            using (new EditorGUILayout.HorizontalScope())
                            {
                                foreach (var id in ids)
                                {
                                    if (GUILayout.Button($"Preview {id.name}", GUILayout.Width(150f)))
                                        doPreview = id;
                                }
                            }

                            if (doPreview != null)
                            {
                                m_previewId = doPreview;

                                var previewData = Target.Editor_GetSourceData(i).MeshData
                                    .FirstOrDefault(a => a.BuilderTypeID == m_previewId);
                                var isHard = m_previewId.IsHard;
                                Preview(previewData.VariationSets, Target.Editor_GetColliderData(isHard, i));
                            }
                        }
                    }
                    
                    CustomGUI.HSplitter();
                    GUILayout.Space(5f);
                }

                //    if (GUILayout.Button("Preview", GUILayout.Width(150f)))
                //        Preview(Target.Editor_GetSourceData(i).DefaultVariations);
            }
        }

        void SelectPreviewTagID_GUI()
        {
            var allBorderTypeIDs =
                Target.Editor_GetCaseData().SelectMany(e => e.MeshData)
                    .Select(a => a.BuilderTypeID).Where(a => a != null).Distinct().ToArray();
            var allBorderTypeIDNames = allBorderTypeIDs.Select(a => a.ToString()).ToArray();

            var selected = allBorderTypeIDs.FirstIndexWhere(a => a == m_previewId);
            selected = EditorGUILayout.Popup(selected, allBorderTypeIDNames);
            m_previewId = selected.IsInRange(allBorderTypeIDs) ? allBorderTypeIDs[selected] : null;
        }

        void Preview(IList<CustomMeshMaterialDataSet> data, Mesh colliderMesh)
        {
            ClearPreviewObjects();

            if (data.IsNullOrEmpty())
                return;
            var item = data.RandomItem();

            m_previewParent = new GameObject("Preview", typeof(CleanupMarker));
            for (var i = 0; i < item.Data.Length; i++)
            {
                var itemData = item.Data[i];
                var preview = new GameObject($"{i}", typeof(CleanupMarker));
                preview.transform.SetParent(m_previewParent.transform);

                var filter = preview.AddComponent<MeshFilter>();
                var rend = preview.AddComponent<MeshRenderer>();
                preview.hideFlags = HideFlags.DontSave | HideFlags.NotEditable;

                preview.transform.position = itemData.Offset;
                preview.transform.rotation = Quaternion.Euler(itemData.Rotation);
                filter.mesh = itemData.Mesh;
                rend.material = itemData.PreviewMaterial;
            }

            var collider = m_previewParent.AddComponent<MeshCollider>();
            collider.sharedMesh = colliderMesh;
        }

        void CopyWithRotation(int fromIdx, int toIdx, float yRotation)
        {
            var source = Target.Editor_GetSourceData(fromIdx);
            ref var targetData = ref Target.Editor_GetSourceData(toIdx);
            CopyWithRotation(source.MeshData, ref targetData.MeshData, yRotation);
        }

        static void CopyWithRotation(IEnumerable<BuilderTypeID_To_CustomMeshMaterialVariations> source,
            ref BuilderTypeID_To_CustomMeshMaterialVariations[] targetData, float yRotation)
        {
            var removeIdx = targetData.FirstIndexWhere(t => t.BuilderTypeID == null);
            while (removeIdx > -1)
            {
                RemoveAt(ref targetData, removeIdx);
                removeIdx = targetData.FirstIndexWhere(t => t.BuilderTypeID == null);
            }

            foreach (var id_toMeshVariants in source)
            {
                var idx = targetData.FirstIndexWhere(a
                    => a.BuilderTypeID == id_toMeshVariants.BuilderTypeID);
                if (idx == -1)
                {
                    idx = targetData.Length;
                    Add(ref targetData, new BuilderTypeID_To_CustomMeshMaterialVariations()
                    {
                        BuilderTypeID = id_toMeshVariants.BuilderTypeID
                    });
                }

                CopyWithRotation(id_toMeshVariants.VariationSets, ref targetData[idx].VariationSets, yRotation);
            }
        }

        static void CopyWithRotation(IReadOnlyList<CustomMeshMaterialDataSet> source,
            ref CustomMeshMaterialDataSet[] targetData, float yRotation)
        {
            if (source == null)
                return;
            if (targetData?.Length != source.Count)
                targetData = new CustomMeshMaterialDataSet[source.Count];

            for (var i = 0; i < source.Count; i++)
            {
                if (source[i].Data == null)
                    continue;
                if (targetData[i].Data?.Length != source[i].Data.Length)
                    targetData[i].Data = new CustomMeshMaterialData[source[i].Data.Length];

                for (var j = 0; j < source[i].Data.Length; j++)
                {
                    targetData[i].Data[j] = source[i].Data[j];

                    targetData[i].Data[j].Rotation += new Vector3(0, yRotation, 0);
                    targetData[i].Data[j].Offset = Quaternion.Euler(0,yRotation,0) * source[i].Data[j].Offset;
                }
            }
        }

        void ConfigDataGUI(SerializedProperty elementProp)
        {
            using (new EditorGUILayout.VerticalScope())
            {
                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    EditorGUILayout.PropertyField(elementProp, true);
                    if (!check.changed) 
                        return;

                    serializedObject.ApplyModifiedProperties();

                    EditorUtility.SetDirty(target);
                    serializedObject.Update();
                    EventMessenger.TriggerEvent(new ObjectChanged { SourceEditor = this, Changed = target });
                }
            }
        }
    }
}