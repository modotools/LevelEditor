using System.Collections.Generic;
using System.Linq;
using Core.Editor.Inspector;
using Core.Editor.Interface;
using Core.Types;
using Level.Texture;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

namespace Level.Editor.Data
{
    [CustomEditor(typeof(AtlasDataConfig))]
    public class AtlasDataConfigInspector : BaseInspector<AtlasDataConfigEditor>
    {
        public override DefaultGUISettings DefaultGUISettings => new DefaultGUISettings
        {
            DrawDefaultGUIFoldout = true, CanUnlockDefaultGUI = true, DefaultGUIEnabled = false
        };
    }

    public class AtlasDataConfigEditor : BaseEditor<AtlasDataConfig>
    {
        struct TextureChannelsActive
        {
            public bool Active;
            public string TextureChannelName;
        }

        const string k_unityGlobalDefaultTexture = "unity_";
        //int m_currentView;
        Rect m_shaderMenuButtonRect;

        TextureChannelsActive[] m_shaderTextureChannels;

        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);
            if (Target != null && Target.ConfigData.Shader)
                SelectShader(Target.ConfigData.Shader);
        }

        public override void OnGUI(float width)
        {
            base.OnGUI(width);

            NameGUI();

            DimensionGUI();

            TemplateMaterialGUI();
            ShaderSelector();
            TextureChannelNamesGUI();
        }

        void NameGUI()
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label($"Name: ");

                ref var atlasData = ref this.Target.Editor_ConfigData;
                atlasData.Name = GUILayout.TextField(atlasData.Name);
                if (check.changed) 
                    OnChanged();
            }
        }

        void DimensionGUI()
        {
            ref var atlasData = ref Target.Editor_ConfigData;

            GUILayout.Label("Dimension", EditorStyles.boldLabel);
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                atlasData.AtlasDimension = EditorGUILayout.IntField(nameof(AtlasConfigData.AtlasDimension),
                    atlasData.AtlasDimension);
                using (new EditorGUILayout.HorizontalScope())
                {
                    atlasData.HalfTileDimension = EditorGUILayout.IntField("Tile-Dimension | half:",
                        atlasData.HalfTileDimension);
                    var fullDim = atlasData.HalfTileDimension * 2;
                    EditorGUILayout.LabelField($"full: {fullDim}");
                }

                atlasData.RepeatedPixels = EditorGUILayout.IntField(nameof(AtlasConfigData.RepeatedPixels),
                    atlasData.RepeatedPixels);

                if (check.changed) 
                    UpdateUVTileSizeAndInset();
            }
        }

        void UpdateUVTileSizeAndInset()
        {
            ref var atlasData = ref Target.Editor_ConfigData;
            var tileWidthAndHeight = 2f * atlasData.HalfTileDimension;
            if (atlasData.AtlasDimension <= 0)
                return;

            atlasData.UVInset = (float) atlasData.RepeatedPixels / atlasData.AtlasDimension;
            atlasData.UVTileSize = tileWidthAndHeight / atlasData.AtlasDimension;
            OnChanged();
        }

        void TemplateMaterialGUI()
        {
            ref var atlasData = ref Target.Editor_ConfigData;
            using (var check = new EditorGUI.ChangeCheckScope())
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label($"TemplateMaterial: ");

                atlasData.TemplateMaterial =
                    EditorGUILayout.ObjectField(atlasData.TemplateMaterial, typeof(Material), false) as Material;
                if (check.changed)
                {
                    if (atlasData.TemplateMaterial != null)
                        atlasData.Shader = atlasData.TemplateMaterial.shader;
                    OnChanged();
                }
            }
        }

        void ShaderSelector()
        {
            ref var atlasData = ref Target.Editor_ConfigData;

            using (new GUILayout.HorizontalScope())
            {
                var shaderName = atlasData.Shader ? atlasData.Shader.name : "-";
                GUILayout.Label($"shader: {shaderName}");
                if (GUILayout.Button("SelectShader"))
                {
                    var popup = Core.Editor.Utility.ShaderSelector.CreateShaderSelectorPopup(SelectShader);
                    PopupWindow.Show(m_shaderMenuButtonRect, popup);
                }

                if (Event.current.type == EventType.Repaint)
                    m_shaderMenuButtonRect = GUILayoutUtility.GetLastRect();
            }
        }

        void SelectShader(object shaderName)
        {
            ref var atlasData = ref Target.Editor_ConfigData;

            atlasData.Shader = Shader.Find((string) shaderName);
            var shd = atlasData.Shader;
            SelectShader(shd);
        }

        void SelectShader(Shader shader)
        {            
            ref var atlasData = ref Target.Editor_ConfigData;

            ExtractTextureProperties(shader, out var names);
            m_shaderTextureChannels = new TextureChannelsActive[names.Length];
            for (var i = 0; i < m_shaderTextureChannels.Length; i++)
            {
                var active = atlasData.TextureChannelNames?.Contains(names[i]) == true;
                m_shaderTextureChannels[i] = new TextureChannelsActive
                {
                    TextureChannelName = names[i], Active = active
                };
            }

        }

        void ExtractTextureProperties(Shader shd, out string[] textureNames)
        {
            textureNames = null;
            if (shd == null)
                return;

            var propCount = shd.GetPropertyCount();

            using (var texStrings = SimplePool<List<string>>.I.GetScoped())
            {
                for (var i = 0; i < propCount; i++)
                {
                    var propertyType = shd.GetPropertyType(i);
                    if (propertyType != ShaderPropertyType.Texture)
                        continue;

                    var name = shd.GetPropertyName(i);
                    if (name.StartsWith(k_unityGlobalDefaultTexture))
                        continue;
                    texStrings.Obj.Add(name);
                }

                textureNames = texStrings.Obj.ToArray();
            }
        }

        void TextureChannelNamesGUI()
        {
            //var atlasProp = SerializedObject.FindProperty(AtlasDataConfig.Editor_DataPropName);
            //EditorGUILayout.PropertyField(atlasProp.FindPropertyRelative(nameof(AtlasData.TextureChannelNames)));
            ref var atlasData = ref Target.Editor_ConfigData;

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                for (var i = 0; i < m_shaderTextureChannels.Length; i++)
                {
                    ref var ch = ref m_shaderTextureChannels[i];
                    ch.Active = EditorGUILayout.Toggle(ch.TextureChannelName, ch.Active);
                }

                if (check.changed)
                {
                    atlasData.TextureChannelNames = m_shaderTextureChannels.Where(ch => ch.Active)
                        .Select(ch => ch.TextureChannelName).ToArray();
                    OnChanged();
                }
            }

            //var texChannelNames = atlasData.TextureChannelNames;
            //if (texChannelNames == null)
            //    return;
            //using (new GUILayout.HorizontalScope())
            //{
            //    for (var i = 0; i < texChannelNames.Length; i++)
            //    {
            //        if (CustomGUI.ActivityButton(m_currentView == i, texChannelNames[i]))
            //            m_currentView = i;
            //    }
            //}
            //if (m_currentView >= texChannelNames.Length)
            //    m_currentView = 0;
        }
    }
}