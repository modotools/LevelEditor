﻿using Core.Editor.Inspector;
using Core.Editor.Utility;
using Level.Data;
using UnityEditor;
using UnityEngine;

namespace Level.Editor.Tiles
{
    [CustomEditor(typeof(TileMeshConfig))]
    public class TileMeshConfigInspector: BaseInspector<TileMeshConfigEditor> { }

    public class TileMeshConfigEditor : BaseEditor<TileMeshConfig>
    {
        Vector2 m_scrollPos;
        public override void OnGUI(float width)
        {
            base.OnGUI(width);

            using (var scrollScope = new EditorGUILayout.ScrollViewScope(m_scrollPos))
            {
                m_scrollPos = scrollScope.scrollPosition;
                for (var i = 0; i < 16; i++)
                {
                    var elementProp = SerializedObject.FindProperty(TileMeshConfig.Editor_ConfigDataPropName)
                        .GetArrayElementAtIndex(i);
                    //ref var meshData = ref target.Editor_Get(i);
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        EditorGUILayout.LabelField($"{i}",GUILayout.Width(20f));
                        ConfigBoxGUI.DrawConfigurationBox(i);

                        TileMeshConfigDataGUI(elementProp);
                    }
                    CustomGUI.HSplitter();
                    GUILayout.Space(5f);
                }
            }
        }

        void TileMeshConfigDataGUI(SerializedProperty elementProp)
        {
            using (new EditorGUILayout.VerticalScope())
            {
                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    EditorGUILayout.PropertyField(elementProp, true);
                    if (!check.changed) 
                        return;

                    SerializedObject.ApplyModifiedProperties();
                    OnChanged();
                }
            }
        }
    }
}