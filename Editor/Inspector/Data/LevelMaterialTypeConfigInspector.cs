using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Core.Editor.Extensions;
using Core.Editor.Inspector;
using Core.Editor.Interface;
using Core.Editor.PopupWindows;
using Core.Editor.Utility;
using Core.Editor.Utility.GUITools;
using Core.Types;
using Core.Unity.Extensions;
using Core.Unity.Types.InterfaceContainerBase;
using Level.Editor.Texture;
using Level.Data;
using Level.Texture;
using Level.Tiles.Interface;
using UnityEditor;
using Level.Tiles;
using UnityEngine;
using static Core.Extensions.CollectionExtensions;
using static Level.Editor.Settings.LevelEditorSettingsAccess;

namespace Level.Editor.Data
{
    [CustomEditor(typeof(LevelMaterialTypeConfig))]
    public class LevelMaterialTypeConfigInspector : BaseInspector<LevelMaterialTypeConfigEditor>
    {
        public override DefaultGUISettings DefaultGUISettings => new DefaultGUISettings
        {
            DrawDefaultGUIFoldout = true, CanUnlockDefaultGUI = true, DefaultGUIEnabled = false
        };
    }

    public class LevelMaterialTypeConfigEditor : BaseEditor<LevelMaterialTypeConfig>
    {
        // todo: Destroy sub assets when removing them

        public struct SetMeshMaterialForTypeInfo
        {
            public ILevelMaterial Material;
            public int Index;
        }

        UnityEditor.Editor m_cachedTileMeshMaterialEditor;
        SelectTypesPopup m_tileMeshProviderPopup;
        string GetMaterialPath(AtlasDataConfig atlas) => $"{LevelMaterialsPath}{Target.name}{atlas.Name}.mat";

        SetMeshMaterialForTypeInfo m_setTypeInfo;

        delegate void HeaderGUIDelegate(ILevelMaterial lvlMaterial, SerializedProperty propEl, 
            ref ChangeCheck changeCheck, out bool erase);
        delegate void LevelMaterialPropertiesGUIDelegate(float width, ILevelMaterial lvlMaterial,
            SerializedProperty elementProp, ref ChangeCheck check);

        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);

            EditorExtensions.ReflectionGetAllTypes(typeof(ITileMeshMaterialUVProvider), out var types, 
                out var names, out var namespaces);
            m_tileMeshProviderPopup = EditorExtensions.CreateTypeSelectorPopup(types, names, namespaces, 
                EditorExtensions.MenuFromNamespace.Default, TypeSelected);
        }

        public override void OnGUI(float width)
        {
            base.OnGUI(width);
            
            var hasWarnings = (Result.Errors + Result.Warnings) > 0;
            using (new EditorGUI.DisabledScope(hasWarnings))
            {
                if (GUILayout.Button("Generate Atlas", GUILayout.Height(35))) 
                    GenerateAtlas();
            }

            var changeCheck = ChangeCheck.NotChanged;
            GeometryGUI(ref changeCheck);
            MaterialTypesGUI(width, ref changeCheck);
            MaterialTransitionTypesGUI(width, ref changeCheck);
            if (changeCheck == ChangeCheck.Changed)
                OnChanged();
        }

        void GeometryGUI(ref ChangeCheck changeCheck)
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var geometryConfig = SerializedObject.FindProperty(LevelMaterialTypeConfig.Editor_GeometryConfigPropName);
                EditorGUILayout.PropertyField(geometryConfig);
                if (!check.changed) 
                    return;
                SerializedObject.ApplyModifiedProperties();
                changeCheck = ChangeCheck.Changed;
            }
        }

        void TypeSelected(object data)
        {
            var type = (Type) data;
            var obj = ScriptableObject.CreateInstance(type);
            //obj.hideFlags = HideFlags.HideInHierarchy;

            AssetDatabase.AddObjectToAsset(obj, Target);
            var provider = (ITileMeshMaterialUVProvider) obj;

            ref var geoToMeshMatData = ref m_setTypeInfo.Material.MeshMaterials[m_setTypeInfo.Index];
            var prevActionConfig = geoToMeshMatData.MeshMaterial.Result as ScriptableObject;
            if (prevActionConfig != null)
                prevActionConfig.DestroyEx();
            geoToMeshMatData.MeshMaterial = new RefITileMeshMaterialProvider() {Result = provider};

            AssetDatabase.SaveAssets();
            OnChanged();
        }

        void GenerateAtlas()
        {
            Verify();

            var hasWarnings = (Result.Errors + Result.Warnings) > 0;
            if (hasWarnings)
                return;

            var texAtlasDir = Path.GetDirectoryName(LevelTextureAtlasPath);
            // ReSharper disable once AssignNullToNotNullAttribute
            if (!texAtlasDir.IsNullOrEmpty() && !Directory.Exists(texAtlasDir))
                Directory.CreateDirectory(texAtlasDir);
            var materialsDir = Path.GetDirectoryName(LevelMaterialsPath);
            // ReSharper disable once AssignNullToNotNullAttribute
            if (!materialsDir.IsNullOrEmpty() && !Directory.Exists(materialsDir))
                Directory.CreateDirectory(materialsDir);

            using (var atlasToTextureDataScoped = SimplePool<Dictionary<AtlasDataConfig, Editor_AtlasDataValue>>.I.GetScoped())
            {
                var atlasToTexture = atlasToTextureDataScoped.Obj;
                CollectAllAtlasData(atlasToTexture);
                foreach (var kvp in atlasToTexture)
                    AtlasGeneration.GenerateAtlas(Target.name, kvp.Key.ConfigData, Target.UVLayoutConfig, kvp.Value,
                        GetMaterialPath(kvp.Key), LevelTextureAtlasPath, out var output);
            }
           
        }

        void CollectAllAtlasData(Dictionary<AtlasDataConfig, Editor_AtlasDataValue> atlasToTexture)
        {
            var collectAtlasData = new Editor_CollectAtlasData() { Data = atlasToTexture };
            foreach (var matTypes in Target.MaterialTypes)
            foreach (var meshMats in matTypes.MeshMaterials)
                meshMats.MeshMaterial.Result?.Editor_CollectAllAtlasData(collectAtlasData);

            foreach (var matTypes in Target.MaterialTransitions)
            foreach (var meshMats in matTypes.MeshMaterials)
                meshMats.MeshMaterial.Result?.Editor_CollectAllAtlasData(collectAtlasData);
        }

        void MaterialTypesGUI(float width, ref ChangeCheck changeCheck)
        {
            GUILayout.Label("Material Types", EditorStyles.boldLabel);
            var matTypesProp = SerializedObject.FindProperty(LevelMaterialTypeConfig.Editor_MaterialTypesPropName);
            MaterialListGUI(ref Target.Editor_MaterialTypes, width, matTypesProp, ref changeCheck, out var eraseIdx,
                MaterialTypeHeaderGUI, LevelMaterialType_PropertyGUI);
            ListAddOrRemove(ref Target.Editor_MaterialTypes, eraseIdx, ref changeCheck);
        }

        void MaterialTransitionTypesGUI(float width, ref ChangeCheck changeCheck)
        {
            GUILayout.Label("Material Transitions", EditorStyles.boldLabel);
            var matTypesProp = SerializedObject.FindProperty(LevelMaterialTypeConfig.Editor_MaterialTransitionTypesPropName);

            MaterialListGUI(ref Target.Editor_MaterialTransitionTypes, width, matTypesProp, ref changeCheck, out var eraseIdx,
                MaterialTransitionTypeHeaderGUI, LevelMaterialTransition_PropertyGUI);
            ListAddOrRemove(ref Target.Editor_MaterialTransitionTypes, eraseIdx, ref changeCheck);
        }

        void MaterialListGUI<T>(ref T[] arr, float width, SerializedProperty matTypesProp,
            ref ChangeCheck changeCheck, out int eraseIdx,
            HeaderGUIDelegate header, LevelMaterialPropertiesGUIDelegate lvlMaterialGUIDelegate)
            where T: new()
        {
            eraseIdx = -1;
            for (var i = 0; i < arr.Length; i++)
            {
                arr[i] ??= new T();
                var t = arr[i];
                var elProp = matTypesProp.GetArrayElementAtIndex(i);

                ListElementGUI(width, t as ILevelMaterial, elProp, ref changeCheck, out var erase,
                    header, lvlMaterialGUIDelegate);

                if (erase)
                    eraseIdx = i;
            }
        }

        static void ListAddOrRemove<T>(ref T[] arr, int eraseIdx, ref ChangeCheck changeCheck) where T: new()
        {
            var add = GUILayout.Button("+");
            var erase = eraseIdx != -1;
            if (add)
                Add(ref arr, new T());
            if (erase)
                RemoveAt(ref arr, eraseIdx);

            if (add || erase)
                changeCheck = ChangeCheck.Changed;
        }

        void ListElementGUI(float width, ILevelMaterial lvlMaterial, 
            SerializedProperty elementProp, ref ChangeCheck changeCheck, out bool erase,
            HeaderGUIDelegate header, LevelMaterialPropertiesGUIDelegate lvlMaterialGUI)
        {
            header.Invoke(lvlMaterial, elementProp, ref changeCheck, out erase);
            if (!elementProp.isExpanded)
                return;
            LevelMaterialContentGUI(width, lvlMaterial, elementProp, ref changeCheck, out erase,
                lvlMaterialGUI);
        }
        
        void LevelMaterialContentGUI(float width, ILevelMaterial lvlMaterial,
            SerializedProperty elementProp, ref ChangeCheck check, out bool erase,
            LevelMaterialPropertiesGUIDelegate lvlMaterialGUI)
        {
            erase = false;
            using (new IndentScope(true))
            {
                ref var meshMaterials = ref lvlMaterial.Editor_MeshMaterials;
                var meshMaterialProp = elementProp.FindPropertyRelative(lvlMaterial.Editor_MeshMaterialsPropName);

                lvlMaterialGUI.Invoke(width, lvlMaterial, elementProp, ref check);

                var eraseIdx = -1;
                for (var i = 0; i < meshMaterials.Length; i++)
                {
                    Debug.Assert(meshMaterialProp != null);
                    MeshMaterialGUI(width, meshMaterialProp.GetArrayElementAtIndex(i), ref meshMaterials[i], 
                        out var setButtonClicked, out var eraseMeshMat, ref check);

                    if (eraseMeshMat)
                        eraseIdx = i;
                    if (!setButtonClicked) 
                        continue;
                    m_setTypeInfo = new SetMeshMaterialForTypeInfo()
                    {
                        Material = lvlMaterial,
                        Index = i
                    };
                    PopupWindow.Show(GUILayoutUtility.GetLastRect(), m_tileMeshProviderPopup);
                }

                ListAddOrRemove(ref lvlMaterial.Editor_MeshMaterials, eraseIdx, ref check);
            }
        }

        void LevelMaterialType_PropertyGUI(float width, ILevelMaterial lvlMaterial,
            SerializedProperty elementProp, ref ChangeCheck check) =>
            ChildPropertiesGUI(elementProp, ref check, new[] {lvlMaterial.Editor_MeshMaterialsPropName});
        
        void LevelMaterialTransition_PropertyGUI(float width, ILevelMaterial lvlMaterial,
            SerializedProperty elementProp, ref ChangeCheck changeCheck)
        {
            var lvlMaterialTransition = (LevelMaterialTransition) lvlMaterial;

            var names = Target.MaterialTypes.Select(t => t.Name).ToArray();

            var idxA = Array.FindIndex(names, n => string.Equals(n, lvlMaterialTransition.NameA));
            var idxB = Array.FindIndex(names, n => string.Equals(n, lvlMaterialTransition.NameB));
            if (idxA != -1) lvlMaterialTransition.IndexA = (ushort) idxA;
            else if (names.IsIndexInRange(lvlMaterialTransition.IndexA))
                lvlMaterialTransition.NameA = names[lvlMaterialTransition.IndexA];

            if (idxB != -1) lvlMaterialTransition.IndexB = (ushort) idxB;
            else if (names.IsIndexInRange(lvlMaterialTransition.IndexB))
                lvlMaterialTransition.NameB = names[lvlMaterialTransition.IndexB];

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                lvlMaterialTransition.IndexA = (ushort) EditorGUILayout.Popup(lvlMaterialTransition.IndexA, names);
                lvlMaterialTransition.IndexB = (ushort) EditorGUILayout.Popup(lvlMaterialTransition.IndexB, names);
                
                if (check.changed)
                {
                    if (names.IsIndexInRange(lvlMaterialTransition.IndexA))
                        lvlMaterialTransition.NameA = names[lvlMaterialTransition.IndexA];
                    if (names.IsIndexInRange(lvlMaterialTransition.IndexB))
                        lvlMaterialTransition.NameB = names[lvlMaterialTransition.IndexB];

                    changeCheck = ChangeCheck.Changed;
                }
            }
        }

        void MeshMaterialGUI(float width, SerializedProperty elementProp,
            ref GeometryToMeshMaterialData meshMat, 
            out bool setButtonClicked, out bool eraseClicked,
            ref ChangeCheck changeCheck)
        {
            eraseClicked = false;
            setButtonClicked = false;
            // set GeoConfig
            if (Target.GeometryConfig != null
                && meshMat.GeometryId.TileConfig.Result != Target.GeometryConfig)
            {
                meshMat.GeometryId.TileConfig = new RefITileConfig() {Result = Target.GeometryConfig};
                changeCheck = ChangeCheck.Changed;
            }

            bool inspecting;
            using (new EditorGUILayout.HorizontalScope())
            {
                eraseClicked = GUILayout.Button("x", GUILayout.Width(25f));
                if (meshMat.MeshMaterial.Object != null)
                    meshMat.MeshMaterial.Object.name = GUILayout.TextField(meshMat.MeshMaterial.Object.name, GUILayout.Width(150f));
                using (new EditorGUILayout.VerticalScope())
                {
                    var geoProp = elementProp.FindPropertyRelative(nameof(GeometryToMeshMaterialData.GeometryId));
                    using (var check = new EditorGUI.ChangeCheckScope())
                    {
                        EditorGUILayout.PropertyField(geoProp, GUILayout.Width(100f));
                        if (check.changed)
                        {
                            SerializedObject.ApplyModifiedProperties();
                            changeCheck = ChangeCheck.Changed;
                        }
                    }
                }

                var meshMatRefProp = elementProp.FindPropertyRelative(nameof(GeometryToMeshMaterialData.MeshMaterial));
                var objProp = meshMatRefProp.FindPropertyRelative(InterfaceContainerBase.Editor_ObjectFieldPropName);
                CommonSubConfigEditorGUI.ConfigReferenceGUI(SerializedObject, meshMat.MeshMaterial.Object as ScriptableObject, objProp);
                
                MeshMaterialInspectButton("Inspect", meshMat.MeshMaterial, out inspecting);
            }

            if (meshMat.MeshMaterial.Result == null) 
                setButtonClicked = GUILayout.Button("Set MeshMaterial");

            if (!inspecting) 
                return;

            using (new IndentScope(true))
            {
                if (m_cachedTileMeshMaterialEditor is IBaseInspector inspector)
                    inspector.GetEditor().OnGUI(width);
                else if(m_cachedTileMeshMaterialEditor != null)
                    m_cachedTileMeshMaterialEditor.OnInspectorGUI();
            }
        }

        void MaterialTransitionTypeHeaderGUI(ILevelMaterial lvlMaterial,
            SerializedProperty propEl, ref ChangeCheck changeCheck, out bool erase)
        {
            var lvlMaterialTransition = (LevelMaterialTransition) lvlMaterial;
            var content = new GUIContent {text = $"{lvlMaterialTransition.NameA}_{lvlMaterialTransition.NameB}"};
            HeaderGUI(content, propEl, ref changeCheck, out erase);
        }
        

        void MaterialTypeHeaderGUI(ILevelMaterial lvlMaterial, SerializedProperty propEl, 
            ref ChangeCheck changeCheck, out bool erase)
        {
            var lvlMaterialType = (LevelMaterialType) lvlMaterial;
            var content = new GUIContent {image = lvlMaterialType.PreviewTex, text = $"  {lvlMaterialType.Name}"};
            HeaderGUI(content, propEl, ref changeCheck, out erase);
        }

        void HeaderGUI(GUIContent content, SerializedProperty propEl, ref ChangeCheck changeCheck,
            out bool erase)
        {
            erase = false;
            using (var check = new EditorGUI.ChangeCheckScope())
            using (new GUILayout.HorizontalScope())
            {
                if (GUILayout.Button("X", GUILayout.Width(20.0f))) 
                    erase = true;

                GUILayout.Space(10.0f);

                var expand = EditorGUILayout.Foldout(propEl.isExpanded, content, true);
                if (expand != propEl.isExpanded)
                {
                    propEl.isExpanded = expand;
                    SerializedObject.ApplyModifiedProperties();
                }
                
                if (check.changed)
                    changeCheck = ChangeCheck.Changed;
            }
        }

        void MeshMaterialInspectButton(string buttonLabel, RefITileMeshMaterialProvider provider, out bool active)
        {
            active = m_cachedTileMeshMaterialEditor != null && m_cachedTileMeshMaterialEditor.target == provider.Object;
            if (!CustomGUI.ActivityButton(active, buttonLabel, false)) 
                return;

            active = !active;
            if (active)
                UnityEditor.Editor.CreateCachedEditor(provider.Object, null, ref m_cachedTileMeshMaterialEditor);
            else
            {
                m_cachedTileMeshMaterialEditor.DestroyEx();
                m_cachedTileMeshMaterialEditor = null;
            }
        }

        //todo: move to general place, baseEditor or Operations
        void ChildPropertiesGUI(SerializedProperty prop, ref ChangeCheck changeCheck, string[] ignoreNames = null)
        {
            var enter = true;
            var breakProperty = prop.Copy();
            breakProperty.NextVisible(false);

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                while (prop.NextVisible(enter) && !string.Equals(prop.propertyPath, breakProperty.propertyPath))
                {
                    if (ignoreNames.Contains(prop.name))
                        continue;

                    enter = false;
                    EditorGUILayout.PropertyField(prop, true);
                }

                if (!check.changed)
                    return;

                SerializedObject.ApplyModifiedProperties();
                changeCheck = ChangeCheck.Changed;
            }
        }
    }
}