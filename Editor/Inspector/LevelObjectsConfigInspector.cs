using Core.Editor.Inspector;
using Core.Editor.Interface;
using Level.Objects;
using UnityEditor;
using UnityEngine;

namespace Level.Editor.Data
{
    [CustomEditor(typeof(LevelObjectsConfig))]
    public class LevelObjectsConfigInspector : BaseInspector<LevelObjectsConfigEditor>
    {
        public override DefaultGUISettings DefaultGUISettings => new DefaultGUISettings
        {
            DrawDefaultGUIFoldout = true, CanUnlockDefaultGUI = true, DefaultGUIEnabled = false
        };
    }

    public class LevelObjectsConfigEditor : BaseEditor<LevelObjectsConfig>
    {
        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);
        }

        public override void OnGUI(float width)
        {
            if (GUILayout.Button("Clear missing references"))
            {
                Target.ClearMissingPrefabs();
            }
            
            base.OnGUI(width);
        }
        
    }
}