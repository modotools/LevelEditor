﻿using Core.Editor.Inspector;
using Core.Editor.Utility.GUITools;
using Level.Actions.PlatformGrid;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Editor.Actions;
using UnityEditor;


namespace Level.Editor.Action
{
    [CustomEditor(typeof(GridIterator))]
    public class GridIteratorInspector : BaseInspector<GridIteratorEditor>{}
    public class GridIteratorEditor : BaseEditor<GridIterator>
    {
        CommonSubConfigEditorGUI.ConfigMenuData m_menuData;
        CommonSubConfigEditorGUI.ConfigData m_configData;

        public override void Terminate()
        {
            base.Terminate();
            ReleaseEditor(ref m_configData);
        }

        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);

            InitConfigDataDefault(out m_configData, "Continue with", nameof(GridIterator.ContinueWith));
            CreateMenu(typeof(ScriptableBaseAction), ref m_menuData, (data) => OnTypeSelected(m_configData, data));
        }

        public override void OnGUI(float width)
        {
            base.OnGUI(width);
            DefaultConfigGUI(ref m_configData, ref m_menuData);
        }
    }
}
