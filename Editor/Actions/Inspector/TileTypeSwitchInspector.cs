﻿using Core.Editor.Inspector;
using Core.Editor.Utility.GUITools;
using Level.Actions.TileType;
using Level.Tiles.Interface;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Editor;
using ScriptableUtility.Editor.Actions;
using UnityEditor;
using UnityEngine;

namespace Level.Editor.Action
{
    [CustomEditor(typeof(TileTypeSwitch))]
    public class TileTypeSwitchInspector : BaseInspector<TileTypeSwitchEditor>{}
    public class TileTypeSwitchEditor : BaseEditor<TileTypeSwitch>
    {
        CommonSubConfigEditorGUI.ConfigMenuData m_menuData = CommonSubConfigEditorGUI.ConfigMenuData.Default;
        Vector2 m_scrollPos;

        CommonSubConfigEditorGUI.ConfigListData m_configsList;

        ITileConfig m_cacheTileConfig;

        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);
            InitConfigListDataDefault(out m_configsList, TileTypeSwitch.Editor_ActionProperty, "{0}");
            CreateMenu(typeof(ScriptableBaseAction), ref m_menuData, (data) => OnTypeSelected(m_configsList, data));

            SetLabels();
        }

        void SetLabels()
        {
            m_configsList.CustomLabels = null;
            m_cacheTileConfig = Target.Editor_TileConfig;
            if (m_cacheTileConfig == null)
                return;
            m_configsList.CustomLabels = new GUIContent[m_cacheTileConfig.Count];
            for (var i = 0; i < m_cacheTileConfig.Count; i++)
                m_configsList.CustomLabels[i] = new GUIContent(m_cacheTileConfig[i].TileName);
        }

        public override void Terminate()
        {
            base.Terminate();

            ReleaseEditor(ref m_configsList);
        }

        public override void OnGUI(float width)
        {
            NameGUI(TileTypeSwitch.Editor_NameProperty);
            if (ParentContainer is UnityEditor.Editor editor)
                editor.DrawDefaultInspector();

            if (m_cacheTileConfig != Target.Editor_TileConfig)
                SetLabels();

            using (var scroll = new EditorGUILayout.ScrollViewScope(m_scrollPos))
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_scrollPos = scroll.scrollPosition;

                UpdateConfigListData(ref m_configsList);
                ListGUI(ref m_configsList, ref m_menuData);

                if (check.changed)
                    OnChanged();
            }
        }
    }
}
